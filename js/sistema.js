$(document).ready(function() {
    $("form").attr('autocomplete','off');
	// cargar semilleristas que estan en curso
	$(".informar").fadeOut();
	//obtener el sistema para realizar la consulta
	var sistema =$(".ulogin").val();
	var mdl;
	if (sistema=='certificacion'){
	    mdl = 'certificacion';
	} else{
	    mdl = 'fiscalizacion';
	}
	$.ajax({
	    type : "GET",
        url : "control/index.php?mdl="+sistema+"&opt=ver&pag=proceso&sistema="+sistema,
        beforeSend : function() {
            $(".post").html("<div style='width:60%;margin-top:30%;padding-left:50%'><img src='images/loader_3a.gif '/><a> Cargando...</a></div>");
        },
        success : function(data) {
            $.funciones.ocultarMensaje(200);
            $(".post").empty().append(data);
        }
     });
	
	//**datos de manejo en index*/
    $("input[name=submit]").fadeTo('fast', 0.5).mouseover(function() {
        $(this).fadeTo('fast', 1);
    }).mouseleave(function() {
        $(this).fadeTo('fast', 0.5);
    });
    //pagina de inicio
    $("label.inicio,label.usuario,label.img").css('cursor', 'pointer');
    $("#inicio.img, label.inicio").click(function() {
        $(".informar").empty();
        location.href = 'index.php';
    });

    // datos del usuario
    $("label.usuario").attr("title", "Informacion de usuario").click(function() {
        $(".informar").fadeOut();
        setTimeout(function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php',
                data : {
                    mdl : 'login',
                    pag : 'ver_datos'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('ok', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        }, 100);

    });
    $("#usuario.img").attr("title", "Informacion de usuario");
    // salir desl sistema
    $("#sesion.img, label.img").click(function() {
        $.ajax({
            type : "GET",
            url : "control/index.php",
            data : {
                mdl : 'login',
                pag : 'salir'
            },
            beforeSend : function() {
                $.funciones.mostrarMensaje('wait', 'Cerrando Sesion...');
            },
            success : function(data) {
                console.log(data);
                switch (data) {
                    case 'error':
                        $("#ui-icon").addClass(" ui-icon-alert").empty().after("Error. No se pudo salir del sistema");
                        $("#dialog-message").attr('title', 'Sistema de Informacion de Certificacion y Fiscalizacion de Semillas').fadeIn();
                        $("#dialog-message").dialog({
                            dialogClass: "no-close",
                            modal : true,
                            resizable : false,
                        });
                        setTimeout(function() {
                            $("#dialog-message").dialog("close");
                        }, 2000);
                        $(location).attr('href', 'index.php');
                        break;
                    case 'OK':
                        $("#ui-icon").addClass(" ui-icon-info").empty().after("Cerrando sesion...<br>Espere un momento");
                        $("#dialog-message").attr('title', 'SICFIS').fadeIn();
                        $("#dialog-message").dialog({
                            dialogClass: "no-close",
                            modal : false,
                            resizable : false
                        });
                        setTimeout(function() {
                            $("#dialog-message").dialog("close");
                        }, 2000);
                        location.href = 'home.php';
                        break;
                    case '403':
                    $("#ui-icon").addClass(" ui-icon-info").empty().after("Tiempo de sesion expirado.Por favor ingrese nuevamente");
                        $("#dialog-message").attr('title', 'SICFIS').fadeIn();
                        $("#dialog-message").dialog({
                            dialogClass: "no-close",
                            modal : false,
                            resizable : false
                        });
                        setTimeout(function() {
                            $("#dialog-message").dialog("close");
                        }, 2000);
                        location.href = 'home.php';
                    break;    
                }
            }
        });
    });
}); 