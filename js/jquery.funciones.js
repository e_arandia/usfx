jQuery.funciones = {
    actualizarDataBase : function(filename) {
        $("div.plantilla").hide();
        $.getJSON("control/index.php", {
            mdl : 'importar',
            opt : 'actualizar',
            pag : 'database',
            file : filename
        }, function(json) {
            if (json.msg == "OK") {
                $("div#exito>div>p>span.msg").empty().append(json.description);
                $("div#exito>div>p>span.msg").show().fadeOut(5000);
                $("div.plantilla").show();
            } else {
                $("div#exito>div>p>span.msg").empty().append(json.description);
                $("div#exito>div>p>span.msg").show().fadeOut(6000);
            }
            $(".messages").html("");
        });
    },
    //alternar colores
    alternarColores : function(vector) {
        $.each(vector, function(indice, elemento) {
            $("div.body >div." + elemento + ":odd").css('background-color', '#DDF7B7');
        });
    },
    //alternar colores
    alternarColoresTabla : function(idtabla) {
        $("table#" + idtabla + ">tbody>tr:odd").css('background-color', '#DDF7B7');
    },
    //agregar una nueva categoria
    agregarCategoria : function(inputID) {
        datos = $('form#categoria').serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            categoria = $('input#' + inputID);
            var mensaje = '';

            if (!categoria.val() || (categoria.val()).length < 3) {
                mensaje += '<div>- Nombre Categoria</div>';
                categoria.css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                categoria.css({
                    backgroundColor : ""
                });
            }
            if (mensaje != '') {
                $("#errores div p .mensaje").empty().append('Se han detectado los siguientes errores: <br>' + mensaje);
                $("#errores").css('margin-top', '4em').fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK':
                $(".checked").empty();
                $.funciones.mostrarMensaje('ok', 'Registro Exitoso!!!');
                $.funciones.recargarVerDatosAdmin('login', 'categoria');
                $.funciones.ocultarMensaje(2000);
                break;
            default:
                $.funciones.mostrarMensaje('error', statusText + ' ' + responseText);
                $.funciones.ocultarMensaje(2000);
                $(".checked").empty();
                break;
            }
        }

    },
    //agregar colores de opciones
    alternarColorOpcionImagen : function(opciones) {
        $.each(opciones, function(indice, elemento) {
            $("div.body >div.options>div." + elemento + ">input:odd").css('background-color', '#DDF7B7');
        });
    },
    //agregar colores de opciones
    alternarColorCVEE : function(opciones) {
        $.each(opciones, function(indice, elemento) {
            $("div.body >div." + elemento + ">div." + elemento + ">input:odd").css('background-color', '#DDF7B7');
        });
    },
    alternarColorProceso : function() {
        $("div.tbody >div.tr.control>div.td.estado>input:even").css('background-color', '#edffd4');
    },
    //agregar colores de opciones
    alternarColorModificarPermisos : function(opciones) {
        $.each(opciones, function(indice, elemento) {
            $("div.body >div.options>div." + elemento + ">input:even").css('background-color', '#DDF7B7');
        });
    },
    //agregar color estado de usuario
    alternarColorEstado : function(opciones) {
        $.each(opciones, function(indice, elemento) {
            $("div.body >div.allow>div." + elemento + ">input:odd").css('background-color', '#DDF7B7');
        });
    },
    //nombre base de un archivo
    basename : function(path, suffix) {
        //  discuss at: http://locutus.io/php/basename/
        // original by: Kevin van Zonneveld (http://kvz.io)
        // improved by: Ash Searle (http://hexmen.com/blog/)
        // improved by: Lincoln Ramsay
        // improved by: djmix
        // improved by: Dmitry Gorelenkov
        var b = path;
        var lastChar = b.charAt(b.length - 1);
        if (lastChar === '/' || lastChar === '\\') {
            b = b.slice(0, -1);
        }
        b = b.replace(/^.*[/\\]/g, '');
        if ( typeof suffix === 'string' && b.substr(b.length - suffix.length) === suffix) {
            b = b.substr(0, b.length - suffix.length);
        }
        return b;

    },
    //buscar comunidad por nro de solicitud semillera y semillerista
    buscarComunidadBy : function(nro, semillera, semillerista) {
        $.getJSON('control/index.php', {
            mdl : 'fiscalizacion',
            opt : 'ver',
            pag : 'solicitud_semillera',
            opc : 'no_era_ista',
            nro : nro,
            slr : semillera,
            srt : semillerista
        }, function(json) {
            $("input#comunidad").val(json.comunidad);
            $("form#semilla").fadeIn();
        });
    },
    //buscar saldos de una cuenta
    buscarSaldo : function(semillera, semillerista, cultivo, variedad) {
        $.getJSON("control/index.php", {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'cuota',
            slr : semillera,
            smr : semillerista,
            cul : cultivo,
            variedad : variedad
        }, function(json) {
            if (json.saldo > 0) {
                $('#lst-cuota label.sld>span.total').empty().append(json.saldo);
            } else {
                saldo_total = $("div#ctaFinal>div>input#saldototal").val();
                $('#lst-cuota label.sld>span.total').empty().append(saldo_total);
            }

        });
    },
    //buscar cuotas segun el id semilla
    buscarCuotasCuenta : function(isemilla) {
        $('.tbl-cuota').show();
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'cuotas',
            id : isemilla
        }, function(json) {
            //if (json.total >= 1){
            var html = '';
            $.each(json.fecha, function(index, fecha) {
                html += '<tr style=\'text-align:center\'>';
                html += '<td class=\'' + index + '\'>' + json.fecha[index] + '</td>';
                html += '<td class=\'' + index + '\'>' + json.cuota[index] + '</td>';
                html += '<td class=\'' + index + '\'>' + json.saldo[index] + '</td>';
                html += '</tr>';
            });
            //agregar cuotas a dialogo
            $('.tbl-cuota>.table>tbody.buscar').empty().append(html);
            //mostrar cuadro de dialogo
            $.funciones.mostrarDialogoCuotaCuenta('cuotas');
            $('div#tbl-detalle-cuenta').show();
        });
    },
    buscarDetalleCuotasCuenta : function(isemilla) {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'cuotas',
            id : isemilla
        }, function(json) {
            //if (json.total >= 1){
            var html = '';
            $.each(json.fecha, function(index, fecha) {
                html += '<tr style=\'text-align:center\'>';
                html += '<td class=\'' + index + '\'>' + json.fecha[index] + '</td>';
                html += '<td class=\'' + index + '\'>' + json.cuota[index] + '</td>';
                html += '<td class=\'' + index + '\'>' + json.saldo[index] + '</td>';
                html += '</tr>';
            });
            //agregar cuotas a dialogo
            $('.tbl-cuota>.table>tbody.buscar').empty().append(html);
            //mostrar cuadro de dialogo
            $.funciones.mostrarDialogoCuotaCuenta('detalle');
        });
    },
    //carga los datos de un numero de campo para muestra de laboratorio
    buscarDatosCampoForLaboratorio : function(nroCampo) {

        if (nroCampo != "") {
            $("div#btn-muestra").show();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'muestraCampo',
                campo : nroCampo
            }, function(json) {
                $("#cultivo").val(json.cultivo);
                $("#variedad").val(json.variedad);
                $("input#nroSolicitud").val(json.solicitud);
                $("input#origen").val(json.comunidad);
                $("div#solicitud").fadeIn(2000);
                $("div#btn-muestra").fadeIn(4000);
            });
        } else {
            $("div#btn-muestra").hide("fast");
        }
    },
    //busqueda de solicitudes registradas
    buscarSolicitudesRegistradas : function() {
        //busqueda por filtro
        var q = $('input#filter').val();
        if (q != '') {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'filtro',
                opc : 'solicitud',
                search : q
            }, function(json) {
                $("div.body").empty();
                var final = json.total;
                var div = '';
                $.each(json.semillerista, function(index, value) {
                    if (index < json.total) {
                        div += "<div class=\"nSolicitud\"><label class=>" + json.nosolicitud[index] + "</label> </div>";
                        div += "<div class=\"sistema sistem\"><label>" + json.sistema[index] + "</label> </div>";
                        div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                        div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                        div += "<div class=\"dpto\"><label>" + json.departamento[index] + "</label> </div>";
                        div += "<div class=\"prov\"><label>" + json.provincia[index] + "</label> </div>";
                        div += "<div class=\"municipio\"><label>" + json.municipio[index] + "</label> </div>";
                        div += "<div class=\"comunidad\"><label>" + json.comunidad[index] + "</label> </div>";
                        div += "<div class=\"fecha\" style=\"width: 6em\"><label>" + json.fecha[index] + "</label> </div>";
                        div += "<div style=\"padding-top:1%;width: 6em;\" class=\"pdf\"><a id=" + json.id[index] + " style=\"cursor:pointer;padding:0\" class=\"glyphicon glyphicon-print btn-lg\"></a></div>";
                        if (edt == 1110)
                            div += "<div><input type=\"image\" value=\"" + json.id[index] + "\" style=\"position: relative;\" src=\"images/editar22x22.png\" name=\"upd\" alt=\"editar\"></div>";
                    } else {
                        return false;
                    }
                });
                $("div.body").append(div);
                //colorear celdas
                var celdas = ['nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'comunidad', 'fecha', 'pdf', 'options'];
                $.funciones.alternarColores(celdas);
                var opciones = ['editar', 'eliminar'];
                $.funciones.alternarColorOpcionImagen(opciones);
                //envio de datos
                $("div.body").show();
            });
        } else {
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        }

    },
    //buscar semillas registradas
    buscarSemillasRegistradas : function() {
        var q = $('input#filter').val();
        if (q != '') {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'filtro',
                opc : 'semilla',
                search : q
            }, function(json) {
                $('div.body').empty();
                var final = json.total;
                var div = '';
                $.each(json.semillerista, function(index, value) {
                    if (index < json.total) {
                        div += '<div class="semillera"><label class=>' + json.semillera[index] + '</label> </div>';
                        div += '<div class="productor"><label>' + json.semillerista[index] + '</label> </div>';
                        div += '<div class="comunidad" style="width:8em"><label>' + json.comunidad[index] + '</label> </div>';
                        div += '<div class="nSolicitud"><label>' + json.nro_campo[index] + '</label> </div>';
                        div += '<div class="campanha"><label>' + json.campanha[index] + '</label> </div>';
                        div += '<div class="cultivo"><label>' + json.cultivo[index] + '</label> </div>';
                        div += '<div class="variedad" style="text-transform:capitalize"><label>' + json.variedad[index] + '</label> </div>';
                        div += '<div class="cSembrada"><label>' + json.categoria_sembrada[index] + '</label> </div>';
                        div += '<div class="cProducir"><label>' + json.categoria_producir[index] + '</label> </div>';
                        div += '<div class="cantidadS"><label>' + json.cantidad_semilla[index] + '</label> </div>';
                        div += '<div class="fecha" style="width:7em"><label>' + json.fecha[index] + '</label> </div>';
                        div += '<div class="plantas"><label>' + json.plantas_hectarea[index] + '</label> </div>';
                        div += '<div class="sParcela cultAnterior" style="width: 6em"><label>' + json.cultivo_anterior[index] + '</label> </div>';
                        div += '<div class="sParcela supParcela" style="width: 6em"><label>' + json.parcela[index] + '</label> </div>';
                        div += '<div style="padding-top:1%;width: 6em;" class="pdf"><a id=' + json.id[index] + ' style="cursor:pointer;padding:0" class="glyphicon glyphicon-print btn-lg"></a></div>';
                        if (edt == 1110)
                            div += '<div><input type="image" value="' + json.id[index] + '" style="position: relative;" src="images/editar22x22.png" name="upd" alt="editar"></div>';
                    } else {
                        return false;
                    }
                });
                $('div.body').append(div);
                //colorear celdas
                var celdas = ['semillera', 'nSolicitud', 'comunidad', 'localidad', 'campanha', 'cultivo', 'variedad', 'cSembrada', 'cProducir'];
                $.funciones.alternarColores(celdas);
                var celdas2 = ['cantidadS', 'lote', 'fecha', 'plantas', 'cAnterior', 'cultAnterior', 'supParcela', 'productor', 'pdf', 'options'];
                $.funciones.alternarColores(celdas2);
                var opciones = ['editar', 'eliminar'];
                $.funciones.alternarColorOpcionImagen(opciones);
                //envio de datos
                $('div.body').show();
            });
        } else {
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilla',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $('.post').empty().append(data);
            });
        }
    },
    //buscar superficies registradas
    buscarSuperficiesRegistradas : function (){
        var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'superficie',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillerista, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\" style=\"width:17em;\"><label>"+json.semillera[index]+"</label> </div>";
                            div += "<div class=\"productor\" style=\"width:10em\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"comunidad\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"sInscrita nro\" style=\"width:6em\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"sInscrita ins\" style=\"width:6em\"><label>" + json.inscrita[index] + "</label> </div>";
                            div += "<div class=\"sInscrita rec\" style=\"width:7em\"><label>" + json.rechazada[index] + "</label> </div>";
                            div += "<div class=\"sInscrita ret\" style=\"width:6em\"><label>" + json.retirada[index] + "</label> </div>";
                            div += "<div class=\"sInscrita apr\" style=\"width:5.8em\"><label>" + json.aprobada[index] + "</label> </div>";
                            div += "<div class=\"pdf\" style=\"padding-top:1%;width: 6em;\"><a id=\" "  + json.isuperficie[index]+ "\" class=\"glyphicon glyphicon-print btn-lg\" style=\"cursor:pointer;padding:0\"></a></div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','productor','comunidad','nro','ins','rec','ret','apr'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'superficie',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
            }
    },
    //carga los numeros de campos para muestra en certificacion
    cargarListaCamposCertifica : function() {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'lista_campos',
            sistema : 1
        }, function(json) {
            if (json.msg != 'error') {
                //console.log(json.length));
                $.each(json.campos, function(index, value) {
                    if (value) {
                        $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');
                    } else {
                        $("select#nrocampo").empty().append('<option value="">No existen muestras</option>');
                        $("select#muestra").selectmenu({
                            disabled : true
                        });
                        $("select#nrocampo").selectmenu({
                            disabled : true
                        });
                    }
                });
                $("select#nrocampo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
                $("select#nrocampo").selectmenu("refresh");
            } else {
                $("select#nrocampo").selectmenu({
                    disabled : true
                });
            }
        });
    },
    //cargar lista de muestras segun sistema
    cargarListaMuestras : function(sistema) {
        //cargar muestras de campos
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'lista_campos',
            sistema : sistema
        }, function(json) {
            if (json.total > 0) {
                $("select#nrocampo").empty();
                $("select#nrocampo").append('<option value="">[ Seleccione campo ]</option>');
                $.each(json.campos, function(index, value) {
                    $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');
                });
                $("select#nrocampo").selectmenu({
                    disabled : false
                });
                $("select#nrocampo").selectmenu("refresh");
            } else {
                $("select#nrocampo").empty().append('<option value="">No existen muestras</option>');
                $("select#nrocampo").selectmenu({
                    disabled : true
                });
                $("select#nrocampo").selectmenu("refresh");
            }
        });
    },
    //lista de campos de semillaproducida
    cargarCamposSemillaProducida : function(id) {
        $.getJSON("control/index.php", {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'campos_cosecha',
            id : id
        }, function(json) {
            //console.log(json.length));
            $.each(json.nroCampo, function(index, value) {
                if (value) {
                    $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');

                } else {
                    $("select#nrocampo").empty().append('<option value="">No existen muestras</option>');
                    $("select#nrocampo").selectmenu({
                        disabled : true
                    });
                    $("select#nrocampo").selectmenu("refresh");
                }
            });
        });
    },
    //calcular rendimiento de maiz
    calcularRendimientoMaiz : function() {
        var mazorcas_por_metro = $('input#mazorcas-por-metro').val();
        var granos_por_mazorca = $('input#granos').val();
        var peso_mil_granos = $('input#peso-mil-granos').val();
        //resultado en toneladas por hectarea
        var rinde = (mazorcas_por_metro * granos_por_mazorca * peso_mil_granos * 10000 ) / 1000000000;
        $('input#rendimientoE').val(rinde.toFixed(2));
        parcela = $('input#sup_parcela').val();
        $('input#rend_campo').val((parcela * rinde).toFixed(2));
    },
    //calcular rendimiento papa
    calcularRendimientoPapa : function() {
        var plantas_por_metro = $('input#plantas-por-metro').val();
        var peso_tuberculo_por_planta = $('input#peso-tuberculo-planta').val();
        var cantidad_tuberculo_por_planta = $('input#cantidad-tuberculos-plantas').val();
        //resultado en kilo por hectarea
        var rinde = (plantas_por_metro * peso_tuberculo_por_planta * cantidad_tuberculo_por_planta) / 1000;
        $('input#rendimientoE').val(rinde.toFixed(2));
        parcela = $('input#sup_parcela').val();
        $('input#rend_campo').val((parcela * rinde).toFixed(2));

    },
    //calcular rendimiento soya
    calcularRendimientoSoya : function() {
        var nro_granos = $('input#granos-vaina').val();
        var nro_vainas_planta = $('input#vainas-plantas').val();
        var plantas_hectarea = $('input#plantas-hectarea-soya_arveja').val();
        var peso_mil_granos = $('input#peso-granos-soya_arveja').val();
        //resultado en kilogramos por hectarea
        plantas_hectarea /= 10000;
        var rinde = (nro_granos * nro_vainas_planta * plantas_hectarea * peso_mil_granos) / 1000;
        $('input#rendimientoE').val(rinde.toFixed(2));
        parcela = $('input#sup_parcela').val();
        $('input#rend_campo').val((parcela * rinde).toFixed(2));
    },
    //calcular rendimiento trigo - cebada
    calcularRendimientoTrigoCebada : function() {
        var distancia_surcos = $('input#distanciasurcos-trigo-cebada').val();
        //distancia entre surcos en centimetros
        var espigas = $('input#espigas-por-metro').val();
        //en metros cuadrados
        var granos = $('input#granos-por-espiga').val();
        //granos por espiga
        var peso_mil_granos = $('input#peso-granos-trigo_cebada').val();
        var coeficiente = 0.8;
        //resultado en kilogramos por hectarea
        var rinde = (espigas * granos * peso_mil_granos * coeficiente) / 1000;
        $('input#rendimientoE').val(rinde.toFixed(2));
        parcela = $('input#sup_parcela').val();
        $('input#rend_campo').val((parcela * rinde).toFixed(2));

    },
    calendarioEspanhol : function() {
        $.datepicker.regional['es'] = {
            closeText : 'Cerrar',
            prevText : '<Ant',
            nextText : 'Sig>',
            currentText : 'Hoy',
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sab'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            weekHeader : 'Sm',
            dateFormat : 'dd/mm/yy',
            firstDay : 1,
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar",
            isRTL : false,
            showMonthAfterYear : false,
            yearSuffix : ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    },
    //calenario de filtro
    calendario : function(campo, id) {
        $(campo + "#" + id).datepicker({
            changeMonth : false,
            changeYear : false,
            buttonText : "calendario",
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            dateFormat : "dd-mm-yy",
            minDate : new Date(new Date().getFullYear() - 1, 11, 1),
            maxDate : "0d",
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar"
        });
    },
    //calenario de filtro
    calendarioInicial : function(campo, id) {
        $(campo + "#" + id).datepicker({
            changeMonth : false,
            changeYear : false,
            buttonText : "calendario",
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat : "dd-mm-yy",
            minDate : new Date(new Date().getFullYear(), 0, 1),
            maxDate : "0d",
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar"
        });
    },
    //calenario de filtro
    calendarioFinal : function(campo, id) {
        $(campo + "#" + id).datepicker({
            changeMonth : false,
            changeYear : false,
            buttonText : "calendario",
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat : "dd-mm-yy",
            minDate : new Date(new Date().getFullYear(), 0, 1),
            maxDate : new Date(new Date().getFullYear(), 11, 31),
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar"
        });
    },

    //cambiar signo por acento
    cambiarSignoPorAcento : function(cadena) {
        if (cadena != '') {
            texto = cadena.toString();
            cambiar = texto.replace(/\!/g, "á");
            cambiar = cambiar.replace(/\¡/g, "Á");
            cambiar = cambiar.replace(/\#/g, "é");
            cambiar = cambiar.replace(/\?/g, "É");
            cambiar = cambiar.replace(/\$/g, "í");
            cambiar = cambiar.replace(/\¿/g, "Í");
            cambiar = cambiar.replace(/\%/g, "ó");
            cambiar = cambiar.replace(/\{/g, "Ó");
            cambiar = cambiar.replace(/\&/g, "ú");
            cambiar = cambiar.replace(/\}/g, "Ú");
        } else {
            cambiar = '';
        }
        return cambiar;
    },
    //cambiar signo por enhe
    cambiarSignoPorEnhe : function(cadena) {
        if (cadena != '') {
            texto = cadena.toString();
            cambiar = texto.replace(/\(/g, "ñ");
            cambiar = cambiar.replace(/\)/g, "Ñ");
        } else {
            cambiar = '';
        }
        return cambiar;
    },
    editarGeneracion : function() {
        datos = $("form#actualizacion").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            nombre_generacion = $("input#generacion");

            msg = '';

            if (!nombre_generacion.val()) {
                nombre_generacion.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Nombre de generaci&oacute;n</div>';
            } else {
                nombre_generacion.css({
                    backgroundColor : ""
                });
            }

            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("edt-generacion");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatosAdmin('login', 'categoria'), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //enviar semilla para certificacion
    enviarSemillaCertificada : function() {
        datos = $("form#semilla").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            cant_semilla = $("#cant_semilla");
            plantasha = $("#plantasha");
            superficie = $("#superficie");
            cultivo = $("select#cultivo").find(':selected');
            variedad = $("select#variedad").find(':selected');
            cat_sembrada = $("select#cat_sembrada").find(':selected');
            cat_producir = $("select#cat_producir").find(':selected');
            cult_anterior = $("select#cult_anterior").find(':selected');
            msg = '';

            if (!cant_semilla.val()) {
                cant_semilla.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Cantidad de semilla</div>';
            } else {
                cant_semilla.css({
                    backgroundColor : ""
                });
            }
            if (!plantasha.val()) {
                plantasha.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- plantas por hectarea</div>';
            } else {
                plantasha.css({
                    backgroundColor : ""
                });
            }
            if (!superficie.val()) {
                superficie.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Superficie Parcela</div>';
            } else {
                superficie.css({
                    backgroundColor : ""
                });
            }
            if (!cultivo.val()) {
                $("#lbl-cultivo").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                $("#lbl-cultivo").css({
                    color : ""
                });
            }
            if (variedad.val() == '') {
                $('#lbl-variedad').css({
                    color : '#9b000d'
                });
                msg += '<div>- Variedad</div>';
            } else if (variedad.val() == 'Add_variedad') {
                $('#lbl-variedad').css({
                    color : '#9b000d'
                });
                msg += '<div>- Variedad</div>';
            } else if (variedad.val() == 'add_variedad') {
                $('#lbl-variedad').css({
                    color : '#9b000d'
                });
                msg += '<div>- Variedad</div>';
            } else {
                $('#lbl-variedad').css({
                    color : ''
                });
            }
            if (!cat_sembrada.val()) {
                $("#lbl-cat-sembrada").css({
                    color : "##9b000d"
                });
                msg += '<div>- Categoria sembrada</div>';
            } else {
                $("#lbl-cat-sembrada").css({
                    color : ""
                });
            }
            if (!cat_producir.val()) {
                $("#lbl-cat-producir").css({
                    color : "#9b000d"
                });
                msg += '<div>- Categoria a producir</div>';
            } else {
                $("#lbl-cat-producir").css({
                    color : ""
                });
            }
            if (!cult_anterior.val()) {
                $("#lbl-cult-anterior").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo anterior</div>';
            } else {
                $("#lbl-cult-anterior").css({
                    color : ""
                });
            }

            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-semilla");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'semilla', 1, 1100), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    enviarSemillaFiscal : function() {
        datos = $("form#semilla").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            cultivo = $("select#cultivo").find(':selected');
            variedad = $("select#variedad").find(':selected');
            categoria_producir = $("select#cgeneracion").find(':selected');
            msg = '';

            if (!cultivo.val()) {
                $("#lbl-cultivo").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                $("#lbl-cultivo").css({
                    color : ""
                });
            }
            if (!variedad.val() || variedad.val == "add_variedad") {
                $("#lbl-variedad").css({
                    color : "#9b000d"
                });
                msg += '<div>- Variedad</div>';
            } else {
                $("#lbl-variedad").css({
                    color : ""
                });
            }
            if (!categoria_producir.val()) {
                $("#lbl-categoria-producir").css({
                    color : "##9b000d"
                });
                msg += '<div>- Categoria sembrada</div>';
            } else {
                $("#lbl-categoria-producir").css({
                    color : ""
                });
            }
            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-semilla-fiscal");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('fiscalizacion', 'semilla', 2, 1100), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //validacion y envio de datos hoja cosecha de fiscalizacion
    enviarCosechaFiscal : function() {
        datos = $("form#cosecha").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            planta_acondicionadora = $("input#plantaa");
            msg = '';

            if (!planta_acondicionadora.val()) {
                planta_acondicionadora.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Planta Acondiconadora</div>';
            } else {
                planta_acondicionadora.css({
                    backgroundColor : ""
                });
            }

            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-cosecha-fiscal");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('fiscalizacion', 'cosecha', 2, 1100), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //enviar cuenta certifica
    enviarCuentaCertifica : function() {
        datos = $('form#cuenta').serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : function() {
                //mensaje de advertencia
            },
            success : showResponse
        });
        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK':
                setTimeout($.funciones.recargarVerDatos('certificacion', 'cuentas', 1, 1100), 10000);
                break;
            }
        }

    },
    //enviar acondicionamiento
    enviarAcondicionamiento : function() {
        datos = $("form#acondicionamiento").serialize();

        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            nro_campo = $("input#nocampo");
            fecha_emision = $("input#emision");
            categoria_campo = $("select#categoria_acondicionamiento").find(':selected');
            rendimiento_estimado = $("input#rendimientoE");
            rendimiento_campo = $("input#rendimientocampo");
            superficie = $("input#superficie-acondicionamiento");
            planta_acondicionadora = $("input#plantaacondicionadora");
            peso_bruto = $("input#pesobruto");
            msg = '';

            if (!nro_campo.val()) {
                nro_campo.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- N&uacute;mero campo</div>';
            } else {
                nro_campo.css({
                    backgroundColor : ""
                });
            }
            if (!fecha_emision.val()) {
                fecha_emision.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Fecha emision</div>';
            } else {
                temp = fecha_emision.val();
                temp2 = temp.split('-');
                longitud = temp2.length;
                if (longitud == 3 && temp.length == 10) {
                    dia = temp2[0];
                    mes = temp2[1];
                    anho = temp2[2];
                    if (dia >= 1 && dia <= 31) {
                        if (mes >= 1 && mes <= 12) {
                            fecha_emision.css({
                                backgroundColor : ""
                            });
                        } else {
                            fecha_emision.css({
                                backgroundColor : "#f5c9c9"
                            });
                            msg += '<div>- Mes incorrecto</div>';
                        }
                        if (anho < 2000) {
                            msg += '<div>- A&ntilde;o incorrecto</div>';
                        } else {
                            fecha_emision.css({
                                backgroundColor : ""
                            });
                        }
                    } else {
                        fecha_emision.css({
                            backgroundColor : "#f5c9c9"
                        });
                        msg += '<div>- Dia Incorrecto</div>';
                    }

                } else {
                    fecha_emision.css({
                        backgroundColor : "#f5c9c9"
                    });
                    msg += '<div>- Fecha emision</div>';
                }

            }
            if (!superficie.val()) {
                superficie.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Superficie</div>';
            } else {
                superficie.css({
                    backgroundColor : ""
                });
            }
            if (!categoria_campo.val()) {
                $("label#lbl-categoria-acondicionamiento").css({
                    color : "#9b000d"
                });
                msg += '<div>- Categoria Campo</div>';
            } else {
                $("label#lbl-categoria-acondicionamiento").css({
                    color : ""
                });
            }
            if (!rendimiento_estimado.val()) {
                rendimiento_estimado.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Rendimiento Estimado</div>';
            } else {
                rendimiento_estimado.css({
                    backgroundColor : ""
                });
            }
            if (!rendimiento_campo.val()) {
                rendimiento_campo.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Rendimiento Campo</div>';
            } else {
                rendimiento_campo.css({
                    backgroundColor : ""
                });
            }
            if (!planta_acondicionadora.val()) {
                planta_acondicionadora.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Planta acondicionadora</div>';
            } else {
                planta_acondicionadora.css({
                    backgroundColor : ""
                });
            }
            if (!peso_bruto.val()) {
                peso_bruto.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Peso Bruto de Semilla</div>';
            } else {
                peso_bruto.css({
                    backgroundColor : ""
                });
            }
            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-acondicionamiento");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'acondicionamiento', 1, 1100), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //enviar plantines
    enviarPlantines : function() {
        datos = $("form#plantines").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            cultivo = $("select#cultivo-plantines").find(':selected');
            variedad = $("select#variedad-plantines").find(':selected');
            msg = '';

            if (!cultivo.val()) {
                $("#lbl-cultivo-plantines").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                $("#lbl-cultivo-plantines").css({
                    color : ""
                });
            }
            if (!variedad.val() || variedad.val == "add_variedad") {
                $("#lbl-variedad-plantines").css({
                    color : "#9b000d"
                });
                msg += '<div>- Variedad</div>';
            } else {
                $("#lbl-variedad-plantines").css({
                    color : ""
                });
            }

            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-plantines");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'plantines', 1, 1100), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //enviar superficie
    enviarSuperficie : function() {
        datos = $("form#FormAprobada").serialize();
        //console.log(datos);
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            var aprobada = $('input#aprobada');
            var inscrita = $('inscrita#inscrita');
            msg = '';
            if (!(aprobada.val() >= 0)) {
                msg += '<div>La superficie aprobada debe ser mayor a 0.0</div>';
                aprobada.css({
                    backgroundColor : '#f5c9c9'
                });
            } else {
                aprobada.css({
                    backgroundColor : ''
                });
            }

            if (aprobada.val().length == 0) {
                msg += '<div>Debe ingresar la superficie aprobada</div>';
                aprobada.css({
                    backgroundColor : '#f5c9c9'
                });
            } else {
                aprobada.css({
                    backgroundColor : ''
                });
            }
            if (aprobada.val() > inscrita.val()) {
                msg += '<div>La superficie aprobada es mayor a la superficie inscrita</div>';
                aprobada.css({
                    backgroundColor : '#f5c9c9'
                });
            } else {
                aprobada.css({
                    backgroundColor : ''
                });
            }
            if (msg != '') {
                $('#errores div p .mensaje').empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $('#errores').fadeIn('slow');
                return false;
            } else {
                $('#errores').fadeOut('slow');
                $.funciones.deshabilitarTexto('enviar-superficie');
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'superficie', 1, 1100), 10000);
                break;
            }
        }

    },
    //enviar semilla producida para certificacion
    enviarSemillaProducida : function() {
        datos = $("form#semillaprod").serialize();
        //console.log(datos);
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            nro_campo = $('select#nrocampo').find(':selected');
            msg = '';
            if (!nro_campo.val()) {
                $("#lbl-nocampo-semilla_producida").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                $("#lbl-nocampo-semilla_producida").css({
                    color : ""
                });
            }
            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-semillaProd");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'produccion', 1, 1100), 10000);
                break;
            }
        }

    },
    //enviar muestra de laboratorio para certificacion
    enviarMuestraCertifica : function() {
        datos = $('form#muestraForm').serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            tipo_analisis = $('input:checkbox:checked');
            msg = '';
            if (!tipo_analisis.val()) {
                $('#lbl-tp-analisis').css({
                    color : '#9b000d'
                });
                msg = 'ERROR';
            } else {
                $('#lbl-tp-analisis').css({
                    color : ''
                });
            }
            if (msg != '') {
                $('#errores div p .mensaje').empty().append('Debe seleccionar un tipo de an&aacute;lisis');
                $('#errores').fadeIn('slow');
                return false;
            } else {
                $('#errores').fadeOut('slow');
                $.funciones.deshabilitarTexto('enviar-muestra');
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK':
                $(".informar").empty();
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'new',
                    pag : 'muestras',
                    area : 1,
                }, function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                });
                //remover opciones seleccionadas anteriores
                $.funciones.removeSelectedOption();
                //agregar estilo
                $.funciones.addTextStyle('li-vlab', 'vlaboratorio');
                break;
            }
        }

    },
    //enviar muestra de laboratorio para fiscalizacion
    enviarMuestraFiscal : function() {
        datos = $("form#muestraForm").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            nro_campo = $('select#nrocampo').find(':selected');
            semilla = $('select#semilla').find(':selected');
            tipo_analisis = $('input:radio[name=nro]:checked');
            msg = '';
            if (!nro_campo.val()) {
                $("#lbl-nocampo-semilla_producida").css({
                    color : "#9b000d"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                $("#lbl-nocampo-semilla_producida").css({
                    color : ""
                });
            }
            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-muestra");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatos('certificacion', 'produccion', 1, 1100), 10000);
                break;
            }
        }

    },
    //ocultar etapas segun estado
    etapasRegistro : function(estado) {
        switch(estado) {
        case 0 :
            $("a#lbl-reg-semilla").css('display', 'none');
            $("a#lbl-reg-superficie,div#reg-superficie").css('display', 'none');
            $("a#lbl-reg-inspeccion,div#reg-inspeccion").css('display', 'none');
            $("a#lbl-reg-hoja_cosecha,div#hoja_cosecha").css('display', 'none');
            $("a#lbl-reg-laboratorio,div#laboratorio").css('display', 'none');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 1 :
            //SEMILLA
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie,div#reg-superficie").css('display', 'none');
            $("a#lbl-reg-inspeccion,div#reg-inspeccion").css('display', 'none');
            $("a#lbl-reg-hoja_cosecha,div#hoja_cosecha").css('display', 'none');
            $("a#lbl-reg-laboratorio,div#laboratorio").css('display', 'none');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 4 :
            //SUPERFICIE
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion,div#reg-inspeccion").css('display', 'none');
            $("a#lbl-reg-hoja_cosecha,div#hoja_cosecha").css('display', 'none');
            $("a#lbl-reg-laboratorio,div#laboratorio").css('display', 'none');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case '5':
        case '6':
        case '7':
        case 5:
        case 6:
        case 7:
            //INSPECCION
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion").css('display', '');
            $("a#lbl-reg-hoja_cosecha,div#hoja_cosecha").css('display', 'none');
            $("a#lbl-reg-laboratorio,div#laboratorio").css('display', 'none');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 8 :
            //HOJA COSECHA
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion").css('display', '');
            $("a#lbl-reg-hoja_cosecha").css('display', '');
            $("a#lbl-reg-laboratorio,div#laboratorio").css('display', 'none');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 9:
        case 11:
        case '9':
        case '11' :
            //MUESTRA Y RESULTADO LABORATORIO
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion").css('display', '');
            $("a#lbl-reg-hoja_cosecha").css('display', '');
            $("a#lbl-reg-laboratorio").css('display', '');
            $("a#lbl-reg-semilla_producida,div#producida").css('display', 'none');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 12 :
            //SEMILLA PRODUCIDA
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion").css('display', '');
            $("a#lbl-reg-hoja_cosecha").css('display', '');
            $("a#lbl-reg-laboratorio").css('display', '');
            $("a#lbl-reg-semilla_producida").css('display', '');
            $("a#lbl-reg-cuenta,div#reg-cuenta").css('display', 'none');
            break;
        case 13 :
            //CUENTA
            $("a#lbl-reg-semilla").css('display', '');
            $("a#lbl-reg-superficie").css('display', '');
            $("a#lbl-reg-inspeccion").css('display', '');
            $("a#lbl-reg-hoja_cosecha").css('display', '');
            $("a#lbl-reg-laboratorio").css('display', '');
            $("a#lbl-reg-semilla_producida").css('display', '');
            $("a#lbl-reg-cuenta").css('display', '');
            break;
        }
    },
    //calendario inicial para gestiones anteriores
    gestionesAnterioresCalendarioInicial : function(campo, id, gestion) {
        $(campo + "." + id).datepicker({
            changeMonth : true,
            changeYear : false,
            buttonText : "calendario",
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat : "dd-mm-yy",
            minDate : new Date(gestion, 0, 1),
            maxDate : new Date(gestion, 11, 31),
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar"
        });
    },
    //calendario final gestiones anteriores
    //calenario de filtro
    gestionesAnterioresCalendarioFinal : function(campo, id, gestion) {
        $(campo + "#" + id).datepicker({
            changeMonth : true,
            changeYear : false,
            buttonText : "calendario",
            monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
            dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat : "dd-mm-yy",
            minDate : new Date(gestion, 0, 1),
            maxDate : new Date(gestion, 11, 31),
            showButtonPanel : true,
            currentText : "Hoy",
            closeText : "Cerrar"
        });
    },
    //creacion de PDF para gestiones anteriores
    gestionesPasadasPDF : function() {
        pestana = $('input#informes').val();
        area = $('input#area').val();
        if (pestana == 'detalle') {
            cultivo = $('select#cultivo').find(':selected').val();
            if (cultivo != '') {
                $('#lbl-cultivo').css({
                    color : ''
                });
                $('div.alert').fadeOut('slow');
                $('div.alert').css({
                    'margin-left' : '',
                    'margin-top' : ''
                });
                win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=detalle&sistema=' + area + '&gestion=' + gestion + '&desde=' + desde + '&hasta=' + hasta + '&cultivo=' + cultivo, '_blank');
            } else {
                $('#lbl-cultivo').css({
                    color : '#9b000d'
                });
                $('div.alert>label').empty().text('Debe seleccionar un cultivo');
                $('div.alert').css({
                    'margin-left' : '20%',
                    'margin-top' : '-14%'
                }).fadeIn('slow');
            }
        } else {
            //captura de fecha inicial y fecha final
            desde = $('input.f_desde');
            hasta = $('input.f_hasta');
            desde_valor = $('input.f_desde').val();
            hasta_valor = $('input.f_hasta').val();
            win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=' + pestana + '&sistema=' + area + '&gestion=' + gestion + '&desde=' + desde_valor + '&hasta=' + hasta_valor, '_blank');
        }
    },
    //creacion de EXCEL para gestiones anteriores
    gestionesPasadasXLS : function() {
        tab_clicked = $('input#informes').val();
        var desde = $('input.f_desde').val();
        var hasta = $('input.f_hasta').val();
        var area = $('input#area').val();
        var gestion = $('select#gestion').find(':selected').val();
        if (tab_clicked == 'detalle') {
            var cultivo = $('select#cultivo').find(':selected').val();
            if (cultivo != '') {
                $(this).css('disabled', 'disabled');
                $('#lbl-cultivo').css({
                    color : ''
                });
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'detalle',
                    cultivo : cultivo,
                    sistema : area,
                    gestion : gestion,
                    desde : desde,
                    hasta : hasta
                }, function(json) {
                    $('div.alert').fadeOut('slow');
                    if (json.msg == 'OK') {
                        $('.resumenXLS').empty().append('<iframe class=\'icostos\' style=\'display:none\' src=\'' + json.file + '\'></iframe>');
                        window.open(json.file, '_blank');
                    } else {
                        var user_msg = 'No existen registros en la fecha especificada';
                        $('#advertencia>div>p').empty().append(user_msg);
                        $('#advertencia').show().fadeOut(3500);
                    }
                });
            } else {
                $('#lbl-cultivo').css({
                    color : '#9b000d'
                });
                $('div.alert>label').empty().text('Debe seleccionar un cultivo');
                $('div.alert').css({
                    'margin-left' : '20%',
                    'margin-top' : '-14%'
                }).fadeIn('slow');
            }
        } else {
            switch (tab_clicked) {
            case 'resumen':
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'resumen',
                    gestion : gestion,
                    sistema : area,
                    desde : desde,
                    hasta : hasta
                }, function(data) {
                    if (data.msg != 'error') {
                        $('div.mostrar-detalle-resumen').show();
                        $('.resumenXLS').empty().append('<iframe class=\'iresumen\' style=\'display:none\' src=\'' + data + '\'></iframe>');
                    } else {
                        $('div.mostrar-detalle-resumen').hide();
                        var user_msg = 'No existen registros en la fecha especificada';
                        $('#advertencia>div>p').empty().append(user_msg);
                        $('#advertencia').show().fadeOut(3500);
                    }
                });
                break;
            case 'superficie':
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'superficie',
                    area : area,
                    gestion : gestion,
                    desde : desde,
                    hasta : hasta
                }, function(json) {
                    if (json.msg != 'error') {
                        $('.resumenXLS').empty().append('<iframe class=\'iresumen\' style=\'display:none\' src=\'' + json.xls + '\'></iframe>');
                    } else {
                        $('.alert>label').empty().append('Error al crear archivo');
                        $('.alert').show().fadeOut(3000);
                    }
                });
                break;
            case 'volumen':
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'volumen',
                    area : area,
                    gestion : gestion,
                    desde : desde,
                    hasta : hasta
                }, function(json) {
                    if (json.msg != 'error') {
                        $('.resumen-date').fadeIn();
                        $('.resumenXLS').empty().append('<iframe class=\'iresumen\' style=\'display:none\' src=\'' + json.xls + '\'></iframe>');
                    } else {
                        $('.resumen-date').hide();
                        $('.alert>label').empty().append('Error al crear archivo');
                        $('#advertencia').show().fadeOut(3000);
                    }
                });
                break;
            }
        }
    },
    //cargar campos para muestras
    cargarCampos : function(module, select, area) {
        $.getJSON('control/index.php', {
            mdl : module,
            opt : 'buscar',
            pag : 'lista_campos',
            area : area
        }, function(json) {
            $.each(json, function(index, value) {
                if (json.msg == 'OK') {
                    $("select#" + select).empty().append('<option value="">[ Seleccione campo ]</option>');
                    $.each(json.campos, function(index, value) {
                        $("select#" + select).append('<option value="' + value + '">' + value + '</option>');
                    });
                    $("select#" + select).selectmenu({
                        disabled : false
                    });
                } else {
                    $("select#" + select).empty().append('<option value="">No existen muestras</option>');
                    $("select#" + select).selectmenu({
                        disabled : true
                    });
                }
                $("select#" + select).selectmenu("refresh");
            });
        });
    },
    //devuelve todos los cultivos
    cargarListaCultivos : function(id_select) {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'ls_cultivos',
            gestion : new Date().getFullYear()
        }, function(json) {
            if (json.cultivo.length > 0) {
                $.each(json.cultivo, function(index, value) {
                    $('select#' + id_select).append('<option value="' + value + '">' + value + '</option>');
                });
            }
        });
    },
    //devuelve todos los cultivos
    cargarListaSemilleras : function(id_select, area) {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'lst_semilleras_acre',
            area : area
        }, function(json) {
            if (json.length > 0) {
                $.each(json, function(index, value) {
                    $('select#' + id_select).append('<option value="' + value + '">' + value + '</option>');
                });
            }
        });
    },
    //cargar muestras de campos disponibles
    cargarMuestrasCampos : function() {
        $.getJSON('control/index.php', {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'lista_campos',
            sistema : 3
        }, function(json) {
            //console.log(json.length));
            $.each(json.campos, function(index, value) {
                if (value) {
                    $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');

                } else {
                    $("select#nrocampo").empty().append('<option value="">No existen muestras</option>');
                    $("select#muestra").selectmenu({
                        disabled : true
                    });
                    $("select#nrocampo").selectmenu({
                        disabled : true
                    });
                    $("select#nrocampo").selectmenu("refresh");

                }
            });
        });
    },
    cargarTiposSemilla : function(select) {
        $.getJSON('control/index.php', {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'tipo_semilla'
        }, function(json) {
            //console.log(json.length));
            $.each(json.tipo, function(index, value) {
                if (value) {
                    $("select#" + select).append('<option value="' + json.id[index] + '">' + value + '</option>');

                } else {
                    $("select#" + select).empty().append('<option value="">Tipos de semilla</option>');
                }

                $("select#" + select).selectmenu();
            });
        });
    },
    cargarMuestraCampos : function(nroCampo) {
        $.getJSON('control/index.php', {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'muestraCampo',
            sistema : 'laboratorio',
            campo : nroCampo
        }, function(json) {
            $("#cultivo").val(json.cultivo);
            $("#variedad").val(json.variedad);
            $("input#nroSolicitud").val(json.solicitud);
            $("div#solicitud").fadeIn(2000);
            $("div#btn-muestra").fadeIn(4000);
        });
    },
    //copia los datos de las cajas de texto a buscar
    copiarBusquedaDatos : function(id) {
        $("form#" + id).find(':text').each(function() {
            var elemento = this;
            $("input[name=" + elemento.id + "]").val(elemento.value);
        });
    },
    //muestra el cultivo y variedad segun el numero de campo
    cultivoVariedadFiscal : function(proceso, nro_campo) {
        $.getJSON('control/index.php', {
            mdl : proceso,
            opt : 'buscar',
            pag : 'cultivo_variedad',
            nro : nro_campo
        }, function(json) {

            $("input#cultivo").val(json.cultivo);
            $("input#variedad").val(json.variedad);
        });
    },
    //deshabilitar boton y agregar texto enviando..
    deshabilitarTexto : function(id) {
        imagen = '<span class=\"glyphicon glyphicon-save\"><\span>';
        $('#' + id).html(imagen + ' Guardando...').attr("disabled", "disabled");
    },
    //vaciar campo de texto
    vaciarCampoTexto : function(id) {
        $("input#" + id).val('');
    },
    //mostrar campo animado
    animarMostrarCampo : function(tipo, id) {
        $(tipo + "#" + id).fadeIn("slow");
    },
    //ocultar campo animado
    animarOcultarCampo : function(tipo, id) {
        $(tipo + "#" + id).fadeOut("slow");
    },
    //carga el formulario para nueva solicitud
    nuevaSolicitud : function() {
        $("button#btn-new").fadeOut();
        $("div.div-new").show();
        $("div.div-filter").hide(function() {
            $("button#btn-new").hide();
            valor = $('div#paginacion').css('display');
            if (valor != 'none') {
                $('div#paginacion').hide();
            }
        });
        $("form#filter-box").slideUp();
        $("div#btn-new-filter").css({
            'float' : 'right',
            'margin-top' : '6%'
        });
        $.ajax({
            data : {
                mdl : 'certificacion',
                pag : 'solicitud',
                opt : 'new'
            },
            beforeSend : function() {
                $.funciones.mostrarMensaje('wait', 'Cargando...');
            }
        }).done(function(html) {
            //$(".entry").dialog();
            $.funciones.cargarImagen('seguimiento', 'certificacion');
            $.funciones.cargarImagen('solicitud', 'certificacion');
            $("div.informar").slideDown();
            $("div.filter").slideUp();
            //ocultar paginas
            $("div#paginacion").hide();
            $(".post").empty().append(html);
        });
    },
    //mostrar campo
    noAnimarMostrarCampo : function(tipo, id) {
        $(tipo + "#" + id).hide("fast");
    },
    //ocultar campo
    noAnimarOcultarCampo : function(tipo, id) {
        $(tipo + "#" + id).hide("fast");
    },
    //buscar resultados de muestras de laboratorio
    buscarResultados : function(term) {
        alert(term);
        $.post('control/index.php', {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'respruebas',
            buscar : term
        }, function(showResponse) {
            alert(showResponse);
            $("#bx_resultado").empty().prepend(showResponse);
        });
    },

    //permitir solo (1-9)
    soloNumeros : function(nombreId) {
        $.each(nombreId, function(index, value) {
            $("input#" + value).on("keypress", function(key) {
                if ((key.which < 48 || key.which > 57) && (key.which != 46)/*punto decimal*/ && (key.which != 8)/*retroceso*/)
                    return false;
            });
        });
    },
    //permitir solo letras
    soloLetras : function(nombreId) {
        $("input#" + nombreId).on("keypress", function(key) {
            if ((key.which < 97 || key.which > 122)//letras mayusculas
                && (key.which < 65 || key.which > 90)//letras minusculas
                && (key.which != 8)//retroceso
                && (key.which != 241)//ñ
                && (key.which != 209)//Ñ
                && (key.which != 32)//espacio
                && (key.which != 225)//á
                && (key.which != 233)//é
                && (key.which != 237)//í
                && (key.which != 243)//ó
                && (key.which != 250)//ú
                && (key.which != 193)//Á
                && (key.which != 201)//É
                && (key.which != 205)//Í
                && (key.which != 211)//Ó
                && (key.which != 218) //Ú

                )
                return false;
        });
    },
    //permitir alfanumericos (0-9)(A-Z)
    alfanumerico : function(nombreInput) {
        $('#' + nombreInput).keypress(function(event) {
            if (event.which && ((event.which <= 57 && event.which >= 46 ) || (event.which == 32 || event.which < 65 || event.which > 90 ))) {
                event.preventDefault();
            }
        });
    },
    //campanha
    getCampanha : function(nombreInput) {
        var date = new Date();

        var mes = date.getMonth();
        var ano = date.getFullYear().toString();
        var digitos = ano.substr(2);
        //dos ultimod digitos del año

        var campanas = $("#" + nombreInput);
        if (mes >= 6 && mes <= 12)
            var campana = 'invierno';
        else
            var campana = 'verano';
        campanas.attr('value', campana + '/' + digitos);
    },
    //campanha
    getCampanhaSpan : function(idSpan) {
        var date = new Date();

        var mes = date.getMonth();
        var ano = date.getFullYear().toString();
        var digitos = ano.substr(2);
        //dos ultimod digitos del año

        var campanas = $("#" + idSpan);
        if (mes >= 6 && mes <= 12)
            var campana = 'Invierno';
        else
            var campana = 'Verano';
        campanas.text(campana + '/' + digitos);
        $("input#hiddencampania").empty().val(campana + '/' + digitos);
    },
    //imprimir PDF
    imprimirPDFsolicitud : function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idsolicitud").val($("a.glyphicon-print").attr("id"));
        }).on("mouseleave", function() {
            $("input#idsolicitud").val('');
        }).on("click", function() {
            var id = $("input#idsolicitud").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=solicitud&id=' + id, '_blank');
            win.focus();
        });
    },
    //generar password
    generarPass : function(nombreObj) {
        var newPass = '';

        $.getJSON('control/index.php', {
            mdl : 'usuario',
            pag : 'generar_pass'
        }, function(json) {
            $("#" + nombreObj).val(json.password);
        });
    },
    guardarPermisos : function(id, psistem) {
        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'pactualizar',
            pid : id,
            psistem : psistem
        }, function(json) {
            if (json.msg == 'POK') {
                $.funciones.mostrarMensaje('info', 'Permiso de edicion concedido');
                //$.funciones.ocultarMensaje();
                $.ajax({
                    url : 'control/index.php',
                    method : 'get',
                    data : {
                        mdl : 'login',
                        pag : 'permisos'
                    },
                    success : function(responseText) {
                        $(".post").empty().append(responseText);
                    }
                });
            } else {
                $.funciones.mostrarMensaje('info', 'permiso de edicion eliminado');
                //$.funciones.ocultarMensaje();
                $.ajax({
                    url : 'control/index.php',
                    method : 'get',
                    data : {
                        mdl : 'login',
                        pag : 'permisos'
                    },
                    success : function(responseText) {
                        $(".post").empty().append(responseText);
                    }
                });
            }
        });
    },
    listaSemilleristasBySemilleraForGraph : function(destino, semillera) {

        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'lst_productoresBySemillera',
            slr : semillera
        }, function(json) {
            if (json.msg == 'OK') {

                $.each(json.semillerista, function(index, value) {
                    $('select#' + destino).append('<option value="' + value + '">' + value + '</option>');
                });
                $('select#' + destino).selectmenu({
                    disabled : false
                });
            } else {
                $('select#' + destino).selectmenu("destroy").empty();
                $('select#' + destino).selectmenu();
                $('select#' + destino).append('<option value="" selected="selected">No existen semilleristas</option>');
            }
        });
    },
    listaCultivosForGraph : function(destino, semillera, semillerista) {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'lst_productoresBySemillera',
            slr : semillera,
            smr : semillerista
        }, function(json) {
            if (json.msg == 'OK') {

                $.each(json.cultivo, function(index, value) {
                    $('select#' + destino).append('<option value="' + value + '">' + value + '</option>');
                });
                $('select#' + destino).selectmenu({
                    disabled : false
                });
            } else {
                $('select#' + destino).selectmenu("destroy").empty();
                $('select#' + destino).selectmenu();
                $('select#' + destino).append('<option value="" selected="selected">No existen cultivos</option>');
            }
        });
    },
    //formatea el numero a la forma xxx,xxx,xxx.xx
    modificar_decimales : function(input) {
        $('#' + input).blur(function() {
            numero = $(this).val();
            var posDot = numero.length - numero.lastIndexOf('.');
            var size = numero.length;
            var difSize = size - posDot;
            var miles = numero.substring(0, difSize);
            var decimales = numero.substring(difSize + 1, posDot);
            if (miles.indexOf(',') < -1)
                var aux1 = miles.split(',');
            var aux2;
            var tamanho = aux.length;
            for (var i = 0; i <= tamanho - 1; i++) {
                aux2 += aux1[i] + ',';
            }
            aux2 += aux1[tamanho];
            aux2 += '.' + decimales;
            $('#' + input).empty().val(aux2);
        });
    },
    //mostrar cuadro para calcular rendimiento  segun cultivo
    calculadoraRendimientoCultivo : function() {
        var cultivo = $('.tbl-cultivo-hoja-cosecha').text();
        switch(cultivo) {
        case 'Amaranto':
        case 'Trigo':
        case 'Cebada':
            $("div#calcularTrigoCebada>div>input").val('');
            $("div#calcularTrigoCebada").dialog({
                title : 'Calculo de rendimiento estimado de ' + cultivo,
                resizable : false,
                height : 320,
                buttons : {
                    'Aceptar' : function() {
                        $.funciones.calcularRendimientoTrigoCebada();
                        //mostrar resultado
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    },
                    'Cancelar' : function() {
                        rendimiento = $("input#rendimientoE");
                        if (!rendimiento.val().length) {
                            rendimiento.val('');
                        }
                        $(this).dialog('close');
                    }
                }
            });
            break;
        case 'Cumanda':case 'Haba':
        case 'Frejol':case 'Mani': case 'Ajo':
        case 'Aji':
        case 'Soya':
        case 'Arveja':
            $("div#calcularSoya>div>input").val('');
            numeros_enteros = ['granos-vaina', 'vainas-plantas', 'plantas-hectarea-soya_arveja'];
            $.funciones.soloNumeros(numeros_enteros);
            $("div#calcularSoya>div>input#peso-granos-soya_arveja").numeric();
            $("div#calcularSoya").dialog({
                title : 'Calcular rendimiento de ' + cultivo,
                resizable : false,
                height : 280,
                width : 300,
                buttons : {
                    'Aceptar' : function() {
                        $.funciones.calcularRendimientoSoya();
                        //mostrar resultado
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    },
                    'Cancelar' : function() {
                        $("input#rend_campo").val('');
                        $(this).dialog('close');
                    }
                }
            });
            break;
        case 'Papa':
            $("div#calcularPapa>div>input").val('');
            numeros_enteros = ['plantas-por-metro', 'peso-tuberculo-planta', 'cantidad-tuberculos-plantas', 'peso-tuberculo'];
            $.funciones.soloNumeros(numeros_enteros);
            $("div#calcularPapa").dialog({
                title : 'Calcular rendimiento de ' + cultivo,
                resizable : false,
                height : 240,
                width : 300,
                buttons : {
                    'Aceptar' : function() {
                        $.funciones.calcularRendimientoPapa();
                        //mostrar resultado
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    },
                    'Cancelar' : function() {
                        $(this).dialog('close');
                    }
                }
            });
            break;
        case 'Maiz':
            $("div#calcularMaiz>div>input").val('');
            //permitir solo numeros
            numeros_enteros = ['mazorcas-por-metro', 'granos', 'peso-mil-granos'];
            $.funciones.soloNumeros(numeros_enteros);
            $("div#calcularMaiz").dialog({
                title : 'Calcular rendimiento de ' + cultivo,
                resizable : false,
                height : 250,
                buttons : {
                    'Aceptar' : function() {
                        $.funciones.calcularRendimientoMaiz();
                        //mostrar resultado
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    },
                    'Cancelar' : function() {
                        $(this).dialog('close');
                    }
                }
            });
            break;
        }
    },

    mostrarDialogoCuotaCuenta : function(tipo) {
        //ocultar datos para ingresar monto
        $('div#tbl-detalle-cuenta').hide();
        $('#lst-cuota').dialog({// Dialog
            title : 'Cuotas',
            dialogClass : "no-close",
            resizable : false,
            height : 400,
            buttons : {
                "Aceptar" : function() {
                    //$("div#tipo-cuota").buttonset("destroy");
                    if (tipo != 'detalle') {
                        var saldo = $("input#saldototal").val();
                        var monto = $("input#cuota").val();
                        var fecha_pago = $("input#f_pago").val();
                        var descripcion = $('input:radio[name=cuota]:checked').val();
                        $("input#montoPagado").val(monto).addClass("not-edit");
                        $("input#saldototal").val(parseInt(saldo) - parseInt(monto));
                        //copiar datos a campos ocultos
                        $("input#cuota-monto").val(monto);
                        $("input#cuota-fecha").val(fecha_pago);
                        $("input#cuota-descripcion").val(descripcion);
                        $("div#tipo-cuota").buttonset("destroy");
                    }
                    $('#lst-cuota').dialog("close");
                    $('#lst-cuota').dialog("destroy");
                },
                "Cancelar" : function() {
                    $(this).dialog("close");
                }
            }
        });
    },

    //carga los datos segun el estado
    mostrarDatosRegistro : function(estado, json) {
        switch(estado) {
        case 0:
            //solicitud
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            break;
        case 1:
            //solicitud y semilla
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            break;
        case 4:
            //solicitud,semilla y superficie
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            break;
        case 5:
        case 6:
        case 7:
            //solicitud,semilla,superficie e inspeccion
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            // inspeccion---------------------------------------------------------------------------------------
            $(".rvegetativa").empty().text(json.vegetativa);
            $("span.robsvegetativa").empty().text(json.obs_vegetativa);
            $(".rfloracion").empty().text(json.floracion);
            $("span.robsfloracion").empty().text(json.obs_floracion);
            $(".rcosecha").empty().text(json.cosecha);
            $("span.robscosecha").empty().text(json.obs_cosecha);
            break;
        case 8:
            //solicitud,semilla,superficie,inspeccion y hoja cosecha
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            // inspeccion---------------------------------------------------------------------------------------
            $(".rvegetativa").empty().text(json.vegetativa);
            $("span.robsvegetativa").empty().text(json.obs_vegetativa);
            $(".rfloracion").empty().text(json.floracion);
            $("span.robsfloracion").empty().text(json.obs_floracion);
            $(".rcosecha").empty().text(json.cosecha);
            $("span.robscosecha").empty().text(json.obs_cosecha);
            // hoja cosecha-----------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rfemision").empty().text(json.emision);
            $(".rcatcampo").empty().text(json.catSembrada);
            $(".restimado").empty().text(json.rendEstimado);
            $(".rparcela").empty().text(json.aprobada);
            $(".rrendcampo").empty().text(json.rendCampo);
            $(".rcupones").empty().text(json.cupones);
            $(".rplantaa").empty().text(json.plantaa);
            break;
        case 9:
        case 11:
            //solicitud,semilla,superficie,inspeccion,hoja cosecha,[muestra - resultado laboratorio] y semilla producida
            //solicitud,semilla,superficie,inspeccion y hoja cosecha
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            // inspeccion---------------------------------------------------------------------------------------
            $(".rvegetativa").empty().text(json.vegetativa);
            $("span.robsvegetativa").empty().text(json.obs_vegetativa);
            $(".rfloracion").empty().text(json.floracion);
            $("span.robsfloracion").empty().text(json.obs_floracion);
            $(".rcosecha").empty().text(json.cosecha);
            $("span.robscosecha").empty().text(json.obs_cosecha);
            // hoja cosecha-----------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rfemision").empty().text(json.emision);
            $(".rcatcampo").empty().text(json.catSembrada);
            $(".restimado").empty().text(json.rendEstimado + " (TM / Ha.)");
            $(".rparcela").empty().text(json.aprobada + " (Ha.)");
            $(".rrendcampo").empty().text(json.rendCampo + " (TM.)");
            $(".rcupones").empty().text(json.cupones);
            $(".rplantaa").empty().text(json.plantaa);
            // laboratorio-----------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rnoanalisis").empty().text(json.noanalisis);
            $(".rfecharecepcion").empty().text(json.recepcion);
            $(".rfecharesultado").empty().text(json.resultado);
            $(".rorigen").empty().text(json.origen);
            $(".rpureza").empty().text(json.pureza + ' %');
            $(".rgerminacion").empty().text(json.germinacion + ' %');
            $(".rhumedad").empty().text(json.humedad + ' %');
            $(".robservacion").empty().text(json.observacion);
            break;
        case 12:
            //solicitud,semilla,superficie,inspeccion,hoja cosecha,[muestra - resultado laboratorio] y semilla producida
            //solicitud,semilla,superficie,inspeccion y hoja cosecha
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            // inspeccion---------------------------------------------------------------------------------------
            $(".rvegetativa").empty().text(json.vegetativa);
            $("span.robsvegetativa").empty().text(json.obs_vegetativa);
            $(".rfloracion").empty().text(json.floracion);
            $("span.robsfloracion").empty().text(json.obs_floracion);
            $(".rcosecha").empty().text(json.cosecha);
            $("span.robscosecha").empty().text(json.obs_cosecha);
            // hoja cosecha-----------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rfemision").empty().text(json.emision);
            $(".rcatcampo").empty().text(json.catSembrada);
            $(".restimado").empty().text(json.rendEstimado + " (TM / Ha.)");
            $(".rparcela").empty().text(json.aprobada + " (Ha.)");
            $(".rrendcampo").empty().text(json.rendCampo + " (TM.)");
            $(".rcupones").empty().text(json.cupones);
            $(".rplantaa").empty().text(json.plantaa);
            // laboratorio-----------------------------------------------------------------------------------
            if ((json.cultivo !== 'Papa') && (json.cultivo !== 'Plantines')) {
                $(".rnocampo").empty().text(json.nocampo);
                $(".rnoanalisis").empty().text(json.noanalisis);
                $(".rfecharecepcion").empty().text(json.recepcion);
                $(".rfecharesultado").empty().text(json.resultado);
                $(".rorigen").empty().text(json.origen);
                $(".rpureza").empty().text(json.pureza + ' %');
                $(".rgerminacion").empty().text(json.germinacion + ' %');
                $(".rhumedad").empty().text(json.humedad + ' %');
                $(".robservacion").empty().text(json.observacion);
            } else {
                //si es papa o plantines ocultar pestaña laboratorio
                $("a#lbl-reg-laboratorio").css('display', 'none');
            }
            // semilla producida-----------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rgeneracion").empty().text(json.generacion);
            $(".rsemNeta").empty().text(json.semilla_neta);
            $(".rproducir").empty().text(json.catProducir);
            $(".rnoetiquetas").empty().text(json.nro_etiquetas);
            $(".rrango").empty().text(json.rango + ' - ' + json.rango_final);
            break;
        case 13:
            //solicitud,semilla,superficie,inspeccion,hoja cosecha,[muestra - resultado laboratorio],semilla producida y cuenta
            alert('estado ' + estado + ' mostrar datos laboratorio');
            $(".rnosol").empty().text(json.nosolicitud);
            $(".rdepto").empty().text(json.departamento);
            $(".rprov").empty().text(json.provincia);
            $(".rmun").empty().text(json.municipio);
            $(".rcom").empty().text($.funciones.cambiarSignoPorAcento(json.comunidad));
            $(".rfsol").empty().text(json.fsolicitud);
            // semilla---------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rcampanha").empty().text(json.campanha);
            $(".rcultivo").empty().text($.funciones.cambiarSignoPorAcento(json.cultivo));
            $(".rvariedad").empty().text($.funciones.cambiarSignoPorAcento(json.variedad));
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rproducir").empty().text(json.catProducir);
            $(".rcantidad").empty().text(json.cantidad_semilla + " (Kg.)");
            $(".rfsiembra").empty().text(json.fecha_siembra);
            $(".rplantas").empty().text(json.plantas);
            // superficie---------------------------------------------------------------------------------------
            $(".rinscrita").empty().text(json.inscrita + " (Ha.)");
            $(".rrechazada").empty().text(json.rechazada + " (Ha.)");
            $(".rretirada").empty().text(json.retirada + " (Ha.)");
            $(".raprobada").empty().text(json.aprobada + " (Ha.)");
            // inspeccion---------------------------------------------------------------------------------------
            $(".rvegetativa").empty().text(json.vegetativa);
            $("span.robsvegetativa").empty().text(json.obs_vegetativa);
            $(".rfloracion").empty().text(json.floracion);
            $("span.robsfloracion").empty().text(json.obs_floracion);
            $(".rcosecha").empty().text(json.cosecha);
            $("span.robscosecha").empty().text(json.obs_cosecha);
            // hoja cosecha--------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rfemision").empty().text(json.emision);
            $(".rcatcampo").empty().text(json.catSembrada);
            $(".restudio").empty().text(json.rendEstimado);
            $(".rparcela").empty().text(json.aprobada + " (Ha.)");
            $(".rrendcampo").empty().text(json.rendCampo);
            $(".rcupones").empty().text(json.cupones);
            $(".rplantaa").empty().text(json.plantaa);
            // semilla producida--------------------------------------------------------------------------------------------
            $(".rnocampo").empty().text(json.nocampo);
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rgeneracion").empty().text(json.generacion);
            $(".rsemNeta").empty().text(json.semNeta);
            $(".rproducir").empty().text(json.catProducir);
            $(".rnoetiquetas").empty().text(json.noEtiquetas);
            $(".rrango").empty().text(json.rango);
            // cuenta--------------------------------------------------------------------------------------------
            $(".rfemision").empty().text(json.femision);
            $(".rcatcampo").empty().text(json.ccampo);
            $(".restimado").empty().text(json.estimado + "(T.M./Ha.)");
            $(".rparcela").empty().text(json.parcela);
            $(".rrendcampo").empty().text(json.rendcampo + "(T.M./Ha.)");
            $(".rcupones").empty().text(json.cupones);
            $(".rplantaa").empty().text(json.planta);
            $(".rsembrada").empty().text(json.catSembrada);
            $(".rgeneracion").empty().text(json.generacion);
            $(".rsemNeta").empty().text(json.semNeta);
            $(".rproducir").empty().text(json.catProducir);
            $(".rnoetiquetas").empty().text(json.noEtiquetas);
            $(".rrango").empty().text(json.rango);
            $(".rresponsable").empty().text($.funciones.cambiarSignoPorAcento(json.responsable));
            $(".rcampanha").empty().text(json.campanha);
            $(".rcinscripcion").empty().text(json.inscripcion);
            $(".rcinspeccion").empty().text(json.inspeccion);
            $(".rcanalisis").empty().text(json.analisis);
            $(".rcacondicionamiento").empty().text(json.acondicionamiento);
            $(".rcplantines").empty().text(json.plantines);
            $(".rfcuenta").empty().text(json.fecha_cta);
            break;
        }
    },
    /**
     * destino   id del select donde seran cargados los datos
     * model     nombre del controlador
     * opcion    opcion q es solicitada
     */
    cargarSelect : function(destino, model, opcion, opcion2, opcion3, nombre, apellido, cultivo) {
        $('#' + destino).removeAttr('disabled');
        var url = 'control/index.php';
        var datos = {
            mdl : model,
            opt : 'buscar',
            pag : opcion,
            semillera : opcion2,
            opt : opcion3,
            nombre : nombre,
            apellido : apellido,
            cultivo : cultivo
        };
        $.getJSON(url, datos, function(json) {
            //console.log(destino+':: opt > '+opcion);
            if (json.length > 0) {
                $('#' + destino).empty();
                if (opcion == 'campoLab') {
                    $('#' + destino).empty().append('<option value="" selected="selected">-Seleccione numero de campo-</option>');
                } else {
                    $('#' + destino).empty().append('<option value="" selected="selected">[ Selecionar ]</option>');
                }
                if (opcion == 'categoria') {
                    $.each(json, function(index, value) {
                        $('#' + destino).append('<option value="' + index + '">' + value + '</option>');
                    });
                } else {
                    //console.log(json);
                    $.each(json, function(index, value) {
                        if (value != 'no existen datos')
                            $('#' + destino).append('<option value="' + value + '">' + value + '</option>');
                    });
                }
            } else {
                $('#' + destino).empty().append('<option value="">-No existen datos-</option>').attr('disabled', 'disabled');
            }

        });
    },
    /**
     * Utilizado en semilla producida - fiscalizacion
     * destino   id del select donde seran cargados los datos
     * model     nombre del controlador
     * opcion    opcion q es solicitada
     */
    cargarSelectFiscal : function(destino, model, opcion, nombre, apellido, id) {
        $('#' + destino).removeAttr('disabled');
        var url = 'control/index.php';
        var datos = {
            mdl : model,
            pag : opcion,
            nombre : nombre,
            apellido : apellido,
            id : id
        };
        $.getJSON(url, datos, function(json) {
            if (json.length > 0) {
                $('#' + destino).empty();
                if (opcion == 'campoLab') {
                    $('#' + destino).empty().append('<option value="">-Seleccione numero de campo-</option>');
                } else {
                    $('#' + destino).empty().append('<option value="">-Selecione ' + opcion + '-</option>');
                }
                if (opcion == 'categoria') {
                    $.each(json, function(index, value) {
                        $('#' + destino).append('<option value="' + index + '">' + value + '</option>');
                    });
                } else {

                    $.each(json, function(index, value) {
                        if (value != 'no existen datos')
                            $('#' + destino).append('<option value="' + value + '">' + value + '</option>');
                    });
                }
            } else {
                $('#' + destino).empty().append('<option value="">-No existen datos-</option>').attr('disabled', 'disabled');
            }

        });
    },
    datosHojaCosecha : function() {
        var semillera = $(".tbl-semillera").text();
        var productor = $(".tbl-semillerista").text();
        var cultivo = $("td.tbl-cultivo-hoja-cosecha").text();
        var partes = productor.split(" ");
        var nombre = partes[0];
        var nro_parcelas;
        var nombres = partes.length;
        switch (nombres) {
        case 2:
            var apellido = partes[1];
            break;
        case 3:
            var apellido = partes[2];
            break;
        case 4:
            var apellido = partes[2];
            break;
        default:
            var apellido = partes[3];
            break;
        }
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ver',
            pag : 'campos',
            id : $("#iSolicitud").val(),
            semillera : semillera
        }, function(data) {
            $("#campo").empty();
            $("select#campo").append('<option value="" selected="selected">[ Seleccione un n&uacute;mero de campo ]</option>');
            $.each(data.nroCampo, function(index, value) {
                $("#campo").append('<option value="' + value + '">' + value + '</option>');
            });
            $("select#campo").selectmenu();
            loadSuperficieParcela();
        });
        //verifica que el cultivo no sea papa
        if (cultivo == 'papa' || cultivo == 'Papa') {
            $("#nolab").attr('checked', 'checked');
            $("#estado").empty().val(1);
            $("#laboratorio").buttonset("refresh");
        } else {
            $("#silab").attr('checked', 'checked');
            $("#estado").empty().val(0);
            $("#laboratorio").buttonset("refresh");
        }
        //  load superficie parcela dinamicamente segun el # campo elegido
        function loadSuperficieParcela() {
            $("select#campo").selectmenu({
                change : function(event, ui) {
                }
            });
            $('select#campo').on("selectmenuchange", function(event, ui) {
                var semillera = $(".tbl-semillera").text();
                var productor = $(".tbl-semillerista").text();
                var nro_parcelas;
                var partes = productor.split(" ");
                var nombre = partes[0];
                var nombres = partes.length;
                switch (nombres) {
                case 2:
                    var apellido = partes[1];
                    break;
                case 3:
                    var apellido = partes[2];
                    break;
                case 4:
                    var apellido = partes[2];
                    break;
                default:
                    var apellido = partes[3];
                    break;
                }

                var cultivo = $("td.tbl-cultivo-hoja-cosecha").text();
                var nro_campo = $("select#campo").find(':selected').val();

                if (nro_campo != '') {
                    var isolicitud = $("input#iSolicitud").val();
                    var url = 'control/index.php';
                    var datos = {
                        mdl : 'certificacion',
                        opt : 'ver',
                        pag : 'datosCosecha',
                        semillera : semillera,
                        isolicitud : isolicitud,
                        cultivo : cultivo,
                        nro_campo : nro_campo
                    };
                    $.getJSON(url, datos, function(json) {
                        var superficie = json.superficie;
                        var categCampo = json.categCampo;
                        var superficieID = json.isuperficie;
                        var igeneracion = json.igeneracion;
                        var icategoria = json.icategoria;
                        $('#sup_parcela').val(superficie);
                        $('#categ_campo').val(categCampo);
                        $('#igeneracion').val(igeneracion);
                        $('#icategoria').val(icategoria);
                    });
                    $("#datos-cosecha").fadeIn();
                    //$("#rc-from").numeric(".");
                }
            });
        }

    },
    /**
     * carga resultados en un input en el form cuenta
     * input1   superficie
     * input2   categoria campo
     * input3   id superficie
     * semillera
     * nombre
     * apellido
     * cultivo
     * nroCampo
     */
    datosCosecha : function(input1, input2, input3, semillera, nombre, apellido, cultivo, nroCampo) {
        var url = 'includes/operaciones.php';
        var datos = {
            page : 'datosCosecha',
            semillera : semillera,
            nombre : nombre,
            apellido : apellido,
            cultivo : cultivo,
            nro_campo : nroCampo
        };
        $.getJSON(url, datos, function(json) {
            var superficie = json.superficie;
            var categCampo = json.categCampo;
            var superficieID = json.isuperficie;
            $('#' + input1).val(superficie);
            $('#' + input2).val(categCampo);
            $('#' + input3).val(superficieID);
        });
    },

    datosSemillaP : function(input1, input2, campo) {
        var url = 'control/index.php';
        var datos = {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'semillap',
            campo : campo
        };
        $.getJSON(url, datos, function(json) {
            var laboratorio = json.laboratorio;
            var cosecha = json.cosecha;
            var solicitud = json.isolicitud;

            $('input#' + input1).val(solicitud);
            $('input#' + input2).val(cosecha);

        });
    },
    cargarInput : function(in1, in2, in3, in4, in5, in6, in7, in8, in9, opcion, nombre_, apellido_, campanha, cultivo_, variedad_) {
        var url = 'control/index.php?mdl=certificacion';
        if (opcion == 'resumenCta') {

            // esto es para cuenta
            var datos = {
                opt : 'buscar',
                pag : opcion,
                nombre : nombre_,
                apellido : apellido_,
                campania : campanha,
                cultivo : cultivo_,
                variedad : variedad_
            };
            $.getJSON(url, datos, function(json) {
                var cAprobada = json.catAprobada;
                var sAprobada = json.supAprobada;
                var sTotal = json.supTotal;
                var bolsa = json.bolsa;
                var inscripcion = json.inscripcion;
                var icampo = json.inspeccion;
                var analabet = json.anlet;
                var acondicionamiento = json.acondicionamiento;
                var plantines = json.plantines;

                $('#' + in1).val(cAprobada);
                $('#' + in2).val(sAprobada);
                $('#' + in3).val(sTotal);
                $('#' + in4).val(bolsa);
                $('#' + in5).val(inscripcion);
                $('#' + in6).val(icampo);
                $('#' + in7).val(analabet);
                $('#' + in8).val(acondicionamiento);
                $('#' + in9).val(plantines);
                $('#total1').val(bolsa);

                var inscripcion_ = $('#' + in5).val();
                var inspeccion_ = $('#' + in6).val();
                var acondicionamiento_ = $('#' + in8).val();
                var costos = $('#' + in7).val();
                var bolsaEtiqueta = $('#bolsa').val();
                if (!isNaN(bolsaEtiqueta))
                    var total = 1 * costos;
                var sbtotal2 = parseFloat(inscripcion) + parseFloat(icampo) + parseFloat(acondicionamiento) + parseFloat(total);
                //+parseFloat(plantines);
                $('#total2').val(sbtotal2);
                $('#anlet').val(total);
                $('#montoPagado').keyup(function() {
                    var monto = parseFloat($(this).val());
                    var sbtotal = parseFloat($('#total2').val());
                    if (monto <= sbtotal) {
                        var saldo = parseFloat(sbtotal) - monto;
                        $('#saldo').val(saldo);
                    } else {
                        $(this).css("color", "red");
                        var maxVal = $('#total2').length;
                        if ($(this).val().length > maxVal) {
                            $(this).val($(this).val().substr(0, maxVal));
                        }

                    }
                });
                /*var urlA = 'includes/operaciones.php?page=sup_aprobada';
                 var url = '&campania=' + campanha + '&variedad=' + variedad_ + '&cultivo=' + cultivo_;
                 url += '&nombre=' + nombre_ + '&apellido=' + apellido_;
                 url += '&catAprobada=' + cAprobada;
                 $.getJSON(urlA + url, function(data) {
                 $('#superficie').val(data.aprobada);
                 })*/
                urlA = '';
                urlA = 'control/index.php?mdl=operaciones&pag=sup_total';
                urlA += '&campania=' + campanha + '&variedad=' + variedad_ + '&cultivo=' + cultivo_;
                urlA += '&nombre=' + nombre_ + '&apellido=' + apellido_;
                $.getJSON(urlA, function(data) {
                    $('#superficietot').val(data.total);
                });
                return false;
            });
        } else {
            var datos = {
                pag : opcion,
                nroCampo : in2
            };
            $.getJSON(url, datos, function(json) {
                $('#' + in1).val(json);
            });
        }

    },
    /**
     * Agrega * si tiene espacio para ser enviada
     * semillera nombre de la semillera
     */
    prepararSemillera : function(semillera) {
        var palabras = semillera.split(" ");
        semillera = '';
        semillera = palabras.join("*");

        return semillera;
    },
    precioCertificacion : function() {
        cultivo = $('input#cultivo').val();
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'precios',
            cul : cultivo
        }, function(json) {
            html = '';
            $.each(json.precio, function(index, valor) {
                html += '<tr style = \'text-align:center\'>';
                html += '<td>' + json.descripcion[index] + '</td>';
                html += '<td>' + json.precio[index] + '</td>';
                html += '</tr>';
            });
            //agregar solicitudes a dialogo
            $('.tbl-precios-certifica>.table>tbody.buscar').empty().append(html);
        });
        $('#precioCertifica').dialog({// Dialog
            title : 'Precios de Certificación de ' + cultivo,
            dialogClass : 'no-close',
            resizable : false,
            height : 320,
            width : 350,
            buttons : {
                'Aceptar' : function() {
                    $(this).dialog('close');
                }
            }
        });
    },
    precioFiscalizacion : function() {
        cultivo = $('td.tbl-cultivo').text();
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'precios',
            cul : cultivo
        }, function(json) {
            html = '';
            $.each(json.precio, function(index, valor) {
                html += '<tr style = \'text-align:center\'>';
                html += '<td>' + json.descripcion[index] + '</td>';
                html += '<td>' + json.precio[index] + '</td>';
                html += '</tr>';
            });
            //agregar solicitudes a dialogo
            $('.tbl-precios-certifica>.table>tbody.buscar').empty().append(html);
        });
        $('#precioCertifica').dialog({// Dialog
            title : 'Precios de Certificación de ' + cultivo,
            dialogClass : 'no-close',
            resizable : false,
            height : 320,
            width : 350,
            buttons : {
                'Aceptar' : function() {
                    $(this).dialog('close');
                }
            }
        });
    },
    detalleSemilleras : function(id) {
        var productor = $('#productor');
        var solicitud = $('input[name=solicitud]');
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'full_semilleras',
            area : 1
        }, function(json) {
            if (json.semillera.length > 0) {
                $.each(json.semillera, function(index, value) {
                    $("select#" + id).append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $("select#" + id).empty().append('<option value="">No existen semilleras en esta etapa</option>').attr('disabled', 'disabled');
            }
            $("select#semillera").selectmenu("refresh");
        });
    },
    buscarSemilleras : function(id, tipo) {
        var productor = $('#productor');
        var solicitud = $('input[name=solicitud]');
        switch (tipo) {
        case 'gestiones':
            $.getJSON('control/index.php', {
                mdl : 'busqueda',
                opt : 'buscar',
                pag : 'full_semilleras',
                area : id
            }, function(json) {
                if (json.semillera.length > 0) {
                    $('select#semillera').empty().prepend('<option selected="selected" value="">[ Seleccione semillera ]</option>');
                    $.each(json.semillera, function(index, value) {
                        $("select#semillera").append('<option value="' + value + '">' + value + '</option>');
                    });
                } else {
                    $('select#semillera').selectmenu('destroy');
                    $('select#semillera').empty().attr('disabled', 'disabled').prepend('<option selected="selected" value="todas">no hay semilleras</option>');
                    $('select#semillera').selectmenu().selectmenu("menuWidget").addClass("overflow");
                }

            });
            break;
        }
    },
    buscarSemilleristas : function(id, area, semillera) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'semilleristas',
            area : area,
            slr : semillera
        }, function(json) {
            var nombre = '';
            if (json.semilleristas.length > 0) {
                $('select#' + id).removeAttr('disabled');
                $('select#' + id).prepend('<option value="*">Todos</option>');
                $.each(json.semilleristas, function(index, value) {
                    $('select#' + id).append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $('select#' + id).selectmenu('destroy');
                $('select#' + id).empty().attr('disabled', 'disabled').prepend('<option selected="selected" value="">No hay</option>');
                $('select#' + id).selectmenu();
            }
        });
    },
    buscarCultivo : function(id) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'cultivos'
        }, function(json) {
            if (json.cultivos.length > 0) {
                $('#' + id).removeAttr('disabled');
                $('#' + id).prepend('<option value="*">Todos</option>');
                $.each(json.cultivos, function(index, value) {
                    $('select#' + id).append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $("select#" + id).prepend('<option selected="selected" value="">No hay</option>');
                $("select#" + id).selectmenu("refresh");
            }

        });
    },
    buscarGestiones : function(id, area) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'lst_gestiones',
            area : area
        }, function(json) {
            if (json.gestiones.length > 0) {
                $('select#' + id).append('<option value="">[ Seleccionar gestion ]</option>');
                $.each(json.gestiones, function(index, value) {
                    $('select#' + id).append('<option value="' + json.gestiones[index] + '">' + json.gestiones[index] + '</option>');
                });
                $("#options").tabs();
            } else {
                $('select#gestion').empty().append('<option value="">No hay gestiones</option>').attr('disabled', 'disabled');
                $("#options").tabs({
                    disabled : [0, 1, 2, 3]
                });
                $("button#btn-search-gestiones").attr('disabled', 'disabled');
            }
            $("select#gestion").fadeIn();
            $("select#gestion").selectmenu().selectmenu("menuWidget").addClass("overflow-year");
        });
    },
    buscarGestionesMuestras : function(id) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'lst_gestiones_muestras',
        }, function(json) {
            if (json.gestiones.length > 0) {
                $('select#' + id).append('<option value="">[ Seleccionar gestion ]</option>');
                $.each(json.gestiones, function(index, value) {
                    $('select#' + id).append('<option value="' + json.gestiones[index] + '">' + json.gestiones[index] + '</option>');
                });
                $("#options").tabs();
            } else {
                $('select#gestion').empty().append('<option value="">No hay gestiones</option>').attr('disabled', 'disabled');

                $("button#btn-search-gestiones").attr('disabled', 'disabled');
            }
            $("select#gestion").fadeIn();
            $("select#gestion").selectmenu().selectmenu("menuWidget").addClass("overflow-year");
        });
    },
    buscarCampanias : function(id, area) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'lst_campanhas',
            area : area
        }, function(json) {
            if (json.campanhas.length > 0) {
                $.each(json.campanhas, function(index, value) {
                    $('select#' + id).append('<option value="' + json.campanhas[index] + '">' + json.campanhas[index] + '</option>');
                });
                $("#options").tabs();
            } else {
                $("select#campania").empty().attr('disabled', 'disabled').append('<option value="*">No hay campañas</option>').attr('disabled', 'disabled');
                $("#options").tabs('disable');
            }
            $("select#campania").fadeIn();
            $("select#campania").selectmenu().selectmenu("menuWidget").addClass("overflow-year");

        });
    },
    buscar : function() {
        var semillera = $("select#semillera").find(':selected').val();
        var semillerista = $("select#productor").find(':selected').val();
        var cultivo = $("select#cultivo").find(':selected').val();
        console.log(semillera + '+' + semillerista + '+' + cultivo);
        console.log('jquery.funciones linea 349');
        /*$.getJSON('control/index.php', {
         page : 'semillera',
         opt : 'superficie'
         }, function(data) {
         if (data != '') {
         $('#semillera').append('<option value=>----------------------</option>');
         $.each(data, function(index, value) {
         $('#semillera').append('<option value="' + value + '">' + value + '</option>');
         });
         }
         });*/
    },

    cargarNroCampoSuperficieByIsolicitud : function(id) {
        $.getJSON("control/index.php", {
            mdl : 'certificacion',
            opt : 'buscar'
        }, function(json) {

        });
    },
    //cargar cultivo de plantines
    cargarPlantines : function() {
        $("select#cultivo-plantines").selectmenu({
            change : function(event, ui) {
            }
        });
        //carga de variedad de un cultivo
        $("select#cultivo-plantines").on("selectmenuchange", function(event, ui) {
            var cultivo = $(this).find(':selected').val();
            switch (cultivo) {
            case 'add_cultivo':
                $("#agregarCultivo").dialog({// Dialog
                    title : 'Agregar nuevo cultivo',
                    dialogClass : "no-close",
                    resizable : false,
                    buttons : {
                        "Agregar" : function() {
                            var newCultivo = $("input#nuevoCultivo").val();

                            newCultivo = newCultivo.length ? newCultivo : '';
                            if (newCultivo.length > 0) {
                                $("select#cultivo-plantines").append('<option value="' + newCultivo + '" selected>' + newCultivo + '</option>');
                                $("select#cultivo-plantines").selectmenu("refresh");
                                $("input#hiddenCultivo").val(newCultivo);
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            } else {
                                $("span#mensaje").fadeIn().text("NOMBRE DE CULTIVO INVALIDO");
                            }
                        },
                        "Cancelar" : function() {
                            $(this).dialog("close");
                        }
                    }
                });
                break;
            }
        });
    },
    //carga lista de nro de solicitudes segun sistema
    cargarSolicitudes : function(etapa, sistema) {
        $.getJSON('control/index.php', {
            mdl : mdl,
            opt : 'buscar',
            pag : 'ls_solicitudes',
            etapa : etapa,
            sistema : sistema
        }, function(json) {
            //console.log(json.nro_solicitudes.length+'lol');
            if (json.nro_solicitudes.length > 0) {
                $.each(json.nro_solicitudes, function(index, value) {
                    if (value == '') {
                        $("select#nosolicitud").empty().append('<option value="">No existen solicitudes pendientes</option>').attr('disabled', 'disabled');
                    } else {
                        $('select#nosolicitud').append('<option value="' + value + '">' + value + '</option>');
                    }
                });
            } else {
                $("div#advertencia").show();
                $("form").hide();
                $("select#nosolicitud").empty().append('<option value="">No existen solicitudes pendientes</option>').attr('disabled', 'disabled');
                $("select#nosolicitud").selectmenu();
                $(".current-nocampo").hide();
            }
        });
    },
    cargarSolicitudesCertifica : function(etapa, sistema) {
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'buscar',
            pag : 'ls_solicitudes',
            etapa : etapa,
            sistema : sistema
        }, function(json) {
            if (json.nro_solicitudes.length > 0) {
                $.each(json.nro_solicitudes, function(index, value) {
                    if (value == '') {
                        $("select#nosolicitud").empty().append('<option value="">No existen solicitudes pendientes</option>').attr('disabled', 'disabled');
                    } else {
                        $('select#nosolicitud').append('<option value="' + value + '">' + value + '</option>');
                    }
                });
            } else {
                $("div#advertencia").show();
                $("form").hide();
                $("select#nosolicitud").empty().append('<option value="">No existen solicitudes pendientes</option>').attr('disabled', 'disabled');
                $("select#nosolicitud").selectmenu();
                $(".current-nocampo").hide();
            }
        });
    },
    //carga lista de nro de solicitudes segun sistema
    cargarSemillerasCuenta : function(etapa, sistema, modulo) {
        $.getJSON('control/index.php', {
            mdl : modulo,
            opt : 'buscar',
            pag : 'ls_semilleras',
            etapa : etapa,
            sistema : sistema
        }, function(json) {
            if (json.semilleras.length > 0) {
                $.each(json.semilleras, function(index, value) {
                    $('select#semillera').append('<option value="' + json.id[index] + '">' + value + '</option>');
                });

            } else {
                $("div#advertencia").show();
                $("form").hide();
            }
            $("select#semillera").selectmenu();
            $("select#semillera").selectmenu().selectmenu("menuWidget").addClass("overflow");
            $(".current-nocampo").hide();
        });
    },
    // carga las semilleras de acuerdo a la etapa en al que se encuentran
    cargarSemillerasSup : function(tabla, sistema) {
        $('#semillera').empty().append('<option value="">[ Seleccione semillera ]</option>');
        var productor = $('#productor');
        var solicitud = $('input[name=solicitud]');
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            pag : 'semillera',
            opt : 'all',
            std : sistema,
            tbl : tabla
        }, function(json) {
            if (json.length > 0) {
                $.each(json, function(index, value) {
                    if (value == 'E') {
                        $("select#semillera").append("<option value=''>No existen semilleras en esta etapa</option>").attr('disabled', 'disabled');
                    } else {
                        $("select#semillera").append("<option value='" + value + "'>" + value + "</option>");
                    }
                });
            } else {
                $("select#semillera").empty().append('<option value="">No existen semilleras en esta etapa</option>').attr('disabled', 'disabled');
                $(".current-nocampo").hide();
            }
        });
    },
    // carga las semilleras de acuerdo a la etapa en al que se encuentran
    cargarSemillerasFiscal : function(tabla) {
        $("#semillera").empty().append('<option value="">[ Seleccione semillera ]</option>');
        var productor = $('#productor');
        var solicitud = $('input[name=solicitud]');

    },
    //semilleras q tienen como cultivo papa
    cargarSemillerasPapa : function() {
        $.getJSON('control/index.php', {
            page : 'semillera',
            opt : 'superficie'
        }, function(data) {
            if (data != '') {
                $('#semillera').append('<option value=>----------------------</option>');
                $.each(data, function(index, value) {
                    $('#semillera').append('<option value="' + value + '">' + value + '</option>');
                });
            }
        });
    },
    //cargar variedades de un plantin
    cargarVariedadPlantines : function() {
        //carga de variedades de un cultivo
        function cargarVariedadesPlantines(cultivo) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'ls_variedades_plantines',
                cultivo : cultivo
            }, function(json) {
                if (json.total > 0) {
                    $('select#variedad-plantines').empty();
                    $("select#variedad-plantines").append('<option value="">[Seleccione variedad]</option>');
                    //$('select#variedad-plantines').selectmenu("refresh");
                    $.each(json.variedades, function(index, value) {
                        $('select#variedad-plantines').append('<option value="' + $.funciones.cambiarSignoPorAcento(value) + '">' + $.funciones.cambiarSignoPorAcento(value) + '</option>');
                    });
                }
                $("select#variedad-plantines").selectmenu('destroy').append('<option value="add_variedad">Agregar variedad</option>');
                $("select#variedad-plantines").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
            });
        }

        //carga de variedad de un cultivo
        $("select#cultivo-plantines").on("selectmenuchange", function(event, ui) {
            var cultivo = $(this).find(':selected').val();
            if (cultivo != '') {
                //cargamos el valor en un campo oculto en el formulario para ser guardado cuando envie todos los datos
                $("input#hiddenCultivo").val(cultivo);
                $("select#variedad-plantines").selectmenu({
                    disabled : false
                });
                cargarVariedadesPlantines(cultivo);

                $("select#variedad-plantines").selectmenu("destroy").selectmenu();
            } else {
                if (cultivo == 'add_variedad') {
                    alert('lol');
                } else {
                    $("select#variedad-plantines").selectmenu({
                        disabled : true
                    });
                }
            }
        });
    },
    cargarVariedadesDePlantin : function() {
        // mostramos el nombre del cultivo
        $("#nuevaVariedad").focus();
        $("select#variedad-plantines").selectmenu({
            change : function(event, ui) {
            }
        });
        //carga de variedad de un cultivo
        $("select#variedad-plantines").on("selectmenuchange", function(event, ui) {
            $("label#nombreCultivo").text($("input#hiddenCultivo").val());
            var variedad = $(this).find(':selected').val();
            switch (variedad) {
            case 'add_variedad':
                $("input#nombreVariedad").val("");
                $("#agregarVariedad").dialog({// Dialog
                    title : 'Agregar nueva variedad',
                    dialogClass : "no-close",
                    resizable : false,
                    buttons : {
                        "Agregar" : function() {
                            variedad = $("input#nuevaVariedad").val();
                            variedad = variedad.length ? variedad : '';
                            if (variedad.length > 0) {
                                $("select#variedad-plantines").append('<option value="' + variedad + '" selected>' + variedad + '</option>');
                                $("select#variedad-plantines").selectmenu("refresh");
                                $("input#hiddenVariedad").val(variedad);
                                delete variedad;
                                $("div#agregarVariedad").dialog("close");
                                $("div#agregarVariedad").dialog("destroy");
                            } else {
                                $("span#mensaje").fadeIn().text("NOMBRE DE VARIEDAD INVALIDO");
                                $("span#mensaje").empty();
                            }
                        },
                        "Cancelar" : function() {
                            $(this).dialog("close");
                        }
                    }
                });
                break;
            }
        });
    },
    // productores q tienen papa
    cargarProductoresPapa : function(semillera) {
        $.getJSON('control/index.php', {
            page : 'productor_papa',
            semillera : semillera,
            tipo : 1
        }, function(data) {
            if (data.length > 0) {
                $("#productor").empty().append('<option value="">Seleccione productor</option>');
                $.each(data, function(index, value) {
                    $('#productor').append('<option value="' + value + '">' + value + '</option>');
                });
            };
        });
    },

    //cargar campañas
    cargarCampanhas : function(sistema) {
        $.getJSON('control/index.php', {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'campanhas',
            sistema : sistema

        }, function(json) {
            if (json.length > 0 && json.length) {
                $("#campanha").empty().removeAttr('disabled').append('<option value="">[ Seleccione campa&ntilde;a ]</option>');
                $.each(json, function(index, value) {
                    $("#campanha").append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $("#campanha").empty().append('<option value="">No existen campa&ntilde;as</option>');
            }
        });
    },
    //cargar campañas
    cargarCategoriasFiscal : function() {
        $.getJSON('control/index.php', {
            mdl : 'fiscalizacion',
            opt : 'buscar',
            pag : 'ls_categorias'
        }, function(json) {
            $('select#cgeneracion').selectmenu();
            $('select#cgeneracion').selectmenu('destroy').empty();
            $('select#cgeneracion').append('<option value="">[ Seleccionar ]</option>');
            $.each(json.generaciones, function(index, value) {
                $('select#cgeneracion').append('<option value="' + value + '">' + value + '</option>');
            });

            $('select#cgeneracion').selectmenu().selectmenu('menuWidget').addClass('overflow-semilla-cultivo');
        });
    },
    verificarDisponibilidad : function(nombre) {
        var login = $("#" + nombre).val();
        if (login != '' && (login.length > 5)) {
            $(this).removeAttr('style');
            $.getJSON('control/index.php', {
                mdl : 'login',
                pag : 'check',
                usuario : login
            }, function(data) {
                var dato = data.uso;
                if (dato == 'ocupado') {
                    $(".checked").empty().removeClass('libre').append('En uso').addClass('ocupado');
                    $("#login").removeAttr('style').css({
                        "color" : "#FB7535",
                        "font-style" : 'italic'
                    });
                } else {
                    $(".checked").empty().removeClass('ocupado').append('Disponible').addClass('libre');
                    $("#login").removeAttr('style');
                }

            });
        } else {
            $("#login").css({
                "background-color" : "#ff9c9c"
            });
            $(".checked").empty().removeClass('libre').append('nombre de usuario inv&aacute;lido').addClass('ocupado');
        }
    },
    // carga las semilleras de acuerdo a la etapa en al que se encuentran
    cargarNrosCampos : function() {
        $("#campo").append('<option value=""> Cargando...</option>');
        $.getJSON('control/index.php', {
            mdl : 'operaciones',
            pag : 'labCampos'
        }, function(json) {
            if (json.length > 0) {
                $('#campo').empty().append('<option value="">[ Seleccione numero de campo ]</option>');
                $.each(json, function(index, value) {
                    if (value == 'E') {
                        $("#semillera").empty().append('<option value="">No se encontraron resultados</option>').attr('disabled', 'disabled');

                    } else {
                        $('#semillera').append('<option value="' + value + '">' + value + '</option>');
                    }
                });
            } else {
                $("#campo").empty().append('<option value="">No se encontraron resultados</option>').attr('disabled', 'disabled');
                $(".current-nocampo").hide();
            }
        });
    },
    //mostrar filtro para graficos
    mostrarFiltro : function(provincia, municipio, semillera, semillerista) {
        if (provincia != '') {
            $("span#prv").text(provincia);
            $("div#prv").fadeIn();
        } else {
            $("span#prv").text();
            $("div#prv").hide();
        }
        if (municipio != '') {
            $("span#mcp").text(municipio);
            $("div#mcp").fadeIn();
        } else {
            $("span#mcp").text();
            $("div#mcp").hide();
        }
        if (semillera != '%') {
            $("span#slr").text(semillera);
            $("div#slr").fadeIn();
        } else {
            $("span#slr").text();
            $("div#slr").hide();
        }
        if (semillerista != '%') {
            $("span#smr").text(semillerista);
            $("div#smr").fadeIn();
        } else {
            $("span#smr").text();
            $("div#smr").hide();
        }
    },
    //mostrar mensajes al usuario
    mostrarMensaje : function(imagen, texto) {
        var img;
        var styleCSS = {
            'height' : '15%',
            'left' : '41%',
            'padding' : '0 0.2em',
            /*'position' : 'absolute',*/
            'top' : '80%',
            'width' : '15%',
            'z-index' : '999'
        };

        switch (imagen) {
        case 'warning':
            img = "<img src='images/info.png' alt='Correcto' style='position: relative;top: 0.5em;'/>";
            break;
        case 'login':
            img = "<img src='images/loader_4.gif' alt='Correcto' style='position: relative;top: 0.5em;'/>";
            break;
        case 'info':
            img = "<img src='images/info.png' alt='Informacion' style='position: relative;top: 0.5em;'/>";
            break;
        case 'error':
            img = "<img src='images/error.png' alt='Acceso Incorrecto' style='position: relative;top: 0.5em;'/>";
            break;
        case 'ok':
            img = "<img src='images/check.png' alt='Correcto' style='position: relative;top: 0.5em;'/>";
            break;
        case 'wait':
            img = "<img src='images/loader_6.gif' alt='Correcto' style='position: relative;top: 0.5em;'/>";
            break;
        }
        //agregamos la imagen y el mensaje correspondiente al cuadro
        $("#icono").html(img).css(styleCSS);
        $(".mensaje").empty().text(texto);
        //mostramos el cuadro
        $("div#errores").fadeOut(10000);
    },
    //ocultar mensaje despues de 5 seg
    ocultarMensaje : function(tiempo) {

        if (tiempo > 0) {
            $("div#content>div#errores").fadeOut(tiempo);
        } else {
            $("div#content>div#errores").fadeOut(5000);
        }
    },
    //opciones de inspeccion para certificacion
    opcionesInspeccionCertifica : function() {
        $('input:radio[name=semilla]').on('click', function() {
            valor = $('input:radio[name=semilla]:checked').val();
            area = $('input#control').val();
            switch (valor) {
            case '1':
                //cultivo
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'vinspecciones',
                    area : area
                }, function(html) {
                    $('.post').empty().html(html);
                });
                break;
            case '3':
                //plantines
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'inspecciones-plantines',
                    area : area
                }, function(html) {
                    $('.post').empty().html(html);
                });
                break;
            }
        });
    },
    //opciones de semilla para certificacion
    opcionesSemillaCertifica : function() {
        $('input:radio[name=semilla]').on('click', function() {
            valor = $('input:radio[name=semilla]:checked').val();
            area = $('input#control').val();
            switch (valor) {
            case '1':
                //cultivo
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'semilla',
                    area : area
                }, function(html) {
                    $('.post').empty().html(html);
                });
                break;
            case '2':
                //acondicionamiento
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'acondicionamiento',
                    area : area
                }, function(html) {
                    $('.post').empty().html(html);
                });
                break;
            case '3':
                //plantines
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'plantines',
                    area : area
                }, function(html) {
                    $('.post').empty().html(html);
                });
                break;
            }
        });
    },
    //paginar resultados de solicitudes
    bloqueSolicitudes : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    //paginar resultados de solicitudes
    bloqueSemilla : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'semilla',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    bloqueSuperficie : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'superficie',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    bloqueInspeccion : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'vinspecciones',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    bloqueProduccion : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'produccion',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    bloqueCuenta : function() {
        opciones = $('a.pagination');
        nro_pagina = 1;
        nro_pagina_superior = 11;

        opciones.click(function() {
            area = $('input#control').val();
            opciones.removeClass('active');
            page = $(this).text();
            if (page != '<<' && page != '>>') {
                $(this).addClass('active');
                //si es un numero mostar pagina
                $.get('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'cuentas',
                    edt : 1100,
                    area : area,
                    page : page
                }, function(response) {
                    $('.post').empty().html(response);
                });

            } else {
                //ocultar numero y mostrar flecha de regreso
                switch (page) {
                case '>>':
                    //$(this).hide();
                    $('a#anterior').show();
                    $('a[id=' + nro_pagina + ']').hide();
                    $('a[id=' + nro_pagina_superior + ']').show();
                    nro_pagina += 1;
                    nro_pagina_superior += 1;
                    break;
                case '<<':
                    $('a[id=' + (nro_pagina - 1) + ']').show();
                    nro_pagina -= 1;
                    if (nro_pagina == 1) {
                        $('a#anterior').hide();
                    }
                    $('a[id=' + (nro_pagina_superior - 1) + ']').hide();
                    nro_pagina_superior -= 1;
                    break;
                }
            }
        });
        $('div.post>div.div-filter>div#paginacion').hide();
    },
    primerLetra : function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    //cuadro de dialogo de confirmacion
    cuadroDialogo : function(sistema, estado, titulo, ancho, alto) {
        console.log(sistema + '=>' + estado);
        $('form#dialog-' + estado).dialog({// Dialog
            title : titulo,
            dialogClass : "no-close",
            resizable : false,
            height : alto,
            width : ancho,
            buttons : {
                "NO" : function() {
                    $(this).dialog("close");
                    $.funciones.ocultarMensaje();
                },
                "SI" : function() {
                    $(this).dialog("close");
                }
            }
        });
    },
    recargarFomulario : function(etapa, proceso) {
        $.ajax({
            url : 'control/index.php',
            method : 'get',
            data : {
                mdl : proceso,
                opt : 'new',
                pag : etapa
            },
            success : function(responseText) {
                $(".post").empty().append(responseText);
            }
        });
    },
    //recargarVerDatos
    recargarVerDatos : function(proceso, etapa, area, edt) {
        $.ajax({
            url : 'control/index.php',
            method : 'get',
            data : {
                mdl : proceso,
                opt : 'ver',
                pag : etapa,
                area : area,
                edt : edt
            },
            success : function(responseText) {
                $(".post").empty().append(responseText);
                $("div.informar").fadeOut();
            }
        });
    },
    //recargarVerDatos
    recargarVerDatosAdmin : function(proceso, etapa) {
        $.ajax({
            url : 'control/index.php',
            method : 'get',
            data : {
                mdl : proceso,
                pag : etapa
            },
            success : function(responseText) {
                $(".post").empty().append(responseText);
                $("div.informar").fadeOut();
            }
        });
    },
    //recargar Resultados laboratorio
    recargarIngresarResultados : function() {
        $.ajax({
            url : 'control/index.php',
            method : 'get',
            data : {
                mdl : 'laboratorio',
                opt : 'new',
                pag : 'resultado'
            },
            success : function(responseText) {
                $(".post").empty().append(responseText);
                $("div.informar").fadeOut();
            }
        });
    },
    //recargarVerDatos
    recargarVerDatosv2 : function(proceso, etapa, area) {
        $.ajax({
            url : 'control/index.php',
            method : 'get',
            data : {
                mdl : proceso,
                opt : 'ver',
                pag : etapa,
                area : area
            },
            success : function(responseText) {
                $(".post").empty().append(responseText);
                $("div.informar").fadeOut();
            }
        });
    },
    registrarNuevoCosto : function() {
        datos = $("form#actualizacion").serialize();
        $.ajax({
            url : 'control/index.php',
            data : datos,
            type : 'POST',
            beforeSend : validar,
            success : showResponse
        });
        function validar() {
            cultivo = $('input#costo-cultivo');
            inscripcion = $('input#costo-inscripcion');
            inspeccion = $('input#costo-inspeccion');
            analisis = $('input#costo-analisis');
            msg = '';

            if (!cultivo.val() || cultivo.val().length < 3) {
                cultivo.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Cultivo</div>';
            } else {
                cultivo.css({
                    backgroundColor : ""
                });
            }
            if (!inscripcion.val()) {
                inscripcion.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Inscripcion</div>';
            } else {
                inscripcion.css({
                    backgroundColor : ""
                });
            }
            if (!inspeccion.val()) {
                inspeccion.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Inspeccion</div>';
            } else {
                inspeccion.css({
                    backgroundColor : ""
                });
            }
            if (!analisis.val()) {
                analisis.css({
                    backgroundColor : "#f5c9c9"
                });
                msg += '<div>- Analisis</div>';
            } else {
                analisis.css({
                    backgroundColor : ""
                });
            }

            if (msg != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                $.funciones.deshabilitarTexto("enviar-costo-cultivo");
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                setTimeout($.funciones.recargarVerDatosAdmin('administracion', 'costos'), 10000);
                break;
            default:
                $.funciones.mostrarMensaje('error', 'No se pudieron actualizar los datos');
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    },
    //carga las imagenes segun la etapa que se encuentra
    cargarImagen : function(stage, sistem) {
        var res;
        res = stage.split('fis');
        if (res.length == 1) {
            switch (stage) {

            case 'seguimiento':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'seguimiento'
                    },
                    success : function(responseText) {
                        $(".informar").empty().append(responseText);
                    }
                });
                break;
            case 'solicitud':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segSol'
                    },
                    success : function(responseText) {
                        $(".checkSol").empty().append(responseText);
                    }
                });
                break;
            case 'semilla':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segSem'
                    },
                    success : function(responseText) {
                        $(".checkSem").empty().append(responseText);
                    }
                });
                break;
            case 'superficie':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segSup'
                    },
                    success : function(responseText) {
                        $(".checkSup").empty().append(responseText);
                    }
                });
                break;
            case 'inspeccion':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segInsp'
                    },
                    success : function(responseText) {
                        $(".checkInsp").empty().append(responseText);
                    }
                });
                break;
            case 'cosecha':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segCos'
                    },
                    success : function(responseText) {
                        $(".checkCos").empty().append(responseText);
                    }
                });
                break;
            case 'laboratorio':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segLab'
                    },
                    success : function(responseText) {
                        $(".checkLab").empty().append(responseText);
                    }
                });
                break;
            case 'semillaP':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segProd'
                    },
                    success : function(responseText) {
                        $(".checkProd").empty().append(responseText);
                    }
                });
                break;
            case 'cuenta':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segCta'
                    },
                    success : function(responseText) {
                        $(".checkCta").empty().append(responseText);
                    }
                });
                break;
            }
        } else {
            switch (stage) {
            //Fiscalizacion
            case 'seguimientofis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'seguimiento'
                    },
                    success : function(responseText) {
                        $(".informar").empty().append(responseText);
                    }
                });
                break;
            case 'solicitudfis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segSol'
                    },
                    success : function(responseText) {
                        $(".checkSol").empty().append(responseText);
                    }
                });
                break;
            case 'semillafis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segSem'
                    },
                    success : function(responseText) {
                        $(".checkSem").empty().append(responseText);
                    }
                });
                break;
            case 'cosechafis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segCos'
                    },
                    success : function(responseText) {
                        $(".checkCos").empty().append(responseText);
                    }
                });
                break;
            case 'semillaPfis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segProd'
                    },
                    success : function(responseText) {
                        $(".checkProd").empty().append(responseText);
                    }
                });
                break;
            case 'cuentafis':
                $.ajax({
                    url : 'control/index.php',
                    method : 'GET',
                    data : {
                        mdl : sistem,
                        opt : 'new',
                        pag : 'segCta'
                    },
                    success : function(responseText) {
                        $(".checkCta").empty().append(responseText);
                    }
                });
                break;
            }
        }
    },
    //reemplazar acento
    reemplazarAcento : function(cadena) {
        var vector = new Array('á', 'Á', 'é', 'É', 'í', 'Í', 'ó', 'Ó', 'ú', 'Ú', 'Ñ', 'ñ');
        $.each(vector, function(indice, elemento) {
            cadena = cadena.replace(elemento, '_');
        });
        return cadena;
    },
    //remover opciones seleccionadas anteriores
    removeSelectedOption : function() {
        $("li[class^=li-]").removeClass('back-color');
        $("li>a").removeClass('back-text-color');
    },
    //validar fecha guion como separador
    soloNumerosFecha : function(nombreId) {
        $("input#" + nombreId).on("keypress", function(key) {
            if ((key.which < 48 || key.which > 57) && (key.which != 45)/*guion*/ && (key.which != 55)/*signo menos*/ && (key.which != 8)/*retroceso*/)
                return false;
        });
    },
    //validar porcentaje de analisis
    validarAnalisis : function(id, valor_valido, resultado) {

        $.each(id, function(index, valor) {
            //alert(valor+"::"+resultado[index]+'='+valor_valido[index]);
            if (resultado[index] < valor_valido[index]) {
                $("input#" + valor).css({
                    'border' : '1px solid #de3a3a',
                    'font-style' : 'oblique',
                    'color' : '#de3a3a'
                });
            } else {
                $("input#" + valor).css({
                    'border' : '1px solid #949494',
                    'font-style' : '',
                    'color' : '#cecece'
                });
            }
        });

    },
    //agregar estilo y texto
    addTextStyle : function(estilo, texto) {
        //agregar estilo
        $("li." + estilo).addClass('back-color');
        // seleccionar texto
        $("a#" + texto).addClass('back-text-color');
    },

    //dialogo de confirmacion
    showConfirmDialog : function(funcion) {
        //console.log(formulario);
        opcion = bootbox.confirm({
            message : "Los datos son correctos?",
            buttons : {
                confirm : {
                    label : '<i class="fa fa-check"></i> Aceptar',
                    className : 'btn-success'
                },
                cancel : {
                    label : '<i class="fa fa-times"></i> Cancelar',
                    className : 'btn-danger'
                }
            },
            callback : function(result) {
                return result;
            }
        });
        console.log(opcion);
    },
    relogin : function(usuario) {
        $.ajax({
            type : "GET",
            url : 'control/index.php',
            data : {
                mdl : 'login',
                pag : 'relogin',
                login : usuario
            }
        });
    },
    //verificacion contenido input
    animar : function(input, span) {
        var inputValor = $("input#" + input).val();
        if (inputValor.length > '2') {
            $("input#" + input).blur(function() {
                $("span#" + span).fadeIn();
            });
        } else {
            $("span#" + span).hide();
        }
    },
    //habilitar deshabilitar usuario
    habilitarUsuario : function(id, estado) {
        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'habilitar',
            id : id,
            estado : estado
        }, function(json) {
            if (json.msg == 'OK') {
                $.funciones.mostrarMensaje('info', 'Se cambio el estado del usuario');
                $.ajax({
                    type : "GET",
                    url : url,
                    data : {
                        mdl : 'login',
                        pag : 'permisos'
                    },
                    beforeSend : function() {
                        $.funciones.mostrarMensaje('wait', 'Cargando...');
                    },
                    success : function(data) {
                        $.funciones.ocultarMensaje(200);
                        $(".post").empty().append(data);
                    }
                });
            } else {
                $.funciones.mostrarMensaje('alert', json.msg);
            }
        });
    }
};
