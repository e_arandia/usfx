$(document).ready(function() {
	$('#error').hide();
	$('input.input').css({
		backgroundColor : "#FFFFFF"
	});
	$('input.input').focus(function() {
		$(this).css({
			backgroundColor : "#F0F5FF"
		});
	});
	$('input.input').blur(function() {
		$(this).css({
			backgroundColor : "#FFFFFF"
		});
	});

	$("#autForm").submit(function(event) {
		// stop form from submitting normally
		event.preventDefault();

		var $form = $(this);
		var user = $form.find('input[name="usuario"]').val(), pass = $form.find('input[name="clave"]').val();
		var url = $form.attr('action');
		var mensaje = validar(user, pass);
		pass = hex_sha1(pass);
		if (mensaje) {
			$.post(url, {
				mdl : 'login',
				pag : 'ingresar',
				usuario : user,
				clave : pass
			}, function(data) {
				showResponsed(data);
			});
		}
	});
	function validar(user, pass) {
		var message = '';
		if (user.length <= 4 || user == 'usuario') {
			$("span#errorU").show();
			$("input#ulogin").css("border-color","red");
			
			return false;

		} else {
			$("span#errorU").hide();
			$("input#ulogin").css("border-color","");
		}
		if (pass.length < 5) {
			$("span#errorP").show();
			$("input#upass").css("border-color","red");
			return false;
		} else {
			$("span#errorP").hide();
			$("input#upass").css("border-color","");
		}
		return true;
	}

	function showResponsed(responseText) {
	    //alert(responseText);
		switch (responseText) {
			case 'error':
				var msg = 'Usuario no valido<br>usuario y/o contrase&ntilde;a incorrecto';
				$("#dialog-message").attr('title', 'Advertencia').empty().append("<img src='images/error.png' alt='Error' style='position: relative; top: 30%; left:-2%'/>" + msg).fadeIn();
				$("#dialog-message").dialog({
				    dialogClass: "no-close",
					modal : true,
					resizable : false,
					buttons : {
						Aceptar : function() {
							$(this).dialog("close");
						}
					}
				});
				break;
			case 'OK':
				$(".checked").empty();
				$("#ui-icon").addClass(" ui-icon-info").empty().after("Ingresando al sistema. <br>Espere un momento <br>");
				$("#dialog-message").attr('title', 'Sistema de Certificacion y Fiscalizacion de semillas').fadeIn();
				$("#dialog-message").dialog({
				    dialogClass: "no-close",
					modal : true,
					resizable : false
				});
				setTimeout(function() {
				    $("#dialog-message").css('z-index','999');
					$("#dialog-message").dialog("close").empty();
				}, 5000);
				$(location).attr('href', 'index.php');
				break;
			default:
			    var msg = 'Acceso denegado. El nombre de usuario es incorrecto';
                $("#dialog-message").attr('title', 'Advertencia').empty().append("<img src='images/error.png' alt='Error' style='position: relative; top: 30%; left:-2%'/>" + msg).fadeIn();
                $("#dialog-message").dialog({
                    dialogClass: "no-close",
                    modal : true,
                    resizable : false,
                    buttons : {
                        Aceptar : function() {
                            $(this).dialog("close");
                        }
                    }
                });
				$("input").empty();
				break;
		}
	}
});