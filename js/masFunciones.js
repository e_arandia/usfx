jQuery.fn.alternateRowColors = function() {
    $('.tbody .tr:odd', this).removeClass('even').addClass('odd');
    $('.tbody .tr:even', this).removeClass('odd').addClass('even');
    return this;
};
jQuery.fn.calendarioInspeccion = function(objetivo, a, m, d, Amax, Mmax, Dmax) {//fechas de inspecciones
    $("input#" + objetivo).datepicker({
        changeMonth : false,
        changeYear : false,
        buttonText : "calendario",
        dateFormat : "dd-mm-yy",
        minDate : new Date(a, (m - 1), d),
        maxDate : new Date(Amax, (Mmax - 1), Dmax),
        showButtonPanel : true,
        currentText : "Hoy",
        closeText : "Cerrar"
    });
    $.funciones.calendarioEspanhol();
};
jQuery.fn.calendarioSolicitud = function(objetivo) {
    $("#" + objetivo).datepicker({
        changeMonth : false,
        changeYear : false,
        buttonText : "calendario",
        dateFormat : "dd-mm-yy",
        minDate : new Date(new Date().getFullYear() + "-" + (new Date().getMonth() - 1) + "-" + new Date().getDate()),
        maxDate : "0d",
        showButtonPanel : true,
        currentText : "Hoy",
        closeText : "Cerrar"
    });
    $.funciones.calendarioEspanhol();
};
jQuery.fn.calendarioSemilla = function(objetivo, a, m, d) {//fecha de siembra
    $("#" + objetivo).datepicker({
        changeMonth : false,
        changeYear : false,
        buttonText : "calendario",
        dateFormat : "dd-mm-yy",
        minDate : new Date(a, (m - 1), d),
        maxDate : "1w",
        showButtonPanel : true,
        currentText : "Hoy",
        closeText : "Cerrar"
    });
    $.funciones.calendarioEspanhol();
};
jQuery.fn.calendarioLab = function(objetivo) {
    $("#" + objetivo).datepicker({
        changeMonth : true,
        changeYear : true,
        showOn : "button",
        buttonText : "calendario",
        buttonImage : "images/calendar.gif",
        buttonImageOnly : true,
        dateFormat : "dd-mm-yy",
        minDate : "-2M",
        maxDate : "0D",
        showButtonPanel : true,
        currentText : "Hoy",
        closeText : "Cerrar"
    });
    $("#" + objetivo).css('margin-right', '10px');
};
jQuery.fn.calRecibir = function(objetivo) {
    $("#" + objetivo).datepicker({
        changeMonth : true,
        changeYear : false,
        buttonText : "calendario",
        buttonImage : "images/calendar.gif",
        monthNames : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort : ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort : ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
        dayNamesMin : ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dateFormat : "dd-mm-yy",
        minDate : new Date(new Date().getFullYear(), 0, 1),
        maxDate : new Date(new Date().getFullYear(), 11, 31),
        showButtonPanel : true,
        currentText : "Hoy",
        closeText : "Cerrar"
    });
    $("#" + objetivo).css('margin-right', '10px');
};

jQuery.fn.hideButton = function(objetivo) {
    $("input#" + objetivo).hide();
};
jQuery.fn.showButton = function(objetivo) {
    console.log('mostrar boton ' + objetivo);
    $("input#" + objetivo).show();
};
jQuery.fn.buscar = function(formulario, msj, input) {
    $("#" + formulario).ajaxForm({
        url : 'control/busqueda/busqueda.ctrl.php',
        type : 'post',
        data : {
            page : 'buscar'
        },
        beforeSubmit : peticion,
        success : showResponse
    });
    function peticion(formData, jqForm, options) {
        var mensaje = '';
        if (!form.cadena.value) {
            mensaje += '<div>Debe ingresar una palabra o texto para buscar</div>';
        }
        if (formulario == 'busqueda') {
            var opcion_seleccionada = $("input:checked").length;
            if (opcion_seleccionada == 0) {
                mensaje += '<div>no a seleccionado una opcion</div>';
            }
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje + msj);
            $("#errores").fadeIn('slow');
            return false;
        }
        $("#errores").fadeOut('slow');
    }

    function showResponse(responseText, statusText, xhr, $form) {
        if (responseText == 'OK') {
            $('div.parametros').block({
                theme : true,
                title : 'Buscando',
                message : 'Cargando resultado de la busqueda',
                timeout : 20000
            });
            $('div.parametros').empty().prepend('<label>Resultado de b&uacute;squeda</label>');
        }
    }

};
jQuery.fn.gestionesAnteriores = function() {
    $("#semilleras").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'busqueda',
            opt : 'buscar',
            pag : 'gestiones'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.semillera.value) {
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Debe seleccionar una Semillera</div>';
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.campanha.value) {
            $("#campanha").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Debe seleccionar una Campa&ntilde;a</div>';
        } else {
            $("#campanha").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $.funciones.ocultarMensaje();
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                return true;
            } else {
                $("#errores").fadeOut('slow');
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        alert(responseText);
        /*switch (responseText) {
         case 'error':
         $.mostrarMensaje('error', 'Ocurri&oacute; un error al guardar la solicitud');
         $.ocultarMensaje(5000);
         break;

         case 'OK' :
         $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
         $.funciones.ocultarMensaje();
         break;
         }*/
    }

    /*
    $('div.parametros').block({
    theme : true,
    title : 'Buscador',
    message : 'Buscando...',
    timeout : 20000
    });
    $('div.parametros').empty().prepend('<h3>Resultado de b&uacute;squeda</h3>');
    } else
    $('#productor').empty().append('<option value="">No se encuentran semilleristas</option>');
    });*/
    //}
};
//validacion nuevo usuario
jQuery.fn.validar = function() {
    $(".password").after("<div><span class='aviso' >(Debe ser m&iacute;nimo 6 caracteres)</span></div>");
    $("#nivel").val($("#area").find(':selected').val());
    $("#area").change(function() {
        var nivel = $('select[name=area]').find(':selected').val();
        $("#nivel").val(nivel);
    });
    // verificar disponibilidad del nombre de usuario
    $("#login").blur(function() {
        $.funciones.verificarDisponibilidad('login');
    });
    $("input[name=submit1]").click(function() {

        $("#registrar").submit();
    });
};

//registar nuevo usuario
jQuery.fn.registrar = function() {
    $("#registrar").ajaxForm({
        url : 'control/index.php?mdl=usuario&pag=registrar',
        type : 'post',
        resetForm : true,
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.nombre.value || form.nombre.length < 3) {
            mensaje += '<div>- Nombre inv&aacute;lido</div>';
            $("#nombre").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#nombre").css({
                backgroundColor : ""
            });
        }
        if (!form.apellido.value || form.apellido.length < 3) {
            mensaje += '<div>- Apellido inv&aacute;lido</div>';
            $("#apellido").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#apellido").css({
                backgroundColor : ""
            });
        }
        if (!form.area.value) {
            mensaje += '<div>- Area inv&aacute;lido</div>';
            $("#area").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#area").css({
                backgroundColor : ""
            });
        }
        if (!form.login.value || form.login.length < 5) {
            mensaje += '<div>- Nombre de usuario inv&aacute;lido </div>';
            $("#login").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#login").css({
                backgroundColor : ""
            });
        }
        if (!form.pass.value || form.pass.length <= 6) {
            mensaje += '<div >- La contrase&ntilde;a debe tener por lo menos 6 caracteres</div>';
            $("#pass").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#pass").css({
                backgroundColor : ""
            });
        }
        if (!form.pass2.value || form.pass2.length <= 5) {
            mensaje += '<div>- Contrase&ntilde;a diferente</div>';
            $("#pass2").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#pass2").css({
                backgroundColor : ""
            });
        }
        if (form.pass.value != form.pass2.value) {
            mensaje += '<div>- Las contrase&ntilde;as son diferentes</div>';
            $("#pass2").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#pass2").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Se han detectado los siguientes errores: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'error','':
            $(".checked").empty();
            $(".imagen").empty().append("<img src='images/error.png' alt='Acceso Incorrecto'/>");
            $(".message").empty().append('ERROR!! ' + responseText);
            $('#dialog').fadeIn('slow').fadeOut(4000);
            break;

        case 'OK':
            $(".checked").empty();
            $(".imagen").empty().append("<img src='images/check.png' alt='Acceso Incorrecto'/>");
            $(".message").empty().append('Registro Exitoso!!!. <br>Su registro debe ser aprobado por el administrador');
            $('#dialog').fadeIn('slow').fadeOut(7000, function() {
                $("input").empty();
            });
            break;
        }
    }

};
//generar pass
jQuery.fn.generarPass = function(boton, resultado, input1, input2) {
    $("#" + boton).click(function() {
        $.ajax({
            url : 'control/index.php?mdl=usuario&pag=generar_pass',
            type : 'GET',
            success : function(response) {
                $("input#" + resultado).val(response);
                $("input#" + input1).val(response);
                $("input#" + input2).val(response);
            }
        });
    });
};
//lista de todas las solicitudes que existen
jQuery.fn.semillerasByNroSolicitud = function(nro_solicitud, sistema, tabla) {
    $('#nosolicitud').selectmenu();
    $.getJSON('control/index.php', {
        mdl : sistema + 'cion',
        opt : 'buscar',
        pag : "ls_semilleraBySolicitud",
        nro : nro_solicitud,
        sistem : sistema
    }, function(data) {
        if (data.semilleras.length > 0) {
            $("select#semillera").removeAttr('disabled');
            semillera = data.semilleras;
            $('select#semillera').empty().append('<option value="' + semillera + '" selected="selected">' + semillera + '</option>');
            $("select#semillera").selectmenu("refresh");
            ///////////////////////////////////////////////////////////////
            var area = $("#hiddenArea").val();
            $("select#productor").selectmenu("destroy").empty();
            $('select#productor').append('<option value="">[ Seleccione Semillerista ]</option>');
            $("select#productor").selectmenu();
            $("select#productor").productores(sistema, semillera[0], tabla, area);
            $("select#productor").selectmenu().selectmenu("menuWidget").addClass("overflow");
            $("select#productor").selectmenu({
                disabled : false
            });

        } else {
            $('select#semillera').empty().append('<option value="">no existen solicitudes pendientes</option>');
        }
        delete semillera;
    });
};
jQuery.fn.listar_semilleras = function(sistema, etapa) {
    $("select#semillera").selectmenu();
    $.getJSON('control/index.php', {
        mdl : 'fiscalizacion',
        opt : 'ver',
        pag : 'lst_semilleras',
        std : 'fiscal',
        tbl : etapa
    }, function(json) {
        if (json.length > 0) {
            $("select#productor").empty();
            $("select#semillera").empty().append("<option value='' selected='selected'>[Seleccione semillera]</option>").attr('disabled', 'disabled');
            $.each(json, function(index, value) {
                $('select#semillera').append('<option value="' + value + '">' + value + '</option>');
            });
        } else {
            $("#semillera").empty().append('<option value="">No existen semilleras en esta etapa</option>').attr('disabled', 'disabled');
        }
        $("select#semillera").selectmenu({
            disabled : false
        });
    });

};
jQuery.fn.productores = function(mdl, semillera, tabla, area) {
    $('#productor').selectmenu();
    $.getJSON('control/index.php', {
        mdl : mdl + 'cion',
        opt : 'ver',
        pag : "lst_productores",
        tabla : tabla,
        semillera : semillera,
        area : area
    }, function(data) {
        if (data.length > 0) {
            $("#productor").removeAttr('disabled');
            $('#productor').empty().append('<option value="">[ Seleccione semillerista ]</option>');
            $.each(data, function(index, value) {
                $("select#productor").append('<option value="' + value + '">' + value + '</option>');
            });
        } else {
            $("select#productor").empty().append('<option value="">No se encuentran semilleristas</option>');
        }
    });
};

//productores segun la etapa en la q se encuentran
jQuery.fn.productoresFiscal = function(semillera, tabla, opt) {
    $('#productor').selectmenu();
    if (opt == '') {
        $.getJSON('control/index.php', {
            mdl : 'operaciones',
            pag : "productoresFiscal",
            tabla : tabla,
            semillera : semillera
        }, function(data) {
            if (data.length > 0) {
                $("#productor").removeAttr('disabled');
                $('#productor').empty().append('<option value="">[ Seleccione semillerista ]</option>');
                $.each(data, function(index, value) {
                    $('#productor').append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $('#productor').empty().append('<option value="">No se encuentran semilleristas</option>');
            }

        });
    } else {
        $.getJSON('control/index.php', {
            mdl : 'operaciones',
            pag : "productoresFiscal",
            tp : 'fiscal',
            tabla : tabla,
            semillera : semillera
        }, function(data) {
            if (data.length > 0) {
                $("#productor").removeAttr('disabled');
                $('#productor').empty().append('<option value="">[ Seleccione semillerista ]</option>');
                $.each(data, function(index, value) {
                    $('#productor').append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $('#productor').empty().append('<option value="">No se encuentran semilleristas</option>');
            }

        });
    }
};
jQuery.fn.guardarPermisos = function() {
    $("#pactualizar").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'login',
            pag : 'pactualizar'
        },
        success : showResponse
    });
    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'POK' :
            $.funciones.mostrarMensaje('info', 'Permiso de edicion concedido');
            $.funciones.ocultarMensaje();
            $.ajax({
                url : 'control/index.php',
                method : 'get',
                data : {
                    mdl : 'login',
                    pag : 'permisos'
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            break;
        case 'MOK' :
            $.funciones.mostrarMensaje('info', 'permiso de edicion eliminado');
            $.funciones.ocultarMensaje();
            $.ajax({
                url : 'control/index.php',
                method : 'get',
                data : {
                    mdl : 'login',
                    pag : 'permisos'
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje();
            break;
        }
    }

};
jQuery.fn.buscarResultados = function(nro) {
    //  var nro = $("#nrocampo").val();
    console.log(nro + 'masFunciones linea 491');
    $("#buscar").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'laboratorio',
            opt : 'ver',
            pag : 'listapruebas',
            stm : 'fis',
            nro : nro
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.nrocampo.value) {
            $("#nrocampo").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += 'N&uacute;mero de campo';
        } else {
            $("#nrocampo").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('El ' + mensaje + ' es inv&aacute;lido');
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $.funciones.mostrarMensaje('wait', 'cargando Resultados...\nPor favor espere');
            $.funciones.ocultaMensaje();
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'La Solicitud de inscripcion se guardo exitosamente');

            $.funciones.ocultarMensaje();

            $.ajax({
                url : 'control/index.php',
                method : 'get',
                data : {
                    mdl : 'certificacion',
                    pag : 'solicitud',
                    opt : 'new'
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            break;
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            $("input:not(name=sistema)").empty();
            break;
        }
    }

};
jQuery.fn.enviarArchivo = function() {

    $("#importar").ajaxForm({
        url : 'control/index.php',
        type : 'POST',
        data : {
            mdl : 'importar',
            opt : 'new',
            pag : 'datos'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        console.log(form.file_upload.value);
        if (!form.file_upload.value) {
            $("#file_upload").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Seleccione el archivo a subir</div>';
        } else {
            $("#file_upload").css({
                backgroundColor : ""
            });
        }

    }

    function showResponse(responseText, statusText, xhr, $form) {
    }

};
jQuery.fn.enviarSolicitud = function() {
    $("#solicitud").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'certificacion',
            pag : 'solicitud',
            opt : 'guardar'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.nro_solicitud.value) {
            $("#nro_solicitud").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- N&uacute;mero de solicitud</div>';
        } else {
            $("#nro_solicitud").css({
                backgroundColor : ""
            });
        }
        if (!form.provincia.value) {
            $(".lblerror-prov").css({
                color : "#9b000d"
            });
            mensaje += '<div>- Provincia</div>';
        } else {
            $(".lblerror-prov").css({
                color : ""
            });
        }
        if (form.municipio.length == 0) {
            $(".lblerror-mun").css({
                color : "#9b000d"
            });
            mensaje += '<div >- Municipio</div>';
        } else {
            $(".lblerror-mun").css({
                color : ""
            });
        }
        if (!form.comunidad.value) {
            $("#mcomunidad").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div >- Comunidad</div>';
        } else {
            $("#mcomunidad").css({
                backgroundColor : ""
            });
        }
        if (!form.noparcelas.value) {
            $("#noparcelas").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Numero de Parcelas</div>';
        } else {
            $("#noparcelas").css({
                backgroundColor : ""
            });
        }
        if (form.municipio.length == 0) {
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Nombre Semillera</div>';
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.name.value) {
            $("#name").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Nombre Semillerista</div>';
        } else {
            $("#name").css({
                backgroundColor : ""
            });
        }
        if (!form.f_solicitud.value) {
            mensaje += '<div>- Fecha Solicitud</div>';
            $("#f_solicitud").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#f_solicitud").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css({
                'margin-top' : '1%'
            }).fadeIn('slow');
            $(".add_semilla").fadeOut();
            return false;
        } else {
            $.funciones.deshabilitarTexto("enviar-solicitud"); 
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            setTimeout($.funciones.recargarVerDatos('certificacion', 'solicitud', 1, 1100), 10000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
jQuery.fn.enviarSolicitudFiscal = function() {

    $("#solicitud").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'fiscalizacion',
            pag : 'solicitud',
            opt : 'guardar'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.nro_solicitud.value) {
            $("#nro_solicitud").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- N&uacute;mero de solicitud</div>';
        } else {
            $("#nro_solicitud").css({
                backgroundColor : ""
            });
        }
        if (!form.sistema.value) {
            $("#sistema").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Sistema</div>';
        } else {
            $("#sistema").css({
                backgroundColor : ""
            });
        }
        if (!form.departamento.value) {
            $("#departamento").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Departamento</div>';
        } else {
            $("#departamento").css({
                backgroundColor : ""
            });
        }
        if (!form.provincia.value) {// || form.provincia.length < 5) {
            $("#lstprovincia").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Provincia</div>';
        } else {
            $("#lstprovincia").css({
                backgroundColor : ""
            });
        }
        if (form.municipio.length == 0) {
            $("#lstmunicipio").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div >- Municipio</div>';
        } else {
            $("#lstmunicipio").css({
                backgroundColor : ""
            });
        }
        if (!form.comunidad.value) {// || form.municipio.length < 5) {
            $("#mcomunidad").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div >- Comunidad</div>';
        } else {
            $("#mcomunidad").css({
                backgroundColor : ""
            });
        }
        if (!form.semillera.value) {
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Nombre Semillera</div>';
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.name.value) {
            $("#name").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Nombre Semillerista</div>';
        } else {
            $("#name").css({
                backgroundColor : ""
            });
        }
        if (!form.f_solicitud.value) {
            mensaje += '<div>- Fecha Solicitud</div>';
            $("#f_solicitud").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#f_solicitud").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css({
                'margin-top' : '56%'
            }).fadeIn('slow');
            $(".add_semilla").fadeOut();
            return false;
        } else {
            $.funciones.deshabilitarTexto("enviar-solicitud"); 
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'La Solicitud de inscripcion se guardo exitosamente');
            $.funciones.ocultarMensaje();
            setTimeout($.funciones.recargarVerDatos('fiscalizacion', 'solicitud', 2, 1100), 10000);

            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            $("input:not(name=sistema)").empty();
            break;
        }
    }

};
jQuery.fn.enviarSemilla = function() {
    $("#semilla").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'certificacion',
            pag : 'semilla',
            opt : 'guardar'
        },
        beforeSubmit : validarDatos,
        success : showResponse
    });
    function validarDatos(formData, jqForm, options) {
        var mensaje = '';

        var form = jqForm[0];
        if (!form.semillera.value) {
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Semillera</div>';
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.productor.value) {//} || form.name.length < 4) {
            $("#productor").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Semillerista</div>';
        } else {
            $("#productor").css({
                backgroundColor : ""
            });
        }

        if (!form.nrocampo.value) {
            mensaje += '<div>- N&uacute;mero de campo</div>';
            $("#nrocampo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#nrocampo").css({
                backgroundColor : ""
            });
        }
        if (!form.campania.value) {// || form.campania.length < 9) {
            mensaje += '<div>- Campa&ntilde;a</div>';
            $("#campania").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#campania").css({
                backgroundColor : ""
            });
        }
        if (!form.cultivo.value) {
            mensaje += '<div>- Cultivo</div>';
            $("#cultivo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cultivo").css({
                backgroundColor : ""
            });
        }
        /***************/
        if (form.variedad.value) {// || form.variedad.length < 3) {
            mensaje += '<div>- Variedad</div>';
            $("#variedad").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#variedad").css({
                backgroundColor : ""
            });
        }
        if (!form.cat_sembrada.value) {// || form.cat_sembrada.length < 3) {
            mensaje += '<div >- Categoria sembrada</div>';
            $("#cat_sembrada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cat_sembrada").css({
                backgroundColor : ""
            });
        }
        if (!form.cat_producir.value) {// || form.cat_producir.length < 3) {
            mensaje += '<div>- Categoria a producir</div>';
            $("#cat_producir").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cat_producir").css({
                backgroundColor : ""
            });
        }
        if (!form.cant_semilla.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.cant_semilla.value)) {
            mensaje += '<div>- Cantidad Semilla</div>';
            $("#cant_semilla").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cant_semilla").css({
                backgroundColor : ""
            });
        }
        if (!form.plantasha.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.plantasha.value)) {
            mensaje += '<div>- Plantas por Hect&aacute;rea</div>';
            $("#plantasha").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#plantasha").css({
                backgroundColor : ""
            });
        }
        if (!form.f_siembra.value) {
            mensaje += '<div>- Fecha Siembra</div>';
            $("#f_siembra").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#f_siembra").css({
                backgroundColor : ""
            });
        }
        if (!form.cult_anterior.value) {
            mensaje += '<div>- Cultivo Anterior</div>';
            $("#cult_anterior").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cult_anterior").css({
                backgroundColor : ""
            });
        }
        if (!form.superficie.value) {
            mensaje += '<div>- Superficie Parcela</div>';
            $("#superficie").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#superficie").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            $.funciones.deshabilitarTexto("enviar-cosecha");
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos de la semilla a producir fueron guardados');
            $.funciones.ocultarMensaje(5000);
            $("div#errores").css('margin-top', '');
            break;
        default:
            {
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
            }
            break;
        }
    }

};
jQuery.fn.enviarSemillaFiscal = function() {
    
    $("#semilla").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'fiscalizacion',
            pag : 'semilla',
            opt : 'guardar'
        },
        beforeSubmit : validarDatos,
        success : showResponse
    });
    function validarDatos(formData, jqForm, options) {
        var mensaje = '';

        var form = jqForm[0];
        if (!form.cultivo.value) {
            mensaje += '<div>- Cultivo</div>';
            $("#lbl-cultivo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#lbl-cultivo").css({
                backgroundColor : ""
            });
        }
        if (form.variedad.value) {// || form.variedad.length < 3) {
            mensaje += '<div>- Variedad</div>';
            $("#lbl-variedad").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#lbl-variedad").css({
                backgroundColor : ""
            });
        }
        if (!form.cgeneracion.value) {
            mensaje += '<div>- Categoria a producir</div>';
            $("#lbl-cat").css({
                color : "#f5c9c9"
            });
        } else {
            $("#lbl-cat").css({
                color : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            $.funciones.deshabilitarTexto("enviar-semilla-fiscal");
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            setTimeout($.funciones.recargarVerDatos('fiscalizacion', 'semilla', 2, 1100), 10000);
            break;
        default:
            {
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
            }
            break;
        }
    }
};
jQuery.fn.fiscalizarSemilla = function() {
    $("#semilla").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'fiscalizacion',
            pag : 'semilla',
            opt : 'guardar'
        },
        beforeSubmit : validarDatos,
        success : showResponse
    });
    function validarDatos(formData, jqForm, options) {
        var mensaje = '';

        var form = jqForm[0];
        if (!form.semillera.value) {//} || form.name.length < 4) {
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Semillera</div>';
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.productor.value) {//} || form.name.length < 4) {
            $("#productor").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Semillerista</div>';
        } else {
            $("#productor").css({
                backgroundColor : ""
            });
        }

        if (!form.cultivo.value) {
            mensaje += '<div>- Cultivo</div>';
            $("#cultivo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cultivo").css({
                backgroundColor : ""
            });
        }
        if (!form.variedad.value) {// || form.variedad.length < 3) {
            mensaje += '<div>- Variedad</div>';
            $("#variedad").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#variedad").css({
                backgroundColor : ""
            });
        }

        if (!form.lotenro.value) {// || form.cat_producir.length < 3) {
            mensaje += '<div>- Lote Nro.</div>';
            $("#lotenro").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#lotenro").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...\nPor favor espere');
                return true;
            } else {
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje();

            break;
        default:
            {
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
            }
            break;
        }
    }

};
jQuery.fn.actualizarSemilla = function() {
    $("#semilla").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'certificacion',
            opt : 'actualizar',
            pag : 'semilla'
        },
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';

        var form = jqForm[0];

        if (!form.nrocampo.value) {
            mensaje += '<div>- N&uacute;mero de campo</div>';
        }
        if (!form.campania.value) {// || form.campania.length < 9) {
            mensaje += '<div>- Campa&ntilde;a</div>';
        }
        if (!form.cultivo.value) {
            mensaje += '<div>- Cultivo</div>';
        }
        if (!form.variedad.value) {//|| form.variedad.length < 3) {
            mensaje += '<div>- Variedad</div>';
        }
        if (!form.cat_sembrada.value) {//|| form.cat_sembrada.length < 3) {
            mensaje += '<div >- Categoria sembrada</div>';
        }
        if (!form.cat_producir.value) {//|| form.cat_producir.length < 3) {
            mensaje += '<div>- Categoria a producir</div>';
        }
        if (!form.cant_semilla.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.cant_semilla.value)) {
            mensaje += '<div>- Cantidad Semilla</div>';
        }
        if (!form.f_siembra.value) {
            mensaje += '<div>- Fecha Siembra</div>';
        }
        if (!form.superficie.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.cant_semilla.value)) {
            mensaje += '<div>- Fecha Solicitud</div>';
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Esta seguro?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('info', 'Actualizando datos...');
                return true;
            } else {
                return false;
            }
        }

    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'error', '':
            $.funciones.mostrarMensaje('error', 'No se actualizaron los datos');
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron actualizados');
            $.funciones.ocultarMensaje(5000);
            var stage = $("#stage").val();
            var txt = $("h2.title").text();
            setTimeout($.funciones.cuadroDialogo(stage, txt), 3000);
            break;
        }
    }

};
jQuery.fn.enviarInscrita = function() {
    $("#FormInscrita").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'superficie',
            tipo : 'inscrita'
        },
        type : 'POST',
        beforeSubmit : validarSuperficieInscrita,
        success : showResponse
    });
    /*Validacion de superficie inscrita*/
    function validarSuperficieInscrita(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        if (form.inscrita.value == 0.0) {
            mensaje += '<div>- Superficie inscrita</div>';
            $("#inscrita").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#inscrita").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: ' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('La superficie es correcta?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...\nPor favor espere');
                return true;
            } else {
                return false;
            }
        }
    };

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', 'Ocurrio un error al guardar la superficie');
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje(5000);

            break;
        }
    };

};
jQuery.fn.enviarRechazada = function() {
    $("#FormRechazada").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'superficie',
            tipo : 'rechazada'
        },
        type : 'POST',
        beforeSubmit : validarSuperficieRechazada,
        success : showResponse
    });
    function validarSuperficieRechazada(formData, jqForm, options) {

        var mensaje = '';
        var form = jqForm[0];
        var diferencia = parseFloat(form.rechazada.value) - parseFloat(form.inscrita.value);
        if (form.rechazada.value == 0.0) {
            mensaje = '<div>La superficie debe ser mayor a 0.0 </div>';
            $("#rechazada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#rechazada").css({
                backgroundColor : ""
            });
        }
        if (diferencia > 0.0) {
            mensaje = '<div>La superficie rechazada es mayor a la superficie inscrita (' + form.inscrita.value + ')</div>';
            $("#rechazada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#rechazada").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('La superficie es correcta?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...\nPor favor espere');
                return true;
            } else {
                return false;
            }
        }
    };

    function showResponse(responseText, statusText, xhr, $form) {
        var msg = '<img src="images/error.png" />';
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', responseText);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje(5000);
            break;
        }
    };

};
jQuery.fn.enviarRetirada = function() {
    $("#FormRetirada").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'superficie',
            tipo : 'retirada'
        },
        type : 'POST',
        beforeSubmit : validarSuperficieRetirada,
        success : showResponse
    });
    function validarSuperficieRetirada(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        var diferencia = parseFloat(form.inscrita.value) - parseFloat(form.rechazada.value);
        if (form.retirada.value == 0.0) {
            mensaje = '<div>La superficie debe ser mayor a 0.0</div>';
            $("#retirada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#retirada").css({
                backgroundColor : ""
            });
        }
        if (form.retirada.value == '') {
            mensaje = '<div>Debe ingresar la superficie retirada</div>';
            $("#retirada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#retirada").css({
                backgroundColor : ""
            });
        }
        if (form.retirada.value > diferencia) {
            mensaje = '<div>La superficie retirada es mayor a la superficie inscrita (' + form.inscrita.value + ')</div>';
            $("#retirada").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#retirada").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('La superficie es correcta?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...\nPor favor espere');
                return true;
            } else {
                return false;
            }
        }
    };

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje(5000);
            break;
        }
    };

};
jQuery.fn.enviarAprobada = function() {
    $("#FormAprobada").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'superficie',
            tipo : 'aprobada'
        },
        type : 'POST',
        beforeSubmit : validarSuperficieAprobada,
        success : showResponse
    });
    function validarSuperficieAprobada(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        var aprobada = $('input#aprobada');
        var inscrita = $('inscrita#inscrita');  
           
        if (aprobada.val() < 0.0) {
            mensaje += '<div>La superficie aprobada debe ser mayor a 0.0</div>';
            aprobada.css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            aprobada.css({
                backgroundColor : ""
            });
        }
        if (aprobada.val().length == '') {
            mensaje += '<div>Debe ingresar la superficie aprobada</div>';
            aprobada.css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            aprobada.css({
                backgroundColor : ""
            });
        }
        if (aprobada.val() > 0 && (aprobada.val() > inscrita.val())) {
            mensaje += '<div>La superficie aprobada es mayor a la superficie inscrita</div>';
            aprobada.css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $aprobada.css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $.funciones.deshabilitarTexto("enviar-superficie");
            $("#errores").fadeOut('slow');
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        var msg = '<img src="images/error.png" />';
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', 'Ocurrio un error al guardar la superficie Aprobada');
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            setTimeout($.funciones.recargarVerDatos('certificacion', 'superficie', 1, 1100), 10000);
            break;
        }
    };
};

jQuery.fn.etapa1 = function() {
    $("#seguimiento").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'inspeccion',
            tipo : 1
        },
        type : 'post',
        clearForm : true,
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        if (!form.evegetativa.value) {
            $("input[name=evegetativa]").css({
                backgroundColor : "#f5c9c9"
            });
            $("input[name=evegetativa]").focus();
            mensaje = 'Debe introducir la fecha de inspeccion';
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje);
            $("#errores").fadeIn('slow');
            return false;
        }
        $.funciones.mostrarMensaje('ok', 'Guardando...\nPor favor espere');
        return true;
    };
    function showResponse(responseText, statusText, xhr, $form) {
        var msg = '<img src="images/error.png" />';
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', 'No existen datos');
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.cargarSemilleras('inspeccion');
            $.funciones.ocultarMensaje(5000);
            $("#productor").empty().attr('disabled', 'disabled');
            $("label.campo").empty();
            break;
        }
    };
};
jQuery.fn.enviarHojaCosecha = function() {
    $("#cosecha").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'cosecha'
        },
        resetForm : true,
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.campo.value) {
            mensaje += '<div>- N&uacute;mero de campo</div>';
            $("#campo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#campo").css({
                backgroundColor : ""
            });
        }
        if (!form.estimado.value) {
            mensaje += '<div>- Rendimiento estimado</div>';
            $("#rendimientoE").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#rendimientoE").css({
                backgroundColor : ""
            });
        }
        if (!form.rend_campo.value) {
            mensaje += '<div>- Rendimiento campo</div>';
            $("#rend_campo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#rend_campo").css({
                backgroundColor : ""
            });
        }
        if (!form.plantaa.value) {
            mensaje += '<div>- Planta Acondicionadora</div>';
            $("#plantaa").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#plantaa").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css('margin-top', '3em').fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK':
            setTimeout($.funciones.recargarVerDatos('certificacion', 'cosecha', 1, 1100), 10000);
            break;
        case 'error':
            break;
        }
    }

};
jQuery.fn.hojaCosechaFiscal = function() {
    $("#cosecha").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'fiscalizacion',
            opt : 'guardar',
            pag : 'cosecha'
        },
        resetForm : true,
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.plantaa.value) {
            mensaje += '<div>- Planta Acondicionadora</div>';
            $("#plantaa").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#plantaa").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css('margin-top', '3em').fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Esta seguro de los datos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');

            $.funciones.ocultarMensaje(5000);
            var stage = $("#stage").val();
            var txt = $("h2.title").text();
            setTimeout($.funciones.cuadroDialogo(stage, txt), 3000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
jQuery.fn.enviarLaboratorio = function() {
    $("#muestraForm").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'laboratorio',
            opt : 'guardar',
            pag : 'prueba'
        },
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.analisis.value) {
            mensaje += '<div>- N&uacute;mero de analisis</div>';
            $("#analisis").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#analisis").css({
                backgroundColor : ""
            });
        }
        if (!form.f_recepcion.value) {
            mensaje += '<div>- Fecha de recepci&oacute;n</div>';
            $("#f_recepcion").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#f_recepcion").css({
                backgroundColor : ""
            });
        }
        if (!form.nrocampo.value) {
            mensaje += '<div>- Numero de campo</div>';
            $("#campo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#campo").css({
                backgroundColor : ""
            });
        }
        if (!form.tipo.value) {
            mensaje += '<div>- Tipo de semilla</div>';
            $("#tipo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#tipo").css({
                backgroundColor : ""
            });
        }
        if (!form.bolsa.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.bolsa.value)) {
            mensaje += '<div>- N&uacute;mero de bolsas</div>';
            $("#bolsa").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#bolsa").css({
                backgroundColor : ""
            });
        }
        if (!form.lote.value || form.lote.length < 7) {
            mensaje += '<div >- Lote (ej. XXX-YYY)</div>';
            $("#lote").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#lote").css({
                backgroundColor : ""
            });
        }
        if (!form.kgbolsa.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.kgbolsa.value)) {
            mensaje += '<div>- Peso de las bolsas</div>';
            $("#kgbolsa").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#kgbolsa").css({
                backgroundColor : ""
            });
        }
        if (!form.total.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.total.value)) {
            mensaje += '<div>- Total (peso total de las bolsas)</div>';
            $("#total").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#total").css({
                backgroundColor : ""
            });
        }
        if (!form.calibre.value) {
            mensaje += '<div>- Calibre</div>';
            $("#calibre").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#calibre").css({
                backgroundColor : ""
            });
        }
        if (!form.semillakg.value) {
            mensaje += '<div>- Cantidad de semillas por kilo</div>';
            $("#semillakg").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#semillakg").css({
                backgroundColor : ""
            });
        }
        if (!form.pureza.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.pureza.value)) {
            mensaje += '<div>- % Pureza</div>';
            $("#pureza").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#pureza").css({
                backgroundColor : ""
            });
        }
        if (!form.germinacion.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.germinacion.value)) {
            mensaje += '<div>- % Germinaci&oacute;n</div>';
            $("#germinacion").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#germinacion").css({
                backgroundColor : ""
            });
        }
        if (!form.humedad.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.humedad.value)) {
            mensaje += '<div>- % Humedad</div>';
            $("#humedad").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#humedad").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'error':
            $.funciones.mostrarMensaje('error', 'Error al guardar');
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
jQuery.fn.enviarMuestra = function() {
    $("#muestraForm").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'laboratorio',
            opt : 'guardar',
            pag : 'muestra'
        },
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.recepcion.value) {
            mensaje += '<div>- Fecha de recepcion</div>';
            $("#f_recepcion").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#f_recepcion").css({
                backgroundColor : ""
            });
        }
        if (!form.nrocampo.value) {
            mensaje += '<div>- N&uacute;mero de campo</div>';
            $("#nrocampo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#nrocampo").css({
                backgroundColor : ""
            });
        }
        if (!form.cultivo.value) {
            mensaje += '<div>- Cultivo</div>';
            $("#cultivo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cultivo").css({
                backgroundColor : ""
            });
        }
        if (!form.variedad.value) {
            mensaje += '<div>- Variedad</div>';
            $("input#variedad").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("input#variedad").css({
                backgroundColor : ""
            });
        }
        if (form.origen.value != '') {
            mensaje += '<div>- Origen</div>';
            $("input#origen").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("input#origen").css({
                backgroundColor : ""
            });
        }
        if (form.pureza.value != '' || form.humedad.value != '' || form.germinacion.value != '') {
            mensaje += '<div>- Tipo de Analisis</div>';
        } 

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Debe seleccionar un valor en los siguientes campos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            setTimeout($.funciones.recargarVerDatos('certificacion', 'muestras', 1, 1100), 10000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }
};
jQuery.fn.enviarMuestraFiscal = function(opcion) {

    if (opcion == 'importada') {
        $("#muestra").ajaxForm({
            url : 'control/index.php',
            type : 'post',
            data : {
                mdl : 'laboratorio',
                opt : 'guardar',
                pag : 'importada'
            },
            beforeSubmit : verificar,
            success : showResponse
        });
        function verificar(formData, jqForm, options) {
            var mensaje = '';
            var form = jqForm[0];

            if (!form.recepcion.value) {
                mensaje += '<div>- Fecha de recepcion</div>';
                $("#f_recepcion").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#f_recepcion").css({
                    backgroundColor : ""
                });
            }
            if (!form.especieImp.value) {
                mensaje += '<div>- Especie</div>';
                $("#especieImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#especieImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.variedadImp.value) {
                mensaje += '<div>- Variedad</div>';
                $("#variedadImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#variedadImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.categoriaImp.value) {
                mensaje += '<div>- Categoria</div>';
                $("#categoriaImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#categoriaImp").css({
                    backgroundColor : ""
                });
            }

            if (!form.origenImp.value) {
                mensaje += '<div>- Lugar de Origen</div>';
                $("#origenImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#origenImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.cantidadImp.value) {
                mensaje += '<div>- Cantidad</div>';
                $("#cantidadImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#cantidadImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.certificadoImp.value) {
                mensaje += '<div>- Certificado Fitosanitario</div>';
                $("#certificadoImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#certificadoImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.aduanaingresoImp.value) {
                mensaje += '<div>- Aduana de Ingreso</div>';
                $("#aduanaingresoImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#aduanaingresoImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.dsemillaImp.value) {
                mensaje += '<div>- Destino semilla</div>';
                $("#dsemillaImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#dsemillaImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.adistribucionImp.value) {
                mensaje += '<div>- Area de distribucion</div>';
                $("#adistribucionImp").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#adistribucionImp").css({
                    backgroundColor : ""
                });
            }
            if (!form.cultivo.value) {
                mensaje += '<div>- Cultivo</div>';
                $("#cultivo").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#cultivo").css({
                    backgroundColor : ""
                });
            }

            if (mensaje != '') {
                $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
                $("#errores").fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                    $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                    return true;
                } else {
                    return false;
                }
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'OK' :
                $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
                $.funciones.ocultarMensaje(5000);
                break;
            default:
                $.funciones.mostrarMensaje('error', responseText);
                $.funciones.ocultarMensaje(5000);
                break;
            }
        }

    } else {
        $("#muestra").ajaxForm({
            url : 'control/index.php',
            type : 'post',
            data : {
                mdl : 'laboratorio',
                opt : 'guardar',
                pag : 'muestra'
            },
            beforeSubmit : function(formData, jqForm, options) {
                var mensaje = '';
                var form = jqForm[0];

                if (!form.recepcion.value) {
                    mensaje += '<div>- Fecha de recepcion</div>';
                    $("#f_recepcion").css({
                        backgroundColor : "#f5c9c9"
                    });
                } else {
                    $("#f_recepcion").css({
                        backgroundColor : ""
                    });
                }
                if (!form.variedad.value) {
                    mensaje += '<div>- Variedad</div>';
                    $("#variedad").css({
                        backgroundColor : "#f5c9c9"
                    });
                } else {
                    $("#variedad").css({
                        backgroundColor : ""
                    });
                }
                if (!form.cultivo.value) {
                    mensaje += '<div>- Cultivo</div>';
                    $("#cultivo").css({
                        backgroundColor : "#f5c9c9"
                    });
                } else {
                    $("#cultivo").css({
                        backgroundColor : ""
                    });
                }
                if (!form.origen.value) {
                    mensaje += '<div>- Lugar de Origen</div>';
                    $("#origen").css({
                        backgroundColor : "#f5c9c9"
                    });
                } else {
                    $("#origen").css({
                        backgroundColor : ""
                    });
                }

                if (mensaje != '') {
                    $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
                    $("#errores").fadeIn('slow');
                    return false;
                } else {
                    $("#errores").fadeOut('slow');
                    if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                        $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            success : function(responseText, statusText, xhr, $form) {
                switch (responseText) {
                case 'OK' :
                    $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
                    $.funciones.ocultarMensaje(5000);
                    $.ajax({
                        url : 'control/index.php',
                        method : 'GET',
                        data : {
                            mdl : 'fiscalizacion',
                            opt : 'new',
                            pag : 'muestra',
                            nivel : 2
                        },
                        success : function(responseText) {
                            $(".post").empty().append(responseText);
                        }
                    });
                    break;
                default:
                    $.funciones.mostrarMensaje('error', responseText);
                    $.funciones.ocultarMensaje(5000);
                    break;
                }
            }
        });
    }
};
jQuery.fn.buscarCampo = function() {
    $("#buscar").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'campo'
        },
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.nrocampo.value) {
            mensaje += '<div>- Debe ingresar el N&uacute;mero de campo que desea buscar</div>';
            $("#nrocampo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#nrocampo").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos no son v&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            $.funciones.mostrarMensaje('ok', 'Buscando...Por favor espere');
            return true;

        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case '1':
            $("#errores div p .mensaje").empty().append('Los resultados estan disponibles');
            $("#errores").fadeIn('slow');
            break;
        case '2' :
            $("#errores div p .mensaje").empty().append('El numero de campo elegido esta a la espera de los resultados');
            $("#errores").fadeIn('slow');
            break;
        default:
            $("#errores div p .mensaje").empty().append('no se encuentra en laboratorio');
            $("#errores").fadeIn('slow');
            break;
        }
    }

};
jQuery.fn.buscarMuestra = function() {
    if (showRequest() == 't') {
        var campo = $("#nrocampo").val();
        $.getJSON('control/index.php', {
            mdl : 'laboratorio',
            opt : 'buscar',
            pag : 'prueba',
            nrocampo : campo
        }, function(data) {
            if (data.res == 'OK') {
                $("#fecha").attr('value', data.fecha);
                $("#campo").attr('value', data.campo);
                $("#variedad").attr('value', data.variedad);
                $("#origen").attr('value', data.origen);
                $("#cultivo").attr('value', data.cultivo);
                $(".ver").show();
            } else {
                $.funciones.mostrarMensaje('error', 'Datos de la muestra no existe');
                $.funciones.ocultarMensaje(5000);
            }

        });
    } else {
        $("span#icono").addClass('ui-icon').addClass('ui-icon-info');
        $("span.mensaje").empty().append('Numero campo invalido');
        $("#errores").fadeIn('slow');
    }
    function showRequest() {
        var mensaje = '';
        var campo = $("#nrocampo").val();
        if (campo == '') {
            $("#nrocampo").css({
                backgroundColor : "#f5c9c9"
            });
            return 'f';
        } else {
            $("#nrocampo").css({
                backgroundColor : ""
            });

        }

        if (mensaje != '') {
            return 'f';
        } else {
            $("#errores").fadeOut('slow');
        }
    }

};
jQuery.fn.enviarResultado = function() {
    $("#resultado").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'laboratorio',
            pag : 'resultados',
            opt : 'guardar'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.bolsa.value){
                $('input#bolsa').css({
                  backgroundColor: '#f5c9c9'
                });
                mensaje += '<div>- Cantidad de bolsas</div>';
            }else{
                $('input#bolsa').css({
                  backgroundColor: ''
                });
            }
            
            if (!form.kgbolsa.value){
                $('input#kgbolsa').css({
                  backgroundColor: '#f5c9c9'
                });
                mensaje += '<div>- Kg./bolsa</div>';
            }else{
                $('input#kgbolsa').css({
                  backgroundColor: ''
                });
            }
            
            if (!form.semillakg.value){
                $('input#semillakg').css({
                  backgroundColor: '#f5c9c9'
                });
                mensaje += '<div>- N&uacute;mero de semillas por kilo</div>';
            }else{
                $('input#semillakg').css({
                  backgroundColor: ''
                });
            }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css({
                'margin-top' : '1%'
            }).fadeIn('slow');
            return false;
        } else {
            return true;
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            setTimeout($.funciones.recargarIngresarResultados(), 10000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
jQuery.fn.enviarSemillaProducida = function() {
    $("#semillaprod").ajaxForm({
        url : 'control/index.php',
        type : 'POST',
        data : {
            mdl : 'certificacion',
            opt : 'guardar',
            pag : 'semillaprod'
        },
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.semillera.value) {
            mensaje += '<div>- Semillera</div>';
            $("#semillera").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#semillera").css({
                backgroundColor : ""
            });
        }
        if (!form.productor.value) {
            mensaje += '<div>- Semillerista</div>';
            $("#productor").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#productor").css({
                backgroundColor : ""
            });
        }
        if (!form.campo.value) {
            mensaje += '<div>- N&uacute;nero de campo</div>';
            $("#campo").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#campo").css({
                backgroundColor : ""
            });
        }
        if (!form.categoria.value) {
            mensaje += '<div>- Categor&iacute;a</div>';
            $("#categoria").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#categoria").css({
                backgroundColor : ""
            });
        }
        if (!form.etiqueta.value) {
            mensaje += '<div>- N&uacute;mero Etiqueta</div>';
            $("#etiqueta").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#etiqueta").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css('margin-top', '3em').fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Semilla producida guardada');

            $.funciones.ocultarMensaje(5000);
            setTimeout($.funciones.cuadroDialogo(stage, txt), 3000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
jQuery.fn.enviarSemillaProducidaFiscal = function() {
    $("#semillaP").ajaxForm({
        url : 'control/index.php',
        type : 'POST',
        data : {
            mdl : 'fiscalizacion',
            opt : 'guardar',
            pag : 'semillaprod'
        },
        beforeSubmit : verificar,
        success : respuesta
    });
    function verificar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        if (!form.semilla_neta.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.semilla_neta.value)) {
            mensaje += '<div>- Semilla neta</div>';
            $("#semilla_neta").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#semilla_neta").css({
                backgroundColor : ""
            });
        }
        if (!form.categ_obtenida.value) {
            mensaje += '<div>- Categor&iacute;a obtenida</div>';
            $("#categ_obtenida").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#categ_obtenida").css({
                backgroundColor : ""
            });
        }
        if (!form.etiqueta.value) {
            mensaje += '<div>- N&uacute;mero Etiqueta</div>';
            $("#etiqueta").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#etiqueta").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").css('margin-top', '3em').fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Guardando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    }

    function respuesta(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
            $.funciones.ocultarMensaje(5000);
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};

jQuery.fn.enviarCuenta = function() {
    datos = $('form#cuenta').serialize();
    //console.log(datos);
    $.ajax({
        url : 'control/index.php',
        data : datos,
        type : 'POST',
        success : showResponse
    });
    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK':
            setTimeout($.funciones.recargarVerDatos('certificacion', 'produccion', 1, 1100), 10000);
            break;
        }
    }

};
jQuery.fn.actualizarInfo = function() {
    $("#updInfo").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'usuario',
            pag : 'actualizar'
        },
        type : 'POST',
        beforeSubmit : validarNombre,
        success : showResponse
    });
    /*Validacion de nombre y apellido*/
    function validarNombre(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        var nombreLength = form.nombre.value;
        var apellidoLength = form.apellido.value;
        if (!nombreLength || nombreLength.length <= 4) {
            mensaje += '<div>- Debe escribir un nombre</div>';
            $("#nombre").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#nombre").css({
                backgroundColor : ""
            });
        }
        if (!apellidoLength || apellido.Length <= 4) {
            mensaje += '<div>- Debe escribir un apellido</div>';
            $("#apellido").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#apellido").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: ' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Desea modificar nombre y apellido?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('ok', 'Actualizando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    };

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'Error':
            $.funciones.mostrarMensaje('error', 'No se pudo actualizar el nombre y apellido');
            $.funciones.ocultarMensaje(2000);
            break;
        case 'OK':
            $.funciones.mostrarMensaje('ok', 'Actualizacion Exitosa!!!');
            $.funciones.ocultarMensaje(2000);
            $.ajax({
                url : 'control/index.php',
                method : 'get',
                data : {
                    mdl : 'login',
                    pag : 'ver_datos',
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            break;
        }
    };
};
jQuery.fn.actualizarPass = function() {
    $("#cambiarpass").ajaxForm({
        url : 'control/index.php',
        data : {
            mdl : 'usuario',
            pag : 'cambiar'
        },
        type : 'post',
        beforeSubmit : validarPass,
        success : showResponse
    });
    /*Validacion de nombre y apellido*/
    function validarPass(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        var password = form.password.value;
        var password2 = form.password2.value;
        if (!password2.length && !password.length) {
            mensaje += '<div>- Debe escribir una contrase&ntilde;a</div>';
            $("#password").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#password").css({
                backgroundColor : ""
            });
        }
        if (password == password2) {
            $("#password2").css({
                backgroundColor : ""
            });
        } else {
            mensaje += '<div>- Contrase&ntilde;as no coinciden</div>';
            $("#password2").css({
                backgroundColor : "#f5c9c9"
            });
        }
        if (password.length < 6) {
            mensaje += '<div>- Contrase&ntilde;a debe tener minimo 6 caracteres</div>';
            $("#password").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#password").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: ' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#password").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: ' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            if (confirm('Desea cambiar su contraseña?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                $.funciones.mostrarMensaje('wait', 'Actualizando...Por favor espere');
                return true;
            } else {
                return false;
            }
        }
    };

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'contrasenhas':
            $.funciones.mostrarMensaje('error', 'Contraseñas no coinciden');
            $.funciones.ocultarMensaje(2000);
            break;
        case 'Error':
            $.funciones.mostrarMensaje('error', 'No se pudo cambiar la contraseña');
            $.funciones.ocultarMensaje(2000);
            break;
        case 'OK':
            $.funciones.mostrarMensaje('ok', 'Se a cambiado la contraseña');
            $.funciones.ocultarMensaje(2000);
            $.ajax({
                url : 'control/index.php',
                method : 'get',
                data : {
                    mdl : 'login',
                    pag : 'ver_datos',
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            break;
        default:
            $.funciones.mostrarMensaje('warning', responseText);
            $.funciones.ocultarMensaje(7000);
            break;
        }
    };
};
/*nombre archivo sql que sera generado*/
jQuery.fn.nombreBackup = function() {
    $("#respaldar").ajaxForm({
        url : 'control/index.php',
        type : 'post',
        data : {
            mdl : 'administracion',
            pag : 'respaldar'
        },
        beforeSubmit : validar,
        success : showResponse
    });
    function validar(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];
        if (!form.nombreArchivo.value) {
            $("#archivo").css({
                backgroundColor : "#f5c9c9"
            });
            mensaje += '<div>- Debe escribir un nombre para el archivo</div>';
        } else {
            $("#archivo").css({
                backgroundColor : ""
            });
        }
        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append(mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores").fadeOut('slow');
            $.funciones.mostrarMensaje('wait', 'Creando copia...');
            return true;
        }
    };
    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'Error':
            var msg = 'La copia no fue creada. Intentelo nuevamente';
            $.funciones.mostrarMensaje('error', msg);
            $.funciones.ocultarMensaje(5000);
            break;

        case 'OK' :
            $.funciones.mostrarMensaje('ok', 'Respaldo creado exitosamente');
            $.funciones.ocultarMensaje(5000);
            setTimeout(function() {
                $.get(url, {
                    mdl : 'login',
                    pag : 'nueva_copia'
                }, function(data) {
                    $(".post").empty().append(data);
                });
            }, 50);
            break;
        }
    };
};

/*Generar grafico*/
jQuery.fn.generarGrafico = function() {
    $("#graficar").ajaxForm({
        url : 'control/index.php',
        type : 'get',
        data : {
            mdl : 'operaciones',
            pag : 'graficar'
        },
        clearForm : true,
        beforeSubmit : showRequest,
        success : showResponse
    });
    function showRequest(formData, jqForm, options) {
        var mensaje = '';
        var form = jqForm[0];

        if (!form.tipo.value) {
            mensaje += '<div>- Debe seleccionar opcion a graficar</div>';
            $("#tipos").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#tipos").css({
                backgroundColor : ""
            });
        }
        if (!form.option.value) {
            mensaje += '<div>- Debe seleccionar opcion a graficar</div>';
            $("#option").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#option").css({
                backgroundColor : ""
            });
        }
        if (!form.cultivos.value) {
            mensaje += '<div>- Debe seleccionar cultivo</div>';
            $("#cultivos").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#cultivos").css({
                backgroundColor : ""
            });
        }
        if (!form.grafico.value) {
            mensaje += '<div>- Debe elegir el tipo de grafico</div>';
            $("#graficos").css({
                backgroundColor : "#f5c9c9"
            });
        } else {
            $("#graficos").css({
                backgroundColor : ""
            });
        }

        if (mensaje != '') {
            $("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
            $("#errores").fadeIn('slow');
            return false;
        } else {
            $("#errores div p .mensaje").empty();
            $("#errores").fadeOut('slow');
            if (confirm('Los datos son correctos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
                S.funciones.nostrarMensaje('wait', 'Creando el grafico');
                return true;
            } else {
                return false;
            }
        }
    }

    function showResponse(responseText, statusText, xhr, $form) {
        switch (responseText) {
        case 'OK' :
            $.ajax({
                url : 'control/index.php',
                method : 'GET',
                data : {
                    mdl : 'usuario',
                    pag : 'grafico'
                },
                success : function(responseText) {
                    $(".post").empty().append(responseText);
                }
            });
            /*
             var direccion = "control/index.php?mdl=operaciones&pag=graficar";
             var opciones = "fullscreen=0,toolbar=0,location=1,status=1,menubar=0,scrollbars=0,resizable=0,width=500,height=300,left=250,top=250";
             var sustituir = 0;
             var ventana = window.open(direccion, "SICF - Informe Grafico", opciones, sustituir);*/
            break;
        default:
            $.funciones.mostrarMensaje('error', responseText);
            $.funciones.ocultarMensaje(5000);
            break;
        }
    }

};
