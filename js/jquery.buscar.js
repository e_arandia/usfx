jQuery.buscar = {
    //*****************************************opciones generales*****************************************
    estiloCuadroBusqueda : function() {
        $("input#buscarTXT").css('width', '310px');
    },
    //*****************************************CERTIFICACION*****************************************
    autocompletarSemilla : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=no_sem_sol&opc=nosol&estado=0",
            minLength : 2
        });
    },
    autocompletarSuperficie : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=no_sem_sol&estado=1&opc=nosol",
            minLength : 2
        });
    },
    autocompletarInspeccion : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=no_sem_sol&inicio=4&fin=7&opc=nosol_inspeccion",
            minLength : 2
        });
    },
    autocompletarCosecha : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=no_sem_sol&inicio=6&fin=8&opc=nosol_cosecha",
            minLength : 2
        });
    },
    autocompletarSemillaProducida : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=no_sem_sol&inicio=8&fin=12&estado=11&opc=nosol_semillap",
            minLength : 2
        });
    },
    autocompletarCuenta : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=buscar&pag=cuentas",
            minLength : 2
        });
    },
    semillaCertifica : function() {
        temp_nro = $("input#buscarTXT").val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro',
                estado : 0,
                solicitud : temp_nro
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillera, function(index, semillera) {
                        html += '<tr style=\'text-align:center\'>';
                        html += '<td style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</td> ';
                        html += '<td class=\'' + index + '\'>' + json.semillera[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.comunidad[index] + '</td>';
                        html += '</tr>';
                    });
                    //agregar solicitudes a dialogo
                    $('.lst-nosolicitud>.table>tbody.buscar').empty().append(html);
                    $('div#nosolicitudes label>span').empty().append(temp_nro);
                    //mostrar cuadro de dialogo
                    $('div#nosolicitudes').dialog({// Dialog
                        title : 'Solicitudes de semillera',
                        dialogClass : 'no-close',
                        resizable : false,
                        height : 400,
                        width : 500,
                        buttons : {
                            'Aceptar' : function() {
                                $('.tbl-nosolicitud').text(temp_nro);
                                $('form#semilla').fadeIn();
                                $(this).dialog('close');
                                $(this).dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $('form#semilla').fadeOut();
                                $(this).dialog('close');
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('.tbl-semillera').text(temp[0]);
                        $('.tbl-semillerista').text(temp[1]);
                        var comunidad_name = temp[2];
                        $('.tbl-comunidad').text(comunidad_name.replace('$', '\''));
                        $('input#idSolicitud').val(temp[3]);
                        $('#f_siembra').calendarioSemilla('f_siembra', temp[6], temp[5], temp[4]);
                    });
                } else {
                    if (json.total == 1) {
                        $('input#nosolicitud').text(nro);
                        $('.tbl-nosolicitud').text(temp_nro);
                        $('.tbl-semillera').text(json.semillera);
                        $('.tbl-semillerista').text(json.semillerista);
                        $('.tbl-comunidad').text(((json.comunidad).toString()).replace('$', '\''));
                        $('#idSolicitud').val(json.isolicitud);
                        $('#f_siembra').calendarioSemilla('f_siembra', json.anho, json.mes, json.dia);
                        $('#hiddenNoSolicitud').val(nro);
                        $('form#semilla').fadeIn();
                    } else {
                        $('div.alert>label').empty().text('No se encuentra el numero de solicitud');
                        $('div.alert').fadeIn().fadeOut(3000);
                    }
                }
            });
        } else {
            //buscar semillera
            semillera = $('input#buscarTXT').val();
            if (semillera.length >= 2) {
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 0,
                    semillera : semillera
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += '<tr sytle=\'text-align:center\'>';
                            html += '<td scope=\'row\'>';
                            html += '<input type=\'radio\' value=\'' + json.nosolista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                            html += '</td> ';
                            html += '<td class=\'' + index + '\'>' + json.nosolicitud[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                            html += '<td>' + (json.comunidad[index]).replace('$', '\'') + '</td>';
                            html += '</tr>';
                        });
                        //agregar solicitudes a dialogo
                        $('label.nosoli').hide();
                        $('div.tbl-semillera-comunidad-semilla>.table>tbody.buscar').empty().append(html);
                        $('div#lst-semilleras-comunidades-semilla label.slr>span').empty().append(semillera.toUpperCase());
                        //cuadro de dialogo
                        $('#lst-semilleras-comunidades-semilla').dialog({
                            title : 'Números de solicitudes',
                            dialogClass : 'no-close',
                            resizable : false,
                            height : 400,
                            width : 500,
                            buttons : {
                                'Aceptar' : function() {
                                    $('.tbl-semillera').text(semillera);
                                    $('form#semilla').fadeIn();
                                    $('div#sup_campos').fadeIn();
                                    $(this).dialog('close');
                                    $('#lst-semilleras-comunidades-semilla').dialog('destroy');
                                },
                                'Cancelar' : function() {
                                    $('input#nosolicitud,input#semillerista').empty();
                                    $('form.seguimiento,div#sup_campos').fadeOut();
                                    $(this).dialog('close');
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $('.tbl-nosolicitud').text(temp[0]);
                            $('.tbl-semillerista').text(temp[1]);
                            var comunidad_name = temp[2];
                            $('.tbl-comunidad').text(comunidad_name.replace('$', '\''));
                            $('input#idSolicitud').val(temp[3]);
                        });
                    } else if (json.total == 1) {
                        $('#hiddenNoSolicitud').val(json.nosolicitud);
                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                        $('.tbl-semillera').empty().text(semillera);
                        $('.tbl-semillerista').empty().text(json.semillerista);
                        $('.tbl-comunidad').empty().text(json.comunidad);
                        $('input#cultivo').val(json.cultivo);
                        $('input#idSolicitud').val(json.isolicitud);
                        $('form#semilla').fadeIn();
                        $('form.seguimiento,div#sup_campos').fadeIn();
                    } else {
                        //buscar semilleristas
                        productor = $('input#buscarTXT').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'solicitud_semillera',
                            opc : 'productor_semilla',
                            estado : 0,
                            semillerista : productor
                        }, function(json) {
                            if (json.total >= 2) {
                                //varias solicitudes de un semillerista
                                var html = '';
                                $.each(json.nosolicitud, function(index, nro) {
                                    html += '<tr style=\'text-align:center\'>';
                                    html += '<td scope=\'row\'>';
                                    html += '<input type=\'radio\' value=\'' + json.solsemcomid[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                    html += '</td> ';
                                    html += '<td class=\'' + index + '\' >' + json.nosolicitud[index] + '</td>';
                                    html += '<td class=\'' + index + '\' >' + json.semillera[index] + '</td>';
                                    html += '<td class=\'' + index + '\' >' + json.comunidad[index] + '</td>';
                                    html += '</tr>';
                                });
                                //agregar solicitudes a dialogo
                                $('div.tbl-semillerista-comunidad-semilla>.table>tbody.buscar').empty().append(html);
                                $('div#lst-semilleristas-comunidades-semilla label>span').empty().append(productor);
                                //cuadro de dialogo
                                $('div#lst-semilleristas-comunidades-semilla').dialog({// Dialog
                                    title : 'Solicitudes de semillerista ' + productor,
                                    dialogClass : 'no-close',
                                    resizable : false,
                                    height : 400,
                                    width : 500,
                                    buttons : {
                                        'Aceptar' : function() {
                                            $('.tbl-semillerista').empty().text(productor);
                                            $('form#semilla').fadeIn();
                                            $(this).dialog('close');
                                            $(this).dialog('destroy');
                                        },
                                        'Cancelar' : function() {
                                            $('input#nosolicitud,input#semillerista').empty();
                                            $('form#semilla').fadeOut();
                                            $(this).dialog('close');
                                        }
                                    }
                                });
                                //copiar datos seleccionados
                                $('input:radio[name=nro]').on('click', function() {
                                    valor = $('input:radio[name=nro]:checked').val();
                                    temp = valor.split('=');
                                    $('.tbl-nosolicitud').empty().text(temp[0]);
                                    $('.tbl-semillera').empty().text(temp[1]);
                                    var comunidad_name = temp[2];
                                    $('.tbl-comunidad').empty().text(comunidad_name.replace('$', '\''));
                                    $('input#idSolicitud').empty().val(temp[3]);
                                });
                            } else {
                                //suna solicitud de un semillerista
                                $('#hiddenNoSolicitud').val(json.nosolicitud);
                                $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                                $('.tbl-semillera').empty().text(json.semillera);
                                $('.tbl-semillerista').empty().text(productor);
                                $('.tbl-comunidad').empty().text(json.comunidad);
                                $('input#cultivo').val(json.cultivo);
                                $('input#idSolicitud').val(json.isolicitud);
                                var iSolicitud = $('input#idSolicitud').val();
                                $.getJSON('control/index.php', {
                                    mdl : 'certificacion',
                                    opt : 'ver',
                                    pag : 'estado_semilla_superficie',
                                    solicitud : iSolicitud
                                }, function(json) {
                                    $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                                    $('input#iEstado').empty().val(json.estado);
                                    $('input#iSemilla').empty().val(json.semilla);
                                    $('input#iSuperficie').empty().val(json.superficie);
                                });
                                $('form#semilla').fadeIn();
                                $('form.seguimiento,div#sup_campos').fadeIn();
                            }
                        });
                    }
                });
            }
        }
    },
    superficieCertifica : function() {
        temp_nro = $('input#buscarTXT').val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro',
                estado : 1,
                solicitud : temp_nro
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillera, function(index, semillera) {
                        html += '<tr style=\'text-align:center;\'>';
                        html += '<td style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</td> ';
                        html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.comunidad[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                        html += '</tr>';
                    });
                    //agregar datos a cuadro de dialogo
                    $('.tbl-semillerista-inspeccion>.table>tbody.buscar').empty().append(html);
                    $('#lst-semillerista-inspeccion label>span').empty().append(temp_nro);
                    $('#lst-semillerista-inspeccion label.slr>span').empty().append(json.semillera[0]);
                    //mostrar titulo de cultivo
                    $('th#tbl-nro-cultivo').show();
                    //cuadro de dialogo
                    $('div#lst-semillerista-inspeccion').dialog({// Dialog
                        title : 'Cantidad de semilleristas',
                        dialogClass : 'no-close',
                        resizable : false,
                        height : 400,
                        width : 495,
                        buttons : {
                            'Aceptar' : function() {

                                //ocultar ultima fila de informacion
                                $("tr#tr-no-superficie").hide();
                                //copiar nro de solicitud escrito
                                $('.tbl-nosolicitud').empty().text(temp_nro);
                                cargarDatos();
                                $('form.seguimiento,div#sup_campos').fadeIn();
                                $(this).dialog('close');
                                $(this).dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $(this).dialog('destroy');
                                $('div#sup_campos').fadeOut();
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('.tbl-semillera').empty().text(temp[0]);
                        $('.tbl-semillerista').empty().text(temp[1]);
                        $('.tbl-comunidad').empty().text(temp[2]);
                        $('input#iSolicitud').empty().val(temp[3]);
                        $('.tbl-cultivo').empty().text(temp[7]);
                        $('input#cul').empty().text(temp[7]);
                        $('.tbl-variedad').empty().text(temp[8]);
                        //cargar isuperficie,isemilla y estado segun isolicitud
                        var iSolicitud = $('#iSolicitud').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'estado_semilla_superficie',
                            solicitud : iSolicitud
                        }, function(json) {
                            $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                            $('input#iEstado').empty().val(json.estado);
                            $('input#iSemilla').empty().val(json.semilla);
                            $('input#iSuperficie').empty().val(json.superficie);
                        });
                    });
                } else {
                    if (json.total == 1) {
                        //cargar datos
                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                        $('.tbl-semillera').empty().text(json.semillera);
                        $('.tbl-semillerista').empty().text(json.semillerista);
                        $('.tbl-comunidad').empty().text(json.comunidad);
                        $('.tbl-cultivo').empty().text(json.cultivo);
                        $('input#cul').empty().text(json.cultivo);
                        $('.tbl-variedad').empty().text(json.variedad);
                        $('input#iSolicitud').val(json.isolicitud);
                        $('input#noSolicitud').val(json.nosolicitud);
                        var iSolicitud = $('input#iSolicitud').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'estado_semilla_superficie',
                            solicitud : iSolicitud
                        }, function(json) {
                            $('label.campo').text(json.nrocampo);
                            $('input#noSolicitud').empty().val($('.tbl-nosolicitud').text());
                            $('input#iEstado').empty().val(json.estado);
                            $('input#iSemilla').empty().val(json.semilla);
                            $('input#iSuperficie').empty().val(json.superficie);
                        });
                        cargarDatos();
                        $('form.seguimiento,div#sup_campos').fadeIn();
                    } else {
                        $('div.alert>label').empty().text('No se encuentra el numero de solicitud');
                        $('div.alert').fadeIn().fadeOut(3000);
                    }
                }
            });
        } else {
            //buscar semillera
            semillera = $("input#buscarTXT").val();
            if (semillera.length >= 2) {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 1,
                    semillera : semillera
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += '<tr style=\'text-align:center;\'>';
                            html += '<td scope=\'row\'>';
                            html += '<input type=\'radio\' value=\'' + json.nosolista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                            html += '</td> ';
                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                            html += '<td>' + (json.comunidad[index]).replace('$', '\'') + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                            html += '</tr>';
                        });
                        //ocultar nro de solicitud para superficie
                        $('#no-nro-superficie').hide();
                        //agregar solicitudes a dialogo
                        $(".lst-nosolicitud-certifica>.table>tbody").empty().append(html);
                        $('#nosolicitudescertifica label.slr>span').empty().append(semillera.toUpperCase());
                        $('#nosolicitudescertifica label.nosoli>span').empty().append(json.nosolicitud[0]);
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Números de solicitudes',
                            dialogClass : "no-close",
                            resizable : false,
                            height : 400,
                            width : 470,
                            buttons : {
                                "Aceptar" : function() {
                                    //ocultar ultima fila de informacion
                                    $("tr#tr-no-superficie").hide();
                                    var semillera = $("input#buscarTXT").val();
                                    $('.tbl-semillera').empty().text(semillera);
                                    $("div#sup_campos").fadeIn();
                                    $('#nosolicitudescertifica').dialog("destroy");
                                    cargarDatos();
                                },
                                "Cancelar" : function() {
                                    $("input#nosolicitud,input#semillerista").empty();
                                    $("form.seguimiento,div#sup_campos").fadeOut();
                                    $(this).dialog("close");
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $(".tbl-nosolicitud").empty().text(temp[0]);
                            $(".tbl-semillerista").empty().text(temp[1]);
                            var comunidad_name = temp[2];
                            $(".tbl-comunidad").empty().text(comunidad_name.replace("$", "'"));
                            $("input#iSolicitud").val(temp[3]);
                            $('.tbl-cultivo').empty().text(temp[4]);
                            $('input#cul').empty().val(temp[4]);
                            $('.tbl-variedad').empty().text(temp[5]);
                            //cargar isuperficie,isemilla y estado segun isolicitud
                            var iSolicitud = $("input#iSolicitud").val();
                            $.getJSON("control/index.php", {
                                mdl : "certificacion",
                                opt : "ver",
                                pag : "estado_semilla_superficie",
                                solicitud : iSolicitud
                            }, function(json) {
                                $("input#noSolicitud").empty().val($(".tbl-nosolicitud").text());
                                $("input#iEstado").empty().val(json.estado);
                                $("input#iSemilla").empty().val(json.semilla);
                                $("input#iSuperficie").empty().val(json.superficie);
                            });
                        });
                    } else {
                        if (json.total == 1) {
                            $("#tr-no-superficie").hide();
                            $("#noSolicitud").val(json.nosolicitud);
                            $(".tbl-nosolicitud").text(json.nosolicitud);
                            $(".tbl-semillera").text(semillera);
                            $(".tbl-semillerista").text(json.semillerista);
                            $(".tbl-comunidad").text(json.comunidad);
                            $(".tbl-cultivo").text(json.cultivo);
                            $("input#cul").empty().val(json.cultivo);
                            $(".tbl-variedad").text(json.variedad);
                            $("input#iSolicitud").val(json.isolicitud);
                            var iSolicitud = $("input#iSolicitud").val();
                            $.getJSON("control/index.php", {
                                mdl : "certificacion",
                                opt : "ver",
                                pag : "estado_semilla_superficie",
                                solicitud : iSolicitud
                            }, function(json) {
                                $("input#noSolicitud").empty().val($("input#nosolicitud").val());
                                $("input#iEstado").empty().val(json.estado);
                                $("input#iSemilla").empty().val(json.semilla);
                                $("input#iSuperficie").empty().val(json.superficie);
                            });
                            cargarDatos();
                            $("form.seguimiento,div#sup_campos").fadeIn();
                        } else {
                            //buscar semilleristas
                            var productor = $("input#buscarTXT").val();
                            $.getJSON("control/index.php", {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'solicitud_semillera',
                                opc : 'productor',
                                estado : 1,
                                semillerista : productor
                            }, function(json) {
                                if (json.total >= 2) {
                                    var html = '';
                                    $.each(json.nosolicitud, function(index, nro) {
                                        html += '<tr style=\'text-align:center\'>';
                                        html += '<td scope=\'row\'>';
                                        html += '<input type=\'radio\' value=\'' + json.nosemcomestsemsup[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                        html += '</td> ';
                                        html += '<td class=\'' + index + '\' >' + json.nosolicitud[index] + '</td>';
                                        html += '<td class=\'' + index + '\' >' + json.semillera[index] + '</td>';
                                        html += '<td class=\'' + index + '\' >' + json.comunidad[index] + '</td>';
                                        html += '<td class=\'' + index + '\' >' + json.cultivo[index] + '</td>';
                                        html += '<td class=\'' + index + '\' >' + json.variedad[index] + '</td>';
                                        html += '</tr>';
                                    });
                                    //agregar solicitudes a dialogo
                                    $('.tbl-semillera-comunidad-superficie>.table>tbody.buscar').empty().append(html);
                                    $('div#lst-semilleras-comunidades-superficie label>span').empty().append(productor);
                                    //cuadro de dialogo
                                    $('div#lst-semilleras-comunidades-superficie').dialog({// Dialog
                                        title : 'Solicitudes de semillerista',
                                        dialogClass : 'no-close',
                                        resizable : false,
                                        height : 400,
                                        width : 500,
                                        buttons : {
                                            'Aceptar' : function() {
                                                //ocultar ultima fila de informacion
                                                $("tr#tr-no-superficie").hide();
                                                //copiar nombre de semillerista
                                                $('.tbl-semillerista').empty().text(productor);
                                                cargarDatos();
                                                $("div#sup_campos").fadeIn();
                                                $(this).dialog('close');
                                                $(this).dialog('destroy');
                                            },
                                            'Cancelar' : function() {
                                                $('input#nosolicitud,input#semillerista').empty();
                                                $('form#semilla').fadeOut();
                                                $(this).dialog('close');
                                            }
                                        }
                                    });
                                    //copiar datos seleccionados
                                    $('input:radio[name=nro]').on('click', function() {
                                        valor = $('input:radio[name=nro]:checked').val();
                                        temp = valor.split('=');
                                        $('.tbl-nosolicitud').empty().text(temp[0]);
                                        $('.tbl-semillera').empty().text(temp[1]);
                                        var comunidad_name = temp[2];
                                        $('.tbl-comunidad').empty().text(comunidad_name.replace('$', '\''));
                                        $('input#iSolicitud').empty().val(temp[5]);
                                        $('.tbl-cultivo').empty().text(temp[6]);
                                        $('input#cul').empty().val(temp[6]);
                                        $('.tbl-variedad').empty().text(temp[7]);
                                    });
                                } else if (json.total == 1) {
                                    $("#tr-no-superficie").hide();
                                    $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                                    $('.tbl-semillera').empty().text(json.semillera);
                                    $('.tbl-semillerista').empty().text(productor);
                                    $('.tbl-comunidad').empty().text(json.comunidad);
                                    $('.tbl-cultivo').empty().text(json.cultivo);
                                    $('input#cul').empty().val(json.cultivo);
                                    $('.tbl-variedad').empty().text(json.variedad);
                                    $('input#iSolicitud').empty().val(json.isolicitud);
                                    $('input#iEstado').empty().val(json.estado);
                                    $('input#iSemilla').empty().val(json.semilla);
                                    $('input#iSuperficie').empty().val(json.superficie);
                                    $('form.seguimiento,div#sup_campos').fadeIn();
                                    cargarDatos();
                                } else {
                                    $('div.alert>label').empty().text('No se encuentra el semillerista');
                                    $('div.alert').fadeIn().fadeOut(3000);
                                }
                            });
                        }
                    }
                });
            }
        }
        function cargarDatos() {
            var semillera = $(".tbl-semillera").text();
            var pro = $(".tbl-semillerista").text();
            var partes = pro.split(" ");
            var nombre = $.funciones.reemplazarAcento(partes[0]);
            var nro_solicitud = $(".tbl-nosolicitud").text();
            var nombres = partes.length;

            var isolicitud = $("input#iSolicitud").val();

            switch (nombres) {
            case 2:
                var apellido = $.funciones.reemplazarAcento(partes[1]);
                break;
            case 3:
                var apellido = $.funciones.reemplazarAcento(partes[2]);
                break;
            case 4:
                var apellido = $.funciones.reemplazarAcento(partes[2]);
                break;
            default:
                var apellido = '';
                break;
            }
            //cargar id semilla
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'isemilla',
                isolicitud : isolicitud
            }, function(json) {
                $("input#iSemilla").empty().val(json.isemilla);

            });
            //cargar datos de parcela
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'superficie',
                nombre : nombre,
                apellido : apellido,
                semillera : semillera,
                nro : nro_solicitud,
                estado : 1
            }, function(json) {
                //$("input#comunidad").empty().val(json.comunidad);
                $("input#inscrita").empty().val(json.inscrita);
                $("input#retirada,input#rechazada").val(0);
                $("input#aprobada").empty().val(json.inscrita - ($("input#retirada").val() + $("input#rechazada").val()));
                $("label.campo").empty().text(json.nro_campo);
            });
        }

    },
    inspeccionCertifica : function() {
        temp_nro = $('input#buscarTXT').val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro_inspeccion',
                solicitud : temp_nro
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillera, function(index, semillera) {
                        html += '<tr style=\'text-align:center;\'>';
                        html += '<td style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</td> ';
                        html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.comunidad[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                        html += '</tr>';
                    });
                    //agregar solicitudes a dialogo
                    $('.tbl-semillerista-inspeccion>.table>tbody.buscar').empty().append(html);
                    //mostrar titulo cultivo
                    $('#lst-semillerista-inspeccion label>span').empty().append(temp_nro);
                    $('#lst-semillerista-inspeccion label.slr>span').empty().append(json.semillera[0]);
                    //mostrar cuadro de dialogo
                    $('div#lst-semillerista-inspeccion').dialog({// Dialog
                        title : 'Cantidad de semilleristas',
                        resizable : false,
                        height : 400,
                        width : 460,
                        buttons : {
                            'Aceptar' : function() {
                                $('.tbl-nosolicitud').empty().text(temp_nro);
                                $('form#seguimiento').fadeIn();
                                cargarDatos();
                                $('div#lst-semillerista-inspeccion').dialog('close');
                                $('div#lst-semillerista-inspeccion').dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $('div#lst-semillerista-inspeccion').dialog('close');
                                $('form#seguimiento').fadeOut();
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('.tbl-semillera').text(temp[0]);
                        $('.tbl-semillerista').text(temp[1]);
                        $('.tbl-comunidad').text(temp[2]);
                        $('.tbl-cultivo').text(temp[4]);
                        $('.tbl-variedad').text(temp[5]);
                        $('input#isolicitud').val(temp[3]);
                        $('input[name=plantines]').val(temp[6]);
                        $('input#cul').val(temp[4]);
                    });
                } else {
                    //solo 1 nro de solicitud
                    if (json.total == 1) {
                        //cargar datos
                        $('.tbl-nosolicitud').text(temp_nro);
                        $('.tbl-semillera').text(json.semillera);
                        $('.tbl-semillerista').text(json.semillerista);
                        $('.tbl-comunidad').text(json.comunidad);
                        $('.tbl-cultivo').text(json.cultivo);
                        $('.tbl-variedad').text(json.variedad);
                        $('input#isolicitud').val(json.isolicitud);
                        $('input#noSolicitud').val(json.nosolicitud);
                        $('input[name=plantines]').val(json.plantin);
                        $('input#cul').val(json.cultivo);
                        cargarDatos();
                        var iSolicitud = $('input#isolicitud').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'estado_semilla_superficie',
                            solicitud : iSolicitud
                        }, function(json) {
                            $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                            $('input#iEstado').empty().val(json.estado);
                            $('input#iSemilla').empty().val(json.semilla);
                            $('input#iSuperficie').empty().val(json.superficie);
                            $('input#nrocampo').empty().val(json.nrocampo);
                        });
                        $.funciones.cargarNroCampoSuperficieByIsolicitud(json.isolicitud);
                        $('form#seguimiento').fadeIn();
                    } else {
                        $('div.alert>label').empty().text('No se encuentra el numero de solicitud');
                        $('div.alert').fadeIn().fadeOut(3000);
                    }
                }
            });
        } else {
            //buscar semillera
            semillera = $("input#buscarTXT").val();
            if (semillera.length >= 2) {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 4,
                    semillera : semillera
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += '<tr style= \'text-align:center;\'>';
                            html += '<th scope=\'row\'>';
                            html += '<input type=\'radio\' value=\'' + json.nosolista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                            html += '</th> ';
                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                            html += '<td>' + (json.comunidad[index]).replace('$', '\'') + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.nosolicitud[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                            html += '</tr>';
                        });
                        //agregar solicitudes a dialogo
                        $('.lst-nosolicitud-certifica>.table>tbody').empty().append(html);
                        $('#nosolicitudescertifica label.slr>span').empty().append(semillera.toUpperCase());
                        $('#nosolicitudescertifica label.nosoli>span').empty().append(json.nosolicitud[0]);
                        $('#nosolicitudescertifica label.nosoli').hide();
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Semilleristas de semillera ' + semillera,
                            dialogClass : 'no-close',
                            resizable : false,
                            height : 400,
                            width : 470,
                            buttons : {
                                'Aceptar' : function() {
                                    var semillera = $('#buscarTXT').val();
                                    $('input#semillera').val(semillera);
                                    $('input#nosolicitud,input#semillerista').addClass('not-edit');
                                    $('form#seguimiento').fadeIn();
                                    $(this).dialog('close');
                                    cargarDatos();
                                },
                                'Cancelar' : function() {
                                    $('input#nosolicitud,input#semillerista').empty();
                                    $('form#semilla').fadeOut();
                                    $(this).dialog('close');
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $('.tbl-nosolicitud').text(temp[0]);
                            $('.tbl-semillerista').text(temp[1]);
                            $('.tbl-comunidad').text(temp[2]);
                            $('input#isolicitud').val(temp[3]);
                            $('.tbl-cultivo').text(temp[4]);
                            $('.tbl-variedad').text(temp[5]);
                            $('input[name=tipo]').val(temp[6]);
                            $('input#cul').val(temp[4]);
                            //cargar isuperficie,isemilla y estado segun isolicitud
                            var iSolicitud = $('input#iSolicitud').val();
                            $.getJSON('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'estado_semilla_superficie',
                                solicitud : iSolicitud
                            }, function(json) {
                                $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                                $('input#iEstado').empty().val(json.estado);
                                $('input#iSemilla').empty().val(json.semilla);
                                $('input#iSuperficie').empty().val(json.superficie);
                            });
                        });
                    } else if (json.total == 1) {
                        $('#hiddenNoSolicitud').val(json.nosolicitud);
                        $('.tbl-nosolicitud').text(json.nosolicitud);
                        $('.tbl-semillera').text(semillera);
                        $('.tbl-semillerista').text(json.semillerista);
                        $('.tbl-comunidad').text(json.comunidad);
                        $('.tbl-cultivo').text(json.cultivo);
                        $('.tbl-variedad').text(json.variedad);
                        $('input#isolicitud').val(json.isolicitud);
                        $('input[name=plantines]').val(json.plantin);
                        $('input#cul').val(json.cultivo);
                        cargarDatos();
                        var iSolicitud = $('input#isolicitud').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'estado_semilla_superficie',
                            solicitud : iSolicitud
                        }, function(json) {
                            $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                            $('input#iEstado').empty().val(json.estado);
                            $('input#iSemilla').empty().val(json.semilla);
                            $('input#iSuperficie').empty().val(json.superficie);
                        });
                        $.funciones.cargarNroCampoSuperficieByIsolicitud(json.isolicitud);
                        $('form#seguimiento').fadeIn();
                    } else {
                        //buscar semillerista
                        productor = $('input#buscarTXT').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'solicitud_semillera',
                            opc : 'productor_inspeccion',
                            estado : 4,
                            semillerista : productor
                        }, function(json) {
                            if (json.total >= 2) {
                                var html = '';
                                $.each(json.semillera, function(index, nro) {
                                    html += '<tr style = \'text-align:center\'>';
                                    html += '<td scope=\'row\'>';
                                    html += '<input type=\'radio\' value=\'' + json.solsemcomid[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                    html += '</td> ';
                                    html += '<td class=\'' + index + '\'>' + json.nosolicitud[index] + '</td>';
                                    html += '<td>' + (json.comunidad[index]).replace('$', '\'') + '</td>';
                                    html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                                    html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                                    html += '</tr>';
                                });
                                //agregar solicitudes a dialogo
                                $('.tbl-semillera-comunidad-inspeccion>.table>tbody').empty().append(html);
                                $('#lst-semilleras-comunidades-inspeccion label.smr>span').empty().append(json.semillera[0]);
                                $('#lst-semilleras-comunidades-inspeccion label.slr>span').empty().append(productor);
                                //cuadro de dialogo
                                $('#lst-semilleras-comunidades-inspeccion').dialog({// Dialog
                                    title : 'Lista de semilleristas',
                                    dialogClass : 'no-close',
                                    resizable : false,
                                    height : 400,
                                    width : 470,
                                    buttons : {
                                        'Aceptar' : function() {
                                            $('.tbl-semillerista').empty().text(productor);
                                            $('form#seguimiento').fadeIn();
                                            $(this).dialog('close');
                                            cargarDatos();
                                        },
                                        'Cancelar' : function() {
                                            $('input#nosolicitud,input#semillerista').empty();
                                            $('form#semilla').fadeOut();
                                            $(this).dialog('close');
                                        }
                                    }
                                });
                                //copiar datos seleccionados
                                $('input:radio[name=nro]').on('click', function() {
                                    valor = $('input:radio[name=nro]:checked').val();
                                    temp = valor.split('=');
                                    $('.tbl-nosolicitud').empty().text(temp[0]);
                                    $('.tbl-semillera').empty().text(temp[1]);
                                    $('.tbl-comunidad').empty().text(temp[2]);
                                    $('input#isolicitud').val(temp[3]);
                                    $('.tbl-cultivo').empty().text(temp[4]);
                                    $('.tbl-variedad').empty().text(temp[5]);
                                    $('input[name=plantines]').val(temp[6]);
                                    $('input#cul').val(temp[4]);
                                    //cargar isuperficie,isemilla y estado segun isolicitud
                                    var iSolicitud = $('input#iSolicitud').val();
                                    $.getJSON('control/index.php', {
                                        mdl : 'certificacion',
                                        opt : 'ver',
                                        pag : 'estado_semilla_superficie',
                                        solicitud : iSolicitud
                                    }, function(json) {
                                        $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                                        $('input#iEstado').empty().val(json.estado);
                                        $('input#iSemilla').empty().val(json.semilla);
                                        $('input#iSuperficie').empty().val(json.superficie);
                                    });
                                });
                            } else {
                                $('input#noSolicitud').val(json.nosolicitud);
                                $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                                $('.tbl-semillera').empty().text(json.semillera);
                                $('.tbl-semillerista').empty().text(productor);
                                $('.tbl-comunidad').empty().text(json.comunidad);
                                $('.tbl-cultivo').empty().text(json.cultivo);
                                $('.tbl-variedad').empty().text(json.variedad);
                                $('input#isolicitud').val(json.isolicitud);
                                $('input#iEstado').empty().val(json.estado);
                                $('input#iSemilla').empty().val(json.semilla);
                                $('input#iSuperficie').empty().val(json.superficie);
                                $('input[name=plantines]').val(json.plantin);
                                $('input#cul').val(json.cultivo);
                                $('form#seguimiento').fadeIn();
                                cargarDatos();
                                mostrarInspeccion();
                            }
                        });
                    }
                });
            }
        }
        function cargarDatos() {
            //cargar isuperficie,isemilla y estado segun isolicitud
            var iSolicitud = $('input#isolicitud').val();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'estado_semilla_superficie',
                solicitud : iSolicitud
            }, function(json) {
                $('input#noSolicitud').empty().val($('input#nosolicitud').val());
                $('input#iEstado').empty().val(json.estado);
                $('input#iSemilla').empty().val(json.semilla);
                $('input#iSuperficie').empty().val(json.superficie);
                $('.tbl-nocampo').empty().text(json.nrocampo);
                $('input#hiddendia').val(json.dia);
                $('input#hiddenmes').val(json.mes);
                $('input#hiddenanho').val(json.anho);
                $('.tbl-fecha-siembra').empty().text(json.dia + ' - ' + json.mes + ' - ' + json.anho);
                mostrarInspeccion(json.superficie, json.estado);
            });
        }// mostrar inspeccion segun estado

        function mostrarInspeccion(isuperficie, estado) {
            //primera:5, segunda:6, tercera:7
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'inspecciones',
                isuperficie : isuperficie
            }, function(data) {
                switch (data.estado) {
                case 4:
                case '4':
                    //primera inspeccion
                    var hoy = new Date();
                    var dia = hoy.getDate();
                    var mes = hoy.getMonth() + 1;
                    var anho = hoy.getFullYear();
                    $('#ins_1').calendarioInspeccion('ins_1', anho, mes, dia, anho, mes, dia);
                    $('form#seguimiento').show();
                    $('#ifecha').empty().val(1);
                    $('.flora,.obs2').fadeOut('slow');
                    $('.cose,.obs3').fadeOut('slow');
                    break;
                case 5:
                case '5':
                    //segunda inspeccion
                    var hoy = new Date();
                    var dia = hoy.getDate();
                    var mes = hoy.getMonth() + 1;
                    var anho = hoy.getFullYear();
                    $('#ins_2').calendarioInspeccion('ins_2', anho, mes, dia, new Date().getFullYear(), (mes + 3), 28);
                    $('input#ins_1').val(data.vegetativa).addClass('not-edit');
                    $('#obs1').val(data.observacion_1).addClass('not-edit');
                    $('input#ins_2').removeClass('not-edit').val();
                    $('#obs2').removeClass('not-edit').val(data.observacion_2);
                    $('#seguimiento').css('padding', '3% 2% 61%');
                    $('#ifecha').empty().val(2);
                    $('.flora,.obs2').fadeIn('slow');
                    $('.cose,.obs3').fadeOut('slow');
                    break;
                case 6:
                case '6':
                    //tercera inspeccion
                    var hoy = new Date();
                    var dia = hoy.getDate();
                    var mes = hoy.getMonth() + 1;
                    var anho = hoy.getFullYear();
                    $('#ins_3').calendarioInspeccion('ins_3', anho, mes, dia, new Date().getFullYear(), 12, 31);
                    $('input#ins_1').val(data.vegetativa).addClass('not-edit');
                    $('#obs1').val(data.observacion_1).addClass('not-edit');
                    $('input#ins_2').val(data.floracion).addClass('not-edit');
                    $('#obs2').val(data.observacion_2).addClass('not-edit');
                    $('input#ins_3').val(data.precosecha);
                    $('#obs3').val(data.observacion_3);
                    $('input#ins_1,input#ins_2,#obs1,#obs2').addClass('not-edit');
                    $('#seguimiento').css('padding', '3% 2% 99%');
                    $('#ifecha').empty().val(3);
                    $('.flora,.obs2,.cose,.obs3').fadeIn('slow');
                    break;
                }
            });
        }

    },
    hojaCosechaCertifica : function() {
        temp_nro = $("input#buscarTXT").val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            //buscar nro de solicitud
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro_cosecha',
                solicitud : temp_nro
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillera, function(index, semillera) {
                        html += '<tr style=\'text-align:center;\'>';
                        html += '<th style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</th> ';
                        html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.comunidad[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                        html += '</tr>';
                    });
                    //agregar solicitudes a dialogo
                    $('.tbl-semillerista-inspeccion>.table>tbody.buscar').empty().append(html);
                    $('#lst-semillerista-inspeccion label>span').empty().append(temp_nro);
                    $('#lst-semillerista-inspeccion label.slr>span').empty().append(json.semillera[0]);
                    //cuadro de dialogo
                    $('div#lst-semillerista-inspeccion').dialog({// Dialog
                        title : 'Cantidad de semilleristas',
                        dialogClass : 'no-close',
                        resizable : false,
                        height : 400,
                        width : 500,
                        buttons : {
                            'Aceptar' : function() {
                                $('.tbl-nosolicitud').empty().text(temp_nro);
                                $("tr#cultivo-hoja-cosecha").show();
                                $.funciones.datosHojaCosecha();
                                $('form#seguimiento,form#campos').fadeIn();
                                cargarDatos();
                                $(this).dialog('close');
                                $(this).dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $('select#cultivo').selectmenu({
                                    disabled : true
                                });
                                $('form#seguimiento,form#cosecha').fadeOut();
                                $(this).dialog('close');
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('.tbl-semillera').empty().text(temp[0]);
                        $('.tbl-semillerista').empty().text(temp[1]);
                        $('.tbl-comunidad').empty().text(temp[2]);
                        $('input#iSolicitud').empty().val(temp[3]);
                        $('.tbl-cultivo').empty().text(temp[4]);
                        $('.tbl-variedad').empty().text(temp[5]);
                    });
                } else {
                    if (json.total == 1) {
                        $('td.tbl-nosolicitud').empty().text(temp_nro);
                        $('td.tbl-semillera').empty().text(json.semillera).addClass('not-edit');
                        $('td.tbl-semillerista').empty().text(json.semillerista).addClass('not-edit');
                        $('td.tbl-comunidad').empty().text(json.comunidad);
                        $('td.tbl-cultivo-hoja-cosecha').empty().text(json.cultivo);
                        $('input#hiddencultivo').empty().val(json.cultivo);
                        $('#iSolicitud').val(json.isolicitud);
                        cargarDatos();
                        $("tr#cultivo-hoja-cosecha").show();
                        $.funciones.datosHojaCosecha();
                        $('form#seguimiento,form#cosecha').fadeIn();
                    } else {
                        $('form#seguimiento').fadeOut();
                        $('input#semillera,input#semillerista').removeClass('not-edit');
                        $('div.alert').fadeIn();
                    }
                }
            });
        } else {
            //buscar semillera
            semillera = $('input#buscarTXT').val();
            if (semillera.length >= 2) {
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 8,
                    semillera : semillera
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += '<tr style=\'text-align:center;\'>';
                            html += '<td scope=\'row\'>';
                            html += '<input type=\'radio\' value=\'' + json.nosolista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                            html += '</td> ';
                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                            html += '<td>' + json.comunidad[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.nosolicitud[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                            html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                            html += '</tr>';
                        });
                        //agregar solicitudes a dialogo
                        $('.lst-nosolicitud-certifica>.table>tbody').empty().append(html);
                        $('#nosolicitudescertifica label.slr>span').empty().append(semillera.toUpperCase());
                        $('#nosolicitudescertifica label.nosoli>span').empty().append(json.nosolicitud[0]);
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Semilleristas de semillera ' + semillera,
                            dialogClass : 'no-close',
                            resizable : false,
                            height : 400,
                            width : 585,
                            buttons : {
                                'Aceptar' : function() {
                                    $('input#nosolicitud,input#semillerista').addClass('not-edit');
                                    $.funciones.datosHojaCosecha();
                                    cargarDatos();                                    
                                    $('form.seguimiento,form#cosecha').fadeIn();
                                    $('div#nosolicitudescertifica').dialog('close');
                                },
                                'Cancelar' : function() {
                                    $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                    $('select#cultivo').selectmenu({
                                        disabled : true
                                    });
                                    $('form.seguimiento,form#cosecha').fadeOut();
                                    $('div#nosolicitudescertifica').dialog('close');
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $('td.tbl-nosolicitud').empty().text(temp[0]);
                            $('td.tbl-semillera').empty().text(semillera);
                            $('td.tbl-semillerista').empty().text(temp[1]);
                            $('td.tbl-comunidad').empty().text(temp[2]);
                            $('input#iSolicitud').empty().val(temp[3]);
                            $('td.tbl-cultivo-hoja-cosecha').empty().text(temp[4]);
                            $("tr#cultivo-hoja-cosecha").show();
                        });
                    } else {
                        if (json.total == 1) {
                            $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                            $('.tbl-semillera').empty().text(semillera);
                            $('.tbl-semillerista').empty().text(json.semillerista);
                            $('.tbl-comunidad').empty().text(json.comunidad);
                            $('#iSolicitud').val(json.isolicitud);
                            $('td.tbl-cultivo-hoja-cosecha').empty().text(json.cultivo);
                            $('input#hiddencultivo').empty().val(json.cultivo);
                            cargarDatos();
                            $("tr#cultivo-hoja-cosecha").show();
                            $.funciones.datosHojaCosecha();
                            $('form.seguimiento,form#cosecha').fadeIn();
                        } else {
                            //buscar semillerista
                            productor = $('input#buscarTXT').val();
                            $.getJSON('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'solicitud_semillera',
                                opc : 'productor',
                                estado : 6,
                                semillerista : productor
                            }, function(json) {
                                if (json.total >= 2) {
                                    var html = '';
                                    $.each(json.semillera, function(index, nro) {
                                        html += '<tr style = \'text-align:center\'>';
                                        html += '<td scope=\'row\'>';
                                        html += '<input type=\'radio\' value=\'' + json.nosemcomestsemsup[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                        html += '</td> ';
                                        html += '<td class=\'' + index + '\'>' + json.nosolicitud[index] + '</td>';
                                        html += '<td>' + (json.comunidad[index]).replace('$', '\'') + '</td>';
                                        html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                                        html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                                        html += '</tr>';
                                    });
                                    //agregar solicitudes a dialogo
                                    $('.tbl-semillera-comunidad-inspeccion>.table>tbody').empty().append(html);
                                    $('#lst-semilleras-comunidades-inspeccion label.smr>span').empty().append(json.semillera[0]);
                                    $('#lst-semilleras-comunidades-inspeccion label.slr>span').empty().append(productor);
                                    //cuadro de dialogo
                                    $('#lst-semilleras-comunidades-inspeccion').dialog({// Dialog
                                        title : 'Lista de semilleristas',
                                        dialogClass : 'no-close',
                                        resizable : false,
                                        height : 400,
                                        width : 470,
                                        buttons : {
                                            'Aceptar' : function() {
                                                $('.tbl-semillerista').empty().text(productor);
                                                $("tr#cultivo-hoja-cosecha").show();
                                                $('form#seguimiento,form#cosecha').fadeIn();
                                                $(this).dialog('close');
                                                cargarDatos();
                                                $.funciones.datosHojaCosecha();
                                            },
                                            'Cancelar' : function() {
                                                $('input#nosolicitud,input#semillerista').empty();
                                                $('form#semilla').fadeOut();
                                                $(this).dialog('close');
                                            }
                                        }
                                    });
                                    //copiar datos seleccionados
                                    $('input:radio[name=nro]').on('click', function() {
                                        valor = $('input:radio[name=nro]:checked').val();
                                        temp = valor.split('=');
                                        $('.tbl-nosolicitud').empty().text(temp[0]);
                                        $('.tbl-semillera').empty().text(temp[1]);
                                        $('.tbl-comunidad').empty().text(temp[2]);
                                        $('input#iSolicitud').val(temp[5]);
                                        $('.tbl-cultivo').empty().text(temp[6]);
                                        $('td.tbl-cultivo-hoja-cosecha').text(temp[6]);
                                        $('.tbl-variedad').empty().text(temp[7]);
                                        $('input#iEstado').empty().val(temp[3]);
                                        //cargar isuperficie,isemilla y estado segun isolicitud
                                        var iSolicitud = $('input#iSolicitud').val();
                                        //cargarDatos2(iSolicitud);                                        
                                    });
                                } else {
                                    if (json.total == 1) {
                                        $('input#noSolicitud').empty().text(json.nosolicitud);
                                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                                        $('.tbl-semillera').empty().text(json.semillera);
                                        $('.tbl-semillerista').empty().text(productor);
                                        $('.tbl-comunidad').empty().text(json.comunidad);
                                        $('input#iSolicitud').val(json.isolicitud);
                                        $('input#iEstado').empty().val(json.estado);
                                        $('input#iSemilla').empty().val(json.semilla);
                                        $('input#iSuperficie').empty().val(json.superficie);
                                        $('td.tbl-cultivo-hoja-cosecha').empty().text(json.cultivo);
                                        $('input#hiddencultivo').empty().val(json.cultivo);
                                        $("tr#cultivo-hoja-cosecha").show();
                                        $('form#seguimiento,form#cosecha').fadeIn();
                                        cargarDatos2(json.isolicitud);
                                        $.funciones.datosHojaCosecha();
                                    } else {
                                        $('div.alert>label').empty().text('No se encuentra el semillerista');
                                        $('div.alert').fadeIn().fadeOut(3000);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }

        function cargarDatos2(id_solicitud) {
            //cargar isuperficie,isemilla y estado segun isolicitud
            var iSolicitud = $('input#iSolicitud[type=hidden]').val();
            $.getJSON("control/index.php", {
                mdl : "certificacion",
                opt : "ver",
                pag : "estado_semilla_superficie",
                solicitud : iSolicitud
            }, function(json) {
                $("input#iEstado").empty().val(json.estado);
                $("input#iSemilla").empty().val(json.semilla);
                $("input#iSuperficie").empty().val(json.superficie);
            });
            //cargarCultivos(iSolicitud);
        }

        function cargarDatos() {
            //cargar isuperficie,isemilla y estado segun isolicitud
            var iSolicitud = $('input#iSolicitud[type=hidden]').val();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'estado_semilla_superficie',
                solicitud : iSolicitud
            }, function(json) {
                $('input#iEstado').empty().val(json.estado);
                $('input#iSemilla').empty().val(json.semilla);
                $('input#iSuperficie').empty().val(json.superficie);
            });
            //cargarCultivos(iSolicitud);
        }
/*
        function cargarCultivos(isolicitud) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'isemilla',
                isolicitud : isolicitud
            }, function(data) {
                $('#iSemilla').val(data.isemilla);
                //cargar cultivos de una semilla determinada
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'cultivo',
                    isolicitud : isolicitud
                }, function(json) {
                    $('.seguimiento,.following').show();
                    $('#filter-box').hide();
                });
            });
            $('select#cultivo').selectmenu({
                disabled : false
            });
        }
        */

    },
    semillaProducidaCertifica : function() {
        temp_nro = $("input#buscarTXT").val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            //buscar nro de solicitud
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro_sem_prod',
                solicitud : temp_nro
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillera, function(index, semillera) {
                        html += '<tr style=\'text-align:center;\'>';
                        html += '<td style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</td> ';
                        html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.comunidad[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.cultivo[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.variedad[index] + '</td>';
                        html += '</tr>';
                    });
                    //agregar solicitudes a dialogo
                    $('.tbl-semillerista-inspeccion>.table>tbody.buscar').empty().append(html);
                    $('#lst-semillerista-inspeccion label>span').empty().append(temp_nro);
                    $('#lst-semillerista-inspeccion label.slr>span').empty().append(json.semillera[0]);
                    //mostrar cuadro de dialogo
                    $('div#lst-semillerista-inspeccion').dialog({// Dialog
                        title : 'Cantidad de semilleristas',
                        dialogClass : 'no-close',
                        resizable : false,
                        height : 400,
                        width : 470,
                        buttons : {
                            'Aceptar' : function() {
                                $('td.tbl-nosolicitud').text(temp_nro);
                                $('form#semillaprod').fadeIn();
                                cargarNroCampo();
                                $(this).dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $('form#semilla').fadeOut();
                                $(this).dialog('close');
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('.tbl-semillera').empty().text(temp[0]);
                        $('.tbl-semillerista').empty().text(temp[1]);
                        $('.tbl-comunidad').empty().text(temp[2]);
                        $('input#iSolicitud').empty().val(temp[3]);
                        $('input#cultivo').empty().val(temp[4]);
                        $('input#variedad').empty().val(temp[5]);
                    });
                } else {
                    if (json.total == 1) {
                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                        $('.tbl-semillera').empty().text(json.semillera);
                        $('.tbl-semillerista').empty().text(json.semillerista);
                        $('.tbl-comunidad').empty().text(json.comunidad);
                        $('input#cultivo').empty().val(json.cultivo);
                        $('input#variedad').empty().val(json.variedad);
                        $('#iSolicitud').val(json.isolicitud);
                        $('#hidden-nrosolicitud').val(nro);
                        cargarNroCampo();
                        $('form#semillaprod').fadeIn();
                    } else {
                        $('div.alert>label').empty().text('No se encuentra el numero de solicitud');
                        $('div.alert').fadeIn().fadeOut(3000);
                    }
                }
            });
        } else {
            //buscar semillera
            semillera = $('input#buscarTXT').val();
            if (semillera.length >= 2) {
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera_producida',
                    estado : 11,
                    semillera : semillera
                }, function(json) {
                    if (json.total > 1) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += '<tr style=\'text-align:center;\'>';
                            html += '<td scope=\'row\'>';
                            html += '<input type=\'radio\' value=\'' + json.nosolista[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                            html += '</td> ';
                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                            html += '<td>' + json.comunidad[index] + '</td>';
                            html += '<td>' + json.nosolicitud[index] + '</td>';
                            html += '<td>'+ json.cultivo[index]+' </td>';
                            html += '<td>'+json.variedad[index]+'</td>';
                            html += '</tr>';
                        });
                        //agregar solicitudes a dialogo
                        $('.lst-nosolicitud-certifica>.table>tbody').empty().append(html);
                        //ordenar por columnas
                        //$(".table").tablesorter();
                        $('#nosolicitudescertifica label.slr>span').empty().append(semillera.toUpperCase());
                        $('#nosolicitudescertifica label.nosoli>span').empty().append(json.nosolicitud[0]);
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Números de solicitudes',
                            dialogClass : 'no-close',
                            resizable : false,
                            height : 400,
                            width : 470,
                            buttons : {
                                'Aceptar' : function() {
                                    $('.tbl-semillera').empty().text(semillera);
                                    $('form#semillaprod').fadeIn();
                                    cargarNroCampo();
                                    $(this).dialog('destroy');
                                },
                                'Cancelar' : function() {
                                    $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                    $('form#semilla').fadeOut();
                                    $(this).dialog('close');
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $('.tbl-nosolicitud').empty().text(temp[0]);
                            $('.tbl-semillera').empty().text(semillera);
                            $('.tbl-semillerista').empty().text(temp[1]);
                            $('.tbl-comunidad').empty().text(temp[2]);
                            $('input#iSolicitud').val(temp[3]);
                            $('input#cultivo').empty().text(temp[4]);
                            $('input#variedad').empty().text(temp[5]);
                            $('input#iSemilla').val(temp[6]);
                            $('input#iEstado').val(temp[7]);
                            $('input#iCosecha').val(temp[8]);

                            //alert('valores copiados');
                        });
                    } else if (json.total == 1) {
                        $('#hidden-nrosolicitud').val(json.nosolicitud);
                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                        $('.tbl-semillera').empty().text(semillera);
                        $('.tbl-semillerista').empty().text(json.semillerista);
                        $('.tbl-comunidad').empty().text(json.comunidad);
                        $('input#cultivo').empty().val(json.cultivo);
                        $('input#variedad').empty().val(json.variedad);
                        $('#iSolicitud').val(json.isolicitud);
                        cargarNroCampo();
                        $('form#semillaprod').fadeIn();
                    } else {
                        //buscar semillerista
                        semillerista = $('input#buscarTXT').val();
                        if (semillerista.length >= 2) {
                            $.getJSON('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'solicitud_semillera',
                                opc : 'productor',
                                estado : 11,
                                semillerista : semillerista
                            }, function(json) {
                                if (json.total >= 2) {
                                    var html = '';
                                    $.each(json.nosolicitud, function(index, nro) {
                                        html += '<tr style=\'text-align:center;\'>';
                                        html += '<td scope=\'row\'>';
                                        html += '<input type=\'radio\' value=\'' + json.nosemcomestsemsup[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                        html += '</td> ';
                                        html += '<td>' + json.nosolicitud[index] + '</td>';
                                        html += '<td>' + json.comunidad[index] + '</td>';
                                        html += '<td>' + json.cultivo[index] + '</td>';
                                        html += '<td>' + json.variedad[index] + '</td>';
                                        html += '</tr>';
                                    });
                                    //agregar solicitudes a dialogo
                                    $('.tbl-semillera-comunidad-inspeccion>.table>tbody').empty().append(html);
                                    //ordenar por columnas
                                    //$(".table").tablesorter();
                                    $('#lst-semilleras-comunidades-inspeccion label.slr>span').empty().append(semillerista);
                                    $('#lst-semilleras-comunidades-inspeccion label.smr>span').empty().append(json.semillera[0]);
                                    //cuadro de dialogo
                                    $('#lst-semilleras-comunidades-inspeccion').dialog({// Dialog
                                        title : 'Números de solicitudes',
                                        dialogClass : 'no-close',
                                        resizable : false,
                                        height : 400,
                                        width : 470,
                                        buttons : {
                                            'Aceptar' : function() {
                                                $('.tbl-semillerista').empty().text(semillerista);
                                                $('form#semillaprod').fadeIn();
                                                cargarNroCampo();
                                                $(this).dialog('destroy');
                                            },
                                            'Cancelar' : function() {
                                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                                $('form#semilla').fadeOut();
                                                $(this).dialog('close');
                                            }
                                        }
                                    });
                                    //copiar datos seleccionados
                                    $('input:radio[name=nro]').on('click', function() {
                                        valor = $('input:radio[name=nro]:checked').val();
                                        temp = valor.split('=');
                                        $('.tbl-nosolicitud').empty().text(temp[0]);
                                        $('.tbl-semillera').empty().text(temp[1]);
                                        $('.tbl-comunidad').empty().text(temp[2]);
                                        $('input#iSolicitud').val(temp[4]);
                                        $('input#cultivo').val(temp[6]);
                                        $('input#variedad').val(temp[7]);
                                        $('input#iSemilla').val(temp[5]);
                                        $('input#iEstado').val(temp[3]);

                                        //alert('valores copiados');
                                    });
                                } else {
                                    if (json.total == 1) {
                                        $('.tbl-nosolicitud').empty().text(json.nosolicitud);
                                        $('.tbl-semillera').empty().text(json.semillera);
                                        $('.tbl-semillerista').empty().text(semillerista);
                                        $('.tbl-comunidad').empty().text(json.comunidad);
                                        $('input#cultivo').empty().val(json.cultivo);
                                        $('input#variedad').empty().val(json.variedad);
                                        $('#iSolicitud').empty().val(json.isolicitud);
                                        $('#iSemilla').empty().val(json.isemilla);
                                        $('#iEstado').empty().val(json.estado);
                                        cargarNroCampo();
                                        $('form#semillaprod').fadeIn();
                                    } else {
                                        $('div.alert>label').empty().text('No se encuentra el semillerista');
                                        $('div.alert').fadeIn().fadeOut(3000);
                                    }
                                }
                            });
                        }
                    }
                });
            }

        }
        function cargarNroCampo() {
            var semillera = $('.tbl-semillera').text();
            var productor = $('.tbl-semillerista').text();

            if (productor != '') {
                var partes = productor.split(" ");
                var nombre = partes[0];
                var nombres = partes.length;
                switch (nombres) {
                case 2:
                    var apellido = partes[1];
                    break;
                case 3:
                    var apellido = partes[2];
                    break;
                case 4:
                    var apellido = partes[2];
                    break;
                default:
                    var apellido = partes[3];
                    break;
                }
                var iarea = $("#hiddenArea").val();
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'campos',
                    nombre : nombre,
                    apellido : apellido,
                    semillera : semillera,
                    area : iarea
                }, function(data) {
                    //$("input#comunidad").val(data.comunidad);
                    if (data.papa.length > 0) {
                        $("select#nrocampo").empty().append('<option value="">[Seleccione un campo]</option>');
                        $.each(data.papa, function(index, value) {
                            $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');
                        });
                        if (data.otros.length > 0) {
                            $.each(data.otros, function(index, value) {
                                $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');
                            });
                        }
                    } else {
                        if (data.otros.length > 0) {
                            $("select#nrocampo").empty().append('<option value="">[Seleccione un campo]</option>');
                            $.each(data.otros, function(index, value) {
                                $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');
                            });
                        } else {
                            $("select#nrocampo").append('<option value="">No se encuentran mas campos</option>').attr('disabled', 'disabled');
                        }
                    }
                    $("select#nrocampo").selectmenu().selectmenu("menuWidget").addClass("overflow-municipio");
                    $("select#nrocampo").selectmenu("refresh");
                });
                $("select#nrocampo").selectmenu({
                    disabled : false
                });
            }
        }

    },
    cuentaCertifica : function() {
        semillera = $('input#buscarTXT').val();
        if (semillera.length >= 2) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilleras_cuenta',
                opc : 'semillera',
                slr : semillera
            }, function(json) {
                if (json.total >= 2) {
                    var html = '';
                    $.each(json.semillerista, function(index, semillera) {
                        html += '<tr style=\'text-align:center\'>';
                        html += '<td style="width: 15%;" scope=\'row\'>';
                        html += '<input type=\'radio\' value=\'' + json.semcamculvar[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                        html += '</td> ';
                        html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.comunidad[index]) + '</td>';
                        html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento($.funciones.cambiarSignoPorEnhe(json.semillerista[index])) + '</td>';
                        html += '<td class=\'' + index + '\'>' + json.campanha[index] + '</td>';
                        html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.cultivo[index]) + '</td>';
                        html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorEnhe($.funciones.cambiarSignoPorAcento(json.variedad[index])) + '</td>';
                        html += '</tr>';
                    });
                    //agregar solicitudes a dialogo
                    $('.tbl-cuenta>.table>tbody.buscar').empty().append(html);
                    $('#lst-semilleras label.slr>span').empty().append(semillera.toUpperCase());
                    //mostrar cuadro de dialogo
                    $('div#lst-semilleras').dialog({// Dialog
                        title : 'Lista de semilleristas',
                        dialogClass : 'no-close',
                        resizable : false,
                        height : 400,
                        width : 610,
                        buttons : {
                            'Aceptar' : function() {
                                //copiar nombre de semillera
                                $('input#semillera').val(semillera);
                                //mostrar resultado
                                $('form#cuenta').fadeIn();
                                $(this).dialog('close');
                                $(this).dialog('destroy');
                            },
                            'Cancelar' : function() {
                                $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                $('#ctaRight,#ctaLeft,#ctaFinal').fadeIn();
                                $(this).dialog('close');
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $('input#semillerista').val($.funciones.cambiarSignoPorAcento(temp[0]));
                        $('input#campo').empty().val(temp[1]);
                        $('input#cultivo').empty().val(temp[2]);
                        $('input#variedad').empty().val(temp[3]);
                        $('input#campanha').empty().val(temp[4]);
                        $('input#iEstado').empty().val(temp[5]);
                        $('input#iSolicitud').empty().val(temp[6]);
                        $('input#iSemilla').empty().val(temp[7]);
                        $('input#comunidad').empty().val(temp[8]);
                        $('input#iSuperficie').empty().val(temp[9]);
                        cargarISuperficie(temp[6], temp[7]);
                    });
                } else {
                    if (json.total == 1) {
                        $('input#semillerista').empty().val($.funciones.cambiarSignoPorAcento(json.semillerista)).addClass('not-edit');
                        $('input#semillera').empty().val(semillera).addClass('not-edit');
                        $('input#comunidad').empty().val(json.comunidad).addClass('not-edit');
                        $('input#campo').empty().val(json.nrocampo).addClass('not-edit');
                        $('input#cultivo').empty().val(json.cultivo).addClass('not-edit');
                        $('input#variedad').empty().val(json.variedad).addClass('not-edit');
                        $('input#campanha').empty().val(json.campanha).addClass('not-edit');
                        $('input#iEstado').empty().val(json.estado);
                        $('input#iSolicitud').empty().val(json.isolicitud);
                        $('input#iSemilla').empty().val(json.isemilla);
                        $('input#iSuperficie').empty().val(json.isuperficie);
                        cargarISuperficie(json.isolicitud, json.isemilla);
                        $('form#cuenta').fadeIn();
                        $('#ctaRight,#ctaLeft,#ctaFinal').fadeIn();
                        $('#button-send').fadeIn();
                    } else {
                        //buscar semillerista
                        semillerista = $('input#buscarTXT').val();
                        $.getJSON('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'semilleras_cuenta',
                            opc : 'semillerista',
                            smr : semillerista
                        }, function(json) {
                            if (json.total >= 2) {
                                var html = '';
                                $.each(json.semillerista, function(index, semillera) {
                                    html += '<tr style=\'text-align:center\'>';
                                    html += '<td style="width: 15%;" scope=\'row\'>';
                                    html += '<input type=\'radio\' value=\'' + json.semcamculvar[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                    html += '</td> ';
                                    html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.comunidad[index]) + '</td>';
                                    html += '<td class=\'' + index + '\'>' + json.semillera[index] + '</td>';
                                    html += '<td class=\'' + index + '\'>' + json.campanha[index] + '</td>';
                                    html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.cultivo[index]) + '</td>';
                                    html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorEnhe($.funciones.cambiarSignoPorAcento(json.variedad[index])) + '</td>';
                                    html += '</tr>';
                                });
                                //agregar solicitudes a dialogo
                                $('.tbl-cuenta-smr>.table>tbody.buscar').empty().append(html);
                                $('#lst-cuenta-smr label.smr>span').empty().append(semillerista);
                                //mostrar cuadro de dialogo
                                $('div#lst-cuenta-smr').dialog({// Dialog
                                    title : 'Lista de semilleristas',
                                    dialogClass : 'no-close',
                                    resizable : false,
                                    height : 400,
                                    width : 610,
                                    buttons : {
                                        'Aceptar' : function() {
                                            //copiar nombre de semillera
                                            $('input#semillera').val(semillera);
                                            //mostrar resultado
                                            $('form#cuenta').fadeIn();
                                            $(this).dialog('destroy');
                                        },
                                        'Cancelar' : function() {
                                            $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                            $('#ctaRight,#ctaLeft,#ctaFinal').fadeIn();
                                            $(this).dialog('close');
                                        }
                                    }
                                });
                                //copiar datos seleccionados
                                $('input:radio[name=nro]').on('click', function() {
                                    valor = $('input:radio[name=nro]:checked').val();
                                    temp = valor.split('=');
                                    $('input#semillerista').val($.funciones.cambiarSignoPorAcento(temp[0]));
                                    $('input#campo').empty().val(temp[1]);
                                    $('input#cultivo').empty().val(temp[2]);
                                    $('input#variedad').empty().val(temp[3]);
                                    $('input#campanha').empty().val(temp[4]);
                                    $('input#iEstado').empty().val(temp[5]);
                                    $('input#iSolicitud').empty().val(temp[6]);
                                    $('input#iSemilla').empty().val(temp[7]);
                                    $('input#comunidad').empty().val(temp[8]);
                                    $('input#iSuperficie').empty().val(temp[9]);
                                    cargarISuperficie(temp[6], temp[7]);
                                });
                            } else {
                                if (json.total == 1) {
                                    $('input#semillera').empty().val(json.semillera);
                                    $('input#semillerista').empty().val(json.semillerista);
                                    $('input#campanha').empty().val(json.campanha);
                                    $('input#comunidad').empty().val(json.comunidad);
                                    $('input#cultivo').empty().val(json.cultivo);
                                    $('input#variedad').empty().val(json.variedad);
                                    $('input#categoriaAprob').empty().val(json.categoria_producir);
                                    $('input#iEstado').val(json.estado);
                                    $('input#iSolicitud').val(json.isolicitud);
                                    $('input#iSemilla').val(json.isemilla);
                                    $('input#iSuperficie').val(json.isuperficie);
                                    cargarISuperficie(json.isolicitud, json.isemilla);
                                    $('form#cuenta').fadeIn();
                                } else {
                                    //buscar cultivo
                                    cultivo = $("input#buscarTXT").val();
                                    $.getJSON('control/index.php', {
                                        mdl : 'certificacion',
                                        opt : 'ver',
                                        pag : 'semilleras_cuenta',
                                        opc : 'cultivo',
                                        cul : cultivo
                                    }, function(json) {
                                        if (json.total >= 2) {
                                            var html = '';
                                            $.each(json.semillerista, function(index, semillera) {
                                                html += '<tr style=\'text-align:center\'>';
                                                html += '<td style="width: 15%;" scope=\'row\'>';
                                                html += '<input type=\'radio\' value=\'' + json.slrsmrcamvar[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                                html += '</td> ';
                                                html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.comunidad[index]) + '</td>';
                                                html += '<td class=\'' + index + '\'>' + json.semillera[index] + '</td>';
                                                html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.semillerista[index]) + '</td>';
                                                html += '<td class=\'' + index + '\'>' + json.campanha[index] + '</td>';
                                                html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorEnhe($.funciones.cambiarSignoPorAcento(json.variedad[index])) + '</td>';
                                                html += '</tr>';
                                            });
                                            //agregar solicitudes a dialogo
                                            $('.tbl-cuenta-cul>.table>tbody.buscar').empty().append(html);
                                            $('#lst-cuenta-cul label.cltv>span').empty().append(cultivo);
                                            //mostrar cuadro de dialogo
                                            $('div#lst-cuenta-cul').dialog({// Dialog
                                                title : 'Lista cultivos de ' + cultivo,
                                                dialogClass : 'no-close',
                                                resizable : false,
                                                height : 400,
                                                width : 610,
                                                buttons : {
                                                    'Aceptar' : function() {
                                                        //copiar nombre de semillera
                                                        $('input#semillera').val(semillera);
                                                        //mostrar resultado
                                                        $('form#cuenta').fadeIn();
                                                        $(this).dialog('destroy');
                                                    },
                                                    'Cancelar' : function() {
                                                        $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                                        $('#ctaRight,#ctaLeft,#ctaFinal').fadeIn();
                                                        $(this).dialog('close');
                                                    }
                                                }
                                            });
                                            //copiar datos seleccionados
                                            $('input:radio[name=nro]').on('click', function() {
                                                valor = $('input:radio[name=nro]:checked').val();
                                                temp = valor.split('=');
                                                $('input#semillerista').val($.funciones.cambiarSignoPorAcento(temp[0]));
                                                $('input#campo').empty().val(temp[1]);
                                                $('input#cultivo').empty().val(temp[2]);
                                                $('input#variedad').empty().val(temp[3]);
                                                $('input#campanha').empty().val(temp[4]);
                                                $('input#iEstado').empty().val(temp[5]);
                                                $('input#iSolicitud').empty().val(temp[6]);
                                                $('input#iSemilla').empty().val(temp[7]);
                                                $('input#comunidad').empty().val(temp[8]);
                                                $('input#iSuperficie').empty().val(temp[9]);
                                                cargarISuperficie(temp[6], temp[7]);
                                            });
                                        } else {
                                            if (json.total == 1) {
                                                $('input#semillera').empty().val(json.semillera);
                                                $('input#semillerista').empty().val(json.semillerista);
                                                $('input#campanha').empty().val(json.campanha);
                                                $('input#comunidad').empty().val(json.comunidad);
                                                $('input#cultivo').empty().val(json.cultivo);
                                                $('input#variedad').empty().val(json.variedad);
                                                $('input#categoriaAprob').empty().val(json.categoria_producir);
                                                $('input#iEstado').val(json.estado);
                                                $('input#iSolicitud').val(json.isolicitud);
                                                $('input#iSemilla').val(json.isemilla);
                                                $('input#iSuperficie').val(json.isuperficie);
                                                cargarISuperficie(json.isolicitud, json.isemilla);
                                                $('form#cuenta').fadeIn();
                                            } else {
                                                //buscar variedad
                                                variedad = $("input#buscarTXT").val();
                                                $.getJSON('control/index.php', {
                                                    mdl : 'certificacion',
                                                    opt : 'ver',
                                                    pag : 'semilleras_cuenta',
                                                    opc : 'variedad',
                                                    variedad : variedad
                                                }, function(json) {
                                                    if (json.total >= 2) {
                                                        var html = '';
                                                        $.each(json.semillerista, function(index, semillera) {
                                                            html += '<tr style=\'text-align:center\'>';
                                                            html += '<td style="width: 15%;" scope=\'row\'>';
                                                            html += '<input type=\'radio\' value=\'' + json.slrsmrcamcul[index] + '\' class=\'' + index + '\' name=\'nro\'/>';
                                                            html += '</td> ';
                                                            html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.comunidad[index]) + '</td>';
                                                            html += '<td class=\'' + index + '\'>' + json.semillera[index] + '</td>';
                                                            html += '<td class=\'' + index + '\'>' + json.semillerista[index] + '</td>';
                                                            html += '<td class=\'' + index + '\'>' + json.campanha[index]+ '</td>';
                                                            html += '<td class=\'' + index + '\'>' + $.funciones.cambiarSignoPorAcento(json.cultivo[index])  + '</td>';
                                                            html += '</tr>';
                                                        });
                                                        //agregar solicitudes a dialogo
                                                        $('.tbl-cuenta-vrd>.table>tbody.buscar').empty().append(html);
                                                        $('#lst-cuenta-vrd label.vrdd>span').empty().append(variedad);
                                                        //mostrar cuadro de dialogo
                                                        $('div#lst-cuenta-vrd').dialog({// Dialog
                                                            title : 'Lista cultivos de ' + cultivo,
                                                            dialogClass : 'no-close',
                                                            resizable : false,
                                                            height : 400,
                                                            width : 610,
                                                            buttons : {
                                                                'Aceptar' : function() {
                                                                    //copiar nombre de semillera

                                                                    //mostrar resultado
                                                                    $('form#cuenta').fadeIn();
                                                                    $(this).dialog('destroy');
                                                                },
                                                                'Cancelar' : function() {
                                                                    $('form#form-buscador input').empty().val('').removeClass('not-edit');
                                                                    $('#ctaRight,#ctaLeft,#ctaFinal').fadeIn();
                                                                    $(this).dialog('close');
                                                                }
                                                            }
                                                        });
                                                        //copiar datos seleccionados
                                                        $('input:radio[name=nro]').on('click', function() {
                                                            valor = $('input:radio[name=nro]:checked').val();
                                                            temp = valor.split('=');
                                                            $('input#semillera').val(temp[0]);
                                                            $('input#semillerista').val($.funciones.cambiarSignoPorAcento(temp[1]));
                                                            $('input#campo').empty().val(temp[1]);
                                                            $('input#cultivo').empty().val(temp[3]);
                                                            $('input#variedad').empty().val(temp[7]);
                                                            $('input#campanha').empty().val(temp[2]);
                                                            $('input#iEstado').empty().val(temp[4]);
                                                            $('input#iSolicitud').empty().val(temp[5]);
                                                            $('input#iSemilla').empty().val(temp[6]);
                                                            $('input#comunidad').empty().val(temp[8]);
                                                            $('input#iSuperficie').empty().val(temp[9]);
                                                            cargarISuperficie(temp[5], temp[6]);
                                                        });
                                                    } else {
                                                        if (json.total == 1) {
                                                            $('input#semillera').empty().val(json.semillera);
                                                            $('input#semillerista').empty().val(json.semillerista);
                                                            $('input#campanha').empty().val(json.campanha);
                                                            $('input#comunidad').empty().val(json.comunidad);
                                                            $('input#cultivo').empty().val(json.cultivo);
                                                            $('input#variedad').empty().val(json.variedad);
                                                            $('input#categoriaAprob').empty().val(json.categoria_producir);
                                                            $('input#iEstado').val(json.estado);
                                                            $('input#iSolicitud').val(json.isolicitud);
                                                            $('input#iSemilla').val(json.isemilla);
                                                            $('input#iSuperficie').val(json.isuperficie);
                                                            cargarISuperficie(json.isolicitud, json.isemilla);
                                                            $('form#cuenta').fadeIn();
                                                        } else {
                                                            //buscar campanha
                                                            campanha = $("input#buscarTXT").val();
                                                            $.getJSON('control/index.php', {
                                                                mdl : 'certificacion',
                                                                opt : 'ver',
                                                                pag : 'semilleras_cuenta',
                                                                opc : 'semillera',
                                                                campanha : campanha
                                                            }, function(json) {
                                                                if (json.total >= 2) {

                                                                } else {
                                                                    //mostrar resultado en una sola campanha
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
            //funciones complementarias
            function cargarISuperficie(isolicitud, isemilla) {
                var isol = parseInt(isolicitud);
                var isem = parseInt(isemilla);
                //cargar saldo campanha anterior
                $.ajax({
                    url : 'control/index.php',
                    type : 'POST',
                    async : true,
                    dataType : 'json',
                    data : {
                        mdl : 'certificacion',
                        opt : 'saldo_anterior',
                        isol : isol,
                        isem : isem
                    },
                    success : function(data) {
                        $('#campAnterior').val(data.saldo);
                    }
                });
                //cargar datos de la cuenta
                var isup = $('input#iSuperficie').val();
                var std = $('input#iEstado').val();
                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'resumenCta',
                    iSolicitud : isol,
                    iSemilla : isem,
                    iSuperficie : isup,
                    iEstado : std
                }, function(json) {
                    $('input#categoriaAprob').empty().val(json.catAprobada);
                    $('input#superficie').empty().val(json.supAprobada);
                    $('input#superficietot').empty().val(json.supTotal);
                    
                    $('input#inscripcion').empty().val((json.costo_inscripcion));
                    $('input#icampo').empty().val((json.costo_inspeccion).toFixed(2));
                    $('input#anlet').empty().val((json.analisisEtiqueta).toFixed(2));
                    $('input#acondicionamiento').empty().val(json.acondicionamiento);
                    $('input#plantines').empty().val(json.plantines);
                    $('input#total2').empty().val((json.total).toFixed(2));
                    $('input#saldototal').empty().val((json.saldo).toFixed(2));
                    $('input#bolsa').empty().val(json.etiquetas);
                });
            }

        }
    },
    //*****************************************FISCALIZACION*****************************************
    autocompletar : function(opcion, estado) {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=buscar&pag=no_sem_sol&opc=" + opcion + "&estado=" + estado,
            minLength : 2
        });
    },
    autocompletarCuentaFiscal : function() {
        $("input#buscarTXT").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=buscar&pag=cuentas",
            minLength : 2
        });
    },
    semillaFiscal : function() {
        var semillera;
        var productores;
        var area;
        temp_nro = $('input#buscarTXT').val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro',
                estado : 0,
                solicitud : temp_nro
            }, function(json) {
                $("td.tbl-nosolicitud-fiscal").empty().text(temp_nro);
                $("td.tbl-semillera-fiscal").empty().text(json.semillera);
                $("td.tbl-semillerista-fiscal").empty().text(json.semillerista);
                $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);
                $("td.tbl-cultivo_title-fiscal,td.tbl-variedad_title-fiscal").css('visibility','hidden');
                $("input#iSolicitud").val(json.isolicitud);
                $("input#hiddenNoSolicitud").val(nro);
                $("form#semilla,div#cultivos,form#semillaP").fadeIn();
            });
        } else {
            semillera = $('input#buscarTXT').val();
            //semillera
            if (semillera.length > 0) {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 0,
                    semillera : semillera
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += "<tr>";
                            html += "<th scope='row'>";
                            html += "<input type='radio' value='" + json.nosolista[index] + "' class='" + index + "' name='nro'/>";
                            html += "</th> ";
                            html += "<td class='" + index + "'>" + json.semillerista[index]+ "</td>";
                            html += "<td>" + json.comunidad[index] + "</td>";
                            html += "</tr>";
                        });
                        //agregar solicitudes a dialogo
                        $(".lst-nosolicitud-certifica>.table>tbody").empty().append(html);
                        $("#nosolicitudescertifica label.slr>span").empty().append(semillera.toUpperCase());
                        $("#nosolicitudescertifica label.nosoli>span").empty().append(json.nosolicitud[0]);
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Números de solicitudes',
                            dialogClass : "no-close",
                            resizable : false,
                            height : 400,
                            buttons : {
                                "Aceptar" : function() {
                                    var nro = $("input#nosolicitud").val();
                                    var semillerista = $("input#semillerista").val();
                                    var semillera = $("input#semillera").val();
                                    $.funciones.buscarComunidadBy(nro, semillera, semillerista);
                                    $("input#semillera").addClass(semillera);
                                    $('#nosolicitudescertifica').dialog("close");
                                },
                                "Cancelar" : function() {
                                    $("input#nosolicitud,input#semillerista").empty();
                                    $(this).dialog("close");
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $("input#nosolicitud,input#hiddenNoSolicitud").val(temp[0]);
                            $("input#semillerista").val(temp[1]);
                        });
                    } else if (json.total == 1) {
                        $("#hiddenNoSolicitud").val(json.nosolicitud);
                        $("td.tbl-nosolicitud-fiscal").empty().text(json.nosolicitud);
                        $("td.tbl-semillera-fiscal").empty().text(semillera);
                        $("td.tbl-semillerista-fiscal").empty().text(json.semillerista);
                        $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);                        
                        $("td.tbl-cultivo_title-fiscal,td.tbl-variedad_title-fiscal").css('visibility','hidden');
                        $("#iSolicitud").val(json.isolicitud);
                        $("form#semilla,div#cultivos,form#semillaP").fadeIn();
                    } else {
                        //buscar semillerista
                        productor = $("input#buscarTXT").val();
                        $.getJSON("control/index.php", {
                            mdl : 'fiscalizacion',
                            opt : 'ver',
                            pag : 'solicitud_semillera',
                            opc : 'productor_semilla',
                            estado : 0,
                            semillerista : productor
                            
                        }, function(json) {
                            if (json.total >= 2) {
                                var html = '';
                                $.each(json.nosolicitud, function(index, nro) {
                                    html += "<tr style=\"text-align:center\">";
                                    html += "<td scope='row'>";
                                    html += "<input type='radio' value='" + json.nosolera[index] + "' class='" + index + "' name='nro'/>";
                                    html += "</td> ";
                                    html += "<td>" + nro+"</td>";
                                    html += "<td>" + json.comunidad[index] + "</td>";
                                    html += "</tr>";
                                });
                                //agregar solicitudes a dialogo
                                $(".lst-nosolicitud>.table>tbody").empty().append(html);
                                $('#lst-nosolicitudes label>span').empty().append(productor);
                                $('#lst-nosolicitudes label>span#smr').empty().append(json.semillera[0]);
                                //cuadro de dialogo
                                $('#lst-nosolicitudes').dialog({// Dialog
                                    title : 'Números de solicitudes',
                                    dialogClass : "no-close",
                                    resizable : false,
                                    height : 400,
                                    buttons : {
                                        "Aceptar" : function() {
                                            var nro = $("input#nosolicitud").val();
                                            var semillerista = $("input#semillerista").val();
                                            var semillera = $("input#semillera").val();
                                            $.funciones.buscarComunidadBy(nro, semillera, semillerista);
                                            $("input#semillerista").val(productor);
                                            $(this).dialog("close");
                                        },
                                        "Cancelar" : function() {
                                            $("input#nosolicitud,input#semillera").empty();
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                                //copiar datos seleccionados
                                $('input:radio[name=nro]').on('click', function() {
                                    valor = $('input:radio[name=nro]:checked').val();
                                    temp = valor.split('=');
                                    $("td.tbl-nosolicitud-fiscal").empty().text(temp[0]);
                                    $("td.tbl-semillera-fiscal").empty().text(temp[1]);
                                    $("td.tbl-comunidad-fiscal").empty().text(temp[2]);
                                    
                                });
                            } else {
                                if (json.total == 1) {
                                    $("#hiddenNoSolicitud").val(json.nosolicitud);
                                    $("td.tbl-nosolicitud-fiscal").empty().text(json.nosolicitud);
                                    $("td.tbl-semillera-fiscal").empty().text(json.semillera);
                                    $("td.tbl-semillerista-fiscal").empty().text(productor);
                                    $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);
                                    $("td.tbl-cultivo_title-fiscal,td.tbl-variedad_title-fiscal").css('visibility','hidden');
                                    $("input#iSolicitud").val(json.isolicitud);
                                    $("form#semilla,div#cultivos,form#semillaP").fadeIn();
                                } else {
                                    $("div.alert").fadeIn().fadeOut(3000);
                                }
                            }
                        });

                    }
                });
            }
        }
    },
    hojaCosechaFiscal : function() {
        temp_nro = $("input#buscarTXT").val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro',
                estado : 1,
                solicitud : temp_nro
            }, function(json) {
                if (json.total > 0) {
                    $("#hiddenNoSolicitud").val(temp_nro);
                        $("td.tbl-nosolicitud-fiscal").empty().text(temp_nro);
                        $("td.tbl-semillera-fiscal").empty().text(json.semillera);
                        $("td.tbl-semillerista-fiscal").empty().text(json.semillerista);
                        $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);
                        $("td.tbl-cultivo-fiscal").empty().text(json.cultivo);
                        $("td.tbl-variedad-fiscal").empty().text(json.variedad);
                        $("input#iSolicitud").val(json.isolicitud);
                    $("form.seguimiento").fadeIn();
                } else {
                    $("div.alert").fadeIn().fadeOut(3000);
                }
            });
        } else {
            //buscar semillera
            semillera = $("input#buscarTXT").val();
            if (semillera.length > 0) {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    estado : 1,
                    semillera : semillera
                }, function(json) {
                    if (json.total>0) {
                        $("#hiddenNoSolicitud").val(json.nosolicitud);
                        $("td.tbl-nosolicitud-fiscal").empty().text(json.nosolicitud);
                        $("td.tbl-semillera-fiscal").empty().text(semillera);
                        $("td.tbl-semillerista-fiscal").empty().text(json.semillerista);
                        $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);
                        $("td.tbl-cultivo-fiscal").empty().text(json.cultivo);
                        $("td.tbl-variedad-fiscal").empty().text(json.variedad);
                        $("input#iSolicitud").val(json.isolicitud);
                        $("form.seguimiento").fadeIn();                      
                    } else {
                        //buscar semillerista
                        productor = $("input#buscarTXT").val();
                        $.getJSON("control/index.php", {
                            mdl : 'fiscalizacion',
                            opt : 'ver',
                            pag : 'solicitud_semillera',
                            opc : 'productor',
                            estado : 1,
                            semillerista : productor
                        }, function(json) {
                            if (json.total>0) {
                                $("#hiddenNoSolicitud").val(json.nosolicitud);
                                $("td.tbl-nosolicitud-fiscal").empty().text(json.nosolicitud);
                                $("td.tbl-semillera-fiscal").empty().text(json.semillera);
                                $("td.tbl-semillerista-fiscal").empty().text(productor);
                                $("td.tbl-comunidad-fiscal").empty().text(json.comunidad);
                                $("td.tbl-cultivo-fiscal").empty().text(json.cultivo);
                                $("td.tbl-variedad-fiscal").empty().text(json.variedad);
                                $("input#iSolicitud").val(json.isolicitud);
                                $("form.seguimiento").fadeIn();                  
                            } else {                                
                                    $("div.alert").fadeIn().fadeOut(3000);
                            }
                        });
                    }
                });
            }
        }
    },
    semillaProducidaFiscal : function() {
        temp_nro = $("input#buscarTXT").val();
        nro = parseInt(temp_nro);
        if (Number.isInteger(nro)) {
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'solicitud_semillera',
                opc : 'nro',
                solicitud : nro,
                estado : 11
            }, function(json) {
                if (json.total > 0) {
                    $("#semillera").val(json.semillera).addClass("not-edit").css('readonly', 'readonly');
                    $("#semillerista").val(json.semillerista).addClass("not-edit").css('readonly', 'readonly');
                    $("input#comunidad").val(json.comunidad);
                    $("#iSolicitud").val(json.isolicitud);
                    $("#hiddenNoSolicitud").val(nro);
                    $("form.seguimiento").fadeIn();
                    $.getJSON("control/index.php", {
                        mdl : 'fiscalizacion',
                        opt : 'buscar',
                        pag : 'icosecha',
                        cos : json.isolicitud
                    }, function(json) {
                        $("input#icosecha").val(json.id_cosecha);
                    });
                } else {
                    $("div.alert").fadeIn().fadeOut(3000);
                }
            });
        } else {
            //buscar semillera
            semillera = $("input#buscarTXT").val();
            if (semillera.length > 0) {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'solicitud_semillera',
                    opc : 'semillera',
                    semillera : semillera,
                    estado : 11
                }, function(json) {
                    if (json.total >= 2) {
                        var html = '';
                        $.each(json.nosolicitud, function(index, nro) {
                            html += "<tr>";
                            html += "<th scope='row'>";
                            html += "<input type='radio' value='" + json.nosolista[index] + "' class='" + index + "' name='nro'/>";
                            html += "</th> ";
                            html += "<td class='" + index + "'>" + json.semillerista[index];
                            html += "</td>";
                            html += "<td>" + json.comunidad[index] + "</td>";
                            html += "</tr>";
                        });
                        //agregar solicitudes a dialogo
                        $(".lst-nosolicitud-certifica>.table>tbody").empty().append(html);
                        $('#nosolicitudescertifica label.slr>span').empty().append(semillera.toUpperCase());
                        $('#nosolicitudescertifica label.nosoli>span').empty().append(json.nosolicitud[0]);
                        //cuadro de dialogo
                        $('#nosolicitudescertifica').dialog({// Dialog
                            title : 'Números de solicitudes',
                            dialogClass : "no-close",
                            resizable : false,
                            height : 400,
                            buttons : {
                                "Aceptar" : function() {
                                    var nro = $("input#nosolicitud").val();
                                    var semillerista = $("input#semillerista").val();
                                    var semillera = $("input#semillera").val();
                                    $.funciones.buscarComunidadBy(nro, semillera, semillerista);
                                    $("input#nosolicitud,input#semillerista").addClass('not-edit');
                                    $(this).dialog("close");
                                    $("form.seguimiento").fadeIn();
                                },
                                "Cancelar" : function() {
                                    $("input#nosolicitud,input#semillerista").empty();
                                    $(this).dialog("close");
                                }
                            }
                        });
                        //copiar datos seleccionados
                        $('input:radio[name=nro]').on('click', function() {
                            valor = $('input:radio[name=nro]:checked').val();
                            temp = valor.split('=');
                            $("input#nosolicitud").val(temp[0]);
                            $("input#semillerista").val(temp[1]);
                        });
                    } else if (json.total == 1) {
                        $("#hiddenNoSolicitud").val(json.nosolicitud);
                        $("#nosolicitud").val(json.nosolicitud).addClass("not-edit");
                        $("#semillerista").val(json.semillerista).addClass("not-edit");
                        $("input#comunidad").val(json.comunidad);
                        $("#iSolicitud").val(json.isolicitud);
                        $("input#cultivo").addClass('not-edit').val(json.cultivo);
                        $("form.seguimiento").fadeIn();
                    } else {
                        //buscar semillerista
                        productor = $("input#buscarTXT").val();
                        $.getJSON("control/index.php", {
                            mdl : 'fiscalizacion',
                            opt : 'ver',
                            pag : 'solicitud_semillera',
                            opc : 'productor_semilla',
                            estado : 11,
                            semillerista : productor
                        }, function(json) {
                            if (json.total > 0) {
                                $("#hiddenNoSolicitud").val(json.nosolicitud);
                                $("#nosolicitud").val(json.nosolicitud).css('readonly', 'readonly');
                                $("#semillera").val(json.semillera).css('readonly', 'readonly');
                                $("input#semillerista").val(productor).css('readonly', 'readonly');
                                $("input#comunidad").val(json.comunidad);
                                $("#iSolicitud").val(json.isolicitud);
                                $("form.seguimiento").fadeIn();
                            } else {
                                $("div.alert").fadeIn().fadeOut(3000);
                            }
                        });
                    }
                });
            } else {
                $("#hiddenNoSolicitud").val('');
                $("input#comunidad").val('');
                $("#iSolicitud").val('');
                $("#nosolicitud").removeClass("not-edit").val('');
                $("#semillerista").removeClass("not-edit").val('');
                $("form#semilla,div#cultivos,form#semillaP,div.alert").fadeOut();
            }
        }
    },
    cuentaFiscal : function() {
        texto = $('input#buscarTXT').val();
        if (texto.length >= 2) {
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'buscar',
                pag : 'resumen_cuenta',
                slr : texto
            }, function(json) {
                if (json.total >= 2) {
                    $("th#th-campanha").text('Nro Solicitud');
                    $('div#lst-semilleras>div#txtSearch').css('margin-left', '35%');
                    var html = '';
                    $.each(json.isolicitud, function(index, id) {
                        html += "<tr style=\"text-align:center\">";
                        html += "<td scope='row'>";
                        html += "<input type='radio' value='" + json.res_cuenta[index] + "' class='" + index + "' name='nro'/>";
                        html += "</td> ";
                        html += "<td>" + (json.comunidad[index]).replace("$", "'") + "</td>";
                        html += "<td class='" + index + "'>" + json.semillerista[index] + "</td>";
                        html += "<td class='" + index + "'>" + json.nosolicitud[index] + "</td>";
                        html += "<td class='" + index + "'>" + json.cultivo[index] + "</td>";
                        html += "<td class='" + index + "'>" + json.variedad[index] + "</td>";
                        html += "</tr>";
                    });
                    //agregar solicitudes a dialogo
                    $("#lst-semilleras>.tbl-cuenta>.table>tbody.buscar").empty().append(html);
                    $('#lst-semilleras>label.slr>span').empty().text(json.semillera[0]);

                    //cuadro de dialogo
                    $('div#lst-semilleras').dialog({// Dialog
                        title : 'Cuenta de fiscalizacion',
                        resizable : false,
                        height : 400,
                        width : 470,
                        buttons : {
                            "Aceptar" : function() {
                                $("form#cuenta").fadeIn();
                                $(this).dialog('close');
                                $(this).dialog("destroy");
                            },
                            "Cancelar" : function() {
                                $("form#cuenta").fadeOut();
                                $(this).dialog("close");
                            }
                        }
                    });
                    //copiar datos seleccionados
                    $('input:radio[name=nro]').on('click', function() {
                        valor = $('input:radio[name=nro]:checked').val();
                        temp = valor.split('=');
                        $("td.tbl-semillera").text(temp[0]);
                        $("td.tbl-semillerista").text(temp[1]);
                        $("td.tbl-cultivo").text(temp[2]);
                        $("td.tbl-variedad").text(temp[3]);
                        $("input#categoriaAprob").val(temp[4]);
                        var comunidad_name = temp[7];
                        $("td.tbl-comunidad").text(comunidad_name.replace("$", "'"));
                        $("input#iSolicitud").val(temp[5]);
                        $("input#iEstado").val(temp[10]);
                        cargarISuperficie(temp[5], temp[6]);
                    });
                } else if (json.total == 1) {
                    $(".tbl-semillera").empty().text(json.semillera);
                    $(".tbl-semillerista").empty().text(json.semillerista);
                    $(".tbl-cultivo").empty().text(json.cultivo);
                    $(".tbl-variedad").empty().text(json.variedad);
                    $(".tbl-comunidad").empty().text(json.comunidad);
                    $("input#categoriaAprob").empty().val(json.categoria_producir);
                    $("input#iSolicitud").empty().val(json.isolicitud);
                    $("input#iSemilla").empty().val(json.isemilla);
                    $("input#iEstado").empty().val(json.estado);
                    cargarISuperficie(json.isolicitud, json.isemilla);
                    $("form#cuenta").fadeIn();
                } else {
                    $("form#cuenta").fadeOut();
                }

            });
            //funciones complementarias
            function cargarISuperficie(isolicitud, isemilla) {
                var isol = parseInt(isolicitud);
                var isem = parseInt(isemilla);
                //cargar saldo campanha anterior
                $.ajax({
                    url : 'control/index.php',
                    type : 'POST',
                    async : true,
                    dataType : 'json',
                    data : {
                        mdl : 'certificacion',
                        opt : 'saldo_anterior',
                        isol : isol,
                        isem : isem
                    },
                    success : function(data) {
                        $('#campAnterior').val(data.saldo);
                    }
                });
                //cargar datos de la cuenta
                var isup = $('input#iSuperficie').val();
                var std = $('input#iEstado').val();
                $.getJSON('control/index.php', {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'resumenCtav2',
                    iSolicitud : isol,
                    iSemilla : isem,
                    iSuperficie : isup,
                    iEstado : std
                }, function(json) {
                    $('input#categoriaAprob').val(json.catAprobada).text(json.catAprobada);
                    $('input#superficie').val(json.supAprobada).text(json.supAprobada);
                    $('input#superficietot').val(json.supTotal).text(json.supTotal);
                    $('input#inscripcion').val(json.inscripcion).text(json.inscripcion);
                    $('input#icampo').val(json.inspeccion).text(json.inspeccion);
                    $('input#anlet').val(json.analisisEtiqueta).text(json.analisisEtiqueta);
                    $('input#acondicionamiento').val(json.acondicionamiento).text(json.acondicionamiento);
                    $('input#plantines').val(json.plantines).text(json.plantines);
                    $('input#total2,input#saldototal').val(json.total).text(json.total);
                    $('input#bolsa').val(json.etiquetas).text(json.etiquetas);
                });
            }

        }
    },
};
