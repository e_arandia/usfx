	var cache = {};
	var lastXhr;
	$("#semillera").autocomplete({
		minLength : 1,
		source : function(request, response) {
			var term = request.term;
			if( term in cache) {
				response(cache[term]);
				return;
			}
			lastXhr = $.getJSON("control/index.php?mdl=operaciones&pag=semillera&opt=auto&q=" + term, resultado);
			function resultado(data) {
				cache[term] = data;
			};

		}
	});
	$("#provincia").autocomplete({
		minLength : 1,
		source : function(request, response) {
			var term = request.term;
			if( term in cache) {
				response(cache[term]);
				return;
			}
			lastXhr = $.getJSON("control/index.php?mdl=operaciones&pag=lugar&seccion=provincia&q=" + term, resultado);
			function resultado(data) {
				cache[term] = data;
			};

		}
	});

	$("#municipio").autocomplete({
		minLength : 1,
		source : function(request, response) {
			var term = request.term;
			if( term in cache) {
				response(cache[term]);
				return;
			}
			lastXhr = $.getJSON("control/index.php?mdl=operaciones&pag=lugar&seccion=municipio&q=" + term, resultado);
			function resultado(data) {
				cache[term] = data;
			};

		}
	});
	$("#comunidad").autocomplete({
		minLength : 1,
		source : function(request, response) {
			var term = request.term;
			if( term in cache) {
				response(cache[term]);
				return;
			}
			lastXhr = $.getJSON("control/index.php?mdl=operaciones&pag=lugar&seccion=comunidad&q=" + term, resultado);
			function resultado(data) {
				cache[term] = data;
			};

		}
	});
	$("#localidad").autocomplete({
		minLength : 1,
		source : function(request, response) {
			var term = request.term;
			if( term in cache) {
				response(cache[term]);
				return;
			}
			lastXhr = $.getJSON("control/index.php?mdl=operaciones&pag=semillera&opt=auto&q=" + term, resultado);
			function resultado(data) {
				cache[term] = data;
			};

		}
	});