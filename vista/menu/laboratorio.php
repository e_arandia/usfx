<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
<!--
    <div>
        <h3><a id="buscar" href="">Buscar</a></h3>
    </div>
-->
	<div>
		<h3><a id="muestra" href="">Ingresar datos muestra</a></h3>
	</div>
	<div>
        <h3><a id="resultado" href="">Ingresar Resultado laboratorio</a></h3>
    </div>
	<div>
		<h3><a id="vresultado" href="">Ver Resultado laboratorio</a></h3>
	</div>
	<div>
        <h3><a id="general" href=""> Gestiones Anteriores</a></h3>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#menuP").accordion({
			header : "h3"
		});
		$.ajaxSetup({
            url : 'control/index.php',
            method : 'GET',
            success : function(responseText) {
                $(".post").empty().append(responseText);
            }
        });
        $("#buscar").click(function() {
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'campo',
                    opt : 'new'
                },
                beforeSend : function(){
                    $.funciones.mostrarMensaje('wait','Cargando..');
                }
            });
        });
        $("#muestra").click(function() {
            $.ajax({
                data : {
                	mdl : 'laboratorio',
                    pag : 'muestra',
                    opt : 'new',
                    nivel : 3
                },
                beforeSend : function(){
                    $.funciones.mostrarMensaje('wait','Cargando..');
                }
            });
        });
        $("#resultado").click(function() {
            $.ajax({
                data : {
                	mdl : 'laboratorio',
                    pag : 'resultado',
                    opt : 'new'
                }
            });
        });
        $("#vresultado").click(function() {
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'resultados',
                    opt : 'ver',
                    area: 3
                    
                },
                beforeSend : function(){
                    $.funciones.mostrarMensaje('wait','Cargando..');
                }
            });
        });
        $("#general").click(function() {
            //$(".informar").empty();
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'busqueda',
                    pag : 'laboratorio_form',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
		$("a.salir").click(function() {
			$.get("check.php", {
				page : "salir"
			}, function(data) {
				if(data == 'ok')
					setTimeout("location.href = 'home.php'", 100);
			});
		});
	})
</script>