<div>
    <h3><a href="" id="mn_edit_form">Editar</a></h3>
    <div id="opt-edit">
        <ul>
            <li class="li-sol">
                <a id="edtsolicitud">Solicitud</a>
            </li>
            <li class="li-sem">
                <a id="edtsemilla">Semilla Sembrada</a>
            </li>
            <li class="li-sup">
                <a id="edtsuperficie">Superficie</a>
            </li>
            <li class="li-ins">
                <a id="edtinspecciones">Inspeccion</a>
            </li>
            <li class="li-cos">
                <a id="edtcosecha">Hoja de Cosecha</a>
            </li>
            <li class="li-lab">
                <a id="edtlaboratorio">Laboratorio</a>
            </li>
            <li class="li-semP">
                <a id="edtproduccion">Semilla Producida</a>
            </li>
            <li class="li-cta">
                <a id="edtcuentas">Cuenta</a>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#mn_edit_form").click(function() {
            $("#opt-edit").slideToggle("fast");
        });

        $("#edtsolicitud").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'solicitud',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-sol', 'edtsolicitud');
        });
        $("#edtsemilla").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'semilla',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-sem', 'edtsemilla');
        });
        $("#edtsuperficie").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'superficie',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-sup','edtsuperficie');
        });
        $("#edtinspecciones").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'vinspecciones',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-ins','edtinspecciones');
        });
        $("#edtcosecha").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'cosecha',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-cos','edtcosecha');
        });
        $("#edtproduccion").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'produccion',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-semP','edtproduccion');
        });
        $("#edtcuentas").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'certificacion',
                    opt : 'ver',
                    pag : 'cuentas',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-cta','edtcuentas');
        });
        $("#edtlaboratorio").click(function() {
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'laboratorio',
                    opt : 'ver',
                    pag : 'laboratorio',
                    area : 10,
                    edt : 1110
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-lab','edtlaboraorio');
        });
    }); 
</script>