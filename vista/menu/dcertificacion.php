<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
    <div>
        <h3><a id="etapafisc" href="#">Proceso fiscalizaci&Oacute;n</a></h3>
    </div>
    <!--<div>
    <h3><a id="buscar" href="#">buscar</a></h3>
    </div>-->
    <div>
        <h3><a id="resultado" href="#">Resultado muestra laboratorio</a></h3>
    </div>
    <div>
        <h3><a id="vetapa" href="#">Proceso certificaci&Oacute;n</a></h3>
    </div>
    <!--<div>
    <h3><a id="buscar" href="#">Buscar</a></h3>
    </div>-->
    <div>
        <h3><a href="" id="mn_nuevo_form">Nueva</a></h3>
        <div id="opt_nuevo">
            <ul>
                <li>
                    <a id="solicitud">Solicitud</a>
                </li>
                <li>
                    <a id="semilla">Semilla Sembrada</a>
                </li>
                <li>
                    <a id="superficie">Superficie</a>
                </li>
                <li>
                    <a id="inspeccion" >Inspeccion</a>
                </li>
                <li>
                    <a id="cosecha" >Hoja de Cosecha</a>
                </li>
                <li>
                    <a id="laboratorio">Laboratorio</a>
                </li>
                <li>
                    <a id="produccion" >Semilla Producida</a>
                </li>
                <li>
                    <a id="cuenta">Cuenta</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <h3><a href="#" id="mn_ver_form">Ver</a></h3>
        <div id="opt_ver">
            <ul>
                <li>
                    <a id="vsolicitud">Solicitud</a>
                </li>
                <li>
                    <a id="vsemilla">Semilla Sembrada</a>
                </li>
                <li>
                    <a id="vsuperficie">Superficie</a>
                </li>
                <li>
                    <a id="vinspecciones">Inspeccion</a>
                </li>
                <li>
                    <a id="vcosecha">Hoja de Cosecha</a>
                </li>
                <li>
                    <a id="vlaboratorio">Laboratorio</a>
                </li>
                <li>
                    <a id="vproduccion">Semilla Producida</a>
                </li>
                <li>
                    <a id="vcuentas">Cuenta</a>
                </li>
            </ul>
        </div>
    </div>
    <?php
    if ($_SESSION['usr_edt'] == 1110) {
        include 'mn_editar_cer.php';
    }
    ?>
    <div>
        <h3><a href="#" id="mn_info_form">Informes</a></h3>
        <div id="opt_info">
            <ul>
                <li>
                    <a id ="resumen">Resumen</a>
                </li>
                <li>
                    <a id ="graficos">Graficos</a>
                </li>
                <li>
                    <a id ="acreedores">Acreedores</a>
                </li>
                <li>
                    <a id="gAnteriores">Detalle certificaci&oacute;n</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <h3><a href="#" id="general"> Buscar</a></h3>
    </div>
    <div>
        <h3><a href="#" id="import">Importar</a></h3>
    </div>
</div>
<div>
    <input class="usistem" type="hidden" value="<?php echo $_SESSION['usrID'] ?>" />
</div>
<!-- menu nuevo y salir del sistema-->
<script type="text/javascript" >
    $(document).ready(function() {
        $("#menuP").accordion();
        /*$("#menuP").accordion({
            header : "h3"
        });*/
        var id = $(".usistem").val();

        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'mn_editar',
            id : id
        }, function(json) {
            if (json.resp === 1110) {
                $(".edit").show();
            } else {
                $(".edit").empty().hide();
            }
        });
        $("#mn_nuevo_form").click(function(){
            $("#opt_nuevo").slideToggle("slow");
        });
        $("#mn_ver_form").click(function(){
            $("#opt_ver").slideToggle("slow");
        });
        $("#mn_info_form").click(function(){
            $("#opt_info").slideToggle("slow");
        });

        $.ajaxSetup({
            url : 'control/index.php',
            method : 'GET',
            success : function(responseText) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(responseText);

            }
        });
        $("#gAnteriores").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'fcampanhas',
                    opt : 'detalle'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        $("#busquedaGeneral").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'fBusquedaGeneral'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        $("#general").click(function() {
            //$(".informar").empty();
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'busqueda',
                    pag : 'buscar',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#import").click(function() {
            //$(".informar").empty();
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'importar',
                    pag : 'form_import',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#solicitud").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'solicitud',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('solicitud'),10);            
        });
        $("#semilla").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'semilla',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });            
            setTimeout($.funciones.cargarImagen('semilla'),10);
        });
        $("#superficie").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'superficie',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });            
            setTimeout($.funciones.cargarImagen('superficie'),10);
        });
        $("#inspeccion").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'inspeccion',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });            
            setTimeout($.funciones.cargarImagen('inspeccion'),10);
        });
        $("#cosecha").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'cosecha',
                    opt : 'new'

                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('cosecha'),10);
        });
        $("#laboratorio").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'muestra',
                    opt : 'new',
                    nivel : 1
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('laboratorio'),10);
        });
        $("#produccion").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'semillaP',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('semillaP'),10);
        });
        $("#cuenta").click(function() {
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'cuenta',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('cuenta'),10);
        });
        $("#vetapa").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                //url : "control/index.php?mdl=certificacion&opt=new&pag=buscar",
                url : "control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion",
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
    });

</script>
<!-- editar datos-->
<?php
/**
 * 10 administracion
 * 1  certificacion
 * 2 fiscalizacion
 * */

switch ($_SESSION['usr_nivel']) {
    case 1 :
        if ($_SESSION['usr_edt'] == 1110) {
            include 'editar_cer.php';
        } else {
            include 'noneditar_cer.php';
        }
        break;
}
?>
<!-- menu ver-->
<script type="text/javascript">
    $(document).ready(function() {
        $("#vsolicitud").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vsemilla").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilla',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vsuperficie").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'superficie',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vinspecciones").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'inspecciones',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            ;
        });
        $("#vcosecha").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cosecha',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vproduccion").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'produccion',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vcuentas").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cuentas',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        $("#vlaboratorio").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'laboratorio',
                opt : 'ver',
                pag : 'laboratorio',
                area : 1
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
    }); 
</script>
<!-- menu informes-->
<script>
    $(document).ready(function() {
        $("#resumen").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'informe',
                opt : 'resumen',
                pag : 'certifica'
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
        $("#graficos").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'usuario',
                pag : 'grafico'
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
        $("#acreedores").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'usuario',
                pag : 'acreedores'
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
    });
</script>