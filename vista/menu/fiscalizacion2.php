<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
    <div>
        <h3><a id="etapafisc" href="#">Proceso fiscalizaci&Oacute;n</a></h3>
    </div>
    
    <div>
        <h3><a id="vsolicitudfis" href="#">Solicitud</a></h3>
        <!--<h3><a id="" href="#">Solicitud</a></h3>-->
    </div>
    <div>
        <h3><a id="vsemillafis" href="#">Semilla</a></h3>
    </div>
    <div>
        <h3><a id="vcosechafis" href="#">Hoja de Cosecha</a></h3>
    </div>
    <div>
        <h3><a id="laboratoriofis" href="#">Muestra laboratorio</a></h3>
    </div>
    <div>
        <h3><a id="vlaboratoriofis" href="#">Ver Laboratorio</a></h3>
    </div>
    <div>
        <h3><a id="resultado" href="#">Resultado muestra laboratorio</a></h3>
    </div>
    <div>
        <h3><a id="vproduccionfis" href="#">Semilla Producida</a></h3>
    </div>
    <div>
        <h3><a id="vcuentasfis" href="#">Cuenta</a></h3>
    </div>
<div>
        <h3><a href="#" id="mn_info_fisc">Informes</a></h3>
        <div id="opt_info">
            <ul>
                <li class="li-resf">
                    <a id ="resumen">Resumen</a>
                </li>
                <li class="li-graf">
                    <a id ="graficos">Graficos</a>
                </li>
                <!--
                <li class="li-acrf">
                <a id ="acreedores">Acreedores</a>
                </li>
                -->
            </ul>
        </div>
    </div>
    <?php
    if ($_SESSION['usr_edt'] == 1110) {
        include 'mn_editar_fisc.php';
    }
    ?>

</div>
<div>
    <input class="usistem" type="hidden" value="<?php echo $_SESSION['usrID'] ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menuP").accordion({
            header : "h3"
        });
        var id = $(".usistem").val();
        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'mn_editar',
            id : id
        }, function(json) {
            if (json.resp == 1110) {
                $(".edit").show();
            } else {
                $(".edit").empty().hide();
            }
        });
        $("#mn_ver_form_fisc").click(function() {
            $("#opt_ver").slideToggle("slow");
        });
        $("#mn_info_fisc").click(function() {
            $("#opt_info").slideToggle("slow");
        });
        $.ajaxSetup({
            url : 'control/index.php',
            method : 'GET',
            success : function(responseText) {
                $.funciones.ocultarMensaje(500);
                $("div.div-new").empty().append(responseText);
            }
        });
        $("#buscar").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'campo',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        $("#resultado").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'pruebas',
                    opt : 'ver',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        //FORMULARIOS
        $("#solicitudfis").click(function() {

            $(".informar").slideUp("fast");
            $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
            $(".informar").slideDown("fast");
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'solicitud',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('solicitudfis', 'fiscalizacion');
            });
            /*
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-solf', 'solicitudfis');
           */
        });
        $("#semillafis").click(function() {
            $(".informar").slideUp("fast");
            $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
            $(".informar").slideDown("fast");
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'semilla',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('semillafis', 'fiscalizacion');
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-semf', 'semillafis');
        });
        $("#cosechafis").click(function() {
            $(".informar").slideUp("fast");
            $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
            $(".informar").slideDown("fast");
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'cosecha',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('cosechafis', 'fiscalizacion');
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-cosf', 'cosechafis');
        });
        $("#produccionfis").click(function() {
            $(".informar").slideUp("fast");
            $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
            $(".informar").slideDown("fast");
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'semillaP',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('semillaPfis', 'fiscalizacion');
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-prodf', 'produccionfis');
        });
        $("#cuentafis").click(function() {
            $(".informar").slideUp("fast");
            $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
            $(".informar").slideDown("fast");
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'cuenta',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('cuentafis', 'fiscalizacion');
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-ctaf', 'cuentafis');
        });
        $("#laboratoriofis").click(function() {
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'muestra',
                    opt : 'new',
                    nivel : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
    }); 
</script>
<!-- editar datos-->
<?php
/**
 * 10 administracion
 * 1  certificacion
 * 2 fiscalizacion
 * */

switch ($_SESSION['usr_nivel']) {
    case 2 :
        if ($_SESSION['usr_edt'] == 1110)
            include 'editar_fisc.php';
        else {
            include 'noneditar_fisc.php';
        }
        break;
}
?>
<!-- menu ver datos-->
<script type="text/javascript">
    $(document).ready(function() {
        $("#vsolicitudfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'solicitud',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vsolf', 'vsolicitudfis');
        });

        $("#vsemillafis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'semilla',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vsemf', 'vsemillafis');
        });
        $("#vcosechafis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'cosecha',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vcosf', 'vcosechafis');
        });
        $("#vproduccionfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'produccion',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vprodf', 'vproduccionfis');
        });
        $("#vcuentasfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'cuentas',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vctaf', 'vcuentasfis');
        });
        $("#vlaboratoriofis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'laboratorio',
                    opt : 'ver',
                    pag : 'resultados',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vlabf', 'vlaboratoriofis');
        });
        $("#resumen").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'informe',
                    opt : 'resumen',
                    pag : 'fiscaliza'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    var html;
                    if (data !== 'error') {
                        html = "<a href='" + data + "'>RESUMEN FISCALIZACION </a>";
                        $(".post").empty().append(html);
                    } else {
                        html = "<div id='advertencia' class='ui-widget'>";
                        html += "<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>";
                        html += "<p>";
                        html += "<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>";
                        html += "No existen datos";
                        html += "</p></div></div>";
                        $(".post").empty().append(html);
                    }
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-resf', 'resumen');
        });
        $("#graficos").click(function() {
            $(".informar").empty();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'usuario',
                pag : 'grafico'
            }, function(data) {
                $(".post").empty().append(data);
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-graf', 'graficos');
        });
        $("#etapafisc").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'proceso',
                    sistema : 'fiscaliza'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
        });
    }); 
</script>