<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
    <div>
        <h3><a href="" id="mn_gestionar_form">Gestionar</a></h3>
        <div id="opt-gestionar">
            <ul class="topnav">
                <li class="li-user">
                    <a id="usuarios">Usuarios</a>
                </li>
                <li class="li-record">
                    <a id="bitacora">Bitacora</a>
                </li>
                <li class="li-costo">
                    <a id="costos">Costos</a>
                </li>
                <!--
                <li class="li-allow">
                    <a id="permisos">Permisos</a>
                </li>
            -->
                <li class="li-category">
                    <a id="categoria">Categoria</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <h3><a href="" id="mn_db_form">Base de datos</a></h3>
        <div id="opt-database">
            <ul>
                <li class="li-backup">
                    <a id="respaldar">Respaldar</a>
                </li>
                <li class="li-restore">
                    <a id="restaurar">Restaurar</a>
                </li>
            </ul>
        </div>
    </div>
    <?php
    if ($_SESSION['usr_nivel'] == 10)
        require_once 'edit_admin.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menuP").accordion({
            header : "h3"
        });
        url = 'control/index.php';
        
        $("#mn_gestionar_form").click(function() {
            $("#opt-gestionar").slideToggle("fast");
        });
        $("#mn_db_form").click(function() {
            $("#opt-database").slideToggle("fast");
        });
 
        $("#usuarios").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'login',
                    pag : 'listar'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-user', 'usuarios');
        });
        $("#bitacora").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'usuario',
                    pag : 'bitacora'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-record', 'bitacora');
        });
        $("#costos").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'administracion',
                    pag : 'costos'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-costo', 'costos');
        });
        $("#permisos").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'login',
                    pag : 'permisos'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-allow', 'permisos');
        });
        $("#categoria").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'login',
                    pag : 'categoria'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-catogory', 'cateogria');
        });
        $("#respaldar").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'login',
                    pag : 'nueva_copia'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $(".post").empty().append(data);
                    $.funciones.ocultarMensaje(200);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-backup', 'respaldar');
        });
        $("#restaurar").click(function() {
            $.ajax({
                type : "GET",
                url : url,
                data : {
                    mdl : 'administracion',
                    pag : 'restauracion'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $(".post").empty().append(data);
                    $.funciones.ocultarMensaje(200);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-restore', 'restaurar');
        });
    })
</script>
<!-- menu ver datos-->
<script type="text/javascript">
    $(document).ready(function() {
        $("#vetapa").click(function() {
            $.ajax({
                type : "GET",
                url : 'control/index.php',
                data : {
                    mdl : 'usuario',
                    pag : 'vetapa',
                    sistema : 'certificacion'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $(".post").empty().append(data);
                    $.funciones.ocultarMensaje(200);
                }
            });
        });
    })
</script>