<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
    <div>
        <h3><a id="vetapa" href="#">Proceso certificaci&Oacute;n</a></h3>
    </div>
    <div>
        <h3><a href="#" id="mn_ver_form">Ver</a></h3>
        <div id="opt_ver">
            <ul>
                <li class="li-vsol">
                    <a id="vsolicitud">Solicitud</a>
                </li>
                <li class="li-vsem">
                    <a id="vsemilla">Semilla Sembrada</a>
                </li>
                <li class="li-vsup">
                    <a id="vsuperficie">Superficie</a>
                </li>
                <li class="li-vins">
                    <a id="vinspecciones">Inspeccion</a>
                </li>
                <li class="li-vcos">
                    <a id="vcosecha">Hoja de Cosecha</a>
                </li>
                <li class="li-vlab">
                    <a id="vlaboratorio">Laboratorio</a>
                </li>
                <li class="li-vpro">
                    <a id="vproduccion">Semilla Producida</a>
                </li>
                <li class="li-vcta">
                    <a id="vcuentas">Cuenta</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <h3><a href="#" id="mn_info_form">Informes</a></h3>
        <div id="opt_info">
            <ul>
                <li class="li-res">
                    <a id ="resumen">Resumen</a>
                </li>
                <li class="li-gra">
                    <a id ="graficos">Graficos</a>
                </li>
                <li class="li-dcer">
                    <a id="pCostos">Detalle certificaci&oacute;n, producci&oacute;n y costos</a>
                </li>
                <li class="li-super_prod">
                    <a id="spSemillas">Superficies de producci&oacute;n de semillas</a>
                </li>
                <li class="li-vol_prod">
                    <a id="vpCultivos">Vol&uacute;menes de produccion </a>
                </li>
                <!--
                <li class="li-acree">
                    <a id="acreedores">Acreedores</a>
                </li>
            
            <li class="li-info-visual">
                <a id="nnn">NNNNNNNNNNNNNNN </a>
            </li>-->
            </ul>
        </div>
    </div>
    
    <div>
        <h3><a href="#" id="general"> Gestiones Anteriores</a></h3>
    </div>
    
    <div>
        <h3><a href="#" id="import">Importar</a></h3>
    </div>
</div>
<div>
    <input class="usistem" type="hidden" value="<?php echo $_SESSION['usrID'] ?>" />
    <input class="crud" type="hidden"/>
</div>
<!-- menu nuevo y salir del sistema-->
<script type="text/javascript" >
    $(document).ready(function() {

        $("#menuP").accordion({
            header : "h3"
        });
        var id = $(".usistem").val();
        
        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'mn_editar',
            id : id
        }, function(json) {
            if (json.resp == 1110) {
                $(".crud").val(json.resp);
                $(".edit").show();
            } else {
                $(".crud").val('1100');
                $(".edit").empty().hide();
            }
        });
        $("#mn_ver_form").click(function() {
            $("#opt_ver").slideToggle("fast");
        });
        $("#mn_info_form").click(function() {
            $("#opt_info").slideToggle("fast");
        });

        $.ajaxSetup({
            url : 'control/index.php',
            method : 'GET',
            success : function(responseText) {
                $.funciones.ocultarMensaje(500);
                $("div.div-new").empty().append(responseText);

            }
        });

        $("#busquedaGeneral").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'fBusquedaGeneral'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        $("#general").click(function() {
            //$(".informar").empty();
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'busqueda',
                    pag : 'buscador_form',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#import").click(function() {
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'importar',
                    pag : 'form_import',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#vetapa").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion",
                beforeSend : function() {
                    $(".post").html("<div style='width:60%;margin-top:30%;padding-left:50%'><img src='images/loader_3a.gif '/><a> Cargando...</a></div>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
    });

</script>
<!-- editar datos-->

<!-- menu ver-->
<script type="text/javascript">
    $(document).ready(function() {
        edt = $(".crud").val();
        $("#vsolicitud").click(function() {
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();
            //agregar estilo
            $.funciones.addTextStyle('li-vsol','vsolicitud');
        });
        $("#vsemilla").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilla',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();
            //agregar estilo
            $.funciones.addTextStyle('li-vsem','vsemilla'); 
        });
        $("#vsuperficie").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'superficie',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();              
            //agregar estilo
            $.funciones.addTextStyle('li-vsup','vsuperficie');
        });
        $("#vinspecciones").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'vinspecciones',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            }); 
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-vins','vinspecciones');       
        });
        $("#vcosecha").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cosecha',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-vcos','vcosecha');   
        });
        $("#vproduccion").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'produccion',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-vpro','vproduccion'); 
        });
        $("#vcuentas").click(function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cuentas',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();              
            //agregar estilo
            $.funciones.addTextStyle('li-vcta','vcuentas'); 
        });
        $("#vlaboratorio").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'muestras',
                area : 1,
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-vlab','vlaboratorio');
        });
    }); 
</script>
<!-- menu informes-->
<script>
    $(document).ready(function() {
        $("#resumen").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'informe',
                opt : 'new',
                pag : 'resumen'
            }, function(page) {
                   $(".post").empty().append(page);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-res','resumen'); 
        });
        $("#graficos").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'usuario',
                pag : 'grafico'
            }, function(data) {
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-gra','graficos'); 
        });
        $("#gAnteriores").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    opt : 'new',
                    pag : 'detalle'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-dcer','gAnteriores'); 
        });
        $("#pCostos").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'detalle_costos'
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-dcer','pCostos'); 
        });
        $("#spSemillas").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'superficieProduccion'
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-super_prod','spSemillas'); 
        });
        $("#vpCultivos").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'volumenProduccion'
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-vol_prod','vpCultivos'); 
        });
        /*
        $("#nnn").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'informe',
                opt : 'new',
                pag : 'info_detallada'
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-info-visual','nnn'); 
        });
        */
        $("#acreedores").click(function() {
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'acreedores'
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            //remover opciones seleccionadas anteriores
            $.funciones.removeSelectedOption();            
            //agregar estilo
            $.funciones.addTextStyle('li-acree','acreedores'); 
        });
    })
</script>