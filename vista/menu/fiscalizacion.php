<h2 class="demoHeaders">Menu Principal</h2>
<div id="menuP">
    <div>
        <h3><a id="etapafisc" href="#">Proceso fiscalizaci&Oacute;n</a></h3>
    </div>
    
    <div style="background:#f2eecf none repeat scroll 0 0;margin: 0 0 0 0;border-radius : 12px;">
        <div id="opt_ver">
            <ul>
                <li class="li-vsolf no-title">
                    <a id="vsolicitudfis">Solicitud</a>
                </li>
                <li class="li-vsemf no-title">
                    <a id="vsemillafis">Semilla</a>
                </li>
                <li class="li-vcosf no-title">
                    <a id="vcosechafis">Hoja de Cosecha</a>
                </li>
                <li class="li-vlabf no-title">
                    <a id="vlaboratoriofis">Laboratorio</a>
                </li>
                <li class="li-vprodf no-title">
                    <a id="vproduccionfis">Semilla Producida</a>
                </li>
                <li class="li-vctaf no-title">
                    <a id="vcuentasfis">Cuenta</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <h3><a href="#" id="mn_info_fisc">Informes</a></h3>
        <div id="opt_info">
            <ul>
                <li class="li-resf">
                    <a id ="resumen">Resumen</a>
                </li>
                <li class="li-graf">
                    <a id ="graficos">Graficos</a>
                </li>
                <!--
                <li class="li-acrf">
                    <a id ="acreedores">Acreedores</a>
                </li>
                -->
            </ul>
        </div>
    </div>
        <div>
        <h3><a href="#" id="general"> Gestiones Anteriores</a></h3>
    </div>
    
    <div>
        <h3><a href="#" id="import">Importar</a></h3>
    </div>
    <?php
    if ($_SESSION['usr_edt'] == 1110) {
        include 'mn_editar_fisc.php';
    }
    ?>
    
</div>
<div>
    <input class="usistem" type="hidden" value="<?php echo $_SESSION['usrID'] ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menuP").accordion({
            header : "h3"
        });
        var id = $(".usistem").val();
        $.getJSON('control/index.php', {
            mdl : 'login',
            pag : 'mn_editar',
            id : id
        }, function(json) {
            if (json.resp == 1110) {
                $(".edit").show();
            } else {
                $(".edit").empty().hide();
            }
        });
        $("#mn_ver_form_fisc").click(function() {
            $("#opt_ver").slideToggle("slow");
        });
        $("#mn_info_fisc").click(function() {
            $("#opt_info").slideToggle("slow");
        });
        $.ajaxSetup({
            url : 'control/index.php',
            method : 'GET',
            success : function(responseText) {
                $.funciones.ocultarMensaje(500);
                $("div.div-new").empty().append(responseText);
            }
        });
        $("#buscar").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'campo',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
        $("#resultado").click(function() {
            $(".informar").empty();
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'pruebas',
                    opt : 'ver',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
        });
    }); 
</script>

<!-- menu ver datos-->
<script type="text/javascript">
    $(document).ready(function() {
        $("#vsolicitudfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'solicitud',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vsolf','vsolicitudfis');
        });

        $("#vsemillafis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'semilla',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vsemf','vsemillafis');
        });
        $("#vcosechafis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'cosecha',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vcosf','vcosechafis');
        });
        $("#vproduccionfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'produccion',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vprodf','vproduccionfis');
        });
        $("#vcuentasfis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'cuentas',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vctaf','vcuentasfis');
        });
        $("#vlaboratoriofis").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'new',
                    pag : 'muestras',
                    area : 2
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-vlabf','vlaboratoriofis');
        });
        $("#resumen").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'informe',
                    opt : 'new',
                    pag : 'resumenFiscal'
                },
                success : function(data) {
                    $(".post").empty().append(data);
                                         
                }
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-resf','resumen');
        });
        $("#graficos").click(function() {
            $(".informar").empty();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'usuario',
                pag : 'grafico'
            }, function(data) {
                $(".post").empty().append(data);
            });
            $.funciones.removeSelectedOption();
            $.funciones.addTextStyle('li-graf','graficos');
        });
        $("#etapafisc").click(function() {
            $(".informar").empty();
            $.ajax({
                type : "GET",
                url : "control/index.php",
                data : {
                    mdl : 'fiscalizacion',
                    opt : 'ver',
                    pag : 'proceso',
                    sistema : 'fiscaliza'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(500);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#general").click(function() {
            //$(".informar").empty();
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'busqueda',
                    pag : 'buscador_form',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
        $("#import").click(function() {
            $.ajax({
                type : "get",
                url : 'control/index.php',
                data : {
                    mdl : 'importar',
                    pag : 'form_import',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $(".post").empty().append(data);
                }
            });
        });
    });
</script>