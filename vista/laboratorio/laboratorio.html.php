<h2 class="title">Datos Laboratorio</h2>
<div class="entry">
	<form id="laboratorio">
		<div>
			<label>Nro An&aacute;lisis</label>
			<input id="analisis" name="analisis" type="text"/>
		</div>
		<div>
			<label>Fecha Recepcion</label>
			<input id="f_recepcion" name="recepcion" type="text"  readonly="true"/>
		</div>
		<div>
			<label>Fecha Resultado</label>
			<input id="f_resultado" name="resultado" type="text" readonly="true"/>
		</div>
		<div>
			<label>Numero Campo</label>
			<select size="1" id="campo" name="campo"></select>
		</div>
		<div>
			<label>Lote Nro</label>
			<input id="lote" name="lote" type="text" />
		</div>
		<div>
			<label>Forma de Semilla</label>
			<input id="tipo" name="tipo" type="text" />
		</div>
		<div>
			<label>N&uacute;mero Bolsa</label>
			<input id="bolsa" class="number" name="bolsa" type="text" />
		</div>
		<div>
			<label>Kg. / Bolsa</label>
			<input id="kgbolsa" class="number" name="kgbolsa" type="text" />
		</div>
		<div>
			<label>Total</label>
			<input id="total" class="number" name="total" type="text" value="0"/>
		</div>
		<div>
			<label>Calibre</label>
			<input id="calibre" name="calibre" type="text" />
		</div>
		<div>
			<label>N&uacute;mero Semilla por Kilo</label>
			<input id="semillakg" class="number" name="semillakg" type="text" />
		</div>
		<div>
			<label>%Pureza</label>
			<input id="pureza" class="number" name="pureza" type="text" />
		</div>
		<div>
			<label>%Germinacion</label>
			<input id="germinacion" class="number" name="germinacion" type="text" />
		</div>
		<div>
			<label>%Humedad</label>
			<input id="humedad" class="number" name="humedad" type="text" />
		</div>
		<div>
			<label>Observacion</label>
			<textarea  name="observacion" cols="26" rows="3" ></textarea>
		</div>
		<div>
			<input name="idCosecha" type="hidden" id="idCosecha"/>
			<input name="iSolicitud" type="hidden" id="iSolicitud"/>
		</div>
		<div>
			<input class="button-primary ui-state-default ui-corner-all" name="send" value="Guardar" type="submit" />
		</div>
	</form>
	<?php
	include '../vista/error/aviso.php';
	include '../vista/error/errores.php';
	include '../vista/dialogos/confirmacion.html';
	?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#f_recepcion").calendario('f_recepcion');
		$("#f_resultado").calendario('f_resultado');

		$.funciones.cargarSelect('campo', 'operaciones', 'campoLab', '', '', '', '');

		$('#campo').change(function() {
			var valor = $(this).find(':selected').val();
			if(valor != '') {
				$.funciones.cargarInput('idCosecha', valor, '', '', '', '', '', '', '', 'icosecha');
				$.getJSON('control/index.php?mdl=operaciones&pag=isolnroCampo&campo=' + valor, function(data) {
					$("#iSolicitud").val(data);
				});
			}
		});
	});

</script>
<!-- calculo de total=kg/bolsa*bolsa-->
<script type="text/javascript">
	$(document).ready(function() {
		$("#bolsa").keypress(function(event) {
			$.funciones.numeros();
			if($("#kgbolsa").val() != '') {
				var bolsas = $(this).val();
				var kgBolsa = $("#kgbolsa").val();
				if(kgBolsa != /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(kgBolsa)) {
					var total = parseFloat(bolsas) * parseFloat(kgBolsa);
					$("#total").val(total);
				}
			}
		});
		$("#kgbolsa").keypress(function() {
			$.funciones.numeros();
		}).keyup(function() {
			var bolsas = $("#bolsa").val();
			var kgBolsa = $(this).val();
			var total = parseFloat(bolsas) * parseFloat(kgBolsa);
			$("#total").val(total);
		})
	});

</script>
<!-- envio y validacion de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		$("#laboratorio").enviarLaboratorio();
	})
</script>
