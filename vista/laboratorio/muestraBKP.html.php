<h2 class="title">Muestra de laboratorio</h2>
<div class="entry">
    <form id="muestraForm" class="seguimiento" >
        
        <div>
            <label>Fecha Recepcion</label>
            <input id="f_recepcion" name="recepcion" type="text"  readonly="true" value="<?php echo date('d-m-Y')?>" style="width:65px"/>
        </div>

        <div>
            <label>Muestra de</label>
            <select id="muestra" style="width: 40%">
                <option value="">[Seleccionar]</option>
                <option value="1">Certificacion</option>
                <option value="2">Fiscalizacion</option>
            </select>
        </div>

        <div>
            <label>Numero Campo</label>
            <select id="nrocampo" name="nrocampo" disabled="disabled">
                <option value="">[Seleccione campo]</option>
            </select>
        </div>
        <div id="nroSolicitud" style="display: none">
            <label>Nro. Solcitud</label>
            <input id="nosolicitud" name="solicitud" class="not-edit" type="text" readonly="readonly"/>
        </div>
        <div id="fiscalizacion" style="display: none">
            <label>Semilla</label>
            <select id="select-fiscal" style="width: 40%" name="tipoSemilla">
                <option value="" selected="selected"> [ Seleccione una opcion ] </option>
                <option value="importada">Importada</option>
                <option value="actualizacion">Actualizacion</option>
                <option value="sin_norma">Semilla no cuenta con norma de certificacion</option>
                <option value="con_norma">Semilla con normas especificas</option>
                <option value="uso_propio">Semilla uso propio</option>
                <option value="otro">Otro</option>
            </select>
        </div>

        <div>
            <label>Cultivo</label>
            <input id="cultivo" name="cultivo" class="not-edit" type="text" readonly="readonly"/>
        </div>
        <div>
            <label>Variedad</label>
            <input id="variedad" name="variedad" class="not-edit" type="text" readonly="readonly"/>
        </div>
        <div id="sorim"  style="display:none">
            <label>Origen</label>
            <input id="origen" name="origen" type="text" autocomplete="off"/>
        </div>
        <div id="import" style="display: none;height: 30em;">
            <div style="float: left; margin-bottom: 1%; clear: left; width: 250px;">
                <label>Especie</label>
                <input id="especie" name="especie" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label>Variedad</label>
                <input id="ivariedad" name="analisis" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label>Categoria</label>
                <input id="categoria" name="categoria" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label>Origen</label>
                <input id="origen" name="origenImp" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label style="width: 235px;">Cantidad</label>
                <input id="cantidad" name="cantidad" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label style="width: 235px;">Certificado fitosanitario</label>
                <input id="certificado" name="certificado" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label style="text-transform:none;width: 235px;">Aduana de ingreso</label>
                <input id="aduanaingreso" name="aduanaingreso" type="text"/>
            </div>
            <div style="float: left;width: 250px;">
                <label style="width: 235px;">Destino semilla</label>
                <input id="datossemilla" name="datossemilla" type="text"/>
            </div>
            <div style="float: left;margin-bottom: 1%; clear: left; width: 250px;">
                <label style="text-transform:none;width: 235px;">Area de distribucion y/o comercializaci&oacute;n</label>
                <input id="adistribucion" name="adistribucion" type="text"/>
            </div>
        </div>

        <div >
            <fieldset>
            <label>TIPO DE AN&Aacute;LISIS</label>

            <input type="checkbox" id="germinacion" name="germinacion" value="1" style="width: 3%"/>
            <label for="germinacion" style="width: 13%">Germinacion</label>

            <input type="checkbox" id="pureza" name="pureza" value="1" style="width: 3%"/>
            <label for="pureza" style="width: 10%">Pureza</label>

            <input type="checkbox" id="humedad" name="humedad" value="1" style="width: 3%"/>
            <label for="humedad" style="width: 10%">Humedad</label>
            </fieldset>
        </div>
        
        <div id="test">
            <input id="insp" name="insp" type="hidden" value="1"/>
            <input id="mdl" name="mdl" type="hidden" value="laboratorio"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="muestra"/>

        </div>
        <div style="float:right; margin-top: -10%;display: none" id="btn-muestra">

            <button id="enviar-muestra" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>

    </form>
    <?php
    include '../vista/error/errores.php';
?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(":checkbox").checkboxradio();

        //calendario
        $("#f_recepcion").calRecibir('f_recepcion');

        $("#f_recepcion").css('margin-right', '10px');
        $("#errores").css('padding-top', '3em');

        //cargar muestras de campos
        $.funciones.cargarMuestrasCampos();
        //cargar lista de tipos de semilla
        $.funciones.cargarTiposSemilla('select-fiscal');
        $("select#select-fiscal").selectmenu();
        //mostrar | ocultar opcion segun tipo de muestra [ certificacion | fiscalizacion ]
        $("select#muestra").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#muestra").on("selectmenuchange", function(event, ui) {
            valor = $(this).val();
            console.log(valor);
            if (valor == 1) { //certificacion
                $.getJSON('control/index.php', {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'lista_campos',
                    sistema : 1
                }, function(json) {
                    if (json.msg != 'error') {
                        //console.log(json.length));
                        $.each(json.campos, function(index, value) {
                            if (value) {
                                $("select#nrocampo").append('<option value="' + value + '">' + value + '</option>');

                            } else {
                                $("select#nrocampo").empty().append('<option value="">No existen muestras</option>');
                                $("select#muestra").selectmenu({
                                    disabled : true
                                });
                                $("select#nrocampo").selectmenu({
                                    disabled : true
                                });
                                $("select#nrocampo").selectmenu("refresh");

                            }
                        });
                    } else {
                        $("select#nrocampo").selectmenu({
                            disabled : true
                        });
                    }
                });
                $("select#nrocampo").selectmenu({
                    disabled : false
                });
            } else if (valor == 2) {
                alert('cargando campos de fiscalizacion');
                $("select#nrocampo").selectmenu({
                    disabled : false
                });
            } else {
                $.funciones.noAnimarOcultarCampo("div", "btn-muestra");
                $.funciones.noAnimarOcultarCampo("div", "import");
                $.funciones.vaciarCampoTexto("cultivo");
                $.funciones.vaciarCampoTexto("variedad");
                $("select#nrocampo").selectmenu({
                    disabled : true
                });
            }
        });
        $("select#nrocampo").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#nrocampo").on("selectmenuchange", function(event, ui) {
            var nroCampo = $("select#nrocampo").val();
            if (nroCampo != "") {
                $("div#btn-muestra").show();
                //cargar muestra campo
                $.funciones.cargarMuestraCampos(nroCampo);
            } else {
                $("div#btn-muestra").hide("fast");
            }
        });
        $("select#muestra").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#muestra").on("selectmenuchange", function(event, ui) {
            var muestra = $("select#muestra").val();
            if (muestra == 2) {
                $.funciones.animarMostrarCampo("div", "fiscalizacion");
            } else {
                $.funciones.animarOcultarCampo("div", "fiscalizacion");
            }
        });
        
        
    }); 
</script>
<script>
    $(document).ready(function() {
        $("select#fiscalizacion").selectmenu({
            width : 250
        });
        $("select#fiscal").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#select-fiscal").on("selectmenuchange", function(event, ui) {
            var opcion = $(this).find(":selected").text();
            if (opcion == "Importada") {
                $.funciones.animarMostrarCampo('div','import');
                $.funciones.noAnimarOcultarCampo('div','sorim');
                $("input#germinacion").removeAttr("checked");
                $(":checkbox").checkboxradio("refresh");
            } else if(opcion == "Actualizacion"){
                $.funciones.animarOcultarCampo('div','import');
                $("input#germinacion").attr("checked","checked");
                $(":checkbox").checkboxradio("refresh");
            }else {
                $.funciones.animarOcultarCampo('div','import');
                $.funciones.noAnimarMostrarCampo('div','sorim');
                $("input#germinacion").removeAttr("checked");
                $(":checkbox").checkboxradio("refresh");
            }
            
        });
        //envio de datos de la muestra
        $("#muestraForm").enviarMuestra();
    })
</script>