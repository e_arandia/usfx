<h2>Resultados de laboratorio</h2>
<div id="bx_resultado">
    <?php
    include_once 'form_resultado.html.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {


        //colorear celdas
        $("div.body >div.cultivo:odd,div.body >div.recepcion:odd,div.body >div.analisis:odd,div.body >div.nocampo:odd,div.body >div.noanalisis:odd,div.body >div.nSolicitud:odd").css('background-color', '#edffcb');
        $("div.body >div.analab:odd,div.body >div.nolote:odd,div.body >div.germinacion:odd,div.body >div.pureza:odd,div.body >div.humedad:odd,div.body >div.observ:odd").css('background-color', '#edffcb');
        
    })
</script>