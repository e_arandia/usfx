<h2 class="title">Muestra de laboratorio</h2>
<div class="entry">
	<div style="float: right; margin-top: -10%;">
		<button id="return" class="btn btn-success" type="button">
			<span class="glyphicon glyphicon-arrow-left"> </span>
			Volver
		</button>
	</div>
	<form id="muestraForm" class="seguimiento" >

		<div>
			<label>Fecha Recepcion</label>
			<input id="f_recepcion" name="recepcion" type="text"  readonly="true" value="<?php echo date('d-m-Y')?>" style="width:65px"/>
		</div>
		<div>
			<label>Muestra de</label>
			<select id="muestra" style="width: 40%">
				<option value="">[Seleccionar]</option>
				<option value="certificacion">Certificaci&oacute;n</option>
				<option value="fiscalizacion">Fiscalizaci&oacute;n</option>
			</select>
		</div>
		<div>
			<label>Numero Campo</label>
			<select id="nrocampo" name="nrocampo" disabled="disabled">
				<option value="">[Seleccione campo]</option>
			</select>
		</div>
		<div id="nroSolicitud" style="display: none">
			<label>Nro. Solcitud</label>
			<input id="nosolicitud" name="solicitud" class="not-edit" type="text" readonly="readonly"/>
		</div>
		<div id="fiscalizacion" style="display: none">
			<label>Semilla</label>
			<select id="select-fiscal" style="width: 40%">
				<option value=""> [Seleccione una opcion] </option>
				<option value="importada">Importada</option>
				<option value="actualizacion">Actualizacion</option>
				<option value="sin_norma">Semilla no cuenta con norma de certificacion</option>
				<option value="con_norma">Semilla con normas especificas</option>
				<option value="uso_propio">Semilla uso propio</option>
				<option value="otro">Otro</option>
			</select>
		</div>

		<div id="sorim-1" style="display: none">
			<label>Cultivo</label>
			<input id="cultivo" name="cultivo" class="not-edit" type="text" readonly="readonly"/>
		</div>
		<div id="sorim-2" style="display: none">
			<label>Variedad</label>
			<input id="variedad" name="variedad" class="not-edit" type="text" readonly="readonly"/>
		</div>
		<div id="sorim-3" style="display: none">
			<label>Origen</label>
			<input id="origen" class="not-edit" name="origen" type="text" autocomplete="off"/>
		</div>
		<div id="import" style="height: 30em;display:none;">
			<div style="float:left;width:49%;">
				<div style="float: left; margin-bottom: 1%; clear: left; width: 250px;">
					<label>Especie</label>
					<input id="especie" name="especie" type="text">
				</div>
				<div style="float: left; width: 250px;">
					<label>Variedad</label>
					<input id="ivariedad" name="analisis" type="text">
				</div>
				<div style="float: left; width: 250px;">
					<label>Categoria</label>
					<input id="categoria" name="categoria" type="text">
				</div>
				<div style="float: left; width: 250px;">
					<label>Origen</label>
					<input id="origen" name="origenImp" type="text">
				</div>
			</div>
			<div style="float:right;width:50%;">
				<div style="float: right; width: 250px;">
					<label style="width: 235px;">Cantidad</label>
					<input id="cantidad" name="cantidad" type="text">
				</div>
				<div style="width: 250px; float: right; margin-top: 1%;">
					<label style="width: 235px;">Certificado fitosanitario</label>
					<input id="certificado" name="certificado" type="text">
				</div>
				<div style="width: 250px; float: right;">
					<label style="text-transform:none;width: 235px;">Aduana de ingreso</label>
					<input id="aduanaingreso" name="aduanaingreso" type="text">
				</div>
				<div style="width: 250px; float: right;">
					<label style="width: 235px;">Destino semilla</label>
					<input id="datossemilla" name="datossemilla" type="text">
				</div>

			</div>
			<div style="margin-bottom: 1%; clear: left; width: 250px;">
				<label style="text-transform:none;width: 235px;">Area de distribucion y/o comercialización</label>
				<input id="adistribucion" name="adistribucion" type="text">
			</div>

		</div>

		<div id="radio" class="analisis" style="display: none">
				<label>TIPO DE AN&Aacute;LISIS</label>

				<input type="checkbox" id="germinacion" name="germinacion" value="1" style="width: 3%"/>
				<label for="germinacion" style="width: 13%">Germinacion</label>

				<input type="checkbox" id="pureza" name="pureza" value="1" style="width: 3%"/>
				<label for="pureza" style="width: 10%">Pureza</label>

				<input type="checkbox" id="humedad" name="humedad" value="1" style="width: 3%"/>
				<label for="humedad" style="width: 10%">Humedad</label>
		</div>

		<div id="test">
			<input id="insp" name="insp" type="hidden" value="1"/>
			<input id="mdl" name="mdl" type="hidden" value="laboratorio"/>
			<input id="opt" name="opt" type="hidden" value="guardar"/>
			<input id="pag" name="pag" type="hidden" value="muestra"/>
			<input name="sistema" type="hidden" value="certificacion"/>

		</div>
		<div style="float:right;display: none"id="btn-muestra">

			<button id="enviar-muestra" class="btn btn-success" type="button">
				<span class="glyphicon glyphicon-floppy-disk"></span>
				Registrar
			</button>
		</div>

	</form>
    <?php
        include '../vista/error/errores.php';
    ?>
</div>

<!-- inicializacion de variables -->
<script type="text/javascript">
    $(document).ready(function(){        
        $("#f_recepcion").calRecibir('f_recepcion');// calendario        
        $("select#nrocampo").selectmenu();//nro de campo
        $(":checkbox").checkboxradio(); //tipo de analisis
    });
</script>

<!-- acciones segun opciones -->
<script type="text/javascript">
    $(document).ready(function(){
        $("select#muestra").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#muestra").on("selectmenuchange", function(event, ui) {
            var muestra_val = $("select#muestra").val();
            if (muestra_val == 'fiscalizacion'){//fiscalizacion  mostrar opciones
                $.funciones.cargarCampos('fiscalizacion', 'nrocampo', muestra_val);
                
            }else if (muestra_val == 'certificacion'){//certificacion mostrar opciones
                $("div#fiscalizacion").fadeOut();
                $.funciones.cargarListaMuestras(1);
                
            }else if(muestra_val == ""){// ocultar opciones
                $("form#muestraForm").css('height','');
                $("div#sorim-1,div#sorim-2,div#sorim-3,div#btn-muestra").fadeOut();
                $("div.analisis,div#fiscalizacion").fadeOut();
                $("select#nrocampo").empty().append('<option value="">[ Seleccione campo ]</option>').attr('disabled','disabled');
                $("select#nrocampo").selectmenu("refresh");
            }
        });
        
        $("select#select-fiscal").on("selectmenuchange", function(event, ui) {
            var opcion = $(this).find(":selected").text();
            if (opcion == "Importada") {
                $.funciones.animarMostrarCampo('div','import');
                $.funciones.noAnimarOcultarCampo('div','sorim');
                $('form#muestraForm').css('height','50em');
            } else if(opcion == "Actualizacion"){
                $.funciones.animarOcultarCampo('div','import');
                $('form#muestraForm').css('height','19em');
            }else {
                $.funciones.animarOcultarCampo('div','import');
                $.funciones.noAnimarMostrarCampo('div','sorim');
                $('form#muestraForm').css('height','19em');
            }
            
        });
        //carga de datos segun seleccion de numero de campo
        $("select#nrocampo").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#nrocampo").on("selectmenuchange", function(event, ui) {
            var nroCampo = $("select#nrocampo").find(':selected').val();
            var muestra = $('select#muestra').find(':selected').val();            
            if (muestra=='certificacion'){
                $.funciones.buscarDatosCampoForLaboratorio(nroCampo);
            }else{
                if (nroCampo != '')
                    $.funciones.cultivoVariedadFiscal('fiscalizacion', nroCampo);
            }
            if (nroCampo){
                $('div#btn-muestra').fadeIn();
            }else{
                $('div#btn-muestra').fadeOut();
            }
            if (muestra == 'fiscalizacion'){
                $("form#muestraForm").css('height','19em');
                $("div#sorim-1,div#sorim-2,div#sorim-3").fadeOut();
                $("div#fiscalizacion").fadeIn();
                $("select#select-fiscal").selectmenu();
                $("div.analisis").fadeIn();
            }else{
                $("form#muestraForm").css('height','25em');
                $("div#sorim-1,div#sorim-2,div#sorim-3").fadeIn();
                $("div.analisis").fadeIn();
            }
        });
        $('button#enviar-muestra').on('click',function(){
            var text_selected = $('select#muestra').find(':selected').val();
            var nro_campo = $('select#nrocampo').find(':selected').val();
            if (text_selected == 'certificacion' && nro_campo != ''){
                $.funciones.enviarMuestraCertifica();
            }else if (text_selected == 'fiscalizacion' && nro_campo != ''){
                $.funciones.enviarMuestraFiscal();
            }else{
                alert('Debe seleccionar una opcion');
            }
        });
    });
</script>