<h2>Pruebas de laboratorio (Resultados)</h2>
<div class="entry" style="margin-bottom: 25px;">
    <form id="buscar">
        <div>
            <label>Numero de campo</label>
        </div>
        <div>
            <input id="search-text" type="text" name="buscar" autocomplete="off" placeholder="escriba numero campo. Ej. 440/16">
        </div>
        <!--
        <div style="left: 36em; position: relative; top: -3.8em;">
            <button id="buscar-campo" class="btn btn-success">
                <span class="glyphicon glyphicon-search"></span>
                Buscar
            </button>
        </div>
    -->
    </form>
</div>
<?php
include '../vista/error/errores.php';
?>
<div id="calendar" style="width: 102%" style="display: none"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $("input#search-text").css({
            'margin-bottom' : '0',
            'margin-left' : '0'
        });
        //colorear celdas
        var celdas = ['nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'localidad', 'comunidad', 'fecha', 'options'];
        $.funciones.alternarColores(celdas);
        //envio de datos

    })
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /**buscar campo*/
        $("#search-text").keyup(function(event) {

            var nro = $(this).val();
            if (nro != '') {
                console.log(event.which + '=' + nro);
                $("#errores").hide();
                $.post('control/index.php', {
                    mdl : 'laboratorio',
                    opt : 'ver',
                    pag : 'listapruebas',
                    stm : 2,
                    nro : nro

                }, function(showResponse) {
                    if (showResponse != 'Error')
                        $("#calendar").empty().prepend(showResponse);
                    else {
                        $("span.mensaje").append("No se encuentra el numero de campo seleccionado");
                        $("#errores").show();
                    }
                });

            }
        });
        // validacion y envio de datos
        $("button#buscar-campo").click(function() {
            var nro = $("#search-text").val();

            $("#buscar-campo").submit(function() {
                $(this).buscarResultados(nro)
            });
        });

    })
</script>