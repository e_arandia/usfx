<?php
if (DBConnector::filas()){

?>
<h2>laboratorio</h2>
<?php
$width = ($areaID == 10) ? "'width:146.5%'" : "'width:134.2%'";
?>
<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 3.4em">
		<div class="htitle">
			<label style="width:4.5em;padding-top: 1%;">Nro Analisis</label>
			<label style="width:6.5em;padding-top: 1%;">Fecha Recepccion</label>
			<label style="width:6.5em;padding-top: 1%;">Fecha Resultado</label>
			<label style="width:4.5em;padding-top: 1%;">Nro campo</label>
			<label style="width:7em;padding-top: 1%;">Lote</label>
			<label style="width:7.5em;padding-top: 1%;">Tipo de Semilla</label>
			<label style="width:5.5em;padding-top: 1%;">Numero de bolsas</label>
			<label style="width:6em;padding-top: 1%;">Kg/Bolsa</label>
			<label style="width:4.5em;padding-top: 1%;">Total</label>
			<label style="width:6.5em;padding-top: 1%;">Calibre</label>
			<label style="width:6.5em;padding-top: 1%;">Nro Semillas por Kilo</label>
			<label style="width:4.5em;padding-top: 1%;">Pureza (%)</label>
			<label style="width:7.5em;padding-top: 1%;">Germinacion (%)</label>
			<label style="width:6.5em;padding-top: 1%;">Humedad (%)</label>
			<label style="width:7.5em;padding-top: 1%;">Observacion</label>
			<?php if ($areaID == 10){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php }?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::resultado()){
		?>
		<div class="nSolicitud" style="width:4.5em;">
			<label><?php
			echo $datos -> nro_analisis;
				?></label>
		</div>
		<div class="fecha rec" style="width:6.5em;">
			<label><?php
			echo $dates -> cambiar_formato_fecha($datos -> fecha_recepcion);
				?></label>
		</div>
		<div class="fecha rep" style="width:6.5em;">
			<label><?php
			echo $dates -> cambiar_formato_fecha($datos -> fecha_resultado);
				?></label>
		</div>
		<div class="nSolicitud campo" style="width:4.5em;">
			<label><?php
			echo $datos -> nro_campo;
				?></label>
		</div>
		<div class="lote" style="width:7em;">
			<label><?php
			echo $datos -> nro_lote;
				?></label>
		</div>
		<div class="cultivo tipo" style="width:7.5em;">
			<label><?php
			echo $datos -> tipo_semilla;
				?></label>
		</div>
		<div class="nroEtiqueta nro" style="width:5.5em;">
			<label><?php
			echo $datos -> nro_bolsa;
				?></label>
		</div>
		<div class="nroEtiqueta kg" style="width:6em;">
			<label><?php
			echo $datos -> kg_bolsa;
				?></label>
		</div>
		<div class="sInscrita total" style="width:4.5em;">
			<label><?php
			echo $datos -> total;
				?></label>
		</div>
		<div class="variedad" style="width:6.5em;">
			<label><?php
			echo $datos -> calibre;
				?></label>
		</div>
		<div class="sInscrita semillaKg" style="width:6.5em;">
			<label> <?php
			echo $datos -> nro_semilla_kilo;
				?></label>
		</div>
		<div class="cultivo pur" style="width:4.5em;">
			<label> <?php
			echo $datos -> pureza;
				?></label>
		</div>
		<div class="cultivo ger" style="width:7.5em;">
			<label> <?php
			echo $datos -> germinacion;
				?></label>
		</div>
		<div class="cultivo hum" style="width:6.5em;">
			<label> <?php
			echo $datos -> humedad;
				?></label>
		</div>
		<div class="cAnterior" style="width:7.4em;">
			<label> <?php
			echo $datos -> observacion;
				?></label>
		</div>
		<?php
if ($areaID == 10){
		?>
		<div class="sistema options" style="height: 28px; width: 8.4em;">
			<div>
				<input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_laboratorio;?>" />
			</div>
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_laboratorio;?>" />
			</div>
		</div>
		<?php
		}
		}
		?>
		<input id="idlaboratorio" type="hidden"/>
		<input id="control" type="hidden" value="<?php echo $areaID;?>" />
	</div>
</div>
<?php
}else{
echo "<div id='advertencia' class='ui-widget'>
<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>
<p>
<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>
No existen datos
</p>
</div>
</div>";
}
?>
<script type="text/javascript">
	$(document).ready(function() {
		//$("div.body >div.odd0:odd,div.body >div.odd1:odd,div.body >div.odd2:odd,div.body >div.odd3:odd,div.body >div.odd4:odd,div.body >div.odd5:odd,div.body >div.odd6:odd,div.body >div.odd7:odd,div.body >div.odd8:odd,div.body >div.odd9:odd,div.body >div.odd0:odd").css('background-color', '#f6ffe8');
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#idlaboratorio").val($(this).val());
		}).click(function() {
			if(confirm('Esta seguro de eliminar el registro?')) {
				var id = $("#idlaboratorio").val();
				$.post('control/laboratorio/laboratorio.ctrl.php', {
					page : 'eliminar',
					id : id
				}, function(data) {

					if(data == 'OK') {
						var area = $("#control").val();
						$.post('control/usuario/usuario.ctrl.php', {
							page : 'vlaboratorio',
							area : 'administracion'
						}, function(data) {
							$(".post").empty().append(data);
						});
					}
				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#idlaboratorio").val($(this).val());
		}).click(function() {
			var id = $("#idlaboratorio").val();
			$.post('control/laboratorio/laboratorio.ctrl.php', {
				page : 'actualizar',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
