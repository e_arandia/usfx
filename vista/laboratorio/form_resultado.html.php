<?php
include '../vista/dialogos/busquedaAvanzadaLaboratorio.html';
?>
<div class="div-filter">
<h2>Resultados de laboratorio</h2>
<div id="calendar" style="width: 118%">
    <div class="head" style="height: 2.2em">
        <div class="htitle" >
            <label style="width:7em;padding-top: 1%;">N&uacute;mero An&aacute;lisis</label> 
            <label style="width:7.6em;padding-top: 1%;">Semilla</label>
            <label style="width:7em;padding-top: 1%;">N&uacute;mero Campo</label>
            <label style="width:7em;padding-top: 1%;">Fecha Recepcion</label>                                  
            <label style="width:7em;padding-top: 1%;">Fecha Resultado</label>
            <label style="width:9em;padding-top: 1%;">Nro. Lote</label>
            <label style="width:6.7em;padding-top: 1%;">% Germinaci&oacute;n</label>
            <label style="width:4.5em;padding-top: 1%;">% Pureza</label>
            <label style="width:5.8em;padding-top: 1%;">% Humedad</label>
            <label style="width:12.8em;padding-top: 1%;">Observacion</label>
            <label style="width:6em;padding-top: 1%;">Imprimir PDF</label>
        </div>
    </div>
    <div class="body" style="width: 99.6%">
        <?php
        $i = 0;
        #var_dump($nro_muestras);
        while ($i < $nro_muestras){
            #$lugar_origen = array();$cantidad= array();$certificado = array(); 
        ?>
        <div class="localidad noanalisis" style="width: 7em;">
            <label><?php echo $id_muestra[$i]; ?></label>
        </div>
        <div class="cultivo">
            <label><?php echo $cultivo[$i]; ?></label>
        </div>
        <div class="localidad nocampo" style="width: 7em;">
            <label><?php echo $nro_campo[$i]; ?></label>
        </div>
        <div class="fecha recepcion" style="width:7em">
            <label><?php echo $dates->cambiar_formato_fecha($recepcion[$i]); ?></label>
        </div>
        <div class="anaLab nSolicitud" style="width: 7em;">
            <label><?php echo $dates->cambiar_formato_fecha($resultado[$i]); ?></label>
        </div>
        <div class="lote nolote"  style="width:9em">
            <label><?php echo $lote[$i]; ?></label>
        </div>
        <div class="lote germinacion" style="width: 6.7em">
            <label><?php echo $germinacion[$i]; ?></label>
        </div>
        <div class="lote pureza" style="width: 4.5em;">
            <label><?php echo $pureza[$i]; ?></label>
        </div>
        <div class="lote humedad" style="width: 5.8em;">
            <label><?php echo $humedad[$i]; ?></label>
        </div>
        <div class="lote observ" style="width: 12.7em;">
            <label><?php echo $observacion[$i]; ?></label>            
        </div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $id_muestra[$i]; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
        <?php 
        $i++;
        }
         ?>

    </div>
</div>
<input id="idsolicitud" type="hidden"/>
</div>
<div class="div-new" style="display:none;">
    
</div>   

<script type="text/javascript">
    $(document).ready(function() {
        //calendario
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idsolicitud").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#idsolicitud").val('');
        }).on("click", function() {
            var id = $("input#idsolicitud").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=reslab&id='+id, '_blank');
            win.focus();
        });
        //solo numeros
        $("#rechazada,#retirada").numeric(".");
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'solicitud',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nosolicitud, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label>" + json.nosolicitud[index] + "</label> </div>";
                            div += "<div class=\"sistema sistem\"><label>" + json.sistema[index] + "</label> </div>";
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"dpto\"><label>" + json.departamento[index] + "</label> </div>";
                            div += "<div class=\"prov\"><label>" + json.provincia[index] + "</label> </div>";
                            div += "<div class=\"municipio\"><label>" + json.municipio[index] + "</label> </div>";
                            div += "<div class=\"comunidad\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label>" + json.fecha[index] + "</label> </div>";
                            //div += "<div class=\"sistema options\"><label>" + json.options[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'comunidad', 'fecha', 'options','pdf'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $("div.body").show();
            }
        });
        
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        
        $("button#btn-new").on("click", function() {
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'laboratorio',
                    pag : 'resultado',
                    opt : 'new',
                    nivel: 3
                },
                beforeSend : function() {
                    message = $("<img src='images/loader_6.gif'/><span class='before'> Ingresando datos, por favor espere...</span>");
                    showMessage(message)
                }
            }).done(function(html) {
                $("div.informar").slideDown();
            });
        });
    }); 
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //colorear celdas
        var celdas = ['noanalisis','cultivo','recepcion','analisis','nocampo','nSolicitud','nolote','germinacion','humedad','pureza','observ','pdf'];
        $.funciones.alternarColores(celdas);
        //envio de datos
    })
</script>