<?php
$laboratorio = mysql_fetch_object($datos);
?>
<h2 class="title">Datos Laboratorio</h2>
<div class="entry">
	<form id="laboratorio">
		<div>
			<label>Nro An&aacute;lisis</label>
			<input id="analisis" name="analisis" type="text" value="<?php echo $laboratorio -> nro_analisis;?>"/>
		</div>
		<div>
			<label>Fecha Recepcion</label>
			<input id="f_recepcion" name="recepcion" type="text" value="<?php echo $laboratorio -> fecha_recepcion;?>"/>
		</div>
		<div>
			<label>Fecha Resultado</label>
			<input id="f_resultado" name="resultado" type="text" value="<?php echo $laboratorio -> fecha_resultado;?>"/>
		</div>
		<div>
			<label>Numero Campo</label>
			<input id="campo" name="campo"  value="<?php echo $laboratorio -> nro_campo;?>" />
		</div>
		<div>
			<label>Lote Nro</label>
			<input id="lote" name="lote" type="text"  value="<?php echo $laboratorio -> nro_lote;?>"/>
		</div>
		<div>
			<label>Tipo de Semilla</label>
			<input id="tipo" name="tipo" type="text" value="<?php echo $laboratorio -> tipo_semilla;?>" />
		</div>
		<div>
			<label>N&uacute;mero Bolsa</label>
			<input id="bolsa" name="bolsa" type="text" value="<?php echo $laboratorio -> nro_bolsa;?>" />
		</div>
		<div>
			<label>Kg. / Bolsa</label>
			<input id="kgbolsa" name="kgbolsa" type="text" value="<?php echo $laboratorio -> kg_bolsa;?>" />
		</div>
		<div>
			<label>Total</label>
			<input id="total" name="total" type="text" value="<?php echo $laboratorio -> total;?>"/>
		</div>
		<div>
			<label>Calibre</label>
			<input id="calibre" name="calibre" type="text" value="<?php echo $laboratorio -> calibre;?>" />
		</div>
		<div>
			<label>N&uacute;mero Semilla por Kilo</label>
			<input id="semillakg" name="semillakg" type="text"  value="<?php echo $laboratorio -> nro_semilla_kilo;?>"/>
		</div>
		<div>
			<label>%Pureza</label>
			<input id="pureza" name="pureza" type="text" value="<?php echo $laboratorio -> pureza;?>" />
		</div>
		<div>
			<label>%Germinacion</label>
			<input id="germinacion" name="germinacion" type="text" value="<?php echo $laboratorio -> germinacion;?>"/>
		</div>
		<div>
			<label>%Humedad</label>
			<input id="humedad" name="humedad" type="text"  value="<?php echo $laboratorio -> humedad;?>"/>
		</div>
		<div>
			<label>Observacion</label>
			<textarea  name="observacion" cols="26" rows="3" > <?php echo $laboratorio -> observacion;?></textarea>
		</div>
		<div>
			<input name="idCosecha" type="hidden" id="idCosecha" value="<?php echo $laboratorio -> id_cosecha;?>"/>
		</div>
		<div>
			<input class="button-primary ui-state-default ui-corner-all" name="send" value="Guardar" type="button" />
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        $("#f_recepcion").calendario('f_recepcion');
        $("#f_resultado").calendario('f_resultado');
	});

</script>
<!-- calculo de total=kg/bolsa*bolsa-->
<script type="text/javascript">
	$(document).ready(function() {
		var bolsas = $("#bolsa").val();
		$("#kgbolsa").keyup(function() {
			/*
			if ($(this).val()!= 0 && (bolsas!= 0 || !isNaN(bolsas) ) )
			var total = parseFloat($(this).val()) * parseFloat(bolsas);
			else total=0;*/
			//valor = $("#kgbolsa").val();
			alert('bolsas =>' + parseFloat(bolsas) + ' kgBolsas=>' + $("this").val());
			//$("#total").val(total);
		})
	});

</script>
<!-- envio y validacion de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		$('input:button').click(function() {
			$(this).submit();
			$("#laboratorio").ajaxForm({
				url : 'control/laboratorio/laboratorio.ctrl.php',
				type : 'post',
				data : {
					page : 'analisis'
				},
				resetForm : true,
				beforeSubmit : showRequest,
				success : showResponse
			});
			function showRequest(formData, jqForm, options) {
				// validate signup form on keyup and submit
				$("#laboratorio").validate({
					rules : {
						analisis : {
							required : true
						},
						recepcion : {
							required : true
						},
						resultado : {
							required : true
						},
						campo : {
							required : true
						},
						tipo : {
							required : true
						},
						bolsa : {
							required : true,
							number : true
						},
						lote : {
							required : true,
							minlength : 5
						},
						kgbolsa : {
							required : true,
							number : true,
							minlength : 2
						},
						total : {
							required : true,
							number : true,
							minlength : 2
						},
						calibre : {
							required : true
						},
						semillakg : {
							required : true
						},
						pureza : {
							required : true,
							number : true
						},
						germinacion : {
							required : true,
							number : true
						},
						humedad : {
							required : true,
							number : true
						}
					},
					messages : {
						analisis : {
							required : 'Debe ingresar el número de solicitud'
						},
						recepcion : "Debe seleccionar un sistema",
						resultado : "Escriba el nombre de un departamento",
						campo : "Seleccione numero de campo",
						tipo : "Tipo de semilla",
						bolsa : {
							required : "ingrese numero de bolsa",
							number : 'Debe ingresar un numero'
						},
						lote : "ingrese lote",
						kgbolsa : "Debe ingresar nombre de la provincia",
						total : "Debe ingresar nombre del municipio",
						calibre : "Ingrese calibre ",
						semillakg : "Ingrese cantidad de semillas por kilo",
						pureza : {
							required : 'Debe ingresar el número de solicitud',
							number : 'Debe ingresar un número'
						},
						germinacion : {
							required : 'Debe ingresar el número de solicitud',
							number : 'Debe ingresar un número'
						},
						humedad : {
							required : 'Debe ingresar el número de solicitud',
							number : 'Debe ingresar un número'
						}
					}
				});
			}

			function showResponse(responseText, statusText, xhr, $form) {
				var msg = '<img src="images/error.png" />';
				switch (responseText) {
					case 'error':
						$.unblockUI({
							fadeOut : 1000
						});
						$.blockUI({
							theme : true,
							title : 'Sistema CERFIS',
							message : msg + 'Error. No existen datos',
							timeout : 1000
						});
						break;

					case 'OK' :
						var msg = '<img src="images/check.png" />';
						$.unblockUI({
							fadeOut : 2000
						});
						$.blockUI({
							theme : true,
							title : 'Sistema CERFIS',
							message : msg + 'Los datos fueron guardados',
							timeout : 2000
						});
						$.funciones.cargarSelect('campo', 'operaciones', 'campoLab', '', '', '', '');
						break;
				}
			}

		});
	});

</script>
