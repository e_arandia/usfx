<div class="entry">
    <form id="resultado" class="seguimiento">
        <?php
           $row = DBConnector::objeto();
        ?>
        <div id="fechar">
            <label>Fecha Recepcion</label>
            <input id="fecha" class="info not-edit" name="f_recepcion" type="text" readonly="readonly" value="<?php echo $dates -> cambiar_formato_fecha($row -> fecha_recepcion); ?>"/>
        </div>
        <div>
        <label>Cultivo</label>
        <input id="cultivo" class="info not-edit" name="cultivo" type="text" readonly="readonly" value="<?php echo $row -> cultivo; ?>"/>
        </div>
        <div>
        <label>Variedad</label>
        <input id="variedad" class="info not-edit" name="variedad" type="text" readonly="readonly" value="<?php echo $row -> variedad; ?>"/>
        </div>
        <div>
        <label>Origen</label>
        <input id="origen" class="info not-edit" name="origen" type="text" readonly="readonly" value="<?php echo $row -> origen; ?>"/>
        </div>
    </form>
    <form id="resultado" class="seguimiento"  style="padding:2% 2%">

        <div>
            <label style="width: 20%">Nro An&aacute;lisis</label>
            <input id="analisis" class="not-edit" name="analisis" type="text" value="<?php echo $nro_analisis; ?>" style="width: 5%"/>
        </div>
        <div>
            <label style="width: 20%">Fecha Resultado</label>
            <input id="f_resultado" class="not-edit" name="resultado" type="text" readonly="true" value="<?php echo date('d-m-Y'); ?>" style="width: 25%"/>
        </div>
        <div>
            <label style="width: 20%">Lote Nro</label>
            <input id="lote" class="not-edit" name="lote" type="text" value="<?php echo $lote; ?>" style="width: 25%"/>
        </div>
    </form>
    <form id="importada" class="seguimiento" style="padding:2% 2% 90% 2%">
        <div  style="width: 48%; float: left;">
            <div>
                <label style="width:50%">Cultivo</label>
                <input id="cultivo" name="cultivo" type="text" style="width: 40%" maxlength="20" value="<?php echo $row->cultivo;?>"/>
            </div>
            <div>
                <label style="width:50%">Especie</label>
                <input id="especie" name="especie" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Variedad</label>
                <input id="variedad" name="variedad" type="text" style="width: 40%" maxlength="5" value="<?php echo $row->variedad;?>"/>
            </div>
            <div>
                <label style="width:50%">Categoria</label>
                <input id="categoria" name="categoria" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Lugar | Origen</label>
                <input id="origen" name="origen" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Cantidad</label>
                <input id="cantidad" class="number" name="cantidad" type="text" style="width: 25%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Certificado</label>
                <input id="certificado" class="number" name="certificado" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Aduana Ingreso</label>
                <input id="aduana" name="aduana" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Destino Semilla</label>
                <input id="destino" name="destino" type="text" style="width: 40%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">Distribucion</label>
                <input id="distribucion" name="distribucion" type="text" style="width: 40%" maxlength="5"/>
            </div>
        </div>
        <div style="width: 50%; float: right;">
            <div>
                <label style="text-transform:none;width: 35%">Tipo de Semilla</label>
                <input id="tipo" name="tipo" type="text" style="width: 25%"/>
            </div>
            <div>
                <label style="width: 35%">N&uacute;mero Bolsa</label>
                <input id="bolsa" class="number" name="bolsa" type="text" style="width: 25%"/>
            </div>
            <div>
                <label style="width: 35%">Kg. / Bolsa</label>
                <input id="kgbolsa" class="number" name="kgbolsa" type="text" style="width: 25%"/>
            </div>
            <div>
                <label style="width: 35%">Total</label>
                <input id="total" class="not-edit" name="total" type="text" style="width: 25%"/>
            </div>
            <div>
                <label>Calibre</label>
                <input id="calibre" name="calibre" type="text" />
            </div>
            <div>
                <label style="text-transform:none;width:50%">N&uacute;mero Semilla por Kilo</label>
                <input id="semillakg" class="number" name="semillakg" type="text" style="width: 25%" />
            </div>
            <div id="pureza">
                <label style="width:50%">% Pureza</label>
                <input id="pureza" class="number" name="pureza" type="text" style="width: 25%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">% Germinacion</label>
                <input id="germinacion" class="number" name="germinacion" type="text" style="width: 25%" maxlength="5"/>
            </div>
            <div>
                <label style="width:50%">% Humedad</label>
                <input id="humedad" class="number" name="humedad" type="text" style="width: 25%" maxlength="5"/>
            </div>
            <div>
                <label>Observacion</label>
                <textarea style="margin-top: 5%; width: 95%;" name="observacion" cols="26" rows="3"></textarea>
            </div>
            <div style="float: right">
                <input id="mdl" name="mdl" type="hidden" value="laboratorio"/>
                <input id="opt" name="opt" type="hidden" value="guardar"/>
                <input id="pag" name="pag" type="hidden" value="prueba"/>
                <input id="isol" name="isol" type="hidden" value="<?php echo $nosolicitud; ?>"/>
                <input id="icos" name="icos" type="hidden" value="<?php echo $isemilla; ?>"/>
                <input id="campo" name="campo" type="hidden" value="<?php echo $idlab; ?>"/>

                <button id="resultado-muestra" class="btn btn-success" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                </button>
            </div>
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        //solo numeros
        $("#pureza,#germinacion,#humedad").numeric();
        $("#resultado-muestra").click(function(event) {
            event.preventDefault();
            datos = $("form#resultado").serialize();
            $.ajax({
                url : 'control/index.php',
                data : datos,
                type : 'POST',
                beforeSend : function() {

                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                switch (html) {
                case 'OK' :
                    $.funciones.mostrarMensaje('ok', 'Los datos fueron guardados');
                    $.funciones.ocultarMensaje(5000);
                    break;
                default:
                    $.funciones.mostrarMensaje('error', responseText);
                    $.funciones.ocultarMensaje(5000);
                    break;
                }
            });
        });
    }); 
</script>