<h2 class="title">Ingresar Resultados de laboratorio</h2>
<div class="bx-search">
    <?php
echo "<div id='advertencia' class='ui-widget' style='margin-bottom:2%'>
<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>
<p>
<span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>
La busqueda se realiza por uno de los siguientes campos:
<ul style=\"line-height:normal;list-style-image: url('css/images/menu_right.png');\">
<li>Nombre de la semilla</li>
<li>Numero_campo</li>
<li>Nombre de semillerista</li>
</ul>
</p>
</div>
</div>";
?>
    <form id="busqueda_general" style="width: 70%;">
        <div>
            <input id="search-text" type="text" name="buscar" autocomplete="off">
        </div>
        <div style="left: 36em; position: relative; top: -3.8em;">
            <button id="buscar-muestra" class="btn btn-success">
              <span class="glyphicon glyphicon-search"></span>
                Buscar
            </button>
        </div>
        <?php
        include '../vista/error/errores.php';
        ?>
    </form>
</div>

<div id="bx_muestras">
<?php
include_once '../vista/laboratorio/muestras.html.php'; 
?>    
</div>

<div class="entry ver">

</div>
<script>
    $(document).ready(function() {
        $("#f_resultado").calendarioLab('f_resultado');
        
        $("#search-text").keypress(function(event) {
            var tecla = event.which;
            var term = $("#search-text").val();
            if ((term != '') && (tecla==13)) {
                event.preventDefault();
                $.post('control/index.php', {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'muestras',
                    nrocampo : term
                }, function(showResponse) {
                    alert(showResponse);
                    $("div#bx_muestras").empty();
                    $("div#bx_muestras").empty().append(showResponse);
                });
            }
        });
        $("#buscar-muestra").click(function(event) {
            event.preventDefault();
            var term = $("#search-text").val();
            var mensaje = '';
            if (term != '') {
                $("#search-text").css({
                    backgroundColor : ""
                });
                $.post('control/index.php', {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'muestras',
                    nrocampo : term
                }, function(showResponse) {
                    $("#bx_muestras").empty().prepend(showResponse);
                });
            }else{
                $("#search-text").css({
                    backgroundColor : "#f5c9c9"
                });
                mensaje += '<div>- Debe escribir texto a buscar</div>';
            }
        });
    })
</script>