    <div class="head" style="height: 2.2em">
        <div class="htitle" >
            <label style="width:6em;padding-top: 1%;">N&uacute;mero Solicitud</label>
            <label style="width:12em;padding-top: 1%;">Semillera</label>
            <label style="width:10em;padding-top: 1%;">Semillerista</label>
            <label style="width:5.7em;padding-top: 1%;">N&uacute;mero Campo</label>
            <label style="width:5.7em;padding-top: 1%;">N&uacute;mero An&aacute;lisis</label>
            <label style="width:5em;padding-top: 1%;">Pureza</label>
            <label style="width:7em;padding-top: 1%;">Germinacion</label>
            <label style="width:5.7em;padding-top: 1%;">Humedad</label>
            <label style="width:5.7em;padding-top: 1%;">Fecha Recepcion</label>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>
        </div>
    </div>
    <div class="body">
        <?php
        //echo json_encode($muestras);
        while ($muestras = DBConnector::objeto()) {                         
        ?>
        <div class="nsLab nSolicitud">
            <label class="nosolLab"><?php echo $muestras->nro_solicitud; ?></label>
        </div>
        <div class="semlab semillera">
            <label class="semilleraLab"><?php echo $muestras->semillera; ?></label>
        </div>
        <div class="proLab productor">
            <label class="semilleristaLab"><?php echo utf8_encode($muestras->semillerista); ?></label>
        </div>
        <div class="ncLab nSolicitud" style="width: 5.7em;">
            <label class="nocampoLab"><?php echo $muestras->nro_campo; ?></label>
        </div>
        <div class="naLab nSolicitud" style="width: 5.7em;">
            <label class="noanaLab"><?php echo empty($muestras -> nro_analisis) ? '-' : $muestras -> nro_analisis; ?></label>
        </div>
        <div class="anaLab nSolicitud" style="width: 5em;">
            <label class="analisis"><?php echo ($muestras->pureza)?'SI':'NO'; ?></label>
        </div>
        <div class="anaLab nSolicitud" style="width: 7em;">
            <label class="analisis"><?php echo ($muestras->germinacion)?'SI':'NO'; ?></label>
        </div>
        <div class="anaLab nSolicitud" style="width: 5.7em;">
            <label class="analisis"><?php echo ($muestras->humedad)?'SI':'NO'; ?></label>
        </div>
        <div class="fLab fecha" style="width: 5.7em">
            <label class="fechaLab"><?php echo $dates->cambiar_formato_fecha($muestras -> fecha_recepcion); ?></label>
        </div>
        <?php if ($muestras->estado==1) { ?>
        <div class="stdLab lote" style="width: 8.4em;">
            <a class="stadoLab" style="cursor:pointer">Ver resultado</a>
        </div>
        <?php } else { ?>
        <div class="stdLab lote" style="width: 8.4em;">
            <a class="stadoLab" style="cursor:pointer">Ingresar resultado</a>
        </div>
        <?php
        }
        }
        ?>

    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $("div#calendar").css('width','122%');
    });
</script>