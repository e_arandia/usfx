<div class="entry">
    <form id="resultado" class="seguimiento">
        <?php
        $row = DBConnector::objeto();
        ?>
        <div id="fechar">
            <label>Fecha Recepcion</label>
            <input id="fecha" class="info not-edit" style="width:70px" name="f_recepcion" type="text" readonly="readonly" value="<?php echo $dates -> cambiar_formato_fecha($row -> fecha_recepcion); ?>"/>
        </div>
        <div>
            <label>Cultivo</label>
            <input id="cultivo" class="info not-edit" style="width:70px" name="cultivo" type="text" readonly="readonly" value="<?php echo $row -> cultivo; ?>"/>
        </div>
        <div>
            <label>Variedad</label>
            <input id="variedad" class="info not-edit" style="width:110px" name="variedad" type="text" readonly="readonly" value="<?php echo utf8_encode($row -> variedad); ?>"/>
        </div>
        <div>
            <label style="width: 20%">Nro An&aacute;lisis</label>
            <input id="analisis" class="not-edit" name="analisis" type="text" value="<?php echo (++$nro_analisis); ?>" style="width: 5%"/>
        </div>
        <div style="float:right;width:380px;margin-top:-23%;font-weight:bolder"> 
   <!-- <table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th></th>
      <th>Basica </th>
      <th>Certificada</th>
      <th>Registrada</th>
      <th>Fiscalizada</th>
    </tr>
  </thead>
  <tbody style="font-weight:normal">
    <tr class="success" >
      <th scope="row">Humedad</th>      
      <td style="text-align:center"><?php echo intval($lst_humedad[0])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_humedad[1])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_humedad[2])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_humedad[3])." %";?></td>
    </tr>
    <tr>
      <th scope="row">Germinacion</th>
      <td style="text-align:center"><?php echo intval($lst_germinacion[0])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_germinacion[1])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_germinacion[2])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_germinacion[3])." %";?></td>
    </tr>
    <tr class="success">
      <th scope="row">Pureza</th>
      <td style="text-align:center"><?php echo intval($lst_pureza[0])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_pureza[1])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_pureza[2])." %";?></td>
      <td style="text-align:center"><?php echo intval($lst_pureza[3])." %";?></td>
    </tr>
  </tbody>
</table>  -->
  </div>
    </form>
    <form id="resultado" class="seguimiento"  style="padding:2% 2% 65% 2%">
        <div style="width: 48%; float: left;">
            <div>
                <label style="margin-top:5%;width:25%">Semilla</label>
            </div>
            <div id="laboratorio">
                    <input type="radio" id="silab" name="estado" value="2"  checked="checked" />
                    <label for="silab" style="width: 30%">Certificada</label>
                    <input type="radio" id="nolab" name="estado" value="3"/>
                    <label for="nolab" style="width: 30%;">Importada</label>
            </div>
            <div>
                <label style="width: 35%">Fecha Resultado</label>
                <input id="f_resultado" class="not-edit" name="resultado" type="text" readonly="true" value="<?php echo date('d-m-Y'); ?>" style="width: 25%"/>
            </div>
            <div>
                <label style="width: 35%">Lote Nro</label>
                <input id="lote" class="not-edit" name="lote" type="text" value="<?php echo $lote; ?>" style="width: 40%"/>
            </div>
            <div>
                <label id="lbl-tipo-semilla" style="text-transform:none;width: 35%">Tipo de Semilla</label>
                <select size="1" name="tipo" id="tipo" style="width: 25%">
                    <option value="">[ Seleccione Tipo ]</option>
                    
                </select>
            </div>
            <div>
                <label style="width: 35%">Cantidad de Bolsas</label>
                <input id="bolsa" class="number" name="bolsa" type="text" style="width: 25%" maxlength="4"/>
            </div>
            <div>
                <label style="width: 35%">Kg. / Bolsa</label>
                <input id="kgbolsa" class="number" name="kgbolsa" type="text" style="width: 25%" maxlength="2"/>
            </div>
            <div>
                <label style="width: 35%">Total</label>
                <input id="total" class="not-edit" name="total" type="text" style="width: 25%"/>
            </div>
        </div>
        <div style="width: 50%;float: right;">
            <div>
                <label id="lbl-calibre">Calibre</label>
                <!--<input id="calibre" name="calibre" type="text" />-->
                <select name="calibre" id="calibre" >
                    <option value="">[ Seleccione Calibre]</option>
                    <option value="grande">Grande</option>
                    <option value="mediana">Mediana</option>
                    <option value="otro">Otro</option>                    
                </select>
            </div>
            <div>
                <label style="text-transform:none;width:50%">N&uacute;mero Semilla por Kilo</label>
                <input id="semillakg" class="number" name="semillakg" type="text" style="width: 25%" maxlength="6"/>
            </div>

            
            <div id="humedad" style="display:<?php echo ($row->humedad==1)?'':'none'?>">
                <label style="width:50%">% Humedad</label>
                <input id="humedad" class="number" name="humedad" type="text" style="width: 25%" maxlength="5"/>
                <span style="font-size: 9px; margin-left: 15%; font-style: italic;"></span>
            </div>
            <div id="germinacion" style="display:<?php echo ($row->germinacion==1)?'':'none';?>">
                <label style="width:50%">% Germinacion</label>
                <input id="germinacion" class="number" name="germinacion" type="text" style="width: 25%" maxlength="5"/>
                <span style="font-size: 9px; margin-left: 15%; font-style: italic;"></span>
            </div>
            <div id="pureza" style="display:<?php echo ($row->pureza==1)?'':'none'?>">
                <label style="width:50%">% Pureza</label>
                <input id="pureza" class="number" name="pureza" type="text" style="width: 25%" maxlength="5"/>
                <span style="font-size: 9px; margin-left: 15%; font-style: italic;"></span>
            </div>
          
            <div>
                <label>Observacion</label>
                <textarea id="observacion" style="margin-top: 5%; width: 95%;" name="observacion" cols="26" rows="3"></textarea>
            </div>
            <div style="float: right">
                <?php
                Categoria::getCategoriaPorcentaje($row->categoria_sembrada);
                #$obj2 = DBConnector::objeto();
                ?>
                <input id="mdl" name="mdl" type="hidden" value="laboratorio"/>
                <input id="opt" name="opt" type="hidden" value="guardar"/>
                <input id="pag" name="pag" type="hidden" value="resultados"/>
                <input id="isol" name="isol" type="hidden" value="<?php echo $nosolicitud;?>"/>
                <input id="icos" name="icos" type="hidden" value="<?php echo $isemilla;?>"/>
                <input id="campo" name="campo" type="hidden" value="<?php echo $idlab;?>"/>
                <input id="muestra" name="muestra" type="hidden" value="<?php echo $row->id_muestra;?>"/>
                <input id="catsem" name="catsem" type="hidden" value="<?php echo $row->categoria_sembrada;?>"/>
                <!--
                <input id="cat-pureza" name="catPureza" type="hidden" value="<?php echo intval($obj2->pureza);?>"/>
                <input id="cat-germinacion" name="catGerminacion" type="hidden" value="<?php echo intval($obj2->germinacion);?>"/>
                <input id="cat-humedad" name="catHumedad" type="hidden" value="<?php echo intval($obj2->humedad);?>"/>
                
                <button id="resultado-muestra" class="btn btn-success" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                </button>
            -->
                <button id="enviar-resultado-muestra" class="btn btn-success"  type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        //opcion laboratorio
        $("#laboratorio").buttonset();
        //seleccion de tipos y calibre
        $("select#tipo,select#calibre").selectmenu();
        //solo numeros
        $("#pureza,#germinacion,#humedad").numeric();
        
        $.getJSON("control/index.php",{
            mdl:'laboratorio',
            opt:'buscar',
            pag:'tipo_semilla'
            },function(json){
            $.each(json.tipo,function(index,tipo){
                $("select#tipo").append("<option value='"+json.id[index]+"'>"+tipo+"</option>");    
            });
        });
        var inputs = ['semillakg'];
        $.funciones.soloNumeros(inputs);
        $("input#pureza").on("keyup", function() {
            var digito = $(this).val();
            if (parseFloat(digito) > 100.00) {
                $(this).val(0);
            }3
        });
        $("input#germinacion").on("keyup", function() {
            var digito = $(this).val();
            if (parseFloat(digito) > 100.00) {
                $(this).val(0);
            }
        });
        $("input#humedad").on("keyup", function() {
            var digito = $(this).val();
            if (parseFloat(digito) > 100.00) {
                $(this).val(0);
            }
        });
        $("input#kgbolsa").on("keyup", function() {
            var nro = $("input#bolsa").val();
            var kilo = $(this).val();
            var total = parseInt(nro) * parseInt(kilo);

            $("input#total").val(total);
        });
        function validadAnalisis(){
            var pureza = $("input#pureza").val();
            var germinacion = $("input#germinacion").val(); 
            var humedad = $("input#humedad").val();
            var resultados = [pureza,germinacion];
            var cat_pureza = $("input#cat-pureza").val();
            var cat_germinacion = $("input#cat-germinacion").val(); 
            var cat_humedad = $("input#cat-humedad").val();
            var valores = [cat_pureza,cat_germinacion];
            var inputs = ["pureza","germinacion"]
            $.funciones.validarAnalisis (inputs,valores,resultados);
        };
        $("#enviar-resultado-muestra").on("click", function() {            
            datos = $("form#resultado").serialize();
            $.ajax({
                url : 'control/index.php',
                data : datos,
                type : 'POST',
                beforeSend: validar,
                success : showResponse
            });
            function validar (){
                tipo_semilla = $("select#tipo").find(':selected');
                cant_bolsa = $("#bolsa");
                kgbolsa = $("#kgbolsa");
                calibre = $("select#calibre").find(':selected');
                semilla_kilo = $("#semillakg");
                msg = '';
                if (!tipo_semilla.val()){
                    $("#lbl-tipo-semilla").css({
                        color : "#9b000d"
                    });
                    msg += '- Tipo de Semilla<br>';
                }else{
                    $("#lbl-tipo-semilla").css({
                        color : ""
                    });
                }
                if (!cant_bolsa.val()){
                    cant_bolsa.css({
                        backgroundColor : "#f5c9c9"
                    });
                    msg += '- Cantidad de bolsas<br>';
                }else{
                    cant_bolsa.css({
                        backgroundColor : ""
                    });
                }
                if (!kgbolsa.val()){
                    kgbolsa.css({
                        backgroundColor : "#f5c9c9"
                    });
                    msg += '- Kg. / Bolsa<br>';
                }else{
                    kgbolsa.css({
                        backgroundColor : ""
                    });
                }
                if (!calibre.val()){
                    $("#lbl-calibre").css({
                        color : "#9b000d"
                    });
                    msg += '- Calibre<br>';
                }else{
                    $("#lbl-calibre").css({
                        color : ""
                    });
                }
                if (!semilla_kilo.val()){
                    semilla_kilo.css({
                        backgroundColor : "#f5c9c9"
                    });
                    msg += '- N&uacute;mero de semillas por kilo<br>';
                }else{
                    semilla_kilo.css({
                        backgroundColor : ""
                    });
                }

                if (msg != '') {
                    $("#errores div p").css('margin-left','2%');
                    $("#errores div p .mensaje").css('margin-left','-60%').empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + msg);
                    $("#errores").fadeIn('slow');
                    return false;
                } else {
                    $("#errores").fadeOut('slow');    
                    $.funciones.deshabilitarTexto("enviar-resultado-muestra");      
                    return true;            
                }
            }
            function showResponse(responseText, statusText, xhr, $form) {
                switch (responseText) {
                case 'OK' :
                    setTimeout($.funciones.recargarIngresarResultados(), 10000);
                    break;
                default:
                    $.funciones.mostrarMensaje('error', responseText);
                    $.funciones.ocultarMensaje(5000);
                    break;
                }
            }
        });
       
    }); 
</script>