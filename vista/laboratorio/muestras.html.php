<div class="div-filter">
<h2>Muestras de laboratorio</h2>
<div id="calendar" style="width:66.1%;">
    <div class="head" style="height: 3em">
        <div class="htitle" >
            <label style="width:6em;padding-top: 1%;">Numero campo</label>
            <label style="width:5.5em;padding-top: 1%;">Fecha Recepcion</label>
            <label style="width:10em;padding-top: 1%;">Semillerista</label>
            <label style="width:7.6em;padding-top: 1%;">Cultivo</label>
            <label style="width:8.5em;padding-top: 1%;">Variedad</label>
            <label style="width:7.6em;padding-top: 1%;">Estado</label>
        </div>
    </div>
    <div class="body">
        <?php
        while ($row = DBConnector::objeto()) {
        ?>
        <div class="nSolicitud">
            <label class="nocampoLab"><?php echo $row -> nro_campo; ?></label>
        </div>
        <div class="fecha">
            <label class="fecharLab"><?php echo $dates -> cambiar_formato_fecha($row -> fecha_recepcion); ?> </label>
        </div>
        <div class="productor">
            <label class="productorLab"><?php echo ucwords(utf8_encode($row -> semillerista)); ?></label>
        </div>
        <div class="cultivo">
            <label class="cultivoLab"><?php echo ucfirst($row -> cultivo); ?> </label>
        </div>
        <div class="variedad">
            <label class="variedadLab"> <?php echo utf8_encode(strtoupper($row -> variedad)); ?></label>
        </div>
        <div class="rango">
            <input name="stadoLabIn" type="image" title="Ingresar resultados" src="images/editar22x22.png" value="<?php echo $row -> nro_campo; ?>" />
        </div>
        <input type="hidden" class="ilab"/>
        <input type="hidden" class="nosolicitud" name="nosolicitud" value="<?php echo $row->nro_solicitud?>"/>
        <input type="hidden" class="tipoSemilla" name="tipoSemilla" value="1"/>
        <?php
        }
        ?>
        
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        //alternar colores
        var celdas = ['nSolicitud', 'fecha','productor','cultivo','variedad','rango'];
        $.funciones.alternarColores(celdas);
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'muestras',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nro_campo, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label class=\"nocampoLab\">" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label class=\"fecharLab\">" + json.fecha_recepcion[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label class=\"productorLab\">" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label class=\"cultivoLab\">" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label class=\"variedadLab\">" + json.variedad[index] + "</label> </div>";
                            json.estado[index];
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['nSolicitud', 'fecha','productor','cultivo','variedad','rango'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $.getJSON("control/index.php", {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'muestras',
                    search : '%'
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nro_campo, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label class=\"nocampoLab\">" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label class=\"fecharLab\">" + json.fecha_recepcion[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label class=\"productorLab\">" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label class=\"cultivoLab\">" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label class=\"variedadLab\">" + json.variedad[index] + "</label> </div>";
                            json.estado[index];
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['nSolicitud', 'fecha','productor','cultivo','stadoLab'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
                $("div.body").show();
            }
        });
        //boton de busqueda avanzada
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '0'
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var idlab;
        //ingresar resultados de analisis
        $("input[name=stadoLabIn]").mouseover(function() {
            var cssObj = {
                'text-decoration' : 'underline'
            };
            $(this).css(cssObj);
            $(".ilab").val($(this).val());
        }).mouseleave(function() {
            var cssObj = {
                'text-decoration' : 'none'
            };
            $(this).css(cssObj);
            $(".ilab").empty();
        }).click(function() {
            idlab = $(".ilab").val();
            tipo = $(".tipoSemilla").val();
            noSolicitud = $(".nosolicitud").val();
            $.post('control/index.php', {
                mdl : 'laboratorio',
                opt : 'ver',
                pag : 'inMuestra',
                id : idlab,
                tipo : tipo,
                nro : noSolicitud
            }, function(showResponse) {
                if (showResponse != 'Error')
                    $(".post").empty().prepend(showResponse);
                else {
                    $("span.mensaje").append("No se encuentra el numero de campo seleccionado");
                    $("#errores").show();
                }
            });
        });
        //ver resultados de analisis
        $("input[name=stadoLabOut]").mouseover(function() {
            var cssObj = {
                'text-decoration' : 'underline'
            };
            $(this).css(cssObj);
            $(".ilab").val($(this).val());
        }).mouseleave(function() {
            var cssObj = {
                'text-decoration' : 'none'
            };
            $(this).css(cssObj);
            $(".ilab").empty();
        }).click(function() {
            idlab = $(".ilab").val();
            $.post('control/index.php', {
                mdl : 'laboratorio',
                opt : 'ver',
                pag : 'muestras',
                id : idlab
            }, function(showResponse) {
                if (showResponse != 'Error')
                    $(".post").empty().prepend(showResponse);
                else {
                    $("span.mensaje").append("No se encuentra el numero de campo seleccionado");
                    $("#errores").show();
                }
            });
        });
    })
</script>