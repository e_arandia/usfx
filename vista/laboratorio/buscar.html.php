<div class="ui-widget" id="advertencia">
    <div style="padding: 0 .7em;width: 35em" class="ui-state-error ui-corner-all">
        <p>
            <span style="float: left; margin-right: .3em; margin-top: 0.5em;" class="ui-icon ui-icon-info"></span>
            La busqueda se realiza por uno de los siguientes campos:
        </p>
        <ul style="line-height:normal;list-style-image: url('css/images/menu_right.png');">
            <li>
                Nombre de la semilla
            </li>
            <li>
                Numero de campo
            </li>
            <li>
                Numero de lote
            </li>
            <li>
                Numero de analisis
            </li>
        </ul>
        <p></p>
    </div>
</div>
<form id="busqueda_general" style="margin: 5em 0 0 0">
    <div>
        <input id="search-text" type="text" name="buscar" autocomplete="off">
        <!--<input id="search-text" type="text" name="buscar" placeholder="semilla,numero_campo,lote,numero_analisis">-->
    </div>
    <div style="left: 38em;position: relative;top: -4em;width: 15%">
        <button id="buscar-resultado" class="btn btn-success" type="submit">
            <span class="glyphicon glyphicon-search"></span>
            Buscar
        </button>
    </div>
</form>
<?php
include '../vista/error/errores.php';
?>
<div id="bx_resultado"></div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#search-text").keypress(function(event) {
            if (event.which == 13) {
                var term = $("#search-text").val();
                alert(term);
                //$.funciones.buscarResultados(term);
                $.post('control/index.php', {
                    mdl : 'laboratorio',
                    opt : 'buscar',
                    pag : 'respruebas',
                    buscar : term
                }, function(showResponse) {
                    //alert(showResponse);
                    $("#bx_resultado").empty().prepend(showResponse);
                });
            }
        });

        $("button#buscar-resultado").on("click", function(event) {
            //event.preventDefault();
            var term = $("#search-text").val();
            $.funciones.buscarResultados(term);
        });

        /*
         $("#search-text").keyup(function() {

         $.post('control/index.php', {
         mdl : 'laboratorio',
         opt : 'buscar',
         pag : 'respruebas',
         buscar : term
         }, function(showResponse) {
         //console.log(showResponse);
         $("#bx_resultado").empty().prepend(showResponse);
         });
         });
         */
    }); 
</script>