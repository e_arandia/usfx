<?php
//if (DBConnector::filas() > 0 ){
if (TRUE){  
?>
<h2>Resultados de pruebas de laboratorio</h2>
<?php
$width = ($nro == 10) ? "'width:134.4%'" : "'width:122.1%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
//ejecutar consulta sql
DBConnector::ejecutar($blocks);
?>
<div id="calendar" style=<?php echo $width; ?>>
    <div class="head" style="height: 3.2em">
        <div class="htitle" >            
            <label style="width:6em;padding-top: 1%;">Semilla</label>
            <label style="width:7em;padding-top: 1%;">Especie</label>
            <label style="width:12em;padding-top: 1%;">Variedad</label>
            <label style="width:10em;padding-top: 1%;">Categoria</label>
            <label style="width:8.2em;padding-top: 1%;">Origen</label>
            <label style="width:8em;padding-top: 1%;">Cantidad</label>
            <label style="width:6.7em;padding-top: 1%;">Certificado Fitosanitario</label>
            <label style="width:10em;padding-top: 1%;">Aduana ingreso</label>
            <label style="width:10em;padding-top: 1%;">Destino semilla</label>
            <label style="width:5.7em;padding-top: 1%;">Area de distribucion y/o comercializaci&oacute;n</label>
            
            <label style="width:5.7em;padding-top: 1%;">N&uacute;mero Campo</label>
            <label style="width:5.7em;padding-top: 1%;">Fecha recepcion</label>
            <label style="width:5.7em;padding-top: 1%;">Fecha Resultado</label>
            <label style="width:5.7em;padding-top: 1%;">Lote</label>
            <label style="width:5.7em;padding-top: 1%;">Tipo Semilla</label>
            <label style="width:5.7em;padding-top: 1%;">Numero Bolsa</label>
            <label style="width:5.7em;padding-top: 1%;">Kg/Bolsa</label>
            <label style="width:5.7em;padding-top: 1%;">Total</label>
            <label style="width:5.7em;padding-top: 1%;">Calibre</label>
            <label style="width:5.7em;padding-top: 1%;">N&uacute;mero Semilla por kilo</label>
            <label style="width:5.7em;padding-top: 1%;">% Pureza</label>
            <label style="width:5.7em;padding-top: 1%;">% Germinaci&oacute;n</label>
            <label style="width:5.7em;padding-top: 1%;">% Humedad</label>
            <label style="width:5.7em;padding-top: 1%;">Observaci&oacute;n</label>
            <?php if ($nro == 10){
            ?>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>
            <?php } ?>
        </div>
    </div>
    <div class="body">
        <?php
while ($datos = DBConnector::resultado()){
        ?>
        <div class="nSolicitud">
            <label> <?php    echo $datos -> nro_solicitud; ?></label>
        </div>
        <div class="sistema sistem">
            <label> <?php  echo utf8_encode($datos -> sistema); ?></label>
        </div>
        <div class="semillera">
            <label> <?php   echo $datos -> semillera; ?></label>
        </div>
        <div class="productor">
            <label> <?php   echo utf8_encode($datos -> nombre) . ' ' . utf8_encode($datos -> apellido); ?></label>
        </div>
        <div class="dpto">
            <label> <?php   echo $datos -> departamento; ?></label>
        </div>
        <div class="prov">
            <label> <?php  echo utf8_encode($datos -> provincia); ?></label>
        </div>
        <div class="municipio">
            <label> <?php   echo utf8_encode($datos -> municipio); ?></label>
        </div>
        <div class="localidad">
            <label> <?php echo utf8_encode($datos -> localidad); ?></label>
        </div>
        <div class="comunidad">
            <label> <?php   echo $datos -> comunidad; ?></label>
        </div>
        <div class="fecha">
        
            <label> <?php echo $dates -> cambiar_formato_fecha($datos -> fecha); ?></label>
        </div>
        <?php
if ($nro == 10){
        ?>
        <div class="sistema options" style="height: 28px; width: 8.4em;">
            <div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
        </div>
        <?php
        }
        }
        ?>
        <input id="idsolicitud" type="hidden"/>
        <input id="control" type="hidden" value="<?php echo $nro; ?>" />
    </div>  
</div>
<div class="meneame" style="padding-top: 75%; width: 50%; margin-left: 30%;">
    <?php
    if ($total > $nroRegistros)
        for ($cont = 1; $cont <= $plinks; $cont++) {
            if (($cont % 20) == 1)
                echo "</br></br>";
            $temp = $cont - 1;
            echo "<a id=$cont name='pagina' onclick=cargarSolicitudes('certificacion','ver','solicitud',$nro,$temp)>$cont</a> ";
        }
     ?>
    </div> 
    <?php
    }else{
    echo "<div id='advertencia' class='ui-widget'>
    <div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>
    <p>
    <span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>
    No existen datos en esta etapa
    </p>
    </div>
    </div>";
    }
?>
<script type="text/javascript">
    $(document).ready(function() {
            
    });
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        }); 
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {

        //colorear celdas
        $("div.body >div.nSolicitud:odd,div.body >div.sistem:odd,div.body >div.semillera:odd,div.body >div.productor:odd,div.body >div.dpto:odd").css('background-color', '#f6ffe8');
        $("div.body >div.prov:odd,div.body >div.municipio:odd,div.body >div.localidad:odd,div.body >div.comunidad:odd,div.body >div.fecha:odd,div.body >div.options:odd").css('background-color', '#f6ffe8');
        //envio de datos

    })
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar el registro?')) {
                $.funciones.mostrarMensaje('info', 'Eliminando registro')
                var id = $("#idsolicitud").val();
                $.post('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'eliminar',
                    pag : 'solicitud',
                    id : id
                }, function(data) {
                    if (data == 'OK') {
                        $.funciones.mostrarMensaje('ok', 'Solicitud eliminada');

                        setTimeout(function() {
                            var area = $("#control").val();
                            $.post('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'solicitud',
                                area : area
                            }, function(data) {
                                $(".post").empty().append(data);
                            });
                        }, 50);
                        $.funciones.ocultarMensaje(5000);

                    } else {
                        $.funciones.mostrarMensaje('error', 'No se puede eliminar la solicitud');
                        $.funciones.ocultarMensaje(5000);
                    }

                });
            }
        });
        /**actualizar datos solicitud*/
        $("input[name=upd]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            var id = $("#idsolicitud").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_solicitud',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>