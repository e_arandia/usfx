<h2 class="title" style="letter-spacing: .5em;margin-left: 40%">Detalle Certificaci&oacute;n</h2>
<div class="entry parametros">
    <form id="semilleras" class="seguimiento">
        <?php
        include '../vista/certificacion/semilleras_productores.html';
        ?>
        <div>
            <label>Campa&ntilde;a: </label>
            <select id="campanha" name="campanha" disabled="disabled">
                <option value="">[ Seleccionar campaña ]</option>
            </select>
        </div>
        <div>
            <input type="hidden" value="<?php echo $_SESSION['usr_iarea'] ?>"  id="area" name="area"/>
        </div>
        <div style="float: right; margin-top: -7%;">
            <button id="button-detail" style="display: none">
                Buscar
            </button>
        </div>
    </form>
</div>
<?php
include '../vista/error/errores.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("select#campanha").selectmenu();
        $.funciones.detalleSemilleras('semillera');
        $("#campanha").attr('disabled', 'disabled');
        //creacion de evento onchange
        $("select#semillera").selectmenu({
            change : function(event, ui) {
            }
        });
        //boton buscar
        //boton de registro
        $("button#button-detail").button({
            icons : {
                primary : "ui-icon-search"
            }
        });

        //scroll para select
        $("#semillera").selectmenu().selectmenu("menuWidget").addClass("overflow");

        $("select#semillera").on("selectmenuchange", function(event, ui) {

            var semillera = $("#semillera").val();
            if (semillera != "") {
                $("select#productor").selectmenu({
                    disabled : false
                });
                $("button#button-detail").show();
                var palabras = semillera.split(" ");
                semillera = '';
                semillera = palabras.join("*");
                var area = $('#area').val();
                $.funciones.cargarCampanhas('certifica');
                $("#productor").productores('certifica',semillera, 'detalle', area);
                $("select#productor").selectmenu().selectmenu("menuWidget").addClass("overflow");
                $("select#productor").selectmenu({
                    disabled : false
                });
                $("select#campanha").selectmenu({
                    disabled : false
                });
            } else {
                $("select#campanha").selectmenu({
                    disabled : true
                });
                $("button#button-detail").hide();
            }
        });
        //creacion de evento onchange
        $("select#productor").selectmenu({
            change : function(event, ui) {
            }
        });
        $("#button-detail").click(function(event) {
            $("form#semilleras").gestionesAnteriores();
        });
    })
</script>