<div class="entry resultado">
	<div style="width: 220%;" id="calendar">
		<div>
			<a id="pdf" class="img">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>
		</div>
		<div style="height: 3.2em" class="head">
			<div class="htitle" style="border-top: 2px solid #39C613">
				<label style="width:99.8em;padding-top: 1%;"> </label>
				<label style="width:21.3em;padding-top: 1%;">COSTOS POR CERTIFICACION </label>
			</div>
			<div class="htitle" style="border-top: 47px solid #39C613">
				<label style="padding-top: 1%; width: 3em;">Nº</label>
				<label style="padding-top: 1%; width: 12em;">SEMILLERA</label>
				<label style="padding-top: 1%; width: 12em;">COMUNIDAD</label>
				<label style="padding-top: 1%; width: 11em;">RESPONSABLE / SEMILLERISTA</label>
				<label style="padding-top: 1%; width: 9em;">CULTIVO</label>
				<label style="padding-top: 1%; width: 9em;">VARIEDAD</label>
				<label style="padding-top: 1%; width: 8.8em;">CATEGORIA SEMBRADA</label>
				<label style="width: 6em; padding-bottom: 1%;">SUPERFICIE Has.</label>
				<label style="width: 4em; padding-bottom: 1%;">Nº CAMPO</label>
				<label style="width: 6em; padding-bottom: 1%;">APROBADO Has.</label>
				<label style="width: 6em; padding-bottom: 1%;">RECHAZADO Has.</label>
				<label style="width: 6em; padding-bottom: 1%;">CATEGORIA APROBADO</label>
				<label style="width: 6em; padding-bottom: 1%;">NÚMERO BOLSAS</label>
				<label style="width: 5em; padding-bottom: 1%;">INSCRIP. Bs</label>
				<label style="width: 5em; padding-bottom: 1%;">CAMPO Bs.</label>
				<label style="width: 6em; padding-bottom: 1%;">ETIQUETAS Bs.</label>
				<label style="width: 5em; padding-bottom: 1%;">TOTAL Bs.</label>
			</div>
		</div>
		<div style="border: 1px solid #39C613" class="body" >
		    <?php
		    $indice = 1;
             while ($fila = DBConnector::resultado()){
            ?>
			<div style="padding-top: 1%; width: 3em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"> <?php echo $indice;?></label>
			</div>
			<div style="padding-top: 1%; width: 12em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->semillera;?></label>
			</div>
			<div style="padding-top: 1%; width: 12em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->comunidad;?> </label>
			</div>
			<div style="padding-top: 1%; width: 11em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->nombre.' '.$fila->apellido;?></label>
			</div>
			<div style="padding-top: 1%; width: 9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->cultivo;?></label>
			</div>
			<div style="padding-top: 1%; width: 9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->variedad;?></label>
			</div>
			<div style="padding-top: 1%; width: 8.8em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->categoria_sembrada;?></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->inscrita;?></label>
			</div>
			<div style="width: 4em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->nro_campo;?></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->aprobada;?></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->rechazada;?> </label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->categoria_aprobada;?></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"><?php echo $fila->nro_bolsa;?></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;height: 3em;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<?php
			}
			?>
		</div>
		<div style="border-top: 1px solid #39C613;border-bottom: 2px solid #39C613;border-left: 2px solid #39C613;border-right: 2px solid #39C613" class="body" >
			<div style="padding-top: 1%; width: 65.2em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;letter-spacing: 1.5em"> TOTAL</label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> 1,50</label>
			</div>
			<div style="padding-top: 1%; width: 4em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"></label>
			</div>
			<div style="padding-top: 1%; width: 6em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;">1,00</label>
			</div>
			<div style="padding-top: 1%; width: 6em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> 0,50</label>
			</div>
			<div style="padding-top: 1%; width: 6em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
			<div style="padding-top: 1%; width: 6em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
			<div style="padding-top: 1%; width: 5em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
			<div style="padding-top: 1%; width: 5em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
			<div style="padding-top: 1%; width: 6em;float: left;border-right:1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
			<div style="padding-top: 1%; width: 5em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;"> </label>
			</div>
		</div>
	</div>
</div>