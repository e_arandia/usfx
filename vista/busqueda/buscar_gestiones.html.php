<h2 class="title">Buscador gestiones anteriores</h2>
<div class="entry" style="margin-left: 0px;"  >
	<form id="buscador" class="nuevo" style="margin-top: -8%; width: 92%; border:none">
		<div>
			<label>Gestion: </label>
			<select size="1" name="gestion" id="gestion" class="special" ></select>
		</div>
		<div>
			<button class="btn btn-success" type="button" id="btn-search-gestiones" style="margin-left:82%">
				<span class="glyphicon glyphicon-search"></span>
				Buscar
			</button>
		</div>
		<div id="options" style="display: none">
			<ul>
				<li>
					<a id="lbl-resumen" href="#resumen">Resumen</a>
				</li>
				<li>
					<a id="lbl-detalle" href="#detalle">Detalle certificaci&oacute;n</a>
				</li>
				<li>
					<a id="lbl-superficie" href="#superficie">Superficie de produccion</a>
				</li>
				<li>
					<a id="lbl-volumen" href="#volumen">Volumen de produccion</a>
				</li>
			</ul>
			<div id="resumen">
				<div>
					<label style="width: 20%">Desde : </label>
					<input type="text" class="f_desde" style="width: 80px" name="desde" value=""/>
				</div>
				<div>
					<label style="10%">Hasta : </label>
					<input type="text" class="f_hasta" style="width: 80px" name="hasta"/>
				</div>

			</div>
			<div id="detalle">
				<div>
					<label id="lbl-cultivo" style="width: 10%;">Cultivo:</label>
					<select id="cultivo" name="cultivo">
						<option value="">[ Seleccionar cultivo ]</option>
					</select>
				</div>	
					<div style="width: 75%">
						<div>
							<label style="width: 20%">Desde : </label>
							<input type="text" class="f_desde" style="width: 80px;" name="desde" value=""/>
						</div>
						<div>
							<label style="10%">Hasta : </label>
							<input type="text" class="f_hasta" style="width: 80px;" name="hasta"/>
						</div>                        
					</div>				
			</div>
			<div id="superficie">
				<div>
					<label style="width: 20%">Desde : </label>
					<input type="text" class="f_desde" style="width: 80px;" name="desde" value=""/>
				</div>
				<div>
					<label style="10%">Hasta : </label>
					<input type="text" class="f_hasta" style="width: 80px;" name="hasta"/>
				</div>
			</div>
			<div id="volumen">
				<div>
					<label style="width: 20%">Desde : </label>
					<input type="text" class="f_desde" style="width: 80px;" name="desde" value=""/>
				</div>
				<div>
					<label style="10%">Hasta : </label>
					<input type="text" class="f_hasta" style="width: 80px;" name="hasta"/>
				</div>
			</div>
		</div>
		<?php
		  include '../vista/confirmacion/pdf_excel_gestiones.html';
		  include '../vista/confirmacion/advertencia_resumen_detalle.html';
		?>
		<input id="informes" type="hidden" value="resumen"/>
		<input type="hidden" value="<?php echo $_SESSION['usr_iarea'] ?>"  id="area" name="area"/>
	</form>
	<?php
        include '../vista/error/advertencia.php';
	?>
	<div class="resumenXLS" style="height: 460px; margin: 8% 0px;">
	    <iframe class="iresumen" style="display: none;">
	        
	    </iframe>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        /////////////////nombre de pestaña activa
        $("a#lbl-resumen").on("click",function(){
            $("#informes").val('resumen');
        });
        $("a#lbl-detalle").on("click",function(){
            $("#informes").val('detalle');
        });
        $("a#lbl-superficie").on("click",function(){
            $("#informes").val('superficie');
        });
        $("a#lbl-volumen").on("click",function(){
            $("#informes").val('volumen');
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var area = $('#area').val();
        sistema = $("input#area").val();
        $.funciones.buscarGestiones('gestion', area);
        //creacion de tabs con opciones
        $("#options").tabs();

        //boton buscar
        $("button#btn-search-gestiones").on("click", function() {
            gestion = $("select#gestion").find(":selected").val();
            if (gestion != '') {
                //llenar cultivos de gestiones anteriores
                $.getJSON("control/index.php", {
                    mdl : 'busqueda',
                    opt : 'buscar',
                    pag : 'cultivos_anteriores',
                    gestion : gestion
                }, function(json) {
                    if(json.msg == 'OK'){
                        $("select#cultivo").empty().append("<option value=''>[ Seleccionar cultivo ] </option>");
                        $.each(json.cultivos,function(id,cultivo){
                            $("select#cultivo").append("<option value='"+cultivo+"'>"+cultivo+"</option>");                        
                        });                        
                    }else{
                        $("select#cultivo").empty().append("<option value=''>[ No existen cultivos ] </option>").attr('disabled','disabled');    
                    }
                    $("button#btn-detalle-report").attr('disabled','disabled');
                    $("select#cultivo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");                    
                });
                $("input.f_desde").val("01-01-" + gestion);
                $.funciones.gestionesAnterioresCalendarioInicial("input", "f_desde", gestion);
                $.funciones.gestionesAnterioresCalendarioFinal("input", "f_hasta", gestion);

                //mostrar opciones y botones PDF-EXCEL
                $("div#options,div#previous-info").fadeIn();
                //mostrar botones
                $()
            } else {
                $(".alert>label").empty().append("Debe seleccionar una gestion");
                $(".alert").show().fadeOut(3000);
            }
        });
        //////////////////////CREACION Y VISUALIZACION DE PDF SEGUN OPCION
        $('button#pdf').on('click', function () {
            $.funciones.gestionesPasadasPDF();
        });
        $('button#xls').on('click', function () {
            $.funciones.gestionesPasadasXLS();
        });
    }); 
</script>