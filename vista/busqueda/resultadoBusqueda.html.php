<?php
$limite = !empty($semilleras)?count ($semilleras):0;
echo 'Coincidencia(s): '.$limite;
if ($limite){
?>
<div class="entry">
	<!--</div> style=" position: relative;left:-7em;">-->
	<div id="calendar" style="width: 140.5%;" >
		<div class="head" style="height: 3.2em">
			<div style="border-top: 1px solid #39C613" class="htitle">
				<label style="padding-top: 1%; width: 3em;">Nro</label>
				<label style="padding-top: 1%; width: 12em;">SEMILLERA</label>
				<label style="padding-top: 1%; width: 15em;">SEMILLERISTA</label>
				<label style="padding-top: 1%; width: 8.8em;">CAMPAÑA</label>
				<label style="padding-top: 1%; width: 9em;">CULTIVO</label>
				<label style="padding-top: 1%; width: 9em;">VARIEDAD</label>
				<label style="padding-bottom: 1%; width: 10em;">CATEGORIA SEMBRADA</label>
				<label style="padding-bottom: 1%; width: 10em;">CATEGORIA A PRODUCIR</label>
			</div>
		</div>
		<div class="body" style="border: 1px solid #39C613;width: 77.3em;">
			<?php
for ($counter=0; $counter < $limite; $counter++){
			?>
			<div style="padding-top: 1%; width: 3em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center" class="posicion"><?php $pos = $counter+1; echo $pos;?></label>
			</div>
			<div style="padding-top: 1%; float: left; border-right: 1px solid rgb(57, 198, 19); height: 3em; width: 12em;">
				<label style="text-align: center" class="semilleras"><?php echo $semilleras[$counter];?></label>
			</div>
			<div style="padding-top: 1%; float: left; border-right: 1px solid rgb(57, 198, 19); height: 3em; width: 15em;">
				<label style="text-align: center" class="semilleristas"><?php echo $semilleristas[$counter];?></label>
			</div>
			<div style="padding-top: 1%; float: left; border-right: 1px solid rgb(57, 198, 19); height: 3em; width: 8.8em;">
				<label style="text-align: center" class="campanhas"><?php echo $campanhas[$counter];?></label>
			</div>

			<div style="padding-top: 1%; float: left; border-right: 1px solid rgb(57, 198, 19); height: 3em; width: 9em;">
				<label style="text-align: center" class="cultivos"><?php echo $cultivos[$counter];?></label>
			</div>
			<div style="padding-top: 1%; width: 9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center" class="variedades"><?php echo $variedades[$counter];?></label>
			</div>
			<div style="padding-top: 1%; width: 10em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center" class="producidas"><?php echo $categoria_sembradas[$counter];?></label>
			</div>

			<div style="padding-top: 1%; width: 9.8em;float: left;height: 3em;">
				<label style="text-align: center" class="aprobadas"><?php echo $categoria_producidas[$counter];?></label>
			</div>
			<?php
            }
			?>
		</div>
	</div>
</div>
<?php
}
?>