<form id="busqueda_general" style="margin: 5em 0 0 0">
	<div>
		<input id="search-text" type="text" name="cadena_buscar">
	</div>
	<div style="left: -35em;position: relative;top: -4em;">
		<input id="search-submit" class="button-primary ui-state-default ui-corner-all" type="submit"
		style="text-transform: uppercase;height: 30px"	value="Buscar"	/>
	</div>
</form>
<div class="resultado"></div>
<?php
include '../vista/error/errores.php';
?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#busqueda_general").ajaxForm({
			url : 'control/index.php',
			type : 'post',
			data : {
				mdl : 'busqueda',
				opt : 'buscar',
				pag : 'todo'
			},
			beforeSubmit : peticion,
			success : showResponse
		});
		function peticion() {
			var mensaje = '';
			if($("input:text").val() == '') {
				mensaje += '<div>Debe ingresar una palabra o texto para buscar</div>';
			}

			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append(mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				return true;
			}

		}

		function showResponse(responseText, statusText, xhr, form) {
		    console.log(responseText);
			if (responseText.length != 123) {
			  $(".resultado").empty().prepend(responseText);
			}else{
				$("#errores").empty();
				$(".resultado").empty().prepend('La b&uacute;squeda no gener&oacute; ning&uacute;n resultado');
			}
		}
	})
</script>
