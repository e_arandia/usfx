<div class="entry">
<h2> Importar Base de datos desde archivo</h2>
<!--el enctype debe soportar subida de archivos con multipart/form-data-->
    <form enctype="multipart/form-data" class="formulario seguimiento">
        <input class="btn btn-success" type="file" style="width:420px" name="archivo" id="imagen" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
        <button id="send-file" class="btn btn-success" type="button" style="float:right;margin:-5.5% 5%">
            <span class="glyphicon glyphicon-upload bg-lg"></span>
            Importar
        </button>
    </form>
        <!--div para visualizar en el caso de imagen-->
    <div class="plantilla" style="font-size: smaller;float: right;">
      <span style="font-weight:bolder">Nota:</span>
      <span>El archivo con el formato correcto lo puede obtener</span>
      <a href = "vista/templates/formato.xlsx">aqui</a>
    </div>
    <!--div para visualizar mensajes-->
    <div class="messages"></div><br /><br /> 
</div>
<?php
include '../vista/confirmacion/importacion.html';
include '../vista/error/advertencia.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".messages").hide();
        //queremos que esta variable sea global
        var fileExtension = "";
        //función que observa los cambios del campo file y obtiene información
        $(':file').change(function() {
            //obtenemos un array con los datos del archivo
            var file = $("#imagen")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            //obtenemos el tamaño del archivo
            var fileSize = file.size/1024;
            //obtenemos el tipo de archivo image/png ejemplo
            var fileType = file.type;
            //mensaje con la información del archivo
            //showMessage("<span class='info'>Archivo para subir: " + fileName + ", peso total: " + fileSize.toFixed(2) + " kilobytes.</span>");
        });

        //al enviar el formulario
        $(':button').click(function() {
            //información del formulario
            var formData = new FormData($(".formulario")[0]);
            var message = "";
            
            
            //hacemos la petición ajax
            $.ajax({
                url : 'control/index.php?mdl=importar&opt=new&pag=datos',
                type : 'POST',
                dataType : 'json',
                // Form data
                //datos del formulario
                data : formData,
                //necesario para subir archivos via ajax
                cache : false,
                contentType : false,
                processData : false,
                //mientras enviamos el archivo
                beforeSend : function() {
                    $("#imagen,#send-file").attr("disabled","disabled");
                    $("div.plantilla").hide();
                    message = $("<img src='images/loader_6.gif'/><span class='before'> Ingresando datos, por favor espere...</span>");
                    showMessage(message)
                },
                //una vez finalizado correctamente
                success : function(json) {
                    if(json.state == 'OK'){
                        message = $("<span class='success'> Datos cargados correctamente!!!</span>");
                        showMessage(message);
                        message = $("<img src='images/loader_6.gif'/><span class='success'> Actualizando base de datos...</span>");
                        showMessage(message);
                        $.funciones.actualizarDataBase(json.file);
                    }else if (json.state == 'formato'){
                        $(".alert>label").empty().append(json.msg);
                        $(".alert").show().fadeOut(3000);
                    }else{
                        showMessage('');
                        $(".alert>label").empty().append(json.msg);
                        $(".alert").show().fadeOut(3000);
                    }
                    $("#imagen,#send-file").removeAttr("disabled");
                    $("div.plantilla").show();
                },
                //si ha ocurrido un error
                error : function(jqXHR, textStatus, errorThrown) {
                    alert( textStatus );
                    message = $("<img src='images/error.png'/><span class='error'> Ha ocurrido un error.</span>");
                    showMessage(message);
                }
            });
        });
    });
    //como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}
 
</script>