<form id="busqueda">
	<div style="border:1px dotted;">
		<input id="search-text" type="text" name="cadena">
	</div>
	<div style="left: -35em;position: relative;top: -4em;">
		<button id="search-submit" class="button-primary ui-state-default ui-corner-all" type="submit"
		style="text-transform: uppercase;height: 30px">
			Buscar
		</button>
	</div>
	<div id="parametros">
		<div>
			<input type="checkbox" name="semillera" value="semillera"/>
			<label>Semillera</label>
		</div>
		<div>
			<input type="checkbox" name="semillerista" value="semillerista"/>
			<label>Semillerista</label>
		</div>
		<div>
			<input type="checkbox" name="cultivo" value="cultivo"/>
			<label>Cultivo</label>
		</div>
		<div>
			<input type="checkbox" name="variedad" value="variedad"/>
			<label>Variedad</label>
		</div>
		<div>
			<input type="checkbox" name="categoria" value="categoria"/>
			<label>Categoria</label>
		</div>
	</div>
</form>
<?php 
include '../error/errores.php';
?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#busqueda").buscar('busqueda','');
	})
</script>