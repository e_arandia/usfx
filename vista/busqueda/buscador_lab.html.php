<h2 class="title">Buscador</h2>
<div class="entry" style="margin-left: 0px;"  >
    <form id="buscador" class="nuevo" style="margin-top: -8%; width: 73%;">
        <div>
            <label>Semillera: </label>
            <select size="1" name="semillera" id="semillera" class="special"></select>
        </div>
        <div class="semilleristas">
            <label>Semillerista: </label>
            <select  name="productor" id="semillerista" class="special"></select>
        </div>
        <div>
            <label>Cultivo: </label>
            <select size="1" name="cultivo" id="cultivo" class="special"></select>
        </div>
        <div>
            <label>Gestion: </label>
            <select size="1" name="gestion" id="gestion" class="special"></select>
        </div>
        <input type="hidden" value="<?php echo $_SESSION['usr_iarea'] ?>"  id="area" name="area"/>

        <!-- <input type="submit" style="top: 82%; position: absolute; right: 5%;" value="BUSCAR" class="button-primary ui-state-default ui-corner-all" name="submit">-->
        <button id="buscar-lab" class="btn btn-success">
            <span class="glyphicon glyphicon-search"></span>
            Buscar
        </button>

    </form>

    <?php
    //include '../vista/error/errores.php';
    ?>
    <div class="resultado" style="height: 460px; margin: 8% 0px;"></div>
</div>
<!--    parte para el autocompletado de las semilleras  -->
<script>
    $(document).ready(function() {

        /**
         *buscador
         *  */
        var area = $('#area').val();
        $.funciones.buscarSemilleras(area,'gestiones');
        $.funciones.buscarSemilleristas('semillerista',area);
        $.funciones.buscarCultivo('cultivo');
        $.funciones.buscarGestiones('gestion', area);
        //agregar scroll para select
        $("#semillera").selectmenu().selectmenu("menuWidget").addClass("overflow");
        $("#semillerista").selectmenu().selectmenu("menuWidget").addClass("overflow");
        $("#cultivo").selectmenu().selectmenu("menuWidget").addClass("overflow");
        $("#gestion").selectmenu().selectmenu("menuWidget").addClass("overflow-year");
        //boton de registro

        $("button#buscar-lab").addClass("button-primary");
        $("a.linkpages").click(function() {
            var pagina = $("input#area").val();
            $.funciones.cargarImagen('seguimiento');
            $.ajax({
                data : {
                    mdl : pagina,
                    pag : 'solicitud',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            });
            setTimeout($.funciones.cargarImagen('solicitud'), 10);
        });
        //opcion tod
        // fin del buscador

        $("#buscador").ajaxForm({
            url : 'control/index.php',
            type : 'POST',
            data : {
                mdl : 'busqueda',
                opt : 'buscar',
                pag : 'buscador'
            },
            success : showResponse
        });
        function showResponse(responseText, statusText, xhr, form) {

            if (responseText.length != 123) {
                $(".resultado").empty().prepend(responseText);
            } else {
                $(".resultado").empty().prepend('La b&uacute;squeda no gener&oacute; ning&uacute;n resultado');
            }
        }

    }); 
</script>