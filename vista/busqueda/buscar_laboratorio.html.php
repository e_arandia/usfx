<h2 class="title">Buscador gestiones anteriores</h2>
<div class="entry" style="margin-left: 0px;"  >
	<form id="buscador" class="nuevo" style="margin-top: -8%; width: 92%;">
		<div>
			<label>Gestion: </label>
			<select size="1" name="gestion" id="gestion" class="special" ></select>
		</div>
		<div>
			<button class="btn btn-success" type="button" id="btn-search-gestiones" style="margin-left:82%">
				<span class="glyphicon glyphicon-search"></span>
				Buscar
			</button>
		</div>
		<div id="options" style="display: none">

		</div>
		<input type="hidden" value="<?php echo $_SESSION['usr_iarea'] ?>"  id="area" name="area"/>
	</form>
	<?php
	include '../vista/confirmacion/pdf_excel_gestiones.html';
    include '../vista/error/advertencia.php';
    
	?>
	<div class="resumenXLS" style="height: 460px; margin: 8% 0px;">
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        sistema = $("input#area").val();
        $.funciones.buscarGestionesMuestras('gestion');
        //creacion de tabs con opciones
        $("#options").tabs({
            activate : function(event, ui) {
                //ocultar botones PDF - EXCEL
                $("div.resumen-date,div.detalle-date,div.superficie-date,div.volumen-date").hide();
            }
        });

        //boton buscar
        $("button#btn-search-gestiones").on("click", function() {
            gestion = $("select#gestion").find(":selected").val();

            if (gestion != '') {
                //llenar cultivos de gestiones anteriores
                $.getJSON("control/index.php", {
                    mdl : 'busqueda',
                    opt : 'buscar',
                    pag : 'cultivos_anteriores',
                    gestion : gestion
                }, function(json) {
                    if(json.msg == 'OK'){
                        $("select#cultivo").empty().append("<option value=''>[ Seleccionar cultivo ] </option>");
                        $.each(json.cultivos,function(id,cultivo){
                            $("select#cultivo").append("<option value='"+cultivo+"'>"+cultivo+"</option>");                        
                        });                        
                    }else{
                        $("select#cultivo").empty().append("<option value=''>[ No existen cultivos ] </option>").attr('disabled','disabled');    
                    }
                    $("button#btn-detalle-report").attr('disabled','disabled');
                    $("select#cultivo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");                    
                });
                $("input#f_desde").val("01-01-" + gestion);
                $.funciones.gestionesAnterioresCalendarioInicial("input", "f_desde", gestion);
                $.funciones.gestionesAnterioresCalendarioFinal("input", "f_hasta", gestion);

                //mostrar opciones
                $("div#options").fadeIn();
            } else {
                $(".alert>label").empty().append("Debe seleccionar una gestion");
                $(".alert").show().fadeOut(3000);
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //boton generar resumen
        $("button#btn-rango-resumen-report").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (desde != '') {
                $("div.resumen-date").fadeIn();
                //mostrar boton PDF-EXCEL
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en PDF-resumen
        $("button#pdf-date ").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            gestion = $("select#gestion").find(":selected").val();
            if (desde != '') {
                var win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=resumen&sistema=' + sistema + '&gestion=' + gestion + '&desde=' + desde + '&hasta=' + hasta, '_blank');
                win.focus();
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en EXCEL-resumen
        $("button#xls-date").on("click", function() {
            gestion = $("select#gestion").find(":selected").val();
            $.get('control/index.php', {
                mdl : 'busqueda',
                opt : 'xls',
                pag : 'resumen',
                sistema : sistema,
                gestion : gestion,
                desde : desde,
                hasta : hasta
            }, function(data) {
                if (data != 'error')
                    $(".resumenXLS").empty().append("<iframe class='iresumen' style='display:none' src='" + data + "'></iframe>");
                else {
                    $("#advertencia").show().fadeOut(3000);
                }
            });
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //boton generar detalle
        $("button#btn-detalle-report").on("click", function() {
            cultivo = $("select#cultivo").find(":selected").val();
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (cultivo != '') {
                if (desde != '') {
                    $("div.detalle-date").fadeIn();
                    //mostrar boton PDF-EXCEL
                } else {
                    $(".alert>label").empty().append(" Fecha invalida");
                    $(".alert").show().fadeOut(3000);
                }
            } else {
                $(".alert>label").empty().append(" Debe seleccionar cultivo");
                $(".alert").show().fadeOut(3000);
            }

        });
        //hacer click en PDF-detalle
        $("button#pdf-detalle ").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            gestion = $("select#gestion").find(":selected").val();
            if (desde != '') {
                var cultivo = $("select#rango-cultivo").find(":selected").val();
                var win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=detalle&sistema=' + sistema + '&gestion=' + gestion + '&cultivo=' + cultivo + '&desde=' + desde + '&hasta=' + hasta, '_blank');
                win.focus();
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en EXCEL-detalle
        $("button#xls-detalle").on("click", function() {
            gestion = $("select#gestion").find(":selected").val();
            var cultivo = $("select#rango-cultivo").find(":selected").val();
            if (desde != '') {
                tipo = $("input#tipo-rango").val();
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'detalle',
                    sistema : sistema,
                    gestion : gestion,
                    desde : desde,
                    hasta : hasta,
                    cultivo : cultivo
                }, function(json) {
                    if (json.msg != 'error') {
                        $(".resumen-date").fadeIn();
                        $(".ifr-resumen").removeAttr('src').attr('src', json.xls);
                    } else {
                        $(".resumen-date").hide();
                        $(".alert>label").empty().append("Error al crear archivo");
                        $("#advertencia").show().fadeOut(3000);
                    }
                });
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //boton generar resumen-superficie
        $("button#btn-superficie-report").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (desde != '') {
                $("div.superficie-date").fadeIn();
                //mostrar boton PDF-EXCEL
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en PDF-superficie
        $("button#pdf-superficie ").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            gestion = $("select#gestion").find(":selected").val();
            if (desde != '') {
                var win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=superficie&sistema=' + sistema + '&gestion=' + gestion + '&desde=' + desde + '&hasta=' + hasta, '_blank');
                win.focus();
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en EXCEL-superficie
        $("button#xls-superficie").on("click", function() {            
            desde = $('input#f_desde').val();            
            if (desde != '') {
                gestion = $("select#gestion").find(":selected").val();
                hasta = $('input#f_hasta').val();
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'superficie',
                    desde:desde,
                    hasta:hasta,
                    gestion : gestion
                }, function(json) {
                    if (json.msg != 'error') {
                        $(".resumenXLS").empty().append("<iframe class='iresumen' style='display:none' src='" + json.xls + "'></iframe>");
                    } else {
                        $(".resumen-date").hide();
                        $(".alert>label").empty().append("Error al crear archivo");
                        $(".alert").show().fadeOut(3000);
                    }
                });
            } else {
               $(".alert>label").empty().append("Fecha invalida");
               $(".alert").show().fadeOut(3000);
            }
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //boton generar resumen-volumen
        $("button#btn-volumen-report").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (desde != '') {
                $("div.volumen-date").fadeIn();
                //mostrar boton PDF-EXCEL
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en PDF-volumen
        $("button#pdf-volumen ").on("click", function() {
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            gestion = $("select#gestion").find(":selected").val();
            if (desde != '') {
                var win = window.open('control/index.php?mdl=busqueda&opt=pdf&pag=volumen&sistema=' + sistema + '&gestion=' + gestion + '&desde=' + desde + '&hasta=' + hasta, '_blank');
                win.focus();
            } else {
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        //hacer click en EXCEL-volumen
        $("button#xls-volumen").on("click", function() {
            desde = $('input#f_desde').val();            
            if (desde != '') {
                gestion = $("select#gestion").find(":selected").val();
                hasta = $('input#f_hasta').val();
                $.getJSON('control/index.php', {
                    mdl : 'busqueda',
                    opt : 'xls',
                    pag : 'volumen',
                    desde:desde,
                    hasta:hasta,
                    gestion : gestion
                }, function(json) {
                    if (json.msg != 'error') {
                        $(".iresumen").removeAttr('src').attr('src', json.xls);
                    } else {
                        $(".alert>label").empty().append("Error al crear archivo");
                        $(".alert").show().fadeOut(3000);
                    }
                });
            } else {
               $(".alert>label").empty().append("Fecha invalida");
               $(".alert").show().fadeOut(3000);
            }
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        function cultivoSeleccionado(){
            $("select#cultivo").selectmenu({
            change : function(event, ui) {
            }
            });
        }
            //cargar variedad en campo oculto
        $("select#cultivo").on("selectmenuchange", function(event, ui) {
            var cultivo= $(this).find(':selected').val();
            if (cultivo != ''){
                $("button#btn-detalle-report").removeAttr('disabled');
            }else{
                $("button#btn-detalle-report").attr('disabled','disabled');
            }
        
        });
    }); 
</script>