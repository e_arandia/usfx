<h2>Agregar costo  de cultivo por hectarea </h2>
<div class="entry">
    <form id="costo" autocomplete="off" class="seguimiento">
        <div>
            <label>Cultivo</label>
            <input id="cultivo" name="cultivo"/>
            <span class="checked" style="margin-top: 33em;"></span>
        </div>

        <div>
            <label>Inscripci&oacute;n</label>
            <input id="inscripcion" class="number" name="inscripcion" type= "text" value="0.0"/>
        </div>
        <div>
            <label>Inspecci&oacute;n de campo</label>
            <input id="inspeccion" class="number" name="inspeccion" type= "text" value="0.0"/>
        </div>
        <div>
            <label>An&aacute;lisis y Etiquetaci&oacute;n</label>
            <input id="analisis" class="number" name="analisis" type="text" value="0.0"/>
        </div>
        <div style="margin-left:64%">
            <button id="cultivo" class="btn btn-success" style="margin-top: -7%">
                <span class="glyphicon glyphicon-plus-sign"></span> Registrar
            </button>
            <button id="cancelar-cultivo" class="btn btn-success" style="margin-top: -7%">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>
        </div>
    </form>
    <div style="margin-bottom: 2em">
        &nbsp;&nbsp;
    </div>
    <?php
    include '../vista/error/errores.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        
        $("button.cancelar-cultivo").click(function() {
            $("span.checked").empty();
            $(".new_cultivo").slideUp("slow");
        });
        
        $("input#cultivo").keyup(function() {
            var cultivo = $(this).val();
            if (cultivo != '') {
                $.getJSON("control/index.php", {
                    mdl : "administracion",
                    pag : "verCultivo",
                    cultivo : cultivo
                }, function(json) {
                    if (json.cultivo) {
                        $(".checked").empty().removeClass('libre').append('Ya existe el cultivo').addClass('ocupado');
                        $("input#cultivo").removeAttr('style').css({
                            "color" : "#FB7535",
                            "font-style" : 'italic'
                        });
                        $(".btncosto").hide();
                    } else {
                        $(".checked").empty().removeClass('ocupado').append('Nuevo cultivo').addClass('libre');
                        $("input#cultivo").removeAttr('style');
                        $(".btncosto").show();
                    }

                });
            } else {
                $(".checked").empty().removeAttr('style');
                $("input#cultivo").removeAttr('style');
            }
        });
        $("#costo").ajaxForm({
            url : 'control/index.php',
            data : {
                mdl : 'administracion',
                pag : 'agregar'
            },
            type : 'post',
            resetForm : true,
            beforeSubmit : showRequest,
            success : showResponse
        });
        
        function showRequest(formData, jqForm, options) {
            var mensaje = '';
            var form = jqForm[0];
            if ($("#cultivo").val() == '') {
                mensaje += '<div>- Escriba nombre de cultivo</div>';
                $("#cultivo").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#nombre").css({
                    backgroundColor : ""
                });
            }
            if (!form.inspeccion.value || form.inspeccion.value < 0) {
                mensaje += '<div>- Inspecci&oacute;n inv&aacute;lido</div>';
                $("#inspeccion").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#inspeccion").css({
                    backgroundColor : ""
                });
            }
            if (!form.inscripcion.value || form.inscripcion.value < 0) {
                mensaje += '<div>- Inscripci&oacute;n inv&aacute;lido </div>';
                $("#inscripcion").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#inscripcion").css({
                    backgroundColor : ""
                });
            }
            if (!form.analisis.value || form.analisis.value < 0) {
                mensaje += '<div >- An&aacutelisis inv&aacute;lida</div>';
                $("#analisis").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#analisis").css({
                    backgroundColor : ""
                });
            }
            if (mensaje != '') {
                $("#errores div p .mensaje").empty().append('Se han detectado los siguientes errores: <br>' + mensaje);
                $("#errores").css('margin-top', '4em').fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'error','':
                $.funciones.mostrarMensaje('error', statusText);
                $.funciones.ocultarMensaje(2000);
                break;
            case 'OK':
                $(".checked").empty();
                $.ajax({
                    type : "GET",
                    url : url,
                    data : {
                        mdl : 'administracion',
                        pag : 'costos'
                    },
                    beforeSend : function() {
                        $.funciones.mostrarMensaje('wait', 'Cargando...');
                    },
                    success : function(data) {
                        $.funciones.ocultarMensaje(200);
                        $(".post").empty().append(data);
                    }
                });
                break;
            }
            $(".checked").empty();
        }

    })
</script>