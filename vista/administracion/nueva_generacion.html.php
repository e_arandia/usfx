<h2>Nueva Generaci&oacute;n</h2>
<div class="entry">
    <form id="frmgeneracion" class="nuevoform" style="padding: 1% 4%">
        <div>
            <label>Categoria</label>
            <select id="categoria" name="categoria">
                <option value="">[Seleccionar Categoria]</option>
            </select>
        </div>
        <div>
            <label>Generaci&oacute;n</label>
            <input id="generacion" name="generacion" type="text"/>
            <span class="checked" style="margin-top: 39em;"></span>
        </div>
        <div style="margin-left: 65%" id="enviar">
            <button id="generacion" class="btn btn-success">
                <span class="glyphicon glyphicon-plus-sign"></span> Agregar
            </button>
            <button id="cancel" class="btn btn-success" type="reset">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>  
            <input type="hidden" id="idc" />
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("select#categoria").selectmenu().selectmenu("menuWidget").addClass("overflow");

        $("button#cancel").click(function() {
            $("#agregar_catgen").empty();
        });
        $("input#generacion").keyup(function() {
            var generacion = $(this).val();
            var categoria = $("select#categoria").find(':selected').val();
            if (generacion != '' && categoria != '') {
                $.getJSON("control/index.php", {
                    mdl : "administracion",
                    pag : "verificarCatGen",
                    tp : 'generacion',
                    nombre : generacion,
                    categoria:categoria
                }, function(json) {
                    if (json.estado == 'existe') {
                        $(".checked").empty().removeClass('libre').append('Ya existe la generacion').addClass('ocupado');
                        $("input#generacion").removeAttr('style').css({
                            "color" : "#FB7535",
                            "font-style" : 'italic'
                        });
                        $(".btncosto,#enviar").fadeOut();
                    } else {
                        $(".checked").empty().removeClass('ocupado').append('Nueva generacion').addClass('libre');
                        $("input#generacion").removeAttr('style');
                        $(".btncosto, #enviar").show();                        
                    }

                });                
            }else{
                $("#enviar").show();
                $(".checked").hide();
            }
        });
        $.getJSON('control/index.php', {
            mdl : 'administracion',
            pag : 'categoria'
        }, function(data) {
            $.each(data, function(index, value) {
                $("#categoria").append('<option value="' + index + '">' + value + '</option>');
            });
        });
        $("#frmgeneracion").ajaxForm({
            url : 'control/index.php?mdl=administracion&pag=addGeneracion',
            type : 'post',
            resetForm : true,
            beforeSubmit : showRequest,
            success : showResponse
        });
        function showRequest(formData, jqForm, options) {
            var mensaje = '';
            var form = jqForm[0];

            if (!form.categoria.value) {
                mensaje += '<div>- Categoria</div>';
                $("#categoria").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#categoria").css({
                    backgroundColor : ""
                });
            }
            if ($("input#generacion").val() == '') {
                mensaje += '<div>- Nombre Generacion</div>';
                $("#generacion").css({
                    backgroundColor : "#f5c9c9"
                });
            } else {
                $("#generacion").css({
                    backgroundColor : ""
                });
            }

            if (mensaje != '') {
                $("#errores div p .mensaje").empty().append('Se han detectado los siguientes errores: <br>' + mensaje);
                $("#errores").css('margin-top', '4em').fadeIn('slow');
                return false;
            } else {
                $("#errores").fadeOut('slow');
                return true;
            }
        }

        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
                case 'OK':
                    $(".checked").empty();
                    $.funciones.mostrarMensaje('ok', 'Registro Exitoso!!!');
                    $.funciones.ocultarMensaje(2000);
                    setTimeout(function() {
                        $.ajax({
                            type : "GET",
                            url : url,
                            data : {
                                mdl : 'login',
                                pag : 'categoria'
                            },
                            beforeSend : function() {
                                $.funciones.mostrarMensaje('wait', 'Cargando...');
                            },
                            success : function(data) {
                                $.funciones.ocultarMensaje(200);
                                $(".post").empty().append(data);
                            }
                        });
                    }, 1000);
                    break;
                default:
                    $.funciones.mostrarMensaje('error', statusText);
                    $.funciones.ocultarMensaje(2000);
                    break;
            }
        }

    })
</script>