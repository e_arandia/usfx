<h2>costos de certificaci&oacute;n y fiscalizaci&oacute;n </h2>
<div id="calendar" style="width: 66.5%;">
    <div class="head" style="height: 3em;">
        <div class="htitle">
            <label style="width:10em;padding-top: 1%;">Cultivo</label>
            <label style="width:9.1em;padding-top: 1%;">
                Inscripci&oacute;n</label>
            <label style="width:9em;padding-top: 1%;">
                Inspecci&oacute;n de campo</label>
            <label style="width:9em;padding-top: 1%;">
                An&aacute;lisis y Etiquetaci&oacute;n</label>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>

        </div>
    </div>
    <div class="body" style="width:100%">
        <?php

while ($datos = DBConnector::objeto()){
        ?>
        <div class="nSolicitud cultivo" style="height:24px; width: 10em">
            <label><?php   echo $datos -> cultivo; ?></label>
        </div>
        <div class="productor inscripcion" style="height:24px;width:9em;">
            <label> <?php   echo $datos -> inscripcion; ?></label>
        </div>
        <div class="productor inspeccion" style="height:24px;width:9em;">
            <label> <?php   echo $datos -> inspeccion; ?></label>
        </div>
        <div class="productor analisis" style="height:24px;width:9em;">
            <label> <?php   echo $datos -> analisis_etiqueta; ?></label>
        </div>
        <div class="sistema options" style="height: 24px; width: 8.5em;">
            <div class="editar">
                <input name="upd" type="image" title="Editar" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id; ?>" />
            </div>
            <div class="eliminar">
                <input name="del" type="image" title="Eliminar" src="images/eliminar24.png" style="position: relative;top: -2.5em;right: -1em;" value="<?php echo $datos -> id; ?>" />
            </div>
        </div>
        <?php
		}
        ?>
        <input id="id" type="hidden"/>
    </div>
    <?php
	include '../vista/confirmacion/confirmar.html';
    ?>
    <div>     
       <button id="add_cultivo" class="btn btn-success">
                <span class="glyphicon glyphicon-plus-sign"></span> Agregar cultivo
            </button>
        
    </div>  
</div>
    <div class="new_cultivo" style="position: relative; margin-top: 5%;">
    

</div>

<script type="text/javascript">
		$(document).ready(function() {		
		var celdas = ['cultivo','inscripcion','inspeccion','analisis','options'];
        $.funciones.alternarColores(celdas);
        var opciones = ['editar','eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones); 
		/** agregar costo nuevo cultivo */
		$("#add_cultivo").css('cursor', 'pointer').click(function() {
			$.get('control/index.php', {
				mdl : 'administracion',
				pag : 'costo_cultivo'
			}, function(response) {
			    $('.new_cultivo').empty().append(response).show();
			});
		});

		/**eliminar costo de cultivo*/
		$("input[name=del]").mouseover(function() {
			$("#id").val($(this).val());
		}).click(function() {
			$(".message").empty().append('Desea eliminar el costo de este cultivo?');
			$('#dialog').dialog({
				title: 'Confirmar',
				dialogClass: "no-close",
				resizable : false,
				modal : true,
				width : 260,
				buttons : {
					"Aceptar" : function() {
						$(this).dialog("close");
						var id = $("#id").val();
			            $.get('control/index.php?mdl=administracion&pag=eliminar&id=' + id, function(data) {                        			            	
				        if (data == 'OK') {				        	
				        	$.funciones.mostrarMensaje('ok', 'Cultivo eliminado');
					        $.funciones.ocultarMensaje(2000);
					        var url = 'control/index.php';
					        $.get(url, {
					        	mdl : 'administracion',
						        pag : 'costos'
					        }, function(data) {
					        	$(".post").empty().append(data);
					        });
				        } else {
					        $.funciones.mostrarMensaje('error', 'Ocurrio un error al intentar eliminar el costo. Intente nuevamente');
					        $.funciones.ocultarMensaje(2500);
				        }
			            });					
					},
					"Cancelar" : function() {
						$(this).dialog("close");
					}
				}
			});
			/**/
		});
		/**actualizar datos costo de cultivo*/
		$("input[name=upd]").mouseover(function() {
			$("#id").val($(this).val());
		}).click(function() {
			var id = $("#id").val();
			$.post('control/index.php?mdl=administracion&pag=actualizarDatos&id=' + id, function(data) {
				$(".new_cultivo").empty().append(data);
			});
		});
		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	}); </script>