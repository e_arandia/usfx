<h2>Categoria y Generacion de semillas </h2>
<div class="new-categoria-generacion">
        <button id="categoria_add" class="btn btn-success">
             <span class="glyphicon glyphicon-plus-sign"></span> Nueva Categoria
        </button>
        <button id="generacion_add" class="btn btn-success">
            <span class="glyphicon glyphicon-plus-sign"></span> Nueva Generacion
        </button>
        <input type="hidden" name="opcion"/>
    </div> 
<div id="calendar" style="width: 41%;margin-left:25%">
    <div class="head" style="height: 2em">
        <div class="htitle">
            <label style="width:10em;padding-top: 1%;">Categoria</label>
            <label style="width:9.1em;padding-top: 1%;">Generaci&oacute;n</label>
            <label style="width:8.5em;padding-top: 1%;">Opciones </label>
        </div>
    </div>
    <div class="body" style="width:98%">
        <?php
while ($fila = DBConnector::objeto()) {
        ?>
        <div class="nSolicitud cultivo" style="height:24px; width: 10em">
            <label><?php   echo $fila -> categoria; ?></label>
        </div>
        <div class="productor inscripcion" style="height:24px;width:9em;">
            <label> <?php   echo $fila -> generacion; ?></label>
        </div>
        <div class="sistema options" style="height: 24px; width: 8.5em;">
            <div class="editar">
                <input name="upd" type="image" title="Editar" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $fila -> id_generacion; ?>" />
            </div>
            <div class="eliminar">
                <input name="del" type="image" title="Eliminar" src="images/eliminar24.png" style="position: relative;top: -2.5em;right: -1em;" value="<?php echo $fila -> id_generacion; ?>" />
            </div>
        </div>
        <?php
        }
        ?>
        <input id="id" type="hidden"/>
    </div>
     
</div>
<div id="agregar_catgen" style="margin-top: 40%;position:absolute;">
</div>
<?
include '../vista/confirmacion/mensaje.html';
include '../vista/confirmacion/confirmar.html';
?>
<script type="text/javascript">
    $(document).ready(function() {
        var celdas = ['cultivo','inscripcion','options'];
        $.funciones.alternarColores(celdas);
        var opciones = ['editar','eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
        /** elegir accion a realizar */
        $("button#categoria_add").css('cursor', 'pointer').click(function() {
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'administracion',
                    pag : 'frmcategoria'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $("#agregar_catgen").empty().append(data);
                }
            });
        });

        $("button#generacion_add").css('cursor', 'pointer').click(function() {
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'administracion',
                    pag : 'frmgeneracion'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $("#agregar_catgen").empty().append(data);
                }
            });
        });

        /**eliminar generacion*/
        $("input[name=del]").on('mouseover',function() {
            $("#id").val($(this).val());
        }).on('click',function() {
            $(".message").empty().append('Desea eliminar la generacion?');
            $('#dialog').dialog({
                title : 'Confirmar',
                dialogClass: "no-close",
                resizable : false,
                modal : true,
                width : 205,
                buttons : {
                    "Aceptar" : function() {
                        $(this).dialog("close");
                        var id = $("#id").val();
                        $.get('control/index.php?mdl=administracion&pag=del_generacion&id=' + id, function(data) {
                            if (data == 'OK') {
                                $.funciones.mostrarMensaje('ok', 'Generacion eliminada');
                                $.funciones.ocultarMensaje(2000);
                                var url = 'control/index.php';
                                $.funciones.recargarVerDatosAdmin('login','categoria');
                            } else {
                                $.funciones.mostrarMensaje('error', data);
                                $.funciones.ocultarMensaje(2500);
                            }
                        });
                    },
                    "Cancelar" : function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
        /**actualizar datos Generacion*/
        $("input[name=upd]").on('mouseover',function() {
            $("#id").val($(this).val());
        }).on('click',function() {
            var id = $("#id").val();
            $.ajax({
                type : "POST",
                url : "control/index.php",
                data : {
                    mdl : 'administracion',
                    pag : 'updGeneracion',
                    id : id
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $("#agregar_catgen").empty().append(data);
                }
            });
        });
        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    });
 </script>