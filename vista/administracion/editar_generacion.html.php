<h2> Editar Generaci&oacute;n</h2>
<div class="entry">
    <form id="actualizacion" class="nuevoform" style="padding:1% 4%">
        <?php       
while ($generacion = DBConnector::objeto())
{
        ?>
        <div style="width: 34em;">
            <label style="width: 10em;">Categor&iacute;a</label>
            <input name="categoria" value="<?php  echo $generacion -> categoria; ?>" style="border: none" disabled="disabled"/>
        </div>
        <div>
            <label style="width: 28%;">Nombre Generaci&oacute;n</label>
            <input id="generacion" name="generacion" value="<?php  echo $generacion -> generacion; ?>" style="width: 40%;" autocomplete="off"/>
        </div>
        <div>
        <input type="hidden" name="id" value="<?php echo $generacion -> id_generacion; ?>"/>
        </div>
        <?php
        }
        ?>
        <div style="margin-left:65%;">
            <input name="mdl" type="hidden" value="certificacion"/>
            <input name="opt" type="hidden" value="actualizar"/>
            <input name="pag" type="hidden" value="generacion"/>
            <button id="edt-generacion" class="edt_generacion" type="button">
                Aceptar
            </button>
        </div>
    </form>
</div>
<!--  validacion y envio de datos  -->
<script type="text/javascript">
    $(document).ready(function() {
        $("button.edt_generacion").button({
            icons : {
                primary : "ui-icon-circle-check"
            }
        });
        
        $('button#edt-generacion').on('click',function(){
            $.funciones.editarGeneracion();
        });

    }); 
</script>