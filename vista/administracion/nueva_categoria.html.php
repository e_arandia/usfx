<h2>Nueva categoria</h2>
<div class="entry">
    <form id="categoria" class="nuevoform" style="padding: 1% 4%">
        <div>
            <label>Nombre Categoria</label>
            <input id="categoria" name="categoria" type="text" />
            <span class="checked" style="margin-top: 36em;"></span>
        </div>
        <div style="margin-left: 65%">       
            <input name="mdl" type="hidden" value="administracion"/>
            <input name="pag" type="hidden" value="addCategoria"/>     
            <button id="btn-add-categoria" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-plus-sign"></span> Agregar
            </button>
            <button id="btn-cancel-categoria" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>            
        </div>
    </form>
    <div style="margin-bottom: 2em">
        &nbsp;&nbsp;
    </div>
    <?php
    include '../vista/error/errores.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("input#categoria").keyup(function() {
            var categoria = $(this).val();
            
            if (categoria != '') {
                $.getJSON("control/index.php", {
                    mdl : "administracion",
                    pag : "verificarCatGen",
                    tp : 'categoria',
                    nombre : categoria
                }, function(json) {
                    if (json.estado == 'existe') {
                        $(".checked").empty().removeClass('libre').append('Ya existe la categoria').addClass('ocupado');
                        $("input#categoria").removeAttr('style').css({
                            "color" : "#FB7535",
                            "font-style" : 'italic'
                        });
                        $(".btncosto").hide();
                    } else {
                        $(".checked").empty().removeClass('ocupado').addClass('libre');
                        $("input#categoria").removeAttr('style');
                        $(".btncosto").show();
                    }

                });
            }else{
                $("#categoria").show();
                $(".checked").hide();
            }
        });
        $("button#btn-cancel-categoria").on("click",function(){
           $("#agregar_catgen").empty(); 
        });
        $('button#btn-add-categoria').on('click',function(){
            $.funciones.agregarCategoria('categoria');
        });
    })
</script>