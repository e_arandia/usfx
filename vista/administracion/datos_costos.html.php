<h2>Modificar costo de cultivo por hectarea</h2>
<div class="entry">
	<form id="actualizacion" class="seguimiento">
		<?php       
while ($costoCultivo = DBConnector::objeto())
{
		?>
		<div style="width: 27em;">
			<label>Cultivo</label>
			<input id="costo-cultivo" name="cultivo" value="<?php  echo $costoCultivo -> cultivo; ?>"/>
		</div>
		<div style="width: 27em;">
		<label>Inscripci&oacute;n</label>
		<input id="costo-inscripcion" class="number" name="inscripcion" value="<?php  echo $costoCultivo -> inscripcion; ?>"/> Bs.
		</div>
		<div style="width: 27em;">
		<label>Inspecci&oacute;n</label>
		<input id="costo-inspeccion" class="number" name="inspeccion"  value="<?php  echo $costoCultivo -> inspeccion; ?>" /> Bs.
		</div>
		<div style="width: 27em;">
		<label>An&aacute;lisis</label>
		<input id="costo-analisis" class="number" name="analisis"  value="<?php  echo $costoCultivo -> analisis_etiqueta; ?>"/> Bs.
		</div>

		<div>
		<input type="hidden" name="id" value="<?php echo $costoCultivo -> id; ?>"/>
		</div>
		<?php
        }
		?>
		<div>
		    <input name="mdl" type="hidden" value="administracion"/>
		    <input name="pag" type="hidden" value="actualizar"/>
			<button id="enviar-costo-cultivo" class="btn btn-success button-primary" style="margin-top: -7%;" type="button">
                <span class="glyphicon glyphicon-pencil"></span> Modificar
            </button>
		</div>
	</form>
</div>
<!--  validacion y envio de datos  -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#actualizacion>div>label").css({
            'font-weight' : 'bolder',
            'margin-top' : '2.5%'
        });
        $('button#enviar-costo-cultivo').on('click',function(){
            $.funciones.registrarNuevoCosto();    
        });        
        solo_numeros = ['inscripcion','inspeccion','analisis'];
        $.funciones.soloNumeros(solo_numeros);
    }); 
</script>