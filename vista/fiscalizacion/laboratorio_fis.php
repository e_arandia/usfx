<h2 class="title">Muestra de laboratorio</h2>
<div style="float: right; margin-top: -5%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
<div class="entry">
    <form id="muestraForm" class="seguimiento" >

        <div>
            <label>Fecha Recepcion</label>
            <input id="f_recepcion" name="recepcion" type="text"  readonly="true" value="<?php echo date('d-m-Y')?>" style="width:65px"/>
        </div>
        <div>
            <label>Muestra de</label>
            <input class="not-edit" value="Fiscalizacion"/>
        </div>
        <div>
            <label>Numero Campo</label>
            <select id="nrocampo" name="nrocampo">
                <option value="">[ Seleccione campo ]</option>
            </select>
        </div>
        <div id="div-fiscalizacion">
            <label>Semilla</label>
            <select id="fiscalizacion" name="tipo_semilla" style="width: 50%; !important" disabled="disabled">
                <option value="" selected="selected"> [ Seleccione una opcion ] </option>
                <option value="importada">Importada</option>
                <option value="actualizacion">Actualizacion</option>
                <option value="sin_norma">Semilla no cuenta con norma de certificacion</option>
                <option value="con_norma">Semilla con normas especificas</option>
                <option value="uso_propio">Semilla uso propio</option>
                <option value="otro">Otro</option>
            </select>
        </div>

        <div id="cultivoI" style="display: none">
            <label>Cultivo</label>
            <input id="cultivo" name="cultivo" class="not-edit" type="text" readonly="readonly"/>
        </div>
        <div id="variedadI" style="display: none">
            <label>Variedad</label>
            <input id="variedad" name="variedad" class="not-edit" type="text" readonly="readonly"/>
        </div>
        <div id="sorim" style="display:none">
            <label>Origen</label>
            <input id="origen" name="origen" type="text" autocomplete="off"/>
        </div>
        <div id="import" style="height: 30em;display:none;">
            <div style="float:left;width:49%;">
                <div style="float: left; margin-bottom: 1%; clear: left; width: 250px;">
                    <label>Especie</label>
                    <input id="especie" name="especie" type="text" style="width: 205px;">
                </div>
                <div style="float: left; width: 250px;">
                    <label>Variedad</label>
                    <input id="ivariedad" name="analisis" type="text" style="width: 205px;">
                </div>
                <div style="float: left; width: 250px;">
                    <label>Categoria</label>
                    <input id="categoria" name="categoria" type="text" style="width: 205px;">
                </div>
                <div style="float: left; width: 250px;">
                    <label>Origen</label>
                    <input id="origen" name="origenImp" type="text" style="width: 205px;">
                </div>
            </div>
            <div style="float:right;width:50%;">
                <div style="float: right; width: 250px;">
                    <label style="width: 235px;">Cantidad</label>
                    <input id="cantidad" name="cantidad" type="text">
                </div>
                <div style="width: 250px; float: right; margin-top: 1%;">
                    <label style="width: 235px;">Certificado fitosanitario</label>
                    <input id="certificado" name="certificado" type="text">
                </div>
                <div style="width: 250px; float: right;">
                    <label style="text-transform:none;width: 235px;">Aduana de ingreso</label>
                    <input id="aduanaingreso" name="aduanaingreso" type="text">
                </div>
                <div style="width: 250px; float: right;">
                    <label style="width: 235px;">Destino semilla</label>
                    <input id="datossemilla" name="datossemilla" type="text">
                </div>

            </div>
            <div style="margin-bottom: 1%; clear: left; width: 250px;">
                <label style="text-transform:none;width: 235px;">Area de distribucion y/o comercialización</label>
                <input id="adistribucion" name="adistribucion" type="text" style="width: 445px;">
            </div>

        </div>

        <div style="float: left; width: 50%;">
            <label style="width: 70%;">TIPO DE ANÁLISIS</label>
        </div>
        <div id="radio" style="margin-top:0;">
            <input type="checkbox" id="germinacion" name="germinacion" value="1" style="width: 3%"/>
            <label for="germinacion" style="width: 23%">Germinacion</label>

            <input type="checkbox" id="pureza" name="pureza" value="1" style="width: 3%"/>
            <label for="pureza" style="width: 10%">Pureza</label>

            <input type="checkbox" id="humedad" name="humedad" value="1" style="width: 3%"/>
            <label for="humedad" style="width: 18%">Humedad</label>
        </div>

        <div id="test">
            <input id="sistema" name="sistema" type="hidden" value="2"/>
            <input id="mdl" name="mdl" type="hidden" value="fiscalizacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="muestra"/>

        </div>
        <div style="float:right; margin-top: -10%;display: none" id="btn-muestra">

            <button id="enviar-muestra" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>

    </form>
    <?php
    include '../vista/error/errores.php';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("select#fiscalizacion").selectmenu();
        $("select#nrocampo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        //calendario
        $("#f_recepcion").calRecibir('f_recepcion');
        //cargar muestras de campos
        $.funciones.cargarCampos('fiscalizacion', 'nrocampo', 2);
        //tipos de analisis
        $("#radio").controlgroup();

        //al cambiar el numero de campo habilitar el select de semilla
        $("select#nrocampo").selectmenu({
            change : function(event, ui) {
            }
        });

        $("select#nrocampo").on("selectmenuchange", function(event, ui) {
            var nrocampo = $(this).find(':selected').val();
            if (nrocampo != "") {
                $("select#fiscalizacion").selectmenu({
                    disabled : false
                });
                $.funciones.cultivoVariedadFiscal('fiscalizacion', nrocampo);
            } else {

                $("input#cultivo").val('');
                $("input#variedad").val('');
                $("div#import").fadeOut();

                $("select#fiscalizacion").selectmenu({
                    disabled : true
                });
            }
        });

        //si el tipo de semilla es importada
        $("select#fiscalizacion").selectmenu({
            change : function(event, ui) {
            }
        });

        $("select#fiscalizacion").on("selectmenuchange", function(event, ui) {
            var tipo = $(this).find(':selected').val();
            if (tipo == "importada") {
                $("div#import").fadeIn();
                $("div#btn-muestra").fadeIn();
                $("div#cultivoI, div#variedadI").fadeOut();
            } else if (tipo != '') {
                $("div#import").fadeOut();
                $("div#btn-muestra").fadeIn();
                $("div#cultivoI, div#variedadI").fadeIn();
            } else {
                $("div#btn-muestra").fadeOut();
            }
        });
    })

</script>
<script type="text/javascript">
    $(document).ready(function() {
        //boton registro de muestra
        $("button#enviar-muestra").on("click", function(event) {
            
        });
    }); 
</script>
