<?php
if (count($semillera) > 0 ){
$width = "width: 85.8%";
$width_td = "width: 25%";
?>
<div class="entry" >
    <form id="filter-box" class="seguimiento" style="padding: 3% 2% 1%; width: 90%;">
        <div style="float: left; margin-top: 0%; width: 22%;">
            <div style="width: 62%;">
                <label>Semillera</label>
                <input id="semillera-filter" style="width: 130px;">
            </div>
        </div>
        <div style="float: left; width: 26%; margin-left: 6%;">
            <label style="width:75%">Semilerista</label>
            <input id="semillerista-filter" style="width: 130px;">
        </div>
        <div style="float: left; width: 15%; margin-left: 3%;">
            <label style="width:75%">Cultivo</label>
            <input id="cultivo-filter" style="width: 100px;">
        </div>
        <div style="margin-right: 0em; width: 50%; margin-top: 2em; margin-left: 80%;">
            <button class="btn btn-success" type="reset">
                <span class="glyphicon glyphicon-trash"></span>
                Limpiar
            </button>
        </div>
    </form>
    <div style="margin-left: 80%; width: 50%;">
            <button id="p1" class="btn btn-success" type="button">A - E</button>
            <button id="p2" class="btn btn-success" type="button">F - J</button>
            <button id="p3" class="btn btn-success" type="button">K - O</button>
            <button id="p4" class="btn btn-success" type="button">P - T</button>
            <button id="p5" class="btn btn-success" type="button">U - Z</button>
    </div>
</div>

<input class="proceso" type="hidden" value="<?php echo $sistema; ?>"/>

<div id="calendar" style="border-bottom: none">
    <div id="table" class="striped collapsible filterable" style="width: 100%">
        <div id="thead" style="z-index:999;">
            <div class="tr">
                <div class="th filter-column" style="width: 9.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Semillera
                </div>
                <div class="th" style="width: 5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Cultivo
                </div>
                <div class="th" style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Solicitud
                </div>
                <div class="th" style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Semilla Sembrada
                </div>
                <div class="th" style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Hoja de Cosecha
                </div>
                <div class="th" style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Laboratorio
                </div>
                <div class="th" style="width: 7em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold">
                    Semilla Producida
                </div>
            </div>
        </div>
        <?php
        if (!empty($semillera)){
        $indice = 0;
        $aux = 0;
        foreach ($semillera as $key => $semilleras) {
        ?>
        <div class="tbody">
            <div class="tr">
                <div class="th"  style="<?php echo $width; ?>; height: 5%;background-color:#80b744;font-weight: bold">
                    <?php
                    echo $semilleras;
                    $counter = 0;
                    ?>
                </div>
            </div>
            <?php
while ($counter < $cantidad[$aux]){
    
            ?>
            <div class="tr fcontrol">
                <div class="td persona"style="border-bottom:0;border-left:none;width:10em;">
                    <?php
                    //nombre de semillerista
                    echo $productor[$indice];
                    ?>
                </div>
                <div class="td cultivo" style="border-bottom:0;border-left:none;width:3em;text-align: center;">
                    <?php
                    Semilla::getCampoCultivoFiscalById($id_solicitud[$counter]);
                    $fila = DBConnector::objeto();
                    $cultivo = (DBConnector::filas()) ? $fila -> cultivo : '-';
                    echo $cultivo;
                    ?>
                </div>
                <div class="td estado" style="border-bottom:0;border-left:none;<?php echo $width_td; ?>">
                    <?php
                    $estado = intval($etapa[$indice]);
                    $id = intval($id_solicitud[$indice]);
                    //echo $estado;
                    switch ($estado) {
                        case 0 :
                             echo "<input type='image' class='proImg' value=$id src='images/0.png' style='position: relative;top: 0.5em;margin-left:20%;'/>";
                            //echo "<img src='images/0.png' class='clickable' >";
                            break;
                        case 1 :
                             echo "<input type='image' class='proImg' value=$id src='images/1.png' style='position: relative;top: 0.5em;margin-left:20%;'/>";
                            //echo "<img src='images/1.png' class='clickable''>";
                            break;
                        case 8 :
                             echo "<input type='image' class='proImg' value=$id src='images/2F.png' style='position: relative;top: 0.5em;margin-left:20%;'/>";
                            //echo "<img src='images/2F.png' class='clickable'>";
                            break;
                        case 9 :
                             echo "<input type='image' class='proImg' value=$id src='images/3F.png' style='position: relative;top: 0.5em;margin-left:20%;'/>";
                            break;
                        case ($estado>=10) :
                             echo "<input type='image' class='proImg' value=$id src='images/4F.png' style='position: relative;top: 0.5em;margin-left:20%;'/>";
                            break;
                    }
                    ?>
                </div>
            </div>
            <?php
            $counter++;
            $indice++;
            }
            $aux++;
            ?>
        </div>
        <?php }
            }
        ?>
    </div>
</div>
<?php
}else{
echo "<div id='advertencia' class='ui-widget'>
<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>
<p>
<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>
No se han registrado semilleras en este proceso
</p>
</div>
</div>";
}
?>
<?
include '../vista/dialogos/registroFiscalizacion.html';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("button#btn-filter").on("click", function() {
            $("form#filter-box").slideToggle();
        });

        //function de busqueda  case-insensitive
        $.extend($.expr[':'], {
            'contains-ci' : function(elem, i, match, array) {
                return (elem.textContent || elem.innerText || $(elem).text() || '').toLowerCase().indexOf((match[3] || '').toLowerCase()) >= 0;
            }
        });
        //busqueda de semilleras
        $("input#semillera-filter").on("keyup", function() {
            //obteniene el texto introducido
            var texto = $(this).val();
                //oculta todas las semilleras
                $("div.tbody>.tr>.th").hide();
                //muestra solo las coincidencias
                $("div.tbody>.tr>.th:contains-ci(\'" + texto + "\')").show();
                //$("#table.collapsible>.collapsed").trigger('stripe');
        });
        //busqueda de semilleristas
        $("input#semillerista-filter").on("keyup", function() {
            var texto = $(this).val();
            $("div.tbody>.tr.fcontrol>.td.persona,div.tbody>.tr.fcontrol>.td.cultivo,div.tbody>.tr.fcontrol>.td.estado").hide();
            $("div.tbody>.tr.fcontrol>.td.persona:contains-ci(\'" + texto + "\')").show(function() {
                $(this).show();
                $(this).siblings().show();
            });
        });
        //busqueda de cultivos
        $('input#cultivo-filter').on('keyup', function() {
            var texto = $(this).val();
            $("div.tbody>.tr.fcontrol>.td.persona,div.tbody>.tr.fcontrol>.td.cultivo,div.tbody>.tr.fcontrol>.td.estado").hide();
            $("div.tbody>.tr.fcontrol>.td.cultivo:contains-ci(\'" + texto + "\')").show(function() {
                $(this).show();
                $(this).siblings().show();
            });
        });
        //filtro por botones
        $('button#p1').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=fiscalizacion&opt=ver&pag=proceso&sistema=fiscalizacion',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p2').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=fiscalizacion&opt=ver&pag=proceso&sistema=fiscalizacion&inicio=F&final=J',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p3').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=fiscalizacion&opt=ver&pag=proceso&sistema=fiscalizacion&inicio=K&final=O',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p4').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=fiscalizacion&opt=ver&pag=proceso&sistema=fiscalizacion&inicio=P&final=T',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p5').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=fiscalizacion&opt=ver&pag=proceso&sistema=fiscalizacion&inicio=U&final=Z',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#thead').scrollFix();
        $("div.oddf").css("width", "71.4%");
        $("div.evenf").css("width", "71.4%");
        
                //crear reporte
        $(".proImg").css('cursor','pointer').on("click", function() {
            var id = $(this).val();
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'registro',
                id : id
            }, function(json) {                
                $("#rsemillera").text(json.semillera);
                $("#rsemillerista").text(json.semillerista);
                //solicitud
                $(".rnosol").text(json.nosolicitud);
                $(".rdepto").text(json.departamento);
                $(".rprov").text(json.provincia);
                $(".rmun").text(json.municipio);
                $(".rcom").text(json.comunidad);
                $(".rfsol").text(json.fsolicitud);
                //semilla
                $(".rcultivo").text(json.cultivo);
                $(".rvariedad").text(json.variedad);
                $(".rlote").text(json.lote);
                //hojacosecha
                $(".rplantaa").text(json.plantaa);
                //semilla producida
                $(".rsemNeta").text(json.semNeta);
                $(".rproducir").text(json.catProducir);
                $(".rnoetiquetas").text(json.noEtiquetas);
                
                $(".rresponsable").text(json.responsable);
                $(".rcampanha").text(json.camapanha);
                $(".rcinscripcion").text(json.inscripcion);
                $(".rcinspeccion").text(json.inspeccion);
                $(".rcanalisis").text(json.analisis);
                $(".rcacondicionamiento").text(json.acondicionamiento);
                $(".rcplantines").text(json.plantines);
                $(".rfcuenta").text(json.fecha_cta);
            });
            $("#regFiscaliza").dialog({
                dialogClass : "no-close",
                title : "Registro de semillerista",
                width : 450,
                buttons : [{
                    text : "Aceptar",
                    click : function() {
                        $(this).dialog("close");
                    }
                }]
            });
        });

        $('#table.striped').bind('stripe', function() {
            $('.tbody', this).each(function() {
                $(this).find('.tr:not(:has(.th))').filter(function() {
                    return this.style.display != 'none';
                }).removeClass('oddf').addClass('evenf').filter(function(index) {
                    return (index % 6) < 3;
                }).removeClass('evenf').addClass('oddf');
            });
        }).trigger('stripe');

        var collapseIcon = 'images/bullet_toggle_minus.png';
        var collapseText = 'ocultar esta seccion';
        var expandIcon = 'images/bullet_toggle_plus.png';
        var expandText = 'mostrar esta seccion';
        $('#table.collapsible .tbody').each(function() {
            $('.oddf, .evenf').css('display', 'none');
            var $section = $(this);
            $section.addClass('collapsed');
            $('<img />').attr('src', expandIcon).attr('alt', expandText).prependTo($section.find('.th')).addClass('clickable').click(function() {
                if ($section.is('.collapsed')) {
                    $section.removeClass('collapsed').find('.tr:not(:has(.th)):not(.filtered)').fadeIn('fast');
                    $(this).attr('src', collapseIcon).attr('alt', collapseText);
                } else {
                    $section.addClass('collapsed').find('.tr:not(:has(.th))').fadeOut('fast', function() {
                        $(".control").css('display', 'none');
                    });
                    $(this).attr('src', expandIcon).attr('alt', expandText);
                }
                $section.parent().trigger('stripe');
            });
        });
        //});
    })
</script>