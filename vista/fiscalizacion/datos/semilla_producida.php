<?php
$width = (($area == 10 || $area == 1 || $area == 2)&& $edit==1110)? "'width:74.5%'" : "'width:82.2%'";
        
include '../vista/dialogos/busquedaAvanzada.html';
?>

<div class="div-filter">

<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 2.9em">
		<div class="htitle">
		    <label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:4.5em;padding-top: 1%;">No Campo</label>
			<label style="width:7.5em;padding-top: 1%;">Semilla Neta</label>
			<label style="width:10em;padding-top: 1%;">Categoria Obtenida</label>
			<label style="width:6.1em;padding-top: 1%;">Numero de etiqueta</label>
			<label style="width:6.1em;padding-top: 1%;">Reporte PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php }?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera odd00">
            <label> <?php   echo $datos -> semillera; ?></label>
        </div>
		<div class="productor">
			<label><?php  echo utf8_encode($datos -> nombre) . ' ' . utf8_encode($datos -> apellido);?></label>
		</div>
		<div class="nSolicitud" style="width: 4.5em">
			<label> <?php  echo $datos -> nro_campo;?></label>
		</div>
		<div class="semillaNeta" style="width: 7.5em">
			<label> 
			    <?php	
			    $neta = ($datos -> nro_etiqueta)?0:(($datos -> nro_etiqueta*50)/1000);
                
                echo $neta;			    
			    ?>
			    
			</label>
		</div>
		<div class="cSembrada">
			<label> <?php	echo $datos -> categoria_obtenida;?></label>
		</div>
		<div class="nroEtiqueta" style="width: 6em">
			<label> <?php  echo $datos -> nro_etiqueta;?></label>
		</div>
		<?php
if (($area == 10 ||$area == 1 || $area == 2) &&  $edit==1110){
		?>
		<div class="sistema options" style="height: 28px; width: 8.4em;">
			<div>
				<input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_solicitud;?>" />
			</div>
			<?php if ($area == 10){?>  
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_solicitud;?>" />
			</div>
			<?php } ?>
		</div>
		<?php
		}
		}
		?>
		<input id="idsolicitud" type="hidden"/>
		<input id="control" type="hidden" value="<?php echo $area;?>" />
	</div>
</div>

</div>
<div class="div-new" style="display:none;">
    
</div> 

<script type="text/javascript">
    $(document).ready(function() {
        //busqueda por filtro
        $("button#search-filter").on("click",function(){
            $.funciones.copiarBusquedaDatos('filter-box');
            console.log($("form#filter-box").serialize());
        })
        
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'semillaP',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {

                $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
                $.funciones.cargarImagen('semillaPfis', 'fiscalizacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
            });
        });
    }); 
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var celdas = ['semillera','productor','nSolicitud','categoria','generacion','semillaNeta','cSembrada','nroEtiqueta','rango'];
        $.funciones.alternarColores(celdas);
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {

		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#idsemillap").val($(this).val());
		}).click(function() {
			if(confirm('Esta seguro de eliminar el registro?')) {
				var imagen = '<img src="images/loader_3a.gif" />';
				$('.post').block({
					theme : true,
					title : 'Sistema CERFIS',
					message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
				});
				var id = $("#idsemillap").val();
				$.post('control/certificacion/certificacion.ctrl.php', {
					page : 'eliminar',
					tbl : 'semillap',
					id : id
				}, function(data) {
					if(data == 'OK') {
						$('.post').unblock({
							theme : true,
							title : 'Sistema CERFIS',
							message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4.gif'/></div>",
							timeout: 2000
						});
						var area = $("#control").val();
						$.post('control/usuario/usuario.ctrl.php', {
							page : 'vsolicitud',
							area : area
						}, function(data) {
							$(".post").empty().append(data);
						});
					} else {
						$(".post").append(data + ' No se puede eliminar la solicitud');
					}

				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#idsemillap").val($(this).val());
		}).click(function() {
			var id = $("#idsemillap").val();
			$.post('control/certificacion/certificacion.ctrl.php?', {
				page : 'actualizar',
				tbl : 'semillap',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
