<?php

$width = (($area== 10 || $area== 1 || $area== 2)&& $edit==1110)? "'width:214%'" : "'width:136%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Cuentas de fiscalización:</h2>";
} 
?>

<div class="div-filter">
<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 3.8em">
		<div class="htitle">
			<label style="width:12em;padding-top: 1%;">Responsable</label>
			<label style="width:10em;padding-top: 1%;">Semillera</label>
			<label style="width:12em;padding-top: 1%;">Semillerista</label>
			<label style="width:7.5em;padding-top: 1%;">Cultivo</label>
			<label style="width:8.5em;padding-top: 1%;">Variedad</label>
			<label style="padding-top: 1%; width: 11em;">Analisis de Laboratorio y Etiquetaci&oacute;n</label>
			<label style="padding-top: 1%; width: 6em;">Inscripci&oacute;n</label>
			<label style="width:6em;padding-top: 1%;">Inspecci&oacute;n</label>
			<label style="width:6em;padding-top: 1%;">An&aacute;lisis</label>
			<label style="padding-top: 1%; width: 8em;">Ver Detalle</label>  
			<label style="width:6em;padding-top: 1%;">Reporte PDF</label>
			<?php if (($area== 10 || $area== 1 || $area== 2)&& $edit==1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php }?>
		</div>
	</div>
	<div class="body">
		<?php
foreach ($responsable as $id => $valor) {
		?>
		<div class="productor resp" style="width:12em;">
			<label> <?php    echo utf8_encode($valor);?></label>
		</div>
		<div class="semillera" style="width:10em;">
			<label> <?php   echo strtoupper($semillera[$id]);?></label>
		</div>
		<div class="productor semillerista" style="width:12em;">
			<label> <?php	echo utf8_encode($semillerista[$id]);?></label>
		</div>
		<div class="cultivo" style="width:7.5em;">
			<label> <?php 	echo $cultivo[$id];?></label>
		</div>
		<div class="variedad" style="width:8.5em;">
			<label> <?php  echo utf8_encode($variedad[$id]);?></label>
		</div>
		<div class="sInscrita ale" style="width:11em;">
			<label> <?php	echo $bolsa_etiqueta[$id];?></label>
		</div>
		<div class="sInscrita inscripcion" style="width:6em;">
			<label> <?php	echo $inscripcion[$id];?></label>
		</div>
		<div class="sInscrita inspeccion" style="width:6em;">
			<label> <?php	echo $inspeccion[$id];?></label>
		</div>
		<div class="sInscrita analisis" style="width:6em;">
            <label> <?php   echo $analisis[$id];?></label>
        </div>
		<div class="fecha detalle" style="width:7.9em;">
            <a id="<?php echo $isemilla; ?>" class="glyphicon glyphicon-list-alt btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $isemilla; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>

		<?php
if (($area== 10 || $area== 1 || $area== 2)&& $edit==1110){
		?>
		<div class="sistema options" style="height: 27px; width: 8.4em;">
			<div>
				<input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_cuenta;?>" />
			</div>
			<?php if ($area== 10){?>  
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_cuenta;?>" />
			</div>
			<?php } ?>
		</div>
		<?php
		}
		}
		?>
		<input id="icuenta" type="hidden"/>
		<input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>
</div>
<?php
    include '../vista/dialogos/cuotaCuenta.html';
    ?>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function() {
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'cuenta',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nosolicitud, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label>" + json.responsable[index] + "</label> </div>";
                            div += "<div class=\"sistema sistem\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"semillera\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"dpto\"><label>" + json.departamento[index] + "</label> </div>";
                            div += "<div class=\"prov\"><label>" + json.provincia[index] + "</label> </div>";
                            div += "<div class=\"municipio\"><label>" + json.municipio[index] + "</label> </div>";
                            div += "<div class=\"comunidad\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label>" + json.fecha[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['resp','semillera','semillerista','cultivo','ale','inscripcion','inspeccion','analisis','total','fpago'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $("div.body").show();
            }
        });
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : '','margin-top':'0'});
        });
        
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'cuenta',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
                $.funciones.cargarImagen('cuentafis', 'fiscalizacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();                
                $(".post").empty().append(html);
            });
        });
    }); 
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var celdas = ['resp','semillera','semillerista','cultivo','variedad','ale','inscripcion','inspeccion','analisis','total','f-pago','monto'];
        $.funciones.alternarColores(celdas);
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {

		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#icuenta").val($(this).val());
		}).click(function() {
			
			if(confirm('Esta seguro de eliminar el registro?')) {
				var imagen = '<img src="images/loader_3a.gif" />';
				$('.post').block({
					theme : true,
					title : 'SICERFIS',
					message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
				});
				var id = $("#icuenta").val();
				$.post('control/certificacion/certificacion.ctrl.php', {
					page : 'eliminar',
					tbl : 'cuenta',
					id : id
				}, function(data) {
					if(data == 'OK') {
						$('.post').unblock({
							theme : true,
							title : 'Sistema CERFIS',
							message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4'/></div>",
							timeout: 2000
						});
						$.post('control/usuario/usuario.ctrl.php', {
							page : 'vcuentas',
							area : 'administracion'
						}, function(data) {
							$(".post").empty().append(data);
						});
						
					} else {
						$("#errores div p .mensaje").empty().append(data);
						$("#errores").fadeIn('slow');
					}

				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#icuenta").val($(this).val());
		}).click(function() {
			var id = $("#icuenta").val();
			$.post('control/certificacion/certificacion.ctrl.php', {
				page : 'actualizar_cuenta',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
