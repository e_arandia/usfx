<?php
$width = (($area == 10 || $area == 1 || $area == 2)&& $edit==1110)? "'width:68.7%'" : "'width:69.5%'";
    
include '../vista/dialogos/busquedaAvanzada.html';
?>

<div class="div-filter">
<div id="calendar" style=<?php echo $width; ?>>
    <div class="head" style="height: 2.9em">
        <div class="htitle" >
            <label style="width:12em;padding-top: 1%;">Semillera</label>
            <label style="width:10em;padding-top: 1%;">Semillerista</label>
            <label style="width:7.6em;padding-top: 1%;">Cultivo</label>
            <label style="width:12em;padding-top: 1%;">Planta acondicionadora</label>   
            <label style="width:6em;padding-top: 1%;">Reporte PDF</label>          
            <?php if (($area == 10 || $area == 2)&& $edit==1110){
            ?>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>
            <?php } ?>
        </div>
    </div>
    <div class="body">
        <?php
while ($datos = DBConnector::objeto()){
        ?>
        <div class="semillera odd00">
            <label> <?php   echo utf8_encode($datos -> semillera); ?></label>
        </div>
        <div class="productor odd1">
            <label> <?php   echo utf8_encode($datos -> semillerista); ?></label>
        </div>
        <div class="cultivo odd2">
            <label> <?php   echo utf8_encode($datos->cultivo); ?></label>
        </div>
        <div class="cAnterior odd0" style="width: 11.9em;height: 2.5em;">
            <label> <?php
            $plantaA = (!empty($datos -> planta_acondicionadora)) ? ucfirst($datos -> planta_acondicionadora) : '-';
            echo $plantaA;
                ?></label>
        </div>
        <div style="padding-top:1%;width: 6em;" class="pdf">
            <a id="<?php echo $datos->id_cosecha;?>"style="cursor:pointer;padding:0" class="glyphicon glyphicon-print btn-lg"></a>
        </div>
        <?php
if (($area == 10 ||$area == 1 || $area == 2) &&  $edit==1110){
        ?>
        <div class="sistema options" style="width: 8.2em;">
            <?php
            if ($area == 2){
            ?>  
            <div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_semilla; ?>" />
            </div>
            <?php
            }else{
            ?>
            <div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_semilla; ?>" />
            </div>
            <?php
            }
            ?>
    <?php if ($area == 10){?>     
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_semilla; ?>" />
            </div>
            <?php } ?>
        </div>
        <?php
        }
        }
        ?>
        <input id="semilla" type="hidden"/>
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
    </div>
</div>

</div>
<div class="div-new" style="display:none;">
    
</div> 

<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#semilla").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#semilla").val('');
        }).on("click", function() {
            var id = $("input#semilla").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=fiscal&pag=cosecha&id='+id, '_blank');
            win.focus();
        });
//busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'cosecha',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var final = json.total;
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera odd00\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor odd1\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"cultivo odd2\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"cAnterior odd0\" style=\"width: 12em;height: 2.5em;\"><label>" + json.plantaa[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['odd00', 'odd0', 'odd1', 'odd2', 'odd3', 'odd4', 'odd5', 'odd6', 'odd7', 'odd8', 'odd9', 'odd0','pdf'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $("div.body").show();
            }
        });
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").slideUp();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").slideUp();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'cosecha',
                    opt : 'new'
                }
            }).done(function(html) {
                
                $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
                $.funciones.cargarImagen('cosechafis', 'fiscalizacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var celdas = ['odd00', 'odd0', 'odd1', 'odd2', 'odd3', 'odd4', 'odd5', 'odd6', 'odd7', 'odd8', 'odd9', 'odd0','pdf'];
        $.funciones.alternarColores(celdas);
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#idcosecha").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar el registro?')) {
                var imagen = '<img src="images/loader_3a.gif" />';
                $('.post').block({
                    theme : true,
                    title : 'Sistema CERFIS',
                    message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
                });

                var id = $("#idcosecha").val();
                $.post('control/certificacion/certificacion.ctrl.php', {
                    page : 'eliminar',
                    tbl : 'solicitud',
                    id : id
                }, function(data) {
                    if (data == 'OK') {
                        $('.post').unblock({
                            theme : true,
                            title : 'Sistema CERFIS',
                            message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4.gif'/></div>",
                            timeout : 2000
                        });
                        var area = $("#control").val();
                        $.post('control/certificacion/certificacion.ctrl.php', {
                            page : 'eliminar',
                            tbl : 'cosecha',
                            id : id
                        }, function(data) {
                            $(".post").empty().append(data);
                        });
                    } else {
                        $(".post").append(data);
                    }
                });
            }
        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#idcosecha").val($(this).val());
        }).click(function() {
            var id = $("#idcosecha").val();
            $.post('control/certificacion/certificacion.ctrl.php', {
                page : 'actualizacion',
                tbl : 'cosecha',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>
