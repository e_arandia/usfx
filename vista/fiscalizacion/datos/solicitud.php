<?php
$width = (($area == 10 || $area == 1 || $area == 2) && $edit == 1110) ? "'width:120.4%'" : "'width:116.9%'";
//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
//ejecutar consulta sql
DBConnector::ejecutar($blocks);

include '../vista/dialogos/busquedaAvanzada.html';
?>
<div class="div-filter">
<div id="calendar" style=<?php echo $width; ?>>
	<div class="head" style="height: 3.2em">
		<div class="htitle" >
			<label style="width:6em;padding-top: 1%;">Nro. Solicitud</label>
			<label style="width:7em;padding-top: 1%;">Proceso</label>
			<label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:8.2em;padding-top: 1%;">Departamento</label>
			<label style="width:8em;padding-top: 1%;">Provincia</label>
			<label style="width:6.7em;padding-top: 1%;">Municipio</label>
			<label style="width:10em;padding-top: 1%;">Comunidad</label>
			<label style="width:6em;padding-top: 1%;">Fecha Solicitud</label>
			<label style="width:6em;padding-top: 1%;">Reporte PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php } ?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="nSolicitud">
			<label> <?php    echo $datos -> nro_solicitud; ?></label>
		</div>
		<div class="sistema sistem">
			<label> <?php  echo ucfirst(utf8_encode($datos -> sistema)); ?></label>
		</div>
		<div class="semillera">
			<label> <?php	echo utf8_encode($datos -> semillera); ?></label>
		</div>
		<div class="productor">
			<label> <?php   echo utf8_encode($datos -> semillerista); ?></label>
		</div>
		<div class="dpto">
			<label> <?php 	echo $datos -> nombre_departamento; ?></label>
		</div>
		<div class="prov">
			<label> <?php  echo $datos -> provincia; ?></label>
		</div>
		<div class="municipio">
			<label> <?php	echo $datos -> municipio; ?></label>
		</div>
		<div class="comunidad">
			<label> <?php	echo $datos -> comunidad; ?></label>
		</div>
		<div class="fecha" style="width: 6em">
		
			<label> <?php echo $dates -> cambiar_formato_fecha($datos -> fecha); ?></label>
		</div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_solicitud; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>		
		<?php
if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
		?>
		<div class="sistema options" style="height: 28px; width: 8.3em;">
		  <?php
		  if (($area == 1 || $area == 2)&& $edit==1110){
		  ?>  
			<div>
				<input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_solicitud; ?>" />
			</div>
			<?php
            }else{
			?>
			<div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
	   <?php
    }
    if ($area==10){
	   ?>		
						
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_solicitud; ?>" />
			</div>
		<?php
        }
		?>	
		</div>
		<?php
        }
        }
		?>
		<input id="idsolicitud" type="hidden"/>
		<input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>	
</div>
<div class="meneame" style="width: 78%; margin-left: 20%; padding-top: 78%;">
    <?php
    if ($total > $nroRegistros)
        for ($cont = 1; $cont <= $plinks; $cont++) {
            if (($cont % 20) == 0)
                echo "</br></br></br>";
            $temp = $cont - 1;
            if ($cont >1 ){
                $temp = $temp + 0;
                echo "<a id=$cont name='pagina' onclick=cargarSolicitudes('fiscalizacion','ver','solicitud',$area,$temp)>$cont</a> ";
            }else{
                echo "<a id=$cont name='pagina' onclick=cargarSolicitudes('fiscalizacion','ver','solicitud',$area,$temp)>$cont</a> ";}
        }
     ?>
    </div> 
</div>
<div class="todo" style="display: none;">
    
</div>
<div class="div-new" style="display:none;">
    
</div>   

<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idsolicitud").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#idsolicitud").val('');
        }).on("click", function() {
            var id = $("input#idsolicitud").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=fiscal&pag=solicitud&id='+id, '_blank');
            win.focus();
        });
        $.funciones.calendario('input', 'fecha-filter');
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'solicitud',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nosolicitud, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label>" + json.nosolicitud[index] + "</label> </div>";
                            div += "<div class=\"sistema sistem\"><label>" + json.sistema[index] + "</label> </div>";
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"dpto\"><label>" + json.departamento[index] + "</label> </div>";
                            div += "<div class=\"prov\"><label>" + json.provincia[index] + "</label> </div>";
                            div += "<div class=\"municipio\"><label>" + json.municipio[index] + "</label> </div>";
                            div += "<div class=\"comunidad\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"fecha\" style=\"width: 6em\"><label>" + json.fecha[index] + "</label> </div>";
                            div += "<div class=\"pdf\" style=\"padding-top:1%;width: 6em;\" ><a id=\""+json.id[index]+"\" class=\"glyphicon glyphicon-print btn-lg\" style=\"cursor:pointer;padding:0\"></a></div>";                            
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    $("div.body").show();
                });
            } else {
                $("div.body").show();
            }
        });
        //boton de busqueda avanzada
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '0'
            });
        });
        //boton nueva solicitud
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '6%'
            });
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'solicitud',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
                $.funciones.cargarImagen('solicitudfis', 'fiscalizacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
            });
        });
    }); 
</script>
<script type="text/javascript">
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        edt = $(".crud").val();
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit,
            edt : edt
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //colorear celdas
        var celdas = ['nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'comunidad', 'fecha', 'options','pdf'];
        $.funciones.alternarColores(celdas);
        //envio de datos
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar la solicitud?')) {
                var id = $("#idsolicitud").val();
                $.ajax({
                    type : 'POST',
                    url : 'control/index.php',
                    data : {
                        mdl : 'certificacion',
                        opt : 'eliminar',
                        pag : 'solicitud',
                        id : id
                    },
                    beforeSend : function() {
                        $.funciones.mostrarMensaje('info', 'Eliminando solicitud')
                    },
                    success : function(data) {
                        if (data == 'OK') {
                            $.funciones.mostrarMensaje('ok', 'Solicitud eliminada');
                            var area = $("#control").val();
                            if (area != 10) {
                                var datos = {
                                    mdl : 'certificacion',
                                    opt : 'ver',
                                    pag : 'solicitud',
                                    area : area
                                };
                            } else {
                                var datos = {
                                    mdl : 'certificacion',
                                    opt : 'ver',
                                    pag : 'solicitud',
                                    area : area,
                                    edt : 1110
                                }
                            }
                            $.ajax({
                                type : 'POST',
                                url : 'control/index.php',
                                data : datos,
                                beforeSend : function() {
                                    $.funciones.mostrarMensaje('ok', 'Cargando Solicitudes..');
                                },
                                success : function(data) {
                                    $(".post").empty().append(data);
                                }
                            });
                            $.funciones.ocultarMensaje(5000);

                        } else {
                            $.funciones.mostrarMensaje('error', 'No se puede eliminar la solicitud');
                            $.funciones.ocultarMensaje(5000);
                        }
                    }
                });
            }
        });
        /**actualizar datos solicitud*/
        $("input[name=upd]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            var id = $("#idsolicitud").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_solicitud',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>