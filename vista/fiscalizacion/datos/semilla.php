<?php

$width = (($area == 10 || $area == 2)&& $edit==1110) ? "'width:74.5%'" : "'width:90.8%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);

include '../vista/dialogos/busquedaAvanzada.html';
?>

<div class="div-filter">
<div id="calendar" style=<?php echo $width; ?>>
	<div class="head" style="height: 3em">
		<div class="htitle" >
		    <label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:8em;padding-top: 1%;">Comunidad</label>
			<label style="width:7.5em;padding-top: 1%;">Cultivo</label>
			<label style="width:8.5em;padding-top: 1%;">Variedad</label>
			<label style="width:10.2em;padding-top: 1%;">Numero lote</label>	
			<label style="width:6em;padding-top: 1%;">Reporte PDF</label>		
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
            ?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php } ?>
		</div>
	</div>

	<div class="body">
		<?php
		
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera">
            <label> <?php echo $datos -> semillera; ?></label>
        </div>
		<div class="productor">
			<label> <?php   echo utf8_encode($datos -> semillerista); ?></label>
		</div>
		<div class="comunidad" style="width: 8em">
            <label> <?php   echo ($datos -> comunidad); ?></label>
        </div>
		<div class="cultivo">
			<label> <?php	echo utf8_encode($datos -> cultivo); ?></label>
		</div>
		<div class="variedad">
			<label> <?php	echo (ucfirst($datos -> variedad)); ?></label>
		</div>
		<div class="lote" style="width: 10em">
			<label> <?php
            $lote = !empty($datos -> lote) ? $datos -> lote : '-';
            echo $lote;
				?></label>
		</div>
		<div style="padding-top:1%;width: 6em;" class="pdf">
            <a id="<?php echo $datos->id_solicitud;?>"style="cursor:pointer;padding:0" class="glyphicon glyphicon-print btn-lg"></a>
        </div>
		<?php
if (($area == 10 ||$area == 1 || $area == 2) &&  $edit==1110){
		?>
		<div class="sistema options" style="height: 28px; width: 8.2em;">  
			<div>
				<input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_semilla; ?>" />
			</div>
	<?php if ($area == 10){?>		
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_semilla; ?>" />
			</div>
			<?php } ?>
		</div>
		<?php
        }
        }
//ejecutar consulta sql
#DBConnector::ejecutar($blocks);
		?>
		<input id="semilla" type="hidden"/>
		<input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>
</div>
<div class="div-new" style="display:none;">
    
</div>   

<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#semilla").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#semilla").val('');
        }).on("click", function() {
            var id = $("input#semilla").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=fiscal&pag=semilla&id='+id, '_blank');
            win.focus();
        });
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'semilla',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var final = json.total;
                    var div = '';
                    $.each(json.semillerista, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"comunidad\" style=\"width:8em\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\"><label>" + json.variedad[index] + "</label> </div>";
                            div += "<div class=\"lote\" style=\"width:10em\"><label>" + json.lote[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','nSolicitud','comunidad','localidad','campanha','cultivo','variedad','cSembrada','cProducir','cantidadS','lote','fecha','plantas','cAnterior','sParcela','productor','pdf'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                                $.getJSON("control/index.php", {
                    mdl : 'fiscalizacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'semilla',
                    search : '%'
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"comunidad\" style=\"width:8em;\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\"><label>" + json.variedad[index] + "</label> </div>";
                            div += "<div class=\"lote\" style=\"width:10em;\"><label>" + json.lote[index] + "</label> </div>";
                            //div += "<div class=\"sistema options\"><label>" + json.options[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','productor','comunidad','cultivo','variedad','lote','pdf'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            }
        });
        //boton de busqueda
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        //boton nueva solicitud
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter,form#filter-box").hide();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '6%'
            });
            $.ajax({
                data : {
                    mdl : 'fiscalizacion',
                    pag : 'semilla',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $.funciones.cargarImagen('seguimientofis', 'fiscalizacion');
                $.funciones.cargarImagen('semillafis', 'fiscalizacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
            });
        });
    }); 
</script>
<script type="text/javascript">
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    var celdas = ['semillera','nSolicitud','comunidad','localidad','campanha','cultivo','variedad','cSembrada','cProducir','cantidadS','lote','fecha','plantas','cAnterior','sParcela','productor','pdf'];
        $.funciones.alternarColores(celdas);
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#semilla").val($(this).val());
		}).click(function() {
			if (confirm('Esta seguro de eliminar el registro?')) {
				$.funciones.mostrarMensaje('info', 'Eliminando registro')
				var id = $("#semilla").val();
				$.post('control/index.php', {
					mdl : 'certificacion',
					opt : 'eliminar',
					pag : 'semilla',
					id : id
				}, function(data) {
					if (data == 'OK') {
						$.funciones.mostrarMensaje('ok', 'Semilla eliminada');
						
						setTimeout(function() {
							var area = $("#control").val();
							$.post('control/index.php', {
								mdl : 'certificacion',
								opt : 'ver',
								pag : 'semilla',
								area : area
							}, function(data) {
								$(".post").empty().append(data);
							});
						}, 50);
						$.funciones.ocultarMensaje(5000);
					}else {
                        $.funciones.mostrarMensaje('error', 'No se puede eliminar la semilla');
                        $.funciones.ocultarMensaje(5000);
                    }
				});
			}
			$("#errores").fadeOut(50);

		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#semilla").val($(this).val());
		}).click(function() {
			var id = $("#semilla").val();
			$.post('control/index.php', {
			    mdl : 'certificacion',
                opt : 'form',
				pag : 'upd_semilla',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
