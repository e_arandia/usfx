<h2 class="title">Hoja de Cosecha</h2>
<input type="hidden" class="estado-fiscal" value="1"/>
<div style="float: right; margin-top: -5%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
<?php
include '../vista/confirmacion/advertencia.html';

?>
<div class="entry">
<form id="form-buscador" class="seguimiento">
        <?php
        include '../vista/fiscalizacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>   
    <?php
    include '../vista/error/advertencia.php';
    ?> 
	<form class="seguimiento" style="display:none;height: 9em;">
		<?php
        include '../vista/fiscalizacion/seguimiento.html';
		?>
	</form>
</div>
<div class="entry">
	<form id="cosecha" class="seguimiento" style="display:none;">
		<div>
			<label>Planta Acondicionadora</label>
			<input id="plantaa" name="plantaa" autocomplete="off" style="width: 330px;"/>

			<input type="hidden" name="iSolicitud" id="iSolicitud"/>
            <input id="mdl" name="mdl" type="hidden" value="fiscalizacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="cosecha"/>
            
			<button id="enviar-cosecha-fiscal" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button> 
		</div>
	</form>
	<?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
	?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.buscar.estiloCuadroBusqueda();
        $("span#advice").css('margin-left','21%');
        //autocompletado de numero de solicitudes
        $.buscar.autocompletar('nosol',1);
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#buscarTXT").on("keyup", function(event) {
            if (event.which == 13) {
                $.buscar.hojaCosechaFiscal();
            }
        });

        $("#btn-search").on("click", function() {
            $.buscar.hojaCosechaFiscal();
        });
        
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var isol;
        $("#nosolicitud").on("keyup", function(event) {
            nro = $(this).val();
            var estado_fiscal = $("input.estado-fiscal").val();
            if (nro.length > 0) {
                if (event.which == 13) {
                    
                }
            } else {
                if (!nro.length) {
                    $("#semillera").removeClass("not-edit").val('');
                    $("#semillerista").removeClass("not-edit").val('');
                    $("input#comunidad").val('');
                    $("input#iSolicitud").val('');
                    $("form.seguimiento").not('form#form-buscador').fadeOut();
                }
            }
        });
        //cargar datos de semillera
        $("#semillera").on("keyup", function(event) {
                $("#hiddenNoSolicitud").val('');
                $("input#comunidad").val('');
                $("#iSolicitud").val('');
                $("#nosolicitud").removeClass("not-edit").val('');
                $("#semillerista").removeClass("not-edit").val('');
                $("form#semilla,div#cultivos,form#semillaP,div.alert").fadeOut();

        });
        $("#semillerista").on("keyup", function(event) {
            
            if (productor.length > 0) {
                if (event.which == 13) {
                    
                }
            } else {
                if (!productor.length) {
                    $("#hiddenNoSolicitud").val('');
                    $("input#comunidad").val('');
                    $("#iSolicitud").val('');
                    $("#nosolicitud").removeClass("not-edit").val('');
                    $("#semillera").removeClass("not-edit").val('');
                    $("form.seguimiento").not('form#form-buscador').fadeOut();
                }
            }
        }).on("mouseenter", function() {
            if ($(this).val() == '') {
                $("#help-semillerista").fadeIn();
            }
        }).on("click", function() {
            $("#help-semillerista").fadeIn();
        }).on("blur", function() {
            $("#help-semillerista").fadeOut();
            $("#help-semillera").fadeOut();
            $("#help-nosolicitud").fadeOut();
        }).on("mouseleave", function() {
            $("#help-semillera").fadeOut();
            $("#help-nosolicitud").fadeOut();
        });
        //opcion laboratorio
        $("#laboratorio").buttonset();
        $("select#nosolicitud,select#semillera,select#productor").selectmenu();
        //scroll para select nro de solicitudes
        $("select#nosolicitud").selectmenu().selectmenu("menuWidget").addClass("overflow");
        //$.funciones.cargarSemillerasFiscal('inspeccion');
        //return button
        $("button#return").on("click", function() {
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'semilla',
                area : 2,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("button#enviar-cosecha-fiscal").addClass("button-primary")
        $("button#enviar-cosecha-fiscal").click(function(event) {
            $.funciones.enviarCosechaFiscal();
        });
    })
</script>