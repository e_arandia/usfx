<div id="anteriores">
    <div>
        <label>Nro. solicitud: </label>
        <input id="nosolicitud" name="nosolicitud"/>
        <span id="help-nroSolicitud" class="help">Escriba numero de solicitud</span>
    </div>
    <div>
        <label>Semillera: </label>
        <input id="semillera" name="semillera" style="text-transform:uppercase;"/>
        <span id="help-semillera" class="help">Escriba nombre de semillera</span>
    </div>
    <div class="semilleristas">
        <label>Semillerista: </label>
        <input id="semillerista" name="productor"/>
        <span id="help-semillerista" class="help">Escriba nombre de semillerista</span>
    </div>
</div>
<?php
    include '../vista/dialogos/seleccionarSemilleraSemillerista.html';
    include '../vista/dialogos/seleccionarNroSolicitudCertifica.html';
    
?>