<h2 class="title">Semilla Producida</h2>
<input type="hidden" class="estado-fiscal" value="11"/>
<div style="float: right; margin-top: -5%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
    <form  class="seguimiento">
        <?php
            include '../vista/fiscalizacion/buscador_nros_semilleras_productores.html.php';        
            include '../vista/error/advertencia.php';
        ?>
    </form>
</div>
<div class="entry">
    <form id="semillaP-datos" class="seguimiento" style="display: none">
        <?php
            include '../vista/fiscalizacion/seguimiento.html';
        ?>
        <div id="cultivos" >
            <label>Cultivo:</label>
            <input id="cultivo" class="not-edit" name="cultivo" />
        </div>
    </form>
    
</div>
<div class="entry">
    <form id="semillaP" class="seguimiento" style="display: none">
        <div>
            <label>Prod. Semilla Neta</label>
            <input size="10" id="semillaNeta" name="semilla_neta">
        </div>
        <div>
            <label>Categoria Obtenida</label>
            <input size="10" id="categ_obtenida" name="categ_obtenida">
        </div>
        <div>
            <label>Número de etiquetas</label>
            <input size="10" id="etiqueta" name="etiqueta">
        </div>
        <div>
            <input type="hidden" name="icosecha" id="icosecha">
            <input type="hidden" name="ilaboratorio" id="ilaboratorio">
            <input type="hidden" name="iSolicitud" id="iSolicitud">

            <button id="enviar-semillaP-fiscal" class="btn btn-success button-primary" style="margin-top: -7%;">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button> 
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.buscar.estiloCuadroBusqueda();
        $("span#advice").css('margin-left','21%');
        //autocompletado de numero de solicitudes
        $.buscar.autocompletar('nosol_semillap',11);
        //autocompletado de numero de solicitudes
        
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $.buscar.semillaProducidaFiscal();
    });
</script>
<script type="text/javascript" >
    $(document).ready(function() {
        /*
        //busqueda de semilleras
        $("input#nosolicitud").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=buscar&pag=no_sem_sol&estado=8&opc=nosol_semillap",
            minLength : 2
        });
        $("input#semillera").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=buscar&pag=no_sem_sol&estado=8&opc=semillera",
            minLength : 2
        });
        $("input#semillerista").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=buscar&pag=no_sem_sol&estado=8&opc=semillerista",
            minLength : 2
        });
        */
;

        
       
        //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'semilla',
                area : 2,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        }); 
        $("#productor,#nrocampo,#generacion,#categoria").attr('disabled', 'disabled');       
        //cargar campos pertenecientes al semillerista
       /* $("select#productor").on("selectmenuchange", function(event, ui) {
                //id de cosecha
                $.getJSON('control/index.php', {
                    mdl : 'fiscalizacion',
                    pag : 'isolicitud',
                    nombre : nombre,
                    apellido : apellido,
                    semillera : semillera
                }, function(json) {
                    var isol = json.isolicitud;
                    $("#isolicitud").val(json.isolicitud);
                    //cargar cultivos de semillerista
                    $.funciones.cargarSelectFiscal('cultivo', 'fiscalizacion', 'cultivo', nombre, apellido, isol);
                    var url = 'control/index.php';
                    var datos = {
                        mdl : 'operaciones',
                        pag : 'dCosechaFiscal',
                        isol : isol
                    };
                    $.getJSON(url, datos, function(json) {
                        var cosechaID = json.icosecha;
                        var superficieID = json.isuperficie;
                        $('#icosecha').val(cosechaID);
                        $("#isuperficie").val(superficieID);
                        var url = 'control/index.php';
                        var datos = {
                            mdl : 'laboratorio',
                            opt : 'buscar',
                            pag : 'ilaboratorio',
                            icosecha : cosechaID
                        };
                        $.getJSON(url, datos, function(json) {
                            
                            switch (json.estado){
                                case 1:
                                $("#errores div p .mensaje").empty().append('Esperando resultado de laboratorio');
                                $("#errores").css('margin-top', '3em').fadeIn('slow');
                                break;
                                case 2:
                                //cargar id de laboratorio
                                $("#errores div p .mensaje").empty().append('Resultados de laboratorio listos');
                                $("#errores").css('margin-top', '3em').fadeIn('slow'); 
                                $("#ilaboratorio").val(json.id);
                                $("#cultivos").show();
                                break;
                                default:
                                $("#errores div p .mensaje").empty().append('No se encuentran muestras del cultivo en laboratorio');
                                $("#errores").css('margin-top', '3em').fadeIn('slow'); 
                                $("#cultivos").hide();
                                break;
                            }
                            
                        })
                    });

                });
        });*/
        //fin
    });

</script>
<!--  validacion y envio de datos    -->
<script type="text/javascript">
    $(document).ready(function() {
        $("button#enviar-semillaP-fiscal").addClass("button-primary")
        $("#semillaprod").enviarSemillaProducida();
    })
</script>
