<h2 class="title">Inscripci&oacute;n de campo Semillero</h2>
<div style="float: right; margin-top: -5%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
<div class="entry" style="margin-left: 0px;">
    <form id="solicitud" class="nuevoform" autocomplete="off">
        <div>
            <!--class="soleft"--->
            <div style="width: 48%; float: left;">
                <label style="width: 45%;">Número de Solicitud</label>
                <input style="width: 45px;" name="nro_solicitud" id="nro_solicitud" class="not-edit" maxlength="5">
            </div>
        </div>
        <div>
            <div style="width: 50%; float: right;">
                <label style="width: 25%;">Proceso</label>
                <input type="text" style="width: 75px;border-color: #FAFFEE;position: relative;top: -5px;" value="<?php echo utf8_encode($_SESSION['usr_area']); ?>" name="sistema" id="sistema" class="not-edit">
            </div>
            <div style="width: 35em; float: left;">
                <label style="width: 22%">Departamento</label>
                <input type="text" value="Chuquisaca" name="departamento" id="departamento" class="not-edit" readonly="readonly" style="width:70px; border-color: #FAFFEE;position: relative;top: -5px;"/>
            </div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-prov"  style=";position: relative;top: 5px;">Provincia</label>
                <select id="lstprovincia" style="width: 195px"></select>
            </div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-mun"  style=";position: relative;top: 5px;">Municipio</label>
                <select id="lstmunicipio" name="municipio" style="width: 195px" disabled="disabled">
                    <option value="">[ Seleccione Municipio]</option>
                </select>
            </div>
            <div style="width: 35em;float: left;" class="lst_semillera">
                <label style="width: 20%;position: relative;top: 5px;">Semillera</label>
                <input id="lstsemillera" name="semillera" style="text-transform: uppercase;width: 200px;"/>
            </div>
            <div style="width: 35em; float: left;">
                <span class="required">&nbsp;&nbsp;</span>
                <label style="width:20%;position: relative;top: 5px;">Comunidad</label>
                <input type="text" name="comunidad" id="mcomunidad">
            </div>
            <div style="width: 35em; float: left;">

            </div>
        </div>
        <div class="soright" style="width: 50%;float: right;margin: -34% 0">
            <div style="width: 35em;visibility: hidden;">
                <label style="width: 30%;position: relative;top: 5px;">N&uacute;mero de parcelas</label>
                <input id="noparcelas" name="noparcelas" style="width: 30px;" value="0"/>
            </div>

            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Nombre semillerista</label>
                <input id="name" name="name" type="text" style="width: 200px"/>
            </div>
            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Apellido semillerista</label>
                <input id="apellido" name="apellido" type="text" style="width: 200px"/>
            </div>
            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Fecha Solicitud</label>
                <input id="f_solicitud" name="f_solicitud" readonly="readonly"style="width: 65px;" value="<?php echo date('d-m-Y'); ?>"/>
            </div>
            <div style="float: right;">
                <input id="sarea" name="sarea" type= "hidden" value="<?php echo $_SESSION['usr_nivel']; ?>"/>
                <input id="stage" name="stage" type="hidden" value="solicitud"/>
                <input id="tipo" name="tipo" type="hidden" value="institucional"/>
                <input type="hidden" name="hdn_provincia" id="provincia"/>
                <input type="hidden" name="hdn_municipio" id="hdn_municipio"/>
                <button id="enviar-solicitud" class="btn btn-success">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
        </div>
        <div id="advice">
            <span class="required">&nbsp;&nbsp;</span>&nbsp;Para agregar mas comunidades deben estar separadas por coma &nbsp;(Ej: La Playa,Bella Vista, Yuquina)
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    include '../vista/dialogos/agregarSemillera.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'solicitud',
                area : 2,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        // select menu de numero de parcelas
        $("input#noparcelas").spinner({
            min : 0,
            max : 10
        });
        //ocultar select de municipio
        $("#lstmunicipio").hide();

        $("#nro_solicitud").numeric();
        //calendario
        $("#f_solicitud").calendarioSolicitud('f_solicitud');
        
        //$("button").addClass("button-primary")
        /**estilo para aviso*/
        var cssObj = {
            'float' : 'left',
            'font-size' : '9px',
            'font-style' : 'italic',
            'letter-spacing' : '0.1em',
            'width' : '45%'
        }
        //aviso
        $("#advice").css(cssObj);

        //creacion de scroll para select de provincia
        $("#lstprovincia").append('<option value=""> [ Seleccione provincia ]</option>');
        $("#lstprovincia").selectmenu().selectmenu("menuWidget").addClass("overflow");

        //scroll para select de municipio
        $("select#lstmunicipio").selectmenu().selectmenu("menuWidget").addClass("overflow-municipio");
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        function loadNewSolicitud() {
            //carga de nro de solicitud
            var proceso = $("#sistema").val();
            $.getJSON("control/index.php", {
                mdl : 'fiscalizacion',
                opt : 'buscar',
                pag : 'noSolicitud',
                sis : 'fiscal'
            }, noSolicitud);
            function noSolicitud(json) {
                if (json.estado = 'nuevo') {
                    if (parseInt(json.nro) < 10) {
                        var numero = '00' + parseInt(json.nro) + "/" + json.anho;
                        $("#nro_solicitud").empty().val(numero);
                    } else if (json.nro >= 10 && json.nro < 100) {
                        var numero = '0' + parseInt(json.nro) + "/" + json.anho;
                        $("#nro_solicitud").empty().val(numero);
                    } else {
                        $("#nro_solicitud").empty().val(parseInt(json.nro) + "/" + json.anho);
                    }
                    $("#nro_solicitud").attr('not-edit');
                }

            };
        };
        loadNewSolicitud();

        //carga de provincias de chuquisaca
        $.getJSON("control/index.php?mdl=fiscalizacion&opt=ver&pag=provincia", function(json) {
            $.each(json, function(key, value) {
                $('#lstprovincia').append('<option value="' + key + '">' + value + '</option>');
            });
        });
        //creacion de evento onchange en select provincia
        $("#lstprovincia").selectmenu({
            change : function(event, ui) {
            }
        });

        //evento onchange de select provincia
        $("#lstprovincia").on("selectmenuchange", function(event, ui) {
            var provincia = $("#lstprovincia").val();
            if (provincia != "") {
                $("#lstmunicipio").selectmenu("destroy").empty();
                $('#lstmunicipio').append('<option value="">[ Seleccione Municipio ]</option>');
                $("#lstmunicipio").selectmenu();
                $("#provincia").val(provincia);
                var palabras = provincia.substring(0, 3);
                $.getJSON("control/index.php?mdl=fiscalizacion&opt=ver&pag=municipio&prov=" + palabras, function(json) {
                    $('#lstmunicipio').empty().append('<option value="">[ Seleccione Municipio ]</option>');
                    $.each(json, function(key, value) {
                        $('#lstmunicipio').append('<option value="' + key + '">' + value + '</option>');
                    });
                });
                $("#lstmunicipio").selectmenu({
                    disabled : false
                });
            } else {
                $("#lstmunicipio").selectmenu({
                    disabled : true
                });
            }
        });
        //creacion de evento onchange
        $("select#lstmunicipio").selectmenu({
            change : function(event, ui) {
            }
        });
        // manejo de opciones despues de seleccionar un municipio
        $("select#lstmunicipio").on("selectmenuchange", function(event, ui) {
            //cargar id del municipio
            var id_municipio = $("select#lstmunicipio").val();
            if (id_municipio > 0) {
                $("#hdn_municipio").val(id_municipio);
            } else {
                $("#hdn_municipio").val(0);
            }
        });

        //calculo de parcelas
        $("#mcomunidad").blur(function() {
            var comunidades = $("#mcomunidad").val()
            var arraycomunidades = comunidades.split(',');
            $("#noparcelas").val(arraycomunidades.length);
        });
        //autocompletar semillera,comunidad,nombre y apellido
        $("#lstsemillera").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=ver&pag=semillera",
            minLength : 2
        });
        $("#mcomunidad").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=ver&pag=colugar",
            minLength : 2
        });
        $("#name").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=ver&pag=nombres",
            minLength : 2
        });

        $("#apellido").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=ver&pag=apellidos",
            minLength : 2
        });


        //envio de formulario
        $("#solicitud").enviarSolicitudFiscal();
    }); 
</script>