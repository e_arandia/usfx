<h2>Resumen de fiscalizaciones finalizadas</h2>
<div class="entry" style="padding: 0;">
	<form class="seguimiento" style="border:none;width:75%;height:305px;">
		<div id="tabs">
			<ul>
				<li>
					<a href="#trimestral" id="li-id-trim">Trimestral</a>
				</li>
				<li>
					<a href="#rangoFechas" id="li-id-rango">Entre rango de fechas</a>
				</li>
			</ul>
			<div id="trimestral">
			    <input type="hidden" id="tipo" value="trim"/>
				<div id="radio-btn" style="margin-left: 35px;">
					<label for="efm" style="width: 11%;margin-right: 5%">Enero Febrero Marzo</label>
					<input type="radio" name="option" id="efm" value="1">
					
					<label for="amj" style="width: 10%;margin-right: 5%">Abril Mayo Junio</label>
					<input type="radio" name="option" id="amj" value="2" style="width: 15%">
					
					<label for="jas" style="width: 15%;margin-right: 5%">
						<br>
						Julio
						<br>
						Agosto
						<br>
						Septiembre</label>
					<input type="radio" name="option" id="jas" value="3">
					
					<label for="ond" style="width: 15%">Octubre Noviembre Diciembre</label>
					<input type="radio" name="option" id="ond" value="4">
				</div>
			</div>
			<div id="rangoFechas">
				<div style="width: 75%">
					<div>
						<label style="width: 20%">Desde : </label>
						<input type="text" id="f_desde" style="width: 80px" name="desde" value="<?php echo '01-01-' . date('Y'); ?>"/>
					</div>
					<div>
					    
						<label style="10%">Hasta : </label>
						<input type="text" id="f_hasta" style="width: 80px" name="hasta"/>
					</div>
				</div>
			</div>
            <button class="btn btn-success" type="button" id="detail" style="margin-left:8px;margin-bottom:15px">
                <span class="fa fa-file-text-o" style="font-size:36px;color:green;"></span>
                DETALLE
            </button>
            <button class="btn btn-success" type="button" id="pdf" style="float:right;margin-right: 10px;">
                <span class="fa fa-file-pdf-o" style="font-size:36px;color:red;"></span>
                PDF
            </button>
            <button class="btn btn-success" type="button" id="xls" style="float:right;margin-right: 10px;">
                <span class="fa fa-file-excel-o" style="font-size:36px;color:green;"></span>
                EXCEL
            </button>    
            <input type="hidden" id="tab_clicked" value="trim"/>               
        </div>

		<div id="pruebaa"></div>
		<input id="sistema" type="hidden" name="sistema" value="<?php echo $_SESSION['usr_area']?>"/>
	</form>
</div>
<?php
include '../vista/confirmacion/advertencia_resumen_detalle.html';
?>
<div class="entry" style="padding: 0;">
  <div id="detalle-resumen" style="border:none;float:left;margin-left:-15%;padding:0% 2% 1%;display: none">
      <?php
        include '../vista/certificacion/info_detallada.html.php';
      ?>
</div>

<div class="resumenXLS"></div>
<script  type="text/javascript">
    $(document).ready(function() {
        
        $("#li-id-trim").click(function(){
            $("input#tab_clicked").empty().val('trim');  
        });
        $("#li-id-rango").click(function(){
            $("input#tab_clicked").empty().val('rango');  
        });
        $.funciones.calendarioInicial('input', 'f_desde');
        $.funciones.calendarioFinal('input', 'f_hasta');
        $("input:radio#efm").attr("checked",true);
        $("input[name=option]").checkboxradio();
        $("#tabs").tabs({
            activate : function(event, ui) {
                //ocultar botones PDF - EXCEL
                //$(".resumen,.resumen-date").hide();
                //ocultar boton DETALLE
                //$("div.mostrar-detalle-resumen").hide();
            }
        });

        var gestion = new Date().getFullYear();

        ///////////////////////////////////////////////////////////////////////////////
        //////          b o t o n    p a r a     P D F                           //////
        ///////////////////////////////////////////////////////////////////////////////
        $("button#pdf").on("click", function() {
            var tab_option = $("input#tab_clicked").val();            
            if(tab_option == 'trim'){   
                var opcion = $('input:radio[name=option]:checked').val(); //opcion de trimestre             
                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=certifica&tipo=trim&chs=' + opcion + '&gestion=' + gestion, '_blank');
            }else{
                desde = $('input#f_desde').val(); //fecha inicial
                hasta = $('input#f_hasta').val(); //fecha final
                tipo = $("input#tipo-rango").val();
                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=certifica&tipo=rango&desde=' + desde + '&hasta=' + hasta + '&gestion=' + gestion, '_blank');                
            }            
        });
        ///////////////////////////////////////////////////////////////////////////////
        //////          b o t o n    p a r a     EXCEL                           //////
        ///////////////////////////////////////////////////////////////////////////////
        $("button#xls").on("click",function(){
            var tab_option = $("input#tab_clicked").val();            
            if(tab_option == 'rango'){
                var desde = $("input#f_desde").val();
                var hasta = $("input#f_hasta").val();    
                
                $.get('control/index.php', {
                    mdl : 'informe',
                    opt : 'xls',
                    pag : 'certifica',
                    tipo : tab_option,
                    desde : desde,
                    hasta : hasta
                }, function(data) {
                    if (data != 'error') {
                        $("div.mostrar-detalle-resumen").show();
                        $(".resumenXLS").empty().append("<iframe class='iresumen' style='display:none' src='" + data + "'></iframe>");
                    } else {
                        $("div.mostrar-detalle-resumen").hide();
                        $("#advertencia").show().fadeOut(3000);
                    }
                });  
                         
            }else{
                var opcion = $('input:radio[name=option]:checked').val(); //opcion de trimestre
                var tipo = $("input#tipo").val();
                $.get('control/index.php', {
                    mdl : 'informe',
                    opt : 'xls',
                    pag : 'certifica',
                    chs : opcion,
                    tipo : tab_option
                }, function(data) {
                    if (data != 'error') {
                        $(".resumenXLS").empty().append("<iframe class='iresumen' style='display:none' src='" + data + "'></iframe>");
                    } else {
                        $("div.mostrar-detalle-resumen").hide();
                        $("#advertencia").show().fadeOut(3000);
                    }
                });                
                
            } 
        });

        ///////////////////////////////////////////////////////////////////////////////
        //////           boton detalle de resumen para trimestres                //////
        ///////////////////////////////////////////////////////////////////////////////
        $("button#detail").on("click", function() {
            var tab_option = $("input#tab_clicked").val();  
            if (tab_option == 'trim'){
                $("#myTable>tbody.buscar").empty();
                $("#myTable").tablesorter();
                opcion = $('input:radio[name=option]:checked').val();
                tipo = $("input#tipo").val();
                sistema = $("input#sistema").val();
                
                $.get("control/index.php",{
                    mdl:'informe',
                    opt:'detalle',
                    pag:'xls',
                    sistema : sistema,
                    chs : opcion,
                    tipo : 'trim'
                },function(respuesta){
                    $("div#detalle-resumen").empty().append(respuesta).show();
                });    
            }else{
                $("#myTable>tbody.buscar").empty();
                $("#myTable").tablesorter();
                opcion = $('input:radio[name=option]:checked').val();
                tipo = $("input#tipo").val();
                sistema = $("input#sistema").val();
                desde = $("input#f_desde").val();
                hasta = $("input#f_hasta").val();
                $.get("control/index.php",{
                    mdl:'informe',
                    opt:'detalle',
                    pag:'xls',
                    sistema : sistema,
                    chs : opcion,
                    tipo : 'rango',
                    desde : desde,
                    hasta : hasta
                },function(respuesta){
                    $("div#detalle-resumen").empty().append(respuesta).show();
                });
            }            
        });        
    }); 
</script>