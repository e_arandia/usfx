<div id="anteriores" class="table-responsive">
	<table class="table" style="border-top: none;">
		<tbody class="buscar">
			<tr>
				<td style="width: 1%; font-weight: bolder; text-align: right;">SEMILLERA</td>
				<td class="tbl-semillera" style="width: 3%;"></td>
				<td style="width: 1%; font-weight: bolder; text-align: right;">CULTIVO</td>
				<td class="tbl-cultivo" style="width: 6%;"></td>
			</tr>
			<tr>
				<td style="width: 1%; font-weight: bolder; text-align: right;">SEMILLERISTA</td>
				<td class="tbl-semillerista" style="width: 3%;"></td>
				<td style="width: 1%; font-weight: bolder; text-align: right;">VARIEDAD</td>
				<td class="tbl-variedad" style="width: 3%;"></td>
			</tr>
			<tr>
				<td style="width: 1%; font-weight: bolder; text-align: right;">COMUNIDAD</td>
				<td class="tbl-comunidad" style="width: 3%;" colspan = "1" ></td>
				<td>&nbsp;</td>
				<td>
				<button id="btn-precios" class="btn btn-success" type="button">
					<span class="glyphicon glyphicon-eye-open"> </span>
					Mostrar Precios
				</button></td>
			</tr>
		</tbody>
	</table>
</div>
<?php
    include '../vista/dialogos/seleccionarSemilleraCampanhaCultivoVariedad.html';
    include '../vista/dialogos/seleccionarSemilleristaCampanhaCultivoVariedad.html';
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCultivoVariedad.html';
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCampanhaVariedad.html';
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCampanhaCultivo.html';
?> 
<script type="text/javascript">
    $(document).ready(function(){
        //permitir solo letras
        $.funciones.soloLetras("buscarTXT");
        /////////////////////autocompletado  de busqueda unificada
        //autocompletar cuadro de busqueda
        $.buscar.autocompletarCuentaFiscal();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('input#buscarTXT').on('keyup', function(event) {
            if (event.which == 13){
                $.buscar.cuentaFiscal();
            }
        });
        $('button#btn-search').on("click",function(){
            $.buscar.cuentaFiscal();
        });   
        ///////////////bton mostrar precios
        $('#btn-precios').on('click', function() {
            $.funciones.precioFiscalizacion();
        });
    })
</script>