<h2 class="title">Cuenta de Semilleristas</h2>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
<div style="float: right; margin-top: -8%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
    <form  class="seguimiento">
        <?php
            include '../vista/fiscalizacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <form id="cuenta" class="nuevoform" style="display: none;">      
        <div>
            <label>Responsable</label>
            <input id="responsable" name="responsable" type="text"/>
        </div>   
        <?php
        include '../vista/fiscalizacion/semilleras_productores_campanha_cultivo.html.php';
        ?>        
        <div id="ctaRight" style="width: 49%;float: left;">
            <div>
                <label style="width: 50%">Categoria a Producir</label>
                <input id="categoriaAprob" class="not-edit" name="categoriaAprob" type="text" readonly="true" style="width: 105px"/>
            </div>
            <div>
                <label style="width: 50%">Bolsa / Etiqueta</label>
                <input id="bolsa" class="number" name="bolsaEtiqueta" type="text" style="width: 30px" autocomplete="off"/>
            </div>
            <div style="visibility:hidden;">
                <label style="width: 50%;">Total</label>
                <input id="total1" class="number not-edit" name="total" type="text" value="0" style="width: 30px" />
            </div>

        </div>
        <div id="ctaLeft"  style="width: 50%;float: right;">
            <div>
                <label style="width: 54%">Inscripcion</label>
                <input id="inscripcion" class="number not-edit" name="inscripcion" type="text" style="width: 30px" />
            </div>
            <div>
                <label style="width: 54%">Inspeccion</label>
                <input id="icampo" class="number not-edit" name="icampo" type="text" style="width: 30px" />
            </div>
            <div>
                <label style="width: 54%">Analisis de Laboratorio y Etiquetaci&oacute;n</label>
                <input id="anlet" class="number not-edit" name="anlet" type="text" style="width: 30px" />
            </div>

            <div>
                <label style="width: 49%">Total</label>
                <input id="total2" class="number not-edit" name="total2" type="text" style="width: 30px" />
            </div>

        </div>
        <div id="ctaFinal" style="margin-top: 5%;">
            <div>
                <label>Fecha</label>
                <input id="f_pago" class="date not-edit" name="f_pago" type="text" readonly="readonly" style="width: 65px" value="<?php echo date('d-m-Y')?>"/>
            </div>
            <div>
                <label>Monto pagado</label>
                <input id="montoPagado"  class="number" name="montoPagado" type="text" maxlength="5" style="width: 30px" autocomplete="off"/>
            </div>
            <div>
                <label>Saldo Total</label>
                <input id="saldototal" class="number not-edit" name="saldototal" type="text" readonly="readonly" style="width: 30px" />
            </div>
            <div style="visibility:hidden">
                <label style="width: 30%">Saldo Campa&ntilde;a Anterior</label>
                <input id="campAnterior" class="number not-edit" name="campAnterior" type="text"  readonly="readonly" style="width: 30px;margin-top:-3%" />
                <span class='loading'><img src='images/loader_6.gif'/></span>
            </div>
        </div>
        <div id="button-send" style="float:right; margin-top: -6%;">
            <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
            <input id="iSolicitud" name="iSolicitud" type="hidden"/>
            <input id="iSemilla" name="iSemilla" type="hidden"/>
            <input id="iSuperficie" name="iSuperficie" type="hidden"/>
            <input id="iEstado" name="iEstado" type="hidden"/>
            <input name="mdl" type="hidden" value="fiscalizacion"/>
            <input name="opt" type="hidden" value="guardar"/>
            <input name="pag" type="hidden" value="cuenta"/>

            <button id="enviar-cuenta" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/dialogos/cuotaCuenta.html';
    include '../vista/dialogos/precioCertificacion.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $.buscar.estiloCuadroBusqueda();
        $("span#advice").empty().text('Escribir semillera,semillerista,cultivo o variedad').css('margin-left','21%');
        
    });
</script>
<!-- inicializacion de datos-->
<script type="text/javascript">
    $(document).ready(function() {
        //responsable
        $("#responsable").autocomplete({
            source : "control/index.php?mdl=fiscalizacion&opt=ver&pag=responsable",
            minLength : 2
        });
                //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'cuentas',
                area : 2,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        }); 
        //estilo formulario
        $(".nuevoform").css('padding', '3% 2% 2%')
        //$("#f_pago").calendario('f_pago');
        $("select").not("#semillera").attr('disabled', 'disabled');
        $("input[name=send]").fadeTo('fast', 0.5);

        //validacion de numeros
        $("#total1,#inscripcion,#icampo,#anlet,#acondicionamiento,#plantines,#montoPagado").numeric();
        //calculo de saldo
        $('input#montoPagado').on("click",function(){
            var semillera = $("td.tbl-semillera").text();
            var semillerista = $("td.tbl-semillerista").text();
            var cultivo = $("td.tbl-cultivo").text();
            var variedad = $("td.tbl-variedad").text();
            var total = $("input#saldototal").text();
            var isemilla = $("input#iSemilla").text();
            $("div#tipo-cuota").hide();
            //$("div#tipo-cuota-fiscal").buttonset();
            $("div#tipo-cuota-fiscal").show();
            
            $('#lst-cuota label.slr>span').empty().append(semillera);
            $('#lst-cuota label.smr>span').empty().append($.funciones.cambiarSignoPorAcento(semillerista) );
            $('#lst-cuota label.clt>span').empty().append($.funciones.cambiarSignoPorAcento(cultivo) );
            $('#lst-cuota label.var>span').empty().append($.funciones.cambiarSignoPorAcento(variedad) );
            $('#lst-cuota label.sld>span.total').empty().append(total);
            
            //$.funciones.buscarSaldo(semillera,semillerista,cultivo,variedad);
            $.funciones.buscarCuotasCuenta(isemilla);                                              
        }); 
            $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
            //$(".nuevoform").css('padding', '3% 2% 5%')
    })
</script>
<script type="text/javascript" >
    $(document).ready(function() {
        // boton de registro
        $("#enviar-cuenta").on("click",function(){
            datos = $("form#cuenta").serialize();
            $.ajax({
                url : 'control/index.php',
                method : 'POST',
                data : datos,
                success : function(responseText) {
                    alert(responseText);
                }
            });
        });
    }); 
</script>