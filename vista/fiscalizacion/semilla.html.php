<?php
/**
 * nro solicitud - semillera - semillerista
 * acciones igual que en certificacion
 * al elegir semillerista mostar formulario semilla
 * */
?>

<h2 class="title">Datos de la Semilla</h2>
<div style="float: right; margin-top: -5%;">
    <button type="button" class="btn btn-success" id="return">
        <span class="glyphicon glyphicon-arrow-left"> </span>
        Volver
    </button>
</div>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
    <form id="form-buscador" class="seguimiento" style="width: 78%">
        <?php
        include '../vista/fiscalizacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <?php
    include '../vista/error/advertencia.php';
    ?>
    <form id="semilla" class="nuevo" style="margin: 2% 0 0;padding: 3% 2% 9%; display: none">
        <?php
        include '../vista/fiscalizacion/seguimiento.html';
        ?>
        <div>
            <label id="lbl-cultivo">Cultivo</label>
            <select id="cultivo" name="cultivo">
                <option value="">[Seleccione Cultivo]</option>
            </select>
        </div>
        <div>
            <label id="lbl-variedad">Variedad</label>
            <select id="variedad" name="variedad" disabled="disabled">
                <option value="">[Seleccione variedad]</option>
            </select>
        </div>
        <div>
            <label>Lote Nro</label>
            <input id="lotenro" name="lotenro" type="text" class="special not-edit"/>
        </div>
        <div>
            <label id="lbl-categoria-producir">Categor&iacute;a a producir</label>
            <select id="cgeneracion" name="cgeneracion" disabled="disabled">
                <option value="">[Seleccione variedad]</option>
            </select>
        </div>
        <div>
            <input id="iSolicitud" name="isolicitud" type="hidden"/>
            <input id="sistema" name="sistema" type="hidden" value="<?php echo $_SESSION['usr_area']?>"/>

            <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
            <input id="hiddenCultivo" name="hiddenCultivo" type="hidden"/>
            <input id="hiddenNoSolicitud" name="hiddenNoSolicitud" type="hidden"/>
            
            <input id="mdl" name="mdl" type="hidden" value="fiscalizacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="semilla"/>

        </div>
        <div>
            <button id="enviar-semilla-fiscal" class="btn btn-success" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>
    </form>
    <input id="msg-error" type="hidden"/>
    <?php
    include '../vista/error/errores.php';
    include '../vista/dialogos/agregarCultivo.html';
    include '../vista/dialogos/agregarVariedad.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.buscar.estiloCuadroBusqueda();
        //autocompletado de numero de solicitudes
        $.buscar.autocompletar('nosol',0);
        //mensaje de errores
        $("div#errores").css({'margin-top': '5%'});
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#buscarTXT").on("keyup", function(event) {
            if (event.which == 13) {
                $.buscar.semillaFiscal();
            }
        });
        $("#btn-search").on("click", function() {
            $.buscar.semillaFiscal();
        });
    }); 
</script>
<script type="text/javascript">        
$(document).ready(function(){
        //$("select#nosolicitud").selectmenu();
        //return button
        $("button#return").on("click", function() {
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'ver',
                pag : 'semilla',
                area : 2,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
        $("select#cultivo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        $("select#variedad").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        //carga de cultivos disponibles
        function cargarCultivos() {
            $.getJSON('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'buscar',
                pag : 'ls_cultivos'
            }, function(json) {
                if (json.cultivos.length > 0) {
                    $.each(json.cultivos, function(index, value) {
                        $('select#cultivo').append('<option value="' + value + '">' + value + '</option>');
                    });
                    $("select#cultivo").append('<option value="add_cultivo">Agregar cultivo</option>');
                }
            });
        }

        cargarCultivos();

        $("select#cultivo").selectmenu({
            change : function(event, ui) {
            }
        });

        $("select#cultivo").on("selectmenuchange", function(event, ui) {
            cultivo = $("select#cultivo").val();
            if (cultivo != '') {
                if (cultivo == 'add_variedad') {

                } else {
                    $("input#hiddenCultivo").val(cultivo);
                    cargarVariedades(cultivo);
                    $("select#variedad").selectmenu({
                        disabled : false
                    });
                    $("select#cgeneracion").selectmenu({
                        disabled : false
                    });
                    var nombre, apellido, idsolicitud, semilla, isolicitud, partes, nombres, semillera, noSol;
                    semilla = $("#cultivo").val();
                    productores = $("td.tbl-semillerista-fiscal").text();
                    semillera = $("td.tbl-semillera-fiscal").text();

                    if (semilla != '' && productores != '') {
                        partes = productores.split(" ");
                        nombre = partes[0];
                        nombres = partes.length;
                        if (nombres == 2) {
                            apellido = partes[1];
                        } else if (nombres == 3) {
                            apellido = partes[1] + ' ' + partes[2];
                        } else if (nombres == 4) {
                            apellido = partes[1] + ' ' + partes[2] + ' ' + partes[3];
                        } else {
                            apellido = partes[1] + ' ' + partes[2] + ' ' + partes[3] + ' ' + partes[4];
                        }
                        idsolicitud = $('#iSolicitud').val();
                        temp_nosol = $("td.tbl-nosolicitud-fiscal").text();
                        nosol = temp_nosol.split("/");
                        var url = 'control/index.php?mdl=fiscalizacion&opt=lote&nombre=' + nombre + '&apellido=' + apellido + '&cultivo=' + semilla + '&sol=' + nosol[0] + '&state=fiscal';
                        $.getJSON(url, function(json) {
                            $("#lotenro").val(json.lote);
                        });
                    }
                }
            } else {
                $("select#variedad").selectmenu({
                    disabled : true
                });
            }
        });
        //carga de variedades de un cultivo
        function cargarVariedades(cultivo) {
            $.getJSON('control/index.php', {
                mdl : 'fiscalizacion',
                opt : 'buscar',
                pag : 'ls_variedades',
                cultivo : cultivo

            }, function(json) {
                $("select#variedad").empty();
                $("select#variedad").append('<option value="">[Seleccione variedad]</option>');
                if (json.variedades.length > 0) {
                    $('select#variedad').empty().append('<option value="">[Seleccione variedad]</option>');
                    $.each(json.variedades, function(index, value) {
                        $('select#variedad').append('<option value="' + value + '">' + value + '</option>');
                    });
                }
                $("select#variedad").append('<option value="add_variedad">Agregar variedad</option>');
                $("select#variedad").selectmenu("refresh");
            });
        }

        //determina el nro de lote segun nombre, cultivo y nro de solicitud
        $("#lotenro").on("click", function() {

        });
        //cargar categorias
        $.funciones.cargarCategoriasFiscal();
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("button").addClass("button-primary");
        //enviar semilla para fiscalizacion
        $('button#enviar-semilla-fiscal').on('click',function(){
            $.funciones.enviarSemillaFiscal();    
        });
         
    })
</script>