<div class="entry">
	<div class="coordenadas">
		<div id= "ejeX">
			<h4 class="title">Debe elegir 2 opciones y seleccionar los datos</h4>
			<div class="semilleraX checkbx">
				<input type="checkbox" name="primeroX" value="0" />
				<label>Semillera</label>
				<select size="1" name="semilleraX" id="semilleraX"></select>
			</div>
			<div class="productorX checkbx">
				<input type="checkbox" name="primeroX" value="0" />
				<label>Productor</label>
				<select size="1" name="productorX" id="productorX"></select>
			</div>
			<div class="campaniaX checkbx">
				<input type="checkbox" name="primeroX" value="0" />
				<label>Campa&ntilde;a</label>
				<select size="1" name="campaniaX" id="campaniaX"></select>
			</div>
			<div class="cultivoX checkbx">
				<input type="checkbox" name="primeroX" value="0" />
				<label>Cultivo</label>
				<select size="1" name="cultivoX" id="cultivoX"></select>
			</div>
		</div>
		<div class="opcionesgraficas" >
			<h2 style="position: absolute; right: 270px;"></h2>
			<div class="imagenes" style="float: left; clear: right">
				<div>
					<img src="images/graphs/torta-simple.gif" alt="torta"/>
				</div>
				<div>
					<img src="images/graphs/graph_01.gif" alt="lineal"/>
				</div>

				<div>
					<img src="images/graphs/graph_02.gif" alt="columna"/>
				</div>
			</div>
			<div id="op1">
				<input type="radio" name="grafico" value="0" />
			</div>
			<div id="op2">
				<input type="radio" name="grafico" value="0" />
			</div>
			<div id="op3">
				<input type="radio" name="grafico" value="0" />
			</div>
		</div>
		<div class="boton">
			<input class="button-primary ui-state-default ui-corner-all" type="button" value="Graficar"/>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".checkbx").addClass('checkbox');
		$(".semilleraX label,.productorX label,.campaniaX label,.cultivoX label").addClass('label');
	})
</script>