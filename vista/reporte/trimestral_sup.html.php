<div class="entry">
    <h2>vol&uacute;menes de producci&oacute;n de cultivos</h2>
	<form style="width: 440px; height: 75%;">
		<div id="tabs">
            <ul>
                <li>
                    <a href="#trimestral" id="li-id-trim">Trimestral</a>
                </li>
                <li>
                    <a href="#rangoFechas" id="li-id-rango">Entre rango de fechas</a>
                </li>
            </ul>
            <div id="trimestral">
                <input type="hidden" id="tipo" value="trim"/>
                <div id="radio-btn" style="margin-left: 35px;">
                    <label for="efm" style="width: 11%;margin-right: 5%">Enero Febrero Marzo</label>
                    <input type="radio" name="option" id="efm" value="1">
                    
                    <label for="amj" style="width: 10%;margin-right: 5%">Abril Mayo Junio</label>
                    <input type="radio" name="option" id="amj" value="2" style="width: 15%">
                    
                    <label for="jas" style="width: 15%;margin-right: 5%">
                        <br>
                        Julio
                        <br>
                        Agosto
                        <br>
                        Septiembre</label>
                    <input type="radio" name="option" id="jas" value="3">
                    
                    <label for="ond" style="width: 15%">Octubre Noviembre Diciembre</label>
                    <input type="radio" name="option" id="ond" value="4">
                </div>
            </div>
            <div id="rangoFechas">
                <div style="width: 75%">
                    <div>
                        <label style="width: 20%">Desde : </label>
                        <input type="text" id="f_desde" style="width: 80px" name="desde" value="<?php echo '01-01-' . date('Y'); ?>"/>
                    </div>
                    <div>
                        
                        <label style="10%">Hasta : </label>
                        <input type="text" id="f_hasta" style="width: 80px" name="hasta"/>
                    </div>
                </div>
            </div>
            <button class="btn btn-success" type="button" id="detail" style="margin-left:8px;margin-bottom:15px;visibility: hidden">
                <span class="fa fa-file-text-o" style="font-size:36px;color:green;"></span>
                DETALLE
            </button>
            <button class="btn btn-success" type="button" id="pdf" style="float:right;margin-right: 10px;">
                <span class="fa fa-file-pdf-o" style="font-size:36px;color:red;"></span>
                PDF
            </button>
            <button class="btn btn-success" type="button" id="xls" style="float:right;margin-right: 10px;">
                <span class="fa fa-file-excel-o" style="font-size:36px;color:green;"></span>
                EXCEL
            </button>
            <input type="hidden" name="area" id="area" value="1"/>    
            <input type="hidden" id="tab_clicked" value="trim"/>               
        </div>
	</form>
</div>
<?php
    include '../vista/confirmacion/advertencia_resumen_detalle.html';
    include '../vista/error/advertencia.php';
?>
<div class="resumenes">
    <iframe class="ifr-resumen" style="display: none"></iframe>
</div>
<script  type="text/javascript">
    $(document).ready(function() {
        $("#li-id-trim").click(function() {
            $("input#tab_clicked").empty().val('trim');
        });
        $("#li-id-rango").click(function() {
            $("input#tab_clicked").empty().val('rango');
        });
        //inicializacion de estado
        $.funciones.calendarioInicial('input', 'f_desde');
        $.funciones.calendarioFinal('input', 'f_hasta');
        $("input[name=option]").checkboxradio();
        $("#tabs").tabs({
            activate : function(event, ui) {
                //reiniciar fecha inicial
                $('#f_desde').val('01-01-' + new Date().getFullYear());
            }
        });
        $("button#trim-report").on("click", function() {
            var opcion = $('input:radio[name=option]:checked').val();
            if (opcion) {
                $(".resumen").fadeIn();
            } else {
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            }
        });
        $("#rango-report").on("click", function() {
            desde = $('#f_desde').val();
            if (desde) {
                $(".resumen-date").fadeIn();
            } else {
                $(".alert>label").empty().append("Fecha Invalida");
                $(".alert").show().fadeOut(3000);
            }
        });
        ////////////////////////////////////////////////////////
        ////////////////////// BOTON  PDF //////////////////////
        ////////////////////////////////////////////////////////
        $("button#pdf").on("click", function() {
            var opcion = $('input:radio[name=option]:checked').val();
            if (opcion) {
                var tab_option = $("input#tab_clicked").val();
                var area = $("input#area").val();
                if (tab_option == 'trim'){
                    var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=vp_cultivo&chs=' + opcion + '&tipo=trim&area='+area, '_blank');
                    win.focus();
                }else{
                    desde = $('input#f_desde').val();
                    hasta = $('input#f_hasta').val();
                    if (desde != '') {
                        var cultivo = $("select#trim-cultivo").find(":selected").val();
                        var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=vp_cultivo&area=1&tipo=rango&desde=' + desde + '&hasta=' + hasta+'&area='+area, '_blank');
                        win.focus();
                    } else {
                        $(".alert>label").empty().append("Debe seleccionar una opcion");
                        $(".alert").show().fadeOut(3000);
                    }
                }
            } else {
                $("div#advertencia").show().fadeOut(3000);
            }
        });
        ////////////////////////////////////////////////////////
        ////////////////////// BOTON  XLS //////////////////////
        //////////////////////////////////////////////////////// 
        $("button#xls").on("click", function() {
            opcion = $('input:radio[name=option]:checked').val();
            if (opcion) {
                var tab_option = $("input#tab_clicked").val();
                if (tab_option == 'trim'){
                    $.getJSON('control/index.php', {
                    mdl : 'informe',
                    opt : 'xls',
                    pag : 'vp_cultivo',
                    tipo : 'trim',
                    area : area,
                    chs : opcion                    
                    }, function(json) {
                        if (json.msg != 'error') {
                            $(".ifr-resumen").removeAttr('src').attr('src', json.xls);
                            $(".resumen").fadeIn();
                        } else {
                            $(".resumen").hide();
                            $("#advertencia").show().fadeOut(3000);
                        }
                    });
                }else{
                    desde = $('input#f_desde').val();
                    hasta = $('input#f_hasta').val();
                    if (desde != '') {
                        tipo = $("input#tipo-rango").val();
                        $.getJSON('control/index.php', {
                            mdl : 'informe',
                            opt : 'xls',
                            pag : 'vp_cultivo',
                            tipo : 'rango',
                            area : area,
                            desde : desde,
                            hasta : hasta                            
                        }, function(json) {
                            if (json.msg != 'error') {
                                $(".resumen-date").fadeIn();
                                $(".ifr-resumen").removeAttr('src').attr('src', json.xls);
                            } else {
                                $(".resumen-date").hide();
                                $(".alert>label").empty().append("Error al crear archivo");
                                $("#advertencia").show().fadeOut(3000);
                            }
                        }); 
                    }else {
                        $(".alert>label").empty().append("Fecha invalida");
                        $(".alert").show().fadeOut(3000);
                    }
                }                
            } else {
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            }
        });
    }); 
</script>