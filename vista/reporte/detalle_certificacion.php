<h2 class="title" style="letter-spacing: .5em;margin-left: 40%">Detalle Certificaci&oacute;n</h2>
<div class="entry parametros">
	<form id="semilleras">
		<?php
		include '../certificacion/semilleras_productores.html';
		?>
		<div>
			<label>Campa&ntilde;a: </label>
			<select id="campanha" name="campanha"></select>
		</div>
		<div>
			<input type="submit" name="submit" class="button-primary ui-state-default ui-corner-all" value="Buscar"/>
		</div>
	</form>
</div>
<div class="entry resultado">
	<div style="width: 248.5%;" id="calendar">
		<!--<h4 style="letter-spacing: .5em;margin-left: 40%">DETALLE CERTIFICACION</h4>-->
		<div>
			<a id="pdf" class="img">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>
		</div>
		<div style="height: 3.2em" class="head">
			<div class="htitle" style="border-top: 2px solid #39C613">
				<label style="width:106.9em;padding-top: 1%;"> </label>
				<label style="width:21.3em;padding-top: 1%;">COSTOS POR CERTIFICACION </label>
				<label style="width:8.4em;padding-top: 1%;"></label>
			</div>
			<div class="htitle" style="border-top: 47px solid #39C613">
				<label style="padding-top: 1%; width: 3em;">Nº</label>
				<label style="padding-top: 1%; width: 7em;">FECHA</label>
				<label style="padding-top: 1%; width: 12em;">SEMILLERA</label>
				<label style="padding-top: 1%; width: 12em;">COMUNIDAD</label>
				<label style="padding-top: 1%; width: 11em;">RESPONSABLE / SEMILLERISTA</label>
				<label style="padding-top: 1%; width: 9em;">CULTIVO</label>
				<label style="padding-top: 1%; width: 9em;">VARIEDAD</label>
				<label style="padding-top: 1%; width: 8.8em;">CATEGORIA SEMBRADA</label>
				<label style="width: 6em; padding-bottom: 1%;">SUPERFICIE Has.</label>
				<label style="width: 4em; padding-bottom: 1%;">Nº CAMPO</label>
				<label style="width: 6em; padding-bottom: 1%;">APROBADO Has.</label>
				<label style="width: 6em; padding-bottom: 1%;">RECHAZADO Has.</label>
				<label style="width: 6em; padding-bottom: 1%;">CATEGORIA APROBADO</label>
				<label style="width: 6em; padding-bottom: 1%;">NÚMERO BOLSAS</label>
				<label style="width: 5em; padding-bottom: 1%;">INSCRIP. Bs</label>
				<label style="width: 5em; padding-bottom: 1%;">CAMPO Bs.</label>
				<label style="width: 6em; padding-bottom: 1%;">ETIQUETAS Bs.</label>
				<label style="width: 5em; padding-bottom: 1%;">TOTAL Bs.</label>
				<label style="width:8.4em;padding-top: 1%;"></label>
			</div>
		</div>
		<div style="border: 1px solid #39C613" class="body" >
			<div style="padding-top: 1%; width: 3em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"> 1</label>
			</div>
			<div style="padding-top: 1%; width: 7em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"> 10/05/2011</label>
			</div>
			<div style="padding-top: 1%; width: 12em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">GOBERNACION - SERRANO</label>
			</div>
			<div style="padding-top: 1%; width: 12em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">Mendoza </label>
			</div>
			<div style="padding-top: 1%; width: 11em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">Isidro Flores</label>
			</div>
			<div style="padding-top: 1%; width: 9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">Trigo</label>
			</div>
			<div style="padding-top: 1%; width: 9em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">Yampara</label>
			</div>
			<div style="padding-top: 1%; width: 8.8em;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">REG - 2</label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">0.50</label>
			</div>
			<div style="width: 4em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">2315</label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">0.50</label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center">CER</label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 6em; padding-top: 1%;float: left;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 5em; padding-top: 1%;float: left;height: 3em;border-right: 1px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="width: 8.3em; padding-top: 1%;float: left;height: 3em;">
				<div id="estado" style="float: left;height: 1.5em;margin: 1px 0 0 15%;width: 6em;">
					<input type="checkbox" id="paga" name="estado" value="1" style="width: 10px; clear: right; float: left;">
					<label >Pagar</label>					

				</div>
			</div>
		</div>
		<div style="border-top: 1px solid #39C613;border-bottom: 2px solid #39C613;border-left: 2px solid #39C613;border-right: 2px solid #39C613" class="body" >
			<div style="padding-top: 1%; width: 72.3em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center;font-weight: bold;letter-spacing: 1.5em"> TOTAL</label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> 1,50</label>
			</div>
			<div style="padding-top: 1%; width: 3.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"></label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center">1,00</label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> 0,50</label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="padding-top: 1%; width: 4.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="padding-top: 1%; width: 4.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="padding-top: 1%; width: 5.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="padding-top: 1%; width: 4.9em;float: left;border-right: 2px solid #39C613;height: 3em;">
				<label style="text-align: center"> </label>
			</div>
			<div style="width: 8.4em; padding-top: 1%;float: left;height: 3em;">
				<label style="text-align: center"></label>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$.funciones.cargarSemilleras('semilleras');
		$.funciones.cargarCampanhas();
		$("button").fadeTo('fast', 1);
		$("#semillera").change(function() {
			var semillera = $("#semillera").find(':selected').val();
			if(semillera != "") {
				var palabras = semillera.split(" ");
				semillera = '';
				semillera = palabras.join("*");
				$("#productor").append('<option value="">Cargando...</option>');
				$("#productor").productores(semillera, 'semilleristas');
				$("input[name=submit]").fadeTo('fast', 0.1).mouseover(function() {
					$(this).fadeTo('fast', 1);
				}).mouseleave(function() {
					$(this).fadeTo('fast', 0.1);
				});
			}
		});
		$('#productor').change(function() {
			if($("#productor").find(':selected').val() != '') {
				$("input[name=submit]").removeAttr('style').fadeTo('fast', 0.1).mouseover(function() {
					$(this).fadeTo('fast', 1);
				}).mouseleave(function() {
					$(this).fadeTo('fast', 0.1);
				});
			}
		});
		$("input[name=submit]").click(function() { alert('hola');
			//$("input[name=submit]").submit();
			$("#semilleras").buscar();
		})
	})
</script>