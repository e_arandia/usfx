<?php
    if($_SESSION['usr_iarea']=='1'){
       echo "<h2 class=\"title\">Informe Gr&aacute;fico de semilla (certificaci&oacute;n) </h2>";
    }else{
        echo "<h2 class=\"title\">Informe Gr&aacute;fico de semilla (fiscalizaci&oacute;n) </h2>";
    }
?>
<div class="entry" style="margin-left: 0px;">
    <form id="graficar" class="nuevoform" autocomplete="off" style="padding: 3% 2% 45%;">
        <div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-prov"  style="position: relative;top: 5px;">Provincia :</label>
                <select id="lstprovincia" style="width: 195px">
                    <option value="">Todas</option>  
                </select>
            </div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-mun"  style=";position: relative;top: 5px;">Municipio :</label>
                <select id="lstmunicipio" name="municipio" style="width: 195px" disabled="disabled">
                    <option value="">Todos</option>  
                </select>
            </div>
            <div style="width: 35em;float: left;" class="lst_semillera">
                <label style="width: 20%;position: relative;top: 5px;">Semillera :</label>
                <select id="lstsemillera" name="semillera" style="width: 200px" disabled="disabled">
                    <option value="">Todas</option>  
                </select>
            </div>
            <div style="width: 35em; float: left;">
                <label style="width:20%;position: relative;top: 5px;">Semillerista :</label>
                <select id="lstproductor" name="productor" disabled="disabled">
                    <option value="">Todos</option>                    
                </select>
            </div>  
        </div>
        <div class="soright" style="width: 50%;float: right;margin: -30% 0">
            <div>
            <label style="width:20%;">Graficar : </label>
            <select id="graficar" name="tipo">
                <option value="">[ Seleccionar opcion ]</option>
                <option value="culsuper">Superficie vs Cultivo</option>
                <option value="catecul">Volumens vs Cultivo</option>
            </select>
        </div>
        <div>
            <label style="width:20%;">Tipo :</label>
            <select id="tipo" name="option" disabled="disabled">
                <option value="">[ Seleccionar ]</option>
                <option value="barra"> Barras</option>
                <option value="circular">Torta</option>
                <option value="lineal"> Lineal</option>
            </select>
        </div>
        <div>
            <label style="width:20%;">Desde :</label>
            <input value="<?php echo '01-01-'.date('Y');?>" id="f_desde" name="f_desde" style="width:65px;">
        </div>
            <div>
                <label style="width:20%;">Hasta :</label>
            <input value="<?php echo date('d-m-Y');?>" id="f_hasta" name="f_hasta" style="width: 65px;">
            <input type="hidden" id="sistema" name="sistema" value="<?php echo $_SESSION['usr_iarea']?>">
            
            </div>
        </div>
        <div>
            <button style="display: none;margin-top:-3%;" class="btn btn-success button-primary" id="button-draw">
                <span class="glyphicon glyphicon-picture"></span>
                Graficar
            </button>
        </div>
    </form>


    <?php
    include '../vista/dialogos/mostrar_grafico.html';
    include '../vista/error/errores.php';
    ?>
</div>

<div id="loading" style="border: thick dotted green;display: none">
    <img src="images/loader_4.gif"  />
    <a target="_parent" href="text.php"><img src="images/loader_4.gif"  /></a>

</div>
<div id='test'>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("select#lstprovincia,select#lstmunicipio,select#lstsemillera,select#lstcomunidad,select#lstproductor").selectmenu();

        $("button#button-draw").addClass("button-primary");

        $.funciones.calendarioInicial('input', 'f_desde');
        $.funciones.calendarioFinal('input', 'f_hasta');
        //creacion de evento onchange
        $("select#graficar,select#tipo").selectmenu({
            change : function(event, ui) {
            }
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        //carga de provincias de chuquisaca
        $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=colugar&seccion=provincia", function(json) {
            $.each(json, function(key, value) {
                $('#lstprovincia').append('<option value="' + key + '">' + value + '</option>');
            });
        });
        //agregar estilo css
        $("#lstprovincia").selectmenu().selectmenu("menuWidget").addClass("overflow");
        //creacion de evento onchange en select provincia
        $("#lstprovincia").selectmenu({
            change : function(event, ui) {
            }
        });

        //evento onchange de select provincia
        $("#lstprovincia").on("selectmenuchange", function(event, ui) {
            var provincia = $("#lstprovincia").find(":selected").val();
            if (provincia != "") {
                $("#lstmunicipio").selectmenu("destroy").empty();
                $('#lstmunicipio').append('<option value="">[ Seleccione Municipio ]</option>');
                $("#lstmunicipio").selectmenu();
                $("#provincia").val(provincia);
                var palabras = provincia.substring(0, 3);
                $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=colugar&seccion=municipio&prov=" + palabras, function(json) {
                    $('#lstmunicipio').empty().append('<option value="">Todos</option>');
                    $.each(json, function(id, municipio) {
                        $('#lstmunicipio').append('<option value="' + id + '">' + municipio + '</option>');
                    })
                });
                $("#lstmunicipio").selectmenu({
                    disabled : false
                });
            } else {
                $("#lstmunicipio").selectmenu({
                    disabled : true
                });
            }
        });
        //cargar semilleras
        //creacion de evento onchange
        $("select#lstmunicipio").selectmenu({
            change : function(event, ui) {
            }
        });
        // manejo de opciones despues de seleccionar un municipio
        $("select#lstmunicipio").on("selectmenuchange", function(event, ui) {
            //cargar id del municipio
            var id_municipio = $("select#lstmunicipio").val();
            if (id_municipio > 0) {
                $("#hdn_municipio").val(id_municipio);
            } else {
                $("#hdn_municipio").val(0);
            }
            //cargar semilleras que estan en la provincia
            var provincia = $("#lstprovincia").find(":selected").val();
            var municipio = $("#lstmunicipio").find(":selected").val();
            var area = $(".ulogin").val();
            if (provincia != "" && municipio != "") {
                $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=semillera&chs=solicitud&provincia=" + provincia + "&municipio=" + municipio + "&area=" + area, function(json) {
                    $('#lstsemillera').empty().append('<option value="">Todas</option>');
                    $.each(json, function(id, institucion) {
                        $('#lstsemillera').append('<option value="' + institucion + '">' + institucion + '</option>');
                    });
                });
                $("#lstsemillera").selectmenu({
                    disabled : false
                });
            } else {
                $('#lstsemillera').empty().append('<option value="">Todas</option>');
                $("#lstsemillera").selectmenu({
                    disabled : true
                });
            }
            $("select#lstsemillera").selectmenu().selectmenu("menuWidget").addClass("overflow-semillera");
        });
        //al cambiar semillera cargar comunidades
        $("select#lstsemillera").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#lstsemillera").on("selectmenuchange", function(event, ui) {
            var semillera = $("select#lstsemillera").find(":selected").val();
            var provincia = $("select#lstprovincia").find(":selected").val();
            var municipio = $("#lstmunicipio").find(":selected").val();
            var area = $(".ulogin").val();
            if (semillera != '') {
                $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=semillera&chs=solicitud&provincia=" + provincia + "&municipio=" + municipio + "&area=" + area, function(json) {
                    $('#lscomunidad').empty().append('<option value="">Todas</option>');
                    $.each(json, function(id, institucion) {
                        $('#lstsemillera').append('<option value="' + institucion + '">' + institucion + '</option>');
                    });
                });
                $.funciones.listaSemilleristasBySemilleraForGraph('lstproductor', semillera);
                $("#lstsemillera,#lstcomunidad,#lstproductor").selectmenu({
                    disabled : false
                });
            } else {
                $("#lstsemillera,#lstcomunidad,#lstproductor").selectmenu({
                    disabled : true
                });
            }
        });
        //al cambiar semillerista cargar cultivo
        $("select#lstproductor").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#lstproductor").on("selectmenuchange", function(event, ui) {
            semillera = $("select#lstsemillera").find(":selected").val();
            semillerista = $("select#lstproductor").find(":selected").val();
            if (semillerista != '') {
                $("select#cultivo").selectmenu("destroy").empty();
                $("select#cultivo").empty().append("<option value='' selected='selected'>[Seleccione cultivo]</option>");
                $("select#cultivo").append("<option value=''>Todos</option>");
                $("select#cultivo").selectmenu();
                $.funciones.listaCultivosForGraph("cultivo", semillera, semillerista);
                $("select#cultivo").selectmenu({
                    disabled : false
                });
            } else {
                $("select#cultivo").selectmenu({
                    disabled : true
                });
            }
        });

        //opcion a graficar
        $("#option").selectmenu();
        $("select#graficar").on("selectmenuchange", function(event, ui) {
            var graphic = $(this).val();
            if (graphic != '') {
                $("#tipo").selectmenu({
                    disabled : false
                });
            } else {
                $("#tipo").selectmenu({
                    disabled : true
                });
            }
        });
        //tipo de grafico
        $("select#tipo").on("selectmenuchange", function(event, ui) {
            var opcion = $(this).val();
            if (opcion != '') {
                $("#button-draw").fadeIn();
            } else {
                $("#button-draw").fadeOut();
            }
        });
        $("#button-draw").click(function(event) {
            event.preventDefault();
            var grafico = $("select#graficar").find(":selected").val();
            var tipo = $("select#tipo").find(":selected").val();
            var sistema = $("input#sistema").val();
            var desde = $("input#f_desde").val();
            var hasta = $("input#f_hasta").val();
            var provincia = $("select#lstprovincia").find(":selected").val();
            var municipio = $("select#lstmunicipio").find(":selected").val();
            var semillera = $("select#lstsemillera").find(":selected").val();
            var semillerista = $("select#lstproductor").find(":selected").val();
            $.ajax({
                url : 'control/index.php',
                method : 'GET',
                dataType : 'json',
                jsonp: false,
                data : {
                    mdl : 'informe',
                    opt : 'graficar',
                    grafico : grafico,
                    tipo : tipo,
                    sistema : sistema,
                    desde : desde,
                    hasta : hasta,
                    prv : provincia,
                    mcp : municipio,
                    slr : semillera,
                    smr : semillerista
                },
                beforeSend : function() {
                    if (tipo === '') {
                        return false
                    } else {
                        $("button#button-draw").empty().append("<img src='images/loader_6.gif'/>cargando").attr('disabled', 'disabled');
                    }
                },
                success : function(json) {                    
                    var img = json.img; 
                    if (img == 'msgError.jpg'){
                        var imagen = "images/msgError.jpg";
                    }else{
                        var imagen = "vista/reportes/"+img;    
                    }
                    $.funciones.mostrarFiltro(json.provincia,json.municipio,json.semillera,json.semillerista);
                    $("img#grafico").attr('src',imagen);
                    var response = "java";
                    if(grafico == 'culsuper'){
                        titulo = 'Superficie de semilla certificada';
                    }else{
                        titulo = 'Volumen de produccion';
                    }
                    $('#resultado-grafico').dialog({// Dialog
                        title : titulo,
                        dialogClass : "no-close",
                        resizable : false,
                        height : 400,
                        width : 500,
                        buttons : {
                            "Imprimir" : function() {
                                grafico = $.funciones.basename(json.img,'.png'); 
                                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=grafico&img='+grafico, '_blank');
                                win.focus();
                            },
                            "Cerrar" : function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                    $("#button-draw").empty().text('Graficar').removeAttr('disabled');
                }
            });
        });
    })
</script>