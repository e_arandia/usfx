<div id="tabs">
    <ul>
        <li>
            <a href="#tabs-1">Datos Personales</a>
        </li>
        <li>
            <a href="#tabs-2">Cambiar contrase&ntilde;a</a>
        </li>
    </ul>
    <div id="tabs-1">
        <div class="entry" style="padding: 10px 70px 60px 15px;">
            <form id="updInfo">
                <input type="hidden" name = "iusuario" value="<?php  echo $usuario['id_login']; ?>"/>
                <div style="margin-bottom: 15px; width: 60px;">
                    <label>Nombre: </label>
                    <input class="datos" name="nombre" id="nombre" value="<?php  echo $usuario['nombre']; ?>"/>
                </div>
                <div style="margin-bottom: 15px; width: 60px;">
                    <label>Apellido:</label>
                    <input class="datos" name="apellido" id="apellido" value="<?php  echo $usuario['apellido']; ?>" />
                </div>
                <div style="margin-bottom: 15px; width: 60px;">
                    <label>Area:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <input style="background-color:#f2eecf;border-color:#f2eecf" value="<?php  echo utf8_encode($usuario['nombre_area']); ?>"/>
                </div>
                <div style="margin-bottom: 15px; width: 60px;">
                    <label>Usuario:</label>
                    <!--<label class="datos"> <?php  echo $usuario['login']; ?></label>-->
                    <input style="background-color:#f2eecf;border-color:#f2eecf;" class="not-edit" value="<?php  echo $usuario['login']; ?>">
                </div>
                <div style="margin-top: 9%;">
                <button id="btn-user" class="btn btn-success">
                    <span class="glyphicon glyphicon-refresh"></span>
                    cambiar
                </button>
                </div>
            </form>
        </div>
    </div>
    <div id="tabs-2">
        <div class="entry">
            <form id="cambiarpass">
                <input type="hidden" name = "iusuario" value="<?php  echo $usuario['id_login']; ?>"/>
                <div style="margin-bottom: 15px; width: 83em;">
                    <label>Nueva Contraseña: </label>
                    <input id="password" type="password" name="password" size="10" style="position: relative; left: -8em;">
                </div>
                <div style="margin-bottom: 15px; width: 83em;">
                    <label>Repita Contraseña:</label>
                    <input id="password2" type="password" name="password2" size="10" style="position: relative; left: -8em;">
                </div>
                <div style="margin-right: -9%;">
                    <button id="btn-pwd" class="btn btn-success">
                    <span class="glyphicon glyphicon-refresh"></span>
                    CAMBIAR
                </button>
                </div>
            </form>
            <div id="advertencia" class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;width: 35em">
                    <p class="adv">
                        <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        <strong>Advertencia! </strong>
                        Los cambios tendr&aacute;n efecto la pr&oacute;xima vez que inicie sesi&oacute;n
                    </p>
                </div>
            </div>
            <?php
            include '../vista/error/aviso.php';
            ?>
        </div>
    </div>
</div>
<?php
include '../vista/error/errores.php';
?>

<script type="text/javascript">
    $(document).ready(function() {
        // creacion de Tabs
        $('#tabs').tabs();
        $('.ui-state-error').css("z-index", "9999");

        $("button#btn-pwd,button#btn-user").addClass("button-primary")
        //Modificar nombre y apellido
        $("#modificar").css({
            "z-index" : "9999",
            "width" : "16%"
        }).click(function() {
            $("form#updInfo").actualizarInfo();
        });
        //Modificar password
        $("#advertencia").css({
            "left" : "-1em",
            "position" : "relative",
            "width" : "85%"
        });
        $("#cambiar").css({
            "z-index" : "9999",
            "width" : "16%"
        }).click(function() {
            $("#cambiarpass").actualizarPass();
        });

    });
</script>
