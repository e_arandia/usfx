<?php
date_default_timezone_set('America/La_Paz');
?>
<h2 class="title">Nueva copia de seguridad</h2>
<div class="entry" style="margin: 0.2em 15em 0 5em;">
	<form id="respaldar" class="seguimiento">
		<div>
			<label style="padding-top: 2%;width: 21%;">Nombre archivo</label>
			<input id="archivo" type="text" name="nombreArchivo" value="Backup<?php echo date('dM')?>"/>
		</div>
		<div>
			<label>Fecha</label>
			<input id="fecha" name="fecha" value="<?php echo date('d-m-Y'); ?>" type="text" readonly = "readonly" style="max-width: 6em;border:1px none"/>
		</div>
		<div>
			<label>Hora</label>
			<input id="hora" name="hora"  value="<?php echo date('H:i'); ?>" type="text" readonly = "readonly" style="max-width: 4em;border:1px none"/>
			<input name="iuser"  value="<?php echo $_SESSION['usrID']; ?>" type="hidden"/>
		</div>
		<div style="margin-left: 64%;">
			<button id="cancel" class="btn btn-success">
                <span class="glyphicon glyphicon-ok"></span> Respaldar Datos
            </button>  
		</div>
	</form>
	<div style="margin-bottom: 2em">
		&nbsp;&nbsp;
	</div>
	<?php
	include '../vista/error/errores.php';
	?>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    var cssObj = {
                'text-transform' : 'none'
            };
        $("#archivo").css(cssObj)    
		$("input[name=submit1]").click(function() {
			$("#respaldar").submit();
		});
		$("#respaldar").ajaxForm({
			url : 'control/index.php',
			type : 'post',
			data : {
				mdl : 'administracion',
				pag : 'respaldar'
			},
			beforeSubmit : validar,
			success : showResponse
		});
		function validar(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];
			if (!form.nombreArchivo.value) {
				$("#archivo").css({
					backgroundColor : "#f5c9c9"
				});
				mensaje += '<div>- Debe escribir un nombre para el archivo</div>';
			} else {
				$("#archivo").css({
					backgroundColor : ""
				});
			}
			if (mensaje != '') {
				$("#errores div p .mensaje").empty().append(mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				return true;
			}
		};
		function showResponse(responseText, statusText, xhr, $form) {
			switch (responseText) {
				case 'Error':
					var msg = 'La copia no fue creada. Intentelo nuevamente';
					$.funciones.mostrarMensaje('error', msg);
					$.funciones.ocultarMensaje(5000);
					break;

				case 'OK' :
					$.funciones.mostrarMensaje('ok', 'Respaldo creado exitosamente');
					$.funciones.ocultarMensaje(5000);
					setTimeout(function() {
						$.get(url, {
							mdl : 'login',
							pag : 'nueva_copia'
						}, function(data) {
							$(".post").empty().append(data);
						});
					}, 50);
					break;
			}
		};
	})
</script>