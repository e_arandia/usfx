<div style="width: 25%; margin: 4% 0px 0px 33%;width: 120%">
    <h2 class="title">Actualizaci&oacuten Datos de Usuario</h2>
</div>

<div class="entry">
	<form id="actualizacion">
		<?php		
while ($usuario = DBConnector::objeto())
{
		?>
		<div style="width: 27em;">
			<label>Nombre</label>
			<input name="nombre" value="<?php  echo $usuario -> nombre; ?>"/>
		</div>
		<div style="width: 27em;">
			<label>Apellido</label>
			<input name="apellido" value="<?php  echo $usuario -> apellido; ?>"/>
		</div>
		<div style="width: 27em;">
			<label>Area</label>
			<select id="lstarea">
			    <?php
                $area = $usuario -> id_area;
                Area::getAreas();
                while ($obj = DBConnector::objeto()) {
                    if ($area == $obj -> id_area)
                        echo "<option value='$obj->id_area' selected='selected'>" . $obj -> nombre_area . "</option>";
                    else {
                        echo "<option value='$obj->id_area'>" . $obj -> nombre_area . "</option>";
                    }
                }
            ?>
			</select>
		</div>
		<div style="width: 27em;">
			<label>Usuario</label>
			<input  style="text-transform: none" name="usuario"  value="<?php  echo $usuario -> login; ?>"/>
		</div>
		<div style="width: 27em;">
			<label>Estado </label>
			<?php
			$tipoUsuario = $usuario->estado;
if ($tipoUsuario != 10){
			?>
			<!--<input value="<?php echo $usuario -> estado;?>" name="estado"/>-->
			<select id="estado" name="estado">
				<?php
				   if ($tipoUsuario == 1){ 
				?>
				<option value="1" selected="selected">Habilitar</option>
				<option value="2">Deshabilitar</option>
				<?php
                }else{
				?>
			 	   <option value="2" selected="selected">Deshabilitar</option>
			 	   <option value="1">Habilitar</option
			 	<?php
                }
			 	?>
			</select>
			<?php
            }else{
			?>
			
			<input value="<?php echo $usuario -> estado; ?>" name="estado" readonly="readonly"/>
			<?php
            }
			?>
		</div>
		<div>
			<input type="hidden" name="id" value="<?php echo $usuario -> id_login; ?>"/>
			<input type="hidden" id="iarea" name="iarea" value="<?php echo $usuario -> id_area; ?>"/>
		</div>
		<?php
        }
		?>
		<div style="margin-left: 65%">
			<button class="updUsuario">
			    Actualizar
			</button>
		</div>
	</form>
</div>
<!--  validacion y envio de datos  -->
<script type="text/javascript">
    $(document).ready(function() {
        $("button.updUsuario").button({
            icons : {
                primary : "ui-icon-arrowrefresh-1-e"
            }
        });
        $("select#estado").selectmenu();
        //creacion de evento onchange
        $("#lstarea").selectmenu({
            change : function(event, ui) {
            }
        });
                //creacion de evento onchange
        $("select#lstarea").on("selectmenuchange", function(event, ui) {
            var new_area = $("#lstarea").val();
            
            $("#iarea").val(new_area);
        });
        $("#actualizacion>div>label").css({
            'font-weight' : 'bolder',
            'margin-top' : '2.5%'
        });
        $("#actualizacion").ajaxForm({
            url : 'control/index.php',
            type : 'post',
            data : {
                mdl : 'login',
                pag : 'actualizar'
            },
            success : showResponse
        });
        function showResponse(responseText, statusText, xhr, $form) {
            switch (responseText) {
            case 'error':
                $.funciones.mostrarMensaje('error', 'No se pudieron actualizar los datos');
                $.funciones.ocultarMensaje(2500);
                break;

            case 'OK' :
                $.funciones.mostrarMensaje('ok', 'Los datos fueron actualizados');
                $.funciones.ocultarMensaje(3500);
                var url = 'control/index.php';
                setTimeout(function() {
                    $.get(url, {
                        mdl : 'login',
                        pag : 'listar'
                    }, function(data) {
                        $(".post").empty().append(data);
                    });
                }, 30);
                break;
            }
        }
    });

</script>
