<h2 class="title">copias de seguridad</h2>
<div id="calendar" style="width: 80.6%">
	<div class="head" style="height: 3em">
		<div class="htitle" >
			<label style="width:3em;padding-top: 1%;">Nro</label>
			<label style="width:25em;padding-top: 1%;">Nombre Archivo</label>
			<label style="width:10em;padding-top: 1%;">Fecha</label>
			<label style="width:10em;padding-top: 1%;">Hora</label>
			<label style="width:7em;padding-top: 1%;">Opciones</label>
		</div>
	</div>
	<div class="body">
		<?php
if (DBConnector::filas() == 0){
		?>
		<div class="error" style="width: 55.4em">
			Todavia no se han realizado copias de seguridad
		</div>
		<?php
        }else{
        $indice = 1;
        while ($datos = DBConnector::objeto()){
		?>
		<div class="productor nro" style="width: 3em;height: 28px;">
			<label class="numero"><?php   echo $indice; ?></label>
		</div>
		<div class="productor archivo" style="width: 25em;height: 28px;">
			<label class="archivo"><?php   echo basename($datos -> archivo); ?></label>
		</div>
		<div class="productor fecha" style="width: 10em;height: 28px;">
			<label><?php   echo $format -> cambiar_formato_fecha($datos -> fecha); ?></label>
		</div>
		<div class="productor hora" style="width: 10em;height: 28px;">
			<label><?php   echo $datos -> hora; ?></label>
		</div>
		<div class="sistema options" style="height: 28px; width: 7em;">
			<div class="restaurar">
				<input name="restaurar" type="image" src="images/restaurar.png" style="position: relative;right: 1em;" value="<?php echo $datos -> id_backup; ?>" title="restaurar"/>
			</div>
			<div class="eliminar">
				<input name="eliminar" type="image" src="images/eliminar24.png" style="position: relative;top: -1.5em;right: -1.5em;" value="<?php echo $datos -> id_backup; ?>" title="eliminar"/>
			</div>
		</div>
		<?php
        $indice++;
        }
        }
		?>
		<input type="hidden" name="nombre" id="id"/>
	</div>
	<?php
    include '../vista/error/errores.php';
    include '../vista/confirmacion/confirmar.html';
	?>
	<script type="text/javascript">
        $(document).ready(function() {
            var celdas = ['nro', 'archivo', 'fecha', 'hora', 'options'];
            $.funciones.alternarColores(celdas);
            var opciones = ['restaurar', 'eliminar'];
            $.funciones.alternarColorOpcionImagen(opciones);
            /**eliminar copia de seguridad*/
            $("input[name=eliminar]").mouseover(function() {
                $("#id").val($(this).val());
            }).click(function() {
                $(".message").empty().append('Desea eliminar el respaldo?');
                $('#dialog').dialog({
                    title : 'Confirmar',
                    dialogClass : "no-close",
                    resizable : false,
                    modal : true,
                    width : 260,
                    buttons : {
                        "Aceptar" : function() {
                            $(this).dialog("close");
                            var file = $("#id").val();
                            $.post('control/index.php', {
                                mdl : 'administracion',
                                pag : 'eliminar_copia',
                                file : file
                            }, function(data) {
                                if (data == 'OK') {
                                    var url = 'control/index.php';
                                    $.get(url, {
                                        mdl : 'administracion',
                                        pag : "restauracion"
                                    }, function(data) {
                                        $(".post").empty().append(data);
                                        $.funciones.mostrarMensaje('ok', 'Copia de seguridad eliminada');
                                        $.funciones.ocultarMensaje(5000);
                                    });
                                } else {
                                    $.funciones.mostrarMensaje('error', 'ERROR ' + data + '. No se puede eliminar la copia de seguridad');
                                    $.funciones.ocultarMensaje(5000);
                                }
                            });
                        },
                        "Cancelar" : function() {
                            $(this).dialog("close");
                        }
                    }
                });
                ///////////////////

                /////////////////////////////
            });
            /**restaurar una copia de seguridad*/
            $("input[name=restaurar]").mouseover(function() {
                $("#id").val($(this).val());
            }).click(function() {
                $(".message").html('Los datos actuales seran reemplazados.<br>Desea continuar?')
                $('#dialog').dialog({// Dialog
                    title : 'Confirmar',
                    dialogClass : "no-close",
                    resizable : false,
                    height : 200,
                    width : 300,
                    buttons : {
                        "Aceptar" : function() {
                            $.funciones.mostrarMensaje('info', 'Restaurando copia de seguridad');
                            var file = $("#id").val();
                            $.post('control/index.php', {
                                mdl : 'administracion',
                                pag : 'restaurar',
                                file : file
                            }, function(data) {
                                if (data == 'OK') {
                                    $.funciones.mostrarMensaje('ok', 'Se ha restaurado la copia de seguridad exitosamente !!!');
                                    $.funciones.ocultarMensaje(20000);
                                    url = 'control/index.php';
                                    setTimeout(function() {
                                        $.get(url, {
                                            mdl : 'administracion',
                                            pag : 'restauracion'
                                        }, function(data) {
                                            $(".post").empty().append(data);
                                        })
                                    }, 100);

                                } else {
                                    $.funciones.mostrarMensaje('error', 'ERROR ' + data + '. No se puede restaurar la copia de seguridad');
                                    $.funciones.ocultarMensaje(5000);
                                }
                            });
                            $(this).dialog("close");
                        },
                        "Cancelar" : function() {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        })
	</script>