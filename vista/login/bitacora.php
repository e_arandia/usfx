<?php

//nro de enlaces
$plinks = ceil($total / $nroRegistros);
//ejecutar consulta sql
DBConnector::ejecutar($blocks);
?>
<h2>Bitacora de acceso</h2>
<div id="calendar" style="width: 53em">
	<div class="head" style="height: 3em">
		<div class="htitle">
			<label style="width:7em;padding-top: 1%;">Direccion IP</label>
			<label style="width:10em;padding-top: 1%;">Usuario</label>
			<label style="width:10em;padding-top: 1%;">Fecha y hora de Ingreso</label>
			<label style="width:10em;padding-top: 1%;">Fecha y hora de Salida</label>
			<label style="width:9em;padding-top: 1%;">Duracion</label>
			<label style="width:11em;padding-top: 1%;">Sesion</label>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="nSolicitud" style="width: 7em">
			<label><?php   echo $datos -> ip_address; ?></label>
		</div>
		<div class="productor" style="width: 10em">
			<label> <?php	echo $datos -> usuario; ?></label>
		</div>
		<div class="cSembrada ingreso" style="width: 10em">
			<label> <?php
            $temp1 = substr($datos -> fecha_ingreso, 0, 10);
            $temp2 = substr($datos -> fecha_ingreso, 11, 8);
            echo $dates -> cambiar_formato_fecha($temp1) . ' ' . $temp2;
				?></label>
		</div>
		<div class="cSembrada salida"style="width: 10em">
			<label> <?php
            $temp1 = substr($datos -> fecha_salida, 0, 10);
            $temp2 = substr($datos -> fecha_salida, 11, 8);
            echo $dates -> cambiar_formato_fecha($temp1) . ' ' . $temp2;
				?></label>
		</div>
		<div class="sistema duracion" style="width: 9em">
			<label> <?php	echo $datos -> duracion; ?></label>
		</div>
		<div class="sistema estado" style="width: 11em">
			<label> <?php	echo $datos -> sesion; ?></label>
		</div>
		<?php
        }
		?>
	</div>
	<div class="meneame" style="margin-top:76%;">
    <?php
    if ($total > $nroRegistros)
        for ($cont = 1; $cont <= $plinks; $cont++) {
            if (($cont % 20) == 1)
                echo "<br/><br/><br/><br/>";
            $temp = $cont - 1;
            echo "<a id=$cont name='pagina' onclick=cargarBitacora('usuario','bitacora',$temp)>$cont</a> ";
        }
     ?>
    </div>
</div>
<script type="text/javascript">
    function cargarBitacora(mdl, pag, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            pag : pag,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript" >
    $(document).ready(function() {
        var celdas = ['nSolicitud','productor','ingreso','salida','duracion','estado'];
        $.funciones.alternarColores(celdas);
    })
</script>