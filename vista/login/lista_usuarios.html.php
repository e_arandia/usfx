<h2>LISTA DE USUARIOS</h2>
<div id="calendar" style="width: 91.4%">
	<div class="head" style="height: 3em">
		<div class="htitle">
			<label style="width:4.5em;padding-top: 1%;">Numero</label>
			<label style="width:9.1em;padding-top: 1%;">Apellido</label>
			<label style="width:9em;padding-top: 1%;">Nombre</label>
			<label style="width:9em;padding-top: 1%;">Area</label>
			<label style="width:7.5em;padding-top: 1%;">Fecha de Registro</label>
			<label style="width:7.5em;padding-top: 1%;">Usuario</label>
			<label style="width:7.5em;padding-top: 1%;">Estado</label>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
		</div>
	</div>
	<div class="body" style="width:100%">
		<?php
$count = 1;
while ($datos = DBConnector::objeto()){
		?>
		<div class="nSolicitud nro" style="height:24px; width: 4.5em">
			<label><?php   echo $count; ?></label>
		</div>
		<div class="productor apellido" style="height:24px;width:9em;">
			<label> <?php	echo $datos -> apellido; ?></label>
		</div>
		<div class="productor nombre" style="height:24px;width:9em;">
			<label> <?php	echo $datos -> nombre; ?></label>
		</div>
		<div class="sistema area" style="width: 9em;height:24px">
			<label> <?php	echo $datos -> area; ?></label>
		</div>
		<div class="fecha registro" style="width: 7.5em;height:24px">
			<label> <?php	echo $format -> cambiar_formato_fecha($datos -> fecha_ingreso); ?></label>
		</div>
		<div class="productor usuario" style="width: 7.5em;height:24px">
			<label> <?php   echo $datos -> login; ?></label>
		</div>
		<div class="productor estado"  style="width: 7.5em;height:24px">
			<label> <?php	echo $datos -> estado; ?></label>
		</div>
		<div class="sistema options" style="height: 24px; width: 8.5em;">
			<div class="editar">
				<input name="upd" type="image" title="Editar" src="images/user_edit.png" style="position: relative;top: -0.3em;right: 2em;" value="<?php echo $datos -> id_login; ?>" />
			</div>
			<div class="eliminar">
				<input name="del" type="image" title="Eliminar" src="images/user_delete.png" style="position: relative;top: -2.8em;right: -1em;" value="<?php echo $datos -> id_login; ?>" />
			</div>
		</div>
		<?php
        $count++;
        }
		?>
		<input id="id" type="hidden"/>
	</div>
	<?php
    include '../vista/confirmacion/confirmar.html';
    ?>

	<div>
		<img  class="user_add" src="images/user_add.png" title="Agregar usuario"/>
	</div>	
	
	<div class="add_user"></div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var celdas = ['nro','apellido','nombre','fono','area','mail','registro','usuario','estado','options'];
        $.funciones.alternarColores(celdas);
        var opciones = ['editar','eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
        /** agregar usuario */
        $(".user_add").css({
            "cursor" : "pointer",
            "margin-top" : "1%"
        }).click(function() {
            $.get('control/index.php', {
                mdl : 'login',
                pag : 'nUserAdmin'
            }, function(response) {
                $('.add_user').empty().append(response);
            });
        });

        /**eliminar usuario*/
        $("input[name=del]").mouseover(function() {
            $("#id").val($(this).val());
        }).click(function() {
            $(".message").empty().append('Desea eliminar el usuario?');
            $('#dialog').dialog({
                title : 'Confirmar',
                dialogClass: "no-close",
                resizable : false,
                modal : true,
                width : 260,
                buttons : {
                    "Aceptar" : function() {
                        $(this).dialog("close");
                        var id = $("#id").val();
                        $.get('control/index.php?mdl=login&pag=eliminar&id=' + id, function(data) {
                            if (data == 'OK') {
                                $.funciones.mostrarMensaje('ok', 'Usuario eliminado');
                                $.funciones.ocultarMensaje(2000);
                                var url = 'control/index.php';
                                $.get(url, {
                                    mdl : 'login',
                                    pag : 'listar'
                                }, function(data) {
                                    $(".post").empty().append(data);
                                });
                            } else {
                                $.funciones.mostrarMensaje('error', 'Ocurrio un error al intentar eliminar el usuario.\nIntente nuevamente');
                                $.funciones.ocultarMensaje(2500);
                            }
                        });
                    },
                    "Cancelar" : function() {
                        $(this).dialog("close");
                    }
                }
            });
            /////////////////////////////////////

            ///////////////////////////////
        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#id").val($(this).val());
        }).click(function() {
            var id = $("#id").val();
            $.post('control/index.php?mdl=login&pag=updUsuario&id=' + id, function(data) {
                $(".add_user").empty().append(data);
            });
        });
        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    });
</script>