<div id="edit" class="entry" style="display: none">
    <h2>Modificar permisos</h2>
    <form id="pactualizar-permisos" class="nuevoform" style="padding: 1% 4%;">
        <div style="width: 27em;">
            <label>Nombre</label>
            <input class="permiso pname" />
        </div>
        <div style="width: 27em;">
            <label>Apellido</label>
            <input class="permiso psname" />
        </div>
        <div>
            <label>Editar informaci&oacute;n?</label>
        </div>
        <div>
            <select class="psistem" name="psistem" style="width: 60px"> </select>
        </div>
        <div>
            <input type="hidden" class="pid" name="pid"/>
        </div>
        <div style="float:right;margin-top:-7%;">
            <button id="permiso" class="btn btn-success">
                <span class="glyphicon glyphicon-plus-sign"></span> Actualizar
            </button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("button#permiso").on("click",function(event){
            event.preventDefault();
            id = $(".pid").val();
            sistem = $("select.psistem").find(":selected").val();
            $.funciones.guardarPermisos(id,sistem);
        });
    })
</script>