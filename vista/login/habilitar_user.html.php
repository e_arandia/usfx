<div id="user-allow" class="entry" style="display: none">
    <h2>Habilitar - Deshabilitar usuario</h2>
    <form id="pactualizar-habilitar" class="nuevoform" style="padding: 1% 4%;">
        <div style="width: 27em;">
            <label>Nombre</label>
            <input class="permiso pname" />
        </div>
        <div style="width: 27em;">
            <label>Apellido</label>
            <input class="permiso psname" />
        </div>
        <div>
            <label style="width: 11%;">Habilitar?</label>
        </div>
        <div>
            <select class="uallow" name="uallow" style="width: 60px"> </select>
        </div>
        <div>
            <input type="hidden" class="pid" name="pid"/>
        </div>
        <div style="float:right;margin-top:-7%;">
            <button id="habilitar-user" class="btn btn-success">
                <span class="glyphicon glyphicon-plus-sign"></span> Actualizar
            </button>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("button#habilitar-user").on("click",function(event){
            event.preventDefault();
            id = $(".pid").val();
            estado = $("select.uallow").find(":selected").val();
            //console.log(id+'='+sistem);
            $.funciones.habilitarUsuario(id,estado);
        });
    })
</script>