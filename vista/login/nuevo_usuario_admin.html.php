<div style="width: 40%; margin: 4% 0px 0px 33%;">
	<h2 class="title">Registrar nuevo usuario</h2>
</div>
<div class="post">
	<div class="entry">
		<form id="registrar"  class="nuevo" action="control/index.php" method="post">
			<div>
				<label class="required" style="width: 25%">Nombre </label>
				<input id="nombre" name="nombre" type="text"/>
				<span class="required" style="left:7.5em;position:absolute;">&nbsp;&nbsp;</span>
			</div>
			<div>
				<label style="width: 25%">Apellido</label>
				<input id="apellido" name="apellido" type="text"/>
				<span class="required"style="left:7.5em;position:absolute;">&nbsp;&nbsp;</span>
			</div>
			<div>
				<label style="width: 25%">Area </label>
				<select id="area" name="area" class="required">
					<option value="">[ Seleccione area ]</option>
					<option value="10">Administraci&oacute;n</option>
					<option value="1">Certificaci&oacute;n</option>
					<option value="2">Fiscalizaci&oacute;n</option>
					<option value="3">Laboratorio</option>
				</select>
				<span class="required"style="left:7.5em;position:absolute;">&nbsp;&nbsp;</span>
			</div>
			<div>
				<label style="width: 25%">Usuario </label>
				<input id="login" name="login" type="text"/>
				<span class="required" style="left:7.5em;position:absolute;">&nbsp;&nbsp;</span>
				<span class="checked"></span>
			</div>
			<div>
				<label style="width: 25%">Generar contrase&ntilde;a</label>
				<input id="generar" class="genpass input-not-edit"  name="generar" type="text"   readonly="true"/>
				<!--<input type="button" name="submit" value="Generar" class="button-generar ui-state-default ui-corner-all" id="generarbutton" style="position: absolute; left: 50%; top: 45.7%;">-->
				<button id="generar-pass" class="btn btn-success">
                    <span class="glyphicon glyphicon-cog"></span>
                    Generar
                </button>
			</div>
			<div>
				<label style="width: 25%">Contrase&ntilde;a </label>
				<input type="password" name="pass" id="pass" style="height: 45%;">
				<span class="required" style="left:7.5em;position:absolute;">&nbsp;&nbsp;</span>
			</div>
			<div style="margin-top: 2em">
				<label style="width: 25%">Repita Contrase&ntilde;a</label>
				<input id="pass2" name="pass2" type="password"/>
			</div>
			<input type="hidden" id="usuario" name="usuario" value="<?php echo $_SESSION['usr_nivel']?>"/>
			<div style="margin-left: 65%">
				<button id="enviar-usuario" class="btn btn-success">
                    <span class="glyphicon glyphicon-user"></span>
                    Registrar
                </button>
			</div>
			<div style="float: left; width: 60%;">
			    <span class="required">&nbsp;&nbsp;</span>Los campos son obligatorios 
			</div>
			<div>&nbsp;</div>
		</form>
		<?php
		include '../vista/error/errores.php';
		include '../vista/confirmacion/confirmar.html';
		?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $("#area").selectmenu();
		$("#errores").css({
			"padding" : "0 .7em",
			"width" : "35em"
		});
		$('<span></span>').insertBefore('.mensaje').css({
			'float' : 'left',
			'margin-right' : '.3em',
			'margin-top' : '0.5em'
		}).addClass("ui-icon ui-icon-alert");
		
		$("#generar-pass").on("click",function(event){
		    event.preventDefault();
		    $.funciones.generarPass('generar');
		    $()
		});
		$("#registrar").validar();
		$("#registrar").registrar();
	})
</script>
<!-- Validacion y envio de datos-->
<script type="text/javascript">
	$(document).ready(function() {
	    
		$(".password").after("<div><span class='avisoA' >(Debe ser m&iacute;nimo 6 caracteres)</span></div>");
		// verificar disponibilidad del nombre de usuario
		$("#login").blur(function() {
			$.funciones.verificarDisponibilidad('login');
		});
		$("input[name=submit1]").click(function() {

			$("#registrar").submit();
		});
		$("#registrar").ajaxForm({
			url : 'control/index.php?mdl=usuario&pag=registrar',
			type : 'post',
			resetForm : true,
			beforeSubmit : showRequest,
			success : showResponse
		});
		function showRequest(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];

			if (!form.nombre.value || form.nombre.length < 3) {
				mensaje += '<div>- Nombre inv&aacute;lido</div>';
				$("#nombre").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#nombre").css({
					backgroundColor : ""
				});
			}
			if (!form.apellido.value || form.apellido.length < 3) {
				mensaje += '<div>- Apellido inv&aacute;lido</div>';
				$("#apellido").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#apellido").css({
					backgroundColor : ""
				});
			}
			if (!form.area.value) {
				mensaje += '<div>- Area inv&aacute;lido</div>';
				$("#area").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#area").css({
					backgroundColor : ""
				});
			}
			if (!form.login.value || form.login.length < 5) {
				mensaje += '<div>- Nombre de usuario inv&aacute;lido </div>';
				$("#login").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#login").css({
					backgroundColor : ""
				});
			}
			if (!form.pass.value || form.pass.length <= 6) {
				mensaje += '<div >- Contrase&ntilde;a inv&aacute;lida(Debe tener por lo menos 6 caracteres)</div>';
				$("#pass").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#pass").css({
					backgroundColor : ""
				});
			}
			if (!form.pass2.value || form.pass2.length <= 6) {
				mensaje += '<div>- La contrase&ntilde;a es invalida</div>';
				$("#pass2").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#pass2").css({
					backgroundColor : ""
				});
			}
			if (form.pass.value != form.pass2.value) {
				mensaje += '<div>- Las contrase&ntilde;as son diferentes</div>';
				$("#pass2").css({
					backgroundColor : "#f5c9c9"
				});
			} else {
				$("#pass2").css({
					backgroundColor : ""
				});
			}
			if (mensaje != '') {
				$("#errores div p .mensaje").empty().append('Se han detectado los siguientes errores: <br>' + mensaje);
				$("#errores").css('margin-top', '4em').fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				return true;
			}
		}

		function showResponse(responseText, statusText, xhr, $form) {
			switch (responseText) {
				case 'error','':
					$(".checked").empty();
					$("#dialog:ui-dialog").dialog("destroy");
					$(".ui-icon").addClass(" ui-icon-alert").after(statusText);
					$("#dialog-message").attr('title', 'Informacion').fadeIn();
					$("#dialog-message").dialog({
					    dialogClass: "no-close",
						modal : true,
						resizable : false,
						buttons : {
							Ok : function() {
								$(this).dialog("close");
							}
						}
					});
					break;

				case 'OK':
					$(".checked").empty();
					$("#dialog:ui-dialog").dialog("destroy");
					$("#ui-icon").addClass("ui-icon-info").empty().after("Registro exitoso!!!");
					$("#dialog-message").attr('title', 'Informacion');
					$("#dialog-message").dialog({
					    dialogClass: "no-close",
						modal : true,
						resizable : false,
						buttons : {
							Ok : function() {
								$(this).dialog("close");
							}
						}
					});
					setTimeout($.funciones.recargarVerDatosAdmin('login', 'listar'), 10000);
					break;
			}
		}

	}); 
</script>