<h2>PERMISOS DE USUARIOS</h2>
<div id="calendar" style="width: 113.3%">
	<div class="head" style="height: 3.2em">
		<div class="htitle">
			<label style="width:4.5em;padding-top: 1%;">Numero</label>
			<label style="width:9.1em;padding-top: 1%;">Apellido</label>
			<label style="width:9em;padding-top: 1%;">Nombre</label>
			<label style="width:9em;padding-top: 1%;">Area</label>
			<label style="width:7.5em;padding-top: 1%;">Crear</label>
			<label style="width:7.5em;padding-top: 1%;">Ver</label>
			<label style="width:7.5em;padding-top: 1%;">Editar</label>
			<label style="width:7.5em;padding-top: 1%;">Eliminar</label>
			<label style="width:8.4em;padding-top: 1%;">Modificar Permisos </label>
			<label style="width:7.5em;padding-top: 1%;">Estado</label>
		</div>
	</div>
	<div class="body" style="width:100%">
	    <?php 
	    $count=0;   
	    foreach ($users as $value) {
	        #echo array_keys($value);exit;
	        foreach ($value as $keys=>$values) {        
	    ?>	    
		<div class="nSolicitud nro" style="height:24px; width: 4.5em">
            <label><?php   echo $value['count']; ?></label>
        </div>
		<div class="productor apellido" style="height:24px;width:9em;">
			<label> <?php	echo $value['apellido']; ?></label>
			
		</div>
		<div class="productor nombre" style="height:24px;width:9em;">
			<label> <?php	echo $value['nombre']; ?></label>
		</div>
		<div class="sistema area" style="width: 9em;height:24px">
			<label> <?php	echo utf8_encode($value['area']); ?></label>
		</div>
		<div class="fecha registro create" style="width: 7.5em;height:24px">
			<div class="create"> <?php	if ($value['crear']){?>
			    <input name='crud' type='image' title="SI" src='images/check.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>    
			<?php }else{ ?>			   
			    <input name='ins' type='image' title="NO" src='images/error.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>
			<?php } ?></div>
		</div>
		<div class="productor usuario view" style="width: 7.5em;height:24px">
			<div class="view"> <?php   if( $value['ver']){?> 
			    <input name='view' type='image' title="SI" src='images/check.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>			    
            <?php }else{ ?>
                <input name='view' type='image' title="NO" src='images/error.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>
            <?php
            }
 ?></div>
		</div>
		<div class="productor estado update"  style="width: 7.5em;height:24px">
			<div class="update"> <?php	if ($value['modificar']){?>
			    <input name='del' type='image' title="SI" src='images/check.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/> 
			<?php
            }else{
                ?>
                <input name='del' type='image' title="NO" src='images/error.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>
           <?php
        }
 ?></div>
		</div>
		<div class="productor estado delete"  style="width: 7.5em;height:24px">
            <div class="delete"> <?php   if ($value['eliminar']) { ?>                
                <input name='del' type='image' title="SI" src='images/check.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>
            <?php
            }else{
                ?>
                <input name='del' type='image' title="NO" src='images/error.png' style='position: relative; top: -7%;margin-top: -7%;'value="<?php echo $value['id']; ?>"/>
                
            <?php } ?></div>
        </div>
		<div class="sistema options" style="height: 24px; width: 8.5em;">
			<div class="editar">
			    <?php if($value['area']!= 'administracion'){ ?>
				<input name="upd" type="image" title="SI" src="images/editar22x22.png" style="position: relative;top: -0.5em;" value="<?php echo $value['id']; ?>" />
				<?php }else { ?>
				<img src="images/edit-bg.png" title="NO" />     
                <?php } ?>
			</div>
		</div>
		<div class="sistema allow" style="height: 24px; width: 7.5em;">
		    <div class="user_OK"> <?php if ($value['estado']=='habilitado') { ?>                
                <input name='allow' type='image' src='images/user_OK.png' style='position: relative; top: -6%;margin-top: -6%;'value="<?php echo $value['id']; ?>"/>
            <?php
            }else{
                ?>
                <input name='allow' type='image' src='images/user-bg_OK.png' style='position: relative; top: -6%;margin-top: -6%;'value="<?php echo $value['id']; ?>"/>
                
            <?php } ?></div>
		</div>
		<input id="id" type="hidden"/>
		<?php
        break;
        }
        }
		?>
	</div>
	<?php
    include '../vista/confirmacion/confirmar.html';
    include '../vista/error/errores.php';
    ?>	
</div>
<?php
include 'permiso_user.html.php';
include 'habilitar_user.html.php';
?>
<script type="text/javascript">
    $(document).ready(function() {

        //$("div.body >div.:odd,div.body >div.:odd,div.body >div.:odd,div.body >div.:odd,div.body >div.:odd").css('background-color', '#edffd4');
        //$("div.body >div.:odd,div.body >div.:odd,div.body >div.:odd,div.body >div.:odd,div.body >div.:odd,div.body>div.allow:odd").css('background-color', '#edffd4');
        
        
        var celdas = ['nro','apellido','nombre','fono','area','update','delete','create','view','options','allow'];
        $.funciones.alternarColores(celdas);
        var cvee = ['create','view','delete','update'];
        $.funciones.alternarColorCVEE(cvee);
        var modificar = ['editar'];
        $.funciones.alternarColorModificarPermisos(modificar);
        var permisos = ['user_OK'];
        $.funciones.alternarColorEstado(permisos);
        /**mensaje de informacion*/
        var mensaje = 'Acciones que pueden realizar los usuarios en el sistema';
        $("p.adv").empty().html("<span style='float: left; margin-right: .3em;'></span>").append(mensaje);
        $("p.adv>span").addClass('ui-icon-info').addClass('ui-icon').show();
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#id").val($(this).val());
        }).click(function() {
            var id = $("#id").val();
            $.getJSON('control/index.php', {
                mdl : 'login',
                pag : 'perdetus',
                id : id
            }, function(json) {
                $(".pname").val(json.nombre);
                $(".psname").val(json.apellido);
                if (json.crud == 3) {
                    $('.psistem').empty().append('<option value="d" selected="selected">No</option>');
                    $('.psistem').append('<option value="h">Si</option>');
                } else {
                    $('.psistem').empty().append('<option value="h" selected="selected">Si</option>');
                    $('.psistem').append('<option value="d">No</option>');
                }
                $(".pid").val(json.id);
                $("select.psistem").selectmenu();
                $("div#edit").css("margin-top", "25%").fadeIn();
                $("div#user-allow").fadeOut();
            });
        });
        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
        
        //habilitar o deshabilitar usuario 
        $("input[name=allow]").mouseover(function() {
            $("#id").val($(this).val());
        }).click(function() {
            var id = $("#id").val();
            $.getJSON('control/index.php', {
                mdl : 'login',
                pag : 'allow',
                id : id
            }, function(json) {
                $(".pname").val(json.nombre);
                $(".psname").val(json.apellido);
                if (json.estado == 2) {
                    $('select.uallow').empty().append('<option value="n">No</option>');
                    $('select.uallow').append('<option value="y">Si</option>');
                } else {
                    $('select.uallow').empty().append('<option value="y">Si</option>');
                    $('select.uallow').append('<option value="n">No</option>');
                }
                $(".pid").val(json.id);
                $("select.uallow").selectmenu();
                $("div#user-allow").css("margin-top", "25%").fadeIn();
                $("div#edit").fadeOut();
            });
        });
        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    }); 
</script>