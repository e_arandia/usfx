<div class="entry">
    <h2>detalle certificaci&oacute;n, producci&oacute;n y costos</h2>
	<form id="semilleras" style="padding: 3% 2% 4%;">
		<div id="tabs">
			<ul>
				<li>
					<a href="#trimestral" id="li-id-trim">Trimestral</a>
				</li>
				<li>
					<a href="#rangoFechas" id="li-id-rango">Entre rango de fechas</a>
				</li>
			</ul>
			<div id="trimestral" style="height: 150px">
				<div id="radio-btn">
					<div>
						<label style="width: 10%;">Cultivo:</label>
						<select id="trim-cultivo" name="cultivo">
							<option value="">[ Seleccionar cultivo ]</option>
						</select>
					</div>
					<label for="efm" style="width: 9%;margin-right: 5%">Enero Febrero Marzo</label>
                    <input type="radio" name="option" id="efm" value="1">
                    
                    <label for="amj" style="width: 8%;margin-right: 5%">Abril Mayo Junio</label>
                    <input type="radio" name="option" id="amj" value="2" style="width: 15%">
                    
                    <label for="jas" style="width: 13%;margin-right: 5%">
                        <br>
                        Julio
                        <br>
                        Agosto
                        <br>
                        Septiembre</label>
                    <input type="radio" name="option" id="jas" value="3">
                    
                    <label for="ond" style="width: 11%">Octubre Noviembre Diciembre</label>
                    <input type="radio" name="option" id="ond" value="4">
				</div>
			</div>
			<div id="rangoFechas">
				<div>
					<label style="width: 10%;">Cultivo:</label>
					<select id="rango-cultivo" name="cultivo">
						<option value="">[ Seleccionar cultivo ]</option>
					</select>
					<div style="width: 75%">
                        <div>
                            <label style="width: 20%">Desde : </label>
                            <input type="text" id="f_desde" style="width: 80px" name="desde" value="<?php echo '01-01-'.date('Y');?>"/>
                        </div>
                        <div>
                            <label style="10%">Hasta : </label>
                            <input type="text" id="f_hasta" style="width: 80px" name="hasta"/>
                        </div>                  
                    </div>
				</div>
			</div>
			
			<button class="btn btn-success" type="button" id="detail" style="margin-left:8px;margin-bottom:15px;visibility:hidden;display: none">
                <span class="fa fa-file-text-o" style="font-size:36px;color:green;"></span>
                DETALLE
            </button>
            <button class="btn btn-success" type="button" id="pdf" style="float:right;margin-right: 10px;display:none;">
                <span class="fa fa-file-pdf-o" style="font-size:36px;color:red;"></span>
                PDF
            </button>
            <button class="btn btn-success" type="button" id="xls" style="float:right;margin-right: 10px;display:none;">
                <span class="fa fa-file-excel-o" style="font-size:36px;color:green;"></span>
                EXCEL
            </button>    
            <input type="hidden" id="tab_clicked" value="trim"/>  
		</div>
	</form>
</div>

<div class="pcostos">
    
</div>
<?php
include '../vista/error/advertencia.php';
include '../vista/confirmacion/advertencia_detalle_produccion.html';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#li-id-trim").click(function(){
            $("input#tab_clicked").empty().val('trim');  
        });
        $("#li-id-rango").click(function(){
            $("input#tab_clicked").empty().val('rango');  
        });
        $('select#trim-cultivo').selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        $('select#rango-cultivo').selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        $.funciones.cargarListaCultivos('trim-cultivo');
        $.funciones.cargarListaCultivos('rango-cultivo');
        //inicializacion de calendario
        $.funciones.calendarioInicial('input', 'f_desde');
        $.funciones.calendarioFinal('input', 'f_hasta');
        //opcion por defecto primer trimestre
        $("input#efm").attr('checked','checked');
        //opciones trimestrales
        $("input[name=option]").checkboxradio();
        //creacion de tabs
        $( "#tabs" ).tabs({
          activate: function( event, ui ) {
              //ocultar boytones pdf-excel-detalle
              $("button#detail,button#pdf,button#xls").fadeOut();
          }
        });
        $("select#trim-cultivo,select#rango-cultivo").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#trim-cultivo").on("selectmenuchange", function(event, ui) {
            if ($(this).val() != '') {
                $("button#detail,button#pdf,button#xls").fadeIn();
                
            } else {
                $("button#detail,button#pdf,button#xls").fadeOut();
            }
        });
        $("select#rango-cultivo").on("selectmenuchange", function(event, ui) {
            if ($(this).val() != '') {
                $("button#detail,button#pdf,button#xls").fadeIn();
                
            } else {
                $("button#detail,button#pdf,button#xls").fadeOut();
            }
        });
        
        ///////////////////////////////////////////////////////////////////////////////
        //////          b o t o n    p a r a     P D F                           //////
        ///////////////////////////////////////////////////////////////////////////////
        $("button#pdf").on("click",function(){
            var tab_option = $("input#tab_clicked").val();            
            if(tab_option == 'trim'){
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var trimestre = $("input:radio[name=option]:checked").val();
                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=detalle&area=1&tipo=trim&cultivo='+cultivo+'&chs='+trimestre, '_blank');
                win.focus();
            }else{
                desde = $('input#f_desde').val();
                hasta = $('input#f_hasta').val();
                if (desde != '') {
                    var cultivo = $("select#trim-cultivo").find(":selected").val();
                    var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=detalle&area=1&tipo=rango&cultivo='+cultivo+'&desde='+desde+'&hasta='+hasta, '_blank');
                    win.focus();
                }else{
                    $(".alert>label").empty().append("Debe seleccionar una opcion");
                    $(".alert").show().fadeOut(3000);
                }
            }
        });
        ///////////////////////////////////////////////////////////////////////////////
        //////          b o t o n    p a r a     EXCEL                           //////
        ///////////////////////////////////////////////////////////////////////////////
        $("button#xls").on("click",function(){
            var tab_option = $("input#tab_clicked").val();
            
            if(tab_option == 'trim'){
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var trimestre = $("input:radio[name=option]:checked").val();
                $.getJSON('control/index.php', {
                      mdl : 'informe',
                     opt : 'xls',
                     pag : 'produccion_costos',
                     cultivo : cultivo,
                     area : 1,
                     tipo : 'trim',
                     chs : trimestre
                 }, function(json) {
                     if (json.msg == 'OK'){
                         $(".pcostos").empty().append("<iframe class='icostos' style='display:none' src='" + json.file + "'></iframe>");
                         window.open(json.file,'_parent' );
                     }else{
                         $("#advertencia>.ui-state-error>p>label.msg").empty().append(json.msg).show();
                         $("#advertencia").show().fadeOut(3000);
                     }
                 });
            }else{
                desde = $('input#f_desde').val();
                hasta = $('input#f_hasta').val();
                if (desde != '') {
                    var cultivo = $("select#rango-cultivo").find(":selected").val();             
                    $.getJSON('control/index.php', {
                        mdl : 'informe',
                        opt : 'xls',
                        pag : 'produccion_costos',
                        cultivo : cultivo,
                        area : 1,
                        tipo : 'rango',
                        desde : desde,
                        hasta : hasta
                    }, function(json) {
                        if (json.msg == 'OK'){
                            $(".pcostos").empty().append("<iframe class='icostos' style='display:none' src='" + json.file + "'></iframe>");
                            window.open(json.file,'_parent' );
                        }else{
                            $("#advertencia>.ui-state-error>p>label.msg").empty().append(json.msg).show();
                            $("#advertencia").show().fadeOut(3000);
                        }
                    });
                }else{
                    $(".alert>label").empty().append("Fecha invalida");
                    $(".alert").show().fadeOut(3000);
                }
            }
        });
    })
</script>