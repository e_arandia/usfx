<?php
    include 'seguimiento_superficie_inspeccion.html';
?>

<div id="semright" style="width: 49%;float: left;margin-top:0;">
    <div>
        <label style="margin-left: 50%; width: 35%;margin-top: -2%;"> Fecha </label>
    </div>
    <div style="margin-bottom: 47%;margin-top: 15%;">
        <label style="width: 36%">Etapa Vegetativa</label>
        <input id="ins_1" name="evegetativa" type="text" class="special" style="width: 170px" value="<?php echo date('d-m-Y');?>"/>
        <input name='tipo' type="hidden" value='vegetativa'/>
    </div>

    <div class="flora" style="margin-bottom: 40%;margin-top: 5%;display: none">
        <div>
            <label style="margin-left: 50%; width: 35%;margin-top: -11%;"> Fecha </label>
        </div>
        <label style="width: 36%">Etapa Floraci&oacute;n</label>
        <input id="ins_2" name="efloracion" type="text" class="special" style="width: 170px;"/>
        <input name='tipo' type="hidden" value='floracion'/> 
    </div>

    <div class="cose" style="margin-bottom: 25%;margin-top: 5%;display: none">
        <div>
            <label style="margin-left: 50%; width: 35%;margin-top: -9%;"> Fecha </label>
        </div>
        <label style="width: 36%">Etapa Precosecha</label>
        <input id="ins_3" name="ecosecha" type="text" class="special" style="width: 170px;"/>
        <input name='tipo' type="hidden" value='precosecha'/>
    </div>

</div>
<div id="semleft" style="width: 50%;float:right;margin-top:0;">
    <div>
        <label style="width: 25%;">Observacion</label>
        <textarea id="obs1" name="obs_1" style="width: 228px; height: 89px;"></textarea>
    </div>
    <div class="obs2" style="display: none;margin:0">
        <label style="width: 25%;margin-top:12%;">Observacion</label>
        <textarea id="obs2" name="obs_2" style="width: 228px; height: 89px;"></textarea>
    </div>
    <div class="obs3" style="display: none">
        <label style="width: 25%;margin-top:12%;">Observacion</label>
        <textarea id="obs3" name="obs_3" style="width: 228px; height: 89px;"></textarea>
    </div>
</div>
<div style="float: right">
    <input name="plantines" type="hidden"/>
    <input id="insp" name="insp" type="hidden" value="1"/>
    <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
    <input id="opt" name="opt" type="hidden" value="guardar"/>
    <input id="pag" name="pag" type="hidden" value="inspeccion"/>

    <button id="enviar-inspeccion" class="btn btn-success" type="button">
        <span class="glyphicon glyphicon-floppy-disk"></span>
        Registrar
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        //envio de datos
        //$("form#inspeccion").enviarInspeccion();
        var stage = $("#stage").val();
        var txt = $("h2.title").text();
        $("button#enviar-inspeccion").click(function() {
            datos = $("form#seguimiento").serialize();

            $.ajax({
                url : 'control/index.php',
                data : datos,
                type : 'POST',
                beforeSend : function() {
                    $.funciones.deshabilitarTexto("enviar-inspeccion"); 
                },
                success : showResponse
            });
            function showResponse(responseText, statusText, xhr, $form) {
                switch (responseText) {
                case 'OK' :
                    $("div.div-new").fadeIn();
                    $("div.div-filter").hide();
                    $("form#filter-box").slideUp();
                    $("div#btn-new-filter").css({
                        'float' : 'right',
                        'margin-top' : '6%'
                    });
                    setTimeout($.funciones.recargarVerDatos('certificacion','vinspecciones',1,1100), 10000);
                    break;
                default:
                    $.funciones.mostrarMensaje('error', responseText);
                    $.funciones.ocultarMensaje(5000);
                    break;
                }
            }

        });
    }); 
</script>