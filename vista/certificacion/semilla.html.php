<h2 class="title">Datos de la Semilla sembrada</h2>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
    <div style="float: right; margin-top: -10%;">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <form id="form-buscador" class="seguimiento">
        <?php
            include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <?php
        include '../vista/error/advertencia.php';
    ?>
    <form id="semilla" class="nuevoform" style="padding: 3% 2% 1%;display: none" autocomplete="off">
        <?php
        include 'seguimiento.html';
        ?>
        <div>
            <label style="width: 20%;margin-top:3.5px;">N&uacute;mero de Campo</label>
            <input id="nrocampo" class="not-edit" name="nrocampo" type="text" class="special number"  maxlength="5" style="width: 30px;"/>
        </div>
        <?php
        include 'opciones_semilla.html.php';
        ?>
    </form>
</div>
<?php
include '../vista/dialogos/agregarCultivo.html';
include '../vista/dialogos/agregarVariedad.html';
include '../vista/dialogos/plantasPorHectarea.html';
include '../vista/dialogos/cantidadSemillaEmpleada.html';
include '../vista/error/errores.php';
include '../vista/error/aviso.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("div#anteriores>div>label").css({'width':'15%'});
        $("div#anteriores>div>input").css({'width':'310px'});
        $("div#anteriores>div>span#advice").css({'float':'left','margin-left':'15%'});
        //autocompletado de numero de solicitudes
        $.buscar.autocompletarSemilla();
        //fecha plantines
        //calendario
        $("#inspeccion-plantines").calendarioSolicitud('inspeccion-plantines');
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("input#buscarTXT").on("keyup",function(event){
            if(event.which == 13)
                $.buscar.semillaCertifica();
                //alert('lol');
        });
        $("button#btn-search").on("click", function() {
            $.buscar.semillaCertifica();           
        });
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //configuracion y validacion de campos
        $("input#plantasha").numeric(",");
        $("input#distanciasurcos").numeric();
        $("input#cant_semilla").numeric();
        $("input#superficie").numeric();
        $("input#nroplantas").numeric();
        $.funciones.soloNumerosFecha("emision");
        $("select#cultivo").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        $("select#cult_anterior").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        //creacion de selects
        $("select#nosolicitud,select#productor,select#cat_producir").selectmenu();
        $("select#variedad").selectmenu({
            disabled : true
        });
        //campaña segun el mes
        $.funciones.getCampanhaSpan('campania');
        //carga los cultivos de plantines
        $.funciones.cargarPlantines();
        //cargar el nro de campo automaticamente
        var productor = $('#productor');        
        $.getJSON("control/index.php", {
            mdl : 'certificacion',
            opt : 'campo'
        }, function(json) {
            if (json.estado === 'OK') {
                var total = parseInt(json.nocampo);
                $("#nrocampo").val(total);
            } else {
                $("#nrocampo").val(json.nocampo);
            }
        });
        //carga de cultivos disponibles
        var solicitud = $('input[name=solicitud]');
        function cargarCultivos() {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ls_costos'
            }, function(json) {
                if (json.cultivos.length > 0) {
                    $.each(json.cultivos, function(index, value) {
                        $('select#cultivo').append('<option value="' + value + '">' + value + '</option>');
                    });
                    $("select#cultivo").append('<option value="add_cultivo">Agregar cultivo</option>');
                }
            });
        }
        cargarCultivos();
        function cargarPlantines() {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ls_costos_plantines'
            }, function(json) {
                if (json.total > 0) {
                    $.each(json.cultivos, function(index, value) {
                        $('select#cultivo-plantines').append('<option value="' + value + '">' + value + '</option>');
                    });
                    $("select#cultivo-plantines").append('<option value="add_cultivo">Agregar cultivo</option>');
                }else{
                    $("select#cultivo-plantines").empty().append('<option value="add_cultivo">Agregar cultivo</option>');    
                }                
            });
            $("select#cultivo-plantines").selectmenu();
            $("select#variedad-plantines").selectmenu();
        }
        cargarPlantines();
        $.funciones.cargarVariedadPlantines();
        $.funciones.cargarVariedadesDePlantin();
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        //registrar datos de semilla
        $("#enviar-semilla").on("click", function() {            
            $.funciones.enviarSemillaCertificada();
        });
        //resigstrar datos acondicionamiento
        $("#enviar-acondicionamiento").on("click", function() {            
            $.funciones.enviarAcondicionamiento();
        });
        //registrar datos de plantines
        $("#enviar-plantines").on("click", function() {            
            $.funciones.enviarPlantines();
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        ////////////////////////manejo de eventos de botones
        //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilla',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        }); 
        //carga de cultivo anterior
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_costos'
        }, function(json) {
            if (json.cultivos.length > 0) {
                $.each(json.cultivos, function(index, value) {
                    if (value !== "Acondicionamiento")
                        $('select#cult_anterior').append('<option value="' + value + '">' + value + '</option>');
                });
                $('select#cult_anterior').append('<option value="Barbecho">Barbecho</option>');
                $('select#cult_anterior').append('<option value="Descanso">Descanso</option>');
            }
            $("select#cult_anterior").selectmenu();
        });

        $("select#variedad").selectmenu({
            change : function(event, ui) {
            }
        });
        //carga de variedades de un cultivo
        function cargarVariedades(cultivo) {
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'ls_variedades',
                cultivo : cultivo
            }, function(json) {
                if (json.variedades.length > 0) {
                    $('select#variedad').empty();
                    $("select#variedad").append('<option value="">[Seleccione variedad]</option>');
                    $.each(json.variedades, function(index, value) { 
                        nombre_variedad = $.funciones.cambiarSignoPorEnhe($.funciones.cambiarSignoPorAcento(value));                       
                        $('select#variedad').append('<option value="' + nombre_variedad + '">' + nombre_variedad + '</option>');
                    });
                    $("select#variedad").append('<option value="add_variedad">Agregar variedad</option>');
                    $("select#variedad").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
                } else {
                    $("select#variedad").empty();
                    $("select#variedad").append('<option value="">[Seleccione variedad]</option>');
                    $("select#variedad").append('<option value="add_variedad">Agregar variedad</option>');
                }
            });            
        }

        //carga de variedad de un cultivo
        $("select#cultivo").on("selectmenuchange", function(event, ui) {
            var cultivo = $(this).find(':selected').val();
            if (cultivo != 'Acondicionamiento' || cultivo != 'Plantines' && cultivo != '') {
                //cargamos el valor en un campo oculto en el formulario para ser guardado cuando envie todos los datos
                $("input#hiddenCultivo").val(cultivo);
                $("select#variedad").selectmenu({
                    disabled : false
                });
                cargarVariedades(cultivo);

                $("select#variedad").selectmenu("destroy").selectmenu();
            } else {
                $("select#variedad").selectmenu({
                    disabled : true
                });
            }
        });

        //cargar variedad en campo oculto
        $("select#variedad").on("selectmenuchange", function(event, ui) {
            var variedad = $(this).find(':selected').val();
            $("input#hiddenVariedad").val(variedad);
        })
        //carga de categoria sembrada
        var cultivo = $("select#cat_sembrada").val();
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_catsembrada'
        }, function(json) {
            if (json.csembrada.length > 0) {
                $("select#cat_sembrada").selectmenu("destroy").empty();
                $("select#cat_sembrada").append('<option value="">[ Seleccionar ]</option>');
                $.each(json.csembrada, function(index, value) {
                    $("select#cat_sembrada").append('<option value="' + json.id[index] + '">' + value + '</option>');
                });
            }
            $("select#cat_sembrada").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        });
        //categoria campo de acondicionamiento
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_catsembrada'
        }, function(json) {
            if (json.csembrada.length > 0) {
                //$("select#categoria_acondicionamiento").selectmenu("destroy").empty();
                $.each(json.csembrada, function(index, value) {
                    $("select#categoria_acondicionamiento").append('<option value="' + json.id[index] + '">' + value + '</option>');
                });
            }
            $("select#categoria_acondicionamiento").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        });
        //creacion de evento onchange
        $("select#cat_sembrada").selectmenu({
            change : function(event, ui) {
            }
        });
        //carga categoria a producir
        $("select#cat_sembrada").on("selectmenuchange", function(event, ui) {
            var csembrada = $("select#cat_sembrada").val();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ls_cproducir',
                csembrada : csembrada
            }, function(json) {
                $('select#cat_producir').append('');
                if (json.cat_free.length > 0) {
                    $("select#cat_producir").selectmenu("destroy").empty();
                    $("select#cat_producir").append('<option value="">[ Seleccionar ]</option>');
                    $("select#cat_producir").selectmenu();
                    $.each(json.cat_free, function(index, value) {
                        $("select#cat_producir").append('<option value="' + json.id[index] + '">' + value + '</option>');
                    });
                    $("select#cat_producir").selectmenu({
                        disabled : false
                    });
                    $("select#cat_producir").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
                } else {
                    $("select#cat_producir").selectmenu({
                        disabled : true
                    });
                }
            });
        });
        
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#nosolicitud").numeric();
        $("#superficieParcela,#cantidad").numeric(".");
        $("#cant_semilla").on("click", function() {
            calcularSemilla();
        });
        function semillaEmpleada() {
            superficieParcela = $("#superficieParcela").val();
            cantidad = $("#cantidad").val();
            total = parseFloat(superficieParcela) * parseFloat(cantidad);
            $("input#hiddenSemEmpleada").val((parseFloat(total)).toFixed(2));
            $("input#hiddenSupParcela").val(superficieParcela);

            $("input#cant_semilla").addClass('not-edit').val(total.toFixed(2));
            $("input#superficie").empty().val(superficieParcela);
        };
        function calcularSemilla() {
            var superficieParcela;
            var total;
            //$("#superficieParcela, #cantidad").val(0);
            $('#SemillaEmpleada').dialog({// Dialog
                title : 'Cantidad de Semilla Empleada',
                dialogClass : "no-close",
                resizable : false,
                height : 200,
                width : 300,
                buttons : {
                    "Aceptar" : function() {
                        var total = $("#hiddenSemEmpleada").val();
                        var supParcela = $("#superficieParcela").val();
                        semillaEmpleada();
                        //$("#superficie").val(supParcela);
                        
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                    }
                }
            });
        };

        function calcular() {
            var total;
            var nroPlantas = /^-?(?:\d+|\d{1,3}(?:@\d{3})+)(?:\.\d+)?$/.test($("#nroplantas").val()) ? $("#nroplantas").val() : 1;
            var lineal = $("#boleo").is(":checked");
            var mcuadrado = $("#mcuadrado").is(":checked");
            
            if (mcuadrado) {
                total = 10000 * nroPlantas;
            }

            if (lineal) {
                var parcela = $("input#superficie").val();
                          
                var distancia = /^-?(?:\d+|\d{1,3}(?:.\d{3})+)(?:\.\d+)?$/.test($("#distanciasurcos").val()) ? $("#distanciasurcos").val() : 1;

                var distanciaBy100m = 100 / distancia;
                var nroPlantasBy100m = parseFloat(nroPlantas) * 100;

                var aux0 = parseFloat(distanciaBy100m);
                var aux = (aux0.toFixed(1)) * parseFloat(nroPlantasBy100m);
                total = (aux.toFixed(2)) * parseFloat(parcela);
            }
            total = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(total) ? parseInt(total) : 'error';
            if (total !== '' || !isNaN(total)) {
                $("#plantasha").addClass('not-edit').val(total);
            }
        }


        $("#plantasha").on("click", function() {
            var cantidad_semilla = $("input#cant_semilla");
            if (cantidad_semilla.val() > 0){
                $("#msg-cant_semilla").css('visibility','hidden');
                cantidad_semilla.css('background-color','');
                $("#nroplantas,#distanciasurcos").val('');
                $('#plantasPorHa').dialog({// Dialog
                    title : 'Plantaciones por Hectarea',
                    dialogClass : "no-close",
                    resizable : false,
                    buttons : {
                        "Aceptar" : function() {
                            calcular();
                            $("#nroplantas").addClass("not-edit");
                            $(this).dialog("destroy");
                        },
                        "Cancelar" : function() {
                            $(this).dialog("close");
                        }
                    }
                });
            }else{
                cantidad_semilla.css('background-color','#f5c9c9').focus();    
                $("#msg-cant_semilla").css('visibility','');            
            }
            
        });
    }); 
</script>