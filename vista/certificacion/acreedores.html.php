<div class="entry">
	<form id="semilleras" class="seguimiento" style="padding: 3% 2% 4%;">
		<div id="tabs">
			<ul>
				<li>
					<a href="#trimestral">Trimestral</a>
				</li>
				<li>
					<a href="#rangoFechas">Entre rango de fechas</a>
				</li>
			</ul>
			<div id="trimestral" style="height: 150px">
				<div id="radio-btn">
					<div>
						<label style="width: 12%;">Semillera :</label>
						<select id="trim-semillera" name="cultivo">
							<option value="">[ Seleccionar semillera ]</option>
						</select>
					</div>
					<label for="efm" style="width: 8%;margin-right: 5%">Enero Febrero Marzo</label>
                    <input type="radio" name="option" id="efm" value="1">
                    <label for="amj" style="width: 8%;margin-right: 5%">Abril Mayo Junio</label>
                    <input type="radio" name="option" id="amj" value="2" style="width: 15%">
                    <label for="jas" style="width: 15%;margin-right: 5%">
                        <br>
                        Julio
                        <br>
                        Agosto
                        <br>
                        Septiembre</label>
                    <input type="radio" name="option" id="jas" value="3">
                    <label for="ond" style="width: 12%">Octubre Noviembre Diciembre</label>
                    <input type="radio" name="option" id="ond" value="4">
				</div>
				<div style="float: right;">
				    <input type="hidden" id="tipo-trim" value="trim"/>
					<button id="trim-report" class="btn btn-success" style="display: none" type="button">
						<span class="glyphicon glyphicon-cog"></span>
						Generar
					</button>
				</div>
			</div>
			<div id="rangoFechas">
				<div>
					<label style="width: 12%;">Semillera :</label>
					<select id="rango-semillera" name="cultivo">
						<option value="">[ Seleccionar semillera ]</option>
					</select>
					<div style="width: 75%">
                        <div>
                            <label style="width: 20%">Desde : </label>
                            <input type="text" id="f_desde" style="width: 80px" name="desde" value="<?php echo '01-01-'.date('Y');?>"/>
                        </div>
                        <div>
                            <label style="10%">Hasta : </label>
                            <input type="text" id="f_hasta" style="width: 80px" name="hasta"/>
                        </div>
                        <div style="margin-left:77%;">
                            <input type="hidden" id="tipo-rango" value="ran"/>
                            <button class="btn btn-success" type="button" id="btn-rango-report" style="display: none">
                                <span class="glyphicon glyphicon-cog"></span>
                                Generar
                            </button>
                        </div>                    
                    </div>
				</div>
			</div>
		</div>
	</form>
</div>
<?php
    include '../vista/confirmacion/advertencia_resumen_detalle.html';
    include '../vista/confirmacion/pdf_excel_buttons.html';
 ?>

<div class="pcostos">
    
</div>
<?php
include '../vista/error/advertencia.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("select#trim-cultivo,select#rango-report").selectmenu().selectmenu("menuWidget").addClass("overflow-semilla-cultivo");
        $.funciones.cargarListaSemilleras('trim-semillera',1);
        $.funciones.cargarListaSemilleras('rango-semillera',1);
        //inicializacion de calendario
        $.funciones.calendarioInicial('input', 'f_desde');
        $.funciones.calendarioFinal('input', 'f_hasta');
        //opciones
        $("input[name=option]").checkboxradio();
        //creacion de tabs
        $( "#tabs" ).tabs({
          activate: function( event, ui ) {
              //ocultar botones PDF - EXCEL
              $(".resumen").hide();
          }
        });
        $("select#trim-semillera,select#rango-semillera").selectmenu({
            change : function(event, ui) {
            }
        });
        //creacion scroll
        $("select#rango-semillera").selectmenu().selectmenu("menuWidget").addClass("overflow");
        $("select#trim-semillera").selectmenu().selectmenu("menuWidget").addClass("overflow");
        $("select#trim-semillera").on("selectmenuchange", function(event, ui) {
            if ($(this).val() != '') {
                $("button#trim-report").fadeIn();
                
            } else {
                $("button#trim-report").fadeOut();
            }
        });
        $("select#rango-semillera").on("selectmenuchange", function(event, ui) {
            if ($(this).val() != '') {
                $("button#btn-rango-report").fadeIn();
                
            } else {
                $("button#btn-rango-report").fadeOut();
            }
        });
        
        $("button#trim-report").on("click",function(){
            opcion = $('input:radio[name=option]:checked').val();
            if(opcion){
                $(".resumen").fadeIn();
                $("button#xls,button#xls-date").hide();      
            }else{
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            }
            
        });
        $("button#btn-rango-report").on("click",function(){
            desde = $('f_desde').val();
            if(opcion){
                $(".resumen-date").fadeIn();
                $("button#xls,button#xls-date").hide();    
            }else{
                $(".alert>label").empty().append("Fecha Invalida");
                $(".alert").show().fadeOut(3000);
            }
            
        });
        $("button#xls").on("click",function(){ 
            var opcion = $('input:radio[name=option]:checked').val();
            if(opcion){
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var tipo = $("input#tipo-trim").val();
                
               $.get('control/index.php', {
                    mdl : 'informe',
                    opt : 'xls',
                    pag : 'produccion_costos',
                    cultivo : cultivo,
                    area : 1,
                    tipo : tipo,
                    chs : opcion
                }, function(data) {
                    if (data != 'error'){
                        $(".pcostos").empty().append("<iframe class='icostos' style='display:none' src='" + data + "'></iframe>");
                        window.open(data,'_parent' );
                    }else{
                        $("#advertencia").show();
                    }
                }); 
                
            }else{
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            } 
        });
        $("button#pdf").on("click",function(){ 
            var opcion = $('input:radio[name=option]:checked').val();
            if(opcion){
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=acreedor&area=1&chs='+opcion, '_blank');
                win.focus();
            }else{
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            }
        });
        //////////////////////////////////////////////////////////////////
        ///////   rango de fecha para xls y pdf  /////////////////////////
        //////////////////////////////////////////////////////////////////
        $("button#xls-date").on("click",function(){ 
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (desde != '') {
                tipo = $("input#tipo-rango").val();
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var desde
               $.get('control/index.php', {
                    mdl : 'informe',
                    opt : 'xls',
                    pag : 'produccion_costos',
                    cultivo : cultivo,
                    area : 1,
                    tipo : tipo,
                    chs : opcion
                }, function(data) {
                    if (data != 'error'){
                        $(".pcostos").empty().append("<iframe class='icostos' style='display:none' src='" + data + "'></iframe>");
                        window.open(data,'_parent' );
                    }else{
                        $("#advertencia").show();
                    }
                }); 
            }else{
                $(".alert>label").empty().append("Fecha invalida");
                $(".alert").show().fadeOut(3000);
            } 
        });
        $("button#pdf-date").on("click",function(){ 
            desde = $('input#f_desde').val();
            hasta = $('input#f_hasta').val();
            if (desde != '') {
                tipo = $("input#tipo-rango").val();
                var cultivo = $("select#trim-cultivo").find(":selected").val();
                var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=acreedor&area=1&tipo='+tipo+'&desde='+desde+'&hasta'+hasta, '_blank');
                win.focus();
            }else{
                $(".alert>label").empty().append("Debe seleccionar una opcion");
                $(".alert").show().fadeOut(3000);
            }
        });
    })
</script>