<h2 class="title">Cuenta de Semilleristas</h2>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
   <div style="float: right; margin-top: -10%;">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <form  class="seguimiento">
        <?php
            include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <form id="cuenta" class="nuevoform" style="display: none;">
        <div>
            <label>Responsable</label>
            <input id="responsable" name="responsable" type="text"/>
        </div>
        <?php
        include '../vista/certificacion/semilleras_productores_campanha_cultivo.html.php';
        ?>

        <div id="ctaRight" style="width: 49%;float: left;">
            <div>
                <label style="width: 50%">Categoria Aprobada</label>
                <input id="categoriaAprob" class="not-edit" name="categoriaAprob" type="text" readonly="true" style="width: 105px;margin-top: -5%;"/>
            </div>
            <div>
                <label style="width: 50%">Superficie Aprobada</label>
                <input id="superficie" class="number not-edit" name="superficie" type="text" readonly="true" style="width: 30px;margin-top: -5%;" />
                <span> Ha.</span>
            </div>
            <div>
                <label style="width: 50%">Superficie Total</label>
                <input id="superficietot" class="number not-edit" name="superficietot" type="text" readonly="true" style="width: 30px;margin-top: -5%;" />
                <span> Ha.</span>
            </div>
            <div>
                <label style="width: 50%">Etiquetas</label>
                <input id="bolsa" class="number not-edit" name="bolsaEtiqueta" type="text" style="width: 30px;margin-top: -5%;" autocomplete="off"/>
            </div>
            <div style="visibility: hidden">
                <label style="width: 50%">Total</label>
                <input id="total1" class="number not-edit" name="total" type="text" value="0" style="width: 30px;margin-top: -5%;" />
            </div>

        </div>
        <div id="ctaLeft"  style="width: 50%;float: right;">
            <div>
                <label style="width: 54%">Inscripci&oacute;n</label>
                <input id="inscripcion" class="number not-edit" name="inscripcion" type="text" style="width: 30px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div>
                <label style="width: 54%">Inspeccion de Campo</label>
                <input id="icampo" class="number not-edit" name="icampo" type="text"style="width: 30px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div>
                <label style="width: 54%">An&aacute;lisis de Laboratorio y Etiquetaci&oacute;n</label>
                <input id="anlet" class="number not-edit" name="anlet" type="text" style="width: 30px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div style="display:none;">
                <label style="width: 54%;">Acondicionamiento</label>
                <input id="acondicionamiento" class="number not-edit" name="acondicionamiento" type="text" style="width: 30px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div style="display:none;">
                <label style="width: 54%;">Plantines</label>
                <input id="plantines" class="number not-edit" name="plantines" type="text" style="width: 30px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div >
                <label style="width: 49%">Total</label>
                <input id="total2" class="number not-edit" name="total2" type="text" style="width: 47px; margin-left:-5%; margin-top: -1em;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
        </div>
        <div id="ctaFinal" style="margin-top: 30%;">
            <div>
                <label>Fecha</label>
                <input id="f_pago" class="date not-edit" name="f_pago" type="text" readonly="readonly" style="width: 65px;margin-top: -5%;" value="<?php echo date('d-m-Y')?>"/>
            </div>
            <div>
                <label>Monto pagado</label>
                <input id="montoPagado"  class="number" name="montoPagado" type="text" maxlength="5" style="width: 30px" autocomplete="off"/>
                <span> Bs.</span>
            </div>
            <div>
                <label style="width:18%">Saldo</label>
                <input id="saldototal" class="number not-edit" name="saldototal" type="text" readonly="readonly" style="width: 46px;margin-top: -5%;" />
                <span> Bs.</span>
            </div>
            <div style="visibility: hidden">
                <label style="width: 30%">Saldo Campa&ntilde;a Anterior</label>
                <input id="campAnterior" class="number not-edit" name="campAnterior" type="text"  readonly="readonly" style="width: 30px;margin-top:-3%" />
                <span class='loading'><img src='images/loader_6.gif'/></span>
                <span> Bs.</span>
            </div>
        </div>
        <div>
            <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
            <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="cuenta"/>
            <input id="iSolicitud" name="iSolicitud" type="hidden"/>
            <input id="iEstado" name="iEstado" type="hidden"/>
            <input id="iSemilla" name="iSemilla" type="hidden"/>
            <input id="iSuperficie" name="iSuperficie" type="hidden"/>
            <input id="cuota-descripcion" name="cuota-descripcion" type="hidden"/>
            <input id="cuota-monto" name="cuota-monto" type="hidden"/>
            <input id="cuota-fecha" name="cuota-fecha" type="hidden"/>
        </div>
        <div id="button-send" style="position: absolute; padding-left: 35%; margin-top: -3%;">
            <button id="enviar-cuenta" class="btn btn-success" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/dialogos/cuotaCuenta.html';
    include '../vista/dialogos/precioCertificacion.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //apariencia de cuadro de busqueda
        $("form.seguimiento>div#anteriores>div>label").css("width","15%");
        $("span#advice").css('margin-left','16%').empty().text("Escribir semillera, semillerista, cultivo o variedad");
        $("input#buscarTXT").css( "width", "69%" );
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //permitir solo letras
        $.funciones.soloLetras("buscarTXT");
        /////////////////////autocompletado  de busqueda unificada
        $.buscar.autocompletarCuenta();
        $("input#buscarTXT").on("keypress", function(event) {
            $("div#tipo-cuota").buttonset();
            if (event.which == 13) {
                $.buscar.cuentaCertifica();
            }
        });
        $("button#btn-search").on("click", function() {
            $("div#tipo-cuota").buttonset();
            $.buscar.cuentaCertifica();
        });
        
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //botn volver
        $("button#return").on("click",function(){
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cuentas',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        
        //estilo formulario
        $(".nuevoform").css('padding', '3% 2% 2%')
        $("#f_pago").calendarioSolicitud('f_pago');
        $("select").not("#semillera").attr('disabled', 'disabled');
        $("input[name=send]").fadeTo('fast', 0.5);

        //calculo de saldo
        $('input#montoPagado').on("click",function(){
            var semillera = $("input#semillera").val();
            var semillerista = $("input#semillerista").val();
            var cultivo = $("input#cultivo").val();
            var variedad = $("input#variedad").val();
            var total = $("input#saldototal").val();
            var isemilla = $("input#iSemilla").val();
            $("div#tipo-cuota-fiscal").hide();            
            $("div#tipo-cuota").buttonset();
            $("div#tipo-cuota").show();
            $('#lst-cuota label.slr>span').empty().append(semillera);
            $('#lst-cuota label.smr>span').empty().append($.funciones.cambiarSignoPorAcento(semillerista) );
            $('#lst-cuota label.clt>span').empty().append($.funciones.cambiarSignoPorAcento(cultivo) );
            $('#lst-cuota label.var>span').empty().append($.funciones.cambiarSignoPorAcento(variedad) );
            $('#lst-cuota label.sld>span.total').empty().append(total);
            
            //$.funciones.buscarSaldo(semillera,semillerista,cultivo,variedad);
            $.funciones.buscarCuotasCuenta(isemilla);                                              
        });        
    })
</script>
<script type="text/javascript" >
    $(document).ready(function() {
        $("button#enviar-cuenta").addClass("button-primary");
        $("button#enviar-cuenta").on("click",function(){
            $.funciones.enviarCuentaCertifica();
        });
    }); 
</script>