<div id="opciones-semilla" style="width:101%;">
	<input type="radio" id="semilla-cultivo" name="semilla" value="1"  checked="checked" />
	<label for="semilla-cultivo" style="width: 23%">Cultivo</label>
	<input type="radio" id="semilla-acondicionamiento" name="semilla" value="2"/>
	<label for="semilla-acondicionamiento" style="width: 40%;">Acondicionamiento</label>
	<input type="radio" id="semilla-plantines" name="semilla" value="3"/>
	<label for="semilla-plantines" style="width: 23%;">Plantines</label>
</div>
<div id="tabs">
	<ul>
		<li id="li-cultivo">
			<a href="#tabs-1">Cultivo</a>
		</li>
		<li id="li-acondicionamiento">
		  <a href="#tabs-2">Acondicionamiento</a>
		</li>
		<li id="li-plantines">
		  <a href="#tabs-3">Plantines</a>
		</li>
	</ul>
	<div id="tabs-1">
		<form id="semilla-certifica">
		    <div>
                <label style="width: 34%">Campa&ntilde;a</label>
                <span id="campania">Verano/17</span>
                <input id="hiddencampania" name="campania" type="hidden"/>
            </div>
            <div>
                <label style="width: 34%">Fecha de Siembra</label>
                <input id="f_siembra2" value="<?php echo date('d-m-Y')?>" style="width:70px"/>
                <input id="f_siembra" name="f_siembra" type="hidden" value="<?php echo date('d-m-Y')?>"/>
            </div>
            <div>
                <label id="lbl-cultivo" style="width: 33%">Cultivo</label>
                <select size="1" name="cultivo" id="cultivo" class="special" style="width: 177px">
                  <option value="">[ Seleccione cultivo ]</option>
                </select>
            </div>
            <div>
                <label id="lbl-variedad" style="width: 33%">Variedad</label>
                <select size="1" name="variedad" id="variedad" class="special" style="width: 182px;text-transform: capitalize">
                  <option value="">[ Seleccione variedad ]</option>
                </select>
            </div>
            <div>
                <label id="lbl-cat-sembrada" style="width:33%">Categor&iacute;a Sembrada</label>
                <select size="1" name="cat_sembrada" id="cat_sembrada" class="special" style="width: 187px">
                  <option value="">[ Seleccione categoria ]</option>
                </select>
            </div>
            <div>
                <label id="lbl-cat-producir" style="width: 33%">Categor&iacute;a a Producir</label>
                <select size="1" name="cat_producir" id="cat_producir" class="special" style="width: 187px;" disabled="disabled">
                  <option value="">[ Seleccione categoria ]</option>
                </select>
            </div>
            <div>
                <label id="lbl-cult-anterior" style="width: 33%">Cultivo Anterior</label>
                <select size="1" name="cult_anterior" id="cult_anterior" class="special" style="width: 150px">
                  <option value="">[ Seleccionar ]</option>
                </select>
            </div>
            <div>
                <label style="width: 34%">Cantidad de semilla empleada (kg)</label>
                <input id="cant_semilla" name="cant_semilla" type="text" class="special number" style="width: 60px"/>
                <span id="msg-cant_semilla" style="font-size: 9px; font-style: italic; letter-spacing: 0.1em; width: 45%;margin-left: 50%;visibility: hidden;"> Debe ingresar la Cantidad de semilla empleada </span>
            </div>
            
            <div>
                <label style="display:none;width: 21%">Lote Nro</label>
                <input id="lotenro" name="lotenro" type="text" class="special" style="display:none;text-transform: uppercase" maxlength="11"/>
            </div>      
            <div>
                <label style="width: 34%">Plantas por Hect&aacute;reas</label>
                <input id="plantasha" name="plantasha" type="text" class="special number" style="width: 65px" maxlength="9"/>
            </div>
            
            <div>
                <label style="width: 34%">Superficie Parcela (Has.)</label>
                <input id="superficie" name="superficie" type="text" class="special number"style="width: 30px"  maxlength="5"/>
            </div>
            <div>
                <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
                <input id="idSolicitud" name="solicitud" type="hidden"/>
                <input id="hiddenCultivo" name="hiddenCultivo" type="hidden"/>
                <input id="hiddenVariedad" name="hiddenVariedad" type="hidden"/>
                <input id="hiddenSupParcela" name="supParcela" type="hidden"/>
                <input id="hiddenSemEmpleada" name="semEmpleada" type="hidden"/>
                <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
                <input id="opt" name="opt" type="hidden" value="guardar"/>
                <input id="pag" name="pag" type="hidden" value="semilla"/>
                <button id="enviar-semilla" class="btn btn-success button-primary" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
		</form>
    </div>
<div id="tabs-2" style="display: none;">
<form id="acondicionamiento">
    	<div>
            <label style="width: 37%">Numero Campo </label>
            <input id="nocampo" name="nocampo" type="text" class="special number"style="width: 30px"  maxlength="5"/>
        </div>
        <div>
            <label style="width: 37%">Fecha Emision </label>
            <input id="emision" name="emision" type="text" class="special number"style="width: 70px"  maxlength="10"/>
            <span>[ dia-mes-a&ntilde;o ]</span>
        </div>
        <div>
                <label id="lbl-cultivo" style="width: 33%">Cultivo</label>
                <select size="1" name="cultivo" id="cultivo" class="special" style="width: 177px">
                  <option value="">[ Seleccione cultivo ]</option>
                </select>
            </div>
            <div>
                <label id="lbl-variedad" style="width: 33%">Variedad</label>
                <select size="1" name="variedad" id="variedad" class="special" style="width: 182px;text-transform: capitalize">
                  <option value="">[ Seleccione variedad ]</option>
                </select>
            </div>
        <div>
            <label id="lbl-categoria-acondicionamiento" style="width: 37%">Categoria Campo</label>
            <select id="categoria_acondicionamiento" name="categoria_acondicionamiento">
                <option value="">[ Seleccione categoria ]</option>
            </select>
        </div>
        <div>
            <label style="width: 37%">Rendimiento Estimado (TM/Ha.)</label>
            <input id="rendimientoE" name="rendimientoE" type="text" class="special number"style="width: 40px"  maxlength="5"/>
        </div>
        <div>
            <label style="width: 37%">Superficie (Ha.)</label>
            <input id="superficie-acondicionamiento" name="superficie-acondicionamiento" type="text" class="special number"style="width: 40px"  maxlength="5"/>
        </div>
        <div>
            <label style="width: 37%">Rendimiento Campo (TM.)</label>
            <input id="rendimientocampo" name="rendimientocampo" type="text" class="special number" style="width: 40px"  maxlength="5"/>
        </div>
        <div>
            <label style="width: 37%">Cupones </label>
            <input id="cuponesacondicion" name="cuponesacondicion" type="text" autocomplete="off" style="width: 40px"/>
        </div>
        <div>
                <label>Rango Cupones</label>
                <input id="rc-from" class="number" name="rc-from" style="width:53px" placeholder="desde" maxlength="5" autocomplete="off"/>
                <span>&nbsp;-&nbsp;</span>
                <input id="rc-to" class="not-edit" name="rc-to" style="width:53px" placeholder="hasta"readonly="readonly"/>
            </div>
        <div>
            <label style="width: 37%">Planta Acondicionadora </label>
            <input id="plantaacondicionadora" name="plantaacondicionadora" type="text" autocomplete="off"/>
        </div>
        <div>
            <label style="width: 37%">Peso bruto de Semilla (TM.)</label>
            <input id="pesobruto" name="pesobruto" type="text" class="special number"style="width: 70px"  maxlength="10"/>
        </div>
        <div>
            <input id="idSolicitud" name="solicitud" type="hidden"/>
            <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="acondicionamiento"/>
            <button id="enviar-acondicionamiento" class="btn btn-success button-primary" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>
	</form>
</div>
<div id="tabs-3" style="display: none;">
	<form id="plantines">
	    
	    <div>
            <label id="lbl-cultivo-plantines" style="width: 33%;margin-right: 5%;text-align: right;">Cultivo</label>            
            <select size="1" name="cultivo-plantines" id="cultivo-plantines" class="special" style="width: 182px;text-transform: capitalize">
              <option value="">[ Seleccione variedad ]</option>
            </select>
        </div>
        <div>
            <label id="lbl-variedad-plantines" style="width: 33%;margin-right: 5%;text-align: right;">Variedad</label>
            <select size="1" name="variedad-plantines" id="variedad-plantines" class="special" style="width: 182px;text-transform: capitalize;" disabled="disabled">
              <option value="">[ Seleccione variedad ]</option>
            </select>
        </div>
        <div>
            <input id="idSolicitud" name="solicitud" type="hidden"/>
            <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="plantines"/>
            <button id="enviar-plantines" class="btn btn-success button-primary" type="button">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Registrar
            </button>
        </div>
	</form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        solo_numeros = ['nocampo','superficie','rendimientoE','rendimientocampo','pesobruto','rc-from','cuponesacondicion'];
        $.funciones.soloNumeros(solo_numeros);
        digitos_fecha = ['inspeccion-plantines'];
        $.funciones.soloNumerosFecha(digitos_fecha);
        $("#tabs").tabs();
        $("#tabs").tabs("option", "disabled", [1, 2]);
        $('#li-plantines,#tabs-3,#li-acondicionamiento,#tabs-2').hide();
        //opcion laboratorio
        $("#opciones-semilla").buttonset();
        //calendario
        var hoy = new Date();
        var dia = hoy.getDate();
        var mes = hoy.getMonth() + 1;
        var anho = hoy.getFullYear();
        $('input#emision').calendarioInspeccion('emision', anho, mes-5, dia, anho, mes, dia);
        //calendario cultivo
        $('input#f_siembra2').calendarioInspeccion('f_siembra2', anho, mes-5, dia, anho, mes, dia);
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //calculo de cupones
        $('input#rc-from').on('keyup',function(){
          cupones = $('input#cuponesacondicion').val();
          valor = $('input#rc-from').val();
          rango_hasta = parseInt(valor)+parseInt(cupones);
          $('input#rc-to').val(rango_hasta);
        });
        
        $('div#opciones-semilla>input[type=radio]').on('click', function() {
            opcion = $('div#opciones-semilla>input[type=radio]:checked').val();
            switch (opcion) {
            case '1':
                $('#tabs').tabs('option', 'disabled', [1, 2]);
                $('#tabs').tabs('enable', 0);
                $('#tabs').tabs('option', 'active', 0);
                $('#li-plantines,#tabs-3,#li-acondicionamiento,#tabs-2').hide();
                $('#li-cultivo,#tabs-1').show();
                break;
            case '2':
                $('#tabs').tabs('option', 'disabled', [0, 2]);
                $('#tabs').tabs('enable', 1);
                $('#tabs').tabs('option', 'active', 1);
                $('#li-cultivo,#tabs-1,#li-plantines,#tabs-3').hide();
                $('#li-acondicionamiento,#tabs-2').show();
                break;
            case '3':
                $('#tabs').tabs('option', 'disabled', [0, 1]);
                $('#tabs').tabs('enable', 2);
                $('#tabs').tabs('option', 'active', 0);
                $('#li-cultivo,#tabs-1,#li-acondicionamiento,#tabs-2').hide();
                $('#li-plantines,#tabs-3').show();
                break;
            }
        });
    }); 
</script>
