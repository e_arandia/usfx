<div id="anteriores">
	<div class="resumen-cta-left" style="width: 49%;float:left;">
		<div>
			<label style="width:25%">Semillera</label>
			<input id="semillera" class="not-edit" name="semillera" style="text-transform:uppercase;width:200px;margin-top: -5%;"/>
		</div>
		<div class="semilleristas">
			<label style="width: 25%;">Semillerista </label>
			<input id="semillerista" class="not-edit" name="productor" style="width:200px;margin-top: -5%;"/>
		</div>
		<div>
			<label style="width:25%">Campa&ntilde;a </label>
			<input id="campanha" class="not-edit" name="campanha" style="width:200px;margin-top: -5%;"/>
		</div>
	</div>
	<div class="resumen-cta-right" style="width: 50%;float:left;">
		<div>
			<label style="width:25%">Comunidad</label>
			<input id="comunidad" class="not-edit" style="width:200px;margin-top: -5%;"/>
		</div>
		<div>
			<label style="width:25%">Cultivo </label>
			<input id="cultivo" class="not-edit" name="cultivo" style="width:200px;margin-top: -5%;"/>
		</div>
		<div>
			<label style="width:25%">Variedad </label>
			<input id="variedad" class="not-edit" name="variedad" style="width:200px;margin-top: -5%;"/>
		</div>
	</div>
	<div id="resumen-cta-center" style="float: right;">
	    <button id="btn-precios" class="btn btn-success" type="button">
	    <span class="glyphicon glyphicon-eye-open"> </span>
              Mostrar Precios
        </button>
	</div>
</div>
<?php
    #semillera  mostrar cuadro de dialogo 
    include '../vista/dialogos/seleccionarSemilleraCampanhaCultivoVariedad.html';
    #semillerista  mostrar cuadro de dialogo 
    include '../vista/dialogos/seleccionarSemilleristaCampanhaCultivoVariedad.html';
    #campanha  mostrar cuadro de dialogo 
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCultivoVariedad.html';
    #cultivo  mostrar cuadro de dialogo 
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCampanhaVariedad.html';
    #variedad mostrar cuadro de dialogo 
    include '../vista/dialogos/seleccionarSemilleraSemilleristaCampanhaCultivo.html';
?> 

<script type="text/javascript">
    $(document).ready(function() {
        //////////////////////////////////////////////////////////////////////////////////
        ///////////         llenado de opciones segun  texto ingresado         ///////////
        //////////////////////////////////////////////////////////////////////////////////
        $("button#btn-search").on("click", function(event) {
            $(".table").tablesorter();
        });
        $('#btn-precios').on('click', function() {
            $.funciones.precioCertificacion();
        });

        ////////////////////////////////////////////////SEMILLERISTA
        $("input#semillerista").on("keyup", function(event) {
            semillerista = $(this).val();
            if (semillerista.length > 0) {
                if (event.which == 13) {
                    $.getJSON("control/index.php", {
                        mdl : 'certificacion',
                        opt : 'ver',
                        pag : 'semilleras_cuenta',
                        opc : 'semillerista',
                        smr : semillerista
                    }, function(json) {
                        if (json.total > 1) {
                            var html = '';
                            $.each(json.campanha, function(index, semillera) {
                                html += "<tr>";
                                html += "<th style=\"width: 15%;text-align:center\" scope='row'>";
                                html += "<input type='radio' value='" + json.semcamculvar[index] + "' class='" + index + "' name='nro'/>";
                                html += "</th> ";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.comunidad[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillera[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.campanha[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.cultivo[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.variedad[index] + "</td>";
                                html += "</tr>";
                            });

                            //agregar solicitudes a dialogo
                            $(".tbl-cuenta-smr>.table>tbody.buscar").empty().append(html);
                            //agregar semillera y semillerista
                            $('#lst-cuenta-smr label.smr>span').empty().append(semillerista);
                            //mostrar cuadro de dialogo
                            $('div#lst-cuenta-smr').dialog({// Dialog
                                title : 'Lista de semilleras',
                                dialogClass : "no-close",
                                resizable : false,
                                height : 400,
                                width : 520,
                                buttons : {
                                    "Aceptar" : function() {
                                        var nro = $("input#nosolicitud").val();
                                        var semillerista = $("input#semillerista").val();
                                        var semillera = $("input#semillera").val();
                                        $("input#semillera,input#semillerista").addClass('not-edit');
                                        $("input#campanha,input#cultivo,input#variedad").addClass('not-edit');
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $("#button-send").fadeIn();
                                        $(this).dialog("destroy");
                                    },
                                    "Cancelar" : function() {
                                        $("form#form-buscador input").empty().val('').removeClass("not-edit");
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                            //copiar datos seleccionados
                            $('input:radio[name=nro]').on('click', function() {
                                valor = $('input:radio[name=nro]:checked').val();
                                temp = valor.split('=');
                                $("input#semillerista").empty().val(semillerista);
                                $("input#semillera").empty().val(temp[0]);
                                $("input#campanha").empty().val(temp[1]);
                                $("input#cultivo").empty().val(temp[2]);
                                $("input#variedad").empty().val(temp[3]);
                                $("input#iEstado").empty().val(temp[4]);
                                $("input#iSolicitud").empty().val(temp[5]);
                                $("input#iSemilla").empty().val(temp[6]);
                                $("input#iSuperficie").empty().val(temp[7]);
                                cargarISuperficie2(temp[5], temp[6]);
                            });
                        } else {
                            $("input#semillerista").val(semillerista).addClass('not-edit');
                            $("input#semillera").empty().val(json.semillera).addClass('not-edit');
                            $("input#cultivo").empty().val(json.cultivo).addClass('not-edit');
                            $("input#variedad").empty().val(json.variedad).addClass('not-edit');
                            $("input#campanha").empty().val(json.campanha).addClass('not-edit');
                            $("input#iEstado").empty().val(json.estado);
                            $("input#iSolicitud").empty().val(json.isolicitud);
                            $("input#iSemilla").empty().val(json.isemilla);
                            $("input#iSuperficie").empty().val(json.isuperficie);
                            cargarISuperficie2(json.isolicitud, json.isemilla);
                        }
                    });
                } //fin al presionar enter
            } //fin longitud nombre de semillera
        }).on("mouseenter", function() {
            $("#help-semillera").fadeIn();
        }).on("click", function() {
            $("#help-semillera").fadeOut();
        }).on("blur", function() {
            $("#help-semillera").fadeOut();
        }).on("mouseleave", function() {
            $("#help-semillera").fadeOut();
        });

        function cargarISuperficie2(isolicitud, isemilla) {
            var isol = parseInt(isolicitud);
            var isem = parseInt(isemilla);
            alert(isem+'='+isemilla);
            //cargar id superficie
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'isuperficie',
                id : isem
            }, function(json) {
                if (json.isuperficie > 0) {
                    $("input#iSuperficie").empty().val(json.isuperficie);
                } else {
                    $("input#iSuperficie").empty().val(0);
                }
            });
            
            //cargar saldo campanha anterior
            $.ajax({
                url : 'control/index.php',
                type : 'POST',
                async : true,
                dataType : 'json',
                data : {
                    mdl : 'certificacion',
                    opt : 'saldo_anterior',
                    isol : isol,
                    isem : isem
                },
                success : function(data) {
                    $("#campAnterior").val(data.saldo);
                }
            });

            //cargar datos de la cuenta
            var isup = $("input#iSuperficie").val();
            var std = $("input#iEstado").val();
            alert("SEMILLERISTA \n" + isol + '=' + isem + '=' + isup + '=' + std);
            // alert (isol+'='+isem+'='+isup+'='+std+'='+campanha);
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'resumenCta',
                iSolicitud : isol,
                iSemilla : isem,
                iSuperficie : isup,
                iEstado : std
            }, function(json) {
                $("input#categoriaAprob").val(json.catAprobada).text(json.catAprobada);
                $("input#superficie").val(json.supAprobada).text(json.supAprobada);
                $("input#superficietot").val(json.supTotal).text(json.supTotal);
                $("input#inscripcion").val(json.inscripcion).text(json.inscripcion);
                $("input#icampo").val(json.inspeccion).text(json.inspeccion);
                $("input#anlet").val(json.analisisEtiqueta).text(json.analisisEtiqueta);
                $("input#acondicionamiento").val(json.acondicionamiento).text(json.acondicionamiento);
                $("input#plantines").val(json.plantines).text(json.plantines);
                $("input#total2,input#saldototal").val(json.total).text(json.total);
                $("input#bolsa").val(json.etiquetas).text(json.etiquetas);
            });
        }

        ///////////////////////////////////////////////////////CAMPANHA
        $("input#campanha").on("keyup", function(event) {
            campanha = $(this).val();
            if (campanha.length > 0) {
                if (event.which == 13) {
                    $.getJSON("control/index.php", {
                        mdl : 'certificacion',
                        opt : 'ver',
                        pag : 'semilleras_cuenta',
                        opc : 'campanha',
                        cmp : campanha
                    }, function(json) {
                        if (json.total >= 2) {
                            var html = '';
                            $.each(json.semillera, function(index, semillera) {
                                html += "<tr>";
                                html += "<th style=\"width: 15%;text-align:center\" scope='row'>";
                                html += "<input type='radio' value='" + json.slrsmrculvar[index] + "' class='" + index + "' name='nro'/>";
                                html += "</th> ";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillera[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillerista[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.cultivo[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.variedad[index] + "</td>";
                                html += "</tr>";
                            });

                            //agregar solicitudes a dialogo
                            $(".tbl-cuenta-cmp>.table>tbody.buscar").empty().append(html);
                            //agregar semillera y semillerista
                            $('#lst-cuenta-cmp label.cam>span').empty().append(campanha);
                            //mostrar cuadro de dialogo
                            $('div#lst-cuenta-cmp').dialog({// Dialog
                                title : 'Lista de semilleristas de la campaña',
                                dialogClass : "no-close",
                                resizable : false,
                                height : 400,
                                width : 520,
                                buttons : {
                                    "Aceptar" : function() {
                                        var nro = $("input#nosolicitud").val();
                                        var semillerista = $("input#semillerista").val();
                                        var semillera = $("input#semillera").val();
                                        $("input#semillera,input#semillerista").addClass('not-edit');
                                        $("input#campanha,input#cultivo,input#variedad").addClass('not-edit');
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $("#button-send").fadeIn();
                                        $(this).dialog("destroy");
                                    },
                                    "Cancelar" : function() {
                                        $("form#form-buscador input").empty().val('').removeClass("not-edit");
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                            //copiar datos seleccionados
                            $('input:radio[name=nro]').on('click', function() {
                                valor = $('input:radio[name=nro]:checked').val();
                                temp = valor.split('=');
                                $("input#semillerista").empty().val(temp[1]);
                                $("input#semillera").empty().val(temp[0]);
                                $("input#campanha").empty().val(campanha);
                                $("input#cultivo").empty().val(temp[2]);
                                $("input#variedad").empty().val(temp[3]);
                                $("input#iEstado").empty().val(temp[4]);
                                $("input#iSolicitud").empty().val(temp[5]);
                                $("input#iSemilla").empty().val(temp[6]);
                                $("input#iSuperficie").empty().val(temp[7]);
                                cargarISuperficie3(temp[6], temp[0], temp[1], temp[2], temp[3], campanha);
                            });
                        } else {
                            $("input#semillerista").val(json.semillerista).addClass('not-edit');
                            $("input#semillera").empty().val(json.semillera).addClass('not-edit');
                            $("input#cultivo").empty().val(json.cultivo).addClass('not-edit');
                            $("input#variedad").empty().val(json.cultivo).addClass('not-edit');
                            $("input#campanha").empty().val(campanha).addClass('not-edit');
                            $("input#iEstado").empty().val(json.estado);
                            $("input#iSolicitud").empty().val(json.isolicitud);
                            $("input#iSemilla").empty().val(json.isemilla);
                            $("input#iSuperficie").empty().val(json.isuperficie);
                            cargarISuperficie3(json.isemilla, json.semillera, json.semillerista, json.cultivo, json.variedad, campanha);

                        }
                    });
                } //fin al presionar enter
            } //fin longitud nombre de semillera
        }).on("mouseenter", function() {
            $("#help-semillera").fadeIn();
        }).on("click", function() {
            $("#help-semillera").fadeOut();
        }).on("blur", function() {
            $("#help-semillera").fadeOut();
        }).on("mouseleave", function() {
            $("#help-semillera").fadeOut();
        });

        function cargarISuperficie3(isolicitud, isemilla) {
            //cargar id superficie
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'isuperficie',
                id : isemilla
            }, function(json) {
                if (json.isuperficie > 0) {
                    $("input#iSuperficie").empty().val(json.isuperficie);
                } else {
                    $("input#iSuperficie").empty().val(0);
                }
            });
            var isol = parseInt(isolicitud);
            var isem = parseInt(isemilla);
            //cargar saldo campanha anterior
            $.ajax({
                url : 'control/index.php',
                type : 'POST',
                async : true,
                dataType : 'json',
                data : {
                    mdl : 'certificacion',
                    opt : 'saldo_anterior',
                    isol : isol,
                    isem : isem
                },
                success : function(data) {
                    $("#campAnterior").val(data.saldo);
                }
            });

            //cargar datos de la cuenta
            var isol = $("input#iSolicitud").val();
            var isem = $("input#iSemilla").val();
            var isup = $("input#iSuperficie").val();
            var std = $("input#iEstado").val();

            alert("CAMPANHA \n" + isol + '=' + isem + '=' + isup + '=' + std + '=' + campanha);

            $.getJSON("control/index.php", {
                mdl : 'certificacionN',
                opt : 'buscar',
                pag : 'resumenCta',
                iSolicitud : isol,
                iSemilla : isem,
                iSuperficie : isup,
                iEstado : std
            }, function(json) {
                $("input#categoriaAprob").val(json.catAprobada).text(json.catAprobada);
                $("input#superficie").val(json.supAprobada).text(json.supAprobada);
                $("input#superficietot").val(json.supTotal).text(json.supTotal);
                $("input#inscripcion").val(json.inscripcion).text(json.inscripcion);
                $("input#icampo").val(json.inspeccion).text(json.inspeccion);
                $("input#anlet").val(json.analisisEtiqueta).text(json.analisisEtiqueta);
                $("input#acondicionamiento").val(json.acondicionamiento).text(json.acondicionamiento);
                $("input#plantines").val(json.plantines).text(json.plantines);
                $("input#total2,input#saldototal").val(json.total).text(json.total);
                $("input#bolsa").val(json.etiquetas).text(json.etiquetas);
            });
        }

        ////////////////////////////////////////////////////////CULTIVO
        $("input#cultivo").on("keyup", function(event) {
            cultivo = $(this).val();
            if (cultivo.length > 0) {
                if (event.which == 13) {
                    $.getJSON("control/index.php", {
                        mdl : 'certificacion',
                        opt : 'ver',
                        pag : 'semilleras_cuenta',
                        opc : 'cultivo',
                        cul : cultivo
                    }, function(json) {
                        if (json.total >= 2) {
                            var html = '';
                            $.each(json.semillera, function(index, semillera) {
                                html += "<tr>";
                                html += "<th style=\"width: 15%;text-align:center\" scope='row'>";
                                html += "<input type='radio' value='" + json.slrsmrcamvar[index] + "' class='" + index + "' name='nro'/>";
                                html += "</th> ";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillera[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillerista[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.campanha[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.variedad[index] + "</td>";
                                html += "</tr>";
                            });

                            //agregar solicitudes a dialogo
                            $(".tbl-cuenta-cul>.table>tbody.buscar").empty().append(html);
                            //agregar semillera y semillerista
                            $('#lst-cuenta-cul label.cltv>span').empty().append(cultivo);
                            //mostrar cuadro de dialogo
                            $('div#lst-cuenta-cul').dialog({// Dialog
                                title : 'Lista de semilleras y semilleristas',
                                dialogClass : "no-close",
                                resizable : false,
                                height : 400,
                                width : 520,
                                buttons : {
                                    "Aceptar" : function() {
                                        var nro = $("input#nosolicitud").val();
                                        var semillerista = $("input#semillerista").val();
                                        var semillera = $("input#semillera").val();
                                        $("input#semillera,input#semillerista").addClass('not-edit');
                                        $("input#campanha,input#cultivo,input#variedad").addClass('not-edit');
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $("#button-send").fadeIn();
                                        $(this).dialog("destroy");
                                    },
                                    "Cancelar" : function() {
                                        $("form#form-buscador input").empty().val('').removeClass("not-edit");
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                            //copiar datos seleccionados
                            $('input:radio[name=nro]').on('click', function() {
                                valor = $('input:radio[name=nro]:checked').val();
                                temp = valor.split('=');
                                $("input#semillera").empty().val(temp[0]);
                                $("input#semillerista").empty().val(temp[1]);
                                $("input#campanha").empty().val(temp[2]);
                                $("input#variedad").empty().val(temp[3]);
                                $("input#iEstado").empty().val(temp[4]);
                                $("input#iSolicitud").empty().val(temp[5]);
                                $("input#iSemilla").empty().val(temp[6]);
                                $("input#iSuperficie").empty().val(temp[7]);
                                cargarISuperficie4(temp[6], temp[0], temp[1], cultivo, temp[3], temp[2]);
                            });
                        } else {
                            $("input#semillerista").val(json.semillerista).addClass('not-edit');
                            $("input#semillera").empty().val(json.semillera).addClass('not-edit');
                            $("input#cultivo").empty().val(json.campanha).addClass('not-edit');
                            $("input#variedad").empty().val(json.variedad).addClass('not-edit');
                            $("input#campanha").empty().val(json.campanha).addClass('not-edit');
                            $("input#iEstado").empty().val(json.estado);
                            $("input#iSolicitud").empty().val(json.isolicitud);
                            $("input#iSemilla").empty().val(json.isemilla);
                            $("input#iSuperficie").empty().val(json.isuperficie);
                            cargarISuperficie4(json.isemilla, json.semillera, json.semillerista, cultivo, json.variedad, json.campanha);
                        }
                    });
                } //fin al presionar enter
            } //fin longitud nombre de semillera
        }).on("mouseenter", function() {
            $("#help-semillera").fadeIn();
        }).on("click", function() {
            $("#help-semillera").fadeOut();
        }).on("blur", function() {
            $("#help-semillera").fadeOut();
        }).on("mouseleave", function() {
            $("#help-semillera").fadeOut();
        });

        function cargarISuperficie4(isolicitud, isemilla) {
            var isem = parseInt(isemilla);
            //cargar saldo campanha anterior
            $.ajax({
                url : 'control/index.php',
                type : 'POST',
                async : true,
                dataType : 'json',
                data : {
                    mdl : 'certificacion',
                    opt : 'saldo_anterior',
                    isol : isol,
                    isem : isem
                },
                success : function(data) {
                    $("#campAnterior").val(data.saldo);
                }
            });

            //cargar datos de la cuenta
            var isup = $("input#iSuperficie").val();
            var std = $("input#iEstado").val();

            alert(isol + '=' + isem + '=' + isup + '=' + std + '=' + campanha);

            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'resumenCta',
                iSolicitud : isol,
                iSemilla : isem,
                iSuperficie : isup,
                iEstado : std
            }, function(json) {
                $("input#categoriaAprob").val(json.catAprobada).text(json.catAprobada);
                $("input#superficie").val(json.supAprobada).text(json.supAprobada);
                $("input#superficietot").val(json.supTotal).text(json.supTotal);
                $("input#inscripcion").val(json.inscripcion).text(json.inscripcion);
                $("input#icampo").val(json.inspeccion).text(json.inspeccion);
                $("input#anlet").val(json.analisisEtiqueta).text(json.analisisEtiqueta);
                $("input#acondicionamiento").val(json.acondicionamiento).text(json.acondicionamiento);
                $("input#plantines").val(json.plantines).text(json.plantines);
                $("input#total2,input#saldototal").val(json.total).text(json.total);
                $("input#bolsa").val(json.etiquetas).text(json.etiquetas);
            });
        }

        //////////////////////////////////////VARIEDAD
        $("input#variedad").on("keyup", function(event) {
            variedad = $(this).val();
            if (variedad.length > 0) {
                if (event.which == 13) {
                    $.getJSON("control/index.php", {
                        mdl : 'certificacion',
                        opt : 'ver',
                        pag : 'semilleras_cuenta',
                        opc : 'variedad',
                        vrd : variedad
                    }, function(json) {
                        if (json.total >= 2) {
                            var html = '';
                            $.each(json.semillera, function(index, semillera) {
                                html += "<tr>";
                                html += "<th style=\"width: 15%;text-align:center\" scope='row'>";
                                html += "<input type='radio' value='" + json.slrsmrcamcul[index] + "' class='" + index + "' name='nro'/>";
                                html += "</th> ";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillera[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.semillerista[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.campanha[index] + "</td>";
                                html += "<td class='" + index + "' style=\'text-align:center\'>" + json.cultivo[index] + "</td>";
                                html += "</tr>";
                            });

                            //agregar solicitudes a dialogo
                            $(".tbl-cuenta-vrd>.table>tbody.buscar").empty().append(html);
                            //agregar semillera y semillerista
                            $('#lst-cuenta-vrd label.vrdd>span').empty().append(variedad);
                            //mostrar cuadro de dialogo
                            $('div#lst-cuenta-vrd').dialog({// Dialog
                                title : 'Lista de cultivos, semilleras y semilleristas',
                                dialogClass : "no-close",
                                resizable : false,
                                height : 400,
                                width : 520,
                                buttons : {
                                    "Aceptar" : function() {
                                        var nro = $("input#nosolicitud").val();
                                        var semillerista = $("input#semillerista").val();
                                        var semillera = $("input#semillera").val();
                                        $("input#semillera,input#semillerista").addClass('not-edit');
                                        $("input#campanha,input#cultivo,input#variedad").addClass('not-edit');
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $("#button-send").fadeIn();
                                        $(this).dialog("destroy");
                                    },
                                    "Cancelar" : function() {
                                        $("form#form-buscador input").empty().val('').removeClass("not-edit");
                                        $("#ctaRight,#ctaLeft,#ctaFinal").fadeIn();
                                        $(this).dialog("close");
                                    }
                                }
                            });
                            //copiar datos seleccionados
                            $('input:radio[name=nro]').on('click', function() {
                                valor = $('input:radio[name=nro]:checked').val();
                                temp = valor.split('=');
                                $("input#semillera").empty().val(temp[0]);
                                $("input#semillerista").empty().val(temp[1]);
                                $("input#campanha").empty().val(temp[2]);
                                $("input#cultivo").empty().val(temp[3]);
                                $("input#iEstado").empty().val(temp[4]);
                                $("input#iSolicitud").empty().val(temp[5]);
                                $("input#iSemilla").empty().val(temp[6]);
                                $("input#iSuperficie").empty().val(temp[7]);
                                cargarISuperficie5(temp[6], temp[0], temp[1], temp[3], variedad, temp[2]);
                            });
                        } else {
                            $("input#semillerista").val(json.semillerista).addClass('not-edit');
                            $("input#semillera").empty().val(json.semillera).addClass('not-edit');
                            $("input#cultivo").empty().val(json.campanha).addClass('not-edit');
                            //$("input#variedad").empty().val(jsonvariedad).addClass('not-edit');
                            $("input#campanha").empty().val(json.campanha).addClass('not-edit');
                            $("input#iEstado").empty().val(json.estado);
                            $("input#iSolicitud").empty().val(json.isolicitud);
                            $("input#iSemilla").empty().val(json.isemilla);
                            $("input#iSuperficie").empty().val(json.isuperficie);
                            cargarISuperficie5(json.isemilla, json.semillera, (json.semillerista).toString(), json.cultivo, variedad, json.campanha);
                        }
                    });
                } //fin al presionar enter
            } //fin longitud nombre de semillera
        }).on("mouseenter", function() {
            $("#help-semillera").fadeIn();
        }).on("click", function() {
            $("#help-semillera").fadeOut();
        }).on("blur", function() {
            $("#help-semillera").fadeOut();
        }).on("mouseleave", function() {
            $("#help-semillera").fadeOut();
        });

        function cargarISuperficie5(isolicitud, isemilla) {
            var isol = parseInt(isolicitud);
            var isem = parseInt(isemilla);
            //cargar saldo campanha anterior
            $.ajax({
                url : 'control/index.php',
                type : 'POST',
                async : true,
                dataType : 'json',
                data : {
                    mdl : 'certificacion',
                    opt : 'saldo_anterior',
                    isol : isol,
                    isem : isem
                },
                success : function(data) {
                    $("#campAnterior").val(data.saldo);
                }
            });

            //cargar datos de la cuenta
            var isup = $("input#iSuperficie").val();
            var std = $("input#iEstado").val();

            alert(isol + '=' + isem + '=' + isup + '=' + std + '=' + campanha);

            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'resumenCta',
                iSolicitud : isol,
                iSemilla : isem,
                iSuperficie : isup,
                iEstado : std
            }, function(json) {
                $("input#categoriaAprob").val(json.catAprobada).text(json.catAprobada);
                $("input#superficie").val(json.supAprobada).text(json.supAprobada);
                $("input#superficietot").val(json.supTotal).text(json.supTotal);
                $("input#inscripcion").val(json.inscripcion).text(json.inscripcion);
                $("input#icampo").val(json.inspeccion).text(json.inspeccion);
                $("input#anlet").val(json.analisisEtiqueta).text(json.analisisEtiqueta);
                $("input#acondicionamiento").val(json.acondicionamiento).text(json.acondicionamiento);
                $("input#plantines").val(json.plantines).text(json.plantines);
                $("input#total2,input#saldototal").val(json.total).text(json.total);
                $("input#bolsa").val(json.etiquetas).text(json.etiquetas);
            });
        }
    })
</script>