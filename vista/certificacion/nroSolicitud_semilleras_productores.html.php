<div id="anteriores">
    <div>
        <label>N&uacute;mero de solicitud: </label>
        <input id="nosolicitud" name="nosolicitud"/>
        <span id="help-nroSolicitud" class="help">Escriba numero de solicitud</span>
    </div>
    <div>
        <label>Semillera: </label>
        <input id="semillera" name="semillera" style="text-transform:uppercase;"/>
        <span id="help-semillera" class="help">Escriba nombre de semillera</span>
    </div>
    <div class="semilleristas">
        <label>Semillerista: </label>
        <input id="semillerista" name="productor"/>
        <span id="help-semillerista" class="help">Escriba nombre de semillerista</span>
    </div>
</div>
<?php
    include '../vista/dialogos/seleccionarSemilleraSemillerista.html';
    include '../vista/dialogos/seleccionarNroSolicitudCertifica.html';
    include '../vista/dialogos/seleccionarSemilleraNroSolicitud.html';
?>
<script type="text/javascript">
    $(document).ready(function(){
        //permitir solo numeros
        $.funciones.soloNumeros("nosolicitud");
        //permitir solo letras
        $.funciones.soloLetras("semillera");
        $.funciones.soloLetras("semillerista");
    });
</script>