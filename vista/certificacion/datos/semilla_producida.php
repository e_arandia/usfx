<?php
$width = (($area == 10 || $area == 1 || $area == 2) && $edit == 1110) ? "'width:126.5%'" : "'width:101%'";
//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Semilla producida de certificación y fiscalización:</h2>";
} 
?>

<div class="div-filter">

<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 3.2em">
		<div class="htitle">
		    <label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:6em;padding-top: 1%;">N&uacute;mero Campo</label>
			<label style="width:10em;padding-top: 1%;">Categoria Sembrada</label>
			<label style="width:7.5em;padding-top: 1%;">Semilla Neta (T.M.)</label>
			<label style="width:10em;padding-top: 1%;">Categoria Obtenida</label>
			<label style="width:7.5em;padding-top: 1%;">Numero de etiquetas</label>
			<!--<label style="width:7.6em;padding-top: 1%;">Rango de etiquetas</label>-->
			<label style="width:6em;padding-top: 1%;">Datos PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2) && $edit == 1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php }?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera">
            <label class="datoo"> <?php echo $datos -> semillera; ?></label>
        </div>
		<div class="productor">
			<label><?php  echo utf8_encode($datos -> nombre) . ' ' . utf8_encode($datos -> apellido);?></label>
		</div>
		<div class="nSolicitud">
			<label> <?php  echo $datos -> nro_campo;?></label>
		</div>
		<div class="cSembrada cCampo">
			<label> <?php echo str_replace("-0","",$datos -> categoria_sembrada);?></label>
		</div>
		<div class="semillaNeta">
			<label> <?php	echo $datos -> semilla_neta;?></label>
		</div>
		<div class="cSembrada cObtenida">
			<label> <?php	echo $datos -> categoria_obtenida;?></label>
		</div>
		<div class="nroEtiqueta">
			<label> <?php  echo $datos -> nro_etiqueta;?></label>
		</div>
<!--		
		<div class="rango">
			<label> <?php  echo $datos -> rango;?></label>
		</div>
-->
		<div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_solicitud; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
		<?php
        if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
		<div class="sistema options" style="height: 28px; width: 8.4em;">
		    <?php
            if (($area == 1 || $area == 2)&& $edit==1110){
          ?>
			<div>
				<input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_solicitud;?>" />
			</div>
			<?php
            }else{
            ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_solicitud;?>" />
            </div>
            <?php
            }
            if ($area==10){?>  
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos -> id_solicitud;?>" />
			</div>
			<?php } ?>
		</div>
		<?php
		}
		}
		?>
		<input id="idsolicitud" type="hidden"/>
		<input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>
</div>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function(){
        $.funciones.bloqueProduccion();
        pagina_actual = $('input#view-pagina').val();
        $('div#paginacion>a[id='+pagina_actual+']').addClass('active');
    });
</script> 
<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idsolicitud").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#idsolicitud").val('');
        }).on("click", function() {
            var id = $("input#idsolicitud").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=semillaP&id='+id, '_blank');
            win.focus();
        });
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'semillaP',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"nSolicitud\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"cSembrada cCampo\"><label>" + json.categoria_sembrada[index] + "</label> </div>";
                            div += "<div class=\"generacion\"><label>" + json.generacion[index] + "</label> </div>";
                            div += "<div class=\"semillaNeta\"><label>" + json.semilla_neta[index] + "</label> </div>";
                            div += "<div class=\"cSembrada cObtenida\"><label>" + json.categoria_obtenida[index] + "</label> </div>";
                            div += "<div class=\"nroEtiqueta\"><label>" + json.nro_etiqueta[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'comunidad', 'fecha', 'options','pdf'];
                    $.funciones.alternarColores(celdas);
                    var opciones = ['editar', 'eliminar'];
                    $.funciones.alternarColorOpcionImagen(opciones);
                    $("div.body").show();
                });
            } else {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'semillaP',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"nSolicitud\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"cSembrada cCampo\"><label>" + json.categoria_sembrada[index] + "</label> </div>";
                            div += "<div class=\"generacion\"><label>" + json.generacion[index] + "</label> </div>";
                            div += "<div class=\"semillaNeta\"><label>" + json.semilla_neta[index] + "</label> </div>";
                            div += "<div class=\"cSembrada cObtenida\"><label>" + json.categoria_obtenida[index] + "</label> </div>";
                            div += "<div class=\"nroEtiqueta\"><label>" + json.nro_etiqueta[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','nSolicitud', 'sistem', 'semillera', 'productor', 'dpto', 'prov', 'municipio', 'comunidad', 'fecha', 'options','pdf'];
                    $.funciones.alternarColores(celdas);
                    var opciones = ['editar', 'eliminar'];
                    $.funciones.alternarColorOpcionImagen(opciones);
                    $("div.body").show();
                });
            }
        });
        //boton de busqueda avanzada
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'-0.2em'});
        });
        
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'semillaP',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimientofis', 'certificacion');
                $.funciones.cargarImagen('semillaP', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>
<script type="text/javascript">
	$(document).ready(function() {
		 //colorear celdas
        var celdas = ['semillera','productor','nSolicitud','categoria','generacion','semillaNeta','cObtenida','nroEtiqueta','rango','cCampo','options','pdf'];
        $.funciones.alternarColores(celdas);
        var opciones = ['editar', 'eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {

		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#idsemillap").val($(this).val());
		}).click(function() {
			if(confirm('Esta seguro de eliminar el registro?')) {
				var imagen = '<img src="images/loader_3a.gif" />';
				$('.post').block({
					theme : true,
					title : 'Sistema CERFIS',
					message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
				});
				var id = $("#idsemillap").val();
				$.post('control/certificacion/certificacion.ctrl.php', {
					page : 'eliminar',
					tbl : 'semillap',
					id : id
				}, function(data) {
					if(data == 'OK') {
						$('.post').unblock({
							theme : true,
							title : 'Sistema CERFIS',
							message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4.gif'/></div>",
							timeout: 2000
						});
						var area = $("#control").val();
						$.post('control/usuario/usuario.ctrl.php', {
							page : 'vsolicitud',
							area : area
						}, function(data) {
							$(".post").empty().append(data);
						});
					} else {
						$(".post").append(data + ' No se puede eliminar la solicitud');
					}

				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#idsemillap").val($(this).val());
		}).click(function() {
			var id = $("#idsemillap").val();
			$.post('control/certificacion/certificacion.ctrl.php?', {
				page : 'actualizar',
				tbl : 'semillap',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
