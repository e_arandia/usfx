<?php
$width = (($area == 10 || $area == 1 || $area == 2) && $edit == 1110) ? "'width:129.1%'" : "'width:116.9%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
      
//ejecutar consulta sql
DBConnector::ejecutar($blocks);
if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Solicitudes de certificacion y fiscalizacion</h2>";
}    
?>
<div class="div-filter">
<div id="calendar" style=<?php echo $width; ?>>
	<div class="head" style="height: 3.2em">
		<div class="htitle" >
			<label style="width:6em;padding-top: 1%;">Nro. Solicitud</label>
			<label style="width:7em;padding-top: 1%;">Proceso</label>
			<label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:8.2em;padding-top: 1%;">Departamento</label>
			<label style="width:8em;padding-top: 1%;">Provincia</label>
			<label style="width:6.7em;padding-top: 1%;">Municipio</label>
			<label style="width:10em;padding-top: 1%;">Comunidad</label>
			<label style="width:6em;padding-top: 1%;">Fecha Solicitud</label>
			<label style="width:6em;padding-top: 1%;">Datos PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php } ?>
		</div>
	</div>
	<div class="body">
	    <?php
        
while ($datos = DBConnector::objeto()){
        ?>
        <div class="nSolicitud">
            <label class="datoo"> <?php    echo $datos -> nro_solicitud; ?></label>
        </div>
        <div class="sistema sistem">
            <label class="datoo"> <?php  echo utf8_encode($datos -> sistema); ?></label>
        </div>
        <div class="semillera">
            <label class="datoo"> <?php echo ($datos -> semillera); ?></label>
        </div>
        <div class="productor">
            <label class="datoo"> <?php   echo utf8_encode($datos -> nombre) . ' ' . utf8_encode($datos -> apellido); ?></label>
        </div>
        <div class="dpto">
            <label class="datoo"> <?php     echo $datos -> nombre_departamento; ?></label>
        </div>
        <div class="prov">
            <label class="datoo"> <?php  echo ($datos -> provincia); ?></label>
        </div>
        <div class="municipio">
            <label class="datoo"> <?php echo ($datos -> municipio); ?></label>
        </div>
        <div class="comunidad">
            <label class="datoo"> <?php echo ucfirst($datos -> comunidad); ?></label>
        </div>
        <div class="fecha" style="width: 6em">
            <label class="datoo"> <?php echo $dates -> cambiar_formato_fecha($datos -> fecha); ?></label>
        </div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_solicitud; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0;margin-top:-7%;"></a>
        </div>
        <?php
if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
        <div class="sistema options" style="height: 2.5em; width: 8.3em;">
          <?php
          if (($area == 1 || $area == 2)&& $edit==1110){
          ?>  
            <div class="editar">
                <input class="image datoo" alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;width: 22px;height: 22px;margin-top:-5px;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
            <?php
            }else{
            ?>
            <div class="editar">
                <input class="image datoo" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;margin-top:-5px;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
       <?php
    }
    if ($area==10){
       ?>       
            <div class="eliminar  filtre">
                <input class="image" name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -1.8em;right: -1em;margin-top:-5px;" value="<?php echo $datos -> id_solicitud; ?>" />
            </div>
        <?php
        }
        ?>  
        </div>
        <?php
        }
        }
        ?>
	</div>	
</div>
    <input id="idsolicitud" type="hidden"/>
    <input id="control" type="hidden" value="<?php echo $area; ?>" />
    <input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>
</div>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div>  
<script type="text/javascript">
    $(document).ready(function(){
        //parametros de busqueda
        $('span#search-advice').css('letter-spacing','0.07em').empty().text('Nro. solicitud,semillera,semillerista,provincia,municipio ó comunidad');
        $.funciones.bloqueSolicitudes();
        pagina_actual = $('input#view-pagina').val();
        $('div#paginacion>a[id='+pagina_actual+']').addClass('active');
        $('input#filter').on('keyup', function() {
            $.funciones.buscarSolicitudesRegistradas();
        }); 
        //imprimir PDF
        $.funciones.imprimirPDFsolicitud();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //alternar colores
        var celdas = ['nSolicitud','sistem','semillera','productor','dpto','prov','municipio','comunidad','fecha','pdf','options'];
        $.funciones.alternarColores(celdas);
    })
</script>  
<script type="text/javascript">
    $(document).ready(function(){
        
        $.funciones.calendario('input', 'fecha-filter');
        //busqueda
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("button#btn-new").on("click", function() {            
            $.funciones.nuevaSolicitud();           
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar la solicitud?')) {
                var id = $("#idsolicitud").val();
                $.ajax({
                    type : 'POST',
                    url : 'control/index.php',
                    data : {
                        mdl : 'certificacion',
                        opt : 'eliminar',
                        pag : 'solicitud',
                        id : id
                    },
                    beforeSend : function() {
                        $.funciones.mostrarMensaje('info', 'Eliminando solicitud')
                    },
                    success : function(data) {
                        if (data == 'OK') {
                            $.funciones.mostrarMensaje('ok', 'Solicitud eliminada');
                            var area = $("#control").val();
                            if (area != 10) {
                                var datos = {
                                    mdl : 'certificacion',
                                    opt : 'ver',
                                    pag : 'solicitud',
                                    area : area
                                };
                            } else {
                                var datos = {
                                    mdl : 'certificacion',
                                    opt : 'ver',
                                    pag : 'solicitud',
                                    area : area,
                                    edt : 1110
                                }
                            }
                            $.ajax({
                                type : 'POST',
                                url : 'control/index.php',
                                data : datos,
                                beforeSend : function() {
                                    $.funciones.mostrarMensaje('ok', 'Cargando Solicitudes..');
                                },
                                success : function(data) {
                                    $(".post").empty().append(data);
                                }
                            });
                            $.funciones.ocultarMensaje(5000);

                        } else {
                            $.funciones.mostrarMensaje('error', 'No se puede eliminar la solicitud');
                            $.funciones.ocultarMensaje(5000);
                        }
                    }
                });
            }
        });
        /**actualizar datos solicitud*/
        $("input[name=upd]").mouseover(function() {
            $("#idsolicitud").val($(this).val());
        }).click(function() {
            var id = $("#idsolicitud").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_solicitud',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>