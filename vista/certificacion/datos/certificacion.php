<?php
$width = "width: 89.5%";
$width_td = "width: 67.6%";
?>
<div class="entry" >
    <form id="filter-box" class="seguimiento" style="width: 125%; padding: 3% 2% 14%;">
        <div style="float: left; margin-top: 0%; width: 22%;">
            <div style="width: 62%;">
                <label>Semillera</label>
                <input id="semillera-filter" style="width: 130px;">
            </div>
        </div>
        <div style="float: left; width: 26%;">
            <label style="width:75%">Semilerista</label>
            <input id="semillerista-filter" style="width: 130px;">
        </div>
        <div style="float: left; margin-left: -4%; width: 27%;">
            <label style="width:75%">Numero Campo</label>
            <input id="nocampo-filter" style="width: 100px;">
        </div>
        <div style="width: 25%; float: left; margin-left: -9%;">
            <label style="width:85%;">Cultivo</label>
            <input id="cultivo-filter" style="width: 120px;">
        </div>
        <div style="float: right; margin-top: 2em; margin-right: 0em;">
            <button class="btn btn-success" type="reset">
                <span class="glyphicon glyphicon-trash"></span>
                Limpiar
            </button>
        </div>
    </form>
    <div style="margin-left: 80%; width: 50%;">
            <button id="p1" class="btn btn-success" type="button">A - E</button>
            <button id="p2" class="btn btn-success" type="button">F - J</button>
            <button id="p3" class="btn btn-success" type="button">K - O</button>
            <button id="p4" class="btn btn-success" type="button">P - T</button>
            <button id="p5" class="btn btn-success" type="button">U - Z</button>
    </div>    
</div>

<div id="calendar" style="border-bottom: none">
    	<div id="table" class="striped collapsible filterable" style="width: 129%">
		<div id="thead" style="z-index: 999">
			<div class="tr">
				<div style="width: 12em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
    			    Semillera
				</div>
				<div style="width: 3.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
                    N&uacute;mero campo
                </div>
                <div style="width: 3.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
                    Cultivo 
                </div>
				<div style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Solicitud
				</div>
				<div style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Semilla Sembrada
				</div>
				
				<div style="width: 6.1em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Superficie
				</div>
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Inspeccion
				</div>				
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Hoja de Cosecha
				</div>
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Laboratorio
				</div>
				<div style="width: 6.9em;text-align: center;font-weight: bold" class="th">
					Semilla Producida
				</div>
			</div>
		</div>
		<?php
		if (!empty($semillera)){
        $indice = 0;
        $aux = 0;
        foreach ($semillera as $key => $semilleras) {
		?>
		<div class="tbody">
			<div class="tr">
				<div class="th" style="<?php echo $width; ?>; height: 5%;background-color:#80b744;font-weight: bold">
					<?php
                    echo utf8_decode($semilleras);
                    #semillera
                    $counter = 0;
					?>
				</div>
			</div>
			<?php
			
while ($counter < $cantidad[$aux]){
			?>
			<div class="tr control">
				<div class="td productor"style="border-left:none;width: 12em;">
					<?php echo utf8_encode($dates->cambiar_signoByAcento($productor[$indice])); ?>
				</div>
				<div class="td semilla nocampo"style="border-left:none;width:5em;text-align: center;">
                    <?php
                    Semilla::getCampoCultivoById($id_productor[$indice]);

                    $fila = DBConnector::objeto();
                    echo(DBConnector::filas()) ? $fila -> nro_campo : '0';
                    //echo $nocampo;

                    echo '</div>';
                    echo "<div class='td cultivo' style='border-left:none;width:3em;text-align: center;'>";
                    echo(DBConnector::filas()) ? $fila -> cultivo : '-';
                    //echo $cultivos;
                     ?>
                </div>
				<div class="td estado" style="border-left:none;<?php echo $width_td; ?>">
					<?php
					$color = ($counter%2==0)?'':'#edffd4';
                    $estado = intval($etapa[$indice]);
                    $id = intval($id_productor[$indice]);
                    switch ($estado) {
                        case 0 :
                            echo "<input type='image' class='proImg' value=$id src='images/0.png' style='position: relative;top: 0.5em;margin-left:3em;background-color:$color'/>";
                            break;
                        case 1 :
                            echo "<input type='image' class='proImg' value=$id src='images/1.png' style='position: relative;top: 0.5em;margin-left:2em;background-color:$color'/>";
                            break;
                        case ($estado==2 || $estado==3 || $estado==4) :
                            echo "<input type='image' class='proImg' value=$id src='images/2.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                            break;
                        case ($estado==5 || $estado==6 || $estado==7) :
                            echo "<input type='image' class='proImg' value=$id src='images/3.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                            break;
                        case 8 :
                            if($fila->cultivo !='Acondicionamiento'){
                                echo "<input type='image' class='proImg' value=$id src='images/4.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                            }else{
                                echo "<input type='image' class='proImg' value=$id src='images/4acon.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";                                    
                            }                            
                            break;
                        case 9 :
                            if ($fila -> cultivo != 'Papa' && $fila -> cultivo != 'Plantines'){
                                echo "<input type='image' class='proImg' value=$id src='images/5.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                            }else{
                                echo "<input type='image' class='proImg' value=$id src='images/5papa.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";    
                            }
                            break;
                        case ($estado>=10) :
                            if ($fila -> cultivo != 'Papa' && $fila -> cultivo != 'Plantines'){
                                echo "<input type='image' class='proImg' value=$id src='images/6.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                            }else{
                                if ($fila ->cultivo != 'plantines'){
                                    echo "<input type='image' class='proImg' value=$id src='images/6papa.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                                }else{
                                    echo "<input type='image' class='proImg' value=$id src='images/6plantines.png' style='position: relative;top: 0.5em;margin-left:2.1em;background-color:$color'/>";
                                }        
                            }
                            break;
                    }
					?>
				</div>
			</div>
			<?php
            $counter++;
            $indice++;
            }

            $aux++;
			?>
		</div>
		<?php }
            }
		?>
	</div>
</div>
<?php
include '../vista/dialogos/registroCertificacion.html';
?>
<script type="text/javascript">
    $(document).ready(function() {
        //boton de busqueda avanzada
        $("button#btn-filter").on("click", function() {
            $("form#filter-box").slideToggle();
        });
        //boton de busqueda
        $("button#search-filter").on("click", function() {
            $.funciones.copiarBusquedaDatos('filter-box');
        });
        //busqueda case-isensitive
        $.extend($.expr[':'], {
            'contains-ci' : function(elem, i, match, array) {
                return (elem.textContent || elem.innerText || $(elem).text() || '').toLowerCase().indexOf((match[3] || '').toLowerCase()) >= 0;
            }
        });
        //busqueda de semilleras
        $("input#semillera-filter").on("keyup", function() {
            //obteniene el texto introducido
            var texto = $(this).val();

            //oculta todas las semilleras
            $("div.tbody>.tr>.th").hide();
            //muestra solo las coincidencias
            $("div.tbody>.tr>.th:contains-ci(\'" + texto + "\')").show();
        });
        //busqueda de semilleristas
        $("input#semillerista-filter").on("keyup", function() {
            var texto = $(this).val();
            $("div.tbody>.tr.control>.td.productor,div.tbody>.tr.control>.td.nocampo,div.tbody>.tr.control>.td.cultivo,div.tbody>.tr.control>.td.estado").hide();

            $("div.tbody>.tr.control>.td.productor:contains-ci(\'" + texto + "\')").show(function() {
                $(this).show();
                $(this).siblings().show();
            });
        });
        //busqueda de cultivos
        $('input#cultivo-filter').on('keyup', function() {
            var texto = $(this).val();
            $('div.tbody>.tr.control>.td.productor,div.tbody>.tr.control>.td.nocampo,div.tbody>.tr.control>.td.cultivo,div.tbody>.tr.control>.td.estado').hide();
            $('div.tbody>.tr.control>.td.cultivo:contains-ci(\'' + texto + '\')').show(function() {
                $(this).show();
                $(this).siblings().show();
            });
        });
        //busqueda de numero de campo
        $('input#nocampo-filter').on('keyup', function() {
            var texto = $(this).val();
            $('div.tbody>.tr.control>.td.productor,div.tbody>.tr.control>.td.nocampo,div.tbody>.tr.control>.td.cultivo,div.tbody>.tr.control>.td.estado').hide();
            $('div.tbody>.tr.control>.td.nocampo:contains-ci(\'' + texto + '\')').show(function() {
                $(this).show();
                $(this).siblings().show();
            });
        });
        //filtro por botones
        $('button#p1').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p2').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion&inicio=F&final=J',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p3').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion&inicio=K&final=O',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p4').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion&inicio=P&final=T',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
        $('button#p5').on('click', function() {
            $.ajax({
                type : 'GET',
                url : 'control/index.php?mdl=certificacion&opt=ver&pag=proceso&sistema=certificacion&inicio=U&final=Z',
                beforeSend : function() {
                    $("div#calendar").empty().html("<img src='images/loader_3a.gif' /> <a>Cargando Semilleras...</a>");
                },
                success : function(data) {
                    $.funciones.ocultarMensaje(200);
                    $('.post').empty().append(data);
                }
            });
        });
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#thead').scrollFix();
        $("div.odd").css("width", "98.2%");
        $("div.even").css("width", "98.2%");

        //crear reporte
        $(".proImg").on("click", function() {
            var id = $(this).val();
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'registro',
                id : id
            }, function(json) {
                $.funciones.etapasRegistro(parseInt(json.estado));
                $("span#rsemillera").empty().text($.funciones.cambiarSignoPorAcento(json.semillera));
                $("span#rsemillerista").empty().text($.funciones.cambiarSignoPorEnhe ($.funciones.cambiarSignoPorAcento(json.semillerista)));
                $.funciones.mostrarDatosRegistro(parseInt(json.estado),json);
            });
            $("#regCertifica").dialog({
                title : "Registro de semillerista",
                width : 500,
                buttons : [{
                    text : "Aceptar",
                    click : function() {
                        $('div#reg-solicitud>div>span','div#reg-semilla>div>span').empty();
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                    }
                }]
            });
        });

        //$(document).ready(function() {
        $('#table.striped').bind('stripe', function() {
            $('.tbody', this).each(function() {
                $(this).find('.tr:not(:has(.th))').filter(function() {
                    return this.style.display != 'none';
                }).removeClass('odd').addClass('even').filter(function(index) {
                    return (index % 6) < 3;
                }).removeClass('even').addClass('odd');
            });
        }).trigger('stripe');
        var collapseIcon = 'images/bullet_toggle_minus.png';
        var collapseText = 'ocultar esta seccion';
        var expandIcon = 'images/bullet_toggle_plus.png';
        var expandText = 'mostrar esta seccion';
        $('#table.collapsible .tbody').each(function() {
            $('.odd, .even').css('display', 'none');
            var $section = $(this);
            $section.addClass('collapsed');
            $('<img />').attr('src', expandIcon).attr('alt', expandText).prependTo($section.find('.th')).addClass('clickable').click(function() {

                if ($section.is('.collapsed')) {
                    $section.removeClass('collapsed').find('.tr:not(:has(.th))').fadeOut('fast', function() {
                        $(this).css('display', 'block');
                    });
                    $(this).attr('src', collapseIcon).attr('alt', collapseText);
                } else {
                    $section.addClass('collapsed').find('.tr:not(:has(.th)):not(.filtered)').fadeIn('fast');
                    $(this).attr('src', expandIcon).attr('alt', expandText);
                    $('#table.collapsible .tbody>.control').css('display', 'none');

                }
                $section.parent().trigger('stripe');
            });
        });
    })
</script>