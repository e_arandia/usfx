<?php
$width = (($area == 10 || $area == 1 || $area == 2)&& $edit==1110)? "'width:166.9%'" : "'width:154.3%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);


//ejecutar consulta sql
DBConnector::ejecutar($blocks);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Lista de hojas de cosechas :</h2>";
} 
?>

<div class="div-filter">

<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 3.2em;">
		<div class="htitle" >
		    <label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:12em;padding-top: 1%;">Semillerista</label>
			<label style="width:7em;padding-top: 1%;">Numero Campo</label>
			<label style="width:7em;padding-top: 1%;">Fecha Emision</label>
			<label style="width:10em;padding-top: 1%;">Categoria Campo</label>
			<label style="width:8em;padding-top: 1%;">Rendimiento Estimado</label>
			<label style="width:7em;padding-top: 1%;">Superficie de Parcela</label>
			<label style="width:7em;padding-top: 1%;">Rendimiento Campo</label>
			<label style="width:6em;padding-top: 1%;">Numero de cupones</label>
			<label style="width:9em;padding-top: 1%;">Rango de cupones</label>
			<label style="width:12.7em;padding-top: 1%;">Planta acondicionadora</label>
			<label style="width:5.9em;padding-top: 1%;">Datos PDF</label>
			<?php if (($area == 10 || $area == 1 || $area== 2)&& $edit==1110){?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php }?>
		</div>
	</div>
	<div class="body" style="width:98%">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera">
            <label class="datoo"> <?php echo utf8_encode($datos -> semillera); ?></label>
        </div>
		<div class="productor odd1" style="width: 12em;height: 2.5em;">
			<label><?php  echo utf8_encode($datos -> semillerista);?></label>
		</div>
		<div class="nSolicitud odd2" style="width: 7em;height: 2.5em;">
			<label> <?php  	echo $datos -> nro_campo;?></label>
		</div>
		<div class="fecha odd3" style="width: 7em;height: 2.5em;">
			<label><?php echo !empty($datos->fecha_emision)?$dates -> cambiar_formato_fecha($datos -> fecha_emision):'00-00-0000';?></label>
		</div>
		<div class="cSembrada odd4" style="width: 10em;height: 2.5em;">
			<label><?php echo $datos -> categoria_en_campo;?></label>
		</div>
		<div class="cantidadS odd5" style="width:8em;height: 2.5em;">
			<label> <?php	echo str_replace('.', ',', $datos -> rendimiento_estimado);?></label>
		</div>
		<div class="sParcela odd6" style="width:7em;height: 2.5em;">
			<label><?php	echo str_replace('.', ',', $datos -> superficie_parcela);?></label>
		</div>
		<div class="cantidadS odd7" style="width: 7em;height: 2.5em;">
			<label><?php	echo str_replace('.', ',', $datos -> rendimiento_campo);?></label>
		</div>
		<div class="rango odd8" style="width: 6em;height: 2.5em;">
			<label><?php echo  (!empty($datos -> nro_cupones)) ? $datos -> nro_cupones : 0; ?></label>
		</div>
		<div class="rango odd9" style="width: 9em;height: 2.5em;">
			<label><?php echo (!empty($datos -> rango_cupones)) ? $datos -> rango_cupones : '-';	?></label>
		</div>
		<div class="cAnterior odd0" style="width: 12.5em;height: 2.5em;">
			<label> <?php echo (!empty($datos -> planta_acondicionadora)) ? ucfirst($datos -> planta_acondicionadora) : '-'; ?></label>
		</div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_cosecha; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>        
        <?php
        if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
        <div class="sistema options" style="height: 28px; width: 8.5em;">
              <?php
          if (($area == 1 || $area == 2)&& $edit==1110){
          ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_inspeccion;?>" />
            </div>
            <?php
            }else{
            ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_inspeccion;?>" />
            </div>
            <?php
            }
            if ($area==10){?>   
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2.3em;right: -1em;" value="<?php echo $datos -> id_inspeccion;?>" />
            </div>
            <?php } ?>
        </div>
        <?php
        }
        }
        ?>
		<input id="idcosecha" type="hidden"/>
		<input id="control" type="hidden" value="<?php echo $area;?>" />
	</div>
</div>

<div class="meneame" style="padding-top: 65%; width: 50%; margin-left: 30%;">
    <?php
        if ($total > $nroRegistros) {
        for ($cont = 1; $cont <= $plinks; $cont++) {
            if (($cont % 20) == 1) {
                echo "</br></br>";
            }
            $temp = $cont - 1;
            echo "<a id=$cont name='pagina' onclick=cargarSolicitudes('certificacion','ver','cosecha',$area,$temp)>$cont</a> ";
        }
    }
    ?>
    </div> 
</div>
<div class="div-new" style="display:none;">
    
</div>     

<script type="text/javascript">
    $(document).ready(function() {
        //reporte PDF
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idcosecha").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#idcosecha").val('');
        }).on("click", function() {
            var id = $("input#idcosecha").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=cosecha&id='+id);
            win.focus();
        });
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'cosecha',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (json.total) {
                            div += "<div class=\"semillera\"><label>"+json.semillera[index]+"</label> </div>";
                            div += "<div class=\"productor odd1\" style=\"width: 12em;height: 2.5em;\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"nSolicitud odd2\" style=\"width: 7em;height: 2.5em;\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"fecha odd3\" style=\"width: 7em;height: 2.5em;\"><label>" + json.fecha_emision[index] + "</label> </div>";
                            div += "<div class=\"cSembrada odd4\" style=\"width: 10em;height: 2.5em;\"><label>" + json.categoria_campo[index] + "</label> </div>";
                            div += "<div class=\"cantidadS odd5\" style=\"width:8em;height: 2.5em;\"><label>" + json.rendimiento_estimado[index] + "</label> </div>";
                            div += "<div class=\"sParcela odd6\" style=\"width:7em;height: 2.5em;\"><label>" + json.parcela[index] + "</label> </div>";
                            div += "<div class=\"cantidadS odd7\" style=\"width: 7em;height: 2.5em;\"><label>" + json.rendimiento_campo[index] + "</label> </div>";
                            div += "<div class=\"rango odd8\" style=\"width: 6em;height: 2.5em;\"><label>" + json.nro_cupones[index] + "</label> </div>";
                            div += "<div class=\"rango odd9\" style=\"width: 9em;height: 2.5em;\"><label>" + json.rango_cupones[index] + "</label> </div>";
                            div += "<div class=\"cAnterior odd0\" style=\"width: 12.5em;height: 2.5em;\"><label>" + json.planta_acondicionadora[index] + "</label> </div>";
                            $('div.body').empty().append(div);
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','odd1','odd2','odd3','odd4','odd5','odd6','odd7','odd8','odd9','odd0','pdf','sistema'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cosecha',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            }); 
            }
        });
        
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").slideUp();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").slideUp();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'cosecha',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimientofis', 'certificacion');
                $.funciones.cargarImagen('cosechafis', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
            });
        });
    });
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var celdas = ['semillera','odd1','odd2','odd3','odd4','odd5','odd6','odd7','odd8','odd9','odd0','pdf','sistema'];
        $.funciones.alternarColores(celdas);
	});
	function cargarSolicitudes(mdl, opt, pag, area, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {

		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#idcosecha").val($(this).val());
		}).click(function() {
			if(confirm('Esta seguro de eliminar el registro?')) {
				var imagen = '<img src="images/loader_3a.gif" />';
				$('.post').block({
					theme : true,
					title : 'Sistema CERFIS',
					message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
				});
				
				var id = $("#idcosecha").val();
				$.post('control/certificacion/certificacion.ctrl.php', {
					page : 'eliminar',
					tbl : 'solicitud',
					id : id
				}, function(data) {
					if(data === 'OK') {
						$('.post').unblock({
							theme : true,
							title : 'Sistema CERFIS',
							message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4.gif'/></div>",
							timeout: 2000
						});
						var area = $("#control").val();
						$.post('control/certificacion/certificacion.ctrl.php', {
							page : 'eliminar',
							tbl : 'cosecha',
							id : id
						}, function(data) {
							$(".post").empty().append(data);
						});
					} else {
						$(".post").append(data);
					}
				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#idcosecha").val($(this).val());
		}).click(function() {
			var id = $("#idcosecha").val();
			$.post('control/certificacion/certificacion.ctrl.php', {
				page : 'actualizacion',
				tbl : 'cosecha',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	});
</script>
