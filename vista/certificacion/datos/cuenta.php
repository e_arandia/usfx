<?php
$width = (($area == 10 || $area == 1 || $area == 2) && $edit == 1110)? "'width:211%'" : "'width:203.5%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Cuentas de certificación:</h2>";
} 
?>

<div class="div-filter">
<div id="calendar" style=<?php echo $width;?>>
    <div class="head" style="height: 3.8em">
        <div class="htitle">
            <label style="width:12em;padding-top: 1%;">Responsable</label>
            <label style="width:10em;padding-top: 1%;">Semillera</label>
            <label style="width:12em;padding-top: 1%;">Semillerista</label>
            <label style="width:9em;padding-top: 1%;">Campaña</label>
            <label style="width:7.5em;padding-top: 1%;">Cultivo</label>
            <label style="width:8.5em;padding-top: 1%;">Variedad</label>
            <label style="padding-top: 1%; width: 9em;">Categoria Aprobada</label>
            <label style="height:3.2em;width: 7em;">Superficie Aprobada (Ha.)</label>
            <label style="padding-top: 1%; width: 7em;">Superficie Total (Ha.)</label>
            <label style="width:6em;padding-top: 1%;">Etiquetas - Bolsas </label>
            <!--<label style="width:6em;padding-top: 1%;">Subtotal</label>-->
            <label style="padding-top: 1%; width: 7em;">Inscripci&oacute;n (Bs.)</label>
            <label style="padding-top: 1%; width: 7em;">Inspecci&oacute;n Campo (Bs.)</label>
            <label style="padding-top: 1%; width: 11em;">Analisis y Etiquetaci&oacute;n (Bs.)</label>
            <label style="width:6em;padding-top: 1%;">Total</label>
            <label style="width:6em;padding-top: 1%;">Saldo</label>
            <label style="padding-top: 1%; width: 8em;">Ver Detalle</label>            
            <!--<label style="width:6em;padding-top: 1%;">Plantines</label>-->
            <!--<label style="width:6em;padding-top: 1%;">Fecha</label>-->
            <!--<label style="width:6em;padding-top: 1%;">Monto Pagado</label>-->
            <!--<label style="width:8em;padding-top: 1%;">Saldo Campaña Anterior</label>-->
            <label style="width:6em;padding-top: 1%;">PDF Cuenta</label>
            <?php if (($area == 10 || $area == 1 || $area == 2) && $edit == 1110){
            ?>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>
            <?php }?>
        </div>
    </div>
    <div class="body">
        <?php
foreach($objCuenta['semillera'] as $key=>$semillera){
        ?>
        <div class="productor resp tr" style="width:12em;">
            <label> <?php    echo utf8_encode($objCuenta['responsable'][$key]);?></label>
        </div>
        <div class="semillera tr" style="width:10em;">
            <label> <?php   echo utf8_encode(strtoupper($semillera));?></label>
        </div>
        <div class="productor semillerista tr" style="width:12em;">
            <label> <?php   echo utf8_encode($objCuenta['semillerista'][$key]);?></label>
        </div>
        <div class="campanha tr" style="width:9em;" >
            <label> <?php   echo $objCuenta['campanha'][$key].'/'.date('y');?></label>
        </div>
        <div class="cultivo tr" style="width:7.5em;">
            <label> <?php   echo $objCuenta['cultivo'][$key];?></label>
        </div>
        <div class="variedad tr" style="width:8.5em;">
            <label> <?php  echo utf8_encode($objCuenta['variedad'][$key]);?></label>
        </div>
        <div class="sInscrita cat-aprobada tr" style="width:9em;">
            <label> <?php   echo $objCuenta['categoria_aprobada'][$key];?></label>
        </div>
        <div class="sInscrita sup-aprobada tr" style="width:7em;">
            <label> <?php echo str_replace(',', '.', $objCuenta['superficie_aprobada'][$key]);?></label>
        </div>
        <div class="sInscrita sup-total tr" style="width:7em;">
            <label> <?php   echo str_replace(',', '.', $objCuenta['superficie_total'][$key]);?></label>
        </div>
        <div class="sInscrita bolsa-etiqueta tr" style="width:6em;">
            <label> <?php   echo $objCuenta['etiquetas'][$key];?></label>
        </div>
<!--        
        <div class="sInscrita subtotal tr" style="width:6em;">
            <label> <?php   echo str_replace('.', ',', $datos -> subtotal);?></label>
        </div>
-->        
        <div class="sInscrita inscripcion tr" style="width:7em;">
            <label> <?php   echo ($objCuenta['inscripcion'][$key]*$objCuenta['superficie_aprobada'][$key]);?></label>
        </div>
        <div class="sInscrita icampo tr" style="width:7em;">
            <label> <?php   echo ($objCuenta['inspeccion'][$key]*$objCuenta['superficie_aprobada'][$key]);?></label>
        </div>
        <div class="sInscrita ale tr" style="width:11em;">
            <label> <?php   echo ($objCuenta['analisis'][$key]*$objCuenta['etiquetas'][$key]);?></label>
        </div>
      
        <div class="sInscrita total tr" style="width:6em;">
            <label> <?php   echo str_replace(',', '.', $objCuenta['total'][$key]);?></label>
        </div>
        <div class="sInscrita saldo tr" style="width:6em;">
            <label> <?php   echo str_replace(',', '.', $objCuenta['saldo'][$key]);?></label>
        </div>
   <!--       
        <div class="sInscrita total tr" style="width:6em;">             
        <label> <?php   echo str_replace('.', ',', $datos -> total);?></label>
        </div>
        <div class="fecha f-pago tr" style="width:6em;">
            <label> <?php echo $dates -> cambiar_formato_fecha($datos -> fecha);?></label>
        </div>
        <div class="fecha monto tr" style="width:6em;">
            <label> <?php   echo str_replace('.', ',', $datos -> monto_pagado);?></label>
        </div>
       
        <div class="fecha saldo" style="width:6em;">
            <label> <?php   echo str_replace('.', ',', $datos -> saldo_anterior);?></label>
        </div>
 -->       
        <div class="fecha detalle" style="width:7.9em;">
            <a id="<?php echo $objCuenta['fullcuenta'][$key]; ?>" class="glyphicon glyphicon-list-alt btn-lg" style="cursor:pointer;padding:0"></a>
        </div>

<div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $objCuenta['isemilla'][$key]; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
        <?php
            if (($area == 10 || $area == 1 || $area == 2) && $edit == 1110){
        ?>
        <div class="sistema options" style="height: 27px; width: 8.4em;">
            <?php
            if (($area == 1 || $area == 2)&& $edit==1110){
          ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_cuenta;?>" />
            </div>
            <?php
            }else{
            ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_cuenta;?>" />
            </div>
            <?php
            }
            if ($area==10){?>  
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2.3em;right: -1em;" value="<?php echo $datos -> id_cuenta;?>" />
            </div>
            <?php } ?>
        </div>
        <?php
        }
        }
        ?>
        <input id="icuenta" type="hidden"/>
        <input id="idsolicitud" type="hidden"/>        
        <input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
    </div>
</div>
</div>
<?php
    include '../vista/dialogos/cuotaCuenta.html';
    ?>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function() {
        $.funciones.bloqueCuenta();
        pagina_actual = $('input#view-pagina').val();
        $('div#paginacion>a[id=' + pagina_actual + ']').addClass('active');
        $('div.detalle>a').on('click', function() {
            var semillera;
            var semillerista;
            var cultivo;
            var variedad;
            var total
            var isemilla;
            var valores = $(this).attr('id');
            temp = valores.split('=');
            semillera = temp[1];
            semillerista = temp[2];
            cultivo = temp[3];
            variedad = temp[4];
            total = temp[5];
            isemilla = temp[0];

            $('div#lst-cuota>div#tipos>div#tipo-cuota').buttonset();
            $('#lst-cuota label.slr>span').empty().append(semillera);
            $('#lst-cuota label.smr>span').empty().append($.funciones.cambiarSignoPorAcento(semillerista));
            $('#lst-cuota label.clt>span').empty().append($.funciones.cambiarSignoPorAcento(cultivo));
            $('#lst-cuota label.var>span').empty().append($.funciones.cambiarSignoPorAcento(variedad));
            $('#lst-cuota label.sld>span.total').empty().append(total);

            $('div.tbl-cuota').css({'height': '','border-bottom': ''});
            $.funciones.buscarDetalleCuotasCuenta(isemilla);
        });
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //busqueda por filtro
        $("input#filtersss").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'cuenta',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.semillera, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"productor resp\" style=\"width:12em;\"><label>" + json.responsable[index] + "</label> </div>";
                            div += "<div class=\"semillera\" style=\"width:10em;\"><label>" + json.semillera[index] + "</label> </div>";
                            div += "<div class=\"productor semillerista\" style=\"width:12em;\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"campanha\" style=\"width:9em;\"><label>" + json.campanha[index] + "</label> </div>";
                            div += "<div class=\"cultivo\" style=\"width:7.5em;\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\" style=\"width:8.5em;\"><label>" + json.variedad[index] + "</label> </div>";
                            div += "<div class=\"sInscrita cat-aprobada\" style=\"width:9em;\"><label>" + json.categoria_aprobada[index] + "</label> </div>";
                            div += "<div class=\"sInscrita sup-aprobada\" style=\"width:7em;\"><label>" + json.superficie_aprobada[index] + "</label> </div>";
                            div += "<div class=\"sInscrita sup-total\" style=\"width:7em;\"><label>" + json.total[index] + "</label> </div>";
                            div += "<div class=\"sInscrita bolsa-etiqueta\" style=\"width:6em;\"><label>" + json.bolsa_etiqueta[index] + "</label> </div>";
                            div += "<div class=\"sInscrita subtotal\" style=\"width:6em;\"><label>" + json.subtotal[index] + "</label> </div>";
                            div += "<div class=\"sInscrita inscripcion\" style=\"width:7em;\"><label>" + json.inscripcion[index] + "</label> </div>";
                            div += "<div class=\"sInscrita icampo \" style=\"width:7em;\"><label>" + json.inspeccion[index] + "</label> </div>";
                            div += "<div class=\"sInscrita ale\" style=\"width:11em;\"><label>" + json.analisis[index] + "</label> </div>";
                            div += "<div class=\"sInscrita acondicionamiento\" style=\"width:11em;\"><label>" + json.acondicionamiento[index] + "</label> </div>";
                            div += "<div class=\"sInscrita plantines\" style=\"width:6em;\"><label>" + json.plantines[index] + "</label> </div>";
                            div += "<div class=\"sInscrita total\" style=\"width:6em;\"><label>" + json.total[index] + "</label> </div>";
                            div += "<div class=\"fecha f-pago\" style=\"width:6em;\"><label>" + json.fecha[index] + "</label> </div>";
                            div += "<div class=\"fecha monto\" style=\"width:6em;\"><label>" + json.monto[index] + "</label> </div>";
                            div += "<div class=\"fecha saldo\" style=\"width:6em;\"><label>" + json.saldo_anterior[index] + "</label> </div>";
                            div += "<div class=\"fecha saldo-ant\" style=\"width:7.9em;\"><label>" + json.saldo[index] + "</label> </div>";
                            //div += "<div class=\"sistema options\"><label>" + json.options[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['resp','semillera','semillerista','cultivo','variedad','campanha','cat-aprobada','sup-aprobada','sup-total', 'subtotal'];
                    var celdas2 = ['inscripcion','icampo','ale','acondicionamiento','plantines','total','f-pago','monto','saldo','saldo-ant','bolsa-etiqueta','pdf'];
                    $.funciones.alternarColores(celdas);
                    $.funciones.alternarColores(celdas2);
                    var opciones = ['editar', 'eliminar'];
                    $.funciones.alternarColorOpcionImagen(opciones);
                    $("div.body").show();
                });
            } else {
                $("div.body").show();
            }
        });
        
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : '','margin-top':'0'});
        });
        
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'cuenta',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimientofis', 'certificacion');
                $.funciones.cargarImagen('cuenta', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#idsolicitud").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#idsolicitud").val('');
        }).on("click", function() {
            var id = $("input#idsolicitud").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=cuenta&id=' + id, '_blank');
            win.focus();
        });
        var celdas = ['resp','semillera','semillerista','cultivo','variedad','campanha','cat-aprobada','sup-aprobada','sup-total','subtotal'];
        var celdas2 = ['inscripcion','icampo','ale','acondicionamiento','plantines','total','f-pago','monto','saldo','saldo-ant','bolsa-etiqueta','pdf'];
        $.funciones.alternarColores(celdas);
        $.funciones.alternarColores(celdas2);
        var opciones = ['editar', 'eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#icuenta").val($(this).val());
        }).click(function() {
            
            if(confirm('Esta seguro de eliminar el registro?')) {
                var imagen = '<img src="images/loader_3a.gif" />';
                $('.post').block({
                    theme : true,
                    title : 'SICERFIS',
                    message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
                });
                var id = $("#icuenta").val();
                $.post('control/certificacion/certificacion.ctrl.php', {
                    page : 'eliminar',
                    tbl : 'cuenta',
                    id : id
                }, function(data) {
                    if(data == 'OK') {
                        $('.post').unblock({
                            theme : true,
                            title : 'Sistema CERFIS',
                            message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4'/></div>",
                            timeout: 2000
                        });
                        $.post('control/usuario/usuario.ctrl.php', {
                            page : 'vcuentas',
                            area : 'administracion'
                        }, function(data) {
                            $(".post").empty().append(data);
                        });
                        
                    } else {
                        $("#errores div p .mensaje").empty().append(data);
                        $("#errores").fadeIn('slow');
                    }

                });
            }
        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#icuenta").val($(this).val());
        }).click(function() {
            var id = $("#icuenta").val();
            $.post('control/certificacion/certificacion.ctrl.php', {
                page : 'actualizar_cuenta',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>
