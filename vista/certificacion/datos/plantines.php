<?php

$width = (($area == 10 || $area == 1) && $edit == 1110) ? "'width:188.9%'" : "'width:76.2%'" ;
$widthParcela = (($area == 10 || $area == 1) && $edit == 1110) ? "6em" : "6.2em";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
//ejecutar consulta sql
DBConnector::ejecutar($blocks);
if (DBConnector::nroError()){
    echo "ERROR ::".__METHOD__."::$blocks",DBConnector::mensaje();
}
if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Lista de plantines:</h2>";
} 
?>

<div class="div-filter">
    <h3 style="margin-bottom: 1%;font-size: large;">
        <span style="font-weight: bolder;color:#5cb85c;">MOSTRAR : </span>
    </h3>
    
<div id="view-opciones-semilla" style="width:101%;">    
    <input type="radio" id="semilla-cultivo" name="semilla" value="1"/>
    <label for="semilla-cultivo" style="width: 5%">Cultivo</label>
    <input type="radio" id="semilla-acondicionamiento" name="semilla" value="2"/>
    <label for="semilla-acondicionamiento" style="width: 16%;">Acondicionamiento</label>
    <input type="radio" id="semilla-plantines" name="semilla" value="3"  checked="checked" />
    <label for="semilla-plantines" style="width: 7%;">Plantines</label>
</div>
<div id="calendar" style=<?php echo $width; ?>>
	<div class="head" style="height: 3.2em">
		<div class="htitle" >
		    <label style="width:12em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:8em;padding-top: 1%;">Comunidad</label>
			<label style="width:7.5em;padding-top: 1%;">Cultivo</label>
			<label style="width:8.5em;padding-top: 1%;">Variedad</label>
			<?php
        if ($area == 10 || $area == 1){
        ?>			
			<label style="width:6em;padding-top: 1%;">Datos Plantines</label>
			<?php
            }
        ?>			
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
            ?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php } ?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()) {
		?>
		<div class="semillera">
            <label class="datoo"> <?php echo utf8_encode($datos->semillera); ?></label>
        </div>
		<div class="productor">
			<label class="datoo"> <?php   echo utf8_encode($datos->semillerista); ?></label>
		</div>
		<div class="comunidad" style="width: 8em">
            <label class="datoo"> <?php   echo ucfirst($datos->comunidad); ?></label>
        </div>
		<div class="cultivo">
			<label class="datoo"> <?php	echo ucfirst($datos->cultivo); ?></label>
		</div>
		<div class="variedad">
			<label class="datoo"  style="text-transform:capitalize"> <?php	echo utf8_encode($datos->variedad); ?></label>
		</div>
		<div style="padding-top:1%;width: 6em;" class="pdf">
            <a id="<?php echo $datos->id_semilla;?>"style="cursor:pointer;padding:0" class="glyphicon glyphicon-print btn-lg"></a>
        </div>
        <?php
if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
        <div class="sistema options" style="height: 28px; width: 8.3em;">
          <?php
          if (($area == 1 || $area == 2)&& $edit==1110){
          ?>  
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
            <?php
            }else{
            ?>
            <div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
       <?php
    }
    if ($area==10){
       ?>        
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2em;right: -1em;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
        <?php
        }
        ?>  
        </div>
        <?php
        }
        }
        ?>
		<input id="semilla" type="hidden"/>
		<input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>
</div>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function(){
        //opciones de filtro
        $("#view-opciones-semilla").buttonset();
        $.funciones.opcionesSemillaCertifica();
    });
</script>     
<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#semilla").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#semilla").val('');
        }).on("click", function() {
            var id = $("input#semilla").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=plantines&id='+id, '_blank');
            win.focus();
        });
        edt = $(".crud").val();
        if (edt == 1110){
            $("label:contains('Cantidad de Semilla')").css('height','3em');
        }else{
            $("label:contains('Cantidad de Semilla')").css('height','2.9em');
        }
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'semilla',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var final = json.total;
                    var div = '';
                    $.each(json.semillerista, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label class=>"+json.semillera[index]+"</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"comunidad\" style=\"width:8em\"><label>" + json.comunidad[index] + "</label> </div>";
                            div += "<div class=\"nSolicitud\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"campanha\"><label>" + json.campanha[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\" style=\"text-transform:capitalize\"><label>" + json.variedad[index] + "</label> </div>";
                            div += "<div class=\"cSembrada\"><label>" + json.categoria_sembrada[index] + "</label> </div>";
                            div += "<div class=\"cProducir\"><label>" + json.categoria_producir[index] + "</label> </div>";
                            div += "<div class=\"cantidadS\"><label>" + json.cantidad_semilla[index] + "</label> </div>";
                            div += "<div class=\"lote\" style=\"width:8em\"><label>" + json.nro_lote[index] + "</label> </div>";
                            div += "<div class=\"fecha\" style=\"width:7em\"><label>" + json.fecha[index] + "</label> </div>";
                            div += "<div class=\"plantas\"><label>" + json.plantas_hectarea[index] + "</label> </div>";
                            div += "<div class=\"sParcela cultAnterior\" style=\"width: 6em\"><label>" + json.cultivo_anterior[index] + "</label> </div>";
                            div += "<div class=\"sParcela supParcela\" style=\"width: 6em\"><label>" + json.parcela[index] + "</label> </div>";
                            if (edt==1110)
                                div += "<div><input type=\"image\" value=\"" + json.id[index] + "\" style=\"position: relative;\" src=\"images/editar22x22.png\" name=\"upd\" alt=\"editar\"></div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    //colorear celdas
        var celdas = ['semillera','nSolicitud','comunidad','localidad','campanha','cultivo','variedad','cSembrada','cProducir'];
        $.funciones.alternarColores(celdas);
        var celdas2 = ['cantidadS','lote','fecha','plantas','cAnterior','cultAnterior','supParcela','productor','pdf','options'];
        $.funciones.alternarColores(celdas2);
        var opciones = ['editar', 'eliminar'];
            $.funciones.alternarColorOpcionImagen(opciones);
        //envio de datos
                    $("div.body").show();
                });
            } else {
                $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'semilla',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
            }
        });
        //boton de busqueda
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        //boton nuevo
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div#view-opciones-semilla").hide();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'semilla',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimiento', 'certificacion');
                $.funciones.cargarImagen('semilla', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>
<script type="text/javascript">
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //colorear celdas
        var celdas = ['semillera','nSolicitud','comunidad','localidad','campanha','cultivo','variedad','cSembrada','cProducir'];
        $.funciones.alternarColores(celdas);
        var celdas2 = ['cantidadS','lote','fecha','plantas','cAnterior','cultAnterior','supParcela','productor','pdf','options'];
        $.funciones.alternarColores(celdas2);
        var opciones = ['editar', 'eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
        //envio de datos
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#semilla").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar el registro?')) {
                $.funciones.mostrarMensaje('info', 'Eliminando registro')
                var id = $("#semilla").val();
                $.post('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'eliminar',
                    pag : 'semilla',
                    id : id
                }, function(data) {
                    if (data == 'OK') {
                        $.funciones.mostrarMensaje('ok', 'Semilla eliminada');

                        setTimeout(function() {
                            var area = $("#control").val();
                            $.post('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'semilla',
                                area : area
                            }, function(data) {
                                $(".post").empty().append(data);
                            });
                        }, 50);
                        $.funciones.ocultarMensaje(5000);
                    } else {
                        $.funciones.mostrarMensaje('error', 'No se puede eliminar la semilla');
                        $.funciones.ocultarMensaje(5000);
                    }
                });
            }
            $("#errores").fadeOut(50);

        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#semilla").val($(this).val());
        }).click(function() {
            var id = $("#semilla").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_semilla',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>