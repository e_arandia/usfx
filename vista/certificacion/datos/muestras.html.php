<?php
include '../vista/dialogos/busquedaAvanzada.html';
?>
<div class="div-filter">
<h2>Muestras de laboratorio</h2>
<div id="calendar" style="width:63.5%;">
    <div class="head" style="height: 3em">
        <div class="htitle" >
            <label style="width:6em;padding-top: 1%;">Numero Campo</label>
            <label style="width:5.5em;padding-top: 1%;">Fecha Recepcion</label>
            <label style="width:10em;padding-top: 1%;">Semillerista</label>
            <label style="width:7.6em;padding-top: 1%;">Cultivo</label>
            <label style="width:8.5em;padding-top: 1%;">Variedad</label>
            <label style="width:5.8em;padding-top: 1%;"></label>
        </div>
    </div>
    <div class="body">
        <?php
        while ($row = DBConnector::objeto()) {
        ?>
        <div class="nSolicitud">
            <label class="nocampoLab"><?php echo $row -> nro_campo; ?></label>
        </div>
        <div class="fecha">
            <label class="fecharLab"><?php echo $dates -> cambiar_formato_fecha($row -> fecha_recepcion); ?> </label>
        </div>
        <div class="productor">
            <label class="productorLab"><?php echo ucwords(utf8_encode($row -> semillerista)); ?></label>
        </div>
        <div class="cultivo">
            <label class="cultivoLab"><?php echo ucfirst($row -> cultivo); ?> </label>
        </div>
        <div class="variedad">
            <label class="variedadLab"> <?php echo utf8_encode(ucfirst($row -> variedad)); ?></label>
        </div>
        <div class="pdf" style="padding-top:1%;width: 5.7em;">
            <a id="<?php echo $row->nro_analisis?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
        <input type="hidden" id="ilab"/>
        <input type="hidden" class="nosolicitud" name="nosolicitud" value="<?php echo $row->nro_solicitud?>"/>
        <input type="hidden" class="tipoSemilla" name="tipoSemilla" value="<?php echo $row->tipo_semilla?>"/>
        <?php
        }
        ?>        
    </div>
</div>
</div>
<div class="div-new" style="display:none;">
    
</div> 
<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#ilab").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#ilab").val('');
        }).on("click", function() {
            var id = $("input#ilab").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&&pag=laboratorio&id='+id, '_blank');
            win.focus();
        });
        var celdas = ['nSolicitud', 'fecha', 'productor', 'cultivo', 'variedad','pdf'];
        $.funciones.alternarColores(celdas);
        //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'new',
                pag : 'muestras',
                area : 1,
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });        
        $.funciones.calendario('input', 'fecha-filter');
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'muestras',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nocampo, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label>" + json.nocampo[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label>" + json.fecha_recepcion[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\"><label>" + json.variedad[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['nSolicitud', 'fecha', 'productor', 'cultivo', 'variedad'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'muestras',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nocampo, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"nSolicitud\"><label>" + json.nocampo[index] + "</label> </div>";
                            div += "<div class=\"fecha\"><label>" + json.fecha_recepcion[index] + "</label> </div>";
                            div += "<div class=\"productor\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"cultivo\"><label>" + json.cultivo[index] + "</label> </div>";
                            div += "<div class=\"variedad\"><label>" + json.variedad[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['nSolicitud', 'fecha', 'productor', 'cultivo', 'variedad'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            }
        });
        //boton de busqueda avanzada
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '0'
            });
        });
        //boton nuevo
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({
                'float' : 'right',
                'margin-top' : '6%'
            });
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'muestra',
                    opt : 'new'
                }
            }).done(function(html) {

                $.funciones.cargarImagen('seguimiento', 'certificacion');
                $.funciones.cargarImagen('laboratorio', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();

            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var idlab;
        //ingresar resultados de analisis
        $("input[name=stadoLabIn]").mouseover(function() {
            var cssObj = {
                'text-decoration' : 'underline'
            };
            $(this).css(cssObj);
            $(".ilab").val($(this).val());
        }).mouseleave(function() {
            var cssObj = {
                'text-decoration' : 'none'
            };
            $(this).css(cssObj);
            $(".ilab").empty();
        }).click(function() {
            idlab = $(".ilab").val();
            tipo = $(".tipoSemilla").val();
            noSolicitud = $(".nosolicitud").val();
            $.post('control/index.php', {
                mdl : 'laboratorio',
                opt : 'ver',
                pag : 'inMuestra',
                id : idlab,
                tipo : tipo,
                nro : noSolicitud
            }, function(showResponse) {
                if (showResponse != 'Error')
                    $(".post").empty().prepend(showResponse);
                else {
                    $("span.mensaje").append("No se encuentra el numero de campo seleccionado");
                    $("#errores").show();
                }
            });
        });
        //ver resultados de analisis
        $("input[name=stadoLabOut]").mouseover(function() {
            var cssObj = {
                'text-decoration' : 'underline'
            };
            $(this).css(cssObj);
            $(".ilab").val($(this).val());
        }).mouseleave(function() {
            var cssObj = {
                'text-decoration' : 'none'
            };
            $(this).css(cssObj);
            $(".ilab").empty();
        }).click(function() {
            idlab = $(".ilab").val();
            $.post('control/index.php', {
                mdl : 'laboratorio',
                opt : 'ver',
                pag : 'muestras',
                id : idlab
            }, function(showResponse) {
                if (showResponse != 'Error')
                    $(".post").empty().prepend(showResponse);
                else {
                    $("span.mensaje").append("No se encuentra el numero de campo seleccionado");
                    $("#errores").show();
                }
            });
        });
    })
</script>