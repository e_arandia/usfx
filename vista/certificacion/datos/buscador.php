
<?php
if (count($semillera) > 0 ){
$width = "width: 97.9%";
$width_td = "width: 67.6%";
$proceso = "Certificaci&oacute;n";

?>
<h2>Estado Proceso de <?echo $proceso?></h2>
<div id="calendar" style="border-bottom: none">
	<div id="table" class="striped collapsible filterable" style="width: 129%">
		<div id="thead">
			<div class="tr">
				<div style="width: 12em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
    			    Semillera
				</div>
				<div style="width: 3.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
                    N&uacute;mero campo
                </div>
                <div style="width: 3.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
                    Cultivo 
                </div>
				<div style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Solicitud
				</div>
				<div style="width: 6.5em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Semilla Sembrada
				</div>
				
				<div style="width: 6.1em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Superficie
				</div>
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Inspeccion
				</div>				
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Hoja de Cosecha
				</div>
				<div style="width: 5.9em;border-right: 1px solid #66CC33;text-align: center;font-weight: bold" class="th">
					Laboratorio
				</div>
				<div style="width: 6.9em;text-align: center;font-weight: bold" class="th">
					Semilla Producida
				</div>
			</div>
		</div>
		<?php
		if (!empty($semillera)){
		$indice = 0;
		$aux = 0;
        
		foreach ($semillera as $key => $semilleras) {
		?>
		<div class="tbody">
			<div class="tr">
				<div class="th" style="<?php echo $width; ?>; height: 5%;background-color:#80b744;font-weight: bold">
					<?php
                    echo $semilleras;
                    #semillera
                    $counter = 0;
					?>
				</div>
			</div>
			<?php
while ($counter < $cantidad[$aux]){
			?>
			<div class="tr control" style="width: 99%">
				<div class="td productor"style="border-left:none;width: 12em;">
					<?php echo $productor[$indice]; ?>
				</div>
				<div class="td semilla"style="border-left:none;width:5em;text-align: center;">
                    <?php
                    
                    Semilla::getCampoCultivoById($id_productor[$indice]);
                    $fila = DBConnector::objeto();
                    #echo count($fila);
                    $nocampo = (DBConnector::filas()>0) ? $fila -> nro_campo : '0';
                    echo $nocampo;

                    echo '</div>';
                    echo "<div class='td cultivo' style='border-left:none;width:3em;text-align: center;'>";
                    $cultivos = (DBConnector::filas()) ? $fila -> cultivo : '-';
                    echo $cultivos;
                     ?>
                </div>
				<div class="td estado" style="border-left:none;<?php echo $width_td; ?>">
					<?php
                    $estado = intval($etapa[$indice]);
                    switch ($estado) {
                        case 0 :
                            echo "<img src='images/0.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                        case 1 :
                            echo "<img src='images/1.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                        case ($estado==2 || $estado==3 || $estado==4 || $estado==5) :
                            echo "<img src='images/2.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                        case ($estado==6 || $estado==7 || $estado==8) :
                            echo "<img src='images/3.png' class='clickable' style='position: relative;top: -.2%;'>";
                            break;
                        case 9 :
                            echo "<img src='images/4.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                        case 10 :
                            echo "<img src='images/5.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                        case ($estado==11 || $estado==12) :
                            echo "<img src='images/6.png' class='clickable' style='position: relative;top: 0.5em;'>";
                            break;
                    }
					?>
				</div>
			</div>
			<?php
            $counter++;
            $indice++;
            }
            $aux++;
			?>
		</div>
		<?php }
            }
		?>
	</div>
</div>
<?php
}else{
echo "<div id='advertencia' class='ui-widget'>
<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;width: 35em'>
<p>
<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em; margin-top: 0.5em;'></span>
No existen datos
</p>
</div>
</div>";
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("div.tbody >div.productor:odd,div.body >div.semilla:odd,div.body >div.semilla:odd,div.body >div.cultivo:odd,div.body >div.estado:odd").css('background-color', '#edffd4');      
        $('#thead').scrollFix();

        $("div.odd").css("width", "98.2%");
        $("div.even").css("width", "98.2%");
        
        $('#table.sortable').each(function() {
            var $table = $(this);
            $table.alternateRowColors();
            $('.th', $table).each(function(column) {
                var $header = $(this);
                var findSortKey;
                if ($header.is('.sort-alpha')) {
                    findSortKey = function($cell) {
                        return $cell.find('.sort-key').text().toUpperCase() + ' ' + $cell.text().toUpperCase();
                    };
                } else if ($header.is('.sort-numeric')) {
                    findSortKey = function($cell) {
                        var key = $cell.text().replace(/^[^\d.]*/, '');
                        key = parseFloat(key);
                        return isNaN(key) ? 0 : key;
                    };
                } else if ($header.is('.sort-date')) {
                    findSortKey = function($cell) {
                        return Date.parse('1 ' + $cell.text());
                    };
                }

                if (findSortKey) {
                    $header.addClass('clickable').hover(function() {
                        $header.addClass('hover');
                    }, function() {
                        $header.removeClass('hover');
                    }).click(function() {
                        var sortDirection = 1;
                        if ($header.is('.sorted-asc')) {
                            sortDirection = -1;
                        }
                        var rows = $table.find('.tbody > .tr').get();
                        $.each(rows, function(index, row) {
                            var $cell = $(row).children('.td').eq(column);
                            row.sortKey = findSortKey($cell);
                        });
                        rows.sort(function(a, b) {
                            if (a.sortKey < b.sortKey)
                                return -sortDirection;
                            if (a.sortKey > b.sortKey)
                                return sortDirection;
                            return 0;
                        });
                        $.each(rows, function(index, row) {
                            $table.children('.tbody').append(row);
                            row.sortKey = null;
                        });
                        $table.find('.th').removeClass('sorted-asc').removeClass('sorted-desc');
                        if (sortDirection == 1) {
                            $header.addClass('sorted-asc');
                        } else {
                            $header.addClass('sorted-desc');
                        }
                        $table.find('.td').removeClass('sorted').filter(':nth-child(' + (column + 1) + ')').addClass('sorted');
                        $table.alternateRowColors();
                        $table.trigger('repaginate');
                    });
                }
            });
        });
        //});

        //$(document).ready(function() {
        $('#table.paginated').each(function() {
            var currentPage = 0;
            var numPerPage = 10;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('.tbody .tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            var numRows = $table.find('.tbody .tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<div class="pager"></div>');
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage : page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }
            $pager.insertBefore($table).find('span.page-number:first').addClass('active');
        });

        //});

        //$(document).ready(function() {
        $('#table.striped').bind('stripe', function() {
            $('.tbody', this).each(function() {
                $(this).find('.tr:not(:has(.th))').filter(function() {
                    return this.style.display != 'none';
                }).removeClass('odd').addClass('even').filter(function(index) {
                    return (index % 6) < 3;
                }).removeClass('even').addClass('odd');
            });
        }).trigger('stripe');
        var collapseIcon = 'images/bullet_toggle_minus.png';
        var collapseText = 'ocultar esta seccion';
        var expandIcon = 'images/bullet_toggle_plus.png';
        var expandText = 'mostrar esta seccion';
        $('#table.collapsible .tbody').each(function() {
            $('.odd, .even').css('display', 'none');
            var $section = $(this);
            $section.addClass('collapsed');
            $('<img />').attr('src', expandIcon).attr('alt', expandText).prependTo($section.find('.th')).addClass('clickable').click(function() {

                if ($section.is('.collapsed')) {
                    $section.removeClass('collapsed').find('.tr:not(:has(.th))').fadeOut('fast', function() {
                        $(this).css('display', 'block');
                    });
                    $(this).attr('src', collapseIcon).attr('alt', collapseText);
                } else {
                    $section.addClass('collapsed').find('.tr:not(:has(.th)):not(.filtered)').fadeIn('fast');
                    $(this).attr('src', expandIcon).attr('alt', expandText);
                    $('#table.collapsible .tbody>.control').css('display','none');

                }
                $section.parent().trigger('stripe');
            });
        });

        $('#table.filterable').each(function() {
            var $table = $(this);

            $table.find('.th').each(function(column) {
                if ($(this).is('.filter-column')) {
                    var $filters = $('<div class="filters"></div>');
                    $('<h3></h3>').text('Filter by ' + $(this).text() + ':').appendTo($filters);

                    $('<div class="filter">all</div>').click(function() {
                        $table.find('.tbody .tr').removeClass('filtered');
                        $(this).addClass('active').siblings().removeClass('active');
                        $table.trigger('stripe');
                    }).addClass('clickable active').appendTo($filters);

                    var keywords = {};
                    $table.find('.td:nth-child(' + (column + 1) + ')').each(function() {
                        keywords[$(this).text()] = $(this).text();
                    });

                    $.each(keywords, function(index, keyword) {
                        $('<div class="filter"></div>').text(keyword).bind('click', {
                            key : keyword
                        }, function(event) {
                            $('.tr:not(:has(.th))', $table).each(function() {
                                var value = $('.td', this).eq(column).text();
                                if (value == event.data['key']) {
                                    $(this).removeClass('filtered');
                                } else {
                                    $(this).addClass('filtered');
                                }
                            });
                            $(this).addClass('active').siblings().removeClass('active');
                            $table.trigger('stripe');
                        }).addClass('clickable').appendTo($filters);
                    });

                    $filters.insertBefore($table);
                }
            });
        });

    })
</script>