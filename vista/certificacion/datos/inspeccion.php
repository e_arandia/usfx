<?php

$width = (($area == 10 || $area == 1 || $area == 2)&& $edit==1110)  ? "'width:173.1%'" : "'width:161%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);

//ejecutar consulta sql
DBConnector::ejecutar($blocks);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Inspecciones de certificación</h2>";
} 
?>
<div class="div-filter">
    <h3 style="margin-bottom: 1%;font-size: large;">
        <span style="font-weight: bolder;color:#5cb85c;">MOSTRAR : </span>
    </h3>
    
<div id="view-opciones-inspecciones" style="width:101%;">
    
    <input type="radio" id="semilla-cultivo" name="semilla" value="1"  checked="checked" />
    <label for="semilla-cultivo" style="width: 5%">Cultivo</label>
    <input type="radio" id="semilla-plantines" name="semilla" value="3"/>
    <label for="semilla-plantines" style="width: 7%;">Plantines</label>
</div>
<div id="calendar" style=<?php echo $width;?>>
	<div class="head" style="height: 3.2em">
		<div class="htitle" >
		    <label style="width:17em;padding-top: 1%;">Semillera</label>
			<label style="width:17.3em;padding-top: 1%;">Semillerista</label>
			<label style="width:10em;padding-top: 1%;">Comunidad</label>
			<label style="width:6em;padding-top: 1%;">Numero campo</label>
			<label style="width:8em;padding-top: 1%;">Inspeccion Vegetativa</label>
			<label style="width:10em;padding-top: 1%;">Observacion</label>
			<label style="width:8em;padding-top: 1%;">Inspeccion Floracion</label>
			<label style="width:10em;padding-top: 1%;">Observacion</label>
			<label style="width:8em;padding-top: 1%;">Inspeccion Pre-cosecha</label>
			<label style="width:10em;padding-top: 1%;">Observacion</label>
			<label style="width:6em;padding-top: 1%;">Datos PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php  }?>
		</div>
	</div>
	<div class="body">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera" style="width: 17em;">
            <label> <?php echo utf8_encode($datos -> semillera); ?></label>
        </div>
		<div class="productor" style="width: 17.3em;">
			<label><?php    echo utf8_encode($datos -> semillerista);?></label>
		</div>
		<div class="comunidad">
            <label class="datoo"> <?php echo ucfirst($datos -> comunidad); ?></label>
        </div>
		<div class="sInscrita nro" style="width: 6em;">
            <label> <?php   echo $datos -> nro_campo;?></label>
        </div>
		<div class="fecha pri" style="width: 8em">
			<label> <?php
			$fecha = (!empty($datos -> primera)) ? $dates -> cambiar_formato_fecha($datos -> primera) : '';
			echo $fecha;
				?></label>
		</div>
		<?php
		  $css = (strlen($datos -> observacion_1)>20)?"overflow-y: scroll;":"";
		?>
		<div class="fecha obs1" style="width: 10em;<?php echo $css;?>">
            <label> 
                <?php
            echo $datos -> observacion_1;
                ?>
            </label>
        </div>
		<div class="fecha seg" style="width: 8em">
			<label><?php
			
			$fecha = ($datos -> segunda != '0000-00-00') ? $dates -> cambiar_formato_fecha($datos -> segunda) : '';
			echo $fecha;
				?></label>
		</div>
		<?php
          $css2 = (strlen($datos -> observacion_2)>20)?"overflow-y: scroll;":"";
        ?>
		<div class="fecha obs2" style="width: 10em;<?php echo $css2;?>">
            <label> <?php
            //var_dump($datos->observacion_2);
            echo ($datos -> observacion_2);
                ?></label>
        </div>
		<div class="fecha ter" style="width: 8em">
			<label><?php
			$fecha = ($datos -> tercera != '0000-00-00') ? $dates -> cambiar_formato_fecha($datos -> tercera) : '';
			echo $fecha;
				?>
		</div>
		<?php
          $css3 = (strlen($datos -> observacion_3)>20)?"overflow-y: scroll;":"";
        ?>
		<div class="fecha obs3" style="width: 10em;<?php echo $css3;?>">
            <label> <?php
            echo ($datos -> observacion_3);
                ?></label>
        </div>
        <div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_superficie; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>        
		<?php
        if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
		<div class="sistema options" style="height: 28px; width: 8.5em;">
		      <?php
          if (($area == 1 || $area == 2)&& $edit==1110){
          ?>
			<div>
				<input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_inspeccion;?>" />
			</div>
			<?php
            }else{
            ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_inspeccion;?>" />
            </div>
            <?php
            }
            if ($area==10){?>  
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2.3em;right: -1em;" value="<?php echo $datos -> id_inspeccion;?>" />
			</div>
			<?php } ?>
		</div>
		<?php
		}
		}
		?>
		<input id="inspeccion" type="hidden"/>
		<input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>

<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script>
    $(document).ready(function(){
        $.funciones.opcionesInspeccionCertifica();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //opciones de filtro
        $("#view-opciones-inspecciones").buttonset();
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#inspeccion").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#inspeccion").val('');
        }).on("click", function() {
            var id = $("input#inspeccion").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=inspeccion&id='+id);
            win.focus();
        });
//busqueda por filtro
        $("input#filter").on("keyup", function() {
            var q = $(this).val();
            if (q != '') {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'filtro',
                    opc : 'inspeccion',
                    search : q
                }, function(json) {
                    $("div.body").empty();
                    var div = '';
                    $.each(json.nosolicitud, function(index, value) {
                        if (index < json.total) {
                            div += "<div class=\"semillera\"><label>"+json.semillera[index]+"</label> </div>";
                            div += "<div class=\"productor\" style=\"width:17.3em\"><label>" + json.semillerista[index] + "</label> </div>";
                            div += "<div class=\"sInscrita nro\" style=\"width:6em\"><label>" + json.nro_campo[index] + "</label> </div>";
                            div += "<div class=\"fecha pri\" style=\"width:8em\"><label>" + json.primera[index] + "</label> </div>";
                            div += "<div class=\"fecha obs1\" style=\"width:10em\"><label>" + json.observacion_1[index] + "</label> </div>";
                            div += "<div class=\"fecha pri\" style=\"width:8em\"><label>" + json.segunda[index] + "</label> </div>";
                            div += "<div class=\"fecha obs2\" style=\"width:10em\"><label>" + json.observacion_2[index] + "</label> </div>";
                            div += "<div class=\"fecha pri\" style=\"width:8em\"><label>" + json.tercera[index] + "</label> </div>";
                            div += "<div class=\"fecha obs3\" style=\"width:10em\"><label>" + json.observacion_3[index] + "</label> </div>";
                            //div += "<div class=\"sistema options\"><label>" + json.options[index] + "</label> </div>";
                        } else {
                            return false;
                        }
                    });

                    $("div.body").append(div);
                    var celdas = ['semillera','productor','comunidad','nro','pri','obs1','seg','obs2','ter','obs3','pdf','options'];
                    $.funciones.alternarColores(celdas);
                    $("div.body").show();
                });
            } else {
                $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'vinspecciones',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            }); 
            }
        });
        //boton de busqueda
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0em'});
        });
        //boton nuevo
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'inspeccion',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimiento', 'certificacion');
                $.funciones.cargarImagen('inspeccion', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>
<script type="text/javascript">
    //$(document).ready(cargarSolicitudes('certificacion', 'ver', 'solicitud', 1, 0));
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var celdas = ['semillera','productor','comunidad','nro','pri','obs1','seg','obs2','ter','obs3','pdf'];
        $.funciones.alternarColores(celdas);
        var opciones = ['editar', 'eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
	})
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/**eliminar solicitud*/
		$("input[name=del]").mouseover(function() {
			$("#inspeccion").val($(this).val());
		}).click(function() {
			if(confirm('Esta seguro de eliminar el registro?')) {
				var imagen = '<img src="images/loader_3a.gif" />';
				$('.post').block({
					theme : true,
					title : 'Sistema CERFIS',
					message : imagen + '<div style="text-align:center">Eliminando registro... <br>Por favor espere</div>'
				});
				var id = $("#inspeccion").val();
				$.post('control/certificacion/certificacion.ctrl.php', {
					page : 'eliminar',
					tbl : 'inspeccion',
					id : id
				}, function(data) {
					if(data == 'OK') {
						$('.post').unblock({
							theme : true,
							title : 'Sistema CERFIS',
							message : "<div>El registro fue eliminado exitosamente.</div><div style='margin-left:25%'> <img src='images/loader_4.gif'/></div>",
							timeout: 2000
						});
						var area = $("#control").val();
						$.post('control/usuario/usuario.ctrl.php', {
							page : 'vinspeccion',
							area : area
						}, function(data) {
							$(".post").empty().append(data);
						});
						$(".post").append(data);
					}
				});
			}
		});
		/**actualizar datos usuario*/
		$("input[name=upd]").mouseover(function() {
			$("#inspeccion").val($(this).val());
		}).click(function() {
			var id = $("#inspeccion").val();
			$.post('control/certificacion/certificacion.ctrl.php', {
				page : 'actualizar_inspeccion',
				id : id
			}, function(data) {
				$(".post").empty().append(data);
			});
		});

		$("input:image").mouseleave(function() {
			$("#id").empty();
		});
	})
</script>
