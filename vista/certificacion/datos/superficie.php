<?php

$width = (($area == 10 || $area == 1 || $area == 2) && $edit == 1110) ? "'width:120.6%'" : "'width:108.3%'";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
//ejecutar consulta sql
DBConnector::ejecutar($blocks);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Superficies de certificación:</h2>";
} 
?>

<div class="div-filter">

<div id="calendar" style=<?php echo $width; ?>>
	<div class="head" style="height: 2em">
		<div class="htitle" >
		    <label style="width:17em;padding-top: 1%;">Semillera</label>
			<label style="width:10em;padding-top: 1%;">Semillerista</label>
			<label style="width:10em;padding-top: 1%;">Comunidad</label>
			<label style="width:6em;padding-top: 1%;">Numero campo</label>
			<label style="width:6em;padding-top: 1%;">Superficie Inscrita</label>
			<label style="width:7em;padding-top: 1%;">Superficie Rechazada</label>
			<label style="width:6em;padding-top: 1%;">Superficie Retirada</label>
			<label style="width:6em;padding-top: 1%;">Superficie Aprobada</label>
			<label style="width:6em;padding-top: 1%;">Datos PDF</label>
			<?php if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
			?>
			<label style="width:8.4em;padding-top: 1%;">Opciones </label>
			<?php } ?>
		</div>
	</div>
	<div class="body" style="width: 99.7%;">
		<?php
while ($datos = DBConnector::objeto()){
		?>
		<div class="semillera"  style="width: 17em;">
            <label> <?php echo utf8_encode($datos -> semillera); ?></label>
        </div>
		<div class="productor" style="width: 10em;">
			<label><?php  echo utf8_encode($datos -> semillerista); ?></label>
		</div>
		<div class="comunidad">
            <label class="datoo"> <?php echo ucfirst($datos -> comunidad); ?></label>
        </div>
		<div class="sInscrita nro" style="width: 6em;">
            <label> <?php   echo $datos -> nro_campo; ?></label>
        </div>
		<div class="sInscrita ins" style="width: 6em;">
			<label> <?php	echo $datos -> inscrita; ?></label>
		</div>
		<div class="sInscrita rec" style="width: 7em;">
			<label> <?php	echo $datos -> rechazada; ?></label>
		</div>
		<div class="sInscrita ret" style="width: 6em;">
			<label> <?php	echo $datos -> retirada; ?></label>
		</div>
		<div class="sInscrita apr" style="width: 5.8em;">
			<label> <?php  echo $datos -> aprobada; ?></label>
		</div>
		<div class="pdf" style="padding-top:1%;width: 6em;">
            <a id="<?php echo $datos -> id_superficie; ?>" class="glyphicon glyphicon-print btn-lg" style="cursor:pointer;padding:0"></a>
        </div>
		<?php
            if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
		?>
		<div class="sistema options" style="height: 27px; width: 8.5em;">
		  <?php
            if (($area == 1 || $area == 2)&& $edit==1110){
          ?>
			<div>
				<input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos -> id_superficie; ?>" />
			</div>
			<?php
            }else{
            ?>
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos -> id_superficie; ?>" />
            </div>
            <?php
            }
            if ($area==10){?>  
			<div>
				<input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2.3em;right: -1em;" value="<?php echo $datos -> id_superficie; ?>" />
			</div>
			<?php } ?>
		</div>
		<?php
        }
        }
		?>
		<input id="superficie" type="hidden"/>
		<input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
	</div>
</div>
</div>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function(){
        $.funciones.bloqueSuperficie();
        pagina_actual = $('input#view-pagina').val();
        $('div#paginacion>a[id='+pagina_actual+']').addClass('active');
    });
</script>   
<script type="text/javascript">
    $(document).ready(function() {
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#superficie").val($("a.glyphicon-print").attr("id"));
        }).on("mouseleave", function() {
            $("input#superficie").val('');
        }).on("click", function() {
            var id = $("input#superficie").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&pag=superficie&id='+id, '_blank');
            win.focus();
        });
        //busqueda por filtro
        $("input#filter").on("keyup", function() {
            $.funciones.buscarSuperficiesRegistradas();
        });
        //boton de busqueda avanzada
        //boton de busqueda
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        //boton nuevo
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'superficie',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimiento', 'certificacion');
                $.funciones.cargarImagen('superficie', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>
<script type="text/javascript">
    function cargarSolicitudes(mdl, opt, pag, area, limit) {
        var url = "control/index.php";
        var limit = limit * 15;
        $.get(url, {
            mdl : mdl,
            opt : opt,
            pag : pag,
            area : area,
            limit : limit
        }, function(response) {
            $(".post").html(response);
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //alternar colores
        var celdas = ['semillera','productor','comunidad', 'nro', 'ins', 'rec', 'ret', 'apr','pdf'];
        $.funciones.alternarColores(celdas);
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#superficie").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar el registro?')) {
                $.funciones.mostrarMensaje('info', 'Eliminando registro');
                var id = $("#superficie").val();
                $.post('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'eliminar',
                    pag : 'superficie',
                    id : id
                }, function(data) {
                    if (data == 'OK') {
                        $.funciones.mostrarMensaje('ok', 'Superficie Eliminada');
                        var area = $("#control").val();
                        $.post('control/index.php', {
                            mdl : 'certificacion',
                            opt : 'ver',
                            pag : 'superficie',
                            area : area
                        }, function(data) {
                            $(".post").empty().append(data);
                        });
                        $.funciones.ocultarMensaje(5000);
                    } else {
                        $.funciones.mostrarMensaje('error', 'No se elimino la superficie');
                        $.funciones.ocultarMensaje(5000);
                    }
                });
            }
        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#superficie").val($(this).val());
        }).click(function() {
            var id = $("#superficie").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_superficie',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>