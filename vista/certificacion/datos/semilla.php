<?php

$width = (($area == 10 || $area == 1) && $edit == 1110) ? "'width:185.7%'" : "'width:173.9%'" ;
$widthParcela = (($area == 10 || $area == 1) && $edit == 1110) ? "6em" : "6.2em";

//total de registros
$total = DBConnector::filas();
//nro de enlaces
$plinks = ceil($total / $nroRegistros);
#echo $blocks;
//ejecutar consulta sql
DBConnector::ejecutar($blocks);

if ($area!=10){
    include '../vista/dialogos/busquedaAvanzada.html';
}else{
    echo "<h2>Lista de semillas segun:</h2>";
} 
?>

<div class="div-filter">
    <h3 style="margin-bottom: 1%;font-size: large;">
        <span style="font-weight: bolder;color:#5cb85c;">MOSTRAR : </span>
    </h3>
    
<div id="view-opciones-semilla" style="width:101%;">
    
    <input type="radio" id="semilla-cultivo" name="semilla" value="1"  checked="checked" />
    <label for="semilla-cultivo" style="width: 5%">Cultivo</label>
    <input type="radio" id="semilla-acondicionamiento" name="semilla" value="2"/>
    <label for="semilla-acondicionamiento" style="width: 16%;">Acondicionamiento</label>
    <input type="radio" id="semilla-plantines" name="semilla" value="3"/>
    <label for="semilla-plantines" style="width: 7%;">Plantines</label>
</div>
<div id="calendar" style=<?php echo $width; ?>>
    <div class="head" style="height: 3.2em">
        <div class="htitle" >
            <label style="width:12em;padding-top: 1%;">Semillera</label>
            <label style="width:10em;padding-top: 1%;">Semillerista</label>
            <label style="width:8em;padding-top: 1%;">Comunidad</label>
            <?php
            if ($area == 10 || $area == 1){
            ?>
            <label style="width:6em;padding-top: 1%;">N&uacute;mero Campo</label>
            <label style="width:9em;padding-top: 1%;">Campa&ntilde;a</label>
            <?php
            }
            ?>
            <label style="width:7.5em;padding-top: 1%;">Cultivo</label>
            <label style="width:8.5em;padding-top: 1%;">Variedad</label>
            <?php
            if ($area == 10 || $area == 1){
            ?>
            <label style="width:10em;padding-top: 1%;">Categoria sembrada</label>
            <?php
            }
            ?>
            <label style="width:9em;padding-top: 1%;">Categoria a producir</label>
            <?php
            if ($area == 10 || $area == 1){
            ?>
            <label style="width:7.8em;height:3em;padding-top: 0.6em;">Cantidad de Semilla(Kg.)</label>
            <?php
            }
            ?>
            <!--<label style="width:8em;padding-top: 1%;">Numero lote</label>-->
            <?php
        if ($area == 10 || $area == 1){
        ?>          
            <label style="width:7em;padding-top: 1%;">Fecha Siembra</label>         
            <label style="width:6em;padding-top: 1%;">Plantas por hectarea</label>
            <label style="width:6em;padding-top: 1%;">Cultivo Anterior</label>
            <label style="width:<?php echo $widthParcela; ?>;padding-top: 1%;">Superficie parcela</label>
            <label style="width:6em;padding-top: 1%;">Datos PDF</label>
            <?php
            }
            if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
            ?>
            <label style="width:8.4em;padding-top: 1%;">Opciones </label>
            <?php } ?>
        </div>
    </div>
    <div class="body">
        <?php
while ($datos = DBConnector::objeto()) {
        ?>
        <div class="semillera">
            <label class="datoo"> <?php echo utf8_encode($datos->semillera); ?></label>
        </div>
        <div class="productor">
            <label class="datoo"> <?php   echo utf8_encode($datos->nombre_apellido); ?></label>
        </div>
        <div class="comunidad" style="width: 8em">
            <label class="datoo"> <?php   echo ucfirst($datos->comunidad); ?></label>
        </div>
        <?php
            if ($area == 10 || $area == 1){
            ?>
        <div class="nSolicitud">
            <label class="datoo"> <?php echo $datos->nro_campo; ?></label>
        </div>
        <div class="campanha">
            <label class="datoo"> <?php echo ucfirst($datos->campanha); ?></label>
        </div>
        <?php
        }
        ?>
        <div class="cultivo">
            <label class="datoo"> <?php echo $datos->cultivo; ?></label>
        </div>
        <div class="variedad">
            <label class="datoo"  style="text-transform:capitalize"> <?php  echo ($datos->variedad); ?></label>
        </div>
        <?php
        if ($area == 10 || $area == 1){
        ?>
        <div class="cSembrada">
            <label class="datoo"> <?php  echo $datos->categoria_sembrada; ?></label>
        </div>
        <?php
        }
        ?>
        <div class="cProducir">
            <label class="datoo"> <?php 
                echo $datos->categoria_producir;
                ?>
                </label>
        </div>
        <?php
        if ($area == 10 || $area == 1){
        ?>      
        <div class="cantidadS">
            <label class="datoo"> <?php echo number_format($datos->cantidad_semilla,0,".",","); ?></label>
        </div>
        <?php
        }
        ?>
        <!--<div class="lote" style="width: 8em">
            <label class="datoo"> <?php
            $lote = !empty($datos->nro_lote) ? $datos->nro_lote : '-';
            #echo $lote;
                ?></label>
        </div>-->
        <?php
        if ($area == 10 || $area == 1){
        ?>
        <div class="fecha" style="width: 7em">
            <label class="datoo"> <?php  echo $dates -> cambiar_formato_fecha($datos->fecha_siembra); ?></label>
        </div>      
        <div class="plantas" >
            <label class="datoo"> <?php echo number_format($datos->plantas_hectarea); ?></label>
        </div>
        <div class="sParcela cultAnterior" style="width: 6em">
            <label class="datoo"> <?php   echo $datos->cultivo_anterior; ?></label>
        </div>
        <div class="sParcela supParcela" style="width: 6em">
            <label class="datoo"> <?php echo $datos->inscrita; ?></label>
        </div>
        <div style="padding-top:1%;width: 6em;" class="pdf">
            <a id="<?php echo $datos->id_solicitud;?>"style="cursor:pointer;padding:0" class="glyphicon glyphicon-print btn-lg"></a>
        </div>
        <?php
        }
if (($area == 10 || $area == 1 || $area == 2)&& $edit==1110){
        ?>
        <div class="sistema options" style="height: 28px; width: 8.3em;">
          <?php
          if (($area == 1 || $area == 2)&& $edit==1110){
          ?>  
            <div>
                <input alt="editar" name="upd" type="image" src="images/editar22x22.png" style="position: relative;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
            <?php
            }else{
            ?>
            <div>
                <input name="upd" type="image" src="images/editar22x22.png" style="position: relative;right: 2em;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
       <?php
    }
    if ($area==10){
       ?>        
            <div>
                <input name="del" type="image" src="images/eliminar24x24.png" style="position: relative;top: -2.3em;right: -1em;" value="<?php echo $datos->id_solicitud; ?>" />
            </div>
        <?php
        }
        ?>  
        </div>
        <?php
        }
        }
        ?>
        <input id="semilla" type="hidden"/>
        <input id="view-pagina" type="hidden" value="<?php echo $page; ?>"/>        
        <input id="control" type="hidden" value="<?php echo $area; ?>" />
    </div>
</div>
</div>
<div id="paginacion" style="margin-left: 30%;width: 78%;">
    <?php 
        include 'paginar.php';
    ?>
</div> 
<script type="text/javascript">
    $(document).ready(function(){
        $.funciones.bloqueSemilla();
        pagina_actual = $('input#view-pagina').val();
        $('div#paginacion>a[id='+pagina_actual+']').addClass('active');
        $.funciones.opcionesSemillaCertifica();
    });
</script>   
<script type="text/javascript">
    $(document).ready(function() {
        //parametros de busqueda
        $('span#search-advice').css('letter-spacing','0.3px').empty().text('Escribir semillera,semillerista,comunidad,cultivo ó variedad');
        //opciones de filtro
        $("#view-opciones-semilla").buttonset();
        $("a.glyphicon-print").on("mouseenter", function() {
            $("input#semilla").val($(this).attr("id"));
        }).on("mouseleave", function() {
            $("input#semilla").val('');
        }).on("click", function() {
            var id = $("input#semilla").val();
            var win = window.open('control/index.php?mdl=informe&opt=pdf&area=certifica&pag=semilla&id='+id, '_blank');
            win.focus();
        });
        edt = $(".crud").val();
        if (edt == 1110){
            $("label:contains('Cantidad de Semilla')").css('height','3em');
        }else{
            $("label:contains('Cantidad de Semilla')").css('height','2.9em');
        }
        //busqueda por filtro
        $('input#filter').on('keyup', function () {
            $.funciones.buscarSemillasRegistradas();
        });

        //boton de busqueda
        $("button#btn-filter").on("click", function() {
            $("div.informar").slideUp();
            $("div.div-new").hide();
            $("div.div-filter").slideDown();
            $("form#filter-box").slideToggle();            
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'0'});
        });
        //boton nuevo
        $("button#btn-new").on("click", function() {
            $(this).fadeOut();
            $("div#view-opciones-semilla").hide();
            $("div.div-new").fadeIn();
            $("div.div-filter").hide();
            $("form#filter-box").slideUp();
            $("div#btn-new-filter").css({'float' : 'right','margin-top':'6%'});    
            $.ajax({
                data : {
                    mdl : 'certificacion',
                    pag : 'semilla',
                    opt : 'new'
                },
                beforeSend : function() {
                    $.funciones.mostrarMensaje('wait', 'Cargando...');
                }
            }).done(function(html) {
                $(".post").empty().append(html);
                $.funciones.cargarImagen('seguimiento', 'certificacion');
                $.funciones.cargarImagen('semilla', 'certificacion');
                $("div.informar").slideDown();
                $("div.filter").slideUp();
                
            });
        });
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //colorear celdas
        var celdas = ['semillera','nSolicitud','comunidad','localidad','campanha','cultivo','variedad','cSembrada','cProducir'];
        $.funciones.alternarColores(celdas);
        var celdas2 = ['cantidadS','lote','fecha','plantas','cAnterior','cultAnterior','supParcela','productor','pdf','options'];
        $.funciones.alternarColores(celdas2);
        var opciones = ['editar', 'eliminar'];
        $.funciones.alternarColorOpcionImagen(opciones);
        //envio de datos
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /**eliminar solicitud*/
        $("input[name=del]").mouseover(function() {
            $("#semilla").val($(this).val());
        }).click(function() {
            if (confirm('Esta seguro de eliminar el registro?')) {
                $.funciones.mostrarMensaje('info', 'Eliminando registro')
                var id = $("#semilla").val();
                $.post('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'eliminar',
                    pag : 'semilla',
                    id : id
                }, function(data) {
                    if (data == 'OK') {
                        $.funciones.mostrarMensaje('ok', 'Semilla eliminada');

                        setTimeout(function() {
                            var area = $("#control").val();
                            $.post('control/index.php', {
                                mdl : 'certificacion',
                                opt : 'ver',
                                pag : 'semilla',
                                area : area
                            }, function(data) {
                                $(".post").empty().append(data);
                            });
                        }, 50);
                        $.funciones.ocultarMensaje(5000);
                    } else {
                        $.funciones.mostrarMensaje('error', 'No se puede eliminar la semilla');
                        $.funciones.ocultarMensaje(5000);
                    }
                });
            }
            $("#errores").fadeOut(50);

        });
        /**actualizar datos usuario*/
        $("input[name=upd]").mouseover(function() {
            $("#semilla").val($(this).val());
        }).click(function() {
            var id = $("#semilla").val();
            $.post('control/index.php', {
                mdl : 'certificacion',
                opt : 'form',
                pag : 'upd_semilla',
                id : id
            }, function(data) {
                $(".post").empty().append(data);
            });
        });

        $("input:image").mouseleave(function() {
            $("#id").empty();
        });
    })
</script>