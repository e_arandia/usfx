<h2 class="title">Datos de la Semilla sembrada</h2>

<form id="semilla">
    <?php
    include 'semilleras_productores.html';
    include 'seguimiento.html';
    ?>
    <div id="semright" style="width: 49%;float: left">
        <div>
            <label style="width: 43%">N&uacute;mero de Campo</label>
            <input id="nrocampo" name="nrocampo" type="text" class="special number"  maxlength="5" style="width: 30px"/>
        </div>
        <div>
            <label style="width: 43%">Campa&ntilde;a</label>
            <input id="campania" name="campania" type="text" class="special" maxlength="15" style="width: 70px"/>
        </div>
        <div>
            <label style="width: 43%">Cultivo</label>
            <select size="1" name="cultivo" id="cultivo" class="special" style="width: 150px"></select>
        </div>
        <div>
            <label style="width: 43%">Variedad</label>
            <select size="1" name="variedad" id="variedad" class="special" style="width: 160px"></select>
        </div>
        <div>
            <label style="width:43%">Categor&iacute;a Sembrada</label>
            <select size="1" name="cat_sembrada" id="cat_sembrada" class="special" style="width: 140px"></select>
        </div>
        <div>
            <label style="width: 43%">Categor&iacute;a a Producir</label>
            <select size="1" name="cat_producir" id="cat_producir" class="special" style="width: 140px"></select>
        </div>
    </div>
    <div id="semleft" style="width: 50%;float:right">

        <div>
            <label style="width: 70%">Cantidad de Semilla Empleada (kg)</label>
            <input id="cant_semilla" name="cant_semilla" type="text" class="special number" style="width: 50px"/>
        </div>
        <div>
            <label>Lote Nro</label>
            <input id="lotenro" name="lotenro" type="text" class="special" style="text-transform: uppercase"/>
        </div>
        <div>
            <label style="width: 58%">Fecha de Siembra</label>
            <input id="f_siembra" name="f_siembra" type="text" class="special" readonly="true" style="width: 65px" value="<?php echo date('d-m-Y')?>"/>
        </div>
        <div>
            <label style="width: 58%">Plantas por Hect&aacute;reas</label>
            <input id="plantasha" name="plantasha" type="text" class="special number" style="width: 65px" maxlength="9"/>
        </div>
        <div>
            <label style="width: 58%">Cultivo Anterior</label>
            <select size="1" name="cult_anterior" id="cult_anterior" class="special" style="width: 120px"></select>
        </div>
        <div>
            <label style="width: 58%">Superficie Parcela (Has.)</label>
            <input id="superficie" name="superficie" type="text" class="special number"style="width: 30px" maxlength="5"/>
        </div>
    </div>
    <div>
        <input id="idSolicitud" name="solicitud" type="hidden"/>
        <input id="noparcela" name="noparcela" type="hidden"/>
        <input id="totalparcelas" name="totalparcelas" type="hidden"/>
    </div
    <div>
        <input type="submit" name="submit" class="button-primary ui-state-default ui-corner-all" value="Registrar"/>
    </div>
</form>
<?php
include '../vista/dialogos/plantasPorHectarea.html';
include '../vista/dialogos/cantidadSemillaEmpleada.html';
include '../vista/error/errores.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#errores").css({
            'margin-top' : '56%'
        });

        $("#f_siembra").calendario('f_siembra');
        //campaña segun el mes
        $.funciones.getCampanha('campania');
        //semilleras que realizaron solicitud
        $("select#semillera").append('<option value=""> Cargando...</option>');
        var productor = $('#productor');
        var solicitud = $('input[name=solicitud]');
        $.getJSON('control/index.php', {
            mdl : 'operaciones',
            pag : 'semillera',
            opt : 'all',
            std : 'certifica',
            tbl : 'semilla'
        }, function(json) {
            if (json.length > 0) {
                $('select#semillera').empty().append('<option value="">[ Seleccione semillera ]</option>');
                $.each(json, function(index, value) {
                    $('select#semillera').append('<option value="' + value + '">' + value + '</option>');
                });
            } else {
                $("select#semillera").empty().append('<option value="">No existen semilleras en esta etapa</option>').attr('disabled', 'disabled');
            }
        });

        var productor = $('#productor');
        productor.attr('disabled', 'disabled');

        $("select#semillera").change(function() {
            var semillera = $("select#semillera").find(':selected').val();
            if (semillera != "") {
                var palabras = semillera.split(" ");
                semillera = '';
                semillera = palabras.join("*");
                $("select#productor").append('<option value="">Cargando...</option>');
                $("select#productor").productores(semillera, 'semilla');
            }
        });
        //carga de cultivos disponibles
        $("select#cultivo").append('<option value=""> Cargando...</option>');
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_costos'
        }, function(json) {
            if (json.cultivos.length > 0) {
                $('select#cultivo').empty().append('<option value="">[ Seleccione cultivo ]</option>');
                $.each(json.cultivos, function(index, value) {
                    $('select#cultivo').append('<option value="' + value + '">' + value + '</option>');
                });{
                $("select#cultivo").append('<option value="add_cultivo">Agregar cultivo</option>');}
            }
        });
        //carga de cultivo anterior
        $("select#cult_anterior").append('<option value=""> Cargando...</option>');
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_costos'
        }, function(json) {
            if (json.cultivos.length > 0) {
                $('select#cult_anterior').empty().append('<option value="">[ Seleccionar]</option>');
                $.each(json.cultivos, function(index, value) {
                    if (value != "Acondicionamiento")
                        $('select#cult_anterior').append('<option value="' + value + '">' + value + '</option>');
                });
            }
            $("select#cult_anterior").append('<option value="descanso">Descanso</option>');
        });

        //carga de variedad de un cultivo
        $("select#cultivo").change(function() {
            $("select#variedad").append('<option value=""> Cargando...</option>');
            var cultivo = $(this).find(':selected').val();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'ls_variedad',
                cultivo : cultivo
            }, function(json) {
                if (json.variedades.length > 0) {
                    $('select#variedad').empty().append('<option value="">[ Seleccione variedad ]</option>');
                    $.each(json.variedades, function(index, value) {
                        $('select#variedad').append('<option value="' + value + '">' + value + '</option>');
                    });
                }else{
                    $("select#variedad").empty().append('<option value="">[ Seleccione variedad ]</option>');
                    $("select#variedad").append('<option value="add_variedad">Agregar variedad</option>');  
                }
            });
        });

        //carga de categoria sembrada
        $("select#cat_sembrada").append('<option value=""> Cargando...</option>');
        var cultivo = $("select#cat_sembrada").find(':selected').val();
        $.getJSON('control/index.php', {
            mdl : 'certificacion',
            opt : 'ls_catsembrada'
        }, function(json) {
            if (json.csembrada.length > 0) {
                $('select#cat_sembrada').empty().append('<option value="">[ Seleccionar ]</option>');
                $.each(json.csembrada, function(index, value) {
                    $('select#cat_sembrada').append('<option value="' + value + '">' + value + '</option>');
                });
            }
        });
        //carga categoria a producir
        $("select#cat_sembrada").change(function() {
            var csembrada = $("select#cat_sembrada").find(':selected').val();
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt: 'ls_cproducir',
                csembrada : csembrada
            }, function(json) {
                if (json.cat_free.length > 0) {
                    $('select#cat_producir').empty().append('<option value="">[ Seleccionar ]</option>');                    
                    $.each(json.cat_free, function(index, value) {                       
                         $('select#cat_producir').append('<option value="' + value + '">' + value + '</option>');
                    });
                }
            })
        });
        productor.change(function() {
            var productores = $("#productor").find(':selected').val();
            if (productores != '') {

                var semillera = $("select#semillera").find(':selected').val();
                var aux = '';
                var nombre;
                var apellido;
                var partes = productores.split(" ");
                //console.log(partes.length);
                if (partes.length > 4) {
                    var nombre = partes[0] + ' ' + partes[1];
                    var apellido = partes[2];
                } else {
                    var nombre = partes[0];
                    var apellido = partes[1];
                }

                $(".following").fadeIn('slow');

                var url = 'control/index.php?mdl=operaciones&pag=nroSolicitud&nombre=' + nombre + '&apellido=' + apellido + '&opt=1&tbl=semilla&semillera=' + semillera;
                $.getJSON(url, function(data) {
                    $('#idSolicitud').val(data.iproductor);
                    var id = data.iproductor;
                    $.getJSON('control/index.php', {
                        mdl : 'operaciones',
                        pag : 'parcelas',
                        id : id
                    }, function(data) {
                        $("#totalparcelas").val(data.noparcelas);
                        $("#noparcela").val(data.indice);

                        var temp_comunidades = data.comunidades;
                        var comunidades = temp_comunidades.split(',');

                        var comunidad = $("input#comunidad");
                        comunidad.empty();
                        $(".following div:first").attr('id', 'lista-comunidades');
                        if (comunidades.length != 1) {
                            $("input#comunidad").hide().attr('disabled', 'disabled');
                            $("select#comunidad").empty().show();
                            for (i in comunidades) {
                                $("select#comunidad").append('<option value="' + comunidades[i] + '">' + comunidades[i] + '</option>');
                            }
                        } else {
                            $("select#comunidad").hide(5).attr('disabled', 'disabled');
                            $("input#comunidad").show();
                            comunidad.val(comunidades);
                        }
                    });
                });
            }
        }); 
        //efecto sobre el boton
        $("input[name=send]").fadeTo('fast', 0.5).mouseover(function() {
            $(this).fadeTo('fast', 1);
        }).mouseleave(function() {
            $(this).fadeTo('fast', 0.5);
        });
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cant_semilla").dblclick(function() {
            var superficieParcela;
            var total;
            $("#superficieParcela, #cantidad").empty();
            $('#SemillaEmpleada').dialog({// Dialog
                title : 'Cantidad de Semilla Empleada',
                dialogClass: "no-close",
                resizable : false,
                height : 250,
                width : 300,
                buttons : {
                    "Cacular" : function() {
                        calcularSemilla();
                    },
                    "Aceptar" : function() {
                        var total = $("#semillaEmpleada").val();
                        var supParcela = $("#superficieParcela").val();
                        if (total != '' || !isNaN(total)) {
                            $("#cant_semilla").val(total);
                            $("#superficie").val(superficieParcela);
                        }
                        $(this).dialog("close");
                    },
                    "Cerrar" : function() {
                        $(this).dialog("close");
                    }
                }
            });
            function calcularSemilla() {
                superficieParcela = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($("#superficieParcela").val()) ? $("#superficieParcela").val() : -1;
                var cantidad = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($("#cantidad").val()) ? $("#cantidad").val() : -1;
                total = parseFloat(superficieParcela) * parseFloat(cantidad);
                $("#semillaEmpleada").val(total.toFixed(2));
                //$("#superficie").val(superficieParcela);
            };

        });
        $("#plantasha").dblclick(function() {
            $('#plantasPorHa').dialog({// Dialog
                title : 'Plantaciones por Hect&aacute;rea',
                dialogClass: "no-close",
                resizable : false,
                //height : 280,
                //width : 300,
                buttons : {
                    "Cacular" : function() {
                        calcular();
                    },
                    "Aceptar" : function() {
                        var total = $("#total").val();
                        if (total != '' || !isNaN(total))
                            $("#plantasha").val(total)
                        $("#nroplantas,#distanciasurcos").empty();
                        $(this).dialog("close");
                    },
                    "Cancelar" : function() {
                        $(this).dialog("close");
                    }
                }
            });
            function calcular() {
                var total;
                var nroPlantas = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($("#nroplantas").val()) ? $("#nroplantas").val() : 1;
                var lineal = $("#boleo").is(":checked");
                var mcuadrado = $("#mcuadrado").is(":checked");
                if (mcuadrado) {
                    total = 10000 * nroPlantas;
                }

                if (lineal) {

                    var distancia = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test($("#distanciasurcos").val()) ? $("#distanciasurcos").val() : 1;

                    var distanciaBy100m = 100 / distancia;
                    var nroPlantasBy100m = parseFloat(nroPlantas) * 100;

                    var aux0 = parseFloat(distanciaBy100m);

                    var aux = aux0.toFixed(2) * parseFloat(nroPlantasBy100m);
                    total = aux.toFixed(0);
                }
                total = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(total) ? total : 'error';
                $("#total").val(total);
            }

        });
        $("#semilla").enviarSemilla();
    })
</script>