<h2 class="title">Datos Semilla Producida</h2>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
        <div style="float: right; margin-top: -10%;">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <form id="cosecha-buscar" class="seguimiento" >
        <?php
            include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <?php
    include '../vista/error/advertencia.php';
    ?>
    <form id="semillaprod" class="seguimiento" style="display: none">
        <?php
            include 'seguimiento.html';
        ?>
        <div>
            <label>Cultivo</label>
            <input name="cultivo" id="cultivo" class="not-edit"/>
        </div>
        <div>
            <label>Variedad</label>
            <input name="variedad" id="variedad" class="not-edit"/>
        </div>
        <div>
            <label id="lbl-nocampo-semilla_producida">N&uacute;mero de Campo</label>
            <select size="1" name="campo" id="nrocampo"></select>
        </div>
        <div id="datos-semilla-producida" style="display: none">
            <div>
                <label>Categoria Sembrada</label>
                <input name="categoriaSem" id="categoriaSem" class="not-edit"/>
            </div>
            <div>
                <label>Semilla neta (TM.)</label>
                <input size="10" id="semilla_neta" name="semilla_neta" class="number not-edit" readonly="readonly" style="width: 90px;"/>
            </div>
            <div>
                <label>Categoria Obtenida</label>
                <input size="30" id="cat_obtenida" name="categ_obtenida"  class="not-edit"/>
            </div>
            <div>
                <label>N&uacute;mero etiquetas</label>
                <input size="30" id="etiqueta" name="etiqueta" class="not-edit number"  style="width: 90px;"/>
            </div>
            <div>
                <label>Rango</label>
                <input id="rc-from" class="number not-edit" name="rc-from" style="width:53px" placeholder="desde" maxlength="5" autocomplete="off"/>
                <span>&nbsp;-&nbsp;</span>
                <input id="rc-to" class="number not-edit" name="rc-to" style="width:53px" placeholder="hasta" maxlength="5"/>
            </div>               
            <div>
                <input type="hidden" id="hidden-nrosolicitud" name="nrosolicitud" />
                <input type="hidden" id="iSolicitud" name="isolicitud" />
                <input type="hidden" id="iSemilla" name="isemilla" />
                <input type="hidden" id="iCosecha" name="icosecha" />
                <input type="hidden" id="iEstado" name="iestado"/>
                <input type="hidden" id="iGeneracion" name="igeneracion"/>
                <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
                <input id="opt" name="opt" type="hidden" value="guardar"/>
                <input id="pag" name="pag" type="hidden" value="semillaprod"/>
                <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
                <button id="enviar-semillaProd" class="btn btn-success" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    include '../vista/dialogos/confirmacion.html';
    include '../vista/dialogos/semillaNeta.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        //apariencia de cuadro de busqueda
        $("form#superficie-buscar.seguimiento").css( "width", "80%" );
        $("form#superficie-buscar.seguimiento>div#anteriores>div>label").css("width","15%");
        $("input#buscarTXT").css( "width", "69%" );
        //busqueda de semilleras
        $.buscar.autocompletarSemillaProducida();
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('input#buscarTXT').on('keyup', function(event) {
            if (event.which == 13) {
                $.buscar.semillaProducidaCertifica();
            }
        });
        $("button#btn-search").on("click", function() {
            $.buscar.semillaProducidaCertifica();
        });
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click", function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'produccion',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });

        $("#productor,#nrocampo").attr('disabled', 'disabled');
        //estilo formulario
        $(".seguimiento").css('padding', '3% 2% 6%');

        $("select#nrocampo").selectmenu({
            change : function(event, ui) {
            }
        });
        //cargar categorias
        $("select#nrocampo").on("selectmenuchange", function(event, ui) {
            if ($(this).val() != '') {
                var nroCampo = $(this).val();

                $.getJSON('control/index.php', {
                    mdl : 'certificacion',
                    opt : 'buscar',
                    pag : 'categGener',
                    nro : nroCampo
                }, function(data) {
                    $("input#categoriaSem").val(data.categoria);
                    $("input#cat_obtenida").val(data.categoriaObtenida);
                    $("input#igeneracion").val(data.igeneracion);
                });
                $.funciones.datosSemillaP("iSolicitud", "iCosecha", nroCampo);
                cultivo = $("input#cultivo").val();
                if (cultivo != 'Papa') {
                    $('div#semillaNeta').dialog({// Dialog
                        title : 'Calculo de semilla',
                        dialogClass : "no-close",
                        resizable : false,
                        buttons : {
                            "Aceptar" : function() {
                                $("input#semilla_neta").addClass("not-edit");
                                $('div#semillaNeta').dialog("close");
                                $('div#semillaNeta').dialog("destroy");
                            },
                            "Cancelar" : function() {
                                $(this).dialog("close");
                                $('div#semillaNeta').dialog("destroy");
                            }
                        }
                    });
                } else {
                    //calculo realizado si el cultivo es papa
                    nrocampo = $("select#nrocampo").find(':selected').val();
                    $.getJSON('control/index.php', {
                        mdl : 'certificacion',
                        opt : 'buscar',
                        pag : 'rendimiento_campo',
                        nro : nrocampo
                    }, function(json) {
                        //nombre de cultivo
                        cultivo = $('input#cultivo').val();
                        //calculo de numero de etiquetas segun la semilla neta. empaquetada en bolsa de 50 kilos
                        $("input#semilla_neta").val(json.rendimiento_campo);                        
                        if(cultivo == 'Papa'){
                            peso_bolsa = 50; 
                        }else{
                            peso_bolsa = 20;
                        }
                        total = (parseFloat(json.rendimiento_campo) * 1000)/peso_bolsa;
                        $("input#etiqueta").val(total.toFixed(0));                    
                        //asignacion de rango de etiquetas 
                        $.getJSON('control/index.php',{mdl:'certificacion',opt:'buscar',pag:'cupones'},function(json){
                            var final = json.cupones+total;
                            $('input#rc-from').val(json.cupones);
                            $("input#rc-to").val(final.toFixed(0)).removeClass('number');
                            $('div#datos-semilla-producida').slideDown();
                        });
                    });
                }
            }else{
                //ocultar datos de semilla producida
                $('div#datos-semilla-producida').slideUp();
            }
        });
        //calcular semilla neta
        $("button#calcular-semilla-neta").on("click", function() {
            var etiquetas = $("input#nroetiquetas").val();
            var capacidad = $("input#capacidad").val();

            var temporal = (parseFloat(etiquetas) * parseFloat(capacidad) ) / 1000;
            $("input#etiqueta").val(etiquetas);
            $("input#semilla_neta").val(temporal.toFixed(2));
        });
    }); 
</script>
<!--  validacion y envio de datos    -->
<script type="text/javascript">
    $(document).ready(function() {

        $("button#enviar-semillaProd").addClass("button-primary");
        $("button#enviar-semillaProd").on("click",function(){
            $.funciones.enviarSemillaProducida ();
            
        });
    })
</script>
