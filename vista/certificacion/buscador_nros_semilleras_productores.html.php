<div id="anteriores">
    <div>
        <label>Buscar : </label>
        <input id="buscarTXT" name="buscarTXT" />
        <button id="btn-search" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-search"> </span>
        </button>
        <span id="advice" style="font-size: 9px; font-style: italic; width: 45%;margin-left: 20%"> Escribir numero de solicitud, semillera o semillerista</span>
    </div>
</div>
<?php
    include '../vista/dialogos/seleccionarSemilleraSemillerista.html';  
      
    include '../vista/dialogos/seleccionarNroSolicitudCertificav2.html';
    include '../vista/dialogos/seleccionarSemilleraNroSolicitud.html';
    
    #nro solicitud ingresada mostrar cuadro de dialogo  en semilla-certificacion
    include '../vista/dialogos/seleccionarNroSolicitud.html';
    #nro solicitud ingresada mostrar cuadro de dialogo  en semilla-superficie-hoja_cosecha-certificacion
    include '../vista/dialogos/seleccionarSemilleraSemilleristaInspeccion.html'; 
    
    #semillera ingresada   mostrar   cuadro de dialogo semilla-certificacion
    include '../vista/dialogos/seleccionarSolicitudSemilleristaComunidad.html';
    #semillera ingresada   mostrar   cuadro de dialogo superficie-inspeccion-certificacion
    include '../vista/dialogos/seleccionarNroSolicitudCertifica.html';    
    #semillera ingresado  mostrar cuadro de dialogo semilla-certificacion
    include '../vista/dialogos/seleccionarSolicitudSemilleraComunidad.html';
    
    #semillera ingresado  mostrar cuadro de dialogo semilla-certificacion
    include '../vista/dialogos/seleccionarSolicitudSemilleraComunidadSemillerista.html';
    #semillerista ingresado  mostrar cuadro de dialogo superficie-certificacion
    include '../vista/dialogos/seleccionarSolicitudSemilleraComunidadSuperficie.html';
    #semillerista ingresado  mostrar cuadro de dialogo inspeccion-certificacion
    include '../vista/dialogos/seleccionarSolicitudSemilleraComunidadInspeccion.html';
?>