<div id="semright" style="width: 49%;float: left;margin-top:0;">
    <div>
            <label id="lbl-inspeccion" style="width: 37%">Inspeccion</label>
            <input id="inspeccion-plantines" name="inspeccion-plantines" type="text" class="special number"style="width: 65px" maxlength="10"/>
        </div>
        <div>
            <label style="width: 37%">Tipo de inspeccion</label>
            <input id="tipo-plantines" name="tipo-plantines" type="text" class="special number"style="width: 130px"  maxlength="10"/>
        </div>
        <div>
            <label style="width: 37%">Observacion</label>
            <textarea id="observacion-plantines" name="observacion-plantines" style="width: 228px; height: 89px;"></textarea>
        </div>
</div>
<div id="semleft" style="width: 50%;float:right;margin-top:0;">

</div>
<div style="float: right">
    <input name="plantines" type="hidden"/>
    <input id="insp" name="insp" type="hidden" value="1"/>
    <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
    <input id="opt" name="opt" type="hidden" value="guardar"/>
    <input id="pag" name="pag" type="hidden" value="inspeccion-plantines"/>

    <button id="enviar-inspeccion-plantines" class="btn btn-success" type="button">
        <span class="glyphicon glyphicon-floppy-disk"></span>
        Registrar
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        //envio de datos
        //$("form#inspeccion").enviarInspeccion();
        var stage = $("#stage").val();
        var txt = $("h2.title").text();
        $("button#enviar-inspeccion").click(function() {
            datos = $("form#seguimiento").serialize();

            $.ajax({
                url : 'control/index.php',
                data : datos,
                type : 'POST',
                beforeSend : function() {
                    $.funciones.deshabilitarTexto("enviar-inspeccion-plantines"); 
                },
                success : showResponse
            });
            function showResponse(responseText, statusText, xhr, $form) {
                switch (responseText) {
                case 'OK' :
                    $("div.div-new").fadeIn();
                    $("div.div-filter").hide();
                    $("form#filter-box").slideUp();
                    $("div#btn-new-filter").css({
                        'float' : 'right',
                        'margin-top' : '6%'
                    });
                    setTimeout($.funciones.recargarVerDatos('certificacion','vinspecciones',1,1100), 10000);
                    break;
                default:
                    $.funciones.mostrarMensaje('error', responseText);
                    $.funciones.ocultarMensaje(5000);
                    break;
                }
            }

        });
    }); 
</script>