<h2 class="title">Inscripci&oacute;n de campo Semillero</h2>
<input type="hidden" id="stage" value="solicitud"/>
<div class="entry" style="margin-left: 0px;">
    <div id="radio" style="margin-bottom:5%">
        <input type="radio" id="tipo1" name="tipo" value="personal" />
        <label for="tipo1" style="width: 23%">Personal</label>
        <input type="radio" id="tipo2" name="tipo" value="institucional" checked="checked" />
        <label for="tipo2" style="width: 35%">Institucional</label>
        <br/>
    </div>
    <div style="float:right">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <!-- Personal-->
    <!-- Institucional-->
    <?php
   include '../vista/confirmacion/aviso.html';
    ?>
    <form id="solicitud" class="nuevoform" autocomplete="off">
        <div>
            <!--class="soleft"--->
            <div style="width: 48%; float: left;">
                <label style="width: 45%;">Número de Solicitud</label>
                <input style="width: 45px;" name="nro_solicitud" id="nro_solicitud" class="not-edit" maxlength="5">
            </div>
        </div>
        <div>
            <div style="width: 50%; float: right;">
                <label style="width: 25%;">Proceso</label>
                <input type="text" style="width: 75px;border-color: #FAFFEE;position: relative;top: -5px;" value="<?php echo utf8_encode($_SESSION['usr_area']); ?>" name="sistema" id="sistema" class="not-edit">
            </div>
            <div style="width: 35em; float: left;">
                <label style="width: 22%">Departamento</label>
                <input type="text" value="Chuquisaca" name="departamento" id="departamento" class="not-edit" readonly="readonly" style="width:70px; border-color: #FAFFEE;position: relative;top: -5px;"/>
            </div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-prov"  style=";position: relative;top: 5px;">Provincia</label>
                <select id="lstprovincia" style="width: 195px"></select>
            </div>
            <div style="width: 35em; float: left;">
                <label class="lblerror-mun"  style=";position: relative;top: 5px;">Municipio</label>
                <select id="lstmunicipio" name="municipio" style="width: 195px" disabled="disabled">
                    <option value="">[ Seleccione Municipio]</option>
                </select>
            </div>
            <div style="width: 35em;float: left;" class="lst_semillera">
                <label style="width: 20%;position: relative;top: 5px;">Semillera</label>
                <select id="lstsemillera" name="semillera" style="text-transform: uppercase;width: 200px" disabled="disabled"></select>
            </div>
            <div style="width: 35em; float: left;">
                <span class="required">&nbsp;&nbsp;</span>
                <label style="width:20%;position: relative;top: 5px;">Comunidad</label>
                <input type="text" name="comunidad" id="mcomunidad">
            </div>
            <div style="width: 35em; float: left;">

            </div>
        </div>
        <div class="soright" style="width: 50%;float: right;margin: -34% 0">
            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">N&uacute;mero de parcelas</label>
                <input id="noparcelas" name="noparcelas" style="width: 30px;" value="0"/>
            </div>

            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Nombre semillerista</label>
                <input id="name" name="name" type="text" style="width: 200px"/>
            </div>
            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Apellido semillerista</label>
                <input id="apellido" name="apellido" type="text" style="width: 200px"/>
            </div>
            <div style="width: 35em">
                <label style="width: 30%;position: relative;top: 5px;">Fecha Solicitud</label>
                <input id="f_solicitud" name="f_solicitud" readonly="readonly" style="width: 65px;" value="<?php echo date('d-m-Y'); ?>"/>
            </div>
            <div style="float: right;">
                <input id="sarea" name="sarea" type= "hidden" value="<?php echo $_SESSION['usr_nivel']; ?>"/>
                <input id="stage" name="stage" type="hidden" value="solicitud"/>
                <input id="tipo" name="tipo" type="hidden" value="institucional"/>
                <input type="hidden" name="hdn_provincia" id="provincia"/>
                <input type="hidden" name="hdn_municipio" id="hdn_municipio"/>
                <input type="hidden" name="hdn_isemillera" id="isemillera" />
                <button id="enviar-solicitud" class="btn btn-success">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
        </div>
        <div id="advice">
            <span class="required">&nbsp;&nbsp;</span>&nbsp;Para agregar mas comunidades deben estar separadas por coma &nbsp;(Ej: La Playa,Bella Vista, Yuquina)
        </div>
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    include '../vista/dialogos/agregarSemillera.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click",function(){
            $(".informar").empty();
            edt = $(".crud").val();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'solicitud',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        //opciones personal - intitucional
        $("#radio").buttonset();
        // select menu de numero de parcelas
        $("input#noparcelas").spinner({
            min : 0,
            max : 10
        });
        //ocultar select de municipio
        $("#lstmunicipio").hide();

        $("#nro_solicitud").numeric();
        //calendario
        $("#f_solicitud").calendarioSolicitud('f_solicitud');

        $("input[name=submit]").fadeTo('fast', 0.5).mouseover(function() {
            $(this).fadeTo('fast', 1);
        }).mouseleave(function() {
            $(this).fadeTo('fast', 0.5);
        });
        /**estilo para aviso*/
        var cssObj = {
            'float' : 'left',
            'font-size' : '9px',
            'font-style' : 'italic',
            'letter-spacing' : '0.1em',
            'width' : '45%'
        }
        //aviso
        $("#advice").css(cssObj);

        //creacion de scroll para select de provincia
        $("#lstprovincia").append('<option value=""> [ Seleccione provincia ]</option>');
        $("#lstprovincia").selectmenu().selectmenu("menuWidget").addClass("overflow");
        //scroll para select de municipio
        $("select#lstmunicipio").selectmenu().selectmenu("menuWidget").addClass("overflow-municipio");
    }); 
</script>

<script type="text/javascript">
var anwer;
    $(document).ready(function() {
        
        function loadNewSolicitud() {
            //carga de nro de solicitud
            var proceso = $("#sistema").val();
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'nro',
                sis : 'certifica',
                pro : proceso
            }, noSolicitud);
            function noSolicitud(json) {
                if (json.estado = 'nuevo') {
                    if (parseInt(json.nro) < 10) {
                        var numero = '00' + parseInt(json.nro);
                        $("#nro_solicitud").empty().val(numero);
                    } else if (json.nro >= 10 && json.nro < 100) {
                        var numero = '0' + parseInt(json.nro);
                        $("#nro_solicitud").empty().val(numero);
                    } else {
                        $("#nro_solicitud").empty().val(parseInt(json.nro));
                    }
                } else {
                    $("#nro_solicitud").css('border-color', '#CECECE');
                }
            };
        };
        loadNewSolicitud()
        //opciones segun sea personal o institucional
        $("input#tipo1").on('click', function() {
            if ($(this).is(':checked')) {
                loadNewSolicitud();
                $("input#tipo").val($(this).val());
                // Hacer algo si el checkbox ha sido seleccionado
                $(".lst_semillera").css({
                    display : 'none'
                });
            }
        });
        $("input#tipo2").on('click', function() {
            if ($(this).is(':checked')) {
                $("input#tipo").val($(this).val());
                // Hacer algo si el checkbox ha sido seleccionado
                $(".lst_semillera").css({
                    display : ''
                });
            }
        });

        //carga de provincias de chuquisaca
        $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=colugar&seccion=provincia", function(json) {
            $.each(json, function(key, value) {
                $('#lstprovincia').append('<option value="' + key + '">' + value + '</option>');
            });
        });
        //creacion de evento onchange en select provincia
        $("#lstprovincia").selectmenu({
            change : function(event, ui) {
            }
        });

        //evento onchange de select provincia
        $("#lstprovincia").on("selectmenuchange", function(event, ui) {
            var provincia = $("#lstprovincia").val();
            if (provincia != "") {
                $("#lstmunicipio").selectmenu("destroy").empty();
                $('#lstmunicipio').append('<option value="">[ Seleccione Municipio ]</option>');
                $("#lstmunicipio").selectmenu();
                $("#provincia").val(provincia);
                var palabras = provincia.substring(0, 3);
                $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=colugar&seccion=municipio&prov=" + palabras, function(json) {
                    $('#lstmunicipio').empty().append('<option value="">[ Seleccione Municipio ]</option>');
                    $.each(json, function(key, value) {
                        $('#lstmunicipio').append('<option value="' + key + '">' + value + '</option>');
                    })
                });
                $("#lstmunicipio").selectmenu({
                    disabled : false
                });
            } else {
                $("#lstmunicipio").selectmenu({
                    disabled : true
                });
            }
        });
        //creacion de evento onchange
        $("select#lstmunicipio").selectmenu({
            change : function(event, ui) {
            }
        });
        // manejo de opciones despues de seleccionar un municipio
        $("select#lstmunicipio").on("selectmenuchange", function(event, ui) {
            //cargar id del municipio
            var id_municipio = $("select#lstmunicipio").val();
            if (id_municipio > 0) {
                $("#hdn_municipio").val(id_municipio);
            } else {
                $("#hdn_municipio").val(0);
            }
            //cargar semilleras que estan en la provincia
            var provincia = $("#lstprovincia").find(":selected").val();
            var municipio = $("#lstmunicipio").find(":selected").val();
            var area = $(".ulogin").val();
            if (provincia != "" && municipio != "") {
                $("#lstsemillera").selectmenu("destroy").empty();
                $("#lstsemillera").append('<option value="">[ Seleccione Semillera ]</option>');
                $("#lstsemillera").selectmenu();
                $.getJSON("control/index.php?mdl=certificacion&opt=ver&pag=semillera&chs=solicitud&provincia=" + provincia + "&municipio=" + municipio+"&area="+area, function(json) {
                    $('#lstsemillera').empty().append('<option value="">[ Seleccione Semillera ]</option>');
                    $.each(json, function(key, value) {
                        $('#lstsemillera').append('<option value="' + value + '">' + value + '</option>');
                    });
                    $("select#lstsemillera").append('<option value="agregar">Agregar semillera</option>');
                });

                $("#lstsemillera").selectmenu({
                    disabled : false
                });
            } else {
                $("#lstsemillera").selectmenu({
                    disabled : true
                });
            }
            $("select#lstsemillera").selectmenu().selectmenu("menuWidget").addClass("overflow-semillera");
        });
        //al cambiar semillera cambiar el numero de solicitud
        $("select#lstsemillera").selectmenu({
            change : function(event, ui) {
            }
        });
        $("select#lstsemillera").on("selectmenuchange", function(event, ui) {
            var semillera = $(this).val();
            var proceso = $("#sistema").val();
            if (semillera != 'agregar' && semillera != '') {
                loadNoSolicitud(proceso, semillera);
            }else{
                //mostrar cambio de nro de solicitud al seleccionar agregar semillera
                loadNewSolicitud();
                $("#nro_solicitud").addClass('show-change');
                    setTimeout(function() {
                        $("#nro_solicitud").removeClass('show-change');
                    }, 500);
            }
        });
        //calculo de parcelas
        $("#mcomunidad").blur(function() {
            var comunidades = $("#mcomunidad").val()
            var arraycomunidades = comunidades.split(',');
            $("#noparcelas").val(arraycomunidades.length);
            var minComunidad = arraycomunidades.length;
            console.log(minComunidad);
            $("input#noparcelas").spinner({
                min : minComunidad,
                max : 10
            });
        });
        //autocompletar comunidad,nombre y apellido
        $("#mcomunidad").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=ver&pag=colugar&seccion=comunidad",
            minLength : 2
        });
        $("#name").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=ver&pag=nombres",
            minLength : 2
        });

        $("#apellido").autocomplete({
            source : "control/index.php?mdl=certificacion&opt=ver&pag=apellidos",
            minLength : 2
        });
        function loadNoSolicitud(proceso, semillera) {
            $.getJSON("control/index.php", {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'isemillera',
                sem : semillera
            }, idSemillera);
            function idSemillera(json) {
                $.getJSON("control/index.php", {
                    mdl : 'certificacion',
                    opt : 'nros',
                    pro : proceso,
                    sem : json
                }, noSolicitud);
                function noSolicitud(json) {
                    $("#nro_solicitud").empty().val(json.nro);
                    $("#nro_solicitud").addClass('show-change');
                    setTimeout(function() {
                        $("#nro_solicitud").removeClass('show-change');
                    }, 500);
                };
            };
        };

        //envio de formulario
        $("#solicitud").enviarSolicitud();
    }); 
</script>