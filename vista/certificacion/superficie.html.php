<h2 class="title">Datos Superficie</h2>
<input type="hidden" id="stage" value="superficie"/>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
	<div style="float: right; margin-top: -10%;">
		<button id="return" class="btn btn-success" type="button">
			<span class="glyphicon glyphicon-arrow-left"> </span>
			Volver
		</button>
	</div>
	<form id="superficie-buscar" class="seguimiento">
		<?php
        include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
		?>
	</form>
	<?php
    include '../vista/error/advertencia.php';
	?>
	<div id="sup_campos" style="display: none">
	    <div>
            <input type="hidden" name="noSolicitud" id="noSolicitud"/>
            <input type="hidden" name="isolicitud" id="iSolicitud"/>
            <input type="hidden" name="iestado" id="iEstado"/>
            <input type="hidden" name="isemilla" id="iSemilla"/>
            <input type="hidden" name="isuperficie" id="iSuperficie"/>
            
            <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
        </div>
		<?php
        include 'superficie_campos.html.php';
		?>
	</div>

</div>
<?php
include '../vista/dialogos/cantidadParcelas.html';
include '../vista/error/errores.php';
include '../vista/error/aviso.php';
?>
<!-- acciones de buscador -->
<script type="text/javascript">
    $(document).ready(function() {
        //mensaje de cuadro de busqueda
        $("span#advice").css('margin-left','15%');
        //apariencia de cuadro de busqueda
        $("form#superficie-buscar.seguimiento>div#anteriores>div>label").css("width","15%");
        $("input#buscarTXT").css( "width", "69%" );
        //busqueda de coincidencias segun
        //nro solicitud-semillera-semillerista
        $.buscar.autocompletarSuperficie();
    });
</script>     
<script type="text/javascript">
    $(document).ready(function() {
        $("button#btn-search").on("click", function() {
            $.buscar.superficieCertifica();
        });
        $("input#buscarTXT").on("keyup", function(event) {
            if (event.which == 13)
                $.buscar.superficieCertifica();
        });
        //funcion para cargar datos complementarios
        function determinarEtapa(isemilla, iproductor, isuperficie) {
            /**despues de realizar la primera inspeccion podra ingresar sup rechazada o retirada*/
            //primero obtener el estado de laboratorio  en solicitud si es 7 habilitar superficie rechazada y retirada
            var estado = 0;
            //determinar si el cultivo es papa
            $.getJSON('control/index.php', {
                mdl : 'certificacion',
                opt : 'buscar',
                pag : 'etapa',
                isemilla : isemilla,
                isemillerista : iproductor,
                isuperficie : isuperficie
            }, function(json) {
                $('div.campo>label.campo').empty().text(json.nro_campo);
                $('input#inscrita').empty().val(json.inscrita);
                $('input#rechazada').empty().val(json.rechazada);
                $('input#retirada').empty().val(json.retirada);
                $('input#aprobada').empty().val(json.aprobada);
            });
        }

    });
    //fin document.ready
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click", function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'superficie',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
    }); 
</script>