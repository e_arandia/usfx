<form id="FormAprobada" class="nuevoform" style="padding:3% 2% 4%">
    <?php
        include 'seguimiento_superficie_inspeccion.html';
    ?>
    
    <div id="calendar" style="width: 95%; margin-left: 2.5%;">
        <div class="head" style="height: 2em">
            <div class="htitle" style="#459e00 url("images/ui-bg_highlight-hard_15_459e00_1x100.png") repeat-x scroll 50% 50%">
                <label style="padding-top: 1%;width: 9.9em;">Numero de campo</label>
                <label style="padding-top: 1%;">Superficie Inscrita</label>
                <label style="padding-top: 1%;">Superficie Rechazada</label>
                <label style="padding-top: 1%;">Superficie Retirada</label>
                <label style="padding-top: 1%;">Superficie Aprobada</label>
            </div>
        </div>
        <div class="body" style="width: 99.7%;border-top:'';float:none;background: #459e00 url("images/ui-bg_highlight-hard_15_459e00_1x100.png") repeat-x scroll 50% 50%">
            <form>
                <div style="width: 9.9em;margin: -1% 0%;border-left:1px solid #479f03" class="sInscrita campo">
                    <label name="nocampo" class="campo" style="width: 40px; height: 11px; border-top-color: rgb(255, 255, 255); border-right-color: rgb(255, 255, 255); border-bottom-color: rgb(255, 255, 255); font-size: 14px; margin-left: 35%; display: block; text-align: center;">
                     </label>
                </div>
                <div class="sInscrita ins" style="margin: -1% 0%;">
                <label>
                <input style="margin-left: 150%;width: 30px;height: 11px;" maxlength="5" id="inscrita" class="not-edit" name="inscrita" readonly="readonly"/>
                </label>
                </div>
                <div class="sInscrita rec" style="margin: -1% 0%;">
                <label>
                <input type="text" style="margin-left: 150%;width: 30px;height: 11px;" autocomplete="off" id="rechazada" class="number" maxlength="5" name="rechazada" />
                </label>
                </div>
                <div class="sInscrita ret" style="margin: -1% 0%;">
                <label>
                <input style="margin-left: 150%;width: 30px;height: 11px;" autocomplete="off" id="retirada" class="number" maxlength="5" name="retirada" />
                </label>
                </div>
                <div class="sInscrita apr" style="margin: -1% 0%;">
                <label>
                <input style="margin-left: 150%;width: 30px;height: 11px;" id="aprobada" class="not-edit" maxlength="5" name="aprobada" />
                </label>
                </div>
            </form>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div>
            <input id="mdl" name="mdl" type="hidden" value="certificacion"/>
            <input id="opt" name="opt" type="hidden" value="guardar"/>
            <input id="pag" name="pag" type="hidden" value="superficie"/>
            <input id="tipo" name="tipo" type="hidden" value="aprobada"/>
            <input id="cul" name="cul" type="hidden"/>
            <button id="enviar-superficie" class="btn btn-success" type="button">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
            </button>
            
        </div>
    </div>

</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#rechazada,#retirada").numeric(".");

        $("div.apr").append($("input[name=isolicitud]").clone(true));
        $("div.apr").append($("input[name=isemilla]").clone(true));
        
        
        $("input#rechazada").focus(function() {
            $(this).css({
                'border-color' : '#DADADA'
            });
        }).blur(function() {
            if ( $(this).val().length == 0 ){
                $(this).val(0);
            }
        }).keyup(function() {
            inscrita = $("input#inscrita");
            aprobada = $("input#aprobada");
            rechazada = $("input#rechazada").val().length == 0 ? 0.0 : $(this).val();
            calculo = (inscrita.val() - rechazada - $("input#retirada").val());            
            if (calculo < inscrita || calculo >inscrita) {
                aprobada.val(0).css({
                    'color' : 'red'
                });
            } else {
                aprobada.val(calculo.toFixed(2) < 0 ? 0.0 : calculo.toFixed(2)).css({
                    'color' : ''
                });
            }
        });

        $("input#retirada").focus(function() {
            $(this).css({
                'border-color' : '#DADADA'
            });
        }).blur(function() {
            if ( $(this).val().length == 0 ){
                $(this).val(0);
            }
        }).keyup(function() {
            inscrita = $("input#inscrita");
            aprobada = $("input#aprobada");
            retirada = $("input#retirada").val().length == 0 ? 0.0 : $(this).val();
            calculo = (inscrita.val() - $("input#rechazada").val() - retirada);
            if (calculo < inscrita || calculo >inscrita) {
                aprobada.val(0).css({
                    'color' : 'red'
                });
            } else {
                aprobada.val(calculo.toFixed(2) < 0 ? 0.0 : calculo.toFixed(2)).css({
                    'color' : ''
                });
            }
        });
        
        $("button#enviar-superficie").on('click',function(){
            $.funciones.enviarSuperficie();
        });        
    }); 
</script>