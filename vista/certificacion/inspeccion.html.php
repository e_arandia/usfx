<h2 class="title">Datos Inspeccion</h2>
<input type="hidden" id="stage" value="inspeccion"/>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
        <div style="float: right; margin-top: -10%;">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <form id="inspeccion-buscar" class="seguimiento">
        <?php
            include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <?php
    include '../vista/error/advertencia.php';
    ?>
    <form id="seguimiento" class="nuevoform" style="padding: 3% 2% 36%;display: none;">
        <?php
        include 'inspeccion_superficie.html.php';
        ?>
        <div  class="current-nocampo" >
            <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>            
            <input type="hidden" name="isuperficie" id="iSuperficie"/>
            <input type="hidden" name="isemilla" id="iSemilla"/>
            <input type="hidden" name="isolicitud" id="isolicitud"/>
            <input type="hidden" name="iEstado" id="iEstado"/>
            <input type="hidden" name="ifecha" id="ifecha" value="1"/>
            <input type="hidden" name="dia" id="hiddendia"/>
            <input type="hidden" name="mes" id="hiddenmes"/>
            <input type="hidden" name="anho" id="hiddenanho"/>
        </div>
    </form>
    <div id="insp_campos"></div>
</div>
<?php
include '../vista/dialogos/cantidadParcelas.html';
include '../vista/error/aviso.php';
include '../vista/error/errores.php';
?>

<script type="text/javascript">
    $(document).ready(function() {
        //mensaje de cuadro de busqueda
        $("span#advice").css('margin-left','15%');
        //apariencia de cuadro de busqueda
        $("form#inspeccion-buscar.seguimiento>div#anteriores>div>label").css("width","15%");
        $("input#buscarTXT").css( "width", "69%" );
        //busqueda de semilleras
        $.buscar.autocompletarInspeccion();
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('input#buscarTXT').on('keyup', function(event) {
            if(event.which == 13)
                $.buscar.inspeccionCertifica();
        });
        $("button#btn-search").on("click",function(){ 
            $.buscar.inspeccionCertifica();
        });
    }); 
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click", function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'vinspecciones',
                area : 1,
                edt : edt
            }, function(data) {
                $(".post").empty().append(data);
            });
        });
        //crear el valor oculto del productor para ser enviado junto con la fecha de inspeccion
        $('#aprobada').attr('readonly', 'readonly');
    }); 
</script>