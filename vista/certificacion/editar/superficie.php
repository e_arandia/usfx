<h2 class="title">Datos Superficie</h2>
<div class="entry">
	<?php
	$superficie = DBConnector::resultado();
	?>
	<div>
		<label style="font-weight: bold">Semillera: </label>
		<label><?php echo $superficie -> semillera;?></label>
	</div>
	<div style="margin-bottom: 10px" ></div>
	<div>
		<label style="font-weight: bold">Productor: </label>
		<label><?php echo utf8_encode($superficie -> nombre) . '&nbsp;' . utf8_encode($superficie -> apellido);?></label>
	</div>
	<div style="margin-bottom: 20px;" ></div>
	<div id="tabs">
		<ul>
			<li>
				<a href="#tabs-1">Inscrita</a>
			</li>
			<li>
				<a href="#tabs-2">Rechazada</a>
			</li>
			<li>
				<a href="#tabs-3">Retirada</a>
			</li>
			<li>
				<a href="#tabs-4">Aprobada</a>
			</li>
		</ul>
		<div id="tabs-1">
			<!-- superficie inscrita-->
			<form id="FormInscrita">
				<div>
					<label>Inscrita</label>
					<input id="inscrita" name="inscrita" type="text" value="<?php echo $superficie->inscrita?>"/>
					<input type="hidden" name="iproductor" class="iproductor" value="<?php echo $superficie->id_solicitud?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="isend" value="Actualizar" type="submit"/>
				</div>
			</form>
		</div>
		<div id="tabs-2">
			<!-- superficie rechazada-->
			<form id="FormRechazada">
				<div>
					<label>Rechazada</label>
					<input name="rechazada" type="text" value="<?php echo $superficie -> rechazada;?>"/>
					<input type="hidden" name="iproductor" class="iproductor"  value="<?php echo $superficie->id_solicitud?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="rsend" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
		<div id="tabs-3">
			<!-- superficie retirada-->
			<form id="FormRetirada">
				<div>
					<label>Retirada</label>
					<input name="retirada" type="text" value="<?php $retirada = !empty($superficie -> retirada) ? $superficie -> retirada : '';
						echo $retirada;
					?>"/>
					<input type="hidden" name="iproductor" class="iproductor" value="<?php echo $superficie->id_solicitud?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="tsend" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
		<div id="tabs-4">
			<!-- superficie aprobada-->
			<form id="FormAprobada">
				<div>
					<label>Aprobada</label>
					<input id="aprobada" name="aprobada" type="text" value="<?php $aprobada = !empty($superficie -> aprobada) ? $superficie -> aprobada : '';
						echo $aprobada;
					?>"/>
					<input type="hidden" name="iproductor" class="iproductor"  value="<?php echo $superficie->id_solicitud?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="asend" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
	</div>
	<?php
	include 'error/errores.php';
	?>
</div>
<!--  creacion de tabs    -->
<script type="text/javascript">
	$(document).ready(function() {
		// Tabs
		$('#tabs').tabs();
	});

</script>
<!--    validacion y envio de datos  -->
<script type="text/javascript">
	$(document).ready(function() {
		var id = $(".iproductor").val();
		var imagen = '<img src="images/loader_3a.gif" />';
		$("#FormInscrita").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'inscrita',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest,
			success : showResponse
		});

		$("#FormRechazada").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'rechazada',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest2,
			success : showResponse
		});

		$("#FormRetirada").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'retirada',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest3,
			success : showResponse
		});
		$("#FormAprobada").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'aprobada',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest4,
			success : showResponse
		});
		/*Validacion de superficie inscrita*/

		function showRequest(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];
			if(!form.inscrita.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.inscrita.value)) {
				mensaje += '<div>- Superficie Inscrita</div>';
			}
			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Debe introducir: <br>' + mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro de los datos?\n\t-Presione aceptar para continuar \n\t-Presione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
			   		$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
					});
					return true;
				} else {
					return false;
				}
			}
		};

		/*Validacion de superficie rechazada*/
		function showRequest2(fformData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];
			if(!form.rechazada.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.rechazada.value)) {
				mensaje += '<div>- Superficie Rechazada</div>';
			}
			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Debe introducir: <br>' + mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro de los datos?\n\t-Presione aceptar para continuar \n\t-Presione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
			$.blockUI({
				theme : true,
				title : 'Sistema CERFIS',
				message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
			});
					return true;
				} else {
					return false;
				}
			}
		};

		/*Validacion de superficie retirada*/
		function showRequest3(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];
			if(!form.retirada.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.retirada.value)) {
				mensaje += '<div>- Superficie Retirada</div>';
			}
			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Debe introducir: <br>' + mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro de los datos?\n\t-Presione aceptar para continuar \n\t-Presione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
			$.blockUI({
				theme : true,
				title : 'Sistema CERFIS',
				message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
			});
					return true;
				} else {
					return false;
				}
			}
		};

		/*Validacion de superficie aprobada*/
		function showRequest4(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];
			if(!form.aprobada.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.aprobada.value)) {
				mensaje += '<div>- Superficie Aprobada</div>';
			}
			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Debe introducir: <br>' + mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro de los datos?\n\t-Presione aceptar para continuar \n\t-Presione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
			$.blockUI({
				theme : true,
				title : 'Sistema CERFIS',
				message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
			});
					return true;
				} else {
					return false;
				}
			}
		};

		function showResponse(responseText, statusText, xhr, $form) {
			var msg = '<img src="images/error.png" />';
			switch (responseText) {
				case 'error':
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Error. No se actualizaron los datos<br>Se ha producido un error al intentar actualizar los datos.</div>',
						timeout : 1000
					});
					break;

				case 'OK' :
					var msg = '<img src="images/check.png" />';
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Los datos fueron actualizados.<br><br>Cargando Lista de superficies</div>',
						timeout : 2000
					});
					
					$.post('control/usuario/usuario.ctrl.php', {
						page : 'vsuperficie',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		};

	})
</script>