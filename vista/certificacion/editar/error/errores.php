<div id="errores" class="ui-widget" style="display: none">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;width: 35em">
		<p>
			<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: 0.5em;"></span>
			<span id="icono"></span>
			<span class="mensaje"></span>
		</p>
	</div>
</div>