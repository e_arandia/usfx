<?php
$semilla_producida = mysql_fetch_object($datos);
?>
<h2 class="title">Datos Semilla Producida</h2>
<div class="entry">
	<form id="semillaprod" >
		<div>
			<input type="hidden" id="isemillaP" name="isemillaP" value="<? echo $semilla_producida -> id_semillaProd;?>"/>
		</div>
		<div>
			<label>Semillera: </label>
			<input size="1" name="semillera" id="semillera" value="<? echo $semilla_producida -> semillera;?>"/>
		</div>
		<div>
			<label>Productor: </label>
			<input id="productor" name="productor" value="<? echo utf8_encode($semilla_producida -> nombre) . ' ' . utf8_encode($semilla_producida -> apellido);?>"/>
		</div>
		<div>
			<label>N&uacute;mero de Campo</label>
			<input name="campo" id="nrocampo" value="<? echo $semilla_producida -> nro_campo;?>"/>
		</div>
		<div>
			<label>Categoria</label>
			<input name="categoria" id="categoria" value="<? echo $semilla_producida -> categoria;?>" />
		</div>
		<div>
			<label>Generacion</label>
			<input id="generacion" name="generacion" value="<? echo $semilla_producida->generacion?>"/>
		</div>
		<div>
			<label>Semilla neta</label>
			<input size="10" id="semilla_neta" name="semilla_neta" value="<? echo $semilla_producida->semilla_neta?>"/>
		</div>
		<div>
			<label>categoria obtenida</label>
			<input size="30" id="cat_obtenida" name="categ_obtenida" value="<? echo $semilla_producida -> categoria_obtenida;?>"/>
		</div>
		<div>
			<label>Nro etiqueta</label>
			<input size="30" id="etiqueta" name="etiqueta"value="<? echo $semilla_producida -> nro_etiqueta;?>"/>
		</div>
		<div>
			<label>Rango</label>
			<input size="30" id="rango" name="rango"value="<? echo $semilla_producida -> rango;?>"/>
		</div>
		<div>
			<input class="button-primary ui-state-default ui-corner-all" name="send" type="submit" value="Registrar" />
		</div>
	</form>
</div>
<?php
include 'error/errores.php';
?>
<!--  validacion y envio de datos    -->
<script type="text/javascript">
	$(document).ready(function() {
		var id = $("#isemillaP").val();
		$("#semillaprod").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			type : 'post',
			data : {
				page : 'actualizacion',
				tbl : 'cosecha',
				id : id
			},
			beforeSubmit : showRequest,
			success : showResponse
		});
		function showRequest(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];

			if(!form.semillera.value) {
				mensaje += '<div>- Semillera</div>';
			}
			if(!form.productor.value) {
				mensaje += '<div>- Semillerista</div>';
			}
			if(!form.campo.value) {
				mensaje += '<div>- N&uacute;nero de campo</div>';
			}
			if(!form.categoria.value) {
				mensaje += '<div>- Categor&iacute;a</div>';
			}
			if(!form.semilla_neta.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.semilla_neta.value)) {
				mensaje += '<div>- Semilla neta</div>';
			}
			if(!form.categ_obtenida.value) {
				mensaje += '<div>- Categor&iacute;a obtenida</div>';
			}
			if(!form.etiqueta.value) {
				mensaje += '<div>- N&uacute;mero Etiqueta</div>';
			}
			if(!form.rango.value) {
				mensaje += '<div>- Rango</div>';
			}

			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
				$("#errores").css('margin-top', '3em').fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
					});
					return true;
				} else {
					return false;
				}
			}
		}

		function showResponse(responseText, statusText, xhr, $form) {
			var msg = '<img src="images/error.png" />';
			switch (responseText) {
				case 'error':
					$.unblockUI();
					$('.post').block({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Error. No se actualizaron los datos</div>',
						timeout : 1000
					});
					break;

				case 'OK' :
					$.unblockUI();
					var msg = '<img src="images/check.png" />';
					$('.post').block({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Los datos fueron actualizados.<br><br>Cargando lista de semillas producidas</div>',
						timeout : 2000
					});
					$.post('control/usuario/usuario.ctrl.php', {
						page : 'vproduccion',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		}

	})
</script>
