<?php
$cosecha = mysql_fetch_object($datos);
?>
<h2 class="title">Hoja de Cosecha</h2>
<div class="entry">
	<form>
		<div>
			<label style="font-weight: bold">Semillera: </label>
			<label><?php echo $cosecha -> semillera;?></label>
		</div>
		<div>
			<label  style="font-weight: bold">Productor: </label>
			<label><?php echo utf8_encode($cosecha -> nombre) . ' ' . utf8_encode($cosecha -> apellido);?></label>
		</div>
		<div>
			<label style="font-weight: bold">Cultivo:</label>
			<label><?php echo $cosecha -> cultivo;?></label>
		</div>
	</form>
</div>
<div class="entry">
	<form id="cosecha">
		<div>
			<label>N&uacute;mero Campo</label>
			<input id="campo" name="campo" value="<?php echo $cosecha -> nro_campo;?>"/>
		</div>
		<div>
			<label>Fecha Emision</label>
			<input id="f_emision" name="f_emision" type="text" readonly="readonly" value="<?php echo $cosecha -> fecha_emision;?>"/>
		</div>
		<div>
			<label>Categoria en Campo</label>
			<input id="categ_campo" name="categ_campo" type="text" value="<?php echo $cosecha -> categoria_en_campo;?>"/>
		</div>
		<div>
			<label>Rendimiento Estimado (Tn.)</label>
			<input size="10" id="rendimientoE" name="estimado"value="<?php echo $cosecha -> rendimiento_estimado;?>"/>
		</div>
		<div>
			<label>Superficie Parcela</label>
			<input size="10" id="sup_parcela" name="sup_parcela" value="<?php echo $cosecha -> superficie_parcela;?>"/>
		</div>
		<div>
			<label>Rendimiento Campo</label>
			<input size="10" id="rend_campo" name="rend_campo" value="<?php echo $cosecha -> rendimiento_campo;?>"/>
		</div>
		<div>
			<label>Numero Cupones</label>
			<input size="10" id="nro_cupones" name="nro_cupones" value="<?php echo $cosecha -> nro_cupones;?>" />
		</div>
		<div>
			<label>Rango Cupones</label>
			<input size="10" id="rango_cupones" name="rango_cupones" <?php echo $cosecha -> rango_cupones;?>
			/>
		</div>
		<div>
			<label>Planta Acondicionadora</label>
			<input size="10" id="acondicionadora" name="acondicionadora" value="<?php echo $cosecha -> planta_acondicionadora;?>" />
		</div>
		<div>
			<label>Laboratorio ?</label>
		</div>
		<div id="laboratorio" style="float: left; margin: 0pt; height: 1.5em; width: 15em;">
			<input type="radio" id="silab" name="laboratorio" value="1" style="width: 10px; clear: right; float: left;">
			<label style="width: 15px; left: 3em; clear: right;">SI</label>
			<input type="radio" id="nolab" name="laboratorio" value="0" style="width: 10px;">
			<label style="width: 20px; float: left; margin-top: 0em; margin-left: 2.5em;">NO</label>
		</div>
		<div>
			<input type="hidden" name="icosecha" class="icosecha" value="<?php echo $cosecha -> id_cosecha;?>"/>
		</div>
		<div>
			<input class="button-primary ui-state-default ui-corner-all" name="send" value="Actualizar" type="submit">
		</div>
	</form>
</div>
<?php
include 'error/errores.php';
?>
<!-- inicializacion de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		$("#f_emision").calendario('f_emision');
	})
</script>
<!--  validacion y envio de datos mediante ajax       -->
<script type="text/javascript">
	$(document).ready(function() {
		var id = $(".icosecha").val();
		$("#cosecha").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			type : 'post',
			data : {
				page : 'actualizacion',
				tbl : 'cosecha',
				id : id
			},
			beforeSubmit : showRequest,
			success : showResponse
		});
		function showRequest(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];

			if(!form.campo.value) {
				mensaje += '<div>- N&uacute;mero de campo</div>';
			}
			if(!form.estimado.value) {
				mensaje += '<div>- Rendimiento estimado</div>';
			}
			if(!form.rend_campo.value) {
				mensaje += '<div>- Rendimiento campo</div>';
			}
			if(!form.acondicionadora.value) {
				mensaje += '<div>- Planta Acondicionadora</div>';
			}

			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
				$("#errores").css('margin-top', '3em').fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('slow');
				if(confirm('Esta seguro de los datos?\n\tPresione aceptar para continuar \n\tPresione cancelar para revisar los datos')) {
					var imagen = '<img src="images/loader_3a.gif" />';
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
					});
					return true;
				} else {
					return false;
				}
			}

		}

		function showResponse(responseText, statusText, xhr, $form) {
			var msg = '<img src="images/error.png" />';
			switch (responseText) {
				case 'error':
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Error. No se actualizaron los datos</div>',
						timeout : 2500
					});
					$('.post').empty().load('view/certificacion/editar/hoja_cosecha.php');
					break;

				case 'OK' :
					var msg = '<img src="images/check.png" />';
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Los datos fueron actualizados.<br><br>Cargando lista de hojas de cosecha</div>',
						timeout : 2000
					});
					$.post('control/usuario/usuario.ctrl.php', {
						page : 'vcosecha',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		}

	})
</script>
