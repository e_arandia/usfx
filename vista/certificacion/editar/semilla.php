<?php
#if (TRUE){
if (DBConnector::filas()){
		$semilla = DBConnector::objeto();
?>
<h2 class="title">Datos de la Semilla</h2>
<div class="entry">
	<form id="semilla" class="seguimiento">
		
		<div style="margin-bottom: 4em; border: 1px dotted #FFFFFF;">
			<label style="font-weight: bold">Semillera: </label>
			<label ><?php echo $semilla -> semillera; ?></label>
			<label style="font-weight: bold" >Semillerista: </label>
			<label style="float:left;clear:right;margin-right:5em"><?php echo utf8_encode($semilla->nombre).' '.utf8_encode($semilla->apellido)
				?></label>
		</div>
		<div style="margin-top: 0.5em;">
            <label style="float: left;">Comunidad</label>
            <input id="comunidad" name="comunidad" type="text" class="special" value="<?php echo $semilla -> comunidad; ?>"/>
        </div>
		<div style="margin-top: 0.5em;">
			<label style="float: left;">Número de Campo</label>
			<input id="nrocampo" name="nrocampo" type="text" class="special" value="<?php echo $semilla -> nro_campo; ?>"/>
		</div>
		<div>
			<label>Campa&ntilde;a</label>
			<input id="campania" name="campania" type="text" class="special" value="<?php echo $semilla -> campanha; ?>"/>
		</div>
		<div>
			<label>Cultivo</label>
			<input id="cultivo" name="cultivo" type="text" class="special" value="<?php echo $semilla -> cultivo; ?>"/>
		</div>
		<div>
			<label>Variedad</label>
			<input id="variedad" name="variedad" type="text" class="special" value="<?php echo $semilla -> variedad; ?>"/>
		</div>
		<div>
			<label>Categor&iacute;a Sembrada</label>
			<input id="cat_sembrada" name="cat_sembrada" type="text" class="special" value="<?php echo $semilla -> categoria_sembrada; ?>"/>
		</div>
		<div>
			<label>Categor&iacute;a a Producir</label>
			<input id="cat_producir" name="cat_producir" type="text" class="special" value="<?php echo $semilla -> categoria_producir; ?>"/>
		</div>
		<div>
			<label>Cantidad de Semilla Empleada (kg)</label>
			<input id="cant_semilla" name="cant_semilla" type="text" class="special" value="<?php echo $semilla -> cantidad_semilla_empleada; ?>"/>
		</div>
		<?php 
		    if (!empty($semilla->nro_lote)){
		?>
		<div>
			<label>Lote Nro</label>
			<input id="lotenro" name="lotenro" type="text" class="special" value="<?php echo $semilla -> nro_lote; ?>"/>
		</div>
		<?php
		}
		?>
		<div>
			<label>Fecha de Siembra</label>
			<input id="f_siembra" name="f_siembra" type="text" class="special" readonly="true" value="<?php echo $dates -> cambiar_formato_fecha($semilla -> fecha); ?>"/>
		</div>
		<div>
			<label>Plantas por Hect&aacute;reas</label>
			<input id="plantasha" name="plantasha" type="text" class="special" value="<?php echo $semilla -> plantas_por_hectarea; ?>"/>
		</div>
		<div>
			<label>Cultivo Anterior</label>
			<input id="cult_anterior" name="cult_anterior" type="text" class="special" value="<?php echo $semilla -> cultivo_anterior; ?>"/>
		</div>
		<div>
			<label>Superficie Parcela</label>
			<input id="superficie" name="superficie" type="text" class="special" value="<?php echo $semilla -> superficie_parcela; ?>"/>
		</div>
		<div>
			<input id="idSolicitud" name="solicitud" type="hidden" value="<?php echo $semilla -> id_solicitud; ?>"/>
		</div>
		<div>
			<input type="submit" name="submit" class="button-primary ui-state-default ui-corner-all" value="Actualizar"/>
		</div>
	</form>
</div>
</div> 
<?php
}
?> <!--   inicializacion  y envio de datos                -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#f_siembra").calendario('f_siembra');
		$("#semilla").actualizarSemilla();
	})
</script>