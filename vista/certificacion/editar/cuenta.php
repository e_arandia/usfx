<?php
$cuenta = mysql_fetch_object($datos);
?>
<h2 class="title">Cuenta de Semillaristas</h2>
<div class="entry">
	<form id="cuenta">
		<div>
			<label>Responsable</label>
			<input id="responsable" name="responsable" type="text" value="<?php echo $cuenta -> responsable;?>"/>
		</div>
		<div>
			<label>Semillera</label>
			<input size="1" name="semillera" id="semillera" value="<?php echo $cuenta -> semillera;?>"/>
		</div>
		<div>
			<label>Productor</label>
			<input id="productor" name="productor"  value="<?php echo $cuenta -> productor;?>"/>
		</div>
		<div>
			<label>Campa&ntilde;a</label>
			<input id="campania" name="campania"  value="<?php echo $cuenta -> campanha;?>"/>
		</div>
		<div>
			<label>Cultivo</label>
			<input size="1" name="cultivo" id="cultivo"  value="<?php echo $cuenta -> cultivo;?>"/>
		</div>
		<div>
			<label>Variedad</label>
			<input size="1" name="variedad" id="variedad"  value="<?php echo $cuenta -> variedad;?>"/>
		</div>
		<div>
			<label>Categoria Aprobada</label>
			<input id="categoriaAprob" name="categoriaAprob" type="text"  value="<?php echo $cuenta -> categoria_aprobada;?>"/>
		</div>
		<div>
			<label>Superficie Aprobada</label>
			<input id="superficie" class="number" name="superficie" type="text"  value="<?php echo $cuenta->superficie_aprobada?>"/>
		</div>
		<div>
			<label>Superficie Total</label>
			<input id="superficietot" class="number" name="superficietot" type="text"  value="<?php echo $cuenta->superficie_total?>"/>
		</div>
		<div>
			<label>Bolsa / Etiqueta</label>
			<input id="bolsa" class="number" name="bolsaEtiqueta" type="text"  value="<?php echo $cuenta -> bolsa_etiqueta;?>"/>
		</div>
		<div>
			<label>Total</label>
			<input id="total1" class="number" name="total1" type="text"  value="<?php echo $cuenta -> subtotal;?>" />
		</div>
		<div>
			<label>Inscripcion</label>
			<input id="inscripcion" class="number" name="inscripcion" type="text" value="<?php echo $cuenta -> inscripcion;?>"/>
		</div>
		<div>
			<label>Inspeccion de Campo</label>
			<input id="icampo" class="number" name="icampo" type="text" value="<?php echo $cuenta -> inspeccion_campo;?>"/>
		</div>
		<div>
			<label>Analisis, Laboratorio,Etiquetaci&oacute;n</label>
			<input id="anlet" class="number" name="anlet" type="text"  value="<?php echo $cuenta -> analisis_laboratorio_etiqueta;?>"/>
		</div>
		<div>
			<label>Acondicionamiento</label>
			<input id="acondicionamiento" class="number" name="acondicionamiento" type="text"  value="<?php echo $cuenta -> acondicionamiento;?>"/>
		</div>
		<div>
			<label>Plantines</label>
			<input id="plantines" class="number" name="plantines" type="text" value="<?php echo $cuenta -> plantines;?>"/>
		</div>
		<div >
			<label>Total</label>
			<input id="total2" class="number" name="total2" type="text" value="<?php echo $cuenta -> total;?>"/>
		</div>
		<div>
			<label>Fecha</label>
			<input id="f_pago" class="date" name="f_pago" type="text" value="<?php $fecha = $dates->cambiar_formato_fecha($cuenta -> fecha); echo $fecha;?>"/>
		</div>
		<div>
			<label>Monto pagado</label>
			<input id="montoPagado"  class="number" name="montoPagado" type="text" value="<?php echo $cuenta -> monto_pagado;?>" />
		</div>
		<div>
			<label>Saldo Total</label>
			<input id="saldototal" class="number" name="saldot" type="text" value="<?php echo $cuenta->saldo_anterior?>"/>
		</div>
		<div>
			<label>Saldo Campa&ntilde;a Anterior</label>
			<input id="campAnterior" class="number" name="campAnterior" type="text" value="<?php echo $cuenta -> saldo_campanha_anterior;?>"/>
		</div>
		<div>
			<input  class="button-primary ui-state-default ui-corner-all" id="send"name="send" type="submit" value="Actualizar"/>
		</div>
		<input type="hidden" name="icuenta" id="icuenta"  value="<?php echo $cuenta -> id_cuenta;?>">
	</form>
</div>
<?php
include 'error/errores.php';
?>
<!-- inicializacion de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		$("#f_pago").calendario('f_pago');
	});

</script>
<!--     validacion y envio de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		var id = $(".iproductor").val();
		$("#cuenta").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			type : 'post',
			data : {
				page : 'actualizacion',
				tbl : 'cuenta',
				id : id
			},
			resetForm : true,
			beforeSubmit : showRequest,
			success : showResponse
		});
		function showRequest(formData, jqForm, options) {
			var mensaje = '';
			var form = jqForm[0];

			if(!form.semillera.value) {
				mensaje += '<div>- Semillera</div>';
			}
			if(!form.productor.value) {
				mensaje += '<div>- Semillerista</div>';
			}
			if(!form.campania.value) {
				mensaje += '<div>- Campa&ntilde;a</div>';
			}
			if(!form.cultivo.value) {
				mensaje += '<div>- Cultivo</div>';
			}
			if(!form.variedad.value) {
				mensaje += '<div >- Variedad</div>';
			}
			if(!form.categoriaAprob.value) {
				mensaje += '<div>- Bolsa</div>';
			}
			if(!form.superficie.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.superficie.value)) {
				mensaje += '<div>- Superficie Aprobada</div>';
			}
			if(!form.superficietot.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.superficietot.value)) {
				mensaje += '<div>- Superficie Total</div>';
			}
			if(!form.bolsaEtiqueta.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.bolsaEtiqueta.value)) {
				mensaje += '<div>- Superficie Total</div>';
			}
			if(!form.inscripcion.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.inscripcion.value)) {
				mensaje += '<div>- Inscripcion</div>';
			}
			if(!form.icampo.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.icampo.value)) {
				mensaje += '<div>- Inscripci&oacute;n campo</div>';
			}
			if(!form.anlet.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.icampo.value)) {
				mensaje += '<div>- An&aacute;lisis, Laboratorio, Etiquetaci&oacute;n</div>';
			}
			if(!form.f_pago.value) {
				mensaje += '<div>- Fecha</div>';
			}
			if(!form.montoPagado.value || !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(form.montoPagado.value)) {
				mensaje += '<div>- Monto pagado</div>';
			}

			if(mensaje != '') {
				$("#errores div p .mensaje").empty().append('Los siguientes campos son inv&aacute;lidos: <br>' + mensaje);
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores div p .mensaje").empty();
				$("#errores").fadeOut('slow');
				var imagen = '<img src="images/loader_3a.gif" />';
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
					});
					return true;
			}
		}

		function showResponse(responseText, statusText, xhr, $form) {
			switch (responseText) {
				case 'error', '':
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Error. No se actualizaron los datos</div>',
						timeout : 1000
					});
					break;

				case 'OK' :
					$.unblockUI();
					var msg = '<img src="images/check.png" />';
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msg + '<div style="text-align:center">Los datos fueron actualizados.<br><br>Cargando lista de cuentas</div>',
						timeout : 2000
					});
					$.post('control/usuario/usuario.ctrl.php', {
						page : 'vcuentas',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		}

	})
</script>