<h2 class="title">Inscripci&oacute;n de campo Semillero</h2>
<div class="entry">
	<form id="solicitud" class="seguimiento">
		<?php
        $solicitud = DBConnector::objeto();
		?>
		<div>
			<label>N&uacute;mero de Solicitud</label>
			<input id="nro_solicitud" class="not-edit" name="nro_solicitud" placeholder="Introduzca N&uacute;mero Solicitud" value="<?php echo $solicitud->nro_solicitud?>"/>
		</div>
		<div>
			<label>Sistema</label>
			<input id="sistema" class="not-edit" name="sistema" value="<?php echo utf8_encode($solicitud -> sistema); ?>"/>
		</div>
		<div>
			<label>Departamento</label>
			<input id="departamento" class="not-edit" name="departamento" type= "text" value="<?php echo $solicitud -> departamento;?>" />
		</div>
		<div>
			<label>Provincia</label>
			<input id="provincia"name="provincia" type= "text" value="<?php echo utf8_encode($solicitud -> provincia);?>" />
		</div>
		<div>
			<label>Municipio</label>
			<input id="municipio" name="municipio" type="text" value="<?php echo utf8_encode($solicitud -> municipio); ?>"/>
		</div>
		<div>
			<label>Comunidad</label>
			<input id="comunidad" name="comunidad" type="text" value="<?php echo utf8_decode($solicitud -> comunidad); ?>"/>
		</div>
		<div>
			<label>Nombre Semillera</label>
			<input id="semillera"  name="semillera" value="<?php echo $solicitud -> semillera; ?>"/>
		</div>
		<div>
			<label>Nombre semillerista</label>
			<input id="name" name="name" type="text"  value="<?php echo utf8_encode($solicitud -> nombre); ?>" />
		</div>
		<div>
			<label >Apellido semillarista</label>
			<input id="apellido" name="apellido" type="text" value="<?php echo utf8_encode($solicitud -> apellido); ?>"/>
		</div>
		<div>
			<label>Fecha Solicitud</label>
			<input size="30" id="f_solicitud" class="not-edit" name="f_solicitud" readonly="readonly" value="<?php echo $dates -> cambiar_formato_fecha($solicitud -> fecha); ?>"/>
		</div>
		<div style="margin-left:60%">
			<!--<button id='updSolcitud' type="submit">
			    Actualizar
			</button>-->
			<button id="enviar-solicitud" class="btn btn-success" type="submit">
                    <span class="glyphicon glyphicon-refresh"></span>
                    Actualizar
                </button>
                <button id="cancelar-solicitud" class="btn btn-success" type="submit">
                    <span class="glyphicon glyphicon-ban-circle"></span>
                    Cancelar
                </button>
		</div>
		<div>
			<input id="isolicitud"type="hidden" name="isolicitud" value="<?php echo $solicitud->id_solicitud?>" />
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#f_solicitud").calendario('f_solicitud');
	});

</script>
<!--    parte para el autocompletado de las semilleras  -->
<script>
	$(document).ready(function() {
		var cache = {};
		var lastXhr;
		$("#semillera").autocomplete({
			minLength : 2,
			source : "control/index.php?mdl=certificacion&opt=ver&pag=semilleras",
		});
	});

</script>
<!--  validacion y envio de datos  -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#solicitud").ajaxForm({
			url : 'control/index.php',
			type : 'post',
			clearForm : true,
			data : {
				mdl : 'certificacion',
				opt : 'actualizar',
				pag : 'solicitud',
				edt: 1110
			},
			beforeSubmit : showRequest,
			success : showResponse
		});
		function showRequest(formData, jqForm, options) {
			for (var i = 0; i < formData.length; i++) {
				if (!formData[i].value) {
					$.funciones.mostrarMensaje('info','Por favor revise los datos. Existe(n) algun(os) campo(s) vacio(s)');
					return false;
				}
			}
			$.funciones.mostrarMensaje('info', 'Actualizando datos...');
		}

		function showResponse(responseText, statusText, xhr, $form) {

			switch (responseText) {
				case 'error':
					$.funciones.mostrarMensaje('error', 'No se actualizaron los datos');
					$.funciones.ocultarMensaje(5000);
					break;

				case 'OK' :
					$.funciones.mostrarMensaje('ok', 'Los datos fueron actualizados');
					$.funciones.ocultarMensaje(5000);

					$.post('control/index.php', {
						mdl : 'certificacion',
						opt : 'ver',
						pag : 'solicitud',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		}

	}); 
</script>