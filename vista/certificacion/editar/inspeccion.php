<?php
$inspeccion = mysql_fetch_object($datos);
?>
<h2 class="title">Datos Inspeccion</h2>
<div class="entry">
	<div>
		<label style="font-weight: bold">Semillera: </label>
		<label><?php echo $inspeccion -> semillera;?></label>
	</div>
	<div style="margin-bottom: 10px" ></div>
	<div>
		<label  style="font-weight: bold">Productor: </label>
		<label><?php echo utf8_encode($inspeccion -> nombre) . ' ' . utf8_encode($inspeccion -> apellido);?></label>
	</div>
	<div style="margin-bottom: 20px;" ></div>
	<div id="tabs">
		<ul>
			<li>
				<a href="#tabs-1">Inspeccion 1</a>
			</li>
			<li>
				<a href="#tabs-2">Inspeccion 2</a>
			</li>
			<li>
				<a href="#tabs-3">Inspeccion 3</a>
			</li>
		</ul>
		<div id="tabs-1">
			<form id="inspeccion1">
				<div >
					<label>Inspeccion 1</label>
					<?php
					$inspecciones = empty($inspeccion -> primera) ? '' : $dates -> cambiar_formato_fecha($inspeccion -> primera);
					?>
					<input size="10" id="ins_1" name="evegetativa" readonly="readonly" value="<?php echo $inspecciones;?>"/>
					<input type="hidden" name="isuperficie" class="isuperficie" value="<?php echo $inspeccion -> id_inspeccion;?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="send" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
		<div id="tabs-2">
			<form id="inspeccion2">
				<div  >
					<label style="margin-right: 10px;">Inspeccion 2</label>
					<?php
					$inspecciones = empty($inspeccion -> segunda) ? '' : $dates -> cambiar_formato_fecha($inspeccion -> segunda);
					?>
					<input size="10" id="ins_2" name="efloracion" readonly="readonly"  value="<?php echo $inspecciones;?>"/>
					<input type="hidden" name="isuperficie" class="isuperficie" value="<?php echo $inspeccion -> id_inspeccion;?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="send" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
		<div id="tabs-3">
			<form id="inspeccion3">
				<div>
					<label style="margin-right: 10px;">Inspeccion 3</label>
					<?php
					$inspecciones = empty($inspeccion -> tercera) ? '' : $dates -> cambiar_formato_fecha($inspeccion -> tercera);
					?>
					<input size="10" id="ins_3" name="ecosecha" readonly="readonly" value="<?php echo $inspecciones;?>"/>
					<input type="hidden" name="isuperficie" class="isuperficie" value="<?php echo $inspeccion -> id_inspeccion;?>"/>
					<input class="button-primary ui-state-default ui-corner-all" name="send" type="submit" value="Actualizar"/>
				</div>
			</form>
		</div>
	</div>
	<?php
	include 'error/errores.php';
	?>
</div>
<!-- inicializacion de datos-->
<script type="text/javascript">
	$(document).ready(function() {
		$("#ins_1").calendario('ins_1');
		$("#ins_2").calendario('ins_2');
		$("#ins_3").calendario('ins_3');
	});

</script>
<!--   creacion de tabs   -->
<script type="text/javascript">
	$(document).ready(function() {
		// Accordion
		$("#accordion").accordion({
			header : "h2"
		});
		// Tabs
		$('#tabs').tabs();
	});

</script>
<!--  validacion y envio de datos    -->
<script type="text/javascript">
	$(document).ready(function() {
		var id = $(".isuperficie").val();
        var imagen = '<img src="images/loader_3a.gif" style="margin-left:45%"/>';
		$("#inspeccion1").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'inspeccion_1',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest,
			success : showResponse
		});

		$("#inspeccion2").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'inspeccion_2',
				id : id
			},
			type : 'post',
			beforeSubmit : showRequest2,
			success : showResponse
		});

		$("#inspeccion3").ajaxForm({
			url : 'control/certificacion/certificacion.ctrl.php',
			data : {
				page : 'actualizacion',
				tbl : 'inspeccion_3',
				id : id
			},
			type : 'post',
			clearForm : true,
			beforeSubmit : showRequest3,
			success : showResponse
		});
		/*Validacion de superficie inscrita*/
		function showRequest(formData, jqForm, options) {
			var vegetativa = $('input[name=evegetativa]').fieldValue();

			if(vegetativa[0].length == 0) {
				$("input[name=evegetativa]").css({
					backgroundColor : "#f5c9c9"
				});
				$("#errores div p .mensaje").empty().append('Debe introducir fecha de inspecci&oacute;n');
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('fast');
				$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
				});
				return true;
			}
		};

		function showRequest2(formData, jqForm, options) {
			var floracion = $('input[name=efloracion]').fieldValue();

			if(floracion[0].length == 0) {
				$("input[name=efloracion]").css({
					backgroundColor : "#f5c9c9"
				});
				$("input[name=efloracion]").focus();
				$("#errores div p .mensaje").empty().append('Debe introducir fecha de inspecci&oacute;n');
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('fast');
				$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
				});
				return true;
			}
		};

		function showRequest3(formData, jqForm, options) {
			var usernameValue = $('input[name=ecosecha]').fieldValue();
			if(usernameValue[0].length == 0) {
				$("input[name=ecosecha]").css({
					backgroundColor : "#f5c9c9"
				});
				$("input[name=ecosecha]").focus();
				$("#errores div p .mensaje").empty().append('Debe introducir fecha de inspecci&oacute;n');
				$("#errores").fadeIn('slow');
				return false;
			} else {
				$("#errores").fadeOut('fast');
				
				$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : imagen + '<div style="text-align:center">Actualizando datos... <br>Por favor espere</div>'
				});
				return true;
			}
		};

		function showResponse(responseText, statusText, xhr, $form) {
			var error = '<img src="images/error.png" style="margin-left:45%"/>';
			var msgOK = '<img src="images/check.png" style="margin-left:45%"/>';
			switch (responseText) {
				case 'error','':
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : error + '<div style="text-align:center">Error. No se actualizaron los datos<br>Se ha producido un error al intentar actualizar los datos.</div>',
						timeout : 1000
					});
					break;

				case 'OK' :					
					$.unblockUI();
					$.blockUI({
						theme : true,
						title : 'Sistema CERFIS',
						message : msgOK + '<div style="text-align:center">Los datos fueron actualizados.<br><br>Cargando Lista de inspecciones</div>',
						timeout : 2000
					});
					$.post('control/usuario/usuario.ctrl.php', {
						page : 'vinspecciones',
						area : 'administracion'
					}, function(data) {
						$(".post").empty().append(data);
					});
					break;
			}
		};
	})
</script>