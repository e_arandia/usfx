<form class="seguimiento">
	<div style="margin-bottom:7px;">
		<input id="filter" style="width: 400px;border:1px solid #CECECE;height:20px" autocomplete="off" placeholder="Buscar...">
	</div>
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th>Provincia</th>
                <th>Municipio</th>
                <th>Comunidad</th>
                <th>Semillera</th>
                <th>Semillerista</th>
                <th>Cultivo</th>
                <th>Variedad</th>
                <th>Categorias</th>
                <th>Superficies <br>(Has.)</th>
                <th>Superficie Produccion <br>(T.M.)</th>  
			</tr>
		</thead>

		<tbody class="buscar">

		</tbody>
	</table>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        //buscar en los numeros de solicitudes y semilleras
        $('input#filter').on("keyup", function() {
            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function() {
                return rex.test($(this).text());
            }).show();
        });
    }); 
</script>