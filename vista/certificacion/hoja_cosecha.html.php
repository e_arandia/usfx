<h2 class="title">Hoja de Cosecha</h2>
<?php
include '../vista/confirmacion/advertencia.html';
?>
<div class="entry">
        <div style="float: right; margin-top: -10%;">
        <button id="return" class="btn btn-success" type="button">
            <span class="glyphicon glyphicon-arrow-left"> </span>
            Volver
        </button>
    </div>
    <form id="cosecha-buscar" class="seguimiento" >
        <?php
            include '../vista/certificacion/buscador_nros_semilleras_productores.html.php';
        ?>
    </form>
    <form id="seguimiento" class="seguimiento" style="display: none">
        <?php
        include 'seguimiento.html';
        ?>
        <!--
        <div>
            <label>Cultivo:</label>
            <select id="cultivo" name="cultivo" disabled="disabled">
                <option value=""> [Seleccione Cultivo]</option>
            </select>
        </div>
    -->
    </form>
</div>
<div class="entry">
    <form id="cosecha" class="nuevoform" style="display: none;padding: 3% 2% 8%;">
        <div>
            <label>N&uacute;mero Campo</label>
            <select id="campo" name="campo" ></select>
        </div>
        <div id="datos-cosecha" style="display: none;">
            <div>
                <label>Fecha Emision</label>
                <input id="f_emision" class="not-edit" style="width: 67px" name="f_emision" type="text" readonly="readonly" value="<?php echo date('d-m-Y')?>"/>
            </div>
            <div>
                <label>Categoria en Campo</label>
                <input id="categ_campo" name="categ_campo" type="text" style="border-color: #FAFFEE"/>
            </div>
            <div>
                <label style="width: 25%">Rendimiento Estimado </label>
                <input id="rendimientoE" class="number" name="estimado" style="width: 85px" maxlength="5" autocomplete="off"/> TM/Ha.
            </div>
            <div>
                <label style="width:25%">Superficie Parcela</label>
                <input id="sup_parcela" name="sup_parcela" class="number not-edit" style="width:60px"/> Ha.
            </div>
            <div>
                <label>Rendimiento Campo</label>
                <input id="rend_campo" class="number not-edit" name="rend_campo" style="width: 85px" maxlength="5" autocomplete="off" value="0"/>TM
            </div>
            <!--
            <div>
                <label>Numero Cupones</label>
                <input id="nro_cupones" name="nro_cupones" style="width: 55px" value="0"/>
            </div>
            <div>
                <label>Rango Cupones</label>
                <input id="rc-from" class="number" name="rc-from" style="width:53px" placeholder="desde" maxlength="5" autocomplete="off"/>
                <span>&nbsp;-&nbsp;</span>
                <input id="rc-to" class="not-edit" name="rc-to" style="width:53px" placeholder="hasta" maxlength="5"/>
            </div>
        -->
            <div>
                <label style="width: 25%;">Planta Acondicionadora</label>
                <input id="plantaa" name="plantaa" autocomplete="off"/>
            </div>
            <div>
                <label style="margin-top:5%;width:25%">Realizar analisis en laboratorio?</label>
            </div>
            <div id="laboratorio">
                    <input type="radio" id="silab" name="laboratorio" value="1"  checked="checked" />
                    <label for="silab" style="width: 23%">SI</label>
                    <input type="radio" id="nolab" name="laboratorio" value="0"/>
                    <label for="nolab" style="width: 23%;">NO</label>
            </div>

            <div>
                <button id="enviar-cosecha" class="btn btn-success">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Registrar
                </button>
            </div>
        </div>
        <input id="hiddenArea" name="hiddenArea" type="hidden" value="<?php echo $_SESSION['usr_iarea']?>"/>
        <input type="hidden" name="iSuperficie" id="iSuperficie"/>
        <input type="hidden" name="iSolicitud" id="iSolicitud"/>
        <input type="hidden" name="iSemilla" id="iSemilla"/>
        <input type="hidden" name="estado" id="iEstado" />
        <input type="hidden" name="indice" id="indice" />
        <input type="hidden" name="generacion" id="igeneracion" />
        <input type="hidden" name="icategoria" id="icategoria" />
        <input type="hidden" name="hiddencultivo" id="hiddencultivo" />
        <input type="hidden" name="noparcelas" id="noparcelas" />
    </form>
    <?php
    include '../vista/error/errores.php';
    include '../vista/error/aviso.php';
    include '../vista/dialogos/calcularMaiz.html';
    include '../vista/dialogos/calcularPapa.html';
    include '../vista/dialogos/calcularTrigoCebada.html';
    include '../vista/dialogos/calcularSoya.html';
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //apariencia de cuadro de busqueda
        $("form#superficie-buscar.seguimiento").css( "width", "80%" );
        $("form#superficie-buscar.seguimiento>div#anteriores>div>label").css("width","15%");
        $("input#buscarTXT").css( "width", "69%" );
        $.buscar.autocompletarCosecha();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        
        //RESULTADO DE BUSQUEDA 
        $("#lbl-nro").css('width','35%');
        $("#nosolicitud").css('width','50%');
        
        $("#lbl-slr").css('width','20%');
        $("#semillerista").css('width','70%');
        
        $("#lbl-smr").css('width','25%');
        $("#comunidad").css('width','70%');
        
        $("#lbl-cmd").css('width','25%');
        $("#comunidad").css('width','70%');
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //busqueda segun cadena ingresada
        $('input#buscarTXT').on('keyup', function(event) {
            if (event.which == 13)
                $.buscar.hojaCosechaCertifica();
        });
        $("button#btn-search").on("click", function(event) {
            $.buscar.hojaCosechaCertifica();
        });
        $("input#rendimientoE").on("click",function(){
            $.funciones.calculadoraRendimientoCultivo();    
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //return button
        $("button#return").on("click", function() {
            edt = $(".crud").val();
            $(".informar").empty();
            $.get('control/index.php', {
                mdl : 'certificacion',
                opt : 'ver',
                pag : 'cosecha',
                area : 1,
                edt : edt
            }, function(data) {
                $.funciones.ocultarMensaje(500);
                $(".post").empty().append(data);
            });
        });
        //numero de cupones
        $("input#nro_cupones").spinner({
            min : 0,
            max : 9999
        });
        //opcion laboratorio
        $("#laboratorio").buttonset();
        //calendario
        $("#f_emision").calendarioSolicitud('f_emision');
        $("#f_emision").css('margin-right', '10px');

        $("select[name=cultivo]").css('visibility', 'hidden');
        $("input#hiddencultivo").empty().val($(this).val());
               
        //permitir solo numeros en
        solo_numeros = ['rendimientoE', 'rend_campo', 'nro_cupones'];
        $.funciones.soloNumeros(solo_numeros);
        $.funciones.calculadoraRendimientoCultivo();

    }); 
</script>
<!--  validacion y envio de datos mediante ajax       -->
<script type="text/javascript">
    $(document).ready(function() {
        $("button#enviar-cosecha").addClass("button-primary")
        $("#cosecha").enviarHojaCosecha();
    })
</script>
