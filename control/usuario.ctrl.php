﻿<?php

/**
 * registrar
 * modificar
 * eliminar
 * ver datos
 * act datos
 * realizar bkp
 * rest. bkp
 * listar bkp
 * camb. pass
 * realizar graficos
 * */

if (isset($_GET['pag'])) {
    $pagina = $_GET['pag'];
} else {
    $pagina = $_POST['pag'];
}

switch ($pagina) {
    //formulario de registro de cualquier visitante
    case 'nuevo' :
        echo 'usuario.php';
        break;
    //busqueda de areas en el sistema 
    case 'ver':
        $opcion = $_GET['opt'];
        switch ($opcion) {
            case 'area':
                $json = array();
                Area::getAreas();
                while ($obj = DBConnector::resultado()) {
                    $json_tmp['id'] = $obj->id_area;
                    $json_tmp['sistema'] = $obj->nombre_area;
                    array_push($json,$json_tmp);
                }
                
                echo json_encode($json);
                break;
        }
    break;
    //registra nuevo usuario en el sistema
    case 'registrar' :
        $nombre = ucfirst($_POST['nombre']);
        $apellido = ucfirst($_POST['apellido']);
        $login = strtolower(trim($_POST['login']));
        $contrasena = $_POST['pass'];
        $area = intval($_POST['area']);
        $nivel = $area;
        $usuario = intval($_POST['usuario']);

        if (!empty($login) && !empty($contrasena)) {
            if ($usuario != 10) {
                $usuarios -> setUsuario($usuario, $nombre, $apellido, $login, $contrasena, $nivel);
            } else {
                $usuarios -> setUsuario($usuario, $nombre, $apellido, $login, $contrasena, $nivel);
            }
//echo DBConnector::mensaje();
            if (!DBConnector::nroError())
                echo 'OK';
        } else
            echo DBConnector::mensaje();
        break;
    //modificar nombre y apellido de usuario
    case 'actualizar' :
        $nombre = trim($_POST['nombre']);
        $apellido = trim($_POST['apellido']);
        $id = $_POST['iusuario'];
        $usuarios -> updateInfo($nombre, $apellido, $id);
        if (!DBConnector::nroError())
            echo 'OK';
        else
            echo 'error';
        break;
    //cambiar password
    case 'cambiar' :
        $id = $_POST['iusuario'];
        $newpass = $_POST['password'];
        $newpass2 = $_POST['password2'];
        $error = $usuarios -> checkPassword($newpass, $newpass2);

        if ($error) {
            $usuarios -> update_password($id, $newpass);
            if (!DBConnector::nroError()) {
                echo 'OK';
            } else {
                echo DBConnector::mensaje();
            }
        } else
            echo 'contrasenhas';
        break;
    //generar nuevo password al azar
    case 'generar_pass' :
        #$dates -> noCache();
        $json['password'] = $dates -> generar_password();
        
        echo json_encode($json);
        break;
    //acreedores
    case 'acreedores' :
        include '../vista/reporte/acreedores.php';
        break;
    //formulario reporte
    case 'grafico' :
        session_start();
        include '../vista/reporte/grafico.php';
        break;
    //realizar reporte
    case 'reporte_res' :
        break;
    //registro de operaciones realizadas
    case 'bitacora' :
        $limite = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $bitacora -> getBitacora();
        //total de registros
        $total = DBConnector::filas();
        $nroRegistros = 15;
        $blocks = $bitacora -> getBitacoraByBlock($limite, $nroRegistros);
        
        include '../vista/login/bitacora.php';
        break;
    case 'respaldar' :
        $sel_maximo = "SELECT max(id) as maximo FROM tabbackup";
        $rs_maximo = mysql_query($sel_maximo);
        $identif = mysql_result($rs_maximo, 0, "maximo");
        $identif++;
        if (!empty($_POST['nombreArchivo']))
            $nombre = $_POST['nombreArchivo'] . '_' . $identif;
        else
            $nombre = date('dmHis');
        $fecha = date('Y-m-d');
        $hora = date('H:i:s');

        $insertCopy = $backup -> insertCopia($nombre, $fecha, $hora, $conection);
        $error = $usuarios -> respaldar_base_datos($nombre, $dbuser, $dbpass, $dbname, $link);

        if (empty($error) && $insertCopy)
            echo 'OK';
        else
            echo 'Ha ocurrido un error la copia de seguridad no ha sido creada. ';
        break;
}
?>