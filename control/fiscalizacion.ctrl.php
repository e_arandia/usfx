<?php
$opcion = strip_tags($_REQUEST['opt']);
$pagina = isset($_REQUEST['pag']) ? strip_tags($_REQUEST['pag']) : '-';
#echo $opcion.'='.$pagina;
switch ($opcion) {
    case 'new' :
        switch ($pagina) {
            case 'buscar' :
                #buscador de fiscalizacion
                session_start();
                include '../vista/busqueda/buscador.html.php';
                break;
            case 'solicitud' :
                #formulario de solicitud
                session_start();
                include '../vista/fiscalizacion/solicitud.html.php';
                break;
            case 'muestras' :
                #nueva muestra de fiscalizacion
                $area = Sistema::getIdSistema('fiscaliza');
                $laboratorio -> getMuestras('', $area);
                include '../vista/fiscalizacion/datos/muestras.html.php';
                break;
            case 'muestra' :
                #muestra de laboratorio
                $nivel = isset($_GET['nivel']) ? intval($_GET['nivel']) : 0;
                include '../vista/fiscalizacion/laboratorio_fis.php';
                break;
            case 'fiscalizar' :
                #datos de fiscalizacion
                include '../vista/fiscalizacion/datos.php';
                break;
            case 'recepcion' :
                #formulario de recepcion
                include '../vista/fiscalizacion/laboratorio.php';
                break;
            case 'semilla' :
                #formulario de semilla
                session_start();
                include '../vista/fiscalizacion/semilla.html.php';
                break;
            case 'cosecha' :
                #formulario de hoja de cosecha
                include '../vista/fiscalizacion/hoja_cosecha.html.php';
                break;
            case 'semillaP' :
                #nuevo formulario de semilla producida
                include '../vista/fiscalizacion/semilla_prod.html.php';
                break;
            case 'cuenta' :
                #nuevo formulario de cuenta
                session_start();
                include '../vista/fiscalizacion/cuenta.html.php';
                break;
            case 'seguimiento' :
                #imagen de seguimiento
                include '../vista/menu/seguiFiscal.php';
                break;
            case 'segSol' :
                #etapa solicitud
                echo "<img src='images/checkSol.png' width='75%'/>";
                break;
            case 'segSem' :
                #tipo de semilla a sembrar
                echo "<img src='images/checkSem.png' width='75%'/>";
                break;
            case 'segCos' :
                #Hoja de cosecha
                echo "<img src='images/checkCos.png' width='75%'/>";
                break;
            case 'segProd' :
                #semilla producida
                echo "<img src='images/checkProd.png' width='75%'/>";
                break;
            case 'segCta' :
                #estado de cuenta
                echo "<img src='images/checkCta.png' width='75%'/>";
                break;
        }

        break;
    case 'guardar' :
        switch ($pagina) {
            case 'solicitud' :
                $nombre = utf8_decode(ucwords(trim($_POST['name'])));
                $apellido = (is_string($_POST['apellido']) && !empty($_POST['apellido'])) ? utf8_decode(ucwords(trim($_POST['apellido']))) : '';
                $tmp_comunidad = $dates -> addSpace($_POST['comunidad']);
                //var_dump($tmp_comunidad);
                $comunidad = $dates -> specialWord($tmp_comunidad);
                $id_municipio = intval(trim($_POST['hdn_municipio']));
                $nro_solicitud = trim($_POST['nro_solicitud']);
                $id_sistema = $solicitudes -> getIdSistema(trim($_POST['sistema']));
                $fecha = $dates -> cambiar_tipo_mysql(trim($_POST['f_solicitud']));
                $noparcelas = trim($_POST['noparcelas']);
                #numero de parcelas
                $nroComunidades = (is_array($comunidad) && count($comunidad) > 2) ? $dates -> contarMuniLocal($comunidad) : 1;
                $id_semillera = Semillera::setSemilleraFiscal(strtoupper($_POST['semillera']), $nro_solicitud);

                if ($nroComunidades == 1) {
                    if ($noparcelas == 1) {
                        $semillerista = new Semillerista($nombre, $apellido, $id_semillera);
                        $id_semillerista = $semillerista -> setSemillerista();
                        $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($comunidad[0]), $fecha);
                    } else {
                        $semillerista = new Semillerista($nombre, $apellido, $id_semillera);
                        $id_semillerista = $semillerista -> setSemillerista();
                        $i = 1;
                        while ($noparcelas >= $i) {
                            $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($comunidad[0]), $fecha);
                            $i++;
                        }
                    }
                    echo 'OK';
                }
                break;
            case 'semilla' :
                $nro_campo = trim($_POST['hiddenNoSolicitud']);
                $cultivo = trim(ucfirst($_POST['cultivo']));
                $nombre_variedad = $dates->specialWord(trim($_POST['variedad']));
                $categoria = !empty($_POST['cgeneracion']) ? ucfirst(strtolower($_POST['cgeneracion'])) : 'Sin especificar-0';
                $nro_lote = strtoupper(trim($_POST['lotenro']));

                $isolicitud = trim($_POST['isolicitud']);

                $icultivo = $cultivos -> setCheckCultivo(trim(ucfirst($_POST['cultivo'])));
                $ivariedad = $variedad -> setCheckVariedad($nombre_variedad, $icultivo);

                $temp_categoria = explode('-', $categoria);
                $nombre_generacion = is_int($temp_categoria[1]) ? $temp_categoria[1] : strtoupper($temp_categoria[1]);

                $icategoria = $generacion -> getIdGeneracionByName($nombre_generacion);

                $sistema = isset($_POST['sistema']) ? intval($_POST['sistema']) : '';

                $semilla -> insertSemillaFiscal($nro_campo, $icultivo, $ivariedad, $icategoria, $nro_lote);
                if (!DBConnector::nroError()) {
                    //id de semilla
                    $iSemilla = DBConnector::lastInsertId();
                    //agregamos valores a tabla de relacion
                    Tiene::solicitud_semilla($isolicitud, $iSemilla);

                    //actualizamos estado de solicitud
                    $solicitudes -> updateEstadoFiscal($isolicitud, 1);
                    echo 'OK';
                } else {
                    echo DBConnector::mensaje();
                }
                break;

            case 'cosecha' :
                $acondicionadora = !empty($_POST['plantaa']) ? trim($_POST['plantaa']) : '-';
                $idSolicitud = intval($_POST['iSolicitud']);

                $idSemilla = Tiene::getIdSemilla($idSolicitud);
                $cultivo = Semilla::getCultivoByIdSolicitudFiscaliza($idSolicitud); 
                if ($cultivo != 'Papa'){
                    Superficie::setSupFiscal($idSemilla);
                    $idSuperficie = DBConnector::lastInsertId();
                    $cosechas -> insertHojaCosechaFiscal($idSemilla, 1,$acondicionadora, $idSuperficie);    
                }else{
                    Superficie::setSupFiscal($idSemilla);
                    $idSuperficie = DBConnector::lastInsertId();
                    $cosechas -> insertHojaCosechaFiscal($idSemilla, 0,$acondicionadora, $idSuperficie);
                }
                
                if (!DBConnector::nroError()) {
                    $solicitudes -> updateEstadoFiscal($idSolicitud, 8);
                    echo "OK";
                } else
                    echo DBConnector::mensaje();
                break;

            case 'muestra' :
                //cambiamos la fecha al tipo de mysql
                $muestraFiscal['fecha_recepcion'] = $dates -> cambiar_tipo_mysql(trim($_POST['recepcion']));
                $muestraFiscal['cultivo'] = trim($_POST['cultivo']);
                $muestraFiscal['variedad'] = trim($_POST['variedad']);
                $campo = trim($_POST['nrocampo']);
                $muestraFiscal['nrocampo'] = ($dates -> buscaBarra($campo) == 2) ? Semilla::getIdSemillaMuestraByNroCampoFiscal($campo) : Semilla::getIdSemillaByNroCampo($campo);

                $germinacion = isset($_POST['germinacion']) ? intval($_POST['germinacion']) : 0;
                $humedad = isset($_POST['humedad']) ? intval($_POST['humedad']) : 0;
                $pureza = isset($_POST['pureza']) ? intval($_POST['pureza']) : 0;
                $muestraFiscal['sistema'] = intval($_POST['sistema']);
                $muestraFiscal['analisis'] = Laboratorio::getIdTipoAnalisis($germinacion, $humedad, $pureza);
                $muestraFiscal['origen'] = strip_tags($_POST['origen']);
                $tipo_semilla = ($muestraFiscal['sistema'] == 'fiscalizacion') ? trim($_POST['tipo_semilla']) : '';
                $muestraFiscal['nro_analisis'] = Laboratorio::getAnalisisCount()+1;
                #var_dump($muestra);exit;
                if ($tipo_semilla == 'importada' && $muestraFiscal['sistema'] == 2) {
                    $muestraFiscal['tipo_semilla'] = (strtolower(trim($_POST['tipoSemilla'])) == 'importada') ? 1 : 0;
                    $muestraFiscal['especieImp'] = trim($_POST['especieImp']);
                    $muestraFiscal['variedadImp'] = trim($_POST['variedadImp']);
                    $muestraFiscal['categoriaImp'] = trim($_POST['categoriaImp']);
                    $muestraFiscal['origenImp'] = trim($_POST['origenImp']);
                    $muestraFiscal['cantidadImp'] = trim($_POST['cantidadImp']);
                    $muestraFiscal['fiscalizadoImp'] = trim($_POST['fiscalizadoImp']);
                    $muestraFiscal['aduanaImp'] = trim($_POST['aduanaIngresoImp']);
                    $muestraFiscal['destinoImp'] = trim($_POST['destinoSemillaImp']);
                    $muestraFiscal['distribucionImp'] = trim($_POST['adistribucionImp']);                    
                    $laboratorio -> setMuestraImportada($muestraFiscal);
                } else {
                    $laboratorio -> setMuestra($muestraFiscal);
                }
                #echo DBConnector::mensaje();exit;
                if (!DBConnector::nroError()) {
                    $isolicitud = Solicitud::getIdSolicitudMuestraFiscalLaboratorioByNroSolicitud($campo);
                    Solicitud::updateEstadoMuestra($isolicitud);
                    echo 'OK';
                } else
                    echo DBConnector::mensaje();
                break;
            case 'semillap' :
                $semillaNeta = $_GET['semillaNeta'];
                $categoria = $_GET['categoriaObt'];
                $noEtiqueta = $_GET['etiquetas'];
                $icosecha = !empty($_GET['icosecha']) ? $_GET['icosecha'] : '';
                $ilab = $_GET['ilaboratorio'];
                $isolicitud = $_GET['isolicitud'];

                $producciones -> setSemillaProdFiscal($semillaNeta, $categoria, $noEtiqueta, $icosecha, $ilab);
                if (!DBConnector::nroError()) {
                    Solicitud::updateEstadoFiscal($idSolicitud, 10);
                    echo "OK";
                } else
                    echo DBConnector::mensaje();
                break;
            //guarda la cuenta de fiscalizacion
            case 'cuenta' :
                $id_semillera = Semillera::getIdSemillera($_POST['semillera']);
                $productor = Semillerista::getIdSemilleristaByFullName(utf8_decode(ucwords(trim($_POST['productor']))));

                $responsable = empty($_POST['responsable']) ? Responsable::setReponsable(utf8_decode(ucwords(trim($_POST['productor'])))) : Responsable::getIdResponsable(utf8_decode($_POST['responsable']));
                //          if (isset($_POST['semillera']) && isset($_POST['campania']) && isset($_POST['cultivo']) && isset($_POST['variedad'])) {         $semillera = $_POST['semillera'];

                #echo DBConnector::mensaje();
                $cultivo = $_POST['cultivo'];
                $variedad = Variedad::getIdVariedad($_POST['variedad']);
                #echo DBConnector::mensaje();

                $bolsaEtiqueta = trim($_POST['bolsaEtiqueta']);

                $sbtotal = empty($_POST['total']) ? trim($_POST['total']) : 0;

                $total = !empty($_POST['total2']) ? trim($_POST['total2']) : 0;

                $fecha = trim($_POST['f_pago']);

                $monto_pagado = trim($_POST['montoPagado']);

                $isolicitud = intval($_POST['iSolicitud']);
                $isemilla = Tiene::getIdSemilla($isolicitud);
                #echo DBConnector::mensaje();
                $acondicionamiento = $_POST['acondicionamiento'];
                $plantines = trim($_POST['plantines']);
                $saldoTotal = !empty($_POST['saldototal']) ? intval(trim($_POST['saldototal'])) : 0;
                #Datos necesarios para costo
                $inscripcion = trim($_POST['inscripcion']);
                $inspeccion_campo = trim($_POST['icampo']);
                $analisis_laboratorio_etiqueta = trim($_POST['anlet']);

                $fecha = $dates -> cambiar_tipo_mysql($fecha);

                $cuenta -> setCuentaFiscal($responsable, $id_semillera, $productor, $variedad, $bolsaEtiqueta, $sbtotal, $total, $fecha, $saldoTotal, $isemilla);
                if (!DBConnector::nroError()) {
                    /*$search = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ', 'ñ');
                     $productor = str_ireplace($search, '_', $productor);
                     $aux = explode(' ', $productor);
                     $nombre[1] = $aux[0];
                     $nombre[2] = $aux[1];*/
                    Solicitud::updateEstadoCuenta($isolicitud, 12);

                    echo 'OK';
                } else {
                    echo DBConnector::mensaje();
                }
                break;
        }
        break;
    case 'ver' :
        switch ($pagina) {
            case 'colugar' :
                $q = isset($_GET['term']) ? trim($_GET['term']) : '';
                Solicitud::getComunidadByNombre($q);
                $coincidencias = array();
                while ($fila = DBConnector::objeto())
                    array_push($coincidencias, $fila -> comunidad);
                echo json_encode($coincidencias);

                break;
            case 'comunidad' :
                $id = intval($_GET['isolicitud']);
                $solicitudes -> getComunidadById($id);
                //$comunidades = array();
                $fila = DBConnector::objeto();
                //var_dump($fila);
                $resultado['comunidad'] = $fila -> comunidad;

                echo json_encode($resultado);
                break;
            #paginar hoja de cosecha
            case 'cosecha' :
                $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;

                //realizar consulta sql
                $cosechas -> getCosechaSolicitudFiscal($area);

                $block = $cosechas -> getCosechaSolicitudFiscalByBlock($area, $limite, $nroRegistros);
                $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                include '../vista/fiscalizacion/datos/cosecha.php';
                break;
            case 'isolicitud' :
                //id de solicitud segun nombre-apellido-semillera
                $nombre = $dates -> modificar($_GET['nombre']);
                $apellido = $dates -> modificar($_GET['apellido']);
                $semillera = $dates -> modificar($_GET['semillera']);

                $solicitudes -> getIDSolicitudOperacionesFiscal($nombre, $apellido, $semillera);
                $row = DBConnector::objeto();
                $json['isolicitud'] = $row -> id_solicitud;

                echo json_encode($json);
                break;
            case 'lst_nosolicitudes' :
                // devuelve los nros de solicitud coincidentes
                $q = trim($_GET['term']);
                //$estado = intval($_GET['estado']);
                Solicitud::getNroSolicitudByNroSolicitud($q);
                $json = array();
                while ($obj = DBConnector::objeto()) {
                    array_push($json, $obj -> nro_solicitud);
                }
                echo json_encode($json);
                break;
            case 'lst_productores' :
                $tabla = $_GET['tabla'];
                $semillera = $_GET['semillera'];
                $id_area = $_GET['area'];
                $dates -> noCache();
                Solicitud::getProductoresSemillera(Semillera::getIdSemillera(str_replace('*', '_', $semillera)), $tabla, $id_area);
                $json = array();
                if (DBConnector::filas() >= 1) {
                    while ($fila = DBConnector::objeto()) {
                        array_push($json, utf8_encode($fila -> nombre));
                    }
                }

                echo json_encode($json);
                break;
            // nombre completo de semillerista
            case 'lst_semillerista' :
                $q = trim($_GET['term']);
                Semillerista::getFullNameByNombre($q);
                $json = array();
                while ($obj = DBConnector::objeto()) {
                    array_push($json, utf8_encode($obj -> semillerista));
                }

                echo json_encode($json);
                break;

            case 'lst_semilleras' :
                //lista de semilleras
                $semilleras = array();
                $etapa = trim($_GET['tbl']);
                $sistema = trim($_GET['std']);
                $solicitudes -> getSemillerasBySistema($etapa, $sistema);
                $json = array();
                while ($row = DBConnector::objeto()) {
                    array_push($json, $row -> semillera);
                }
                echo json_encode($json);
                break;
            //lista de todos los municipios de una provincia
            case 'municipio' :
                $prov = utf8_encode(intval($_GET['prov']));
                #echo $prov;exit;
                $solicitudes -> getMunicipios($prov);
                if (!DBConnector::nroError()) {
                    while ($fila = DBConnector::objeto())
                        $json[$fila -> id_municipio] = ($fila -> nombre_municipio);
                } else {
                    echo json_encode(DBConnector::mensaje());
                }
                echo json_encode($json);
                break;
            //nombres que coinciden con la busqueda
            case 'nombres' :
                $json = array();

                $q = isset($_GET['term']) ? trim($_GET['term']) : '';
                Solicitud::getNombres($q);
                while ($obj = DBConnector::objeto()) {
                    array_push($json, utf8_encode($obj -> nombre));
                }
                echo json_encode($json);
                break;
            case 'nro' :
                $sistema = $_GET['pro'];
                $estado = $_GET['estado'];

                //si nombre de la semillera no existe la variable es vacia
                $solicitudes -> view_getNroSolicitudFiscal();
                $fila = DBConnector::resultado();
                if (!empty($fila -> nro_solicitud)) {
                    $aux = explode("/", $fila -> nro_solicitud);
                    $temp = $aux[0];
                } else {
                    $temp = 0;
                }
                $json['nro'] = ++$temp;
                $json['anho'] = date('y');
                $json['estado'] = 'nuevo';

                echo json_encode($json);
                break;
            //lista todas las provincias de un departamento
            case 'provincia' :
                $solicitudes -> getProvincias();
                while ($fila = DBConnector::objeto()) {
                    $json[$fila -> id_provincia] = ($fila -> nombre_provincia);
                }
                echo json_encode($json);
                break;
            //todos los datos de fiscalizacion para cuadro de resumen
            case 'proceso' :
                $inicio = (isset($_GET['inicio'])) ? trim($_GET['inicio']) : 'A';
                $final = (isset($_GET['inicio'])) ? trim($_GET['final']) : 'F';
                $gestion = date('Y');
                $sistema = Sistema::getIdSistema($_GET['sistema']);
                Solicitud::getSemilleras($sistema);
                //nombre de las semilleras
                $semillera = array();
                //nombre de los semilleristas
                $productor = array();
                //cultivo de semillerista
                $id_solicitud = array();
                //estado de solicitud
                $etapa = array();

                //total de semilleristas
                $cantidad = array();
                Solicitud::getSemilleras($sistema, $inicio, $final, $gestion);
                while ($fila = DBConnector::objeto()) {
                    array_push($semillera, utf8_encode($fila -> semillera));
                }

                $counter = 0;
                $limit = count($semillera);
                foreach ($semillera as $key => $semillera_valor) {
                    Solicitud::getCantidad($semillera_valor, 'fiscaliza');
                    while ($aux = DBConnector::objeto()) {
                        array_push($cantidad, $aux -> cantidad);
                    }
                    Solicitud::getProductoresFiscal($dates -> search_tilde($semillera_valor));
                    while ($fila = DBConnector::objeto()) {
                        $nombre = utf8_encode($fila -> semillerista);
                        #echo $nombre,'?',$fila -> semillerista;
                        array_push($productor, $dates -> cambiar_acentoBySigno($nombre));
                        array_push($etapa, $fila -> estado_solicitud);
                        array_push($id_solicitud, $fila -> id_solicitud);
                    }
                }
                /*
                 foreach ($semillera as $key => $semillera_value) {
                 $counter = 0;
                 #echo $counter,'=',$cantidad[$key];
                 while ($counter < $cantidad[$key]){
                 #echo $productor[$counter],'=',$etapa[$counter],'=',$id_solicitud[$counter];
                 $counter++;
                 }
                 }
                 */
                include '../vista/fiscalizacion/datos/fiscalizacion.php';
                break;
            #paginar semilla producida
            case 'produccion' :
                    $area = $_REQUEST['area'];
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;

                //realizar consulta sql
                $producciones -> getSemillaProd();
                $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                include '../vista/fiscalizacion/datos/semilla_producida.php';
                break;

            case 'registro' :
                //registro de semillerista
                $id = intval($_GET['id']);
                Solicitud::getRegistroFiscal($id);
                $obj = DBConnector::objeto();
                $json['semillerista'] = utf8_encode($obj -> semillerista);
                $json['semillera'] = $obj -> semillera;
                $json['nosolicitud'] = $obj -> nro_solicitud;
                $json['estado'] = $obj -> estado;
                $json['departamento'] = $obj -> nombre_departamento;
                $json['provincia'] = $obj -> provincia;
                $json['municipio'] = $obj -> municipio;
                $json['comunidad'] = $obj -> comunidad;
                $json['fsolicitud'] = $dates -> cambiar_formato_fecha($obj -> fecha);
                Semilla::getRegistroFiscal($id);
                if (DBConnector::filas()) {
                    $obj = DBConnector::objeto();
                    $json['cultivo'] = $obj -> cultivo;
                    $json['variedad'] = ucfirst($obj -> variedad);
                    $json['lote'] = $obj -> lote;
                } else {
                    $json['cultivo'] = $json['variedad'] = '-';
                    $json['catProducir'] = '-';
                    $json['lote'] = '-';
                }
                HojaCosecha::getRegistroFiscal($id);
                if (DBConnector::filas()) {
                    $obj = DBConnector::objeto();
                    $json['plantaa'] = $obj -> planta_acondicionadora;
                } else {
                    $json['plantaa'] = '-';
                }
                SemillaProd::getRegistroFiscal($id);
                if (DBConnector::filas()) {
                    $obj = DBConnector::objeto();
                    $json['catSembrada'] = $obj -> categoria_sembrada;
                    $json['semilla_neta'] = $obj -> semilla_neta;
                    $json['noetiquetas'] = $obj -> nro_etiqueta;
                } else {
                    $json['catSembrada'] = '-';
                    $json['semilla_neta'] = 0;
                    $json['noetiquetas'] = 0;
                }
                echo json_encode($json);
                break;
            case 'semillera' :
                //coincidencia de semilleras por nombre
                $q = isset($_GET['term']) ? trim($_GET['term']) : '';

                Semillera::getNombreSemilleraBy($q);
                $coincidencias = array();
                while ($fila = DBConnector::objeto())
                    array_push($coincidencias, $fila -> semillera);

                echo json_encode($coincidencias);
                break;
            #paginar solicitudes de fiscalizacion
            case 'solicitud' :
                $area = $_REQUEST['area'];
                $page = isset($_GET['page'])?intval($_GET['page']):1;
                    $edit = ($area==10)?1110:1100;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //total de solicitudes
                    $solicitudes -> getSolicitudes($area);
                    //lote segun el nro de registros a mostrar 
                    $blocks = $solicitudes -> getSolicitudesbyBlock($area, $limite, $nroRegistros);
                include '../vista/fiscalizacion/datos/solicitud.php';
                break;
            //autocompletado segun nro de solicitud,semillera o semillerista 
            case 'solicitud_semillera' :
                $opcion = trim($_GET['opc']);
                switch ($opcion) {
                    //hoja cosecha
                    case 'nro' :
                        $nosolicitud = trim($_GET['solicitud']);
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;

                        Solicitud::getSolicitudByNroSolicitudAndEstado($nosolicitud, $estado);
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['semillerista'] = array();
                        $jsonFiscal['comunidad'] = $jsonFiscal['nosolicitud']=array();
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad']=array();
                        if (DBConnector::filas()) {                            
                            while ($obj = DBConnector::objeto()) {
                                #echo $obj->comunidad,'=',utf8_encode($obj->comunidad),'=',utf8_decode($obj->comunidad),'=',html_entity_decode($obj->comunidad);
                                array_push($jsonFiscal['nosolicitud'], $obj -> id_solicitud);
                                array_push($jsonFiscal['semillera'], utf8_encode($obj -> semillera));
                                array_push($jsonFiscal['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($jsonFiscal['comunidad'], $obj -> comunidad);
                            }
                            if ($estado > 0){
                                foreach ($jsonFiscal['nosolicitud'] as $key => $id_solicitud) {
                                    $cultivo = Semilla::getCultivoByIdSolicitudFiscaliza($id_solicitud);
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = utf8_encode(Semilla::getVariedadByIdSolicitudFiscal($id_solicitud));
                                    array_push($jsonFiscal['variedad'],($variedad));    
                                }
                            }
                        }
                        $jsonFiscal['total'] = count($jsonFiscal['nosolicitud']);
                        echo json_encode($jsonFiscal);
                        break;
                    //hoja cosecha
                    case 'semillera' :
                        $semillera = trim($_GET['semillera']);
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        if ($estado>0){
                            Solicitud::getSolicitudBySemilleraAndEstado($semillera, $estado, 'fiscaliza');
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolista'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillerista'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad'] = array();
                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'], $obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'], $obj -> nro_solicitud);
                                array_push($jsonFiscal['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($jsonFiscal['comunidad'], $obj -> comunidad);
                                array_push($jsonFiscal['cultivo'], $obj -> cultivo);
                                array_push($jsonFiscal['variedad'], utf8_encode($obj -> variedad));
                            }
                        }
                        }else{
                            //datos para semilla
                            Solicitud::getSolicitudBySemilleraAndEstado($semillera, $estado, 'fiscaliza');
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolista'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillerista'] = array();
                        $jsonFiscal['comunidad'] = array();
                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'], $obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'], $obj -> nro_solicitud);
                                array_push($jsonFiscal['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($jsonFiscal['comunidad'], $obj -> comunidad);
                            }
                        }
                        }
                        $jsonFiscal['total'] = DBConnector::filas();
                        echo json_encode($jsonFiscal);
                        break;
                    //opcion semilla funciona
                    case 'productor_semilla' :
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();  
                        $jsonFiscal['solsemcomid'] = array();
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad'] = array();
                        Solicitud::getSolicitudBySemilleristaAndEstado($semillerista, $estado);
                        $jsonFiscal['total'] = DBConnector::filas();

                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                                array_push($jsonFiscal['semillera'],utf8_encode($obj -> semillera));
                                array_push($jsonFiscal['comunidad'],(str_replace("'","$",$obj->comunidad)));
                                array_push($jsonFiscal['estado'],$obj->estado);
                                
                            } 
                            if($estado > 0){
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = Semilla::getCultivoByIdSolicitudFiscaliza($id_solicitud);
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = utf8_encode(Semilla::getVariedadByIdSolicitudFiscal($id_solicitud));
                                    array_push($jsonFiscal['variedad'],utf8_encode($variedad));
                                } 
                            }else{
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = '-';
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = '-';
                                    array_push($jsonFiscal['variedad'],$variedad);
                                } 
                            }                                                      
                        } else {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                        }
                        
                        echo json_encode($jsonFiscal);
                        break;
                    //opcion hoja de cosecha
                    case 'productor':
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        Solicitud::getSolicitudBySemilleristaAndEstadoFiscal($dates->search_tilde($semillerista), $estado); 
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();
                        $jsonFiscal['semilla'] = $jsonFiscal['cultivo'] = array();
                        $jsonFiscal['superficie'] = $jsonFiscal['variedad'] = array();
                        $jsonFiscal['nosemcomestsemsup'] = array(); 
                        $jsonFiscal['total'] = DBConnector::filas();
                        while($obj = DBConnector::objeto()){
                            array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                            array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                            array_push($jsonFiscal['semillera'],utf8_encode($obj -> semillera));
                            array_push($jsonFiscal['comunidad'],str_replace("'","$",$obj->comunidad));
                            array_push($jsonFiscal['estado'],$obj->estado_solicitud);                            
                            array_push($jsonFiscal['cultivo'],$obj->cultivo);
                            array_push($jsonFiscal['variedad'],($obj->variedad));
                        }
                        foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                            $semilla = Tiene::getIdSemilla($id_solicitud);
                            array_push($jsonFiscal['semilla'],$semilla);
                            $superficie = Superficie::getIdSuperficieByIdSemilla(Tiene::getIdSemilla($id_solicitud));
                            array_push($jsonFiscal['superficie'],$superficie);
                        }
                        //unificacion de valores
                        foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                            array_push($jsonFiscal['nosemcomestsemsup'], $jsonFiscal['nosolicitud'][$key] . '=' . $jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['estado'][$key].'='.Tiene::getIdSemilla($id_solicitud).'='.$id_solicitud);
                        }
                        #no hay resultado devolver vacio
                        if (!DBConnector::filas()) {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                            $jsonFiscal[''] = $jsonFiscal[''] = '';  
                        }

                        foreach ($jsonFiscal['nosemcomestsemsup'] as $key => $value) {
                            $jsonFiscal['nosemcomestsemsup'][$key] = $value.'='.$jsonFiscal['cultivo'][$key].'='.$jsonFiscal['variedad'][$key];
                        }    
                        echo json_encode($jsonFiscal);
                        break;
                    //opcion semilla funciona
                    case 'productor_semilla' :
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();  
                        $jsonFiscal['solsemcomid'] = array();
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad'] = array();
                        Solicitud::getSolicitudBySemilleristaAndEstadoCertifica($semillerista, $estado);
                        $jsonFiscal['total'] = DBConnector::filas();

                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                                array_push($jsonFiscal['semillera'],utf8_encode($obj -> semillera));
                                array_push($jsonFiscal['comunidad'],(str_replace("'","$",$obj->comunidad)));
                                array_push($jsonFiscal['estado'],$obj->estado);
                                
                            } 
                            if($estado > 0){
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = utf8_encode(Semilla::getVariedadByIdSolicitud($id_solicitud));
                                    array_push($jsonFiscal['variedad'],utf8_encode($variedad));
                                    $solsemcomid = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['isolicitud'][$key].'='.$jsonFiscal['estado'][$key].'='.$cultivo.'='.$variedad;
                                    array_push($jsonFiscal['solsemcomid'],$solsemcomid);
                                } 
                            }else{
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = '-';
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = '-';
                                    array_push($jsonFiscal['variedad'],$variedad);
                                    $solsemcomid = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['isolicitud'][$key].'='.$jsonFiscal['estado'][$key].'='.$cultivo.'='.$variedad;
                                    array_push($jsonFiscal['solsemcomid'],$solsemcomid);
                                } 
                            }                                                      
                        } else {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                        }
                        
                        echo json_encode($jsonFiscal);
                        break;
                    case 'no_era_ista' :
                        $nosolicitud = trim($_GET['nro']);
                        $semillera = $dates -> modificar(trim($_GET['slr']));
                        $semillerista = $dates -> modificar(trim($_GET['srt']));
                        Solicitud::getComunidadByNroSemilleraSemillerista($nosolicitud, $semillera, $semillerista);
                        $obj = DBConnector::objeto();

                        $json['comunidad'] = $obj -> comunidad;

                        echo json_encode($json);
                        break;
                }
                break;
            #paginar semilla
            case 'semilla' :
                //area de trabajo
                $area = 2;
                $page = isset($_GET['page'])?intval($_GET['page']):1;
                #$edit = ($area==10)?1110:1100;
                $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                $limite = ($nroRegistros*$page) - $nroRegistros;

                //realizar consulta sql
                $semilla -> getSemilleraSolicitudFiscal();
                //preparamos la consulta con los primeros resultados
                $blocks = $semilla -> getSemilleraSolicitudFiscalByBlock($limite, $nroRegistros);
                $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                include '../vista/fiscalizacion/datos/semilla.php';
                break;

            #paginar cuentas
            case 'cuentas' :
                $area = $_REQUEST['area'];
                $areaID = Usuario::getAreaUsuario($area);

                $cuenta -> getCuentaFiscal();

                $responsable = array();
                $semillera = array();
                $semillerista = array();
                $variedad = array();
                $bolsa_etiqueta = array();
                $total = array();
                $fecha = array();
                $saldo = array();
                $cultivo = array();
                $inscripcion = array();
                $analisis = array();
                $inspeccion = array();
                $isemilla = array();
                while ($obj = DBConnector::objeto()) {
                    array_push($responsable, $obj -> responsable);
                    array_push($semillera, utf8_encode($obj -> semillera));
                    array_push($semillerista, $obj -> semillerista);
                    array_push($variedad, $obj -> variedad);
                    array_push($bolsa_etiqueta, $obj -> bolsa_etiqueta);
                    array_push($total, $obj -> total);
                    array_push($fecha, $dates -> cambiar_formato_fecha($obj -> fecha));
                    array_push($saldo, $obj -> saldo);
                    array_push($inscripcion, $obj -> inscripcion);
                    array_push($cultivo, $obj -> cultivo);
                    array_push($inspeccion, $obj -> inspeccion);
                    array_push($analisis, $obj -> analisis_etiqueta);
                    array_push($isemilla,$obj->id_semilla);
                }

                $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                include '../vista/fiscalizacion/datos/cuenta.php';
                break;
        }
        break;
    case 'eliminar' :
        break;
    case 'buscar' :
        switch ($pagina) {
            case 'campana' :
                //carga las campañas de de un productor
                $nombre = $dates -> modificar($_GET['nombre']);
                $apellido = $dates -> modificar($_GET['apellido']);
                $semillera = $_GET['semillera'];
                $sistema = $_GET['cultivo'];
                $semilla -> getCampaniaProductor($nombre, $apellido, $semillera, $sistema);
                #echo DBConnector::mensaje();
                $json = array();

                if (DBConnector::filas()) {
                    while ($fila = DBConnector::objeto()) {
                        if (!$fila -> campanha)
                            array_push($json, '');
                        else {
                            array_push($json, $fila -> campanha);
                        }
                    }
                } else
                    array_push($json, 'No tiene campaña');
                echo json_encode($json);
                break;
            case 'cultivo' :
                //cultivos segun el id de solicitud
                $isolicitud = empty($_GET['isolicitud']) ? '-' : trim($_GET['isolicitud']);
                $semilla -> getAllCultivosByProductor($isolicitud);
                $cultivo = array();
                while ($obj = DBConnector::objeto()) {
                    array_push($cultivo, $obj -> cultivo);
                }
                $json['cultivo'] = $cultivo;

                echo json_encode($json);
                break;
            case 'cultivo_variedad' :
                //cultivo y variedad segun el nro de campo o nro de solicitud
                $nro = trim($_GET['nro']);
                Solicitud::getCultivoVariedadFiscal($nro);

                $obj = DBConnector::objeto();
                $json['cultivo'] = $obj -> cultivo;
                $json['variedad'] = utf8_encode($obj -> variedad);

                echo json_encode($json);
                break;

            case 'cuentas' :
                //autocompletado para la opcion cuenta
                $buscar = trim($_GET['term']);
                $json = array();

                $respuesta = (DBConnector::filas()) ? '1' : '0';
                $opciones_busqueda = array('semillera', 'semillerista', 'campana', 'cultivo', 'variedad');
                $contar_opciones = 1;
                while ($contar_opciones <= count($opciones_busqueda)) {
                    #buscar semillera
                    $solicitudes -> getSemillerasCuentaFiscal('semillera', $buscar);
                    if (DBConnector::filas()) {
                        $respuesta = '1';
                        break;
                    }
                    #semillerista
                    $solicitudes -> getSemillerasCuentaFiscal('semillerista', $buscar);
                    if (DBConnector::filas()) {
                        $respuesta = '2';
                        break;
                    }
                    #cultivo
                    $solicitudes -> getSemillerasCuentaFiscal('cultivo', $buscar);
                    if (DBConnector::filas()) {
                        $respuesta = '4';
                        break;
                    }
                    #variedad
                    $solicitudes -> getSemillerasCuentaFiscal('variedad', $buscar);
                    if (DBConnector::filas()) {
                        $respuesta = '5';
                        break;
                    }
                    $contar_opciones++;
                }
                switch ($respuesta) {
                    case '1' :
                        while ($row = DBConnector::objeto())
                            array_push($json, utf8_encode($row -> semilleras));
                        break;
                    case '2' :
                        while ($row = DBConnector::objeto())
                            array_push($json, utf8_encode($row -> semillerista));
                        break;
                    case '4' :
                        while ($row = DBConnector::objeto())
                            array_push($json, utf8_encode($row -> cultivo));
                        break;
                    case '5' :
                        while ($row = DBConnector::objeto())
                            array_push($json, utf8_encode($row -> variedad));
                        break;
                }
                echo json_encode($json);
                break;
            case 'filtro' :
                //filtro de solicitud
                $opc = trim($_GET['opc']);
                $search = isset($_GET['search']) ? trim($_GET['search']) : '';
                $columnas = array();
                switch ($opc) {
                    case 'solicitud' :
                        $json['nosolicitud'] = array();
                        $json['sistema'] = array();
                        $json['semillera'] = array();
                        $json['semillerista'] = array();
                        $json['departamento'] = array();
                        $json['provincia'] = array();
                        $json['municipio'] = array();
                        $json['comunidad'] = array();
                        $json['fecha'] = array();

                        if (!empty($search)) {
                            Solicitud::getSolicitudesFiscalFilter($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['nosolicitud'], $obj -> nro_solicitud);
                                array_push($json['sistema'], ucfirst($obj -> sistema));
                                array_push($json['semillera'], $obj -> semillera);
                                array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                array_push($json['departamento'], $obj -> departamento);
                                array_push($json['provincia'], utf8_encode($obj -> provincia));
                                array_push($json['municipio'], utf8_encode($obj -> municipio));
                                array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                                array_push($json['fecha'], $dates -> cambiar_formato_fecha($obj -> fecha));
                            }
                        }
                        $json['total'] = count($json['sistema']);
                        break;
                    case 'semilla' :
                        $json['semillera'] = array();
                        $json['semillerista'] = array();
                        $json['comunidad'] = array();
                        $json['cultivo'] = array();
                        $json['variedad'] = array();
                        $json['lote'] = array();
                        if (!empty($search)) {
                            Semilla::getSemillaFiscalFilter($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['semillera'], $obj -> semillera);
                                array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                                array_push($json['cultivo'], utf8_encode($obj -> cultivo));
                                array_push($json['variedad'], utf8_encode($obj -> variedad));
                                array_push($json['lote'], $obj -> lote);
                            }
                        }
                        $json['total'] = count($json['semillerista']);
                        break;
                    case 'cosecha' :
                        $json['semillera'] = array();
                        $json['semillerista'] = array();
                        $json['cultivo'] = array();
                        $json['plantaa'] = array();
                        if (!empty($search)) {
                            HojaCosecha::getHojaCosechaFiscalFilter($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                array_push($json['cultivo'], utf8_encode($obj -> cultivo));
                                array_push($json['plantaa'], utf8_encode($obj -> planta_acondicionadora));
                            }
                        }
                        $json['total'] = count($json['semillerista']);
                        break;

                    case 'muestra' :
                        $json['nocampo'] = array();
                        $json['fecharecepcion'] = array();
                        $json['semillerista'] = array();
                        $json['cultivo'] = array();
                        $json['variedad'] = array();

                        Laboratorio::getMuestras($search);
                        while ($obj = DBConnector::objeto()) {
                            array_push($json['nocampo'], utf8_encode($obj -> nro_campo));
                            array_push($json['fecharecepcion'], $dates -> cambiar_formato_fecha($obj -> fecha_recepcion));
                            array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                            array_push($json['cultivo'], utf8_encode($obj -> cultivo));
                            array_push($json['variedad'], utf8_encode($obj -> variedad));
                        }
                        $json['total'] = count($json['semillerista']);
                        break;
                    case 'semillaP' :
                        $json['semillera'] = array();
                        $json['semillerista'] = array();
                        $json['nocampo'] = array();
                        $json['semilla_neta'] = array();
                        $json['categoria_obtenida'] = array();
                        $json['nro_etiqueta'] = array();
                        if (!empty($search)) {
                            SemillaProd::getSemillaProdFiscalFilter($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                array_push($json['nocampo'], utf8_encode($obj -> cultivo));
                                array_push($json['semilla_neta'], utf8_encode($obj -> planta_acondicionadora));
                                array_push($json['categoria_obtenida'], $obj -> categoria_obtenida);
                                array_push($json['nro_etiqueta'], $obj -> nro_etiqueta);
                            }
                        }
                        $json['total'] = count($json['semillera']);
                        break;
                    case 'cuenta' :
                        $json['responsable'] = array();
                        $json['semillera'] = array();
                        $json['semillerista'] = array();
                        $json['campanha'] = array();
                        $json['cultivo'] = array();
                        $json['variedad'] = array();
                        $json['inscripcion'] = array();
                        $json['inspeccion'] = array();
                        $json['analisis'] = array();
                        $json['acondicionamiento'] = array();
                        $json['plantines'] = array();
                        $json['total'] = array();
                        $json['fecha'] = array();
                        $json['monto'] = array();
                        if (!empty($search)) {
                            SemillaProd::getSemillaProdFiscalFilter($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                array_push($json['nocampo'], utf8_encode($obj -> cultivo));
                                array_push($json['semilla_neta'], utf8_encode($obj -> planta_acondicionadora));
                                array_push($json['categoria_obtenida'], $obj -> categoria_obtenida);
                                array_push($json['nro_etiqueta'], $obj -> nro_etiqueta);
                            }
                        }
                        $json['total'] = count($json['semillera']);
                        break;
                }
                echo json_encode($json);
                break;

            case 'icosecha' :
                // id de cosecha segun el id de solicitud
                $id = intval($_REQUEST['cos']);
                Solicitud::getIdCosechaByIdSolicitud($id);
                $obj = DBConnector::objeto();
                $json['id_cosecha'] = $obj -> id_cosecha;
                echo json_encode($json);
                break;

            case 'iNro' :
                //id de solicitud segun el nro de solicitud
                $semillera = strip_tags($_GET['slr']);
                $sistema = strip_tags($_GET['sistema']);

                $json['nosol'] = $solicitudes -> getNoSolicitudSemillera($semillera, $sistema);

                echo json_encode($json);
                break;
            case 'isolicitud' :
                //id de solicitud segun el nombre
                $nombre = $dates -> modificar($_GET['nombre']);
                #echo utf8_decode($nombre); exit;
                $solicitudes -> getIdSolicitudProductorFiscal($nombre, 0);
                #echo DBConnector::mensaje();
                $row = DBConnector::objeto();
                #var_dump($row);
                $json['isolicitud'] = $row -> id_solicitud;

                echo json_encode($json);
                break;
            case 'isemilla_superficie' :
                //id de semilla segun id de solicitud
                $isolicitud = intval($_GET['id']);
                $json['isemilla'] = Tiene::getIdSemilla($isolicitud);
                $json['isuperficie'] = Superficie::getIdSuperficieByIdSemilla($json['isemilla']);

                echo json_encode($json);
                break;
            #lista de cmapos para muestra de laboratorio
            case 'lista_campos' :
                $area = strip_tags($_GET['area']);
                $laboratorio -> getListCampos($area);
                $campos['campos'] = array();
                if (DBConnector::filas()) {
                    while ($obj = DBConnector::objeto()) {
                        array_push($campos['campos'], $obj -> nro_campo);
                    }
                    $campos['msg'] = 'OK';
                    echo json_encode($campos);
                } else {
                    echo json_encode(array("msg" => "error"));
                }
                break;
            case 'ls_categorias' :
                //usada en etapa semilla fiscalizacion
                $id = array();
                $categorias = array();
                $solo_palabra = array('Seleccionada', 'PreBasica', 'Generica');
                Categoria::listarCategoriasByCategoria();
                while ($obj = DBConnector::objeto()) {
                    if ($obj -> generacion == 'Primera') {
                        $union = '1';
                    } elseif ($obj -> generacion == 'Segunda') {
                        $union = '2';
                    } elseif ($obj -> generacion == 'Tercera') {
                        $union = '3';
                    } else {
                        $union = 'B';
                    }
                    array_push($id, $obj -> id_generacion);
                    if (($obj -> categoria != 'Rechazada') && ($obj -> categoria != 'Sin especificar')) {
                        if (in_array($obj -> categoria, $solo_palabra)) {
                            array_push($categorias, ucfirst($obj -> categoria));
                        } else {
                            array_push($categorias, ucfirst($obj -> categoria) . '-' . $union);
                        }
                    }
                }
                //$json['id'] = $id;
                $json['generaciones'] = $categorias;

                echo json_encode($json);
                /*
                 $id = array();
                 $variedades = array();
                 $categoria -> listarCategorias();
                 while ($ccultivo = DBConnector::objeto()) {
                 if ($ccultivo -> generacion == 'Primera') {
                 $union = '1';
                 } elseif ($ccultivo -> generacion == 'Segunda') {
                 $union = '2';
                 } elseif ($ccultivo -> generacion == 'Tercera') {
                 $union = '3';
                 } else {
                 $union = 'B';
                 }
                 array_push($id, $ccultivo -> id_generacion);
                 array_push($variedades, ucfirst($ccultivo -> categoria) . '-' . $union);
                 }
                 $json['id'] = $id;
                 $json['generaciones'] = $categorias;

                 echo json_encode($json);*/
                break;
            case 'ls_cultivos' :
                $nombre_cultivo = array();
                Costos::getNombreCostos();
                while ($ccultivo = DBConnector::objeto()) {
                    $cultivo = $ccultivo -> cultivo;
                    if ($cultivo != 'Acondicionamiento' || $cultivo != 'Plantines')
                        array_push($nombre_cultivo, $cultivo);
                }
                $json['cultivos'] = $nombre_cultivo;

                echo json_encode($json);
                break;
            case 'ls_semilleras' :
                //lista de semilleras que ya pueden calcular sus cuentas
                $estado = intval($_GET['estado']);
                $sistema = trim($_GET['sistema']);
                $solicitudes -> getSemillerasBySistemaAndEstado($estado, $sistema);
                $json = array();
                while ($obj = DBConnector::objeto()) {
                    array_push($json, $obj -> semillera);
                }
                echo json_encode($json);
                break;

            case 'ls_semilleraBySolicitud' :
                $nro = $_GET['nro'];
                $sistema = Sistema::getIdSistema(trim($_GET['sistem']));
                $ls_semilleras = array();
                #echo $sistema;

                $solicitudes -> getNoSolicitudBy2($nro, $sistema);
                while ($ls_solicitudes = DBConnector::objeto()) {
                    $nombre = Semillera::getNombreSemillera($ls_solicitudes -> id_semillera);
                    array_push($ls_semilleras, $nombre);
                }
                $json['semilleras'] = $ls_semilleras;

                echo json_encode($json);
                break;

            case 'ls_solicitudes' :
                $nrosolicitudes = array();
                $etapa = trim($_GET['etapa']);
                $sistema = trim($_GET['sistema']);
                $solicitudes -> getNoSolicitudesFiscal($etapa, $sistema);
                //DBConnector::mensaje();
                if (DBConnector::filas()) {
                    while ($ls_solicitudes = DBConnector::objeto()) {
                        array_push($nrosolicitudes, $ls_solicitudes -> nro_solicitud);
                    }
                    $json['nro_solicitudes'] = $nrosolicitudes;
                    $json['msg'] = 'OK';
                } else {
                    $json['nro_solicitudes'] = '';
                    $json['msg'] = 'Error. No se encuentran solicitudes';
                }
                echo json_encode($json);
                break;

            case 'ls_variedades' :
                $cultivo = $_GET['cultivo'];
                $variedades = array();
                $variedad -> getVariedades(Cultivo::getIdCultivo($cultivo));
                #var_dump(DBConnector::objeto());
                if (DBConnector::filas() > 0) {
                    while ($obj = DBConnector::objeto()) {
                        $nombre_variedad = ucfirst($obj -> nombre_variedad);
                        array_push($variedades, utf8_encode($nombre_variedad));
                    }
                    $json['variedades'] = $variedades;
                } else {
                    $json['variedades'] = '';
                }

                echo json_encode($json);
                break;

            case 'nroSolicitud' :
                //id de solicitud de fiscalizacion segun semillerista y semillera
                $semillerista = $dates -> modificar($_GET['semillerista']);
                $semillera = trim($_GET['semillera']);

                $json['isolicitud'] = Solicitud::getIdSolicitudProductorByFullName($semillerista);

                echo json_encode($json);
                break;
            case 'noSolicitud' :
                //devolver ultimo nro de solicitud en fiscalizacion
                $sistema = trim($_GET['sis']);
                $solicitudes -> view_nextNroSolicitudCertifica($sistema);
                $obj = DBConnector::objeto();
                #echo $obj->nro_solicitud;
                $nro = $dates -> devolverBarraSolicitudv2($obj -> nro_solicitud);
                #var_dump($nro);
                $json['nro'] = intval($nro) + 1;
                $json['anho'] = date('y');
                $json['estado'] = 'nuevo';

                echo json_encode($json);
                break;
            case 'no_sem_sol' :
                $opcion = trim($_GET['opc']);
                $q = trim($_GET['term']);
                $estado = intval($_GET['estado']);
                switch ($opcion) {
                    case 'nosol' :
                        $json = array();
                        Solicitud::getNroSolicitudFiscalByNroSolicitudAndEstadoFiscal($q, $estado, 'nro');
                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($json, $obj -> nro_solicitud);
                            }
                        } else {
                            Solicitud::getNroSolicitudFiscalByNroSolicitudAndEstadoFiscal($q, $estado, 'semillera');
                            if (DBConnector::filas()) {
                                while ($fila = DBConnector::objeto())
                                    array_push($json, $fila -> semillera);
                            } else {
                                Solicitud::getNroSolicitudFiscalByNroSolicitudAndEstadoFiscal($q, $estado, 'semillerista');
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json, utf8_encode($obj -> semillerista));
                                }
                            }
                        }

                        echo json_encode($json);
                        break;
                    case 'nosol_cosecha' :
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoFiscal($q, $estado);
                        $json = array();
                        while ($obj = DBConnector::objeto()) {
                            array_push($json, $obj -> nro_campo);
                        }
                        echo json_encode($json);
                        break;
                    case 'nosol_semillap' :
                        $json = array();
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoFiscal($q, $estado, 'nro');
                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto())
                                array_push($json, $obj -> nro_campo);
                        } else {
                            Solicitud::getNroSolicitudByNroSolicitudAndEstadoFiscal($q, $estado, 'semillera');
                            if (DBConnector::filas()) {
                                while ($obj = DBConnector::objeto())
                                    array_push($json, $obj -> semillera);
                            } else {
                                Solicitud::getNroSolicitudByNroSolicitudAndEstadoFiscal($q, $estado, 'semillerista');
                                while ($obj = DBConnector::objeto())
                                    array_push($json, $obj -> semillerista);
                            }
                        }

                        /*
                         Laboratorio::getResultados();
                         while ($obj = DBConnector::objeto()) {
                         array_push($json, $obj -> nro_campo);
                         }
                         */
                        echo json_encode($json);
                        break;
                }
                break;

            case 'resumenCta' :
                //categoria aprobada de un cultivo
                $nombre = trim($dates -> modificar($_GET['nombre']));
                $apellido = trim($dates -> modificar($_GET['apellido']));
                //semilla
                $cultivo = $dates -> modificar($_GET['cultivo']);
                //semilla
                $variedad = $_GET['variedad'];
                //semilla
                $iSolicitud = intval($_GET['iSolicitud']);
                $iSemilla = intval($_GET['iSemilla']);
                $iSuperficie = intval($_GET['iSuperficie']);
                $json = array();

                //obtenemos la categoria que se pretende producir y el cultivo
                Cuenta::getCategoriaCampoFiscal($iSolicitud, $cultivo, $variedad);
                $obj = DBConnector::objeto();
                $json['categoria_producir'] = $obj -> categoria_producir;
                $json['generacion_producir'] = $generacion -> getNombreGeneracionByIdGeneracion($obj -> generacion);

                $costos -> getCostoByName($cultivo);
                #var_dump(DBConnector::objeto());
                while ($fila = DBConnector::objeto()) {
                    $json['inscripcion'] = $fila -> inscripcion;
                    $json['inspeccion'] = $fila -> inspeccion;
                    $json['analisisEtiqueta'] = $fila -> analisis_etiqueta;
                    $json['plantines'] = ($cultivo == 'plantines') ? $fila -> analisis_etiqueta : '0.00';
                    $json['acondicionamiento'] = ($cultivo == 'acondicionamiento') ? $fila -> inscripcion : '0.00';
                    break;
                }
                $json['total'] = $json['inscripcion'] + $json['inspeccion'] + $json['analisisEtiqueta'] + $json['plantines'] + $json['acondicionamiento'];

                //nro de campo del semillerista
                /*$nroCampo = Semilla::getNroCampoByIdSemilla($iSemilla);

                 if ($cultivo != 'papa') {
                 $json['bolsa'] = Laboratorio::getNroBolsasSemilleraFiscal($nroCampo);

                 } else {
                 $json['bolsa'] = '0';
                 }*/
                echo json_encode($json);
                break;
            case 'resumenCtav2' :
                    //id solicitud
                    $iSolicitud = intval($_GET['iSolicitud']);
                    //id semilla
                    $iSemilla = intval($_GET['iSemilla']);
                    //id superficie  
                    $iSuperficie = empty($_GET['iSuperficie'])?0:intval($_GET['iSuperficie']);
                    //estado de solicitud 
                    $iEstado = intval($_GET['iEstado']);
                    
                    $campania = $semilla->getCampaniaByIdSemillaForCuenta($iSemilla);
                    $cultivo = $semilla->getCultivoByIdSemillaCertifica($iSemilla);
                    $variedad = $semilla->getVariedadByIdSemillaForCuenta($iSemilla);
             
                    $json = array();
                    
                    //obtenemos si tiene al menos una inspeccion
                    $inspecciones = (empty($iSuperficie))?0:Inspeccion::getPrimeraInspeccion($iSuperficie);

                    //obtenemos la categoria que se pretende producir y el cultivo
                    Cuenta::getCategoriaCampo($iSolicitud, $cultivo, $campania, $variedad);
                    //obtenemos la superficie aprobada del productor y semilla
                    Superficie::getSuperficieAprobada($iSolicitud, $iSemilla);
                    $fila = DBConnector::objeto();
                    if (DBConnector::filas())
                        $json['supAprobada'] = 0.1;
                    else
                        $json['supAprobada'] = 1;
                    $costos -> getCostoByName($cultivo);
                    #var_dump(DBConnector::objeto());
                    if (!DBConnector::nroError()){
                        $fila = DBConnector::objeto();
                        if (!strcasecmp (strtolower ($fila -> cultivo), 'papa') ){
                            $igeneracion = Semilla::getCategoriaProducir($iSemilla);                        
                            $json['catAprobada'] = Categoria::getNombreCategoriaByIdGeneracion($igeneracion).'-'. Generacion::getInicialGeneracion($igeneracion);
                        }else{
                            $iGeneracion = Semilla::getCategoriaProducir($iSemilla);
                            $json['catAprobada'] = Categoria::getNombreCategoriaByIdGeneracion($iGeneracion).'-'. Generacion::getInicialGeneracion($iGeneracion);
                        }
                        switch ($iEstado) {
                            //inspeccion
                            case '5':case'6':case'7':
                                $excepciones = arrray('plantines','acondicionamiento');
                                if (!in_array($cultivo,$excepciones)){
                                    $json['inscripcion'] = intval($fila -> inscripcion);
                                    $json['costo_inscripcion'] = $json['inscripcion'] * $json['supAprobada']; 
                                    $json['inspeccion'] = intval($fila -> inspeccion);
                                    $json['costo_inspeccion'] = $json['inspeccion'] * $json['supAprobada'];
                                    $json['analisisEtiqueta'] = '0';
                                }else{
                                     $json['inscripcion'] = $json['costo_inscripcion'] = 0;
                                     $json['inspeccion'] = $json['costo_inspeccion'] = 0;
                                    if ($cultivo == 'plantines'){
                                        $json['plantines'] = $fila -> inscripcion * $json['supAprobada'];
                                    }else{
                                        $json['plantines'] = $fila -> analisis_etiqueta * $json['supAprobada'];
                                    }
                                }
                                break;
                                //resultado de laboratorio y semilla producida
                            case '11':case'12':
                                $json['inscripcion'] = number_format($fila -> inscripcion,2,'.',',');
                                $json['inspeccion'] = number_format($fila -> inspeccion,2,'.',',');
                                $json['analisisEtiqueta'] = number_format($fila -> analisis_etiqueta,2,'.',',');
                                if (strcasecmp(strtolower($cultivo), 'plantines')){
                                    $json['costo_inscripcion'] = 0;
                                    $json['inspeccion'] = $json['costo_inspeccion'] = 0;
                                    $json['inscripcion'] = $fila -> inscripcion * $json['supAprobada'];
                                }
                                if (!strcasecmp(strtolower($cultivo), 'acondicionamiento')){
                                    $json['costo_inscripcion'] = 0;
                                    $json['inspeccion'] = $json['costo_inspeccion'] = 0;
                                    $json['inscripcion'] = $fila -> analisis_etiqueta * $json['supAprobada'];
                                }
                                break;
                            default://solo es inscripcion
                                $json['inscripcion'] = number_format($fila -> inscripcion * $json['supAprobada'],2,'.',',');
                                $json['inspeccion'] = '0';
                                $json['analisisEtiqueta'] = '0';
                                break;
                        }

                        $cuenta  =  Cuenta::checkCuentaByIdSemilla($iSemilla);  
                        if ($cuenta){
                            $json['total'] = Cuenta::getSaldoByIdSemilla($iSemilla);
                        }else{
                            $json['total'] = $json['inscripcion'] + $json['inspeccion']+$json['analisisEtiqueta'];    
                        }                                                                                                                           
                        
                    }else{//error en la seleccion de  precios para el cultivo
                        $json['catAprobada'] = 'Sin especificar';
                        $json['inscripcion'] = $json['inspeccion'] = $json['analisisEtiqueta'] = 'ERROR';
                        $json['total'] = 'ERROR';
                    }
                  
                    //superficie total utilizada por el semillerista
                    Superficie::getSuperficieTotal($iSolicitud, $cultivo, $variedad, $campania);
                    $row = DBConnector::objeto();
                    if (!$row -> total)
                        $json['supTotal'] = 0.00;
                    else
                        $json['supTotal'] = $row -> total;
 
                    //nro de campo del semillerista
                    $nroCampo = Semilla::getNroCampoByIdSemilla($iSemilla);

                    if ($cultivo != 'papa') {
                        $nroBolsa = Laboratorio::getNroBolsasSemillera($nroCampo);
                        //echo $nroBolsa;
                        if (DBConnector::filas()) {
                            $fila3 = DBConnector::objeto();
                            $json['bolsa'] = $fila3 -> bolsa;
                        } else {
                            $json['bolsa'] = '0';
                        }
                    } else {
                        $json['bolsa'] = '0';
                    }
                    $cat_sembrada = Semilla::getCategoriaSembrada($iSemilla);
                    $cat_obtenida = Semilla::getCategoriaProducir($iSemilla);
                    $etiquetas = SemillaProd::getEtiquetaByVariedadProductor($cat_sembrada, $cat_obtenida,$solicitudes->getSemilleraByIdSolicitudForCuenta($iSolicitud));
                    $json['etiquetas'] = $etiquetas;
                    
                    echo json_encode($json);
                    break;
            case 'resumen_cuenta' :
                #devuelve todos los datos para el resuem de la cuenta
                $cadena = strip_tags($_GET['slr']);
                $solicitudes -> getResumenCuentaFiscal($cadena);
                $json['total'] = DBConnector::filas();
                $json['semillera'] = $json['semillerista'] = $json['cultivo'] = array();
                $json['comunidad'] = $json['variedad'] = $json['categoria_producir'] = array();
                $json['isolicitud'] = $json['isemilla'] = $json['res_cuenta'] = array();
                $json['nosolicitud'] = $json['lote'] = $json['estado'] = array();
                while($obj = DBConnector::objeto()){
                    array_push($json['nosolicitud'],$obj->nro_campo);
                    array_push($json['semillera'],$obj -> semillera);
                    array_push($json['semillerista'],trim(utf8_encode($obj -> semillerista)));
                    array_push($json['comunidad'],utf8_encode($obj->comunidad));
                    array_push($json['cultivo'],utf8_encode($obj -> cultivo));
                    array_push($json['variedad'],utf8_encode($obj -> variedad));
                    array_push($json['lote'],utf8_encode($obj -> lote));
                    array_push($json['categoria_producir'],$obj -> categoria_producir);
                    array_push($json['estado'],$obj -> estado);
                    array_push($json['isolicitud'],$obj -> id_solicitud);
                    array_push($json['isemilla'],$obj -> id_semilla);
                }
                foreach($json['isolicitud'] as $key=>$isolicitud){
                    $iresumen = $json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['cultivo'][$key].'='.$json['variedad'][$key].'='.$json['categoria_producir'][$key].'='.$json['isolicitud'][$key].'='.$json['isemilla'][$key].'='.$json['comunidad'][$key].'='.$json['nosolicitud'][$key].'='.$json['lote'][$key].'='.$json['estado'][$key];
                    array_push($json['res_cuenta'],$iresumen);
                }
                
                echo json_encode($json);
                break;
        }
        break;

    case 'nro' :
        // nro de solicitud para una fiscalizacion
        $semillera = isset($_GET['sem']) ? trim($_GET['sem']) : '';
        //si nombre de la semillera no existe la variable es vacia
        if (!empty($semillera) && count($semillera) > 3) {
            $solicitudes -> view_getNroSolicitudFiscal();
            $fila = DBConnector::resultado();
            if (!empty($fila -> nro_solicitud)) {
                $aux = explode("/", $fila -> nro_solicitud);
                $temp = $aux[0];
            } else {
                $temp = 0;
            }
            $json['nro'] = ++$temp;
            $json['anho'] = date('y');
            $json['estado'] = 'nuevo';
        }

        echo json_encode($json);
        break;

    case 'id' :
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $estado = intval(trim($_GET['std']));
        //$tabla = isset($_GET['tbl']) ? $_GET['tbl'] : '';
        //echo $nombre;
        $semillera = isset($_GET['semillera']) ? $_GET['semillera'] : '';
        $semillera = str_replace('*', ' ', $semillera);
        $solicitudes -> getIdSolicitudProductor($nombre, $apellido, $semillera, 'fisc', $estado);
        while ($fila = DBConnector::resultado()) {
            $json['iproductor'] = $fila -> id_solicitud;
        }
        echo json_encode($json);
        break;

    case 'lote' :
        //nro de lote segun nombre, apellido,cultivo,nro de analisis y gestion
        $nombre = trim($_GET['nombre']);
        $apellido = trim($_GET['apellido']);
        $cultivo = trim($_GET['cultivo']);
        $nosolicitud = trim($_GET['sol']);
        $flag = trim($_GET['state']);

        $json['lote'] = $dates -> getNroLoteFiscal($nombre, $apellido, $cultivo, $nosolicitud, $flag);

        echo json_encode($json);
        break;
}
?>