<?php
//if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require '../config/Setting.php';
    require '../modelo/DBConnector.php';

    include '../modelo/Bitacora.class.php';
    include '../includes/Browser.php';
    include '../modelo/Usuario.class.php';

    include '../includes/Funciones.php';
    include '../modelo/Backup.class.php';

    require '../modelo/Costos.class.php';
    require '../modelo/Cultivo.class.php';
    require '../modelo/Categoria.class.php';
    require '../modelo/Generacion.class.php';
    require '../modelo/Area.class.php';
    require '../modelo/Sistema.class.php';
    require '../modelo/Comunidad.class.php';
    require '../modelo/Municipio.class.php';
    require '../modelo/Provincia.class.php';
    require '../modelo/Departamento.class.php';
    require '../modelo/Semillera.class.php';
    require '../modelo/Solicitud.class.php';
    require '../modelo/Crud.class.php';
    require '../modelo/Semillerista.class.php';
    require '../modelo/Semilla.class.php';
    require '../modelo/Acondicionamiento.class.php';
    require '../modelo/Plantines.class.php';
    require '../modelo/Variedad.class.php';
    require '../modelo/Tiene.class.php';
    require '../modelo/Inscrita.class.php';
    require '../modelo/Campanha.class.php';
    require '../modelo/Superficie.class.php';
    require '../modelo/Inspeccion.class.php';
    require '../modelo/HojaCosecha.class.php';
    require '../modelo/SemillaProd.class.php';
    require '../modelo/Realiza.class.php';
    require '../modelo/Tipo_semilla.class.php';
    require '../modelo/Laboratorio.class.php';
    require '../modelo/Muestra_laboratorio.class.php';
    require '../modelo/Resultado_muestras.class.php';
    require '../modelo/Resultado_actualizacion.class.php';
    require '../modelo/Resultado_certificada.class.php';
    require '../modelo/Resultado_importada.class.php';
    require '../modelo/Cuenta.class.php';
    
    require '../modelo/Busqueda.class.php';
    require '../modelo/Responsable.class.php';
    require '../modelo/Reportes.class.php';

    /*
     require '../modelo/CtaCsto.class.php';
     require '../modelo/Fiscalizacion.class.php';

     require '../modelo/Grafico.class.php';
     require '../modelo/Informe.class.php';

     */
    //clase necesaria para realizar los informems en pdf y excel
    //require '../libs/fpdf/fpdf.php';
    require '../modelo/Excel.class.php';

    date_default_timezone_set('America/La_Paz');

    $control = $_REQUEST['mdl'];

    $usuarios = new Usuario();
    $cultivos = new Cultivo();
    $categoria = new Categoria();
    $generacion = new Generacion();

    $solicitudes = new Solicitud();
    $semilla = new Semilla();
    $acondicionamiento_obj = new Acondicionamiento();
    $plantines_obj = new Plantines(); 
    $variedad = new Variedad();
    $superficie_inscrita = new Inscrita();
    $superficie = new Superficie();
    $inspecciones = new Inspeccion();
    $cosechas = new hojaCosecha();
    $producciones = new SemillaProd();
    $laboratorio = new Laboratorio();
    $muestra = new Muestra_laboratorio();
    $cuenta = new Cuenta();
    $buscar = new Busqueda();
    $costos = new Costos();
    $dates = new Funciones();
    
    $bitacora = new Bitacora();//instancia para metodos de bitacora    
    $backup = new Backup();//instancia para metodos de backup de BD
    $tiene = new Tiene();
    $reporte = new Reportes();//instancia para creacion de reportes
    $excel = new Excel(); //instancia para formato de hoja excel
    
    /*
     $fiscaliza = new Fiscalizacion();
     $realiza = new Realiza();
     $cta_csto = new CtaCsto();
     $informe = new Informe();
     */

    $dates -> noCache();
    //Numero de registros a mostrar por el paginador
    $nroRegistros = 15;
    switch ($control) {
        case 'usuario' :
            $item_usuarios = new Usuario();

            global $dates;
            global $datosSolicitudes;
            global $semillera;
            global $productor;
            global $etapa;
            global $cantidad;
            include $control . '.ctrl.php';
            break;
        case 'login' :
            global $usuariosReg;
            global $ingresos;
            global $datosBackup;
            global $dates;
            global $format;
            $format = new Funciones();

            include $control . '.ctrl.php';
            break;
        case 'administracion' :
            global $format;
            $format = new Funciones();
            include $control . '.ctrl.php';
            break;
        case 'certificacion' :
            global $nroRegistros;
            include $control . '.ctrl.php';
            break;
        case 'fiscalizacion' :
            global $jsonFiscal;
            include $control . '.ctrl.php';
            break;
        case 'laboratorio' :
            include $control . '.ctrl.php';
            break;
        case 'importar' :
            include $control . '.ctrl.php';
            break;
        case 'informe' :
            include $control . '.ctrl.php';
            break;
        case 'busqueda' :
            include $control . '.ctrl.php';
            break;
    }
/*
} else {
    echo $_SERVER['HTTP_X_REQUEST_WITH'];
    //throw new Exception("Error Procesando Peticion", 1);
}
*/
?>