<?php
$pagina = strip_tags($_REQUEST['pag']);
switch ($pagina) {
    /**
     * formulario para agregar categoria
     * */
    case 'frmcategoria' :
        include '../vista/administracion/nueva_categoria.html.php';
        break;
    /**
     * formulario para agregar nueva generacion a una categoria
     * */
    case 'frmgeneracion' :
        $categoria -> getCategorias();

        include '../vista/administracion/nueva_generacion.html.php';
        break;
    /**
     * agregar generacion a una categoria
     * */

    case 'addGeneracion' :
        $categoria = intval($_POST['categoria']);
        $nombre = strip_tags(trim($_POST['generacion']));

        $generacion -> newGeneracion(ucfirst($nombre), $categoria);
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else {
            echo DBConnector::mensaje();
        }

        break;
    /**
     * agregar categoria
     * **/
    case 'addCategoria' :
        $nuevaCategoria = trim($_POST['categoria']);
        $categoria -> setCategoria(ucfirst($nuevaCategoria));
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else
            echo DBConnector::mensaje();
        break;

    /**
     * verifica si el nombre de generacion o categoria ya existe
     * */
    case 'verificarCatGen' :
        $tipo = trim($_GET['tp']);
        $nombre = trim($_GET['nombre']);

        if ($tipo == 'generacion') {
            $categoria = strip_tags(trim($_GET['categoria']));
            $generacion -> getNameGeneracionByCategoria($categoria, $nombre);

            if (DBConnector::filas() == 0)
                $json['estado'] = 'libre';
            else
                $json['estado'] = 'existe';
        } else {
            $categoria -> getCategoriaByName($nombre);

            if (!DBConnector::filas())
                $json['estado'] = 'libre';
            else
                $json['estado'] = 'existe';
        }
        echo json_encode($json);
        break;
    /**
     * Agrega un nuevo costo de un cultivo
     * */
    case 'agregar' :
        $cultivo = trim($_POST['cultivo']);
        $inscripcion = trim($_POST['inscripcion']);
        $inspeccion = trim($_POST['inspeccion']);
        $analisis = trim($_POST['analisis']);

        $cultivos -> setCheckCultivo(ucfirst($cultivo));
        $costos -> insertCosto($cultivos -> getIdCultivo($cultivo), $inscripcion, $inspeccion, $analisis);
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else
            echo 'error';
        break;
    /**
     * carga todas las categorias
     * **/
    case 'categoria' :
        $categoria -> getCategorias();

        while ($obj = DBConnector::objeto()) {
            $json[$obj -> id_categoria] = $obj -> nombre_categoria;
        }
        echo json_encode($json);
        break;
    /**
     * lista todos los cultivos y sus costso respectivos
     * */
    case 'costos' :
        $costos -> getCostos();
        include '../vista/administracion/costos.html.php';
        break;
    /**
     * verifica la existencia de un cultivo
     * */
    case 'verCultivo' :
        $cultivo = $_GET['cultivo'];
        $json['cultivo'] = $cultivos -> verificarNombreCultivo($cultivo);
        echo json_encode($json);
        break;
    /**
     * agregar cultivos
     * **/
    case 'add_cultivo' :
        $nombre_cultivo = trim($_POST['cultivo']);
        $costos -> insertCultivo($nombre_cultivo);
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else
            echo 'error';
        break;
    /**
     * carga el fromulario de un nuevo cultivo y sus costos
     * */
    case 'costo_cultivo' :
        include '../vista/administracion/nuevo_costo_cultivo.html.php';
        break;
    /**
     * elimina un cultivo y sus costos
     * */
    case 'eliminar' :
        $id = intval($_REQUEST['id']);

        $id_cultivo = $cultivos -> getIdCultivoByIdCosto($id);

        $costos -> deleteCosto($id);
        $cultivos -> delCultivo($id_cultivo);
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else {
            echo 'error';
        }
        break;
    /**
     * eliminar generacion
     * */
    case 'del_generacion' :
        $id = intval($_REQUEST['id']);

        $generacion -> deleteGeneracion($id);

        if (!DBConnector::nroError()) {
            echo 'OK';
        } else {
            echo 'error';
        }
        break;

    /**
     *
     * */
    case 'updGeneracion' :
        $id = intval($_REQUEST['id']);
        $generacion -> getGeneracionById($id);

        include '../vista/administracion/editar_generacion.html.php';
        break;

    /**
     * formulario de actualizacion de costos de un cultivo
     * */
    case 'actualizarDatos' :
        $id = intval($_REQUEST['id']);
        $actualizacion = $costos -> getCostos($id);

        include '../vista/administracion/datos_costos.html.php';
        break;
    /**
     * actualizacion de costos de un cultivo
     * */
    case 'actualizar' :
        $cultivo = trim($_POST['cultivo']);
        $inscripcion = trim($_POST['inscripcion']);
        $inspeccion = trim($_POST['inspeccion']);
        $analisis = trim($_POST['analisis']);
        $id_costo = trim($_POST['id']);

        if (!is_numeric($cultivo)) {
            $costos -> updateCosto($id_costo, $inscripcion, $inspeccion, $analisis);
            if (!DBConnector::nroError()) {
                echo 'OK';
            } else {
                echo 'error';
            }
        } else {
            echo 'Nombre de cultivo incorrecto';
        }
        break;
    case 'respaldar' :
        $nombre = trim(ucfirst($_POST['nombreArchivo']));
        $fecha = trim($_POST['fecha']);
        $hora = trim($_POST['hora']);
        $id = trim($_POST['iuser']);

        if (!file_exists("../../" . BACKUP_FOLDER))
            mkdir("../../" . BACKUP_FOLDER);

        $direccion = "../../" . BACKUP_FOLDER . $nombre.rand(1000,3500) . '.sql';
        
        $titulo = $backup -> cabecera();
        #echo $titulo;
        $backup -> setContenido($direccion, $titulo);

        //semilleras
        $contenido_semillera = $backup -> cabeceraSemillera();
        $semillera = new Semillera();
        $contenido_semillera .= $semillera -> getSemilleraSQL();
        #echo $contenido_semillera;
        $backup -> setContenido($direccion, $contenido_semillera);
           
        //semilleristas
        $contenido_semillerista = $backup -> cabeceraSemillerista();
        $contenido_semillerista .= Semillerista :: getSemilleristaSQL();
        #echo $contenido_semillerista;
        $backup -> setContenido($direccion, $contenido_semillerista);
       
        //sistema
        $contenido_sistema = $backup -> cabeceraSistema();
        $sistema = new Sistema();
        $contenido_sistema .= $sistema -> getSistemaSQL();
        $backup -> setContenido($direccion, $contenido_sistema);
        
        //departamento
        $contenido_depto = $backup -> cabeceraDepartamento();
        $departamento = new Departamento();
        $contenido_depto .= $departamento -> getDepartamentoSQL();
        $backup -> setContenido($direccion, $contenido_depto);
       
        //provincia
        $contenido_prov = $backup -> cabeceraProvincia();
        $provincia = new Provincia();
        $contenido_prov .= $provincia->getProvinciaSQL();
        $backup -> setContenido($direccion, $contenido_prov);
        
        //municipio
        $contenido_municipio = $backup -> cabeceraMunicipio();
        $municipio = new Municipio();
        $contenido_municipio .= $municipio ->getMunicipioSQL();
        $backup -> setContenido($direccion, $contenido_municipio);
        
        // solicitudes
        $contenido_solicitud = $backup -> cabeceraSolicitud();
        $contenido_solicitud .= $solicitudes -> getSolicitudSQL();
        $backup -> setContenido($direccion, $contenido_solicitud);

        
         $contenido_semilla = $backup -> cabeceraSemilla();
         $contenido_semilla .= $semilla -> getSemillaSQL();
         $backup -> setContenido($direccion, $contenido_semilla);
         //echo $contenido;exit;

         $contenido_superficie = $backup -> cabeceraSuperficie();
         $contenido_superficie .= $superficie -> getSuperficieSQL();
         $backup -> setContenido($direccion, $contenido_superficie);

         $contenido_inspeccion = $backup -> cabeceraInspecciones();
         $contenido_inspeccion .= $inspecciones -> getInspeccionesSQL();
         $backup -> setContenido($direccion, $contenido_inspeccion);
         
         $contenido_cosecha = $backup -> cabeceraHojaCosecha();
         $contenido_cosecha .= $cosechas -> getHojaCosechaSQL();
         $backup -> setContenido($direccion, $contenido_cosecha);
         
         $contenido_laboratorio = $backup -> cabeceraLaboratorio();
         $contenido_laboratorio .= $laboratorio -> getLaboratorioSQL();
         $backup -> setContenido($direccion, $contenido_laboratorio);
        
         $contenido_cuenta = $backup -> cabeceraCuenta();
         $contenido_cuenta .= $cuenta -> getCuentaSQL();
         $backup -> setContenido($direccion, $contenido_cuenta);

         $contenido_costo = $backup -> cabeceraCosto();
         $contenido_costo .= $costos -> getCostosSQL();
         $backup -> setContenido($direccion, $contenido_costo);
     /*    
         $contenido_fiscal = $backup -> cabeceraFiscalizacion();
         $contenido_fiscal .= $fiscaliza -> getFiscalizacionSQL();         
         $backup -> setContenido($direccion, $contenido_fiscal);
        
         $contenido_categoria = $backup -> cabeceraCategoria();
         $contenido_categoria .= $categoria -> getCategoriasSQL();         
         $backup -> setContenido($direccion, $contenido_categoria);
        
         $contenido_generacion = $backup -> cabeceraGeneracion();
         $contenido_generacion .= $generacion -> getGeneracionesSQL();         
         $backup -> setContenido($direccion, $contenido_generacion);

         $contenido_semilla_producida = $backup -> cabeceraSemillaProducida();
         $contenido_semilla_producida .= $producciones -> getSemillasProducidasSQL();
         $backup -> setContenido($direccion, $contenido_semilla_producida);

         $contenido = $backup -> cabeceraTiene();
         $contenido .= $tiene -> getTieneSQL();

         $contenido = $backup -> cabeceraRealiza();
         $contenido .= $realiza -> getRealizaSQL();

         $contenido = $backup -> cabeceraCtaCsto();
         $contenido .= $cta_csto -> getCtaCstoSQL();
         */
        $error = $backup -> getRespaldarBaseDeDatos($direccion, $fecha, $hora, $id);
        if ($error)
            echo 'OK';
        else
            echo $error;
        break;
    //a todas las copias realizadas
    case 'restauracion' :
        $backup -> getCopias();

        include '../vista/login/restaurar.php';
        break;
    //restaurar una copia del sistema
    case 'restaurar' :
        #$folder_backup = $_SERVER['DOCUMENT_ROOT'] . '/backupSICEF/';
        $fileName = $_REQUEST['file'];
        $estado = $backup -> restaurarBaseDatos($fileName, DB_USER, DB_PASS, DB_NAME);
        if ($estado) {
            echo 'OK';
        } else
            echo 'ERROR';
        break;
    //elimina una copia de seguridad
    case 'eliminar_copia' :
        $file = $_REQUEST['file'];
        $backup -> getFileNameById($file);
        $fila = DBConnector::objeto();
        $eliminarBackup = $fila -> archivo;
        //$eliminarBackup = $_SERVER['DOCUMENT_ROOT'] . '/backupSICEF/' . $filename;
        unlink($eliminarBackup);
        $backup -> deleteArchivo($file);
        echo 'OK';
        break;
}
?>