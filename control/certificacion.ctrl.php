<?php

# Es una petición AJAX
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $opcion = strip_tags($_REQUEST['opt']);
    $pagina = isset($_REQUEST['pag']) ? strip_tags($_REQUEST['pag']) : '-';
    global $areaID;
    //echo 'opt: '.$opcion.' pag: '.$pagina; exit;
    switch ($opcion) {        
        case 'new' ://mostrar formulario de acuerdo al tipo
            switch($pagina) {
                case 'acreedores':
                    include '../vista/certificacion/acreedores.html.php';
                    break;
                case 'detalle' ://formulario detalle de certificacion,produccion y costos
                    session_start();
                    include '../vista/gestionesAnteriores/gestionesAnteriores.html.php';
                    break;
                case 'detalle_costos':
                    include '../vista/certificacion/produccion_costos.html.php';
                    break;
                case 'solicitud' ://formulario de solicitud
                    session_start();
                    include '../vista/certificacion/solicitud.html.php';
                    break;
                case 'semilla' ://formulario de nueva semilla
                    session_start();
                    include '../vista/certificacion/semilla.html.php';
                    break;
                case 'add_semilla' ://formulario de nueva semilla en solicitud
                    include '../vista/certificacion/add_semilla.html.php';
                    break;
                case 'superficie' ://formulario de nueva superficie
                    session_start();                    
                    include '../vista/certificacion/superficie.html.php';
                    break;
                case 'inspeccion' ://formulario de nueva inspeccion
                    session_start();
                    include '../vista/certificacion/inspeccion.html.php';
                    break;
                case 'cosecha' ://formulario de nueva hoja de cosecha
                    session_start();                    
                    include '../vista/certificacion/hoja_cosecha.html.php';
                    break;
                //formulario de nueva muestra de laboratorio    
                case 'muestra':
                    include '../vista/certificacion/laboratorio.html.php';
                break;  
                #paginar lista de muestras  listas para resultado
                case 'muestras':
                    Laboratorio::getListMuestrasCertificacionv2();
                    include '../vista/certificacion/datos/muestras.html.php';       
                break;                   
                case 'semillaP' ://formulario de nueva semilla producida
                    session_start();                    
                    include '../vista/certificacion/semilla_prod.html.php';
                    break;
                case 'cuenta' ://formulario de nueva cuenta
                    session_start();                    
                    include '../vista/certificacion/cuenta.html.php';
                    break;
                case 'seguimiento' :
                    include '../vista/menu/seguimiento.php';
                    break;
                case 'superficieProduccion' ://parametros para reporte
                    include '../vista/reporte/trimestral.html.php';
                    break;
                case 'volumenProduccion' :
                    include '../vista/reporte/trimestral_sup.html.php';
                    break;      
                #imagenes de opciones    
                case 'segSol' ://etapa solicitud
                    echo "<img src='images/checkSol.png' />";
                    break;
                case 'segSem' ://tipo de semilla a sembrar
                    echo "<img src='images/checkSem.png' />";
                    break;
                case 'segSup' ://imagen de superficie
                    echo "<img src='images/checkSup.png' />";
                    break;
                case 'segInsp' ://inspecciones
                    echo "<img src='images/checkInsp.png' />";
                    break;
                case 'segCos' ://Hoja de cosecha
                    echo "<img src='images/checkCos.png' />";
                    break;
                case 'segLab' ://Analisis de laboratorio
                    echo "<img src='images/checkLab.png' />";
                    break;
                case 'segProd' ://semilla producida
                    echo "<img src='images/checkProd.png' />";
                    break;
                case 'segCta' ://estado de cuenta
                    echo "<img src='images/checkCta.png' />";
                    break;
            }
            break;
        
        case 'guardar' ://guardar datos
            switch ($pagina) {
                //datos de plantin
                case 'plantines':
                    $isolicitud = intval($_POST['solicitud']);                
                    $plantines['icultivo'] = $cultivos -> setCheckCultivo(trim(ucfirst($_POST['cultivo-plantines'])));
                    $plantines['ivariedad'] = $variedad -> setCheckVariedad(trim(ucfirst($_POST['variedad-plantines'])), $plantines['icultivo']);
                    $plantines['nrocampo'] = Semilla::getNextNroCampoPlantines();
                    //guardar datos de plantines
                    $plantines_obj ->insertSemillaPlantines($plantines);
                    $isemilla = DBConnector::lastInsertId();
                    //agregamos valores de los id en la tabla de relacion
                    $tiene -> solicitud_semilla($isolicitud, $isemilla);
                    //incrementa el valor de estado de solicitud en la tabla solicitud en 4
                    Solicitud::updateEstado2($isolicitud);
                    //cambia el estado de semilla a 1
                    Semilla::updateEstado($isemilla);
                    if(DBConnector::nroError()){
                        echo "ERROR :: ".__METHOD__, ":: $query ::",DBConnector::mensaje();
                    }else{
                        echo 'OK';
                    }
                break;
                case 'acondicionamiento':
                    $acondicionamiento['campo'] = strip_tags($_POST['nocampo']);
                    $acondicionamiento['fecha'] = $dates->cambiar_tipo_mysql(strip_tags($_POST['emision']));
                    $acondicionamiento['categoria_en_campo'] = intval($_POST['categoria_acondicionamiento']);
                    $acondicionamiento['rendimiento_estimado'] = floatval($_POST['rendimientoE']);
                    $acondicionamiento['superficie'] = Superficie::getIdSuperficieBySuperficie(floatval($_POST['superficie-acondicionamiento']));
                    $acondicionamiento['rendimiento_campo'] = floatval($_POST['rendimientocampo']);
                    $acondicionamiento['planta'] = strip_tags($_POST['plantaacondicionadora']);
                    $acondicionamiento['pesobruto'] = floatval($_POST['pesobruto']);
                    $acondicionamiento['cupones'] = intval($_POST['cuponesacondicion']);
                    $acondicionamiento['rango'] = intval($_POST['rc-from']);
                    $acondicionamiento['isolicitud'] = intval($_POST['solicitud']);
                    $acondicionamiento['cultivo'] = $cultivos -> setCheckCultivo(trim(ucfirst($_POST['cultivo'])));
                    $acondicionamiento['variedad'] = $variedad -> setCheckVariedad(trim(ucfirst($_POST['variedad'])), $acondicionamiento['cultivo']);
                    //registro en tabla acondicionamiento
                    $acondicionamiento_obj ->insertAcondicionamiento($acondicionamiento);
                    $semilla->insertAcondicionamiento($acondicionamiento);
                    if(DBConnector::nroError()){
                        echo "ERROR :: ",DBConnector::mensaje();
                    }else{
                        //actualizacion de estado para que se pueda realizar la muestra de laboratorio
                        #Solicitud::updateEstado($acondicionamiento['isolicitud']);
                        $solicitudes->updateEstadoCosecha($acondicionamiento['isolicitud']);
                        echo 'OK';
                    }
                    break;
                case 'solicitud' :
                    $nombre = utf8_decode(ucwords(trim($_POST['name'])));
                    $apellido = (is_string($_POST['apellido']) && !empty($_POST['apellido'])) ? utf8_decode(ucwords(trim($_POST['apellido']))) : '';
                    $tmp_comunidad = $dates -> addSpace($_POST['comunidad']);
                    $comunidad = $dates -> specialWord($tmp_comunidad);
                    $id_municipio = intval(trim($_POST['hdn_municipio']));
                    $nro_solicitud = $dates->getNroSolicitud(trim($_POST['nro_solicitud']));
                    $id_sistema = $solicitudes -> getIdSistema(trim($_POST['sistema']));
                    $fecha = $dates -> cambiar_tipo_mysql(trim($_POST['f_solicitud']));
                    $noparcelas = trim($_POST['noparcelas']);
                    #numero de parcelas
                    $nroComunidades = count($comunidad);

                    if ($_POST['tipo'] == 'personal') {
                        $semillera = 'PARTICULAR - ' . strtoupper($apellido);
                        $id_semillera = Semillera::setSemillera($semillera, $nro_solicitud,$id_sistema);
                    } else {
                        $id_semillera = Semillera::setSemillera(strtoupper($_POST['semillera']), $nro_solicitud,$id_sistema);
                    }
                    //1 comunidad - 1 parcela
                    if ($nroComunidades == 1) {
                        if ($noparcelas == 1) {
                            $semillerista = new Semillerista($nombre, $apellido, $id_semillera);
                            $id_semillerista = $semillerista -> setSemillerista();
                            $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($comunidad[0]), $fecha);
                        } else {
                            $semillerista = new Semillerista($nombre, $apellido, $id_semillera);
                            $id_semillerista = $semillerista -> setSemillerista();
                            $i = 1;
                            while ($noparcelas >= $i) {
                                $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($comunidad[0]), $fecha);
                                $i++;
                            }
                        }                        
                        echo 'OK';
                    } else {
                        //comunidades mayores a 1
                        $semillerista = new Semillerista($nombre, $apellido, $id_semillera);
                        $id_semillerista = $semillerista -> setSemillerista();
                        if ($nroComunidades == $noparcelas) {
                            foreach ($comunidad as $key => $value) {
                                $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($value), $fecha);
                            }
                        } else {
                            $i = 1;
                            while ($i <= $nroComunidades) {
                                foreach ($comunidad as $key => $value) {
                                    $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($value), $fecha);
                                }
                                $i++;
                            }
                            $noparcelas = $noparcelas - $nroComunidades;
                            $i = 1;
                            while ($i <= $nparcelas) {
                                $solicitudes -> insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, ucwords($comunidad[0]), $fecha);
                                $i++;
                            }
                        }
                        echo 'OK';
                    }
                    break;

                case 'semilla' :
                    $nro_campo = trim($_POST['nrocampo']);
                    $tmp_campanha = explode('/', trim($_POST['campania']));
                    $id_cultivo = $cultivos -> setCheckCultivo(trim(ucfirst($_POST['hiddenCultivo'])));
                    $variedad_name = $dates->specialWord($_POST['hiddenVariedad']);
                    $id_variedad = $variedad -> setCheckVariedad(trim(ucfirst($variedad_name)), $id_cultivo);

                    $cat_sembrada = strtoupper(trim($_POST['cat_sembrada']));
                    $cat_producir = strtoupper(trim($_POST['cat_producir']));
                    $cant_semilla = trim($_POST['cant_semilla']);
                    #$nro_lote = empty($_POST['lotenro']) ? '' : trim($_POST['lotenro']);
                    $fecha_siembra = $dates -> cambiar_tipo_mysql(trim($_POST['f_siembra']));
                    $plantas_ha = !empty($_POST['plantasha']) ? trim($_POST['plantasha']) : 0;
                    $cultivo_ant = Cultivo::getIdCultivo(trim($_POST['cult_anterior']));
                    $parcela =   trim($_POST['superficie']);
                    $isolicitud = trim($_POST['solicitud']);

                    $campanha = Campanha::getIdCampanha($tmp_campanha[0]);
                    //id de la superficie inscrita
                    $id_inscrita = Superficie::getIdSuperficieBySuperficie($parcela);
                    //guardamos los datos de la semilla
                    $semilla -> insertSemilla($nro_campo, $campanha, $id_cultivo, $id_variedad, $cat_sembrada, $cat_producir, $cant_semilla, $fecha_siembra, intval($plantas_ha), $cultivo_ant, $id_inscrita);                    
                    //obtenemos el id
                    $iSemilla = DBConnector::lastInsertId();          
                    //agregamos el id en la tabla semilla
                    $semilla -> updateSemilla($iSemilla, $id_inscrita);
                    //agregamos valores de los id en la tabla de relacion
                    $tiene -> solicitud_semilla($isolicitud, $iSemilla);
                    //agregamos la superficie de la parcela en la tabla superficie
                    Superficie::setInscritav2($id_inscrita, $iSemilla);
                    //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                    Solicitud::updateEstado($isolicitud);
                    //cambia el estado de semilla a 1
                    Semilla::updateEstado($iSemilla);

                    echo 'OK';
                    break;
                case 'superficie' :
                    $tipo = $_POST['tipo'];
                    $superficie = new Superficie();
                    switch ($tipo) {
                        case 'aprobada' :
                            $supInscrita = $_POST['inscrita'];
                            $supRechazada = $_POST['rechazada'];
                            $supRetirada = $_POST['retirada'];
                            $supAprobada = $supInscrita -$supRechazada-$supRetirada;
                            
                            $supRechazada = $dates -> cambiar_decimal($supRechazada);
                            $supRetirada = $dates -> cambiar_decimal($supRetirada);
                            
                            $semillaID = intval($_POST['isemilla']);
                            $productorID = intval($_POST['isolicitud']);
                            if (!empty($productorID)) {
                                #$supInscrita = Superficie::getSuperficieInscritaByIdSem($semillaID);
                                $superficie -> setRetirada($semillaID, $supRetirada);
                                $superficie -> setRechazada($semillaID, $supRechazada);
                                $superficie -> setAprobada($semillaID, $supAprobada);
                                
                                if (!DBConnector::nroError()) {
                                    Solicitud::updateEstado2($productorID);
                                    Semilla::updateEstadoSemilla($semillaID);
                                    Superficie::updateEstado($semillaID);
                                    echo 'OK';
                                }else{
                                    echo DBConnector::mensaje();
                                }
                            } else
                                echo 'ERROR productor: $productorID';
                            break;
                    }
                    break;
                case 'inspeccion' :
                    $fecha_vegetativa = trim($_POST['evegetativa']);
                    $inspeccion['etapa_vegetativa'] = $dates -> cambiar_tipo_mysql($fecha_vegetativa);
                    $inspeccion['observacion_1'] = (empty($_POST['obs_1'])) ? 'Ninguna' : trim($_POST['obs_1']);

                    $inspeccion['etapa_floracion'] = empty($_POST['efloracion']) ? $dates -> cambiar_tipo_mysql($_POST['efloracion']) : date('Y-m-d');
                    $inspeccion['observacion_2'] = (empty($_POST['obs_2'])) ? 'Ninguna' : trim($_POST['obs_2']);

                    $inspeccion['etapa_precosecha'] = empty($_POST['ecosecha']) ? $dates -> cambiar_tipo_mysql($_POST['ecosecha']) : date('Y-m-d');
                    $inspeccion['observacion_3'] = (empty($_POST['obs_3'])) ? 'Ninguna' : trim($_POST['obs_3']);
                    $inspeccion['ifecha'] = intval($_POST['ifecha']);
                    $inspeccion['isuperficie'] = intval($_POST['isuperficie']);
                    
                    //id de solicitud
                    $inspeccion['isolicitud'] = Tiene::getIdSolicitud(intval($_POST['isemilla']));
                    //id de la semilla
                    $inspeccion['isemilla'] = intval($_POST['isemilla']);
                    //id de la superficie
                    $inspeccion['iInspeccion'] = Superficie::getIdInspeccion($inspeccion['isuperficie']);
                    //si no hay superficie  es la primera inspeccion  realizada
                    if ($inspeccion['ifecha'] == 1) {
                        $inspecciones -> setInspeccion($inspeccion);
                        $inspecID = DBConnector::lastInsertId();
                        if (!DBConnector::nroError()) {
                            Realiza::superficie_inspeccion($inspeccion['isuperficie'], $inspecID);
                            $isemilla = Superficie::getIdSemillaByIdSuperficie($inspeccion['isuperficie']);
                            Superficie::updateEstado($isemilla);
                            Semilla::updateEstadoSemilla($isemilla);
                        }
                    } else {
                        $estado = $inspeccion['ifecha'];
                        switch ($estado) {
                            case 2 :
                                $inspecciones -> updateInspecciones('segunda', $inspeccion, $inspeccion['iInspeccion']);
                                break;
                            case 3 :
                                $inspecciones -> updateInspecciones('tercera', $inspeccion, $inspeccion['iInspeccion']);
                                break;
                        }
                        $inspecciones -> updEstado($inspeccion['iInspeccion']);
                        Semilla::updateEstadoSemilla($inspeccion['isemilla']);

                    }
                    if (!DBConnector::nroError()) {
                        Solicitud::updateEstado(Tiene::getIdSolicitud($inspeccion['isemilla']));
                        echo 'OK';
                    }

                    break;
                //inspeccion de plantines
                case 'inspeccion-plantines':
                    $plantines ['inspeccion'] = $dates->cambiar_tipo_mysql(strip_tags($_POST['inspeccion-plantines']));
                    $plantines ['tipo'] = strip_tags(trim($_POST['tipo-plantines']));
                    $plantines ['observacion'] =  strip_tags(trim($_POST['observacion-plantines']));
                    $plantines ['isolicitud'] = intval($_POST['solicitud']);
                    $plantines_obj->insertPlantines($plantines);
                    if(DBConnector::nroError()){
                        echo "ERROR :: ",DBConnector::mensaje();
                    }else{
                        //establece el estado en 5
                        Solicitud::updateEstadoPlantines($plantines['isolicitud']);
                            echo 'OK';
                    }
                break;
                case 'cosecha' :
                    $nro_campo = Semilla::getIdSemillaByNroCampo(trim($_POST['campo']));
                    if (!empty($_POST['f_emision']))
                        $f_emision = trim($_POST['f_emision']);
                    else
                        $f_emision = date('d-m-Y');
                    $categ_campo = intval($_POST['generacion']);
                    $rend_estimado = trim($_POST['estimado']);
                    $sup_parcela = trim($_POST['sup_parcela']);
                    $rend_campo = trim($_POST['rend_campo']);
                    #$rango_cupones = intval($_POST['rc-from']).'-'.intval($_POST['rc-to']);
                    $acondicionadora = trim($_POST['plantaa']);
                    $laboratorio = intval(trim($_POST['laboratorio']));
                    $idSuperficie = intval(trim($_POST['iSuperficie']));
                    $idSolicitud = intval(trim($_POST['iSolicitud']));
                    $idSemilla = intval(trim($_POST['iSemilla']));
                    $estadoSolicitud = Solicitud::getEstadoSolicitud($idSolicitud);
                    $nro_cupones = !empty($_POST['nro_cupones']) ? trim($_POST['nro_cupones']) : 0;
                    $f_emision2 = $dates -> cambiar_tipo_mysql($f_emision);
                    $estado = intval($_POST['estado']);
                    $indice = intval($_POST['indice']);

                    $cosechas -> insertHojaCosecha($nro_campo, $f_emision2, $categ_campo, $rend_estimado, $sup_parcela, $rend_campo, $acondicionadora, $laboratorio, $estado, $idSuperficie);
                    // si es papa actualizamos estado  a 
                    $cultivo = strtolower(trim($_POST['hiddencultivo']));
                    if ($cultivo != 'papa')
                        $solicitudes->updateEstado4($idSolicitud);
                    else {
                        if ($estadoSolicitud == 6){
                            $solicitudes->updateEstadoCosecha($idSolicitud);
                        }else{
                            $solicitudes->updateEstado4($idSuperficie);
                        }
                    }
                    Inspeccion::updInspeccion($idSuperficie);
                    echo "OK";
                    break;
                case 'semillaprod' :            
                    $isolicitud = intval($_POST['isolicitud']);                    
                    $icosecha = intval($_POST['icosecha']);
                    
                    $isemilla = Tiene::getIdSemilla($isolicitud);
                    Semillera::getNombreSemilleraByIdSolicitud($isolicitud);
                    $obj = DBConnector::objeto();
                    $semillera = $obj->semillera;

                    $nro_campo = strip_tags($_POST['campo']);
                    $semilla_neta = strip_tags($_POST['semilla_neta']);
                    $cat_obtenida = strip_tags($_POST['categ_obtenida']);
                    $etiqueta = strip_tags($_POST['etiqueta']);
                    $rango = trim($_POST['rc-to']);                    
                    $categoria_obtenida = strip_tags($_POST['categ_obtenida']);
                    
                    $nombreCategoria = explode('-', $categoria_obtenida);
                    $iCategoria = Categoria::getIdCategoriaByName(trim($nombreCategoria[0]) );
                    $iGeneracion = $generacion->getIdGeneracionByName(trim($nombreCategoria[1]) );
                    
                    $isuperficie = $cosechas -> getSuperficieSolicitudID($icosecha);
                    
                    $producciones -> setSemillaProd($isolicitud, $isemilla, $iCategoria, $iGeneracion, $semilla_neta, $cat_obtenida, $etiqueta, $rango);
                    
                    if (!DBConnector::nroError()) {
                        Solicitud::updateEstadoBySemillaProd($isolicitud);
                        $cultivo_name = Semilla::getCultivoByNroCampo($nro_campo);
                        if ($cultivo_name != 'papa')
                                HojaCosecha::updateStado($icosecha);
                        Solicitud::updateEstadoSemillaproducida($isolicitud);
                        echo 'OK';
                    } else {
                        echo 'error';
                    }
                    break;
                case 'cuenta' :
                    $semillera = strip_tags(trim($_POST['semillera']));
                    $id_semillera = Semillera::getIdSemillera($dates->modificar($semillera),$_POST['hiddenArea']);

                    $responsable = Responsable::setReponsable(utf8_decode(ucwords(trim($_POST['productor']))));
                    #echo DBConnector::mensaje();
                    //          if (isset($_POST['semillera']) && isset($_POST['campania']) && isset($_POST['cultivo']) && isset($_POST['variedad'])) {         $semillera = $_POST['semillera'];
                    $campanha = Campanha::getIdCampanha($_POST['campanha']);
                    #echo DBConnector::mensaje();
                    $cultivo = Funciones::search_tilde($_POST['cultivo']);
                    $variedad = Variedad::getIdVariedad($dates->search_tilde($dates->cambiar_signoByAcento($_POST['variedad'])) );
                    #echo DBConnector::mensaje();

                    $categoria_aprobada = intval($_POST['iSemilla']);
                    $superficie_aprobada = trim($_POST['superficie']);
                    $superficie_total = trim($_POST['superficietot']);
                    $bolsaEtiqueta = trim($_POST['bolsaEtiqueta']);
                    $sbtotal = empty($_POST['total']) ? trim($_POST['total']) : 0;
                    $total = !empty($_POST['total2']) ? trim($_POST['total2']) : 0;
                    $fecha = trim($_POST['f_pago']);
                    $monto_pagado = trim($_POST['montoPagado']);
                    $saldo_campania_ant = trim($_POST['campAnterior']);
                    $isolicitud = intval($_POST['iSolicitud']);
                    $isemilla = intval($_POST['iSemilla']);
                    $montoPagado = intval(trim($_POST['montoPagado']));
                    $saldoTotal = !empty($_POST['saldototal']) ? intval(trim($_POST['saldototal'])) : 0;
                    #Datos necesarios para costo
                    $inscripcion = trim($_POST['inscripcion']);
                    $inspeccion_campo = trim($_POST['icampo']);
                    $analisis_laboratorio_etiqueta = trim($_POST['anlet']);
                    $fecha = $dates -> cambiar_tipo_mysql($fecha);
                    
                    $productor = $solicitudes -> getIdSemilleristaByIdSolicitud($isolicitud);
                    /////// DATOS CUOTA                    
                    $cuota_fecha = Funciones::cambiar_tipo_mysql($_POST['cuota-fecha']);
                    $cuota_monto = floatval($_POST['cuota-monto']);
                    $cuota_descripcion = trim($_POST['cuota-descripcion']);  
                    
                    
                    $cuenta -> setCuenta($responsable, $id_semillera, $productor, $campanha, $variedad, $categoria_aprobada, $superficie_aprobada, $superficie_total, $bolsaEtiqueta, $sbtotal, $total, $fecha, $montoPagado, $saldoTotal, $saldo_campania_ant, $isemilla);
                    $icuenta = DBConnector::lastInsertId();
                    $cuenta -> setCuota($cuota_monto, $cuota_fecha, $cuota_descripcion, $icuenta);

                    Tiene::cuenta_semilla($icuenta, $isemilla);
                    if (!DBConnector::nroError()) {
                        if ($saldoTotal == 0)
                            Solicitud::updateEstadoCuenta($isolicitud, 13);

                        echo 'OK';
                    } else {
                        echo DBConnector::mensaje();
                    }

                    break;
            }
            break;
        //mostrar todos los datos segun el tipo de peticion
        case 'ver' :
            switch($pagina) {
                case 'apellidos' :
                    $json = array();
                    $q = isset($_GET['term']) ? $_GET['term'] : '';
                    Solicitud::getApellidos($q);
                    while ($obj = DBConnector::objeto()) {
                        array_push($json, utf8_encode($obj -> apellido));
                    }
                    echo json_encode($json);
                    break;
                case 'auto' :
                    $q = $_GET['q'];
                    $sistema = substr(trim($_GET['pro']), 0, 4);
                    $json_coincidencias = array();
                    $solicitudes -> getSemillerabyNombre($q, $sistema);
                    while ($fila = DBConnector::resultado())
                        array_push($json_coincidencias, utf8_encode($fila -> semillera));
                    echo json_encode($json_coincidencias);
                    break;                
                case 'campos' ://devuelve los numeros de campos segun el id del productor
                    $isolicitud = intval($_GET['id']);
                    #$isemillerista = Solicitud::getIdSemilleristaByIdSolicitud($isolicitud);
                    $semillera = trim($_GET['semillera']);
                    $semilla -> getNroCamposProductor($isolicitud,$semillera,FALSE);
                    $json['nroCampo'] = array();
                    $json['comunidad'] = array();
                    $json['campoComunidad'] = array();
                    while ($fila = DBConnector::objeto()){
                        array_push($json['nroCampo'], $fila -> nro_campo);
                        array_push($json['comunidad'],$fila -> comunidad);
                        array_push($json['campoComunidad'],$fila->nro_campo.'='.$fila->comunidad);
                    }
                    echo json_encode($json);
                    break;                
                case 'campos_cosecha' :#numeros de campos disponibles para una hoja de cosecha                    
                    $id = intval($_GET['id']);//numeros de campos segun id de semillerista
                    $semilla -> getNroCamposProductor($id);
                    $campos_array = array();
                    while ($fila = DBConnector::resultado()) {
                        array_push($campos_array, $fila -> nro_campo);
                    }
                    $json['nroCampo'] = $campos_array;

                    echo json_encode($json);
                    break;                
                case 'colugar' :// devuelve las coincidencias segun el lugar {provincia|municipio|comunidad}
                    $lugar = trim($_GET['seccion']);
                    $json = array();
                    switch ($lugar) {
                        case 'provincia' :
                            $solicitudes -> getProvincias();
                            while ($fila = DBConnector::objeto()) {
                                $json[$fila -> id_provincia] = ($fila -> nombre_provincia);
                            }
                            echo json_encode($json);
                            break;
                        case 'municipio' :
                            $prov = utf8_encode(intval($_GET['prov']));
                            #echo $prov;exit;
                            $solicitudes -> getMunicipios($prov);
                            if (!DBConnector::nroError()) {
                                while ($fila = DBConnector::objeto())
                                    $json[$fila -> id_municipio] = ($fila -> nombre_municipio);
                            } else {
                                echo json_encode(DBConnector::mensaje());
                            }
                            echo json_encode($json);
                            break;
                        case 'comunidad' :
                            $q = $dates->search_tilde(trim($_GET['term']));
                            Solicitud::getComunidadByNombre($q);
                            $coincidencias = array();
                            while ($fila = DBConnector::objeto())
                                array_push($coincidencias, $fila -> comunidad);
                            echo json_encode($coincidencias);
                            break;
                    }
                    break;                
                case 'comunidad'://devuelve las comunidades
                    $semillera = trim($_GET['semillera']);
                    Solicitud::getListaComunidadBySemillera($q);
                    break;                
                case 'datosCosecha' ://muestra la superficie de la parcela y la categoria a producir                    
                    $id_solicitud = intval($_GET['isolicitud']);
                    $isemilla = Tiene::getIdSemilla($id_solicitud);
                    $cultivo = strtolower($_GET['cultivo']);
                    $nro_campo = $_GET['nro_campo'];

                    if ($_GET['semillera'] != '') {
                        $semillera = $_GET['semillera'];
                    } else {
                        $semillera = '';
                    }

                    $superficie -> getSuperficieCampo($id_solicitud, $cultivo, $nro_campo);

                    if (DBConnector::filas())
                        while ($temp = DBConnector::objeto()) {
                            $datosCosecha['superficie'] = $temp -> superficie;
                        }
                    else
                        $datosCosecha['superficie'] = '';

                    $igeneracion = Semilla::getCategoriaSembradaByIdSemilla($isemilla);
                    $iCategoria = ($isemilla)?$categoria->getCategoriaByIdGeneracion($igeneracion):-1;
                    $datosCosecha['icategoria'] = $igeneracion;
                    $datosCosecha['igeneracion'] = $iCategoria;
                    if ($iCategoria>0){
                        $nombre_categoria = $categoria->getCategoriaById($iCategoria);    
                        $generacion -> getGeneracionByCategoria($nombre_categoria);
                        $nombre_generacion = $generacion ->getNombreGeneracionByIdGeneracion($igeneracion);
                        $row = DBConnector::objeto();
                        if ($nombre_generacion == 'Primera') {
                             $union = '1';
                        } elseif ($nombre_generacion == 'Segunda') {
                            $union = '2';
                        } elseif ($nombre_generacion == 'Tercera') {
                            $union = '3';
                        } else {
                            $union = 'B';
                        }
                    }else{
                        $nombre_categoria = 'registrada';
                        $union = '1';
                    }
                    
                    
                    $datosCosecha['categCampo'] = $nombre_categoria . '-' . $union;

                    #$isemilla = $semilla -> getIdSemillaBycampo($nro_campo);
                    $superficieID = ($isemilla)?Superficie::getSuperficieByIDNroCampo($isemilla):-1;
                    $datosCosecha['isuperficie'] = $superficieID;

                    echo json_encode($datosCosecha);

                    break;
                                
                case 'estado_semilla_superficie'://devuelve el estado-id semilla-id superficie segun  id solicitud
                        $id_solicitud = intval($_GET["solicitud"]);
                        
                        $estado = Solicitud::getEstadoSolicitud($id_solicitud);
                        $isemilla = Tiene::getIdSemilla($id_solicitud);
                        $nroCampo = (is_numeric($isemilla))?Semilla::getNroCampoByIdSemilla($isemilla):0;
                        $isuperficie = ($isemilla)?Superficie::getIdSuperficieByIdSemilla($isemilla):0;                        
                        
                        $json['estado'] = $estado;
                        $json['semilla'] = $isemilla;
                        $json['superficie'] = $isuperficie;
                        $json['nrocampo'] = $nroCampo;//para inspeccion
                        if ($estado == 4){
                            $fecha = explode("-",Semilla::getFechaSiembraByIdSemilla($isemilla)); //fecha de siembra
                            $json['dia'] = $fecha[2];
                            $json['mes'] = $fecha[1];
                            $json['anho'] = $fecha[0];
                        }
                        if ($estado == 5 ){
                            $inspecciones -> getInspecciones($isuperficie);
                            $fila = DBConnector::objeto();
                            $fecha = explode("-",$fila->vegetativa);
                            $json['dia'] = $fecha[2];
                            $json['mes'] = $fecha[1];
                            $json['anho'] = $fecha[0];
                        } 
                        if ($estado == 6 ){
                            $inspecciones -> getInspecciones($isuperficie);
                            $fila = DBConnector::objeto();
                            $fecha = explode("-",$fila->floracion);
                            $json['dia'] = $fecha[2];
                            $json['mes'] = $fecha[1];
                            $json['anho'] = $fecha[0];
                        }
                        echo json_encode($json);                        
                break;

                case 'inspeccion' :
                    if (isset($_GET['superficie'])) {
                        $isuperficie = $_GET['superficie'];
                        $var = Inspeccion::getInspecciones($isuperficie);
                    } else {
                        $isuperficie = $_GET['semilla'];
                        $var = Inspeccion::getInspecciones($isuperficie);
                    }

                    if (DBConnector::filas()) {
                        $fecha = new Funciones();
                        while ($fila = DBConnector::objeto()) {
                            if ($fila -> primera != '') {
                                $json['primera'] = $fecha -> cambiar_formato_fecha($fila -> primera);
                                $json['obsprimera'] = $fila -> observacion_1;
                            } else {
                                $json['primera'] = '';
                                $json['obsprimera'] = '';
                            }
                            if ($fila -> segunda != '') {
                                $json['segunda'] = $fecha -> cambiar_formato_fecha($fila -> segunda);
                                $json['obssegunda'] = $fila -> observacion_2;
                            } else {
                                $json['primera'] = '';
                                $json['obssegunda'] = '';
                            }
                            if ($fila -> tercera != '') {
                                $json['tercera'] = $fecha -> cambiar_formato_fecha($fila -> tercera);
                                $json['obstercera'] = $fila -> observacion_3;
                            } else {
                                $json['primera'] = '';
                                $json['obstercera'] = '';
                            }
                        }
                    } else {
                        $json['primera'] = '';
                        $json['obsprimera'] = '';
                        $json['segunda'] = '';
                        $json['obssegunda'] = '';
                        $json['tercera'] = '';
                        $json['obstercera'] = '';
                    }
                    echo json_encode($json);
                    break;
                // devuelve el id de semillerista segun nombre completo
                case 'isemillerista':
                    $nombre = trim($_GET['nombre']);
                    $json['isemillerista'] = Semillerista::getIdSemilleristaByNombre($nombre);
                
                    echo json_encode($json);
                break;
                // devuelve el nro de solicitud de una semillera segun nombre de semillera
                case 'isemillera' :
                    $semillera = trim($_GET['sem']);
                    echo json_encode(Semillera::getIdSemillera($semillera,1));
                    break;
                //devuelve el id de la semilla segun el nro de campo
                case 'isemilla' :
                    $nroCampo = isset($_GET['nro']) ? $_GET['nro'] : '';
                    $idSemillerista = isset($_GET['id']) ? $_GET['id'] : '';

                    $nro_campo = $semilla -> getIdSemillaByCampo($nroCampo, $idSemillerista);

                    $resultado['isemilla'] = $nro_campo;

                    echo json_encode($resultado);
                    break;
                case 'ls_cultivos': // lista de cultivos
                    $gestion = isset($_GET['gestion'])?intval($_GET['gestion']):''; 
                    Cultivo::getCultivos($gestion);
                    $json['cultivo']=array();
                    while ($obj = DBConnector::objeto()){
                        array_push($json['cultivo'],$obj->cultivo);
                    }                    
                    echo json_encode($json);
                    break;
                case 'lst_productores' : // lista de productores
                    $tabla = $_GET['tabla'];
                    $semillera = $dates->search_tilde(trim($_GET['semillera']));
                    $id_area = $_GET['area'];
                    $dates -> noCache();
                    
                    Solicitud::getProductoresSemillera(Semillera::getIdSemillera(str_replace('*', '_', $semillera),$id_area), $tabla, Sistema::getSistemaById($id_area));
                    $json = array();
                    if (DBConnector::filas() >= 1) {
                        while ($fila = DBConnector::objeto()) {
                            array_push($json, utf8_encode($fila -> nombre));
                        }
                    }

                    echo json_encode(array_unique($json));
                    break;   
                case 'lst_productoresBySemillera' : // lista de productores para grafico
                    $semillera = $dates->search_tilde(trim($_GET['slr']));
                    $dates -> noCache();
                    
                    Solicitud::getListProductoresBySemilleraForGraph($semillera);
                    $json['semillerista'] = array();
                    if (DBConnector::filas() >= 1) {
                        while ($fila = DBConnector::objeto()) {
                            array_push($json['semillerista'], utf8_encode($fila -> semillerista));
                        }
                        $json['msg'] = 'OK';
                    }else{
                        $json['semillerista'] = '';
                        $json['msg'] = DBConnector::filas();
                    }

                    echo json_encode($json);
                    break;       
                case 'lst_semilleras_acre': // lista de semilleras para acreedores
                    $area = $_GET['area'];
                    $solicitudes->getSemillerasBySistema('acreedores',$area);
                    $json['semillera']=array();
                    while ($obj = DBConnector::objeto()){
                        array_push($json['semillera'],utf8_encode($obj->semillera));
                    }
                    echo json_encode($json['semillera']);
                    break;
                case 'proceso' : // creacion de resumen de certificacion
                    $inicio = (isset($_GET['inicio']))?trim($_GET['inicio']):'A';
                    $final = (isset($_GET['inicio']))?trim($_GET['final']):'F';
                    $sistema = Sistema::getIdSistema($_GET['sistema']);
                    $gestion = date('Y'); //gestion actual
                    //id de solicitud
                    $id_solicitud = array();
                    //nombre de las semilleras
                    $semillera = array();
                    //nombre de los semilleristas
                    $productor = array();
                    //id de semilleristas
                    $id_productor = array();
                    //nro_campo y cultivo de un semillerista
                    $campos = array();
                    $cultivos = array();
                    //etapa en la que se encuentra el semillerista
                    $etapa = array();

                    //total de semilleristas
                    $cantidad = array();
                    Solicitud::getSemilleras($sistema,$inicio,$final,$gestion);
                    while ($fila = DBConnector::objeto()) {
                        array_push($semillera, utf8_encode($fila -> semillera));
                    }
                    
                    $counter = 0;
                    $limit = count($semillera);
                    #echo count($semillera).'='.count($id_solicitud);
                    while ($counter < $limit) {
                        Solicitud::getCantidad($dates->search_tilde($semillera[$counter]));
                        while ($aux = DBConnector::objeto()) {
                            array_push($cantidad, $aux -> cantidad);
                        }

                        Solicitud::getProductoresCertifica($dates->search_tilde($semillera[$counter]));
                        while ($fila = DBConnector::objeto()) {
                            $nombre = utf8_encode($fila -> semillerista);
                            #echo $nombre,'?',$fila -> semillerista;
                            array_push($productor, $dates-> cambiar_acentoBySigno($nombre));
                            array_push($etapa, $fila -> estado);
                            //id solicitud
                            array_push($id_productor, $fila -> id);
                        }
                        $counter++;
                    }

                    $searcher = FALSE;
                    include '../vista/certificacion/datos/certificacion.php';
                    break;     
                //registro de semillerista               
                case 'registro':
                    $isolicitud = intval($_GET['id']);  //idsolicitud
                    #echo "SOLICITUD : $isolicitud ";         
                                                
                    $estado = $solicitudes->getEstadoRegistro($isolicitud);
                    $json['estado'] = $estado;
                    if ($estado == 0){
                        Solicitud::getRegistro($isolicitud);
                        #mostrar solo solicitud
                        $obj = DBConnector::objeto();
                        $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                        $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                        $json['nosolicitud']=$obj->nro_solicitud;
                        $json['estado']=$obj->estado;
                        $json['departamento']=$obj->nombre_departamento;
                        $json['provincia']=$obj->provincia;
                        $json['municipio']=$obj->municipio;
                        $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                        $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                    }else{
                        switch ($estado){
                            case ($estado == 1):
                                #mostrar solicitud y semilla
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst(utf8_encode($obj->variedad));
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                            break;
                            case ($estado == 4):
                                #mostrar solicitud,semilla y superficie
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst(utf8_encode($obj->variedad));
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                            break;
                            case (($estado ==5)|| ($estado ==6)|| ($estado ==7)):
                                #mostrar solicitud,semilla,superficie e inspeccion
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst(utf8_encode($obj->variedad));
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                //INSPECCION
                                Inspeccion::getRegistro($isemilla);
                                $obj = DBConnector::objeto();  
                                $json['vegetativa']=$dates->cambiar_formato_fecha($obj->vegetativa);
                                $json['obs_vegetativa']=($obj->observacion_1)?$obj->observacion_1:'-';
                                $json['floracion']=($obj->floracion!='0000-00-00')?$dates->cambiar_formato_fecha($obj->floracion):'-';
                                $json['obs_floracion']=($obj->observacion_2)?$obj->observacion_2:'-';
                                $json['cosecha']=($obj->precosecha!='0000-00-00')?$dates->cambiar_formato_fecha($obj->precosecha):'-';
                                $json['obs_cosecha']=($obj->observacion_3)?$obj->observacion_3:'-';

                            break;
                            case ($estado ==8):
                                #mostrar solicitud,semilla,superficie,inspeccion  y hoja de cosecha
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst(utf8_encode($obj->variedad));
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                //INSPECCION
                                Inspeccion::getRegistro($isemilla);
                                $obj = DBConnector::objeto();  
                                $json['vegetativa']=$dates->cambiar_formato_fecha($obj->vegetativa);
                                $json['obs_vegetativa']=($obj->observacion_1)?$obj->observacion_1:'-';
                                $json['floracion']=($obj->floracion!='0000-00-00')?$dates->cambiar_formato_fecha($obj->floracion):'-';
                                $json['obs_floracion']=($obj->observacion_2)?$obj->observacion_2:'-';
                                $json['cosecha']=($obj->precosecha!='0000-00-00')?$dates->cambiar_formato_fecha($obj->precosecha):'-';
                                $json['obs_cosecha']=($obj->observacion_3)?$obj->observacion_3:'-';
                                //HOJA DE COSECHA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                $icosecha = $cosechas->getIdCosechaByIdSem($isemilla);
                                HojaCosecha::getRegistro2($icosecha);
                                $obj = DBConnector::objeto();  
                                $json['emision'] = $obj->fecha_emision;
                                $json['rendEstimado'] = $obj->rendimiento_estimado;
                                $json['rendCampo'] = $obj->rendimiento_campo;
                                $json['cupones'] = $obj->nro_cupones;
                                $json['plantaa'] = $obj->planta_acondicionadora;
                            break;
                            case ($estado == 9):
                                #mostrar solicitud,semilla,superficie,inspeccion,hoja de cosecha y muestra de laboratorio
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst($obj->variedad);
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                //INSPECCION
                                Inspeccion::getRegistro($isemilla);
                                $obj = DBConnector::objeto();  
                                $json['vegetativa']=$dates->cambiar_formato_fecha($obj->vegetativa);
                                $json['obs_vegetativa']=($obj->observacion_1)?$obj->observacion_1:'-';
                                $json['floracion']=($obj->floracion!='0000-00-00')?$dates->cambiar_formato_fecha($obj->floracion):'-';
                                $json['obs_floracion']=($obj->observacion_2)?$obj->observacion_2:'-';
                                $json['cosecha']=($obj->precosecha!='0000-00-00')?$dates->cambiar_formato_fecha($obj->precosecha):'-';
                                $json['obs_cosecha']=($obj->observacion_3)?$obj->observacion_3:'-';
                                //HOJA DE COSECHA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                $icosecha = $cosechas->getIdCosechaByIdSem($isemilla);
                                HojaCosecha::getRegistro2($icosecha);
                                $obj = DBConnector::objeto();  
                                $json['emision'] = $obj->fecha_emision;
                                $json['rendEstimado'] = $obj->rendimiento_estimado;
                                $json['rendCampo'] = $obj->rendimiento_campo;
                                $json['cupones'] = $obj->nro_cupones;
                                $json['plantaa'] = $obj->planta_acondicionadora;
                            break;
                            case ($estado == 11):
                                #mostrar solicitud,semilla,superficie,inspeccion,hoja de cosecha,muestra de laboratorio y resultado de laboratorio
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst($obj->variedad);
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                //INSPECCION
                                Inspeccion::getRegistro($isemilla);
                                $obj = DBConnector::objeto();  
                                $json['vegetativa']=$dates->cambiar_formato_fecha($obj->vegetativa);
                                $json['obs_vegetativa']=($obj->observacion_1)?$obj->observacion_1:'-';
                                $json['floracion']=($obj->floracion!='0000-00-00')?$dates->cambiar_formato_fecha($obj->floracion):'-';
                                $json['obs_floracion']=($obj->observacion_2)?$obj->observacion_2:'-';
                                $json['cosecha']=($obj->precosecha!='0000-00-00')?$dates->cambiar_formato_fecha($obj->precosecha):'-';
                                $json['obs_cosecha']=($obj->observacion_3)?$obj->observacion_3:'-';
                                //HOJA DE COSECHA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                $icosecha = $cosechas->getIdCosechaByIdSem($isemilla);
                                HojaCosecha::getRegistro2($icosecha);
                                $obj = DBConnector::objeto();  
                                $json['emision'] = $obj->fecha_emision;
                                $json['rendEstimado'] = $obj->rendimiento_estimado;
                                $json['rendCampo'] = $obj->rendimiento_campo;
                                $json['cupones'] = $obj->nro_cupones;
                                $json['plantaa'] = $obj->planta_acondicionadora;
                            break;
                            case ($estado == 12):
                                #mostrar solicitud,semilla,superficie,inspeccion,hoja de cosecha,muestra de laboratorio,resultado de laboratorio y semilla producida
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst($obj->variedad);
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                //INSPECCION
                                Inspeccion::getRegistro($isemilla);
                                $obj = DBConnector::objeto();  
                                $json['vegetativa']=$dates->cambiar_formato_fecha($obj->vegetativa);
                                $json['obs_vegetativa']=($obj->observacion_1)?$obj->observacion_1:'-';
                                $json['floracion']=($obj->floracion!='0000-00-00')?$dates->cambiar_formato_fecha($obj->floracion):'-';
                                $json['obs_floracion']=($obj->observacion_2)?$obj->observacion_2:'-';
                                $json['cosecha']=($obj->precosecha!='0000-00-00')?$dates->cambiar_formato_fecha($obj->precosecha):'-';
                                $json['obs_cosecha']=($obj->observacion_3)?$obj->observacion_3:'-';
                                //HOJA DE COSECHA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                $icosecha = $cosechas->getIdCosechaByIdSem($isemilla);
                                HojaCosecha::getRegistro2($icosecha);
                                $obj = DBConnector::objeto();  
                                $json['emision'] = $obj->fecha_emision;
                                $json['rendEstimado'] = $obj->rendimiento_estimado;
                                $json['rendCampo'] = $obj->rendimiento_campo;
                                $json['cupones'] = $obj->nro_cupones;
                                $json['plantaa'] = $obj->planta_acondicionadora;
                                //LABORATORIO
                                $cultivo = Semilla::getCultivoCertificaByIdSolicitud($isolicitud);
                                $excepciones = array('Papa','Plantines');
                                if (!in_array($cultivo,$excepciones)){
                                    Muestra_laboratorio::getMuestraLaboratorioByIdSemilla($isemilla);
                                    $obj = DBConnector::objeto();                                
                                    $json['origen']=$obj->origen;
                                    $json['recepcion']=$dates->cambiar_formato_fecha($obj->fecha_recepcion);
                                    $json['noanalisis']=$obj->nro_analisis;                                
                                    Resultado_muestras::getResultadoMuestrasByIdMuestra($obj->id_muestra);
                                    $obj = DBConnector::objeto();                                
                                    $json['resultado']=$dates->cambiar_formato_fecha($obj->fecha);
                                    $json['pureza'] = $obj->pureza;
                                    $json['germinacion']=$obj->germinacion;
                                    $json['humedad']=$obj->humedad;
                                    $json['observacion']=$obj->observacion; 
                                }
                                                                
                                //SEMILLA PRODUCIDA
                                $semilla -> getIdSemillaProducidaByIdSolicitud($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['nocampo']=$obj->nro_campo;
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['generacion']=$obj->generacion;
                                $json['semilla_neta']=$obj->semilla_neta;
                                $json['catProducir']=$obj->categoria_obtenida;
                                $json['nro_etiquetas']=$obj->nro_etiqueta;
                                $json['rango']=$obj->rango;
                                $json['rango_final'] = $obj->rango +$obj->nro_etiqueta;
                            break;
                            case ($estado == 13):
                                #mostrar solicitud,semilla,superficie,inspeccion,hoja de cosecha,muestra de laboratorio,resultado de laboratorio,semilla producida y cuenta.
                                Solicitud::getRegistro($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['semillerista'] = $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                $json['semillera']=$dates->cambiar_acentoBySignov2(utf8_encode($obj->semillera));
                                $json['nosolicitud']=$obj->nro_solicitud;
                                $json['estado']=$obj->estado;
                                $json['departamento']=$obj->nombre_departamento;
                                $json['provincia']=$obj->provincia;
                                $json['municipio']=$obj->municipio;
                                $json['comunidad']=$dates->cambiar_acentoBySigno($obj->comunidad);
                                $json['fsolicitud' ]=$dates->cambiar_formato_fecha($obj->fecha);
                                //SEMILLA
                                $isemilla = Tiene::getIdSemilla($isolicitud);
                                Semilla::getRegistro($isemilla);
                                $obj = DBConnector::objeto();                        
                                $json['nocampo']=$obj->nro_campo;
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']= ucfirst($obj->variedad);
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['catProducir']=$obj->categoria_producir;
                                $json['cantidad_semilla']=$obj->cantidad_semilla;
                                $json['fecha_siembra']=$dates->cambiar_formato_fecha($obj->fecha_siembra);
                                $json['plantas']=number_format($obj->plantas_hectarea, 0, '.', ' ');
                                //SUPERFICIE
                                Superficie::getRegistro($isemilla);
                                $obj = DBConnector::objeto();
                                $json['inscrita']=$obj->inscrita;
                                $json['retirada']=$obj->retirada;
                                $json['rechazada']=$obj->rechazada;
                                $json['aprobada']=$obj->aprobada;
                                //INSPECCION
                                $isuperficie = Superficie::getIdSuperficieByIdSemilla($isemilla);
                                Inspeccion::getInspeccionById($isuperficie);
                                $obj = DBConnector::objeto();
                                $json['vegetativa'] = $dates->cambiar_formato_fecha($obj->primera);
                                $json['msg_vegetativa'] = $obj->observacion_1;
                                $json['floracion'] = $dates->cambiar_formato_fecha($obj->segunda);
                                $json['msg_floracion'] = $obj->observacion_2;
                                $json['cosecha'] = $dates->cambiar_formato_fecha($obj->tercera);
                                $json['msg_cosecha'] = $obj->observacion_3;
                                //HOJA DE COSECHA
                                $icosecha = $cosechas->getIdCosechaByIdSem($isemilla);
                                HojaCosecha::getHojaCosechaCertificaByIdSolicitud($icosecha);
                                $obj = DBConnector::objeto();
                                $json['nocampo'] = $obj->nro_campo;
                                $json['emision'] = $obj ->fecha_emision;
                                $json['rendEstimado'] = $obj->rendimiento_estimado;
                                $json['rendCampo'] = $obj->rendimiento_campo;
                                $json['cupones'] = $obj->nro_cupones;
                                $json['plantaa'] = $obj->planta_acondicionadora;
                                //LABORATORIO
                                Muestra_laboratorio::getMuestraLaboratorioByIdSemilla($isemilla);
                                $obj = DBConnector::objeto();                                
                                $json['origen']=$obj->origen;
                                $json['recepcion']=$dates->cambiar_formato_fecha($obj->fecha_recepcion);
                                $json['noanalisis']=$obj->nro_analisis;                                
                                Resultado_muestras::getResultadoMuestrasByIdMuestra($obj->id_muestra);
                                $obj = DBConnector::objeto();                                
                                $json['resultado']=$dates->cambiar_formato_fecha($obj->fecha);
                                $json['pureza'] = $obj->pureza;
                                $json['germinacion']=$obj->germinacion;
                                $json['humedad']=$obj->humedad;
                                $json['observacion']=$obj->observacion;                                 
                                //SEMILLA PRODUCIDA
                                $semilla -> getIdSemillaProducidaByIdSolicitud($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['nocampo']=$obj->nro_campo;
                                $json['catSembrada']=$obj->categoria_sembrada;
                                $json['generacion']=$obj->generacion;
                                $json['semilla_neta']=$obj->semilla_neta;
                                $json['catProducir']=$obj->categoria_obtenida;
                                $json['nro_etiquetas']=$obj->nro_etiqueta;
                                $json['rango']=$obj->rango;
                                $json['rango_final'] = $obj->rango +$obj->nro_etiqueta;
                                //CUENTA
                                Cuenta::getCuentaByIdSolicitud($isolicitud);
                                $obj = DBConnector::objeto();
                                $json['responsable']=utf8_encode($obj->responsable);
                                $json['campanha']=$obj->campanha;
                                $json['cultivo']=$obj->cultivo;
                                $json['variedad']=$obj->variedad;
                                $json['categoria_aprobada']=$obj->categoria_aprobada;
                                $json['superficie_aprobada']=$obj->superficie_aprobada;
                                $json['inscripcion']=$obj->inscripcion*$obj->superficie_aprobada;
                                $json['inspeccion']=$obj->inspeccion*$obj->superficie_aprobada;
                                $json['analisis']=$obj->analisis*$obj->superficie_aprobada;
                                $json['acondicionamiento']=$obj->acondicionamiento;
                                $json['plantines']=$obj->plantines;
                                $json['fecha']=$dates->cambiar_formato_fecha($obj->fecha);
                            break;
                        }
                    }
                     echo json_encode($json);
                        break;                
                case 'responsable' ://devuelve lista de responsables
                    $json = array();

                    $q = isset($_GET['term']) ? $_GET['term'] : '';
                    Responsable::getListResponsables($q);
                    #echo DBConnector::mensaje();
                    while ($obj = DBConnector::objeto()) {
                        array_push($json, utf8_encode($obj -> nombre));
                    }
                    #var_dump($json);
                    echo json_encode($json);
                    break;
                case 'semillera' : // semilleras segun tipo
                    $tipo = $_GET['chs'];
                    $semilleras_array = array();
                    switch ($tipo) {
                        case 'all' :
                            $etapa = $_GET['tbl'];
                            $std = isset($_GET['std']) ? trim($_GET['std']) : '';
                            $solicitudes -> getSemillerasBySistema($etapa, $std);
                            $list_semilleras = array();
                            while ($fila = DBConnector::objeto()) {
                                if ($fila -> semillera != '' or $fila -> semillera != 'null')
                                    array_push($list_semilleras, $fila -> semillera);
                            }
                            if (DBConnector::filas() > 0) {
                                $json['semilleras'] = $list_semilleras;
                                $json['msg'] = 'OK';
                            } else {
                                $json['semilleras'] = '';
                                $json['msg'] = 'Error. No existen semilleras registradas';
                            }
                            echo json_encode($json);
                            break;
                        case 'solicitud' :
                            $provincia = intval($_GET['provincia']);
                            $municipio = intval($_GET['municipio']);
                            $area =  Area::getIdAreaByNombre($_GET['area']);
                            $json = array();
                            Semillera::getListSemilleras($provincia, $municipio, $area);
                            while ($obj = DBConnector::objeto()) {
                                $json[$obj -> id_semillera] = utf8_encode($obj -> semillera);
                            }
                            echo json_encode($json);
                            break;
                    }
                    break;
                case 'nombres' :
                    $json = array();

                    $q = isset($_GET['term']) ? $_GET['term'] : '';
                    Solicitud::getNombres($q);
                    while ($obj = DBConnector::objeto()) {
                        array_push($json, utf8_encode($obj -> nombre));
                    }
                    echo json_encode($json);
                    break;
                case 'solicitudID' :
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);

                    $solicitudes -> getIdSolicitudProductor($nombre, $apellido);
                    //echo DBConnector::mensaje();
                    $row = DBConnector::objeto();
                    $json['isolicitud'] = $row -> id_solicitud;

                    echo json_encode($json);
                    break;
                //busqueda de texto para la opcion cuenta en certificacion    
                case 'semilleras_cuenta':
                    $opcion = trim($_GET['opc']);
                    switch ($opcion) {
                        case 'semillera':
                            $semillera = trim($_GET['slr']);
                            Cuenta::getCampoCultivoVariedadCuentaBySemillera($semillera);
                            $json['comunidad'] = array();
                            $json['semillerista'] = array();
                            $json['nrocampo'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();
                            $json['campanha'] = array();
                            $json['semcamculvar'] = array();
                            $json['isolicitud'] = array();
                            $json['isemilla']=array();
                            $json['estado']=array();
                            #$row = DBConnector::objeto();
                            #var_dump( DBConnector::objeto());
                            while($obj = DBConnector::objeto()){
                                #echo $dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista));
                                array_push($json['comunidad'],str_replace("'","$",$obj->comunidad) );
                                array_push($json['semillerista'],$dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista)) );
                                array_push($json['nrocampo'],$obj->nro_campo);
                                array_push($json['cultivo'],$obj->cultivo);
                                array_push($json['variedad'],$dates->cambiar_acentoBySigno(utf8_encode($obj->variedad) ) );
                                array_push($json['campanha'],$obj->campanha);
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['isemilla'],$obj->id_semilla);
                                array_push($json['estado'],$obj->estado_solicitud);
                                array_push($json['semcamculvar'],$dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista)).'='.$obj->nro_campo.'='.$obj->cultivo.'='.$dates->cambiar_acentoBySigno(utf8_encode($obj->variedad)).'='.$obj->campanha.'='.$obj->estado_solicitud.'='.$obj->id_solicitud.'='.$obj->id_semilla.'='.str_replace("'","$",$obj->comunidad));
                            }
                            $json['total'] = DBConnector::filas();
                            $json['isuperficie'] = array();
                            foreach ($json['isemilla'] as $id) {
                                $id_spf = Superficie::getIdSuperficieByIdSemilla($id);
                                if ($id_spf>0){
                                    array_push($json['isuperficie'],$id_spf);
                                }else{
                                    array_push($json['isuperficie'],0);
                                }
                            }
                            //agregar isuperficie a cada  fila
                            foreach ($json['semcamculvar'] as $key => $value) {
                                $json['semcamculvar'][$key] .= '='.$json['isuperficie'][$key];
                            } 
                            #var_dump($json);     
                            echo json_encode($json);
                            break;                           
                        case 'semillerista':
                            $semillerista = $dates->search_tilde(trim($_GET['smr']));
                            Cuenta::getSemilleraCampanhaCultivoVariedadBySemillerista($semillerista); 
                            $json['comunidad'] = array();  
                            $json['semillera'] = array();
                            $json['campanha'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();                            
                            $json['semcamculvar'] = array();
                            $json['isolicitud'] = array();
                            $json['isemilla']=array();
                            $json['estado']=array();
                            $json['semillerista'] = array();
                            $json['categoria_producir'] = array();
                            
                            #var_dump(DBConnector::objeto());
                            while($obj = DBConnector::objeto()){
                                array_push($json['comunidad'],str_replace("'","$",$obj->comunidad));
                                array_push($json['semillera'],utf8_encode($obj->semillera));
                                array_push($json['semillerista'],$dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista)) );
                                array_push($json['campanha'],$obj->campanha);
                                array_push($json['cultivo'],utf8_encode($obj->cultivo));
                                array_push($json['variedad'],$dates->cambiar_acentoBySigno(utf8_encode($obj->variedad)));
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['isemilla'],$obj->id_semilla);
                                array_push($json['estado'],$obj->estado_solicitud);
                                array_push($json['categoria_producir'],$obj->categoria_producir);
                                array_push($json['semcamculvar'],utf8_encode($obj->semillera).'='.$obj->campanha.'='.utf8_encode($obj->cultivo).'='.utf8_encode($obj->variedad).'='.$obj->estado_solicitud.'='.$obj->id_solicitud.'='.$obj->id_semilla);
                            }
                            $json['total'] = DBConnector::filas();
                            $json['isuperficie'] = array();
                            foreach ($json['isemilla'] as $key=>$id_semilla) {
                                $valores = $json['semillera'][$key].'='.$json['campanha'][$key].'='.$json['cultivo'][$key].'='.$json['variedad'][$key].'='.$json['estado'][$key].'='.$json['isolicitud'][$key].'='.$id_semilla;
                                $id_spf = Superficie::getIdSuperficieByIdSemilla($id_semilla);
                                if ($id_spf>0){
                                    array_push($json['isuperficie'],$id_spf);
                                }else{
                                    array_push($json['isuperficie'],0);
                                }
                            }
                            //agregar isuperficie a cada  fila
                            foreach ($json['semcamculvar'] as $key => $value) {
                                $json['semcamculvar'][$key] .= '='.$json['isuperficie'][$key];
                            }
                            echo json_encode($json);
                            break;
                        case 'campanha':
                            $campanha = trim($_GET['cmp']);
                            Cuenta::getSemilleraSemilleristaCultivoVariedadByCampanha($campanha);   
                            $json['comunidad'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();                            
                            $json['slrsmrculvar'] = array();
                            $json['isolicitud']=array();
                            $json['isemilla']=array();
                            $json['estado']=array();
                            $json['categoria_producir'] = array();
                            while($obj = DBConnector::objeto()){
                                array_push($json['comunidad'],str_replace("'","$",$obj->comunidad));
                                array_push($json['semillera'],utf8_encode($obj->semillera));
                                array_push($json['semillerista'],$dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista)) );
                                array_push($json['cultivo'],$obj->cultivo);
                                array_push($json['variedad'],$dates->cambiar_acentoBySigno($obj->variedad));  
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['isemilla'],$obj->id_semilla);
                                array_push($json['estado'],$obj->estado_solicitud); 
                                array_push($json['categoria_producir'],$obj->categoria_producir);                                 
                                array_push($json['slrsmrculvar'],utf8_encode($obj->semillera).'='.$dates->cambiar_acentoBySigno(utf8_encode($obj->semillerista)) .'='.$obj->cultivo.'='.$dates->cambiar_acentoBySigno($obj->variedad).'='.$obj->estado_solicitud.'='.$obj->id_solicitud.'='.$obj->id_semilla);
                            }
                            $json['total'] = DBConnector::filas();
                            $json['isuperficie'] = array();
                            foreach ($json['isemilla'] as $id) {
                                $id_spf = Superficie::getIdSuperficieByIdSemilla($id);
                                if ($id_spf>0){
                                    array_push($json['isuperficie'],$id_spf);
                                }else{
                                    array_push($json['isuperficie'],0);
                                }
                            }
                            //agregar isuperficie a cada  fila
                            foreach ($json['slrsmrculvar'] as $key => $value) {
                                $json['slrsmrculvar'][$key] .= '='.$json['isuperficie'][$key];
                            }
                            echo json_encode($json);
                            break; 
                        case 'cultivo':
                            $cultivo = trim($_GET['cul']);
                            Cuenta::getSemilleraSemilleristaCampanhaVariedadByCultivo($cultivo);
                            if (DBConnector::filas()){
                                $json['comunidad'] = array();
                                $json['semillera'] = array();
                                $json['semillerista'] = array();
                                $json['campanha'] = array();
                                $json['cultivo'] = array();
                                $json['variedad'] = array();                            
                                $json['slrsmrcamvar'] = array();                            
                                $json['isolicitud'] = array();
                                $json['isemilla']=array();
                                $json['estado']=array();
                                $json['categoria_producir'] = array(); 
                                #var_dump(DBConnector::objeto());
                                while($obj = DBConnector::objeto()){
                                    array_push($json['comunidad'],str_replace("'","$",$obj->comunidad));
                                    array_push($json['semillera'],utf8_decode($obj->semillera));
                                    array_push($json['semillerista'],utf8_encode($obj->semillerista));
                                    array_push($json['campanha'],$obj->campanha);
                                    array_push($json['cultivo'],$obj->cultivo);
                                    array_push($json['variedad'],utf8_encode($obj->variedad));
                                    array_push($json['isolicitud'],$obj->id_solicitud);
                                    array_push($json['isemilla'],$obj->id_semilla);
                                    array_push($json['estado'],$obj->estado_solicitud);     
                                    array_push($json['categoria_producir'],$obj->categoria_producir);                             
                                    
                                }
                                foreach ($json['isolicitud'] as $key => $id) {
                                    array_push($json['slrsmrcamvar'],$json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['campanha'][$key].'='.$json['variedad'][$key].'='.$json['estado'][$key].'='.$id.'='.$json['isemilla'][$key]);
                                }
                                $json['isuperficie'] = array();
                                foreach ($json['isemilla'] as $id) {
                                    $id_spf = Superficie::getIdSuperficieByIdSemilla($id);
                                    if ($id_spf>0){
                                        array_push($json['isuperficie'],$id_spf);
                                    }else{
                                        array_push($json['isuperficie'],0);
                                    }
                                }
                                //agregar isuperficie a cada  fila
                                foreach ($json['slrsmrcamvar'] as $key => $value) {
                                    $json['slrsmrcamvar'][$key] .= '='.$json['isuperficie'][$key];
                                }
                                $json['total'] = count($json['isolicitud']);
                                
                            }else{
                                    $json['total'] = 0;
                            }
                            echo json_encode($json);
                            break;
                        case 'variedad':
                            $variedad = trim($_GET['variedad']);
                            Cuenta::getSemilleraSemilleristaCampanhaCultivoByVariedad($variedad);
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['campanha'] = array();
                            $json['cultivo'] = array();                            
                            $json['variedad'] = array();
                            $json['slrsmrcamcul'] = array();                            
                            $json['isolicitud'] = array();
                            $json['isemilla']=array();
                             $json['estado']=array();
                            while($obj = DBConnector::objeto()){
                                array_push($json['comunidad'],str_replace("'","$",$obj->comunidad));
                                array_push($json['semillera'],utf8_encode($obj->semillera));
                                array_push($json['semillerista'],utf8_encode($obj->semillerista));
                                array_push($json['campanha'],$obj->campanha);
                                array_push($json['cultivo'],$obj->cultivo);
                                array_push($json['variedad'],utf8_encode($obj->variedad));
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['isemilla'],$obj->id_semilla);
                                array_push($json['estado'],$obj->estado_solicitud);                                                                
                            }
                            $json['total'] = DBConnector::filas();
                            $json['isuperficie'] = array();
                            foreach ($json['isemilla'] as $id) {
                                $id_spf = Superficie::getIdSuperficieByIdSemilla($id);
                                if ($id_spf>0){
                                    array_push($json['isuperficie'],$id_spf);
                                }else{
                                    array_push($json['isuperficie'],0);
                                }
                            }
                            //crear valores unicos
                            foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                array_push($json['slrsmrcamcul'],$json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['campanha'][$key].'='.$json['cultivo'][$key].'='.$json['estado'][$key].'='.$id_solicitud.'='.$json['isemilla'][$key].'='.$json['variedad'][$key].'='.$json['comunidad'][$key].'='.$json['isuperficie'][$key]);
                            }
                            echo json_encode($json);
                            break;            
                    }
                break;    
                //llenado de datos segun autocompletado enviado
                case 'solicitud_semillera' :
                    $opcion = trim($_GET['opc']);
                    switch ($opcion) {
                    //opcion nro funciona [semilla-superficie]NO MODIFICAR
                    case 'nro' :
                        $nosolicitud = trim($_GET['solicitud']);
                        $estado = (isset($_GET['estado'])) ? ($_GET['estado']) : 0;
                        Solicitud::getSolicitudByNroSolicitudAndEstadoCertifica($nosolicitud, $estado);
                       
                        if (DBConnector::filas()) {
                            $json['isolicitud'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['cultivo'] = array();
                            $json['nosolicitud'] = array ();
                            $json['semista'] = array();
                            $json['dia'] = $json['mes'] = $json['anho'] = array();
                            $json['fecha'] = array();
                            $json['variedad'] = array();
                            
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['semillera'], ($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($json['comunidad'], (addslashes($obj -> comunidad)));
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['nosolicitud'],$obj->nro_solicitud);
                                array_push($json['fecha'],$obj->fecha);                                
                            }
                            foreach ($json['fecha'] as $key => $fecha_val) {
                                $temp = explode('-', $fecha_val);
                                array_push($json['dia'],$temp[2]);
                                array_push($json['mes'],$temp[1]);
                                array_push($json['anho'],$temp[0]);
                            }
                            foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                if ($estado != 0){
                                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);
                                    array_push($json['cultivo'],$cultivo);                                
                                    $variedad = Semilla::getVariedadByIdSolicitud($id_solicitud);
                                    array_push($json['variedad'],utf8_encode($variedad));
                                }else{
                                    array_push($json['cultivo'],'-');
                                    array_push($json['variedad'],'-'); 
                                }
                            }                            
                            foreach ($json['fecha'] as $key => $fecha) {
                                $isemista = $json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['comunidad'][$key].'='.$json['isolicitud'][$key];
                                $fecha_sol = explode("-", $fecha);
                                $isemista .= "=".$fecha_sol[2];
                                $isemista .= "=".$fecha_sol[1];
                                $isemista .= "=".$fecha_sol[0];   
                                                            
                                array_push($json['semista'],$isemista);
                            }
                            if ($estado != 0){
                                foreach ($json['semista'] as $key => $value) {
                                    $json['semista'][$key] = $value.'='.$json['cultivo'][$key].'='.$json['variedad'][$key]; 
                                }
                            }

                        } else {
                                $json['isolicitud'] = $jsonFiscal['semillera'] = '';
                                $json['semillerista'] = $jsonFiscal['comunidad'] = '';
                                $json['dia'] = $json['mes'] = $json['anho']=''; 
                                $json['cultivo'] = '';                            
                        }

                        $json['total'] = count($json['semillera']);
                        echo json_encode($json);
                        break;
                    //opcion inspeccion FUNCIONA
                    case 'nro_inspeccion' : //nro para inspeccion
                        $nosolicitud = trim($_GET['solicitud']);
                        Solicitud::getSolicitudByNroSolicitudAndEstadoForInspeccion($nosolicitud);
                        $json['total'] = DBConnector::filas();
                        if (DBConnector::filas()) {
                            $json['isolicitud'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['nosolicitud'] = array ();
                            $json['semista'] = array();
                            $json['cultivo'] = $json['variedad']= array();
                            $json['plantin'] = array();
                            while ($obj = DBConnector::objeto()) {                                
                                array_push($json['isolicitud'],$obj->id_solicitud);                                
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($json['comunidad'], str_replace("'","$",$obj->comunidad));
                                
                            }
                            foreach ($json['isolicitud'] as $key => $id_solicitud){
                                $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);                                
                                array_push($json['cultivo'],$cultivo);
                                $variedad = Semilla::getVariedadByIdSolicitud($id_solicitud);
                                array_push($json['variedad'],utf8_encode($variedad));
                                $plantin = Semilla::getPlantinByIdSolicitud($id_solicitud);
                                array_push($json['plantin'],$plantin);
                                $isemista = $json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['comunidad'][$key].'='.$id_solicitud.'='.$cultivo.'='.utf8_encode($variedad).'='.$plantin;
                                array_push($json['semista'],$isemista);
                            }
                        } else {
                                $json['isolicitud'] = $jsonFiscal['semillera'] = '';
                                $json['semillerista'] = $jsonFiscal['comunidad'] = '';
                                $json['semista'] = $json['cultivo'] = '';
                        }
                        
                        echo json_encode($json);
                        break;
                    //nro de hoja cosecha    
                    case 'nro_cosecha' : 
                        $nosolicitud = trim($_GET['solicitud']);
                        Solicitud::getSolicitudByNroSolicitudAndEstadoForCosecha($nosolicitud);
                        if (DBConnector::filas()) {
                            $json['isolicitud'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['nosolicitud'] = $nosolicitud;
                            $json['semista'] = array();
                            $json['cultivo'] = $json['variedad'] = array();
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['isolicitud'],$obj->id_solicitud);                                                             
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                            }
                            foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                $cultivo = utf8_encode(Semilla::getCultivoByIdSolicitudCertifica($id_solicitud));
                                array_push($json['cultivo'],$cultivo); 
                                $variedad = utf8_encode(Semilla::getVariedadByIdSolicitud($id_solicitud));
                                array_push($json['variedad'],$variedad); 
                                $isemista = $json['semillera'][$key].'='.$json['semillerista'][$key].'='.$json['comunidad'][$key].'='.$id_solicitud.'='.$cultivo.'='.$variedad;
                                array_push($json['semista'],$isemista);
                            }
                        } else {
                                $json['isolicitud'] = $jsonFiscal['semillera'] = '';
                                $json['semillerista'] = $jsonFiscal['comunidad'] = '';
                            
                        }
                        $json['total'] = count($json['isolicitud']);
                        echo json_encode($json);
                        break;
                    //nro de semilla producida    
                    case 'nro_sem_prod' : 
                        $nosolicitud = trim($_GET['solicitud']);
                        Solicitud::getSolicitudByNroSolicitudAndEstadoForSemProd($nosolicitud);
                        $json['isolicitud'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['nosolicitud'] = array ();
                            $json['semista'] = array();
                            $json['cultivo']=$json['variedad']=array();
                        if (DBConnector::filas()) {
                            #var_dump(DBConnector::objeto());                            
                            while ($obj = DBConnector::objeto()) {
                                #echo $obj ->semillerista,'=',utf8_decode($obj ->semillerista),'=',utf8_encode($obj ->semillerista);
                                $isemista = utf8_encode($obj->semillera).'='.utf8_encode($obj->semillerista).'='.utf8_encode(addslashes($obj->comunidad)).'='.$obj->id_solicitud;
                                array_push($json['semista'],$isemista);
                                array_push($json['isolicitud'],$obj->id_solicitud);
                                array_push($json['nosolicitud'],$obj->nro_solicitud);
                                array_push($json['semillera'], utf8_encode($obj -> semillera));
                                array_push($json['semillerista'], ($obj -> semillerista));
                                array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                            }
                            foreach ($json['isolicitud'] as $key => $id_solicitud){
                                $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);                                
                                array_push($json['cultivo'],$cultivo);
                                $variedad = Semilla::getVariedadByIdSolicitud($id_solicitud);
                                array_push($json['variedad'],utf8_encode($variedad));
                            }
                        } else {
                            $json['isolicitud'] = $jsonFiscal['semillera'] = '';
                            $json['semillerista'] = $jsonFiscal['comunidad'] = '';
                        }
                        $json['total'] = DBConnector::filas();
                        echo json_encode($json);
                        break;
                    //opcion semilla-superficie-inspeccion-hoja cosecha     
                    case 'semillera' :
                        $semillera = trim($_GET['semillera']);
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        $json['isolicitud'] = array();
                        $json['nosolista'] = array();
                        $json['nosolicitud'] = array();
                        $json['semillerista'] = array();
                        $json['comunidad'] = array();
                        $json['cultivo'] = $json['variedad']= array();
                        $json['plantin'] = array();    
                        Solicitud::getSolicitudBySemilleraAndEstadoCertifica($dates->modificar($semillera), $estado);
                        
                        
                        if (DBConnector::filas()) {                            
                            while ($obj = DBConnector::objeto()) {                                
                                array_push($json['isolicitud'], $obj -> id_solicitud);
                                array_push($json['nosolicitud'], $obj -> nro_solicitud);
                                array_push($json['semillerista'], utf8_encode(trim($obj -> semillerista)));
                                array_push($json['comunidad'], (str_replace("'","$",$obj->comunidad)));                              
                            }
                            if ($estado != 0){
                                foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                    $plantines = Semilla::getPlantinByIdSolicitud($id_solicitud);
                                    array_push($json['plantin'],$plantines); 
                                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);
                                    array_push($json['cultivo'],$cultivo);
                                    $variedad = Semilla::getVariedadByIdSolicitud($id_solicitud);
                                    array_push($json['variedad'],utf8_encode($variedad));
                                    array_push($json['nosolista'],$json['nosolicitud'][$key]. '=' . $json['semillerista'][$key].'='.$json['comunidad'][$key].'='.$id_solicitud.'='.$cultivo.'='.utf8_encode($variedad).'='.$json['plantin'][$key]);
                            }
                            }else{
                                foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = '-';
                                    array_push($json['cultivo'],$cultivo);
                                    $variedad = '-';
                                    array_push($json['variedad'],'-');
                                    array_push($json['nosolista'],$json['nosolicitud'][$key]. '=' . $json['semillerista'][$key].'='.$json['comunidad'][$key].'='.$id_solicitud.'='.$cultivo.'='.utf8_encode($variedad));
                                }
                            }
                            $json['total'] = count($json['isolicitud']);
                        } else {
                            $json['isolicitud'] = $json['nosolicitud'] = '';
                            $json['semillerista'] = '';
                            $json['comunidad'] = '';                            
                        }
                        echo json_encode($json);
                        break;
                    //opcion semilla producida
                    case 'semillera_producida' :
                        $semillera = $dates->modificar(trim($_GET['semillera']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        $json['isolicitud'] = array();
                        $json['nosolista'] = array();
                        $json['nosolicitud'] = array();
                        $json['semillerista'] = array();
                        $json['comunidad'] = array();
                        $json['cultivo']=$json['variedad']=array();
                        Solicitud::getSolicitudBySemilleraAndEstadoCertifica($semillera, $estado);
                        if (DBConnector::filas()) {                            
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['isolicitud'], $obj -> id_solicitud);
                                array_push($json['nosolicitud'], $obj -> nro_solicitud);
                                array_push($json['semillerista'],utf8_encode(trim($obj -> semillerista)));
                                array_push($json['comunidad'], ($obj -> comunidad));
                                array_push($json['nosolista'], $obj -> nro_solicitud . '=' . utf8_encode(trim($obj -> semillerista)).'='.utf8_encode(addslashes($obj->comunidad)).'='.$obj->id_solicitud);
                            }
                            foreach ($json['isolicitud'] as $key => $id_solicitud) {
                                $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);
                                array_push($json['cultivo'],$cultivo);
                                $variedad = Semilla::getVariedadByIdSolicitud($id_solicitud);
                                array_push($json['variedad'],utf8_encode($variedad));
                            }
                            $json['total'] = count($json['isolicitud']);
                        } else {
                                    $json['isolicitud'] = $json['nosolicitud'] = '';
                                    $json['semillerista'] = $json['comunidad'] = '';  
                                    $json['total']=0;                           
                        }
                        
                        echo json_encode($json);
                        break;    
                    //opcion semilla funciona
                    case 'productor_semilla' :
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();  
                        $jsonFiscal['solsemcomid'] = array();
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad'] = array();
                        Solicitud::getSolicitudBySemilleristaAndEstadoCertifica($semillerista, $estado);
                        $jsonFiscal['total'] = DBConnector::filas();

                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                                array_push($jsonFiscal['semillera'],($obj -> semillera));
                                array_push($jsonFiscal['comunidad'],(str_replace("'","$",$obj->comunidad)));
                                array_push($jsonFiscal['estado'],$obj->estado);
                                
                            } 
                            if($estado > 0){
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id_solicitud);
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = utf8_encode(Semilla::getVariedadByIdSolicitud($id_solicitud));
                                    array_push($jsonFiscal['variedad'],utf8_encode($variedad));
                                    $solsemcomid = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['isolicitud'][$key].'='.$jsonFiscal['estado'][$key].'='.$cultivo.'='.$variedad;
                                    array_push($jsonFiscal['solsemcomid'],$solsemcomid);
                                } 
                            }else{
                                foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                                    $cultivo = '-';
                                    array_push($jsonFiscal['cultivo'],$cultivo);
                                    $variedad = '-';
                                    array_push($jsonFiscal['variedad'],$variedad);
                                    $solsemcomid = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['isolicitud'][$key].'='.$jsonFiscal['estado'][$key].'='.$cultivo.'='.$variedad;
                                    array_push($jsonFiscal['solsemcomid'],$solsemcomid);
                                } 
                            }                                                      
                        } else {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                        }
                        
                        echo json_encode($jsonFiscal);
                        break;
                    //opcion superficie - hoja_cosecha - semilla producida
                    case 'productor' :
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        Solicitud::getSolicitudBySemilleristaAndEstadoCertificaV22($dates->search_tilde($semillerista), $estado,'certifica');                       
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();
                        $jsonFiscal['semilla'] = $jsonFiscal['cultivo'] = array();
                        $jsonFiscal['superficie'] = $jsonFiscal['variedad'] = array();
                        $jsonFiscal['nosemcomestsemsup'] = array(); 
                        $jsonFiscal['total'] = DBConnector::filas();
                        while($obj = DBConnector::objeto()){
                            array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                            array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                            array_push($jsonFiscal['semillera'],utf8_encode($obj -> semillera));
                            array_push($jsonFiscal['comunidad'],str_replace("'","$",$obj->comunidad));
                            array_push($jsonFiscal['estado'],$obj->estado);                            
                            array_push($jsonFiscal['cultivo'],$obj->cultivo);
                            array_push($jsonFiscal['variedad'],utf8_encode($obj->variedad));
                        }
                        foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                            $semilla = Tiene::getIdSemilla($id_solicitud);
                            array_push($jsonFiscal['semilla'],$semilla);
                            $superficie = Superficie::getIdSuperficieByIdSemilla(Tiene::getIdSemilla($id_solicitud));
                            array_push($jsonFiscal['superficie'],$superficie);
                        }
                        //unificacion de valores
                        foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud) {
                            array_push($jsonFiscal['nosemcomestsemsup'], $jsonFiscal['nosolicitud'][$key] . '=' . $jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$jsonFiscal['estado'][$key].'='.Tiene::getIdSemilla($id_solicitud).'='.$id_solicitud);
                        }
                        #no hay resultado devolver vacio
                        if (!DBConnector::filas()) {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                            $jsonFiscal[''] = $jsonFiscal[''] = '';  
                        }

                            foreach ($jsonFiscal['nosemcomestsemsup'] as $key => $value) {
                                $jsonFiscal['nosemcomestsemsup'][$key] = $value.'='.$jsonFiscal['cultivo'][$key].'='.$jsonFiscal['variedad'][$key];
                            }

                        echo json_encode($jsonFiscal);
                        break;
                    case 'productor_inspeccion':
                        $semillerista = trim($dates -> modificar($_GET['semillerista']));
                        $estado = (isset($_GET['estado'])) ? intval($_GET['estado']) : 0;
                        
                        $jsonFiscal['isolicitud'] = array();
                        $jsonFiscal['nosolicitud'] = array();
                        $jsonFiscal['semillera'] = array();
                        $jsonFiscal['comunidad'] = array();
                        $jsonFiscal['estado'] = array();  
                        $jsonFiscal['cultivo'] = $jsonFiscal['variedad'] = array();
                        $jsonFiscal['plantin'] = array();
                        $jsonFiscal['solsemcomid'] = array();
                        
                        Solicitud::getSolicitudBySemilleristaAndEstadoCertificaForInspeccion($semillerista, $estado,'certifica');
                        if (DBConnector::filas()) {
                            while ($obj = DBConnector::objeto()) {
                                array_push($jsonFiscal['isolicitud'],$obj -> id_solicitud);
                                array_push($jsonFiscal['nosolicitud'],$obj -> nro_solicitud);
                                array_push($jsonFiscal['semillera'],utf8_encode($obj -> semillera));
                                array_push($jsonFiscal['comunidad'],str_replace("'","$",$obj->comunidad));
                                array_push($jsonFiscal['estado'],$obj->estado);
                                if ($estado != 4){
                                    array_push($jsonFiscal['cultivo'],$obj->cultivo);
                                    array_push($jsonFiscal['variedad'],utf8_encode($obj->variedad));
                                    array_push($jsonFiscal['plantin'],$obj->plantin);
                                }
                            }
                            foreach ($jsonFiscal['isolicitud'] as $key => $id_solicitud){
                                if ($estado == 4){
                                    $cultivo = utf8_encode(Semilla::getCultivoByIdSolicitudCertifica($id_solicitud));
                                    array_push($jsonFiscal['cultivo'],$cultivo); 
                                    $variedad = utf8_encode(Semilla::getVariedadByIdSolicitud($id_solicitud));
                                    array_push($jsonFiscal['variedad'],$variedad);                                     
                                }    
                                if ($estado != 4){
                                    $isemista = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$id_solicitud.'='.$jsonFiscal['cultivo'][$key].'='.$jsonFiscal['variedad'][$key].'='.$jsonFiscal['plantin'][$key];    
                                }else{
                                    
                                    $isemista = $jsonFiscal['nosolicitud'][$key].'='.$jsonFiscal['semillera'][$key].'='.$jsonFiscal['comunidad'][$key].'='.$id_solicitud.'='.$jsonFiscal['cultivo'][$key].'='.$jsonFiscal['variedad'][$key].'='.'E';
                                }                         
                                
                                array_push($jsonFiscal['solsemcomid'],$isemista);
                            }
                            $jsonFiscal['total'] = count($jsonFiscal['isolicitud']);                                                                                
                        } else {
                            $jsonFiscal['isolicitud'] = $jsonFiscal['nosolicitud'] = '';
                            $jsonFiscal['semillera'] = $jsonFiscal['comunidad'] = '';
                            $jsonFiscal['estado']  = $jsonFiscal['semilla'] = $jsonFiscal['superficie'] = '';
                            $jsonFiscal['total'] = 0; 
                        }
                        
                        echo json_encode($jsonFiscal);
                        break;
                    case 'no_era_ista' :
                        $nosolicitud = trim($_GET['nro']);
                        $semillera = $dates -> modificar(trim($_GET['slr']));
                        $semillerista = $dates -> modificar(trim($_GET['srt']));
                        Solicitud::getComunidadByNroSemilleraSemillerista($nosolicitud, $semillera, $semillerista);
                        $obj = DBConnector::objeto();

                        $json['comunidad'] = $obj -> comunidad;

                        echo json_encode($json);
                        break;
                }
                break;
                //
                case 'nroSolicitud' :                    
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $estado = intval(trim($_GET['opc']));
                    $tabla = isset($_GET['tbl']) ? $_GET['tbl'] : '';
                    $sistema = intval($_GET['area']);

                    $semillera = isset($_GET['semillera']) ? $dates->modificar($_GET['semillera']) : '';
                    $semillera = Semillera::getIdSemillera(str_replace('*', ' ', $semillera),$sistema);
                    switch ($estado) {
                        case ($estado==2 || $estado==3 || $estado==4) :
                            // echo 'caso2-3-4';
                            $solicitudes -> getNroSolicitud($nombre, $apellido, $tabla, Semillera::getNombreSemillera($semillera));
                            #echo DBConnector::mensaje();
                            $obj = DBConnector::objeto();
                            #var_dump($obj);
                            $solicitudID = $obj -> isolicitud;
                            #var_dump(empty($solicitudID));
                            if (empty($solicitudID)) {

                                $tabla = 'superficieI';
                                $solicitudes -> getNroSolicitud($nombre, $apellido, $tabla, $semillera);
                                #echo DBConnector::mensaje();
                                while ($obj = DBConnector::objeto()) {
                                    $solicitudID = $obj -> isolicitud;
                                }
                                $json['iproductor'] = $solicitudID;
                            } else
                                $json['iproductor'] = $solicitudID;
                            $superficieID = $superficie -> getSuperficieID($nombre, $apellido, $solicitudID);
                            #echo DBConnector::mensaje();
                            $json['isuperficie'] = $superficieID;
                            echo json_encode($json);
                            break;
                        default :
                            $solicitudes -> getNroSolicitud($nombre, $apellido, $tabla, Semillera::getNombreSemillera($semillera));
                            //echo DBConnector::mensaje();
                            while ($fila = DBConnector::objeto()) {
                                $json['iproductor'] = $fila -> isolicitud;
                            }
                            $json['isemillerista'] = Semillerista::getIdSemilleristaByNombre($nombre);
                            echo json_encode($json);
                            break;
                    }
                    break;
                case 'parcelas' ://devuelve la comunidad solicitada
                    $id = intval($_GET['id']);
                    $solicitudes -> getComunidadById($id);
                    //$comunidades = array();
                    $fila = DBConnector::objeto();
                    //var_dump($fila);
                    $resultado['comunidades'] = $fila -> comunidad;

                    echo json_encode($resultado);
                    break;                
                case 'superficie_isemilla_iproductor' ://id de la superficie por el nro de campo
                    $isemilla = $_GET['semilla'];
                    $isemillerista = $_GET['productor'];

                    $resultado['isuperficie'] = Superficie::getSuperficieByIdSemilla_IdSemillerista($isemilla, $isemillerista);
                    #echo DBConnector::mensaje();exit;
                    echo json_encode($resultado);
                    break;
                #paginar plantines
                case 'plantines':
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;                    
                    $edit = ($area==10)?1110:1100;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    $blocks = $plantines_obj ->getPlantinesByBlock($area);
                    
                    include '../vista/certificacion/datos/plantines.php';
                    break;
                #paginar acondicionamiento
                case 'acondicionamiento':
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;                    
                    $edit = ($area==10)?1110:1100;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //realizar consulta sql
                    $blocks = $acondicionamiento_obj->getAcondicionamientoByBlock($area, $limite, $nroRegistros);
                    include '../vista/certificacion/datos/acondicionamiento.php';
                    break;
                #paginar solicitudes version 2;
                case 'solicitud' :
                    $area = $_GET['area'];
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    $edit = ($area==10)?1110:1100;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //total de solicitudes
                    $solicitudes -> getSolicitudes($area);
                    //lote segun el nro de registros a mostrar 
                    $blocks = $solicitudes -> getSolicitudesbyBlock($area, $limite, $nroRegistros);
                    include '../vista/certificacion/datos/solicitud.php';
                    break;                    
                #paginar semilla
                case 'semilla' :
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //limite de paginacion de resultados
                    $semilla -> getSemillaSolicitud($area);
                    //realizar consulta sql
                    $blocks = $semilla -> getSemilleraSolicitudByBlock($area, $limite, $nroRegistros);   
                    #echo $blocks;                 
                    include '../vista/certificacion/datos/semilla.php';
                    break;
  
                #paginar superficies
                case 'superficie' :
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //limite de paginacion de resultados
                    $superficie -> getSuperficieSolicitud($area);
                    $blocks = $superficie -> getSuperficieSolicitudByBlock($area, $limite, $nroRegistros);
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;

                    include '../vista/certificacion/datos/superficie.php';
                    break;
                #paginar inspecciones
                case 'vinspecciones' :
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //limite de paginacion de resultados
                    $inspecciones -> getInspeccionSolicitud($area);
                    $blocks = $inspecciones -> getInspeccionSolicitudByBlock($area, $limite, $nroRegistros);
                    //bloque de inspecciones a mostrar
                    include '../vista/certificacion/datos/inspeccion.php';
                    break;
                #paginar inspecciones de plantines
                case 'inspecciones-plantines' :
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //limite de paginacion de resultados
                    $inspecciones -> getInspeccionPlantines($area);
                    $blocks = $inspecciones -> getInspeccionPlantinesByBlock($area, $limite, $nroRegistros);
                    #echo $blocks;
                    //bloque de inspecciones a plantines 
                    include '../vista/certificacion/datos/inspeccionPlantines.php';
                    break;    
                #paginar hoja de cosecha    
                case 'cosecha' :
                    $area = intval($_REQUEST['area']);
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    //limite de paginacion de resultados
                    $cosechas -> getCosechaSolicitud($area);
                    $blocks = $cosechas -> getCosechaSolicitudByBlock($area, $limite, $nroRegistros);
                    #echo $blocks;
                    //bloque de inspecciones a mostrar
                    include '../vista/certificacion/datos/cosecha.php';
                    break;     
                #paginar semilla producida               
                case 'produccion' :
                    $area = $_REQUEST['area'];
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    $producciones -> getSemillaProducida();
                    #echo DBConnector::mensaje();exit;
                    include '../vista/certificacion/datos/semilla_producida.php';
                    break;
                #paginar cuentas    
                case 'cuentas' :
                    $area = $_REQUEST['area'];
                    $page = isset($_GET['page'])?intval($_GET['page']):1;
                    #$edit = ($area==10)?1110:1100;
                    $edit = isset($_REQUEST['edt']) ? $_REQUEST['edt'] : 9999;
                    $limite = ($nroRegistros*$page) - $nroRegistros;
                    
                    $cuenta -> getIdSemillaForCuenta();
                    $isemillaCuenta = array();
                    while($obj = DBConnector::objeto()){
                        array_push($isemillaCuenta,$obj->id_semilla);
                    }
                    $objCuenta['responsable'] =$objCuenta['semillera']=$objCuenta['semillerista']= array();
                    $objCuenta['cultivo'] = $objCuenta['variedad']= $objCuenta['campanha']= array();
                    $objCuenta['categoria_aprobada'] = $objCuenta['superficie_aprobada']= $objCuenta['superficie_total']= array();
                    $objCuenta['etiquetas'] =$objCuenta['inscripcion']= array();
                    $objCuenta['inspeccion'] = $objCuenta['analisis'] = array();
                    $objCuenta['total'] = $objCuenta['saldo'] = array();
                    $objCuenta['isemilla']=$objCuenta['fullcuenta']=array();
                    foreach ($isemillaCuenta as $key => $value) {
                        $isolicitud = Tiene::getIdSolicitud($value);
                        $cuenta->getCuentaByIdSolicitud($isolicitud);
                        $obj = DBConnector::objeto();
                        array_push($objCuenta['isemilla'],$obj->id_semilla);
                        array_push($objCuenta['responsable'],$obj->responsable);
                        array_push($objCuenta['semillera'],$obj->semillera);
                        array_push($objCuenta['semillerista'],$obj->semillerista);
                        array_push($objCuenta['campanha'],$obj->campanha);
                        array_push($objCuenta['cultivo'],$obj->cultivo);
                        array_push($objCuenta['variedad'],$obj->variedad);                        
                        array_push($objCuenta['categoria_aprobada'],$obj->categoria_aprobada);
                        array_push($objCuenta['superficie_aprobada'],$obj->superficie_aprobada);
                        array_push($objCuenta['superficie_total'],$obj->superficie_total);
                        array_push($objCuenta['etiquetas'],$obj->bolsa_etiqueta);
                        array_push($objCuenta['inscripcion'],$obj->inscripcion);
                        array_push($objCuenta['inspeccion'],$obj->inspeccion);
                        array_push($objCuenta['analisis'] ,$obj->analisis_etiqueta);
                        array_push($objCuenta['total'],$obj->total);
                        array_push($objCuenta['saldo'],$obj->saldo);                        
                    }
                    foreach ($objCuenta['isemilla'] as $key => $valor) {
                    	$temp = $objCuenta['isemilla'][$key].'='.$objCuenta['semillera'][$key].'='.$objCuenta['semillerista'][$key].'='.$objCuenta['cultivo'][$key].'='.$objCuenta['variedad'][$key].'='.$objCuenta['saldo'][$key];
                        array_push($objCuenta['fullcuenta'],$temp);
                    }
                    include '../vista/certificacion/datos/cuenta.php';
                    break;             
                #paginar lista de muestras  listas para resultado
                case 'muestras':
                    Laboratorio::getListMuestrasCertificacionv2();
                    include '../vista/certificacion/datos/muestras.html.php';    
                #paginar resultados de laboratorio       
                case 'laboratorio' :
                    $area = ($_REQUEST['area'] == 1) ? 'certificacion' : (($_REQUEST['area'] == 2) ? 'fiscalizacion' : 'administracion');
                    $nro = $_REQUEST['area'];
                    Usuario::getAreaUsuario($area);
                    $laboratorio -> getLaboratorio();
                    include '../vista/laboratorio/datos/laboratorio.php';
                    break;               
            }
            break;
        case 'eliminar' :
            $id = intval($_REQUEST['id']);
            switch ($pagina) {
                case 'solicitud' :
                    $solicitudes -> deleteSolicitud($id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        DBConnector::mensaje();
                    break;

                case 'semilla' :
                    $semilla -> deleteSemilla($id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();
                    break;

                case 'superficie' :
                    $superficie -> deleteSuperficie($id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();
                    break;
                #***************************
                case 'inspeccion' :
                    $inspecciones -> deleteInspeccion($id);
                    if (DBConnector::filas()) {
                        echo 'OK';
                    } else {
                        echo 'error';
                    }
                    break;
                //implementar desde aqui
                case 'cosecha' :
                    $cosechas -> deleteHojaCosecha($id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();

                    break;
                case 'semillaP' :
                    $producciones -> deleteSemillaProd($id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();
            case 'cuenta' :
            $cuenta -> deleteCuenta($id);
            if (DBConnector::filas())
                echo 'OK';
            else
                echo 'error';

            break;
}
            break;
                
        case 'buscar' ://opciones de busqueda
            switch ($pagina) {
                case 'buscar' :
                    $semillera = (strcasecmp($_GET['semillera'], 'todas') == 0) ? '*' : $_GET['semillera'];
                    $semillerista = (strcasecmp($_GET['semillista'], 'todos') == 0) ? '*' : $_GET['semillerista'];
                    $cultivo = (strcasecmp($_GET['cultivo'], 'todos') == 0) ? '*' : Cultivo::getIdCultivo($_GET['cultivo']);

                    include '../vista/certificacion/datos/semilla.php';
                    break;
                case 'campana' ://carga las campañas de de un productor                    
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $semillera = $dates->modificar($_GET['semillera']);
                    $sistema = $_GET['cultivo'];
                    $semilla -> getCampaniaProductor($nombre, $apellido, $semillera, $sistema);
                    #echo DBConnector::mensaje();
                    $json = array();
                    if (DBConnector::filas()) {
                        while ($fila = DBConnector::objeto()) {
                            if (!$fila -> campanha)
                                array_push($json, '');
                            else {
                                array_push($json, $fila -> campanha);
                            }
                        }
                    } else
                        array_push($json, 'No tiene campaña');
                    echo json_encode($json);
                    break;
                case 'categGener' :
                    $nro = strip_tags($_GET['nro']);
                    Semilla::getCategoriaGeneracionByCampo($nro);
                    
                    $row=DBConnector::objeto();
                    $json['generacion']=$row->generacion;
                    $json['categoria']=$categoria->getNombreCategoriaByIdGeneracion($row->categoriaS).'-'.$row->inicial;
                    $json['semillaNeta']=HojaCosecha::getSemillaNetaByNroCampo($nro);
                    $json['igeneracion'] = Semilla::getCategoriaProducirByNroCampo($nro);
                    $generacion = Generacion::getInicialGeneracion($json['igeneracion']);
                    $categoria = $categoria->getNombreCategoriaByIdGeneracion($json['igeneracion']);
                    $json['categoriaObtenida']= $categoria .'-'. $generacion;
                    echo json_encode($json);
                    break;                
                case 'categoria' ://categoria segun generacion
                    $igeneracion = $_GET['igeneracion'];
                    $generacion -> getGeneracionById($igeneracion);
                    $row = DBConnector::objeto();
                    if ($row -> generacion == 'Primera') {
                        $union = '1';
                    } elseif ($row -> generacion == 'Segunda') {
                        $union = '2';
                    } elseif ($row -> generacion == 'Tercera') {
                        $union = '3';
                    } else {
                        $union = 'B';
                    }
                    $json['categoria'] = $row -> categoria . '-' . $union;

                    echo json_encode($json);
                    break;                
                case 'campos' ://muestra los cultivos q pasaron por el laboratorio y cultivos de papa para semilla producida
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $semillera = $dates -> modificar($_GET['semillera']);
                    $sistema = intval($_GET['area']);
                    
                    $datosCosecha['comunidad'] = Solicitud::getComunidadByNombreApellidoSemillera($nombre,$apellido,$semillera,$sistema);
                    
                    $cosechas -> cultivoPapa($nombre, $apellido);
                    if (!DBConnector::nroError() && DBConnector::filas()) {
                        $papaArray = array();
                        while ($fila = DBConnector::objeto()) {
                            array_push($papaArray, $fila -> nro_campo);
                        }
                        $datosCosecha['papa'] = $papaArray;
                    } else {
                        $datosCosecha['papa'] = '';
                    }
                    $nro_campo = Semilla::getNroCampoByIdSemilla(Tiene::getIdSemilla(Solicitud::getIdSolicitudProductorByEstado($nombre, $apellido, 9)));
                    $laboratorio -> nroCampos2($nombre,$apellido);
                    #echo DBConnector::mensaje();
                    if (!DBConnector::nroError()) {
                        $otrosArray = array();
                        while ($fila = DBConnector::objeto()) {
                            array_push($otrosArray, $fila -> nro_campo);
                        }
                        $datosCosecha['otros'] = $otrosArray;
                    } else {
                        $datosCosecha['otros'] = '';
                    }

                    echo json_encode($datosCosecha);
                    break;                
                case 'counter' ://indice contador de los campos
                    $id = $_GET['isolicitud'];
                    $solicitudes -> getNroParcelas($id);
                    while ($fila = DBConnector::objeto()) {
                        $resultado['indice'] = $fila -> count_parcelas;
                        $resultado['noparcelas'] = $fila -> nro_parcelas;
                    }
                    echo json_encode($resultado);
                    break;                
                case 'cuentas'://autocompletado para la opcion cuenta
                    $buscar = trim($_GET['term']);
                    $json = array();
                            
                    $respuesta = (DBConnector::filas())?'1':'0';
                    $opciones_busqueda = array('semillera','semillerista','campana','cultivo','variedad');
                    $contar_opciones = 1;
                    while ($contar_opciones <= count($opciones_busqueda)) {
                        #buscar semillera
                        $solicitudes -> getSemillerasCuenta('semillera', $buscar, 'certifica');
                        if(DBConnector::filas()){
                            $respuesta = '1';
                            break;
                        }
                        #semillerista                                
                        $solicitudes -> getSemillerasCuenta('semillerista', $buscar, 'certifica');
                        if (DBConnector::filas()){
                            $respuesta = '2';
                            break;
                        }
                        #campaña
                        $solicitudes -> getSemillerasCuenta('campanha', $buscar, 'certifica');
                        if (DBConnector::filas()){
                            $respuesta = '3';
                            break;
                        }                                
                        #cultivo
                        $solicitudes -> getSemillerasCuenta('cultivo', $buscar, 'certifica');
                        if (DBConnector::filas()){
                            $respuesta = '4';
                            break;    
                        }                            
                        #variedad
                        $solicitudes -> getSemillerasCuenta('variedad', $buscar, 'certifica');
                        if (DBConnector::filas()){
                            $respuesta = '5';
                            break;
                        }
                        $contar_opciones++;
                    }
                    switch ($respuesta) {
                        case '1':
                            while ($row = DBConnector::objeto())
                                array_push($json, utf8_encode($row -> semilleras));
                        break;                                
                        case '2':
                            while ($row = DBConnector::objeto()) 
                                array_push($json, utf8_encode($row -> semillerista));
                            break;
                        case '3':
                            while ($row = DBConnector::objeto())
                                array_push($json, utf8_encode($row -> campanha));
                            break;
                        case '4':
                            while ($row = DBConnector::objeto()) 
                                array_push($json, utf8_encode($row -> cultivo));
                            break;
                        case '5':
                            while ($row = DBConnector::objeto())
                                array_push($json, utf8_encode($row -> variedad));
                            break;
                    }                            
                    echo json_encode($json);
                break;
                
                case 'cultivo' ://cultivos segun el id de solicitud
                    $isolicitud = empty($_GET['isolicitud']) ? '-' : trim($_GET['isolicitud']);
                    $semilla -> getAllCultivos($isolicitud);
                    #var_dump(DBConnector::objeto());
                    $row = DBConnector::objeto();
                    $json['cultivo'] = $row -> cultivo;
                    $json['comunidad'] = $row -> comunidad;
                    echo json_encode($json);
                    break;               
                case 'cultivos' : //cultivo de un semillerista
                    $semillera = empty($_GET['semillera']) ? '%' : $dates->modificar(trim($_GET['semillera']));
                    $apellido = (isset($_GET['apellido']) && !empty($_GET['apellido'])) ? $dates -> modificar($_GET['apellido']) : '';
                    $nombre = (isset($_GET['nombre']) && !empty($_GET['nombre'])) ? $dates -> modificar($_GET['nombre']) : '';
                    $campania = (isset($_GET['cultivo']) || !empty($_GET['cultivo'])) ? $_GET['cultivo'] : '';
                    #$id = !empty($_GET['id']) ? intval($_GET['id']) : '';

                    $semilla -> getCultivosProductor($nombre, $apellido, $campania, $semillera);

                    $json = array();
                    #echo DBConnector::mensaje();
                    while ($fila = DBConnector::objeto()) {
                        array_push($json, $fila -> cultivo);
                    }
                    echo json_encode($json);
                    break;                
                case 'cuota':
                    $semillera = Funciones::search_tilde( trim($_GET['slr']) );
                    $semillerista = Funciones::search_tilde( trim($_GET['smr']) );
                    $cultivo = Funciones::search_tilde( trim($_GET['cul']) );
                    $variedad = Funciones::search_tilde( trim($_GET['variedad']) );
                    
                    $json['saldo'] = $cuenta -> getCuota($semillera,$semillerista,$cultivo,$variedad);
                    echo json_encode($json);
                    break; 
                case 'cuotas':
                    $id = intval($_GET['id']);
                    
                    $cuenta -> getCuotasByIdSemilla($id);
                    $json['fecha'] = $json['cuota'] = $json['saldo'] = array();
                    while ($obj = DBConnector::objeto()){
                        array_push($json['fecha'],$obj->fecha);
                        array_push($json['cuota'],$obj->cuota);
                        $monto_total = $obj->total;
                        array_push($json['saldo'],$monto_total-array_sum($json['cuota']));
                    }
                    $json['total'] = count($json['fecha']);
                    echo json_encode($json);
                    break; 
                //devuelve el ultimo numero de cupon  emitido
                case 'cupones':
                    $ultimo = $cosechas->getCupones();
                    $json['cupones'] = $ultimo+1;
                    
                    echo json_encode($json);
                break;                    
                case 'detalle_produccion'://lista de cultivos para detalle de produccion y costos de certificacion
                    Semilla::getListCultivos();
                break;                 
                case 'estado'://devuelve el estado de solicitud de un productor
                    $semillera = trim($_GET['semillera']);
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);  
                    $estado = Solicitud::getEstadoByNombreApellidoSemillera($nombre, $apellido, $semillera);
                    
                    $json['estado'] = $estado;
                    
                    echo json_encode($json);                 
                break;                
                case 'filtro' ://filtro de solicitud
                    $opc = trim($_GET['opc']);
                    $search = trim($_GET['search']);
                    $columnas = array();
                    switch ($opc) {
                        case 'solicitud' :
                            $json['id'] = array();
                            $json['nosolicitud'] = array();
                            $json['sistema'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['departamento'] = array();
                            $json['provincia'] = array();
                            $json['municipio'] = array();
                            $json['comunidad'] = array();
                            $json['fecha'] = array();
    
                            if (!empty($search)) {
                                Solicitud::getSolicitudesCertificaFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['id'],$obj->id_solicitud);
                                    array_push($json['nosolicitud'], $obj -> nro_solicitud);
                                    array_push($json['sistema'], $obj -> sistema);
                                    array_push($json['semillera'], utf8_encode($obj -> semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['departamento'], $obj -> departamento);
                                    array_push($json['provincia'], $obj -> provincia);
                                    array_push($json['municipio'], $obj -> municipio);
                                    array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                                    array_push($json['fecha'], $dates -> cambiar_formato_fecha($obj -> fecha));
                                }
                            }
                            $json['total'] = count($json['sistema']);
                            break;
                        case 'semilla' :
                            $json['id']=array();
                            $json['semillera']=array();
                            $json['semillerista'] = array();
                            $json['comunidad'] = array();
                            $json['nro_campo']=array();
                            $json['campanha']=array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();
                            $json['categoria_sembrada']=array();
                            $json['categoria_producir']=array();
                            $json['cantidad_semilla']=array();
                            $json['nro_lote']=array();
                            $json['fecha']=array();
                            $json['plantas_hectarea']=array();
                            $json['cultivo_anterior']=array();
                            $json['parcela']=array();
                            $json['cultivo_anterior_tmp']=array();
    
                                Semilla::getSemillaCertificaFilter($search);
                                #var_dump(DBConnector::objeto());
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['id'],$obj->id_solicitud);
                                    array_push($json['semillera'],utf8_encode($obj->semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['comunidad'], utf8_encode($obj -> comunidad));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['campanha'], $obj -> campanha);
                                    array_push($json['cultivo'], utf8_encode($obj -> cultivo));
                                    array_push($json['variedad'], utf8_encode($obj -> variedad));
                                    array_push($json['categoria_sembrada'], $obj -> categoria_sembrada);
                                    array_push($json['categoria_producir'], utf8_encode($obj -> categoria_producir));
                                    array_push($json['cantidad_semilla'], $obj -> cantidad_semilla);                                
                                    array_push($json['nro_lote'], $obj -> nro_lote);
                                    array_push($json['fecha'], utf8_encode($obj -> fecha_siembra));
                                    array_push($json['plantas_hectarea'], utf8_encode($obj -> plantas_hectarea));
                                    array_push($json['cultivo_anterior_tmp'],utf8_encode($obj -> cultivo_anterior));
                                    array_push($json['parcela'], $obj -> parcela);
                                }

                            $json['total'] = count($json['semillerista']);
                            break;
                        
                        case 'superficie':
                            $json['semillera']=array();
                            $json['semillerista']=array();
                            $json['nro_campo']=array();
                            $json['inscrita']=array();
                            $json['rechazada']=array();
                            $json['retirada']=array();
                            $json['aprobada']=array();
                            $json['isuperficie'] = array();
                            $json['comunidad']=array();
                            if(!empty($search)){
                                Superficie::getSuperficieCertificaFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'],utf8_encode($obj->comunidad));
                                    array_push($json['isuperficie'],$obj->id_superficie);
                                    array_push($json['semillera'],utf8_encode($obj->semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['inscrita'], $obj -> retirada);
                                    array_push($json['rechazada'], $obj -> rechazada);
                                    array_push($json['retirada'],$obj->retirada);
                                    array_push($json['aprobada'],$obj->aprobada);
                                }
                            }
                            $json['total'] = count($json['semillerista']);
                            break;
                        case 'inspeccion':
                            $json['semillera']=array();
                            $json['semillerista']=array();
                            $json['nro_campo']=array();
                            $json['primera']=array();
                            $json['observacion_1']=array();
                            $json['segunda']=array();
                            $json['observacion_2']=array();
                            $json['tercera']=array();
                            $json['observacion_3']=array();
                            if(!empty($search)){
                                Inspeccion::getInspeccionesFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'], utf8_encode($obj -> semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['primera'], $dates->cambiar_formato_fecha($obj -> primera));
                                    array_push($json['observacion_1'], $obj -> observacion_1);
                                    array_push($json['segunda'],$dates->cambiar_formato_fecha($obj->segunda));
                                    array_push($json['observacion_2'],$obj->observacion_2);
                                    array_push($json['tercera'],$dates->cambiar_formato_fecha($obj->tercera));
                                    array_push($json['observacion_3'],$obj->observacion_3);
                                }
                            }
                            $json['total'] = count($json['semillerista']);
                            break;    
                        case 'muestras':
                            $json['nocampo'] = array();
                            $json['fecha_recepcion'] = array();
                            $json['semillerista'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();
                            if (!empty($search)) {
                            $laboratorio->getMuestras($search);
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['nocampo'],$obj->nro_campo);
                                array_push($json['fecha_recepcion'],$dates->cambiar_formato_fecha($obj->fecha_recepcion));
                                array_push($json['semillerista'],utf8_encode($obj->semillerista));
                                array_push($json['cultivo'],$obj->cultivo);
                                array_push($json['variedad'],$obj->variedad); 
                            } 
                            }else{
                                $laboratorio->getMuestras();
                            while ($obj = DBConnector::objeto()) {
                                array_push($json['nocampo'],$obj->nro_campo);
                                array_push($json['fecha_recepcion'],$dates->cambiar_formato_fecha($obj->fecha_recepcion));
                                array_push($json['semillerista'],utf8_encode($obj->semillerista));
                                array_push($json['cultivo'],$obj->cultivo);
                                array_push($json['variedad'],$obj->variedad); 
                            } 
                            }
                            $json['total'] = count($json['semillerista']);
                        break;
                        case 'cosecha' :
                            $json['semillera']=array();
                            $json['semillerista'] = array();
                            $json['nro_campo']=array();
                            $json['fecha_emision']=array();
                            $json['categoria_campo']=array();
                            $json['rendimiento_estimado']=array();
                            $json['parcela'] = array();
                            $json['rendimiento_campo'] = array();
                            $json['nro_cupones']=array();
                            $json['rango_cupones']=array();
                            $json['planta_acondicionadora']=array();
                            if (!empty($search)) {
                                HojaCosecha::getHojaCosechaCertificaFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'],utf8_encode($obj->semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['fecha_emision'], $dates->cambiar_formato_fecha($obj -> fecha_emision));
                                    array_push($json['categoria_campo'], $obj -> categoria_campo);
                                    array_push($json['rendimiento_estimado'],$obj->rendimiento);
                                    #array_push($json['parcela'], $obj -> parcela);
                                    array_push($json['rendimiento_campo'], $obj -> rendimiento_campo);
                                    array_push($json['nro_cupones'], $obj -> cupones);
                                    array_push($json['rango_cupones'], $obj->rango);
                                    array_push($json['planta_acondicionadora'],utf8_encode($obj -> planta_acondicionadora));
                                }
                            }
                            $json['total'] = count($json['semillerista']);
                            break;
                        case 'semillaP' :
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['nro_campo'] = array();
                            $json['categoria_sembrada'] = array();
                            $json['generacion'] = array();
                            $json['semilla_neta'] = array();
                            $json['categoria_obtenida']=array();
                            $json['nro_etiqueta']=array();
                            $json['rango']=array();
                            
                            if (!empty($search)) {
                                SemillaProd::getSemillaProdCertificaFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'], utf8_encode($obj -> semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['categoria_sembrada'], $obj -> categoria_sembrada);
                                    array_push($json['generacion'], $obj -> generacion);
                                    array_push($json['semilla_neta'], $obj -> semilla_neta);
                                    array_push($json['categoria_obtenida'], $obj -> categoria_obtenida);
                                    array_push($json['nro_etiqueta'], $obj -> nro_etiqueta);
                                    array_push($json['rango'], $obj -> rango);
                                }
                            }else{
                                SemillaProd::getSemillaProdCertificaFilter();
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'], utf8_encode($obj -> semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['nro_campo'], $obj -> nro_campo);
                                    array_push($json['categoria_sembrada'], $obj -> categoria_sembrada);
                                    array_push($json['generacion'], $obj -> generacion);
                                    array_push($json['semilla_neta'], $obj -> semilla_neta);
                                    array_push($json['categoria_obtenida'], $obj -> categoria_obtenida);
                                    array_push($json['nro_etiqueta'], $obj -> nro_etiqueta);
                                    array_push($json['rango'], $obj -> rango);
                                }
                            }
                            $json['total'] = count($json['semillera']);
                            break;
                        case 'cuenta' :
                            $json['responsable'] = array();
                            $json['semillera'] = array();
                            $json['semillerista'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();                        
                            if (!empty($search)) {
                                Cuenta::getCuentaCertificaFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['semillera'], utf8_encode($obj -> semillera));
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['cultivo'], utf8_encode($obj -> cultivo));
                                    array_push($json['variedad'], utf8_encode($obj -> variedad));
                                }
                            }
                            $json['total'] = count($json['semillera']);
                            break;
                    }
                    echo json_encode($json);
                    break;                               
                case 'generacion' ://variedad de un cultivo
                    $nombre = strip_tags($_GET['nombre']);
                    $apellido = strip_tags($_GET['apellido']);
                    Variedad::getNombreVariedadByCultivo(Cuenta::getCultivoCuenta($nombre, $apellido));
                    $json = array();
                    while ($fila = DBConnector::objeto()) {
                        array_push($json, $fila -> variedad);
                    }
                    echo json_encode($json);
                    break;               
                case 'isolicitud' ://id de solicitud segun nombre y apellido
                    $nombre = trim($_GET['nombre']);
                    $apellido = trim($_GET['apellido']);

                    Solicitud::getIdSolicitudProductor($nombre, $apellido);
                    $json = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($json, $row -> id_solicitud);
                    }
                    echo json_encode($json);
                    break;
                
                case 'isemilla' :// id de semilla segun id de solicitud
                    $isolicitud = isset($_GET['isolicitud']) ? intval($_GET['isolicitud']) : intval($_GET['id']);
                    $json['isemilla'] = Tiene::getIdSemilla($isolicitud);

                    echo json_encode($json);
                    break;                
                case 'isuperficie' ://id de superficie segun id de semilla
                    $isemilla = isset($_GET['isolicitud']) ? intval($_GET['isolicitud']) : intval($_GET['id']);
                    $json['isuperficie'] = Superficie::getIdSuperficieByIdSemilla($isemilla);

                    echo json_encode($json);
                    break;                 
                case 'lista_campos' ://lista de campos para muestra de laboratorio
                    $campos['campos'] = array();
                /*
                    $laboratorio -> getListCamposCertifica();                    
                    if (DBConnector::filas()) {
                        while ($obj = DBConnector::objeto()) {
                            array_push($campos['campos'], $obj -> nro_campo);
                        } 
                                                                      
                    }
                    */
                    $cosechas->getNroCamposLab('certifica');
                    if(DBConnector::filas()){
                        while ($obj = DBConnector::objeto()) {
                            array_push($campos['campos'], $obj -> nro_campo);
                        }
                    }
                    $laboratorio -> getCamposAcondicionamiento();
                    if (DBConnector::filas()) {
                        while ($obj = DBConnector::objeto()) {
                            array_push($campos['campos'], $obj -> nro_campo);
                        }                                               
                    }
                    if (count($campos['campos'])){
                        $campos['msg'] = 'OK';
                        $campos['total']=count($campos['campos']);
                    }else{
                        $campos['msg'] = 'error';
                    }
                    echo json_encode($campos);
                    break;                
                case 'ls_solicitudes' ://lista de solicitudes segun etapa y sistema
                    $nrosolicitudes = array();
                    $etapa = trim($_GET['etapa']);
                    $sistema = trim($_GET['sistema']);
                    $solicitudes -> getNoSolicitudes($etapa, $sistema);
                    //DBConnector::mensaje();
                    if (DBConnector::filas()) {
                        while ($ls_solicitudes = DBConnector::objeto()) {
                            array_push($nrosolicitudes, $ls_solicitudes -> nro_solicitud);
                        }
                        $json['nro_solicitudes'] = $nrosolicitudes;
                        $json['msg'] = 'OK';
                    } else {
                        $json['nro_solicitudes'] = '';
                        $json['msg'] = 'Error. No se encuentran solicitudes';
                    }
                    echo json_encode($json);
                    break;
                case 'ls_semilleraBySolicitud' :
                    $nro = $_GET['nro'];
                    $sistema = Sistema::getIdSistema(trim($_GET['sistem']));
                    $ls_semilleras = array();
                    #echo $sistema;

                    $solicitudes -> getNoSolicitudBy2($nro, $sistema);
                    #var_dump(DBConnector::objeto());
                    while ($ls_solicitudes = DBConnector::objeto()) {
                        $nombre = Semillera::getNombreSemillera($ls_solicitudes -> id_semillera);
                        $nombre = utf8_encode($nombre);
                        array_push($ls_semilleras, strtoupper($nombre));
                    }
                    $json['semilleras'] = $ls_semilleras;

                    echo json_encode($json);

                    break;

                case 'ls_semilleras' :
                    $semilleras = array();
                    $etapa = trim($_GET['etapa']);
                    $sistema = trim($_GET['sistema']);
                    $solicitudes -> getSemillerasBySistema($etapa, $sistema);
                    $semilleras = array();
                    $id = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($semilleras, utf8_encode($row -> semilleras));
                        array_push($id,$row->id_semillera);
                    }
                    $json['semilleras'] = $semilleras;
                    $json['id']=$id;
                    echo json_encode($json);
                    break;
                case 'ls_semilleristas' :
                    $semilleristas = array();
                    Solicitud::getFullProductores();
                    while ($lssemilleristas = DBConnector::resultado()) {
                        array_push($semilleristas, $lssemilleristas -> nombre);
                    }
                    $json['semilleristas'] = $semilleristas;

                    echo json_encode($json);
                    break;
                case 'ls_cultivos' :
                    $cultivos = array();
                    Semilla::getAllCultivos();
                    while ($lscultivo = DBConnector::objeto()) {
                        $nombre_cultivo = utf8_encode(ucfirst($lscultivo -> cultivo));
                        //if ($nombre_cultivo != "Descanso")
                        array_push($cultivos, $nombre_cultivo);
                    }
                    $json['cultivos'] = $cultivos;

                    echo json_encode($json);
                    break;
                case 'ls_variedades' :
                    $cultivo = $_GET['cultivo'];
                    $variedades = array();
                    $variedad -> getVariedades(Cultivo::getIdCultivo($cultivo));
                    //Semilla::getVariedadesCultivo();
                    if (DBConnector::filas() > 0) {
                        while ($lsvariedad = DBConnector::objeto()) {
                            $nombre_variedad = ucfirst(utf8_encode($lsvariedad -> nombre_variedad));
                            array_push($variedades,($dates->cambiar_acentoBySigno($nombre_variedad)));
                        }
                        $json['variedades'] = ($variedades);
                    } else {
                        $json['variedades'] = '';
                    }

                    echo json_encode($json);
                    break;
                case 'ls_variedades_plantines' :
                        $cultivo = $_GET['cultivo'];
                        $variedades = array();
                        $icultivo = $cultivos -> setCheckCultivo($cultivo);
                        $variedad -> getVariedades($icultivo);
                        //Semilla::getVariedadesCultivo();
                        if (DBConnector::filas() > 0) {
                            while ($lsvariedad = DBConnector::objeto()) {
                                $nombre_variedad = ucfirst($lsvariedad -> nombre_variedad);
                                array_push($variedades,($dates->cambiar_acentoBySigno($nombre_variedad)));
                            }
                            $json['variedades'] = ($variedades);
                        } else {
                            $json['variedades'] = '';
                        }
                        $json['total'] = count($variedades);
                        echo json_encode($json);
                    break;
                case 'ls_gestiones' :
                    $sistema = $solicitudes -> getIdSistema(trim($_GET['area']));
                    $gestiones = array();
                    Solicitud::getGestiones($sistema);
                    while ($lsgestion = DBConnector::resultado()) {
                        $anio = $dates -> obtener_anho($lsgestion -> fecha);

                        array_push($gestiones, $anio);
                    }
                    //array_push($gestiones, date('Y'));
                    $gestiones = array_values(array_unique($gestiones));
                    $json['gestiones'] = $gestiones;

                    echo json_encode($json);
                    break;
                //devuelve los datos deun numero de campo para muestra de laboratorio
                case 'muestraCampo':
                    $campo = strip_tags($_GET['campo']);
                    $semilla->getDatosMuestraLaboratorioByCampo($campo);
                    
                    $obj = DBConnector::objeto();
                    if (DBConnector::filas()){
                        $json['cultivo'] = $obj->cultivo;
                        $json['variedad'] = utf8_encode($obj->variedad);
                        $json['comunidad'] = $obj->comunidad;
                    }
                    $laboratorio -> getCamposAcondicionamientoForMuestra($campo);
                    if(DBConnector::filas()){
                        $obj = DBConnector::objeto();
                        $json['cultivo'] = $obj -> cultivo;
                        $json['variedad'] = utf8_encode($obj -> variedad);
                        $json['comunidad'] = $obj -> comunidad;
                    }
                    echo json_encode($json);                    
                    break;
                case 'nros_semilleras_semilleristas': //opcion para autocompletado segun cadena
                    $cadena = strip_tags($_GET['term']); //cadena a buscar
                    $estado = strip_tags($_GET['estado']); //estado de etapa
                    $etapa = strip_tags($_GET['etapa']); //etapa de busqueda
                    if (is_numeric($cadena)){ //si es numero buscar solo la solicitud
                        $nros_solicitudes = array();
                        switch ($etapa) {
                            case 'semilla':
                                Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertifica($cadena, $estado);
                                break;
                        }                        
                        while ($obj = DBConnector::objeto()) {
                            array_push($nros_solicitudes, $obj -> nro_solicitud);
                        }
                        echo json_encode($nros_solicitudes);
                    }else{
                        //si es texto buscar coincidencia de semillera                        
                        switch ($etapa) {
                            case 'semilla':
                                Semillera::getNombreSemilleraByNombreAndEstadov2($cadena, $estado);
                                break;
                        }
                        if(!DBConnector::filas()){
                            //si no hay semilleras buscar coincidencia de semilleristas
                            switch ($etapa) {
                                case 'semilla':
                                    Semillerista::getFullNameByNombreAndEstado($cadena, $estado);
                                    break;
                            }
                            //llenar coincidencias de semilleristas
                            $semilleristas = array();
                            while ($fila = DBConnector::objeto())
                                array_push($semilleristas, $fila -> semillerista);
                            echo json_encode($semilleristas);
                        }else{
                            //llenar coincidencias de semilleras
                            $semilleras = array();
                            while ($fila = DBConnector::objeto())
                                array_push($semilleras, $fila -> semillera);
                            echo json_encode($semilleras);
                        }           
                    }
                    
                    break;
                case 'no_sem_sol' ://filtro de autocompletado
                $opcion = trim($_GET['opc']);
                $q = trim($_GET['term']);
                
                switch ($opcion) {
                      
                    #opcion para autocompletado  nro_solicitud-semillera-semillerista    
                    #FUNCIONA
                    case 'nosol' :  
                        $estado = intval($_GET['estado']);
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertifica($q, $estado,'nro');#numero solicitud
                        if (DBConnector::filas()){
                            $json = array();
                            while ($obj = DBConnector::objeto()) {
                                array_push($json, $obj -> nro_solicitud);
                            }
                        }else{
                            Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertifica($q, $estado,'semillera');#semillera
                            if (DBConnector::filas()){
                                $json = array();
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json, ($obj -> semillera));
                                }                   
                            }else{
                                Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertifica($q, $estado,'semillerista');#semillerista
                                $json = array();
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json, utf8_encode($obj -> semillerista));
                                }
                            }
                        }
                        #var_dump($json);
                        echo json_encode($json);
                        break;
                        
                    #opcion inspeccion
                    case 'nosol_inspeccion':
                        $inicio = intval($_GET['inicio']); 
                        $fin = intval($_GET['fin']);
                        $json = array();
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertificaSuperficie($q,$inicio,$fin); #numero solicitud
                        if (DBConnector::filas()){
                            while ($obj = DBConnector::objeto()) {
                                array_push($json, $obj -> nro_solicitud);
                            }    
                        }else{
                            Semillera::getNombreSemilleraByNombreAndEstadoCertificaSuperficie($q, $inicio,$fin); #semillera
                            if (DBConnector::filas()){
                                while ($fila = DBConnector::objeto())
                                    array_push($json,utf8_encode($fila -> semillera));
                            }else{
                                Semillerista::getFullNameByNombreAndEstadoCertificaSuperficie($q, $inicio,$fin);#semillerista
                                while ($obj = DBConnector::objeto())
                                    array_push($json, utf8_encode($obj -> semillerista));
                            }                            
                        }
                        echo json_encode($json);
                        break;
                    case 'nosol_cosecha':
                        $inicio = intval($_GET['inicio']); 
                        $fin = intval($_GET['fin']);
                        $json = array();
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertificaSuperficie($q,$inicio,$fin); #numero solicitud
                        if (DBConnector::filas()){
                            while ($obj = DBConnector::objeto()) {
                                array_push($json, $obj -> nro_solicitud);
                            }    
                        }else{
                            Semillera::getNombreSemilleraByNombreAndEstadoCertificaCosecha($q, $inicio,$fin); #semillera
                            if (DBConnector::filas()){
                                while ($fila = DBConnector::objeto())
                                    array_push($json, $fila -> semillera);
                            }else{
                                Semillerista::getFullNameByNombreAndEstadoCertificaSuperficie($q, $inicio,$fin);#semillerista
                                while ($obj = DBConnector::objeto())
                                    array_push($json, utf8_encode($obj -> semillerista));
                            }                            
                        }
                        echo json_encode($json);
                        break;
                    case 'nosol_semillap':
                        $inicio = intval($_GET['inicio']); 
                        $fin = intval($_GET['fin']);
                        $json = array();
                        Solicitud::getNroSolicitudByNroSolicitudAndEstadoCertificaSuperficie($q,$inicio,$fin); #numero de solicitud
                        if (DBConnector::filas()){
                            while ($obj = DBConnector::objeto())
                                array_push($json, $obj -> nro_solicitud);    
                        }else{
                            $estado = intval($_GET['estado']);
                            Semillera::getNombreSemilleraByNombreAndEstadoCertificaSemillaP($q, $estado);  #semillera
                            if (DBConnector::filas()){
                                while ($fila = DBConnector::objeto())
                                    array_push($json, utf8_encode($fila -> semillera));
                                $id_semillera = Semillera::getIdSemilleraCertifica($q);
                                $isolicitud = Solicitud::getIdSolicitudByIdSemillera($id_semillera);
                                $cultivo = Semilla::getCultivoByIdSolicitudCertifica($isolicitud);
                                if ($cultivo == "papa" || $cultivo == "Papa"){
                                    Semillera::getListSemilleraPapa($q);
                                    while ($fila = DBConnector::objeto())
                                        array_push($json, $fila -> semillera);
                                }
                            }else{
                                Semillerista::getFullNameByNombreAndEstadoCertificaSuperficie($q, $inicio,$fin);
                                while ($obj = DBConnector::objeto()) 
                                    array_push($json, utf8_encode($obj -> semillerista));                        
                            }
                        }                        
                        echo json_encode($json);
                        break;
     
                    case 'semillera' :
                        $estado = intval($_GET['estado']);
                        Semillera::getNombreSemilleraByNombreAndEstadov2($q, $estado);
                        $coincidencias = array();
                        while ($fila = DBConnector::objeto())
                            array_push($coincidencias, $fila -> semillera);

                        echo json_encode($coincidencias);
                        break;                    
                    case 'semillerista' :
                        $estado = intval($_GET['estado']);
                        Semillerista::getFullNameByNombreAndEstado($q, $estado);
                        $json = array();
                        while ($obj = DBConnector::objeto()) {
                            array_push($json, utf8_encode($obj -> semillerista));
                        }
                        echo json_encode($json);
                        break;
                }
                break;                 
                
                /**
                 * iproductor   id de la solicitud
                 * isuperficie  id de la superficie
                 * $estado      int   (0-10)   0:solicitud,1:semilla,2-4:superficie,5-7:inspeccion,
                 *                             8:cosecha9:semillaproducida10:laboratorio
                 * */
                case 'nroSolicitud' :
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $estado =  isset ($_GET['estado'])?intval(trim($_GET['estado'])):0;
                    $tabla = isset($_GET['tbl']) ? $_GET['tbl'] : '';
                    //echo $nombre;
                    $semillera = isset($_GET['semillera']) ? $_GET['semillera'] : '';
                    $semillera = str_replace('*', ' ', $semillera);
                    switch ($estado) {
                        case ($estado==2 || $estado==3 || $estado==4) :
                            // echo 'caso2-3-4';
                            $solicitudes -> getNroSolicitud($nombre, $apellido, $tabla, $semillera);
                            while ($idsolicitud = DBConnector::objeto()) {
                                $solicitudID = $idsolicitud -> isolicitud;
                            }
                            if (empty($solicitudID)) {

                                $tabla = 'superficieI';
                                $solicitudes -> getNroSolicitud($nombre, $apellido, $tabla, $semillera);
                                while ($idsolicitud = DBConnector::objeto()) {
                                    $solicitudID = $idsolicitud -> isolicitud;
                                }
                                $json['iproductor'] = $solicitudID;
                            } else

                                $json['iproductor'] = $solicitudID;
                            $superficieID = $superficie -> getSuperficieID($nombre, $apellido, $solicitudID);

                            //$superficie_id = mysql_fetch_object($superficieID);
                            $json['isuperficie'] = $superficieID;
                            //_id->id_superficie;
                            echo json_encode($json);
                            break;
                        default :
                            $json['iproductor'] = Solicitud::getIdSolicitudProductorByEstado($nombre, $apellido, $estado);
                            echo json_encode($json);
                            break;
                    }
                    break;
                //precios de certificacion de un cultivo
                case 'precios':
                    $cultivo = trim($_GET['cul']);
                    $costos->getCostoByName($cultivo);
                    $json['descripcion']= $json['precio'] = array();
                    $obj = DBConnector::objeto();
                    array_push($json['descripcion'],'Inscripci&oacute;n');
                    array_push($json['precio'],$obj->inscripcion);
                    array_push($json['descripcion'],'Inspecci&oacute;n');
                    array_push($json['precio'],$obj->inspeccion);
                    array_push($json['descripcion'],'Analisis de laboratorio y <br>etiquetaci&oacute;n');
                    array_push($json['precio'],$obj->analisis_etiqueta);
                    
                    echo json_encode($json);
                    break;
                case 'saldo_campanha_anterior' :
                    $nombre = $dates -> modificar($_GET['nombre']);
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $semillera = $_GET['semillera'];
                    $isolicitud = Solicitud::getIdSolicitudProductorCuenta($nombre, $apellido);
                    $isemilla = Tiene::getIdSemilla($isolicitud);
                    
                    Cuenta::getSaldoAnterior($nombre, $apellido, $semillera,$isemilla);

                    if (!DBConnector::nroError()) {
                        $total = DBConnector::objeto();
                        $json['saldo_anterior'] = ($total ->saldo_anterior)?$total->saldo_anterior:0;
                    } else {
                        $json['saldo_anterior'] = 0;
                    }
                    echo json_encode($json);
                    break;
                //determinar la etapa en la que se encuentra parte 1
                case 'etapa' :
                    $isemilla = $_GET['isemilla'];
                    $isemillerista = isset($_GET['isemillerista']) ? $_GET['isemillerista'] : '';
                    $json = array();
                    Solicitud::estado($isemilla, $isemillerista);

                    //si el cultivo es papa  obtengo en que estado se encuentra
                    #echo DBConnector::filas();
                    if (DBConnector::filas() > 0) {
                        while ($fila = DBConnector::objeto()) {
                            $json['estado'] = $fila -> laboratorio;
                            $json['esuperficie'] = $fila -> estado;
                            $json['iproductor'] = $fila -> id_solicitud;
                            $json['cultivo'] = $fila -> cultivo;
                            $json['inscrita'] = $fila -> inscrita;
                            $json['rechazada'] = $fila -> rechazada;
                            $json['retirada'] = $fila -> retirada;
                            $json['aprobada'] = ($fila -> rechazada == 0.0 && $fila -> retirada == 00) ? $fila -> inscrita : $fila -> aprobada;
                            $json['inspeccion1'] = empty($fila -> primera) ? '' : $fila -> primera;
                        }
                    } else {//en caso que no sea papa obtengo su estado
                        Solicitud::estado_2($isemilla, $isemillerista);
                        #echo DBConnector::filas().' noPAPA';
                        if (DBCOnnector::filas() > 0) {
                            while ($fila = DBConnector::objeto()) {
                                $json['estado'] = $fila -> laboratorio;
                                $json['esuperficie'] = 0;
                                $json['iproductor'] = $fila -> id_solicitud;
                                $json['cultivo'] = $fila -> cultivo;
                                $json['inscrita'] = $fila -> inscrita;
                                $json['rechazada'] = $fila -> rechazada;
                                $json['retirada'] = $fila -> retirada;
                                $json['aprobada'] = $fila -> aprobada;
                                $json['inspeccion1'] = empty($fila -> primera) ? '' : $fila -> primera;
                            }
                        } else {
                            /**estado cuando no se realizo ninguna inspeccion*/
                            Semilla::getNroCampoInscritaById($isemilla, $isemillerista);
                            /* echo DBCOnnector::filas() . 'sin inspeccion';
                             var_dump(DBConnector::filas()); */
                            if (DBConnector::filas()) {
                                #realizar primera inspeccion
                                while ($fila = DBConnector::objeto()) {
                                    #var_dump($fila);exit;
                                    $json['nro_campo'] = $fila -> nro_campo;
                                    $json['estado'] = $fila -> estado;
                                    $json['esuperficie'] = 0;
                                    $json['iproductor'] = $fila -> id_solicitud;
                                    $json['cultivo'] = $fila -> nombre_cultivo;
                                    $isolicitud = Tiene::getIdSolicitud($isemilla);
                                    $inscrita = Semilla::getSuperficieInscrita($isemilla, $isolicitud);
                                    $json['inscrita'] = $inscrita;
                                    $json['rechazada'] = 0.0;
                                    $json['retirada'] = 0.0;
                                    $json['aprobada'] = $inscrita;
                                }
                            } else {
                                #echo 'por false';
                                #realizada primera inspeccion
                                $json['nro_campo'] = Semilla::getNroCampoByIdSemilla($isemilla);
                                $json['estado'] = Solicitud::getEstadoSolicitud($isemillerista);
                                $json['esuperficie'] = 0;
                                $json['iproductor'] = Solicitud::getEstadoSolicitud($isemillerista);
                                $json['cultivo'] = Semilla::getCultivo($isemilla);
                                $inscrita = Semilla::getSuperficieInscrita($isemilla, Tiene::getIdSolicitud($isemilla));

                                $json['inscrita'] = $inscrita;
                                $json['rechazada'] = 0.0;
                                $json['retirada'] = 0.0;
                                $json['aprobada'] = $inscrita;
                            }
                        }
                    }
                    echo json_encode($json);
                    break;
                //id de solicitud
                //devuelve es estado, comunidad y nro de campo
                case 'etapa_solicitud' :
                    $json = array();
                    $id = intval($_REQUEST['isolicitud']);
                    Solicitud::getEstado($id);
                    #var_dump(DBConnector::objeto());exit;
                    $row = DBConnector::objeto();
                    $json['estado'] = $row -> estado;
                    $json['comunidad'] = $row -> comunidad;
                    $json['nro_campo'] = Semilla::getNroCampoByIdSemilla2(Tiene::getIdSemilla($id));

                    //var_dump($json);exit;
                    echo json_encode($json);
                    break;
                //devuelve los datos de una inspeccion
                case 'inspecciones' :
                    $isuperficie = intval($_GET['isuperficie']);
                    $inspecciones -> getInspecciones($isuperficie);
                    #echo DBConnector::mensaje();
                    $fila = DBConnector::objeto();
                    if (DBConnector::filas()){
                        $json['comunidad'] = '';
                        $json['nroCampo'] = '';
                        $json['vegetativa'] = $dates -> cambiar_formato_fecha($fila -> vegetativa);
                        $json['observacion_1'] = $fila -> observacion_1;
                        $json['floracion'] = $dates -> cambiar_formato_fecha($fila -> floracion);
                        $json['observacion_2'] = $fila -> observacion_2;
                        $json['precosecha'] = $dates -> cambiar_formato_fecha($fila -> precosecha);
                        $json['observacion_3'] = $fila -> observacion_3;
                        $json['estado'] = $fila -> estado;                        
                        $fecha = explode("-",$fila->fecha_siembra);
                        $json['dia'] = $fecha[2];
                        $json['mes'] = $fecha[1];
                        $json['anho'] = $fecha[0];
                    }else{                        
                        $json['estado'] = 4;                        
                    }
                    
                    echo json_encode($json);
                    break;
                //resumen  de todos los pasos para la cancelacion de la cuenta
                case 'resumenCta' :
                    //id solicitud
                    $iSolicitud = intval($_GET['iSolicitud']);
                    //id semilla
                    $iSemilla = intval($_GET['iSemilla']);
                    //id superficie  
                    $iSuperficie = empty($_GET['iSuperficie'])?0:intval($_GET['iSuperficie']);
                    //estado de solicitud 
                    $iEstado = intval($_GET['iEstado']);
                    
                    $campania = $semilla->getCampaniaByIdSemillaForCuenta($iSemilla);
                    $cultivo = $semilla->getCultivoByIdSemillaCertifica($iSemilla);
                    $variedad = $semilla->getVariedadByIdSemillaForCuenta($iSemilla);
             
                    $json = array();
                    
                    //obtenemos si tiene al menos una inspeccion
                    $inspecciones = (empty($iSuperficie))?0:Inspeccion::getPrimeraInspeccion($iSuperficie);

                    //obtenemos la categoria que se pretende producir y el cultivo
                    Cuenta::getCategoriaCampo($iSolicitud, $cultivo, $campania, $variedad);
                    //obtenemos la superficie aprobada del productor y semilla
                    Superficie::getSuperficieAprobada($iSolicitud, $iSemilla);
                    $fila = DBConnector::objeto();
                    if (DBConnector::filas())
                        $json['supAprobada'] = $fila -> aprobada;
                    else
                        $json['supAprobada'] = 0.0;
                    $costos -> getCostoByName($cultivo);
                    #var_dump(DBConnector::objeto());
                    if (!DBConnector::nroError()){
                        $fila = DBConnector::objeto();
                        if (!strcasecmp (strtolower ($fila -> cultivo), 'papa') ){
                            $igeneracion = Semilla::getCategoriaProducir($iSemilla);                        
                            $json['catAprobada'] = Categoria::getNombreCategoriaByIdGeneracion($igeneracion).'-'. Generacion::getInicialGeneracion($igeneracion);
                        }else{
                            $iGeneracion = Semilla::getCategoriaProducir($iSemilla);
                            $json['catAprobada'] = Categoria::getNombreCategoriaByIdGeneracion($iGeneracion).'-'. Generacion::getInicialGeneracion($iGeneracion);
                        }
                        switch ($iEstado) {
                            //inspeccion
                            case '5':case'6':case'7':case'8':case'9':
                                $excepciones = array('plantines','acondicionamiento');
                                if (!in_array($cultivo,$excepciones)){
                                    $json['inscripcion'] = round($fila -> inscripcion,2);
                                    $json['costo_inscripcion'] = number_format(round(($json['inscripcion'] * $json['supAprobada']),2),2,'.',','); 
                                    $json['inspeccion'] = intval($fila -> inspeccion);
                                    $json['costo_inspeccion'] = round($json['inspeccion'] * $json['supAprobada'],2);
                                    $json['analisisEtiqueta'] = '0';
                                    $json['plantines'] = '0';
                                    $json['acondicionamiento'] = '0';
                                }else{
                                     $json['inscripcion'] = $json['costo_inscripcion'] = 0;
                                     $json['inspeccion'] = $json['costo_inspeccion'] = 0;
                                    if ($cultivo == 'plantines'){
                                        $json['plantines'] = number_format($fila -> inscripcion * $json['supAprobada'],2,'.',',');
                                    }else{
                                        $json['plantines'] = number_format($fila -> analisis_etiqueta * $json['supAprobada'],2,'.',',');
                                    }
                                }
                                break;
                                //resultado de laboratorio y semilla producida
                            case '11':case'12':
                            
                                $json['inscripcion'] = $fila -> inscripcion;
                                $json['costo_inscripcion'] = number_format(round(($json['inscripcion'] * $json['supAprobada']),2),2,'.',','); 
                                $json['inspeccion'] = $fila -> inspeccion;
                                #var_dump($json['inspeccion']);
                                $json['costo_inspeccion'] = number_format(round(($json['inspeccion'] * $json['supAprobada']),2),2,'.',',');
                                $json['analisisEtiqueta'] = round($fila -> analisis_etiqueta,2);
                                if (strtolower($cultivo) == 'plantines'  ){
                                    $json['costo_inspeccion'] = 0;
                                    $json['inscripcion'] = round($fila -> inscripcion * $json['supAprobada']);
                                }
                                if (strtolower($cultivo) == 'acondicionamiento'){
                                    $json['inspeccion'] = $json['costo_inspeccion'] = 0;
                                    $json['incripcion'] = $fila->analisis_etiqueta;
                                    $json['costo_inscripcion'] = round($fila -> analisis_etiqueta * $json['supAprobada'],2);
                                }
                                break;
                            default://solo es inscripcion
                                $costo =  $fila -> inscripcion* $json['supAprobada'];
                                $json['inscripcion'] = number_format($costo,2,'.',',');
                                $json['inspeccion'] = '0.00';
                                $json['analisisEtiqueta'] = '0.00';
                                $json['plantines'] = '0.00';
                                $json['acondicionamiento'] = '0.00';
                                break;
                        }

                        $cuenta  =  Cuenta::checkCuentaByIdSemilla($iSemilla);  
                        
                        if ($cuenta){
                            $json['cuota'] = Cuenta::getSaldoByIdSemilla($iSemilla);                            
                        }else{                            
                            $json['cuota'] = 0.00;    
                        }
                        $json['total'] = $json['costo_inscripcion']+$json['costo_inspeccion'];                                                                                                                        
                        
                    }else{//error en la seleccion de  precios para el cultivo
                        $json['catAprobada'] = 'Sin especificar';
                        $json['inscripcion'] = $json['inspeccion'] = $json['analisisEtiqueta'] = 0.00;
                        $json['total'] = 0.00;
                    }
                  
                    //superficie total utilizada por el semillerista
                    Superficie::getSuperficieTotal($iSolicitud, $cultivo, $variedad, $campania);
                    $row = DBConnector::objeto();
                    if (!$row -> total)
                        $json['supTotal'] = 0.00;
                    else
                        $json['supTotal'] = $row -> total;
 
                    //nro de campo del semillerista
                    $nroCampo = Semilla::getNroCampoByIdSemilla($iSemilla);

                    if ($cultivo != 'papa') {
                        $nroBolsa = Laboratorio::getNroBolsasSemillera($nroCampo);
                        //echo $nroBolsa;
                        if (DBConnector::filas()) {
                            $fila3 = DBConnector::objeto();
                            $json['bolsa'] = $fila3 -> bolsa;
                        } else {
                            $json['bolsa'] = '0';
                        }
                    } else {
                        $json['bolsa'] = '0';
                    }
                    $cat_sembrada = Semilla::getCategoriaSembrada($iSemilla);
                    $cat_obtenida = Semilla::getCategoriaProducir($iSemilla);
                    $etiquetas = SemillaProd::getEtiquetaByIdSolicitud($iSolicitud);
                    $json['etiquetas'] = $etiquetas;
                    $json['total'] += number_format(($json['etiquetas']*$json['analisisEtiqueta']),2,'.',',');
                    $json['saldo'] = $json['total']-$json['cuota'];
                    $json['analisisEtiqueta'] *= $json['etiquetas'];
                    echo json_encode($json);
                    break;
                //devolver el rendimiento de campo de un cultivo segun el nro de campo
                case 'rendimiento_campo':
                    $campo = intval($_GET['nro']);
                    
                    $rendimiento = $cosechas->getRendimientoCampoCertificacionByNroCampo($campo);
                    $json['rendimiento_campo'] = $rendimiento;
                    
                    echo json_encode($json);
                    break;
                case 'semillap' ://devuelve el id de cosecha y el id de laboratorio de un cultivo
                    $nro_campo = $_GET['campo'];

                    //id solicitud
                    $isolicitud = Tiene::getIdSolicitud(Semilla::getIdSemillaByNroCampo($nro_campo));
                    // cultivo q existe en el campo
                    $cosechas -> cultivo_Campo($nro_campo);
                    #echo DBConnector::mensaje();
                    $nombreCultivo = '';
                    while ($fila = DBConnector::objeto()) {
                        $nombreCultivo = $fila -> cultivo;
                    }
                    #echo $nombreCultivo;
                    //verifico si el cultivo es papa
                    if (strtolower($nombreCultivo) == 'papa')
                        $datos['laboratorio'] = 5;
                    else {
                        //id de laboratorio
                        $laboratorio -> getIdLaboratorio($nro_campo);
                        #echo '2: '.DBConnector::mensaje();
                        if (!DBConnector::nroError()) {
                            $temp = DBConnector::objeto();
                            $datos['nro_analisis'] = $temp -> nro_analisis;
                        }
                    }
                    //consulta para tener el id de hoja de cosecha
                    $cosechas -> getHojaCosechaId($nro_campo);
                    #echo '3: '.DBConnector::mensaje();
                    if (!DBConnector::nroError()) {
                        while ($temp = DBConnector::objeto()) {
                            $temp2 = $temp -> id_cosecha;
                        }
                        $datos['cosecha'] = $temp2;
                    }
                    $datos['isolicitud'] = $isolicitud;
                    echo json_encode($datos);
                    break;
                case 'superficie' :
                        $noSolicitud = intval($_GET['nro']);
                        $semillera = trim($_GET['semillera']);
                        $nombre = $dates->search_tilde(trim($_GET['nombre']));
                        $apellido = $dates->search_tilde((trim($_GET['apellido'])));
                        $estado = intval($_GET['estado']);
                        //echo $_GET['iproductor'];exit;
                        $json['isolicitud'] = Solicitud::getIdSolicitudProductorByEstado($nombre, $apellido, $estado);
    
                        $solicitudes -> getComunidadById($json['isolicitud']);
                        $row = DBConnector::objeto();
                        $json['comunidad'] = ucfirst($row -> comunidad);
    
                        $json['isemilla'] = Tiene::getIdSemilla($json['isolicitud']);
    
                        $superficie -> getsuperficieCampo($json['isolicitud']);
                        $row = DBConnector::objeto();
    
                        $json['nro_campo'] = $row -> nro_campo;
                        $json['inscrita'] = $row -> superficie;
    
                        echo json_encode($json);
                break;
                case 'variedad':
                    $cultivo = $dates->modificar(trim($_GET['cultivo']));
                    Variedad::getNombreVariedadByCultivo($cultivo);
                    
                    $temp = array();
                    while($obj=DBConnector::objeto()){
                        array_push($temp,$obj->variedad);
                    }
                    echo json_encode($temp);
                    break;  
           }

            break;
        //formularios para la actualizacion de acuerdo a la etapa
        //actualizacion de datos
        case 'form' :
            switch($pagina) {
                case 'upd_solicitud' :
                    $id = intval($_REQUEST['id']);
                    Solicitud::getSolicitudById($id);

                    include '../vista/certificacion/editar/solicitud.php';
                    break;
                case 'upd_semilla' :
                    $id = intval($_REQUEST['id']);
                    Semilla::getSemillaById($id);
                    include '../vista/certificacion/editar/semilla.php';
                    break;
                case 'upd_superficie' :
                    $id = intval($_REQUEST['id']);
                    Superficie::getSuperficieById($id);
                    include '../vista/certificacion/editar/superficie.php';
                    break;
                case 'supcampos' :
                    //numeros de campos segun id de semillerista [id solicitud]
                    $idSol = intval($_GET['id']);
                    $semilla -> getNroCamposProductor($idSol);
                    $campos = array();
                    while ($fila = DBconnector::objeto()) {
                        array_push($campos, $fila -> nro_campo);
                    }
                    $newcampos = array();
                    $total = array();
                    foreach ($campos as $campo) {
                        $id_semilla = $semilla -> getIdSemillaByCampo($campo);
                        $superficie -> getSuperficieInscritaByIdSem($id_semilla);
                        #echo DBConnector::mensaje();
                        $filas = DBConnector::objeto();
                        if ($filas) {
                            array_push($newcampos, $campo);
                            array_push($newcampos, $filas -> inscrita);
                            array_push($newcampos, $filas -> rechazada);
                            array_push($newcampos, $filas -> retirada);
                            array_push($newcampos, $filas -> aprobada);
                        } else {
                            array_push($newcampos, $campo);
                            array_push($newcampos, 0.0);
                            array_push($newcampos, 0.0);
                            array_push($newcampos, 0.0);
                            array_push($newcampos, 0.0);
                        }
                    }
                    //echo json_encode($newcampos);
                    include '../vista/certificacion/superficie_campos.html.php';
                    break;
                case 'upd_inspeccion' :
                    $id = intval($_REQUEST['id']);
                    Inspeccion::getInspeccionById($id);
                    $funcion = new Funciones;
                    include '../vista/certificacion/editar/inspeccion.php';
                    break;
                case 'inspcampos' :
                    //numeros de campos segun id de semillerista
                    $id = intval($_GET['id']);
                    $semilla -> getNroCamposProductor($id);
                    #include '../vista/certificacion/inspecciones_superficie.html.php';
                    include '../vista/certificacion/inspeccion_superficie.html.php';
                    break;
                case 'upd_cosecha' :
                    $id = intval($_REQUEST['id']);
                    HojaCosecha::getCosechaById($id);
                    $funcion = new Funciones;
                    include '../vista/certificacion/editar/hoja_cosecha.php';
                    break;
                case 'upd_semillaP' :
                    $id = intval($_REQUEST['id']);
                    SemillaProd::getSemillaPById($id);
                    $funcion = new Funciones;
                    include '../vista/certificacion/editar/semilla_prod.php';
                    break;
                case 'upd_cuenta' :
                    $id = intval($_REQUEST['id']);
                    Cuenta::getCuentaById($id);
                    $funcion = new Funciones;
                    include '../vista/certificacion/editar/cuenta.php';
                    break;
            }
            break;
        //actualizacion de datos de acuerdo a la etapa
        case 'actualizar' :
            switch ($pagina) {
                case 'solicitud' :
                    $nombre = utf8_decode(ucwords(trim($_POST['name'])));
                    $apellido = (is_string($_POST['apellido']) && !empty($_POST['apellido'])) ? utf8_decode(ucwords(trim($_POST['apellido']))) : '';
                    $localidad = (!empty($_POST['localidad'])) ? ucwords(trim($_POST['localidad'])) : '-';
                    $comunidad = ucwords(trim($_POST['comunidad']));
                    $municipio = ucwords(trim($_POST['municipio']));
                    $provincia = ucwords(trim($_POST['provincia']));
                    $departamento = ucwords(trim($_POST['departamento']));

                    $nro_solicitud = trim($_POST['nro_solicitud']);
                    $sistema = (!empty($_POST['sistema'])) ? ucwords(trim($_POST['sistema'])) : 'Certificacion';
                    $semillera = strtoupper(trim($_POST['semillera']));
                    $nro_parcelas = intval(trim($_POST['nroParcelas']));
                    $id = intval($_POST['isolicitud']);

                    $sistema = utf8_decode($sistema);

                    $fecha = $dates -> cambiar_tipo_mysql(trim($_POST['f_solicitud']));
                    if (!empty($localidad)) {
                        $localidad = $dates -> specialWord($localidad);
                    } else {
                        $localidad = '';
                    }
                    $comunidad = $dates -> specialWord($comunidad);
                    //($id, $nroSolicitud, $sistema, $departamento, $provincia, $municipio, $comunidad, $semillera, $nombre, $apellido, $fecha, $nro_parcelas)
                    Solicitud::updateSolicitud($id, $nro_solicitud, Sistema::getIdSistema($sistema), $departamento, Provincia::getIdProvincia($provincia), Municipio::getIdMunicipio($municipio), $comunidad, Semillera::getIdSemillera($semillera), $nombre, $apellido, $fecha, $nro_parcelas);
                    if (DBConnector::filas()) {
                        echo 'OK';
                    } else {
                        echo 'error';
                    }
                    break;
                case 'semilla' :
                    $comunidad = trim(ucfirst($_POST['comunidad']));
                    $localidad = trim(ucfirst($_POST['localidad']));
                    $nro_campo = trim($_POST['nrocampo']);
                    $campanha = trim($_POST['campania']);
                    $cultivo = trim(ucfirst($_POST['cultivo']));
                    $variedad = trim($_POST['variedad']);
                    $cat_sembrada = strtoupper(trim($_POST['cat_sembrada']));
                    $cat_producir = strtoupper(trim($_POST['cat_producir']));
                    $cant_semilla = trim($_POST['cant_semilla']);
                    $nro_lote = empty($_POST['lotenro']) ? '' : trim($_POST['lotenro']);
                    $f_siembra = trim($_POST['f_siembra']);
                    if (!empty($_POST['plantasha']))
                        $plantas_ha = trim($_POST['plantasha']);
                    else
                        $plantas_ha = 0;
                    $cultivo_ant = trim(ucfirst($_POST['cult_anterior']));
                    $superficie = trim($_POST['superficie']);
                    $solicitud = trim($_POST['solicitud']);
                    Semilla::actualizarSemilla($comunidad, $localidad, $nro_campo, $campanha, $cultivo, $variedad, $cat_sembrada, $cat_producir, $cant_semilla, $nro_lote, $f_siembra, $plantas_ha, $cultivo_ant, $superficie, $solicitud);
                    if (DBConnector::filas()) {
                        echo 'OK';
                    } else {
                        echo 'error';
                    }
                    break;
                //REVISAR DE AQUI EN ADELANTE
                case 'aprobada' :
                    $tamanho['inscrita'] = floatval($_POST['suins']);
                    $tamanho['rechazada'] = floatval($_POST['surec']);
                    $tamanho['retirada'] = floatval($_POST['suret']);
                    $id = intval($_POST['nosolicitud']);

                    if (Superficie::updateSuperficie('aprobada', $tamanho, $id))
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();
                    break;

                //actualiza los datos de la superficie inscrita
                case 'superficie' :
                    $superficie_parcela['inscrita'] = floatval($_POST['suins']);
                    $superficie_parcela['rechazada'] = floatval($_POST['surec']);
                    $superficie_parcela['retirada'] = floatval($_POST['suret']);
                    $nro_campo = strval($_POST['campo']);
                    $isolicitud = intval($_POST['nosolicitud']);
                    $isemilla = intval($_POST['nosemilla']);

                    $superficie -> updateSuperficie('inscrita', $superficie_parcela, $isemilla);
                    Superficie::updateEstado($isemilla, 2, 1);
                    Solicitud::updateEstado($isolicitud, 2);
                    //echo DBConnector::filas();
                    if (DBConnector::filas()) {
                        Solicitud::updateLaboratorio($isolicitud);
                        echo 'OK';
                    } else {
                        echo DBConnector::mensaje();
                    }

                    break;
                case 'inspeccion_1' :
                    $id = intval($_REQUEST['id']);
                    $f_primera = $temp -> cambiar_tipo_mysql($_REQUEST['evegetativa']);
                    Inspeccion::updateInspecciones('primera', $f_primera, $id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();

                    break;

                case 'inspeccion_2' :
                    $id = intval($_REQUEST['id']);
                    $f_segunda = $temp -> cambiar_tipo_mysql($_REQUEST['efloracion']);
                    Inspeccion::updateInspecciones('segunda', $f_segunda, $id);
                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();
                    break;
                /** actualizar generacion*/
                case 'generacion' :
                    $id = intval($_POST['id']);
                    $nuevo = trim($_POST['generacion']);

                    $generacion -> actualizarGeneracion($id, $nuevo);

                    if (DBConnector::filas())
                        echo 'OK';
                    else
                        echo DBConnector::mensaje();

                    break;
            }
            break;

        case 'inspeccion_3' :
            $id = intval($_REQUEST['id']);
            $f_tercera = $dates -> cambiar_tipo_mysql($_REQUEST['ecosecha']);
            Inspeccion::updateInspecciones('tercera', $f_tercera, $id);
            if (DBConnector::filas()) {
                echo 'OK';
            } else {
                echo DBConnector::mensaje();
            }
            break;
        case 'cosecha' :
            $nro_campo = trim($_POST['campo']);
            $f_emsion = !empty($_POST['f_emision']) ? trim($_POST['f_emision']) : date('d-m-Y');
            $categ_campo = trim($_POST['categ_campo']);
            $rend_estimado = trim($_POST['estimado']);
            $sup_parcela = trim($_POST['sup_parcela']);
            $rend_campo = trim($_POST['rend_campo']);
            $acondicionadora = trim($_POST['acondicionadora']);
            $id = trim($_POST['icosecha']);
            $nro_cupones = !empty($_POST['nro_cupones']) ? trim($_POST['nro_cupones']) : 0;
            $rango_cupones = !empty($_POST['rango_cupones']) ? trim($_POST['rango_cupones']) : '';

            $f_emision2 = $temp -> cambiar_tipo_mysql($f_emision);
            HojaCosecha::updateHojaCosecha($nro_campo, $f_emision, $categ_campo, $rend_estimado, $sup_parcela, $rend_campo, $nro_cupones, $rango_cupones, $acondicionadora, $id);
            if (DBConnector::filas())
                echo 'OK';
            else
                echo DBConnector::mensaje();

            break;
        case 'semillaP' :
            $nro_campo = $_POST['campo'];
            $categoria = $_POST['categoria'];
            $generacion = $_POST['generacion'];
            $semilla_neta = $_POST['semilla_neta'];
            $cat_obtenida = $_POST['categ_obtenida'];
            $etiqueta = $_POST['etiqueta'];
            $rango = $_POST['rango'];
            $idLaboratorio = $_POST['ilaboratorio'];
            $isemillaP = $_POST['isemillaP'];
            SemillaProd::updateSemillaProd($nro_campo, $categoria, $generacion, $semilla_neta, $cat_obtenida, $etiqueta, $rango, $isemillaP);
            if (DBConnector::filas())
                echo 'OK';
            else
                echo DBConnector::mensaje();

            break;
        case 'cuenta' :
            $responsable = trim($_POST['responsable']);
            $semillera = strtoupper(trim($_POST['semillera']));
            $productor = ucwords(trim($_POST['productor']));
            $cultivo = trim($_POST['cultivo']);
            $variedad = trim($_POST['variedad']);
            $categoria_aprobada = trim($_POST['categoriaAprob']);
            $superficie_aprobada = trim($_POST['superficie']);
            $superficie_total = trim($_POST['superficietot']);
            $bolsaEtiqueta = trim($_POST['bolsaEtiqueta']);
            $sbtotal = empty($_POST['total1']) ? trim($_POST['total1']) : 0;
            #Datos necesarios para costo
            $inscripcion = trim($_POST['inscripcion']);
            $inspeccion_campo = trim($_POST['icampo']);
            $analisis_laboratorio_etiqueta = trim($_POST['anlet']);
            $acondicionamiento = trim($_POST['acondicionamiento']);
            $plantines = trim($_POST['plantines']);

            $total = !empty($_POST['total2']) ? trim($_POST['total2']) : 0;
            $fecha = trim($_POST['f_pago']);
            $monto_pagado = trim($_POST['montoPagado']);
            $saldoTotal = !empty($_POST['saldototal']) ? intval(trim($_POST['saldototal'])) : 0;
            $saldo_campania_ant = trim($_POST['campAnterior']);
            $id_cuenta = trim($_POST['icuenta']);

            $campanha = trim($_POST['campania']);

            $fecha = $temp -> cambiar_tipo_mysql($fecha);
            Cuenta::updateCuenta($responsable, $semillera, $campanha, $productor, $cultivo, $variedad, $categoria_aprobada, $superficie_aprobada, $superficie_total, $bolsaEtiqueta, $sbtotal, $inscripcion, $inspeccion_campo, $analisis_laboratorio_etiqueta, $acondicionamiento, $plantines, $total, $fecha, $monto_pagado, $saldoTotal, $saldo_campania_ant, $id_cuenta);
            if (DBConnector::filas())
                echo 'OK';
            else
                echo DBConnector::mensaje();
            break;

        //otras operaciones
        case 'ls_costos' :
            $nombre_cultivo = array();
            Costos::getNombreCostos();
            while ($ccultivo = DBConnector::objeto()) {
                array_push($nombre_cultivo, $ccultivo -> cultivo);
            }
            $json['cultivos'] = $nombre_cultivo;

            echo json_encode($json);
            break;
        case 'ls_costos_plantines' :
            $nombre_cultivo = array();
            $idPlantines = Cultivo::getIdCultivo('plantines');
            Costos::getNombreCostosPlantines($idPlantines);
            while ($ccultivo = DBConnector::objeto()) {
                array_push($nombre_cultivo, $ccultivo -> cultivo);
            }
            $json['cultivos'] = $nombre_cultivo;
            $json['total'] = count($nombre_cultivo);
            echo json_encode($json);
            break;
        case 'ls_variedad' :
            $variedades = array();
            $cultivo = $_GET['cultivo'];
            Semilla::getVariedadesCultivo($cultivo);
            while ($ccultivo = DBConnector::resultado()) {
                array_push($variedades, utf8_encode(ucfirst($ccultivo -> variedad)));
            }
            $json['variedades'] = $variedades;

            echo json_encode($json);
            break;
        case 'ls_catsembrada' :
            $id = array();
            $variedades = array();
            $categoria -> listarCategorias();
            while ($ccultivo = DBConnector::objeto()) {
                if ($ccultivo -> inicial == '0') {
                    $union = '';
                } else {
                    $union = '-'.$ccultivo->inicial;
                }
                array_push($id, $ccultivo -> id_generacion);
                if (strcasecmp($ccultivo -> categoria, 'Sin especificar'))
                    array_push($variedades, ucfirst($ccultivo -> categoria) . $union);
            }
            $json['id'] = $id;
            $json['csembrada'] = $variedades;

            echo json_encode($json);
            break;
        case 'ls_cproducir' :
            $id = array();
            $producir = array();
            $categoria_sem = $_GET['csembrada'];
            Categoria::list_CategoriaWithoutCat($categoria_sem);
            while ($ccultivo = DBConnector::objeto()) {
                if ($ccultivo -> generacion == 'Primera') {
                    $union = '1';
                } elseif ($ccultivo -> generacion == 'Segunda') {
                    $union = '2';
                } elseif ($ccultivo -> generacion == 'Tercera') {
                    $union = '3';
                } else {
                    $union = 'B';
                }
                array_push($id, $ccultivo -> id_generacion);
                if (strcasecmp($ccultivo -> categoria, 'Sin especificar'))
                    array_push($producir, ucfirst($ccultivo -> categoria) . '-' . $union);
            }
            $json['id'] = $id;
            $json['cat_free'] = $producir;

            echo json_encode($json);
            break;

        case 'saldo_anterior' :
            $reemplazo = new Funciones();
            $isolicitud = intval($_POST['isol']);
            $isemilla = intval($_POST['isem']);

            $semillera = $solicitudes->getSemilleraByIdSolicitudForCuenta($isolicitud);
            $nombre = $solicitudes -> getNombreByIdSolcitud($isolicitud);
            $apellido = $solicitudes -> getApellidoByIdSolcitud($isolicitud);
            $semilla -> getCultivoByIdSemillaForCuenta($isemilla);
            $campanha = $semilla->getCampaniaByIdSemillaForCuenta($isemilla);
            $cultivo = $semilla -> getCultivoByIdSemillaCertifica($isemilla);
            
            $saldo['saldo'] = 0.0;
            while($row = DBConnector::objeto()) {
                $saldo['saldo'] += Cuenta::getCostosByCultivo($semillera, $nombre, $apellido, $campanha, $cultivo);
            }

            echo json_encode($saldo);
            break;

        case 'nro' :
            //si nombre de la semillera no existe la variable es vacia
            $semillera = isset($_GET['sem']) ? trim($_GET['sem']) : '';
            $sistema = $_GET['sis'];
            #var_dump(!empty($semillera));
            if (!empty($semillera)) {
                $provincia = intval($_GET['prov']);
                $municipio = intval($_GET['mun']);
                //numero de solicitud de una semillera registrada
                Semillera::getListSemilleras($provincia, $municipio);
                if (DBConnector::filas() > 0) {
                    $obj = DBConnector::resultado();
                    $json['nro'] = $obj -> nro_solicitud;
                    $json['anho'] = date('y');
                    $json['estado'] = 'repetir';
                } else {

                }
            } else {
                $solicitudes -> view_nextNroSolicitudCertifica($sistema);
                $fila = DBConnector::objeto();
                $json['nro'] = $fila -> nro_solicitud + 1;
                $json['anho'] = date('y');
                $json['estado'] = 'nuevo';
            }

            echo json_encode($json);
            break;
        case 'nros' :
            //si nombre de la semillera no existe la variable es vacia
            $semillera = isset($_GET['sem']) ? intval($_GET['sem']) : '';
            $sistema = substr(trim($_GET['pro']), 0, 4);
            if (!empty($semillera)) {
                //numero de solicitud de una semillera registrada

                $solicitudes -> getNroSolicitudBySemillera($semillera, $sistema);
                $fila = DBConnector::objeto();
                $json['nro'] = $fila -> nro_solicitud;
                $json['anho'] = date('y');
                $json['estado'] = 'repetir';
            } else {
                //nuevo numero de solicitud
                Semillera::getNroSolicitud();
                $obj = DBConnector::objeto();
                $json['nro'] = $obj -> nro_solicitud + 1;
                $json['anho'] = date('y');
                $json['estado'] = 'nuevo';
            }
            echo json_encode($json);
            break;
        //devuelve el total de campos registrados
        case 'campo' :
            $semilla -> contarNroCampo();

            if (DBConnector::filas()) {
                $fila = DBConnector::objeto();
                $json['estado'] = 'OK';
                $json['nocampo'] = ($fila -> total + 1);
            } else {
                $json['nocampo'] = '100';
                $json['estado'] = 'error';
            }

            echo json_encode($json);
            break;
    }

} else {
    die('Access forbidden!');
}
?>