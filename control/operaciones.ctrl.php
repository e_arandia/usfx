<?php

$opcion = $_GET['pag'];
$coincidencias = array();
// lista de coincidencias
$list_semilleras = array();
// lista de semilleras
$list_campos = array();
//lista de nro campos que tienen resultados de analisis
switch ($opcion) {

    case 'lugar' :
        $lugar = trim($_GET['seccion']);
        $q = trim($_GET['q']);
        switch ($lugar) {
            case 'provincia' :
                Solicitud::getProvinciaByNombre($q);
                while ($fila = DBConnector::resultado())
                    array_push($coincidencias, $fila -> provincia);
                echo json_encode($coincidencias);
                break;
            case 'municipio' :
                Solicitud::getMunicipioByNombre($q);
                while ($fila = DBConnector::resultado())
                    array_push($coincidencias, $fila -> municipio);
                echo json_encode($coincidencias);
                break;

        }

        break;
    /**
     * nombre de todas las semilleras y  para  filtrar  segun la solicitud
     * */
    case 'semillera' :
        $tipo = $_GET['opt'];
        $semilleras_array = array();

        switch ($tipo) {
            case 'all' :
                $etapa = $_GET['tbl'];
                $std = isset($_GET['std']) ? trim($_GET['std']) : '';
                $solicitudes -> getSemillerasBySistema($etapa, $std);
                /*if (!DBConnector::filas())
                 $solicitudes -> getSemillerasBySistema('inspeccion', $std);*/
                //$solicitudes -> getSemillerasv2($etapa);
                while ($fila = DBConnector::resultado()) {
                    if ($fila -> semillera != '' or $fila -> semillera != 'null')
                        array_push($list_semilleras, $fila -> semillera);
                }
                if (DBConnector::filas() > 0)
                    echo json_encode($list_semilleras);
                else
                    echo json_encode('E');
                break;
            
            case 'superficie' :
                $std = (isset($_GET['std']) ? trim($_GET['std']) : '');
                if (empty($std))
                    $solicitudes -> getAllSemilleras2();
                #semilleras que tienene papa
                else
                    $solicitudes -> getAllSemilleras2($std);

                if (DBConector::filas()) {
                    while ($fila = DBConnector::resultado()) {
                        $json[$fila -> semillera] = $fila -> semillera;
                    }
                } else
                    $json[''] = '';
                echo json_encode($json);
                break;
        }
        break;
    /**
     * nombre de semillaristas por semillera
     * */
    case 'productor' :
        $semillera = str_replace('*', ' ', $_GET['semillera']);
        $estado[1] = intval($_GET['opt']);
        $estado[2] = ($_GET['nombre'] == -1) ? -1 : $_GET['nombre'];
        $json = array();
        if ($estado[2] == -1) {

            $solicitudes -> getAllProductores($semillera, $link, $estado);
            while ($fila = DBConnector::resultado()) {
                $nombre = utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                array_push($json, $nombre);
            }
        } else {
            $solicitudes -> getAllProductores($semillera, $estado, 'especifico');

            if (DBConnector::filas() > 0) {
                while ($fila = DBConnector::resultado()) {
                    $nombre = utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                    array_push($json, $nombre);
                }
            } else {
                $solicitudes -> getAllProductores2($semillera);
                while ($fila = DBConnector::resultado()) {
                    $nombre = utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                    array_push($json, $nombre);
                }
            }
        }
        echo json_encode($json);
        break;

    case 'productor_papa' :
        $opcion = $_GET['tipo'];
        $semillera = str_replace('*', ' ', $_GET['semillera']);
        $estado[1] = isset($_GET['started']) ? intval($_GET['started']) : 2;
        $estado[2] = isset($_GET['ended']) ? intval($_GET['ended']) : 4;
        $estado[3] = 8;
        $json = array();
        if ($opcion == -1) {
            //devolver semilleras q no tengan papa
            $productores = $solicitudes -> getAllProductores2($semillera, $link);
            while ($fila = mysql_fetch_object($productores)) {
                //echo utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                $nombre = utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                array_push($json, $nombre);
            }
        } else {
            //semilleras q tengan papa
            $productores = $solicitudes -> getAllProductores($semillera, $link, $estado, 'papa');
            //echo mysql_num_rows($productores);
            while ($fila = mysql_fetch_object($productores)) {
                //echo utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                $nombre = utf8_encode($fila -> nombre) . " " . utf8_encode($fila -> apellido);
                array_push($json, $nombre);
            }
        }
        echo json_encode($json);
        break;
    /**
     * cuenta el nro de solicitudes realizadas por un semillarista que ya registro la semilla sembrada
     *
     * REVISAR
     * */
    case 'contar_solicitudes' :
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $semillera = $_GET['semillera'];
        $nro_solicitudes = Solicitud::cantidadSolicitudes($nombre, $apellido, $semillera, $link);
        if ($nro_solicitudes[0] < 1) {
            $nro_de_solicitudes = json_encode($nro_solicitudes);
        } else {
            $nro_de_solicitudes = '';
        }
        break;
    /**
     * iproductor   id de la solicitud
     * isuperficie  id de la superficie
     * $estado      int   (0-10)   0:solicitud,1:semilla,2-4:superficie,5-7:inspeccion,
     *                             8:cosecha9:semillaproducida10:laboratorio
     * */

    /**
     * cultivo de un semillerista
     * */

    case 'cultivos' :
        $semilla::getAllCultivos();

        $json = array();
        while ($fila = DBConnector::resultado()) {
            array_push($json, $fila -> cultivo);
        }
        echo json_encode($json);
        break;
    /**
     * verifica si el nombre de un cultivo ya existe
     * */
    case 'nombrecultivo' :
        $cultivo = trim($_GET['cultivo']);

        $cultivos -> getNombreCultivo($cultivo);
        if (DBConnector::filas() == 0)
            $json['estado'] = 'libre';
        else
            $json['estado'] = 'existe';
        echo json_encode($json);
        break;
    /**
     * nro de campo de un semillerista
     *
     * opt=actualizacion
     * opt=ingreso
     * opt=-              desde superficie.html.php
     * */
    case 'nocampo' :
        //numeros de campos segun id de semillerista
        $id = intval($_GET['id']);
        $semilla -> getNroCamposProductor($id);
        $campos_array = array();
        while ($fila = DBConnector::resultado())
            array_push($campos_array, $fila -> nro_campo);

        $json['nroCampo'] = $campos_array;
        echo json_encode($json);
        break;
    case 'campo' :
        $opcion = isset($_GET['opt']) ? $_GET['opt'] : '-';
        $campos = array();
        switch ($opcion) {
            case 'actualizacion' :
                //nros de campos que necesitan actualizacion de para una nueva gestion

                $laboratorio -> nroCampoResultado();
                while ($fila = DBConnector::resultado()) {
                    array_push($campos, $fila -> nro_campo);
                }
                $json['nroCampo'] = $campos;

                break;
            case 'ingreso' :
                $laboratorio -> nroCampos();
                while ($fila = DBConnector::resultado()) {
                    array_push($campos, $fila -> nro_campo);
                }
                $json['nroCampo'] = $campos;
                break;

            case 'campos' :

                //numeros de campos segun nombre apellido y cultivo
                if (isset($_GET['cultivo']) && isset($_GET['apellido']) && isset($_GET['nombre'])) {
                    //campos de un cultivo pertenecientes a un semillerista para obtener resultados de laboratorio
                    $apellido = $dates -> modificar($_GET['apellido']);
                    $nombre = $dates -> modificar($_GET['nombre']);

                    $cultivo = $_GET['cultivo'];

                    $iSolicitud = $solicitudes::getIdSolicitudProductorOperaciones($nombre, $apellido);
                    $semilla -> getCampo($iSolicitud, $cultivo);
                    $json2 = array();
                    while ($fila = DBConnector::resultado()) {
                        array_push($json2, $fila -> nro_campo);
                        //array_push($json,$fila->nro_campo);
                    }
                    $json['nroCampo'] = $json2;
                } else {
                    //numeros de campos segun id de semillerista
                    $id = intval($_GET['id']);
                    $semilla -> getNroCamposProductor($id);
                    $campos_array = array();
                    if (DBConnector::filas() > 1) {
                        while ($fila = DBConnector::resultado())
                            array_push($campos_array, $fila -> nro_campo);
                    } else {
                        while ($fila = DBConnector::resultado())
                            array_push($campos_array, $fila -> nro_campo);
                    }
                    $json['nroCampo'] = $campos_array;
                }
                break;
        }
        echo json_encode($json);
        break;

    /**
     * muestra el id de la superficie fiscalizada
     * */
    case 'dCosechaFiscal' :
        $isolicitud = intval($_GET['isol']);

        $solicitudes -> getIdSemillaByIdSol($isolicitud);
        $row = DBConnector::resultado();

        $isemilla = $row -> id_semilla;

        $superficieID = $superficie -> getSuperficieByIdSolSem($isolicitud, $isemilla);
        $cosechas -> getIdCosechaByIdSol($superficieID);

        $row = DBConnector::resultado();
        $json['isuperficie'] = $superficieID;
        $json['icosecha'] = $row -> id_cosecha;
        echo json_encode($json);

        break;
    case 'isuperficie' :
        $nombre = isset($_GET['nombre']) ? $dates -> modificar($_GET['nombre']) : '';
        $apellido = isset($_GET['apellido']) ? $dates -> modificar($_GET['apellido']) : '';
        $id_productor = intval($_GET['isolicitud']);
        $surface = new Superficie();
        $superficieID = $surface -> getSuperficieID($nombre, $apellido, $link, $id_productor);
        while ($fila = mysql_fetch_object($superficieID)) {
            $superficie['isuperficie'] = $fila -> id_superficie;
        }
        echo json_encode($superficie);
    /**
     * muestra los cultivos q pasaron por el laboratorio y cultivos de papa para semilla producida
     * */
    case 'campos' :
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $semillera = $_GET['semillera'];
        #$semillera = (isset($_GET['semillera']) || empty($_GET['semillera'])) ? '' : trim($_GET['semillera']);
        $cosechas -> cultivoPapa($nombre, $apellido, $semillera);
        if (!DBConnector::nroError() && DBConnector::filas()) {
            $papaArray = array();
            while ($fila = DBConnector::resultado()) {
                array_push($papaArray, $fila -> nro_campo);
            }
            $datosCosecha['papa'] = $papaArray;
        } else {
            $datosCosecha['papa'] = '';
        }
        $lab = $laboratorio -> nroCampos();
        if (!DBConnector::nroError()) {
            $otrosArray = array();
            while ($fila = DBConnector::resultado()) {
                array_push($otrosArray, $fila -> nro_campo);
            }
            $datosCosecha['otros'] = $otrosArray;
        } else {
            $datosCosecha['otros'] = '';
        }

        echo json_encode($datosCosecha);
        break;

    /**
     * carga todas las categorias
     * **/
    case 'categoria' :
        $categoria -> getCategorias();

        while ($fila = DBConnector::resultado()) {
            $datosCosecha[$fila -> id_categoria] = $fila -> nombre_categoria;
        }
        echo json_encode($datosCosecha);

        break;
    /**
     * carga las generaciones segun la categoria
     * */
    case 'generacion' :
        $nroCategoria = $_GET['opt'];
        $generacion -> getGeneraciones($nroCategoria);

        while ($fila = DBConnector::resultado()) {
            $datosCosecha[$fila -> id_generacion] = $fila -> nombre_generacion;
        }
        echo json_encode($datosCosecha);

        break;
    /**
     * carga el id de una cosecha
     * */
    case 'icosecha' :
        $nro_campo = $_GET['nroCampo'];

        $cosechas -> hojaCosechaID2($nro_campo);
        $values = array();
        while ($fila = DBConnector::resultado()) {
            array_push($values, $fila -> id_cosecha);
        }
        echo json_encode($values);
        break;

    /**
     * devuelve el id de cosecha y el id de laboratorio de un cultivo
     * */
    case 'semillap' :
        $nro_campo = $_GET['campo'];
        $lab = new Laboratorio();
        // cultivo q existe en el campo
        $cosechas -> cultivo_Campo($nro_campo);
        $nombreCultivo = '';
        while ($fila = DBConnector::resultado()) {
            $nombreCultivo = $fila -> cultivo;
        }
        #echo $nombreCultivo; exit;
        //verifico si el cultivo es papa
        if ($nombreCultivo == 'papa')
            $datos['laboratorio'] = 5;
        else {
            //id de laboratorio
            $lab -> getIdLaboratorio($nro_campo);
            if (!DBConnector::nroError() && DBConnector::filas()) {
                while ($temp = DBConnector::resultado()) {
                    $temp1 = $temp -> id_laboratorio;
                }
                $datos['laboratorio'] = $temp1;
            }
        }
        //consulta para tener el id de hoja de cosecha
        $cosechas -> getHojaCosechaId($nro_campo);
        if (!DBConnector::nroError()) {
            while ($temp = DBConnector::resultado()) {
                $temp2 = $temp -> id_cosecha;
            }
            $datos['cosecha'] = $temp2;
        }

        echo json_encode($datos);
        break;


    case 'variedad' :
        /**opcion para completar en formulario de cuenta*/
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $campania = $_GET['opt'];
        $cultivo = $_GET['cultivo'];
        $json = array();
        $semilla -> getVariedadCultivo($nombre, $apellido, $cultivo, $campania);
        while ($fila = DBConnector::resultado())
        #echo utf8_encode($fila -> variedad); exit;
            array_push($json, utf8_decode($fila -> variedad));
        echo json_encode($json);
        break;

    case 'sup_aprobada' :
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $cultivo = $_GET['cultivo'];
        $campania = $_GET['campania'];
        $variedad = $_GET['variedad'];
        $catAprobada = $_GET['catAprobada'];

        $supAprobada = $superficie -> getSupAprobada($nombre, $apellido, $cultivo, $variedad, $campania, $catAprobada);
        $fila = mysql_fetch_object($supAprobada);
        $json['aprobada'] = $fila -> aprobada;
        echo json_encode($json);
        break;
    /** suma todas las superficies de 1 cultivo de una determinada variedad    y
     *  en determinada campania  */
    case 'sup_total' :
        $cultivo = $_GET['cultivo'];
        $campania = $_GET['campania'];
        $variedad = $_GET['variedad'];
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $solicitud = new Solicitud;
        $idSolicitudProductor = $solicitud::getIdSolicitudProductorOperaciones($nombre, $apellido);

        $superficieTotal0 = Semilla::getSuperficieTotal($idSolicitudProductor, $cultivo, $variedad, $campania);
        $row = DBConnector::resultado();

        /** ? habilitar para mostrar total de superficie rechazada de un cultivo */
        // ?if ($row -> numbers > 0 && $row->total !=0 ) {
        $json['total'] = $row -> total;
        /* ?	} else { //echo 'segundo calculo';
         $superficieTotal = Semilla::getSuperficieTotal($idSolicitudProductor, $cultivo, $variedad, $campania, $link,'rechaza');
         $row = mysql_fetch_object($superficieTotal);
         $json['total'] = $row -> total;
         } ?*/
        echo json_encode($json);
        break;
    /**
     * campos que tienen laboratorio
     * */
    case 'campoLab' :
        $campos = HojaCosecha::analisis();
        $json = array();
        while ($fila = DBConnector::resultado()) {
            array_push($json, $fila -> nro_campo);
        }
        echo json_encode($json);
        break;
    /**id solicitud segun el nro de campo**/
    case 'isolnroCampo' :
        $nro_campo = $_GET['campo'];
        Semilla::idSolicitud($nro_campo);
        $json = array();
        while ($fila = DBConnector::resultado()) {
            array_push($json, $fila -> id_solicitud);
        }
        echo json_encode($json);
        break;
    /** determinar la etapa en la que se encuentra parte 1*/
    case 'etapa' :
        /*$search = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü', 'ü', 'ä', 'Ä', 'Ë', 'ë', 'ï', 'Ï', 'Ö', 'ö');
         $nombre = str_ireplace($search, '_', $_GET['nombre']);
         $apellido = str_ireplace($search, '_', $_GET['apellido']);
         $semillera = $_GET['semillera'];*/
        $isemilla = $_GET['isemilla'];
        $isemillerista = isset($_GET['isemillerista']) ? $_GET['isemillerista'] : '';
        $json = array();
        Solicitud::estado($isemilla, $isemillerista);
        //si el cultivo es papa  obtengo en que estado se encuentra
        #echo DBConnector::filas();
        if (DBConnector::filas() > 0) {
            while ($fila = DBConnector::resultado()) {
                $json['estado'] = $fila -> laboratorio;
                $json['esuperficie'] = $fila -> estado;
                $json['iproductor'] = $fila -> id_solicitud;
                $json['cultivo'] = $fila -> cultivo;
                $json['inscrita'] = $fila -> inscrita;
                $json['rechazada'] = $fila -> rechazada;
                $json['retirada'] = $fila -> retirada;
                $json['aprobada'] = ($fila -> rechazada == 0.0 && $fila -> retirada == 00) ? $fila -> inscrita : $fila -> aprobada;
                $json['inspeccion1'] = empty($fila -> primera) ? '' : $fila -> primera;
            }
        } else {//en caso que no sea papa obtengo su estado
            Solicitud::estado_2($isemilla, $isemillerista);
            #echo DBConnector::filas().' noPAPA';
            if (DBCOnnector::filas() > 0) {
                while ($fila = DBConnector::resultado()) {
                    $json['estado'] = $fila -> laboratorio;
                    $json['esuperficie'] = 0;
                    $json['iproductor'] = $fila -> id_solicitud;
                    $json['cultivo'] = $fila -> cultivo;
                    $json['inscrita'] = $fila -> inscrita;
                    $json['rechazada'] = $fila -> rechazada;
                    $json['retirada'] = $fila -> retirada;
                    $json['aprobada'] = $fila -> aprobada;
                    $json['inspeccion1'] = empty($fila -> primera) ? '' : $fila -> primera;
                }
            } else {
                /**estado cuando no se realizo ninguna inspeccion*/
                Solicitud::estado_2a($isemilla, $isemillerista);
                //echo 'sin inspeccion';
                while ($fila = DBConnector::resultado()) {
                    //echo $fila->inscrita;
                    $json['estado'] = $fila -> laboratorio;
                    $json['esuperficie'] = 0;
                    $json['iproductor'] = $fila -> id_solicitud;
                    $json['cultivo'] = $fila -> cultivo;
                    $json['inscrita'] = $fila -> inscrita;
                    $json['rechazada'] = $fila -> rechazada;
                    $json['retirada'] = $fila -> retirada;
                    $json['aprobada'] = $fila -> aprobada;
                }
            }
        }
        echo json_encode($json);
        break;

    case 'comprobarLogin' :
        $usuario = $_GET['usuario'];
        Usuario::getNombreUsuarios($usuario);


        if (DBConnector::filas())
            $result['uso'] = 'ocupado';
        else
            $result['uso'] = 'libre';
        echo json_encode($result);
        break;

    case 'productoresFiscal' :
        $tabla = $_GET['tabla'];
        $semillera = $_GET['semillera'];
        $piezas = explode('*', $semillera);
        $semillera = implode(' ', $piezas);
        $opt = isset($_GET['tp']) ? $_GET['tp'] : '';
        //solo para cosecha
        $dates -> noCache();
        if (empty($opt))
            Solicitud::getProductoresSemillera($semillera, $tabla);
        else
            Solicitud::getProductoresSemillera($semillera, $tabla, $opt);
        if (!DBConnector::filas())
            Solicitud::getProductoresSemillera($semillera, 'inspeccion');
        $json = array();
        if (DBConnector::filas() >= 1) {
            while ($fila = DBConnector::resultado()) {
                array_push($json, utf8_encode($fila -> nombre) . ' ' . utf8_encode($fila -> apellido));
            }
        }
        echo json_encode($json);
        break;
    /*
     * lista todas las semilleras
     */
    case 'lista_semilleras' :
        $etapa = $_GET['tbl'];
        $solicitud = new Solicitud;
        $json = array();
        $semilleras = $solicitud -> getSemillerasv2($etapa, $link);
        while ($fila = mysql_fetch_object($semilleras)) {
            array_push($json, $fila -> semillera);
        }
        echo json_encode($json);
        break;

    case 'saldo_campanha_anterior' :
        $nombre = $dates -> modificar($_GET['nombre']);
        $apellido = $dates -> modificar($_GET['apellido']);
        $semillera = $_GET['semillera'];
        Cuenta::getSaldoAnterior($nombre, $apellido, $semillera);

        if (!DBConnector::nroError()) {
            $row = DBConnector::resultado();
            $saldo = $row -> saldo_anterior;
        } else {
            $saldo = 0;
        }
        echo json_encode($saldo);
        break;


    /**id de la solicitud segun el nombre y apellido*/
    case 'semillaID' :
        $isol = intval($_GET['stm']);

        $solicitudes -> getIdSemillaByIdSol($isol);

        $row = DBConnector::resultado();
        $json['isemilla'] = $row -> id_semilla;

        echo json_encode($json);
        break;

    /**id de la hoja de cosecha segun el id solcitud*/
    case 'cosechaID' :
        $isol = intval($_GET['id']);

        $cosechas -> getIdCosechaByIdSol($isol);

        $row = DBConnector::resultado();
        $json['icosecha'] = $row -> id_cosecha;

        echo json_encode($json);
        break;


    /*	case 'isuperficiecampo':
     $search = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü', 'ü', 'ä', 'Ä', 'Ë', 'ë', 'ï', 'Ï', 'Ö', 'ö');
     $nombre = str_ireplace($search, '_', $_GET['nombre']);
     $apellido = str_ireplace($search, '_', $_GET['apellido']);
     $nrocampo = $_GET['nro_campo'];
     $isuperficie = Superficie::getSuperficieBySemillerista($nombre,$apellido,$nrocampo,$link);
     $resultado['isuperficie'] = $isuperficie;
     echo json_encode($resultado);
     break;*/

    /**nros de campos que ingresaron a laboratorio*/
    case 'labCampos' :
        $laboratorio -> nroCampoResultado();
        while ($fila = DBConnector::resultado())
            array_push($list_campos, $fila -> nro_campo);
        echo json_encode($list_campos);

        break;

    /*realizar operaciones necesarias para el grafico**/
    case 'graficar' :
        session_start();
        $tipo = $_GET['tipo'];
        //tipo de grafico a realizar
        $cultivo = $_GET['cultivo'];
        //cultivo en el grafico
        $grafico = $_GET['grafico'];
        //tipo de grafico
        $forma = $_GET['forma'];
        require_once '../libs/jpgraph/src/themes/GreenTheme.class.php';

        switch ($forma) {
            case 'barra' :
                require_once '../libs/jpgraph/libs/jpgraph_bar.php';
                break;

            case 'torta' :
                require_once '../libs/jpgraph/libs/jpgraph_pie.php';
                break;

            case 'lineal' :
                require_once '../libs/jpgraph/libs/jpgraph_line.php';
                $datay1 = array(20,15,23,15,80,20,45,10,5,45,60);
        
        // Setup the graph
        $grafico = new Graph(900, 350);
        $grafico -> SetScale("textlin");

        $theme_class = new GreenTheme();

        $grafico -> SetTheme($theme_class);
        $grafico -> img -> SetAntiAliasing(false);
        $titulo = ($tipo == 'culsuper')?'Superficie cultivo de '.$cultivo:'Categoria cultivo de '.$cultivo;
        $grafico -> title -> Set($titulo);
        $grafico -> SetBox(false);

        $grafico -> img -> SetAntiAliasing();

        $grafico -> yaxis -> HideZeroLabel();
        $grafico -> yaxis -> HideLine(false);
        $grafico -> yaxis -> HideTicks(false, false);

        $grafico -> xgrid -> Show();
        $grafico -> xgrid -> SetLineStyle("solid");
        $grafico -> xaxis -> SetTickLabels(array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Nov', 'Oct', 'Dic'));
        $grafico -> xgrid -> SetColor('#E3E3E3');

        // Create the first line
        $p1 = new LinePlot($datay1);
        $grafico -> Add($p1);
        $p1 -> SetColor("#6495ED");
        $p1 -> SetLegend('Tienda 1');

        $grafico -> legend -> SetFrameWeight(1);
        $grafico->Stroke($_SERVER['DOCUMENT_ROOT'] ."/" .$_SESSION['home'] ."/images/graphs/123.png");
                break;
        }
        
        
        
        
        include '../vista/reporte/ver_grafico.html.php';
        break;
}
?>