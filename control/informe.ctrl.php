<?php

// Tiempo ilimitado para el script
set_time_limit(0);

//include '../config/Setting.php';
//libreria necesaria para excel
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/Writer/Excel2007.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/Reader/Excel2007.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/IOFactory.php";
//libreria para PDF
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/fpdf/fpdf.php";
//libreria para grafico
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph.php';

$opcion = (isset($_REQUEST['opt'])) ? strip_tags($_REQUEST['opt']) : '';
$pagina = (isset($_REQUEST['pag'])) ? strip_tags($_REQUEST['pag']) : '';
switch ($opcion) {
    case 'detalle' :
        switch ($pagina) {
            case 'xls' :
                $sistema = substr($_REQUEST['sistema'], 0, 7);
                $tipo = strip_tags($_REQUEST['tipo']);
                $gestion = date('Y');
                if ($tipo != 'trim') {
                    $desde = (!empty($_REQUEST['desde'])) ? $_REQUEST['desde'] : $dates -> cambiar_tipo_mysql(date('Y') . '-01-01');
                    $hasta = (!empty($_REQUEST['hasta'])) ? $_REQUEST['hasta'] : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                $area = Sistema::getIdSistema($sistema);

                $json['isolicitudes'] = $json['provincias'] = array();
                $json['municipios'] = $json['comunidades'] = array();
                $json['semilleras'] = $json['semilleristas'] = array();
                $json['cultivos'] = $json['variedades'] = array();
                $json['categorias'] = $json['superficies'] = array();
                $json['volumenes'] = array();

                Solicitud::getListSolicitudesByFechaForDetalleResumen($desde, $hasta, $area, $gestion);
                #var_dump(DBConnector::objeto());
                while ($obj = DBConnector::objeto()) {
                    array_push($json['isolicitudes'], $obj -> id_solicitud);
                    array_push($json['provincias'], $obj -> provincia);
                    array_push($json['municipios'], $obj -> municipio);
                    array_push($json['comunidades'], addslashes($obj -> comunidad));
                    array_push($json['semilleras'], $obj -> semillera);
                    array_push($json['semilleristas'], utf8_encode($obj -> semillerista));
                }
                //lista de cultivos
                foreach ($json['isolicitudes'] as $key => $isolicitud) {
                    $cultivo = Semilla::getCultivoByIdSemillaForCuenta(Tiene::getIdSemilla($isolicitud));
                    array_push($json['cultivos'], $cultivo);
                }
                //lista de variedades
                foreach ($json['isolicitudes'] as $key => $isolicitud) {
                    $variedad = $semilla -> getVariedadByIdSemillaForCuenta(Tiene::getIdSemilla($isolicitud));
                    array_push($json['variedades'], $variedad);
                }
                //lista de categorias
                foreach ($json['cultivos'] as $key => $cultivo) {
                    $semilla -> getListCategoriaCampo($cultivo, $sistema, $gestion, $desde, $hasta);
                    while ($row = DBConnector::objeto()) {
                        array_push($json['categorias'], $row -> categoria_producir);
                    }

                }
                //lista de superficie de cultivo
                foreach ($json['cultivos'] as $key => $cultivo) {
                    Superficie::getSuperficieAprobadaForDetalleResumen(Tiene::getIdSemilla($json['isolicitudes'][$key]));
                    $obj6 = DBConnector::objeto();

                    if (!empty($obj6 -> aprobada)) {
                        array_push($json['superficies'], $obj6 -> aprobada);
                    } else {
                        array_push($json['superficies'], '0.00');
                    }
                }
                //detalle volumen de produccion
                foreach ($json['isolicitudes'] as $key => $isolicitud) {
                    $volumen = HojaCosecha::getRendimientoCampoForDetalleResumen(Tiene::getIdSemilla($isolicitud));
                    array_push($json['volumenes'], $volumen);
                }
                #on_encode($json);
                include '../vista/certificacion/info_detallada.php';
                break;

            case 'pdf' :
                break;

                break;
        }
        break;
    case 'new' :
        switch ($pagina) {
            case 'resumen' :
                session_start();
                include '../vista/certificacion/resumen.html.php';
                break;
            case 'resumenFiscal' :
                session_start();
                include '../vista/fiscalizacion/resumen.html.php';
                break;
            case 'info_detallada' :
                include '../vista/certificacion/info_detallada.html.php';
                break;
        }

        break;
    case 'xls' :
        switch ($pagina) {
            case 'certifica' :
                $sistema = strip_tags($_REQUEST['pag']);
                $tipo = strip_tags($_REQUEST['tipo']);
                $gestion = date('Y');
                if ($tipo != 'trim') {
                    $desde = (!empty($_REQUEST['desde'])) ? $_REQUEST['desde'] : $dates -> cambiar_tipo_mysql(date('Y') . '-01-01');
                    $hasta = (!empty($_REQUEST['hasta'])) ? $_REQUEST['hasta'] : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                //MODIFICAR ESTADO
                $total_semilleras = Solicitud::getSemillerasTotalByTime($desde, $hasta, $gestion);

                if ($total_semilleras > 0) {
                    $total_solicitudes = Solicitud::getTotalSolicitudes($desde, $hasta, $gestion);
                    $total_finalizadas = Solicitud::getTotalSolicitudesTerminadas($desde, $hasta, $gestion);
                    $total_pendientes = $total_solicitudes - $total_finalizadas;
                    $area = Sistema::getIdSistema($sistema);
                    Solicitud::getListMunicipiosByTime($area, $desde, $hasta, $gestion);
                    $lst_municipios = array();
                    #lista de municipios
                    while ($municipios = DBConnector::objeto()) {
                        array_push($lst_municipios, $municipios -> municipio);
                    }
                    #numero de comunidades
                    $totalComunidades = Solicitud::getListComunidadesByTime($sistema, $desde, $hasta, $gestion);
                    #numero de semilleristas
                    $totalProductores = Solicitud::getProductoresByTime($desde, $hasta, 'contar', $area, $gestion);
                    //lista de semilleras
                    Solicitud::getSemillerasByTime($desde, $hasta, $gestion);
                    $lst_semilleras = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($lst_semilleras, utf8_encode($row -> semillera));
                    }

                    //contar semilleras particulares y agregar a la lista se semilleras
                    //cambiar estado en semillerastotalparticulares
                    $total_particulares = Solicitud::getSemillerasTotalParticularesByTime($desde, $hasta, $gestion, 1);
                    if ($total_particulares > 0) {
                        array_push($lst_semilleras, 'PARTICULARES (' . $total_particulares . ')');
                    }

                    //ordenar semilleras
                    sort($lst_semilleras);
                    //lista de cultivos
                    Semilla::getListCultivosByTime($desde, $hasta, $area, $gestion);
                    $lst_cultivos = array();
                    $lst_no_cultivos = array('Acondicionamiento', 'Plantines', 'Barbecho', 'Descanso');
                    while ($row = DBConnector::objeto()) {
                        if (!in_array($row -> cultivo, $lst_no_cultivos)) {
                            if (strlen($row -> cultivo))
                                array_push($lst_cultivos, $row -> cultivo);
                        }
                    }
                    //lista de cada variedad de un cultivo
                    $lst_variedades = array();
                    //total de variedades
                    $lst_total_variedades = array();
                    foreach ($lst_cultivos as $key => $cultivo) {
                        if (strlen($cultivo)) {
                            $semilla -> getVariedadesCultivo($cultivo, $gestion, $desde, $hasta);
                            if (DBConnector::filas()) {
                                while ($row = DBConnector::objeto()) {
                                    array_push($lst_variedades, $row -> variedad);
                                }
                                array_push($lst_total_variedades, DBConnector::filas());
                            }
                        }
                    }

                    $muestras['aprobadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 1);
                    $muestras['rechazadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 2);
                    $muestras['fiscalizadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 3);
                    $muestras['total'] = $muestras['aprobadas'] + $muestras['rechazadas'] + $muestras['fiscalizadas'];

                    /////////////////////////////////////////////////////////////////////////////////////////////
                    include '../modelo/reporte/xlsx/resumenCertificacion.php';
                    echo 'vista/reportes/' . $filename;

                } else {
                    echo 'error';
                }
                break;
            #terminar
            case 'fiscaliza' :
                $desde = (strip_tags($_REQUEST['desde'])) ? strip_tags($_REQUEST['desde']) : '01-01-' . date('Y');
                $hasta = (strip_tags($_REQUEST['hasta'])) ? strip_tags($_REQUEST['hasta']) : '31-12-' . date('Y');
                $cultivo = strip_tags($_REQUEST['cultivo']);
                $area = 2;
                #cargar plantilla
                $plantilla = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . TEMPLATE . "/resumenFiscal.xlsx";

                Solicitud::getSemillerasByCultivoAndTime($desde, $hasta, $cultivo, $area);
                if (DBConnector::filas()) {

                    #echo 'ListSemilleras>>'.DBConnector::mensaje().'<br>';
                    $semilleras = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($semilleras, $row -> semillera);
                    }
                    $objReader = PHPExcel_IOFactory::createReader("Excel2007");
                    $objPHPExcel = $objReader -> load($plantilla);
                }
                break;
            /**
             * funciona 14-04-2017
             * mejorado 16-04-2017
             * */
            case 'produccion_costos' :
                $cultivo = strip_tags($_REQUEST['cultivo']);
                $area = intval($_REQUEST['area']);
                $tipo = strip_tags($_REQUEST['tipo']);
                if ($tipo != 'trim') {
                    $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : date('Y') . '-01-01';
                    $hasta = (!empty($_REQUEST['hasta'])) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                //lista de semilleras
                $lst_semillera = array();
                $reporte -> semillerasDetalleProduccionCostos($desde, $hasta, $cultivo);
                if (DBConnector::filas() > 0) {
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_semillera, utf8_encode($obj -> semillera));
                    }
                    $lst_fecha = array();
                    $lst_comunidad = array();
                    $lst_semillerista = array();
                    $lst_cultivo = array();
                    $lst_variedad = array();
                    $lst_cat_sembrada = array();
                    $lst_superficie = array();
                    $lst_nocampo = array();
                    $lst_aprobada = array();
                    $lst_rechazada = array();
                    $lst_cat_aprobada = array();
                    $lst_nobolsa = array();
                    $lst_inscripcion = array();
                    $lst_campo = array();
                    $lst_etiqueta = array();
                    $lst_total = array();
                    //costos de produccion de semillera
                    foreach ($lst_semillera as $semillera) {
                        //array que contienen los datos individuales de cada campo
                        $reporte -> getDetalleProduccionBySemillera($semillera, $cultivo, $desde, $hasta);
                        $lst_total_semilleristas = DBConnector::filas();
                        while ($obj = DBConnector::objeto()) {
                            array_push($lst_fecha, $dates -> cambiar_formato_fecha($obj -> fecha));
                            array_push($lst_comunidad, utf8_encode($obj -> comunidad));
                            array_push($lst_semillerista, utf8_encode($obj -> semillerista));
                            array_push($lst_cultivo, utf8_encode($obj -> cultivo));
                            array_push($lst_variedad, utf8_encode($obj -> variedad));
                            array_push($lst_cat_sembrada, $obj -> cat_sembrada);
                            array_push($lst_superficie, floatval($obj -> superficie));
                            array_push($lst_nocampo, $obj -> nro_campo);
                            array_push($lst_aprobada, floatval($obj -> aprobada));
                            array_push($lst_rechazada, floatval($obj -> rechazada));
                            array_push($lst_cat_aprobada, $obj -> cat_aprobada);
                            array_push($lst_nobolsa, 5);
                            array_push($lst_inscripcion, floatval($obj -> inscripcion));
                            array_push($lst_campo, floatval($obj -> campo));
                            array_push($lst_etiqueta, 90);
                            #floatval($obj -> analisis_etiqueta));
                            array_push($lst_total, floatval($obj -> total));
                        }
                    }
                    #cargar plantilla
                    include '../modelo/reporte/xlsx/detalleCertificacion.php';
                    $json['file'] = 'vista/reportes/' . $filename;
                    $json['msg'] = 'OK';
                } else {
                    $json['msg'] = "No existen semilleras que hayan cultivado $cultivo";
                }
                echo json_encode($json);

                break;
            //mejorado 16-04-2017
            case 'sp_produccion' :
                $tipo = strip_tags($_REQUEST['tipo']);
                if ($tipo != 'trim') {
                    $desde = $dates -> cambiar_tipo_mysql($_REQUEST['desde']);
                    $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                $superficies_cultivos = $reporte -> getTotalSuperficieProduccionCultivos($desde, $hasta, date('Y'));
                if ($superficies_cultivos > 0) {
                    $lst_cultivos = array();
                    $gestion = date('Y');
                    $reporte -> getListCultivosSuperficieProduccion($desde, $hasta, $gestion);
                    #lista de cultivos
                    while ($obj = DBConnector::objeto()) {
                        if (strlen($obj -> cultivo))
                            array_push($lst_cultivos, $obj -> cultivo);
                    }
                    $superficies['inscrita'] = array();
                    $superficies['retirada'] = array();
                    $superficies['rechazada'] = array();
                    $superficies['aprobada'] = array();
                    foreach ($lst_cultivos as $key => $nombre) {
                        $reporte -> getSuperficieProduccionCultivos($nombre, $desde, $hasta);
                        #datos de un cultivo determinado
                        while ($row = DBConnector::objeto()) {
                            array_push($superficies['inscrita'], $row -> inscrita);
                            array_push($superficies['retirada'], $row -> retirada);
                            array_push($superficies['rechazada'], $row -> rechazada);
                            array_push($superficies['aprobada'], $row -> aprobada);
                        }
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////////////  EXCEL
                    include '../modelo/reporte/xlsx/superficie_cultivo.php';
                    $json['xls'] = 'vista/reportes/' . $filename;

                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }
                echo json_encode($json);
                break;
            /**
             * funciona 16-04-2017
             * solo opcion trimestral
             * */
            case 'vp_cultivo' :
                $tipo = strip_tags($_REQUEST['tipo']);
                if ($tipo != 'trim') {
                    $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : date('Y') . '-01-01';
                    $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                $filas_volumen_produccion = HojaCosecha::getTotalVolumenProduccion($desde, $hasta);
                if ($filas_volumen_produccion) {
                    #if (TRUE) {
                    $lst_cultivos = array();
                    //lista de cultivos
                    $lst_variedades = array();
                    //lista de variedades
                    $lst_variedades_cultivo = array();
                    //total de variedades de un cultivo en fecha especificada
                    $lst_categorias = array('basica', 'registrada', 'certificada', 'certificada-B', 'fiscalizada');
                    $lst_volumen_produccion_categorias = array();
                    //contiene el total de de la categoria de un cultivo y variedad
                    Semilla::getListCultivosSuperficieForVolumen($desde, $hasta, date('Y'));
                    //lista de cultivos en la fecha determinada
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_cultivos, $obj -> cultivo);
                    }
                    foreach ($lst_cultivos as $key => $cultivo_name) {
                        //lista de cultivos
                        Semilla::getVariedadesCultivoForVolumenProduccion($cultivo_name, $desde, $hasta);
                        array_push($lst_variedades_cultivo, DBConnector::filas());
                        //total de variedades de un cultivo en fecha especificada
                        while ($obj2 = DBConnector::objeto()) {
                            array_push($lst_variedades, $obj2 -> variedad);
                            //lista de variedades
                        }
                    }

                    $i = 0;
                    //variable temporal indice de variedades
                    $temp = 0;

                    foreach ($lst_cultivos as $id => $cultivo) {
                        //total de variedades
                        $variedades = $lst_variedades_cultivo[$id];
                        while ($i < $variedades) {
                            foreach ($lst_categorias as $key => $categoria) {
                                $total_variedades = HojaCosecha::getTotalCategoriaVariedadCultivo($cultivo, $lst_variedades[$temp], $categoria, $desde, $hasta);
                                if ($total_variedades) {
                                    array_push($lst_volumen_produccion_categorias, $total_variedades);
                                } else {
                                    array_push($lst_volumen_produccion_categorias, '0.00');
                                }
                            }
                            $i++;
                        }
                        $temp += $i;
                        //contador de variedades
                        $i = 0;
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    ////   E  X  C  E  L
                    #var_dump($lst_volumen_produccion_categorias,$lst_cultivos,$lst_variedades);
                    include '../modelo/reporte/xlsx/volumen_semilla_certificada.php';
                    $json['xls'] = 'vista/reportes/' . $filename;

                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }
                echo json_encode($json);
                break;
        }
        break;

    case 'graficar' :
        //echo $_SERVER['REMOTE_ADDR'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/libs/jpgraph_bar.php';exit;
        require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph.php';
        $tipo = $_GET['tipo'];
        $grafico = $_GET['grafico'];
        $sistema = $_GET['sistema'];
        $desde = ($_GET['desde']) ? $dates -> cambiar_tipo_mysql($_GET['desde']) : date('Y') . '-01-01';
        $hasta = ($_GET['hasta']) ? $dates -> cambiar_tipo_mysql($_GET['hasta']) : date('Y') . '-12-31';
        $provincia = (!empty($_GET['prv'])) ? trim($_GET['prv']) : '%';
        $municipio = (!empty($_GET['mcp'])) ? trim($_GET['mcp']) : '%';
        $semillera = (!empty($_GET['slr'])) ? trim($_GET['slr']) : '%';
        $semillerista = (!empty($_GET['smr'])) ? trim($_GET['smr']) : '%';
        
        if (empty($provincia) && empty($municipio) && empty($semillera) && empty($semillerista))
            $img_result = $reporte -> graficar($grafico, $tipo, $sistema, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista, FALSE);
        else
            $img_result = $reporte -> graficar($grafico, $tipo, $sistema, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista, TRUE);
        #echo $img_result;
        $json['img'] = $img_result;
        $json['provincia'] = ($provincia != '%') ? Provincia::getNombreProvincia($provincia) : '';
        $json['municipio'] = ($municipio != '%') ? Municipio::getNombreMunicipio($municipio) : '';
        $json['semillera'] = $semillera;
        $json['semillerista'] = $semillerista;
        echo json_encode($json);
        break;

    case 'pdf' :
        $pagina = strip_tags($_REQUEST['pag']);
        switch ($pagina) {
            case 'acreedor' :
                require '../modelo/reporte/pdf/acreedores.php';
                break;
            case 'solicitud' :
                session_start();
                $id = $_REQUEST['id'];
                $area = $_REQUEST['area'];
                Solicitud::getSolicitudById($id);
                $obj = DBConnector::objeto();
                $solicitudPDF['numero'] = $obj -> nro_solicitud;
                $solicitudPDF['sistema'] = ucfirst($obj -> sistema);
                $solicitudPDF['semillera'] = $obj -> semillera;
                $solicitudPDF['semillerista'] = $obj -> semillerista;
                $solicitudPDF['departamento'] = $obj -> departamento;
                $solicitudPDF['provincia'] = $obj -> provincia;
                $solicitudPDF['municipio'] = $obj -> municipio;
                $solicitudPDF['comunidad'] = $obj -> comunidad;
                $solicitudPDF['fecha'] = $dates -> cambiar_formato_fecha($obj -> fecha);
                if ($area == 'fiscal') {
                    require '../modelo/reporte/fiscalizacion/solicitud.class.php';
                } else {
                    require '../modelo/reporte/certificacion/solicitud.class.php';
                }
                break;
            case 'semilla' :
                session_start();
                $id = $_REQUEST['id'];
                $area = trim($_REQUEST['area']);

                if ($area == 'certifica') {
                    Semilla::getSemillaById($id);
                    $obj = DBConnector::objeto();
                    $semillaPDF['semillera'] = $obj -> semillera;
                    $semillaPDF['semillerista'] = $obj -> nombre_apellido;
                    $semillaPDF['comunidad'] = ucfirst($obj -> comunidad);
                    $semillaPDF['nro_campo'] = $obj -> nro_campo;
                    $semillaPDF['cultivo'] = $obj -> cultivo;
                    $semillaPDF['variedad'] = $obj -> variedad;
                    $semillaPDF['categoria_sembrada'] = $obj -> categoria_sembrada;
                    $semillaPDF['categoria_producir'] = $obj -> categoria_producir;
                    $semillaPDF['cantidad_semilla_empleada'] = $obj -> cantidad_semilla;
                    $semillaPDF['fecha_siembra'] = $dates -> cambiar_formato_fecha($obj -> fecha);
                    $semillaPDF['plantas_hectarea'] = $obj -> plantas_hectarea;
                    $semillaPDF['cultivo_anterior'] = $obj -> cultivo_anterior;

                    require '../modelo/reporte/certificacion/semilla.class.php';
                } else {
                    Semilla::getSemillaFiscalById($id);
                    $obj = DBConnector::objeto();
                    $semillaPDF['semillera'] = $obj -> semillera;
                    $semillaPDF['semillerista'] = $obj -> nombre . ' ' . $obj -> apellido;
                    $semillaPDF['comunidad'] = ucfirst($obj -> comunidad);
                    $semillaPDF['nro_solicitud'] = $obj -> nro_campo;
                    $semillaPDF['cultivo'] = $obj -> cultivo;
                    $semillaPDF['variedad'] = $obj -> variedad;
                    $semillaPDF['lote'] = $obj -> lote;
                    $semillaPDF['categoria_producir'] = $obj -> categoria_producir;
                    require '../modelo/reporte/fiscalizacion/semilla.class.php';
                }
                break;
            case 'acondicionamiento' :
                session_start();
                $id = $_REQUEST['id'];
                $acondicionamiento_obj -> getAcondicionamientoByIdSolicitud($id);
                $obj = DBConnector::objeto();
                $acondicionaPDF['semillera'] = $obj -> semillera;
                $acondicionaPDF['semillerista'] = $obj -> semillerista;
                $acondicionaPDF['comunidad'] = ($obj -> comunidad);
                $acondicionaPDF['nro_campo'] = $obj -> nro_campo;
                $acondicionaPDF['emision'] = $obj -> emision;
                $acondicionaPDF['categoria_campo'] = $obj -> categoria_en_campo;
                $acondicionaPDF['rendimiento_estimado'] = $obj -> rendimiento_estimado;
                $acondicionaPDF['rendimiento_campo'] = $obj -> rendimiento_campo;
                $acondicionaPDF['superficie'] = $obj -> superficie;
                $acondicionaPDF['rango'] = $obj -> inicio . '-' . ($obj -> inicio + $obj -> nro_cupones);
                $acondicionaPDF['nocupones'] = $obj -> nro_cupones;
                $acondicionaPDF['planta'] = $obj -> planta_acondicionadora;
                $acondicionaPDF['peso_bruto'] = $obj -> peso_bruto_semilla;
                require '../modelo/reporte/certificacion/acondicionamiento.class.php';
                break;
            case 'plantines' :
                session_start();
                $id = $_REQUEST['id'];
                $plantines_obj -> getPlantinesByIdSemilla($id);
                $obj = DBConnector::objeto();
                $plantinesPDF['semillera'] = $obj -> semillera;
                $plantinesPDF['semillerista'] = $obj -> semillerista;
                $plantinesPDF['comunidad'] = utf8_decode($obj -> comunidad);
                $plantinesPDF['fecha'] = $obj -> fecha_solicitud;
                $plantinesPDF['cultivo'] = ucfirst($obj -> cultivo);
                $plantinesPDF['variedad'] = ucfirst($obj -> variedad);
                require '../modelo/reporte/certificacion/plantines.class.php';
                break;
            case 'superficie' :
                session_start();
                $id = $_REQUEST['id'];
                Superficie::getSuperficieById($id);
                $obj = DBConnector::objeto();
                $superficiePDF['semillera'] = $obj -> semillera;
                $superficiePDF['semillerista'] = $obj -> semillerista;
                $superficiePDF['nro_campo'] = $obj -> nro_campo;
                $superficiePDF['cultivo'] = $obj -> cultivo;
                $superficiePDF['variedad'] = ($obj -> variedad);
                $superficiePDF['inscrita'] = $obj -> inscrita;
                $superficiePDF['rechazada'] = $obj -> rechazada;
                $superficiePDF['retirada'] = $obj -> retirada;
                $superficiePDF['aprobada'] = $obj -> aprobada;
                require '../modelo/reporte/certificacion/superficie.class.php';
                break;
            case 'inspeccion' :
                session_start();
                $id = $_REQUEST['id'];
                Inspeccion::getInspeccionesById($id);
                $obj = DBConnector::objeto();
                $inspeccionPDF['semillera'] = $obj -> semillera;
                $inspeccionPDF['semillerista'] = $obj -> semillerista;
                $inspeccionPDF['comunidad'] = $obj -> comunidad;
                $inspeccionPDF['nro_campo'] = $obj -> nro_campo;
                $inspeccionPDF['cultivo'] = $obj -> cultivo;
                $inspeccionPDF['primera'] = $dates -> cambiar_formato_fecha($obj -> primera);
                $inspeccionPDF['observacion_1'] = empty($obj -> observacion_1) ? '-' : $obj -> observacion_1;
                $inspeccionPDF['segunda'] = $dates -> cambiar_formato_fecha($obj -> segunda);
                $inspeccionPDF['observacion_2'] = empty($obj -> observacion_2) ? '-' : $obj -> observacion_2;
                $inspeccionPDF['tercera'] = $dates -> cambiar_formato_fecha($obj -> tercera);
                $inspeccionPDF['observacion_3'] = empty($obj -> observacion_3) ? '-' : $obj -> observacion_3;
                require '../modelo/reporte/certificacion/inspeccion.class.php';
                break;
            case 'inspeccion-plantines' :
                session_start();
                $id = $_REQUEST['id'];
                $inspeccionPlantines['tipo'] = array();
                $inspeccionPlantines['fecha'] = array();
                $inspeccionPlantines['observacion'] = array();
                Inspeccion::getInspeccionesPlantinById($id);
                $obj = DBConnector::objeto();
                $inspeccionPlantines['semillera'] = $obj -> semillera;
                $inspeccionPlantines['semillerista'] = $obj -> semillerista;
                $inspeccionPlantines['comunidad'] = $obj -> comunidad;
                while ($obj = DBConnector::objeto()) {
                    $inspeccionPlantines['tipo'] = $obj -> tipo;
                    $inspeccionPlantines['fecha'] = $obj -> fecha;
                    $inspeccionPlantines['observacion'] = $obj -> observacion;
                }
                require '../modelo/reporte/certificacion/inspeccion_plantines.class.php';
                break;
            case 'cosecha' :
                session_start();
                $id = $_REQUEST['id'];
                $area = trim($_REQUEST['area']);

                if ($area == 'certifica') {
                    HojaCosecha::getHojaCosechaCertificaById($id);
                    $obj = DBConnector::objeto();
                    $cosechaPDF['semillera'] = $obj -> semillera;
                    $cosechaPDF['semillerista'] = $obj -> semillerista;
                    $cosechaPDF['nro_campo'] = $obj -> nro_campo;
                    $cosechaPDF['fechaEmision'] = $dates -> cambiar_formato_fecha($obj -> fecha_emision);
                    $cosechaPDF['categoriaCampo'] = $obj -> categoria_en_campo;
                    $cosechaPDF['rendimientoEstimado'] = $obj -> rendimiento_estimado;
                    $cosechaPDF['superficieParcela'] = $obj -> superficie_parcela;
                    $cosechaPDF['rendimientoCampo'] = $obj -> rendimiento_campo;
                    $cosechaPDF['nroCupones'] = $obj -> nro_cupones;
                    $cosechaPDF['rango'] = $obj -> rango_cupones;
                    $cosechaPDF['plantaAcondicionadora'] = $obj -> planta_acondicionadora;
                    require '../modelo/reporte/certificacion/cosecha.class.php';
                } else {
                    HojaCosecha::getHojaCosechaFiscalById($id);
                    $obj = DBConnector::objeto();
                    $cosechaPDF['semillera'] = $obj -> semillera;
                    $cosechaPDF['semillerista'] = $obj -> semillerista;
                    $cosechaPDF['cultivo'] = $obj -> cultivo;
                    $cosechaPDF['plantaAcondicionadora'] = $obj -> planta_acondicionadora;
                    require '../modelo/reporte/fiscalizacion/cosecha.class.php';
                }
                break;
            case 'laboratorio' :
                session_start();
                $id = $_REQUEST['id'];
                Laboratorio::getMuestraById($id);
                $obj = DBConnector::objeto();

                $muestraPDF['semillerista'] = $obj -> semillerista;
                $muestraPDF['nro_campo'] = $obj -> nro_campo;
                $muestraPDF['fecha_recepcion'] = $dates -> cambiar_formato_fecha($obj -> fecha_recepcion);
                $muestraPDF['cultivo'] = $obj -> cultivo;
                $muestraPDF['variedad'] = $obj -> variedad;

                require '../modelo/reporte/certificacion/laboratorio.class.php';
                break;
            case 'reslab' :
                session_start();
                $id = $_REQUEST['id'];
                Resultado_muestras::getResultadoLaboratorio($id);
                $obj = DBConnector::objeto();

                $resultadoPDF['nro_analisis'] = $obj -> nro_analisis;
                $resultadoPDF['fecha_recepcion'] = $dates -> cambiar_formato_fecha($obj -> fecha_recepcion);
                $resultadoPDF['cultivo'] = $obj -> cultivo;
                $resultadoPDF['nro_campo'] = $obj -> nro_campo;
                $resultadoPDF['origen'] = $obj -> origen;
                $resultadoPDF['fecha_resultado'] = $dates -> cambiar_formato_fecha($obj -> fecha_resultado);
                $resultadoPDF['lote'] = $obj -> lote;
                $resultadoPDF['pureza'] = $obj -> pureza;
                $resultadoPDF['germinacion'] = $obj -> germinacion;
                $resultadoPDF['humedad'] = $obj -> humedad;
                $resultadoPDF['observacion'] = $obj -> observacion;
                $resultadoPDF['kgBolsa'] = $obj -> kgBolsa;
                $resultadoPDF['calibre'] = $obj -> calibre;

                require '../modelo/reporte/pdf/resultado_laboratorio.class.php';
                break;
            case 'semillaP' :
                session_start();
                $id = $_REQUEST['id'];
                $area = $_REQUEST['area'];
                Solicitud::getSolicitudById($id);
                $obj = DBConnector::objeto();
                $semillaProducidaPDF['numero'] = $obj -> nro_solicitud;
                $semillaProducidaPDF['sistema'] = ucfirst($obj -> sistema);
                $semillaProducidaPDF['semillera'] = $obj -> semillera;
                $semillaProducidaPDF['semillerista'] = $obj -> semillerista;
                $semillaProducidaPDF['departamento'] = $obj -> departamento;
                $semillaProducidaPDF['provincia'] = $obj -> provincia;
                $semillaProducidaPDF['municipio'] = $obj -> municipio;
                $semillaProducidaPDF['comunidad'] = $obj -> comunidad;
                $semillaProducidaPDF['fecha'] = $dates -> cambiar_formato_fecha($obj -> fecha);
                
                if ($area == 'fiscal') {
                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id);
                    $semillaProducidaPDF['cultivo'] = $cultivo;
                    $variedad = Semilla::getVariedadByIdSolicitud($id);
                    $semillaProducidaPDF['variedad'] = $variedad;
                    SemillaProd::getSemillaProdCertificaForPDF($id,'fiscaliza');
                    $obj2 = DBConnector::objeto();
                    $semillaProducidaPDF['semilla_neta'] = $obj2 -> semilla_neta;
                    $semillaProducidaPDF['categoria_obtenida'] = $obj2 -> categoria_obtenida;
                    $semillaProducidaPDF['numero_etiqueta'] = $obj2 -> nro_etiqueta;
                    require '../modelo/reporte/fiscalizacion/semillaP.class.php';
                } else {
                    $cultivo = Semilla::getCultivoByIdSolicitudCertifica($id);
                    $semillaProducidaPDF['cultivo'] = $cultivo;
                    $variedad = Semilla::getVariedadByIdSolicitud($id);
                    $semillaProducidaPDF['variedad'] = $variedad;
                    SemillaProd::getSemillaProdCertificaForPDF($id,'certifica');
                    $obj2 = DBConnector::objeto();
                    $semillaProducidaPDF['semilla_neta'] = $obj2 -> semilla_neta;
                    $semillaProducidaPDF['categoria_obtenida'] = $obj2 -> categoria_obtenida;
                    $semillaProducidaPDF['numero_etiqueta'] = $obj2 -> nro_etiqueta;
                    
                    require '../modelo/reporte/certificacion/semillaP.class.php';
                }

                break;
            case 'cuenta' :
                session_start();
                $id = $_REQUEST['id'];
                $area = $_REQUEST['area'];
                Cuenta::getCuentaByIdPDF($id);

                $obj = DBConnector::objeto();
                $cuentaPDF['isemilla'] = $obj ->id_semilla;
                $cuentaPDF['responsable'] = $obj -> responsable;
                $cuentaPDF['fecha'] = $dates -> cambiar_formato_fecha($obj -> fecha);
                $cuentaPDF['semillera'] = $obj -> semillera;
                $cuentaPDF['semillerista'] = $obj -> semillerista;
                #$cuentaPDF['nro_campo'] = $obj -> ;
                $cuentaPDF['cultivo'] = $obj -> cultivo;
                $cuentaPDF['variedad'] = $obj -> variedad;
                $cuentaPDF['categoria_aprobada'] = $obj -> categoria_aprobada;
                $cuentaPDF['superficie_aprobada'] = $obj -> superficie_aprobada;
                $cuentaPDF['superficie_total'] = $obj -> superficie_total;
                $cuentaPDF['bolsa_etiqueta'] = $obj -> bolsa_etiqueta;
                $cuentaPDF['inscripcion'] = $obj -> inscripcion;
                $cuentaPDF['inspeccion'] = $obj -> inspeccion;
                $cuentaPDF['analisis_laboratorio'] = $obj -> analisis_etiqueta;
                #$cuentaPDF['acondicionamiento'] = $obj -> acondicionamiento;
                #$cuentaPDF['plantines'] = $obj -> plantines;
                $cuentaPDF['total'] = $obj -> total;
                /*
                 Cuenta::getCuotaPDF($obj->id_cuenta);
                 if (DBConnector::filas()){
                 $obj = DBConnector::objeto();
                 $cuentaPDF['fecha'] = $dates->cambiar_formato_fecha($obj -> fecha);
                 $cuentaPDF['cuota'] = $obj -> cuota;
                 $cuentaPDF['saldo'] = $obj -> saldo;
                 }else{
                 $cuentaPDF['fecha'] = '000-00-00';
                 $cuentaPDF['cuota'] = 0;
                 $cuentaPDF['saldo'] = 0;
                 }
                 */
                if ($area == 'fiscal') {
                    require '../modelo/reporte/fiscalizacion/cuenta.class.php';
                } else {
                    require '../modelo/reporte/certificacion/cuenta.class.php';
                }
                break;
            case 'detalle' :
                session_start();
                $cultivo = strip_tags($_REQUEST['cultivo']);
                $area = intval($_REQUEST['area']);
                $tipo = strip_tags($_REQUEST['tipo']);
                if ($tipo != 'trim') {
                    $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : date('Y') . '-01-01';
                    $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                //lista de semilleras
                $lst_semillera = array();
                $reporte -> semillerasDetalleProduccionCostos($desde, $hasta, $cultivo);
                if (DBConnector::filas()) {
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_semillera, utf8_encode($obj -> semillera));
                    }
                    $lst_fecha = array();
                    $lst_comunidad = array();
                    $lst_semillerista = array();
                    $lst_cultivo = array();
                    $lst_variedad = array();
                    $lst_cat_sembrada = array();
                    $lst_superficie = array();
                    $lst_nocampo = array();
                    $lst_aprobada = array();
                    $lst_rechazada = array();
                    $lst_cat_aprobada = array();
                    $lst_nobolsa = array();
                    $lst_inscripcion = array();
                    $lst_campo = array();
                    $lst_etiqueta = array();
                    $lst_total = array();
                    //costos de produccion de semillera
                    foreach ($lst_semillera as $semillera) {
                        //array que contienen los datos individuales de cada campo
                        $reporte -> getDetalleProduccionBySemillera($semillera, $cultivo, $desde, $hasta);
                        $lst_total_semilleristas = DBConnector::filas();
                        #var_dump(DBConnector::objeto());
                        while ($obj = DBConnector::objeto()) {
                            #echo $semillera,' :: ',$obj -> fecha.'<br>';
                            array_push($lst_fecha, $dates -> cambiar_formato_fecha($obj -> fecha));
                            array_push($lst_comunidad, utf8_encode($obj -> comunidad));
                            array_push($lst_semillerista, utf8_encode($obj -> semillerista));
                            array_push($lst_cultivo, utf8_encode($obj -> cultivo));
                            array_push($lst_variedad, utf8_encode($obj -> variedad));
                            array_push($lst_cat_sembrada, $obj -> cat_sembrada);
                            array_push($lst_superficie, floatval($obj -> superficie));
                            array_push($lst_nocampo, $obj -> nro_campo);
                            array_push($lst_aprobada, floatval($obj -> aprobada));
                            array_push($lst_rechazada, floatval($obj -> rechazada));
                            array_push($lst_cat_aprobada, $obj -> cat_aprobada);
                            array_push($lst_nobolsa, $obj->etiqueta);
                            array_push($lst_inscripcion, ($obj -> inscripcion));
                            array_push($lst_campo,($obj -> campo));
                            array_push($lst_etiqueta, ($obj -> analisis_etiqueta));
                            array_push($lst_total, ($obj -> total));
                        }
                    }
                    #var_dump($lst_fecha);
                    #cargar plantilla
                    include_once '../modelo/reporte/pdf/detalle.class.php';
                } else {
                    echo 'No hay registros para mostrar en el periodo seleccionado ';
                }

                break;
            case 'certifica' :
                //resumen
                $sistema = strip_tags($_REQUEST['pag']);
                $tipo = strip_tags($_REQUEST['tipo']);
                $gestion = intval(strip_tags($_REQUEST['gestion']));
                if ($tipo != 'trim') {
                    $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : date('Y') . '-01-01';
                    $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }

                //MODIFICAR ESTADO
                $total_semilleras = Solicitud::getSemillerasTotalByTime($desde, $hasta, $gestion);

                if ($total_semilleras > 0) {
                    $total_solicitudes = Solicitud::getTotalSolicitudes($desde, $hasta, $gestion);
                    $total_finalizadas = Solicitud::getTotalSolicitudesTerminadas($desde, $hasta, $gestion);
                    $total_pendientes = $total_solicitudes - $total_finalizadas;
                    $area = Sistema::getIdSistema($sistema);

                    Solicitud::getListMunicipiosByTime($area, $desde, $hasta, $gestion);
                    $lst_municipios = array();
                    #lista de municipios
                    while ($municipios = DBConnector::objeto()) {
                        array_push($lst_municipios, $municipios -> municipio);
                    }
                    #numero de comunidades
                    $totalComunidades = Solicitud::getListComunidadesByTime($sistema, $desde, $hasta, $gestion);
                    #numero de semilleristas
                    $totalProductores = Solicitud::getProductoresByTime($desde, $hasta, 'contar', $area, $gestion);
                    //lista de semilleras
                    Solicitud::getSemillerasByTime($desde, $hasta, $gestion, $area);
                    $lst_total_semilleras = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($lst_total_semilleras, utf8_encode($row -> semillera));
                    }
                    $lst_semilleras = array_unique($lst_total_semilleras);
                    //contar semilleras particulares y agregar a la lista se semilleras
                    //cambiar estado en semillerastotalparticulares
                    $total_particulares = Solicitud::getSemillerasTotalParticularesByTime($desde, $hasta, 1);
                    if ($total_particulares > 0) {
                        array_push($lst_semilleras, 'PARTICULARES (' . $total_particulares . ')');
                    }
                    //ordenar semilleras
                    sort($lst_semilleras);
                    //lista de cultivos
                    Semilla::getListCultivosByTime($desde, $hasta, $area, $gestion);
                    $lst_cultivos = array();
                    $lst_no_cultivos = array('Acondicionamiento', 'Plantines', 'Barbecho', 'Descanso', ' ');
                    while ($row = DBConnector::objeto()) {
                        if (!in_array($row -> cultivo, $lst_no_cultivos)) {
                            if (strlen($row -> cultivo))
                                array_push($lst_cultivos, $row -> cultivo);
                        }
                    }
                    $lst_variedades = array();
                    //lista de cada variedad de un cultivo
                    $lst_total_variedades = array();
                    //total de variedades
                    $lst_categorias = array();
                    //lista de categorias de un cultivo
                    $lst_total_categorias = array();
                    //total de categorias
                    #print_r($lst_cultivos);exit;
                    foreach ($lst_cultivos as $key => $cultivo) {
                        if (strlen($cultivo)) {
                            $semilla -> getVariedadesCultivo($cultivo, $gestion, $desde, $hasta);
                            if (DBConnector::filas()) {
                                while ($row = DBConnector::objeto()) {
                                    array_push($lst_variedades, $row -> variedad);
                                }
                                array_push($lst_total_variedades, DBConnector::filas());
                            }
                        }
                    }
                    //lista de categoria a producir
                    foreach ($lst_cultivos as $key => $cultivo) {
                        if (strlen($cultivo)) {
                            $semilla -> getListCategoriaCampo($cultivo, $sistema, $gestion, $desde, $hasta);
                            while ($row = DBConnector::objeto()) {
                                array_push($lst_categorias, $row -> categoria_producir);
                                array_push($lst_total_categorias, DBConnector::filas());
                            }
                        }
                    }
                    #var_dump($lst_categorias,$lst_total_categorias);
                    $lst_superficies = array();
                    //lista de superficie de cultivo
                    foreach ($lst_cultivos as $cultivo) {
                        $total = Superficie::getTotalSupAprobadaByCultivo($dates -> search_tilde($cultivo), $area, $desde, $hasta, $gestion);
                        #var_dump($total);
                        if ($total && !is_null($total)) {
                            array_push($lst_superficies, $total);
                        } else {
                            array_push($lst_superficies, '0.00');
                        }

                    }
                    #var_dump($lst_superficies);
                    $lst_volumen = array();
                    //volumenes de produccion
                    foreach ($lst_cultivos as $cultivo) {
                        if (strlen($cultivo)) {
                            HojaCosecha::getVolumenTotalByCultivo($cultivo, $area, $desde, $hasta, $gestion);
                            $row = DBConnector::objeto();
                            if ($row -> total) {
                                array_push($lst_volumen, $row -> total);
                            } else {
                                array_push($lst_volumen, '0.00');
                            }
                        }
                    }

                    $muestras['aprobadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 1);
                    $muestras['rechazadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 2);
                    $muestras['fiscalizadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 3);
                    $muestras['total'] = $muestras['aprobadas'] + $muestras['rechazadas'] + $muestras['fiscalizadas'];
                    $total_categorias = $semilla -> getTotalCategoriaCampo($sistema, $gestion);
                    /////////////////////////////////////////////////////////////////////////////////////
                    require '../modelo/reporte/pdf/resumenCer.class.php';
                } else {
                    echo 'No hay registros para mostrar en el periodo seleccionado ';
                }
                break;
            case 'grafico' :
                session_start();
                $imagen = trim($_GET['img']);
                $titulo = '../images/logoPagina.jpg';
                ob_start();
                require '../modelo/reporte/pdf/grafico.class.php';
                ob_end_flush();
                break;
            case 'sp_produccion' :
                session_start();
                $tipo = strip_tags($_REQUEST['tipo']);
                $gestion = intval($_REQUEST['gestion']);
                if ($tipo != 'trim') {
                    $desde = $dates -> cambiar_tipo_mysql($_REQUEST['desde']);
                    $hasta = (!empty($_REQUEST['hasta'])) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                $superficies_cultivos = $reporte -> getTotalSuperficieProduccionCultivos($desde, $hasta, $gestion);
                if ($superficies_cultivos > 0) {
                    $lst_cultivos = array();
                    $reporte -> getListCultivosSuperficieProduccion($desde, $hasta, $gestion);
                    #lista de cultivos
                    $superficies['inscrita'] = array();
                    $superficies['retirada'] = array();
                    $superficies['rechazada'] = array();
                    $superficies['aprobada'] = array();
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_cultivos, $obj -> cultivo);
                    }
                    foreach ($lst_cultivos as $key => $nombre) {
                        if (strlen($nombre)) {
                            $reporte -> getSuperficieProduccionCultivos($nombre, $desde, $hasta);
                            #datos de un cultivo determinado
                            while ($row = DBConnector::objeto()) {
                                array_push($superficies['inscrita'], $row -> inscrita);
                                array_push($superficies['retirada'], $row -> retirada);
                                array_push($superficies['rechazada'], $row -> rechazada);
                                array_push($superficies['aprobada'], $row -> aprobada);
                            }
                        }
                    }
                    $total_apr = array_sum($superficies['aprobada']);
                    //calculo de porcentaje  de cada cultivo
                    $superficies['porcentaje'] = array();
                    if ($total_apr) {
                        foreach ($superficies['aprobada'] as $valor) {
                            $total = ($valor * 100) / $total_apr;
                            array_push($superficies['porcentaje'], $total);
                        }
                    } else {
                        foreach ($superficies['aprobada'] as $valor) {
                            array_push($superficies['porcentaje'], 0);
                        }
                    }
                    if (in_array($desde, array("01", "04", "07", "10"))) {
                        $desde = $dates -> getInicioMes($desde);
                        $hasta = $dates -> getFinMes($hasta);
                    } else {
                        $desde = $dates -> cambiar_formato_fecha($desde);
                        $hasta = $dates -> cambiar_formato_fecha($hasta);
                    }
                    require '../modelo/reporte/pdf/superficie_produccion_semilla.class.php';
                    $json['pdf'] = $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }

                echo json_encode($json);
                break;
            case 'vp_cultivo' :
                session_start();
                $area = intval($_GET['area']);
                $tipo = strip_tags($_REQUEST['tipo']);
                if ($tipo != 'trim') {
                    $desde = $dates -> cambiar_tipo_mysql($_REQUEST['desde']);
                    $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : date('Y') . '-12-31';
                } else {
                    $opcion = intval($_REQUEST['chs']);
                    $meses = $dates -> getMesesTrimestres($opcion);
                    $desde = $meses['desde'];
                    $hasta = $meses['hasta'];
                }
                $filas_volumen_produccion = HojaCosecha::getTotalVolumenProduccion($desde, $hasta,$area);
                if ($filas_volumen_produccion) {
                    #if (TRUE){
                    $lst_cultivos = array();
                    //lista de cultivos
                    $lst_variedades = array();
                    //lista de variedades
                    $lst_variedades_cultivo = array();
                    //total de variedades de un cultivo en fecha especificada
                    $lst_categorias = array('basica', 'registrada', 'certificada', 'certificada-B', 'fiscalizada');
                    $lst_volumen_produccion_categorias = array();
                    //contiene el total de de la categoria de un cultivo y variedad
                    Semilla::getListCultivosSuperficie($desde, $hasta, date('Y'));
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_cultivos, $obj -> cultivo);
                        //lista de cultivos
                        Semilla::getVariedadesCultivoForVolumenProduccion($obj -> cultivo, $desde, $hasta);
                        while ($obj2 = DBConnector::objeto()) {
                            array_push($lst_variedades, $obj2 -> variedad);
                            //lista de variedades
                        }
                        //total de variedades de un cultivo en fecha especificada
                        array_push($lst_variedades_cultivo, count($lst_variedades));
                    }
                    foreach ($lst_cultivos as $i => $cultivo_name) {
                        #echo $cultivo_name,'<br>';
                        $contador = $lst_variedades_cultivo[$i];
                        while ($contador <= $lst_variedades_cultivo[$i]) {
                            foreach ($lst_variedades as $j => $variedad_name) {
                                foreach ($lst_categorias as $k => $categoria_name) {
                                    $total = HojaCosecha::getTotalCategoriaVariedadCultivo($cultivo_name, $variedad_name, $categoria_name, $desde, $hasta);
                                    if ($total) {
                                        array_push($lst_volumen_produccion_categorias, $total);
                                    } else {
                                        array_push($lst_volumen_produccion_categorias, '0.00');
                                    }
                                }
                            }
                            $contador++;
                        }
                    }
                    if (in_array($desde, array("01", "04", "07", "10"))) {
                        $desde = $dates -> getInicioMes($desde);
                        $hasta = $dates -> getFinMes($hasta);
                    } else {
                        $desde = $dates -> cambiar_formato_fecha($desde);
                        $hasta = $dates -> cambiar_formato_fecha($hasta);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////
                    /////////   PDF
                    //$lst_cultivos, $lst_variedades_cultivo,$lst_variedades, $lst_categorias, $lst_volumen_produccion_categorias
                    #echo count($lst_variedades_cultivo), '=',  count($lst_variedades);
                    require '../modelo/reporte/pdf/volumen_produccion_cultivo.class.php';
                    $json['pdf'] = $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }

                echo json_encode($json);
                break;
        }
        break;
}
?>