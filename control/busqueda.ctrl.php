<?php
// Tiempo ilimitado para el script
set_time_limit(0);
//libreria necesaria para excel
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/Writer/Excel2007.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/Reader/Excel2007.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/IOFactory.php";
//libreria para PDF
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/fpdf/fpdf.php";
//libreria para grafico
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph.php';
$opcion = $_REQUEST['opt'];
$pagina = $_REQUEST['pag'];
global $resultado_array;

switch ($opcion) {
    case 'new' :
        switch ($pagina) {
            /**
             * buscador de gestiones anteriores v3.0
             * */
            case 'buscador_form' :
                session_start();
                include '../vista/busqueda/buscar_gestiones.html.php';
                break;
            case 'laboratorio_form' :
                session_start();
                include '../vista/busqueda/buscar_laboratorio.html.php';
                break;
        }
        break;
    case 'buscar' :
        switch($pagina) {
            case 'buscador' :
                session_start();
                $semillera = ($_POST['semillera'] === 'todas') ? '%' : trim($_POST['semillera']);
                $semillerista = ($_POST['productor'] === 'todos') ? '%' : trim($_POST['productor']);
                $cultivo = ($_POST['cultivo'] === 'todos') ? '%' : trim($_POST['cultivo']);
                $gestion = intval($_POST['gestion']);
                $area = $_POST['area'];
                if ($semillerista != '%') {
                    $productor = $dates -> getNombreApellido($semillerista);
                    $nombre = $productor['nombre'];
                    $apellido = (isset($productor['apellido'])) ? $productor['apellido'] : '';
                } else {
                    $nombre = '%';
                    $apellido = '%';
                }
                if ($semillera != '%')
                    $id_semillera = $solicitudes -> getIdSemilleraBySistema($semillera, $area, $nombre, $apellido, $gestion);
                else {
                    $id_semillera = '%';
                }
                $solicitudes -> buscadorSemilleras('solicitud', $cultivo, $nombre, $id_semillera, $gestion, $area);

                $semillera = array();
                $laboratorio = array();
                $cultivo = array();

                if (DBConnector::filas()) {
                    $sistema = $_POST['area'];
                    //nombre de las semilleras
                    $semillera = array();
                    //nombre de los semilleristas
                    $productor = array();
                    //id de semilleristas
                    $id_productor = array();
                    //nro_campo y cultivo de un semillerista
                    $campos = array();
                    $cultivos = array();
                    //etapa en la que se encuentra el semillerista
                    $etapa = array();

                    //total de semilleristas
                    $cantidad = array();

                    while ($fila = DBConnector::objeto()) {
                        array_push($semillera, $fila -> semillera);
                    }
                    $semillera = array_unique($semillera);
                    $key_semillera = array_keys($semillera);
                    $counter = 0;
                    $limit = count($semillera);
                    while ($counter < $limit) {
                        Solicitud::getCantidad($semillera[$key_semillera[$counter]]);
                        while ($aux = DBConnector::objeto()) {
                            array_push($cantidad, $aux -> cantidad);
                        }

                        Solicitud::getProductores($semillera[$key_semillera[$counter]], $sistema);
                        while ($fila = DBConnector::objeto()) {
                            $nombre = utf8_encode($fila -> semillerista);
                            #array_push($id_productor,$fila->id);
                            array_push($productor, $nombre);
                            array_push($etapa, $fila -> estado);
                            array_push($id_productor, intval($fila -> id));
                        }
                        $counter++;
                    }

                    if ($area == 1) {
                        include '../vista/certificacion/datos/buscador.php';
                    } else {
                        include '../vista/fiscalizacion/datos/fiscalizacion.php';
                    }

                } else {
                    echo "No se encuentran resultados a mostrar";
                }

                break;
            case 'todo' :
                $texto_a_buscar = $_POST['cadena_buscar'];
                $objBuscador = $buscar -> buscarCadena($texto_a_buscar);
                $buscar -> prepareSearch();
                $buscar -> prepareConsulta();

                global $semilleras;
                global $semilleristas;
                global $campanhas;
                global $cultivos;
                global $variedades;
                global $categoria_sembradas;
                global $categoria_producidas;

                $semilleras = array();
                $semilleristas = array();
                $campanhas = array();
                $cultivos = array();
                $variedades = array();
                $categoria_sembradas = array();
                $categoria_producidas = array();
                $buscar -> executeConsulta();

                while ($fila = DBConnector::objeto()) {
                    array_push($semilleras, $fila -> semillera);
                    $nombre_semillerista = utf8_encode($fila -> nombre) . ' ' . utf8_encode($fila -> apellido);
                    array_push($semilleristas, $nombre_semillerista);
                    array_push($campanhas, $fila -> campana);
                    array_push($cultivos, $fila -> cultivo);
                    array_push($variedades, $fila -> variedad);
                    array_push($categoria_sembradas, $fila -> categoria_sembrada);
                    array_push($categoria_producidas, $fila -> categoria_producir);
                }
                $resultado_array['semillera'] = $semilleras;
                $resultado_array['semillerista'] = $semilleristas;
                $resultado_array['campanha'] = $campanhas;
                $resultado_array['cultivo'] = $cultivos;
                $resultado_array['variedad'] = $variedades;
                $resultado_array['categoria_sembrada'] = $categoria_sembradas;
                $resultado_array['categoria_producir'] = $categoria_producidas;

                $filas = count($semilleras);
                if ($filas > 0) {
                    include ('../vista/busqueda/resultadoBusqueda.html.php');
                } else {
                    $json['semillera'] = '';
                    echo json_encode($resultado_array);
                }
                break;
            case 'campanhas' :
                $sistema = trim($_GET['sistema']);
                $json = array();
                $buscar -> getCampanhas($sistema);
                #echo DBConnector::mensaje();
                while ($fila = DBConnector::objeto()) {
                    array_push($json, $fila -> campanha);
                }
                echo json_encode($json);
                break;
            case 'gestiones' :
                $semillera = trim($_POST['semillera']);
                $campanha = trim($_POST['campanha']);
                $productor = isset($_POST['productor']) ? trim($_POST['productor']) : '';

                $counter = 0;

                if (!empty($campanha) && empty($productor)) {
                    $query = Busqueda::getGestionesAnteriores($semillera, $campanha);
                } else {
                    $temp = explode(" ", $productor);

                    if (count($temp) > 2) {
                        $nombre = $temp[0];
                        $apellido = $temp[1] . $temp[2];
                    } else {
                        $nombre = $temp[0];
                        $apellido = $temp[1];
                    }
                    $query = Busqueda::getGestionesAnteriores($semillera, $campanha, $nombre, $apellido);
                }
                DBConnector::ejecutar($query);
                if (!DBConnector::nroError()) {
                    $semilleras = array();
                    $semilleristas = array();
                    $cultivo = array();
                    $comunidad = array();
                    $variedad = array();
                    $categoria_sembrada = array();
                    $inscrita = array();
                    $nro_campo = array();
                    $aprobada = array();
                    $rechazada = array();
                    $categoria_producir = array();
                    $nro_bolsa = array();
                    $inscripcion = array();
                    $inspeccion_campo = array();
                    $analisis_laboratorio_etiqueta = array();
                    $total_costo = array();
                    $total_inscrita = array();
                    $total_aprobada = array();
                    $total_rechazada = array();
                    $nroBolsas_total = array();
                    $inscripcion = array();
                    $inspeccion_campo_total = array();
                    $analisis_laboratorio_etiqueta_total = array();
                    $total_general = array();

                    while ($fila = DBConnector::resultado()) {
                        array_push($semilleras, $fila -> semillera);
                        array_push($semilleristas, $fila -> nombre . ' ' . $fila -> apellido);
                        array_push($comunidad, $fila -> comunidad);
                        array_push($cultivo, $fila -> cultivo);
                        array_push($variedad, $fila -> variedad);
                        array_push($categoria_sembrada, $fila -> categoria_sembrada);
                        array_push($inscrita, $fila -> inscrita);
                        array_push($nro_campo, $fila -> nro_campo);
                        array_push($aprobada, $fila -> aprobada);
                        array_push($rechazada, $fila -> rechazada);
                        array_push($categoria_producir, $fila -> categoria_producir);
                        array_push($nro_bolsa, $fila -> nro_bolsa);
                        array_push($inscripcion, $fila -> inscripcion);
                        array_push($inspeccion_campo, $fila -> inspeccion_campo);
                        array_push($analisis_laboratorio_etiqueta, $fila -> analisis_laboratorio_etiqueta);
                        array_push($total_costo, $fila -> total_costo);
                        array_push($total_inscrita, $fila -> totalInscrita);
                        array_push($total_aprobada, $fila -> totalAprobada);
                        array_push($total_rechazada, $fila -> totalRechazada);
                        array_push($nroBolsas_total, $fila -> nroBolsas_total);
                        array_push($inscripcion, $fila -> inscripcion_total);
                        array_push($inspeccion_campo_total, $fila -> inspeccion_campo_total);
                        array_push($analisis_laboratorio_etiqueta_total, $fila -> analabet_total);
                        array_push($total_general, $fila -> total_general);
                    }
                    $json['semillera'] = $semilleras;
                    $json['semillerista'] = $semilleristas;
                    $json['comunidad'] = $comunidad;
                    $json['cultivo'] = $cultivo;
                    $json['variedad'] = $variedad;
                    $json['cat_sembrada'] = $categoria_sembrada;
                    $json['sup_inscrita'] = $inscrita;
                    $json['nro_campo'] = $nro_campo;
                    $json['sup_aprobada'] = $aprobada;
                    $json['sup_rechazada'] = $rechazada;
                    $json['cat_producir'] = $categoria_producir;
                    $json['nro_bolsa'] = $nro_bolsa;
                    $json['inscripcion'] = $inscripcion;
                    $json['inspeccion_campo'] = $inspeccion_campo;
                    $json['anlabet'] = $analisis_laboratorio_etiqueta;
                    $json['total_costo'] = $total_costo;
                    $json['total_inscrita'] = $total_inscrita;
                    $json['total_aprobada'] = $total_aprobada;
                    $json['total_rechazada'] = $total_rechazada;
                    $json['total_nro_bolsas'] = $nroBolsas_total;
                    $json['inscripcion'] = $inscripcion;
                    $json['total_inspeccion_campo'] = $inspeccion_campo_total;
                    $json['total_anlabet'] = $analisis_laboratorio_etiqueta_total;
                    $json['total_general'] = $total_general;
                    echo json_encode($json);
                } else {
                    echo json_encode('');
                }
                break;
            case 'full_semilleras' :
                $area = $_GET['area'];
                $solicitudes -> getSemilleraForBusqueda($area);
                $json['semillera'] = array();
                while ($row = DBConnector::objeto()) {
                    array_push($json['semillera'], utf8_encode($row -> semillera));
                }
                #var_dump(json_encode($json));
                echo json_encode($json);
                break;
            case 'semilleristas' :
                $area = $_GET['area'];
                $semillera = $_GET['slr'];
                $buscar -> getBuscarSemilleristas($semillera, $area);
                $json['semilleristas'] = array();
                //var_dump(DBConnector::objeto());
                while ($row = DBConnector::objeto()) {
                    array_push($json['semilleristas'], $row -> semillerista);
                }
                echo json_encode($json);
                break;
            case 'cultivos' :
                Cultivo::getFullCultivos();
                $json['cultivos'] = array();
                while ($row = DBConnector::objeto()) {
                    array_push($json['cultivos'], $row -> cultivo);
                }
                echo json_encode($json);
                break;
            case 'lst_gestiones' :
                $sistema = intval($_GET['area']);
                $gestiones = array();
                Solicitud::getGestiones($sistema);
                while ($lsgestion = DBConnector::objeto()) {
                    array_push($gestiones, $lsgestion -> gestion);
                }
                //array_push($gestiones, date('Y'));
                $json['gestiones'] = $gestiones;
                #var_dump(json_encode($json));
                echo json_encode($json);
                break;
            case 'lst_gestiones_muestras' :                
                $gestiones = array();
                Muestra_laboratorio::getGestionesAnteriores();
                while ($lsgestion = DBConnector::objeto()) {
                    array_push($gestiones, $lsgestion -> gestion);
                }
                //array_push($gestiones, date('Y'));
                $json['gestiones'] = $gestiones;
                #var_dump(json_encode($json));
                echo json_encode($json);
                break;
            case 'lst_campanhas' :
                $sistema = intval($_GET['area']);
                $gestiones = array();
                Semilla::getCampaniaForBusqueda($sistema);
                while ($lsgestion = DBConnector::objeto()) {
                    array_push($gestiones, $lsgestion -> campanha);
                }
                //array_push($gestiones, date('Y'));
                $json['campanhas'] = $gestiones;
                #var_dump(json_encode($json));
                echo json_encode($json);
                break;
            //lista cultivos de una gestion anterior
            case 'cultivos_anteriores' :
                $gestion = intval($_GET['gestion']);
                $reporte -> getCultivosGestionesAnteriores($gestion);
                $json['cultivos'] = array();
                if (DBConnector::filas()) {
                    while ($row = DBConnector::objeto()) {
                        array_push($json['cultivos'], $row -> cultivo);
                    }
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }

                echo json_encode($json);
                break;
        }
        break;
    case 'xls' :
        switch ($pagina) {
            case 'resumen' :
                //nro de sistema
                $area = intval($_REQUEST['sistema']);
                //nombre  del sistema
                $sistema = Sistema::getSistemaById($area);
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';
                //MODIFICAR ESTADO
                $total_semilleras = Solicitud::getSemillerasTotalByTime($desde, $hasta,$gestion,$area);

                if ($total_semilleras > 0) {

                    //calculo total de solicitudes realizadas,finalizadas y pendientes
                    $total_solicitudes = Solicitud::getTotalSolicitudes($desde, $hasta,$gestion);
                    $total_finalizadas = Solicitud::getTotalSolicitudesTerminadas($desde, $hasta,$gestion);
                    $total_pendientes = $total_solicitudes - $total_finalizadas;

                    Solicitud::getListMunicipiosByTime($area, $desde, $hasta,$gestion);
                    #lista de municipios
                    $lst_municipios = array();
                    while ($municipios = DBConnector::objeto()) {
                        array_push($lst_municipios, $municipios -> municipio);
                    }
                    #numero de comunidades
                    $totalComunidades = Solicitud::getListComunidadesByTime($sistema, $desde, $hasta,$gestion);
                    #numero de semilleristas
                    $totalProductores = Solicitud::getProductoresByTime($desde, $hasta, 'contar', $area);
                    //lista de semilleras
                    Solicitud::getSemillerasByTime($desde, $hasta, $gestion,$area);
                    $lst_semilleras = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($lst_semilleras, utf8_encode($row -> semillera));
                    }
                    //contar semilleras particulares y agregar a la lista se semilleras
                    //cambiar estado en semillerastotalparticulares
                    $total_particulares = Solicitud::getSemillerasTotalParticularesByTime($desde, $hasta, 1);
                    if ($total_particulares > 0) {
                        array_push($lst_semilleras, 'PARTICULARES (' . $total_particulares . ')');
                    }
                    //ordenar semilleras
                    sort($lst_semilleras);
                    //lista de cultivos
                    Semilla::getListCultivosByTime($desde, $hasta, $area,$gestion);
                    $lst_cultivos = array();
                    $lst_no_cultivos = array('Acondicionamiento', 'Plantines', 'Barbecho', 'Descanso');
                    while ($row = DBConnector::objeto()) {
                        if (!in_array($row -> cultivo, $lst_no_cultivos)) {
                            if (strlen($row -> cultivo))
                                array_push($lst_cultivos, $row -> cultivo);
                        }
                    }
                    $muestras['aprobadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 1);
                    $muestras['rechazadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 2);
                    $muestras['fiscalizadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 3);
                    $muestras['total'] = $muestras['aprobadas'] + $muestras['rechazadas'] + $muestras['fiscalizadas'];

                    /////////////////////////////////////////////////////////////////////////////////////////////
                    include '../modelo/reporte/xlsx/resumenCertificacionGestiones.php';
                    $json['xls'] = 'vista/reportes/' . $filename;
                    $json['msg'] = 'OK';                    
                } else {
                    $json['msg'] = 'error';
                }
                echo json_encode($json); 
                break;

            case 'detalle' :
                $cultivo = strip_tags($_REQUEST['cultivo']);
                $area = intval($_REQUEST['sistema']);
                $gestion = strip_tags($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';
                //lista de semilleras
                $lst_semillera = array();
                $reporte -> semillerasDetalleProduccionCostos($desde, $hasta, $cultivo);
                if (DBConnector::filas() > 0) {
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_semillera, utf8_encode($obj -> semillera));
                    }
                    $lst_fecha = array();
                    $lst_comunidad = array();
                    $lst_semillerista = array();
                    $lst_cultivo = array();
                    $lst_variedad = array();
                    $lst_cat_sembrada = array();
                    $lst_superficie = array();
                    $lst_nocampo = array();
                    $lst_aprobada = array();
                    $lst_rechazada = array();
                    $lst_cat_aprobada = array();
                    $lst_nobolsa = array();
                    $lst_inscripcion = array();
                    $lst_campo = array();
                    $lst_etiqueta = array();
                    $lst_total = array();
                    //costos de produccion de semillera
                    foreach ($lst_semillera as $semillera) {
                        //array que contienen los datos individuales de cada campo
                        $reporte -> getDetalleProduccionBySemillera($semillera, $cultivo, $desde, $hasta);
                        $lst_total_semilleristas = DBConnector::filas();
                        while ($obj = DBConnector::objeto()) {
                            array_push($lst_fecha, $dates -> cambiar_formato_fecha($obj -> fecha));
                            array_push($lst_comunidad, utf8_encode($obj -> comunidad));
                            array_push($lst_semillerista, utf8_encode($obj -> semillerista));
                            array_push($lst_cultivo, utf8_encode($obj -> cultivo));
                            array_push($lst_variedad, utf8_encode($obj -> variedad));
                            array_push($lst_cat_sembrada, $obj -> cat_sembrada);
                            array_push($lst_superficie, floatval($obj -> superficie));
                            array_push($lst_nocampo, $obj -> nro_campo);
                            array_push($lst_aprobada, floatval($obj -> aprobada));
                            array_push($lst_rechazada, floatval($obj -> rechazada));
                            array_push($lst_cat_aprobada, $obj -> cat_aprobada);
                            array_push($lst_nobolsa, 5);
                            array_push($lst_inscripcion, floatval($obj -> inscripcion));
                            array_push($lst_campo, floatval($obj -> campo));
                            array_push($lst_etiqueta, 90);
                            #floatval($obj -> analisis_etiqueta));
                            array_push($lst_total, floatval($obj -> total));
                        }
                    }
                    #cargar plantilla
                    include '../modelo/reporte/xlsx/detalleCertificacionGestiones.php';
                    $json['file'] = 'vista/reportes/' . $filename;
                    $json['msg'] = 'OK';
                } else {
                    $json['msg'] = "ERROR no existen datos del cultivo en las fechas seleccionadas";
                }
                echo json_encode($json);
                break;
            case 'superficie' :
                //sistema certificacion | fiscalizacion
                $area = intval($_GET['area']);
                $sistema = Sistema::getSistemaById(intval($_GET['area']));
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';

                $superficies_cultivos = $reporte -> getTotalSuperficieProduccionCultivos($desde, $hasta,$gestion,$area);
                if ($superficies_cultivos > 0) {
                    $lst_cultivos = array();
                    $reporte -> getListCultivosSuperficieProduccion($desde, $hasta, $gestion,$area);
                    #lista de cultivos
                    while ($obj = DBConnector::objeto()) {
                        if (strlen($obj->cultivo))
                            array_push($lst_cultivos, utf8_encode($obj -> cultivo));
                    }
                    $superficies['inscrita'] = array();
                    $superficies['retirada'] = array();
                    $superficies['rechazada'] = array();
                    $superficies['aprobada'] = array();
                    foreach ($lst_cultivos as $key => $nombre) {
                        $reporte -> getSuperficieProduccionCultivos($nombre, $desde, $hasta,$area);
                        #datos de un cultivo determinado
                        while ($row = DBConnector::objeto()) {
                            array_push($superficies['inscrita'], $row -> inscrita);
                            array_push($superficies['retirada'], $row -> retirada);
                            array_push($superficies['rechazada'], $row -> rechazada);
                            array_push($superficies['aprobada'], $row -> aprobada);
                        }
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////////////  EXCEL
                    include '../modelo/reporte/xlsx/superficie_cultivo.php';
                    $json['xls'] = 'vista/reportes/' . $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }
                echo json_encode($json);
                break;
            case 'volumen' :
                //sistema certificacion | fiscalizacion
                $area = intval($_GET['area']);
                $sistema = Sistema::getSistemaById(intval($_GET['area']));
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';

                $filas_volumen_produccion = HojaCosecha::getTotalVolumenProduccion($desde, $hasta,$area);
                if ($filas_volumen_produccion) {
                #if (TRUE) {
                    $lst_cultivos = array();
                    //lista de cultivos
                    $lst_variedades = array();
                    //lista de variedades
                    $lst_variedades_cultivo = array();
                    //total de variedades de un cultivo en fecha especificada
                    $lst_categorias = array('basica', 'registrada', 'certificada', 'certificada-B', 'fiscalizada');
                    $lst_volumen_produccion_categorias = array();
                    //contiene el total de de la categoria de un cultivo y variedad
                    Semilla::getListCultivosSuperficie($desde, $hasta);
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_cultivos, $obj -> cultivo);
                        //lista de cultivos
                        Semilla::getVariedadesCultivo($obj -> cultivo, $desde, $hasta,$area);
                        array_push($lst_variedades_cultivo, DBConnector::filas());
                        //total de variedades de un cultivo en fecha especificada
                        while ($obj2 = DBConnector::objeto()) {
                            array_push($lst_variedades, utf8_encode($obj2 -> variedad));
                            //lista de variedades
                        }
                    }
                    foreach ($lst_cultivos as $i => $cultivo_name) {
                        $contador = $lst_variedades_cultivo[$i];
                        while ($contador <= $lst_variedades_cultivo[$i]) {
                            foreach ($lst_variedades as $j => $variedad_name) {
                                foreach ($lst_categorias as $k => $categoria_name) {
                                    $total = HojaCosecha::getTotalCategoriaVariedadCultivo($cultivo_name, $dates->modificar($variedad_name), $categoria_name, $desde, $hasta);
                                    if ($total) {
                                        array_push($lst_volumen_produccion_categorias, $total);
                                    } else {
                                        array_push($lst_volumen_produccion_categorias, '0.00');
                                    }
                                }
                            }
                            $contador++;
                        }
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////   E  X  C  E  L
                    include '../modelo/reporte/xlsx/volumen_semilla_certificada.php';
                    $json['xls'] = 'vista/reportes/' . $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }
                echo json_encode($json);
                break;
        }
        break;
    case 'pdf' :
        switch($pagina) {
            //funciona
            case 'resumen' :
                //nro de sistema
                $area = strip_tags($_REQUEST['sistema']);
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';

                //MODIFICAR ESTADO
                $total_semilleras = Solicitud::getSemillerasTotalByTime($desde, $hasta, $gestion,$area);

                if ($total_semilleras > 0) {
                    $total_solicitudes = Solicitud::getTotalSolicitudes($desde, $hasta, $gestion);
                    $total_finalizadas = Solicitud::getTotalSolicitudesTerminadas($desde, $hasta, $gestion);
                    $total_pendientes = $total_solicitudes - $total_finalizadas;

                    Solicitud::getListMunicipiosByTime($area, $desde, $hasta, $gestion);
                    $lst_municipios = array();
                    #lista de municipios
                    while ($municipios = DBConnector::objeto()) {
                        array_push($lst_municipios, $municipios -> municipio);
                    }
                    #numero de comunidades
                    $totalComunidades = Solicitud::getListComunidadesByTime($area, $desde, $hasta, $gestion);
                    #numero de semilleristas
                    $totalProductores = Solicitud::getProductoresByTime($desde, $hasta, 'contar', $area, $gestion);
                    //lista de semilleras
                    Solicitud::getSemillerasByTime($desde, $hasta, $gestion, $area);
                    $lst_total_semilleras = array();
                    while ($row = DBConnector::objeto()) {
                        array_push($lst_total_semilleras, utf8_encode($row -> semillera));
                    }
                    $lst_semilleras = array_unique($lst_total_semilleras);
                    //contar semilleras particulares y agregar a la lista se semilleras
                    //cambiar estado en semillerastotalparticulares
                    $total_particulares = Solicitud::getSemillerasTotalParticularesByTime($desde, $hasta, 1);
                    if ($total_particulares > 0) {
                        array_push($lst_semilleras, 'PARTICULARES (' . $total_particulares . ')');
                    }
                    //ordenar semilleras
                    sort($lst_semilleras);
                    //lista de cultivos
                    Semilla::getListCultivosByTime($desde, $hasta, $area, $gestion);
                    $lst_cultivos = array();
                    $lst_no_cultivos = array('Acondicionamiento', 'Plantines', 'Barbecho', 'Descanso', ' ');
                    while ($row = DBConnector::objeto()) {
                        if (!in_array($row -> cultivo, $lst_no_cultivos)) {
                            if (strlen($row -> cultivo))
                                array_push($lst_cultivos, $row -> cultivo);
                        }
                    }
                    $lst_variedades = array();
                    //lista de cada variedad de un cultivo
                    $lst_total_variedades = array();
                    //total de variedades
                    $lst_categorias = array();
                    //lista de categorias de un cultivo
                    $lst_total_categorias = array();
                    //total de categorias
                    #print_r($lst_cultivos);exit;
                    foreach ($lst_cultivos as $key => $cultivo) {
                        if (strlen($cultivo)) {
                            $semilla -> getVariedadesCultivo($cultivo, $gestion, $desde, $hasta);
                            if (DBConnector::filas()) {
                                while ($row = DBConnector::objeto()) {
                                    array_push($lst_variedades, $row -> variedad);
                                }
                                array_push($lst_total_variedades, DBConnector::filas());
                            }
                        }
                    }
                    //lista de categoria a producir
                    foreach ($lst_cultivos as $key => $cultivo) {
                        if (strlen($cultivo)) {
                            $semilla -> getListCategoriaCampo($cultivo, $area, $gestion, $desde, $hasta);
                            while ($row = DBConnector::objeto()) {
                                array_push($lst_categorias, $row -> categoria_producir);
                                array_push($lst_total_categorias, DBConnector::filas());
                            }
                        }
                    }
                    #var_dump($lst_categorias,$lst_total_categorias);
                    $lst_superficies = array();
                    //lista de superficie de cultivo
                    foreach ($lst_cultivos as $cultivo) {
                        $total = Superficie::getTotalSupAprobadaByCultivo($dates -> search_tilde($cultivo), $area, $desde, $hasta, $gestion);
                        #var_dump($total);
                        if ($total && !is_null($total)) {
                            array_push($lst_superficies, $total);
                        } else {
                            array_push($lst_superficies, '0.00');
                        }

                    }
                    #var_dump($lst_superficies);
                    $lst_volumen = array();
                    //volumenes de produccion
                    foreach ($lst_cultivos as $cultivo) {
                        if (strlen($cultivo)) {
                            HojaCosecha::getVolumenTotalByCultivo($cultivo, $area, $desde, $hasta, $gestion);
                            $row = DBConnector::objeto();
                            if ($row -> total) {
                                array_push($lst_volumen, $row -> total);
                            } else {
                                array_push($lst_volumen, '0.00');
                            }
                        }
                    }

                    $muestras['aprobadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 1);
                    $muestras['rechazadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 2);
                    $muestras['fiscalizadas'] = $muestra -> getTotalMuestrasAprobadasByArea($area, $desde, $hasta, 3);
                    $muestras['total'] = $muestras['aprobadas'] + $muestras['rechazadas'] + $muestras['fiscalizadas'];
                    $total_categorias = $semilla -> getTotalCategoriaCampo($area, $gestion);
                    /////////////////////////////////////////////////////////////////////////////////////
                    if ($area == 1)
                        require '../modelo/reporte/pdf/resumenCerGestiones.class.php';
                    else
                        require '../modelo/reporte/pdf/resumenGestionesFiscal.class.php';
                } else {
                    echo 'No hay registros para mostrar en el periodo seleccionado ';
                }
                break;
            //terminar
            case 'detalle' :
                session_start();
                $cultivo = strip_tags($_REQUEST['cultivo']);
                //nro de sistema
                $area = strip_tags($_REQUEST['sistema']);
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';

                //lista de semilleras
                $lst_semillera = array();
                $reporte -> semillerasDetalleProduccionCostos($desde, $hasta, $cultivo);
                if (DBConnector::filas() > 0) {
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_semillera, utf8_encode($obj -> semillera));
                    }
                    $lst_fecha = array();
                    $lst_comunidad = array();
                    $lst_semillerista = array();
                    $lst_cultivo = array();
                    $lst_variedad = array();
                    $lst_cat_sembrada = array();
                    $lst_superficie = array();
                    $lst_nocampo = array();
                    $lst_aprobada = array();
                    $lst_rechazada = array();
                    $lst_cat_aprobada = array();
                    $lst_nobolsa = array();
                    $lst_inscripcion = array();
                    $lst_campo = array();
                    $lst_etiqueta = array();
                    $lst_total = array();
                    //costos de produccion de semillera
                    foreach ($lst_semillera as $semillera) {
                        //array que contienen los datos individuales de cada campo
                        $reporte -> getDetalleProduccionBySemillera($semillera, $cultivo, $desde, $hasta);
                        $lst_total_semilleristas = DBConnector::filas();
                        while ($obj = DBConnector::objeto()) {
                            array_push($lst_fecha, $dates -> cambiar_formato_fecha($obj -> fecha));
                            array_push($lst_comunidad, utf8_encode($obj -> comunidad));
                            array_push($lst_semillerista, utf8_encode($obj -> semillerista));
                            array_push($lst_cultivo, utf8_encode($obj -> cultivo));
                            array_push($lst_variedad, utf8_encode($obj -> variedad));
                            array_push($lst_cat_sembrada, $obj -> cat_sembrada);
                            array_push($lst_superficie, floatval($obj -> superficie));
                            array_push($lst_nocampo, $obj -> nro_campo);
                            array_push($lst_aprobada, floatval($obj -> aprobada));
                            array_push($lst_rechazada, floatval($obj -> rechazada));
                            array_push($lst_cat_aprobada, $obj -> cat_aprobada);
                            array_push($lst_nobolsa, 5);
                            array_push($lst_inscripcion, floatval($obj -> inscripcion));
                            array_push($lst_campo, floatval($obj -> campo));
                            array_push($lst_etiqueta, 90);
                            #floatval($obj -> analisis_etiqueta));
                            array_push($lst_total, floatval($obj -> total));
                        }
                    }
                    #cargar plantilla
                    include_once '../modelo/reporte/pdf/detalleGestiones.class.php';
                } else {
                    echo 'No hay registros para mostrar en el periodo seleccionado ';
                }
                break;

            case 'superficie' :
                session_start();
                //nro de sistema
                $area = strip_tags($_REQUEST['sistema']);
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';
                $superficies_cultivos = $reporte -> getTotalSuperficieProduccionCultivos($desde, $hasta,$gestion);
                if ($superficies_cultivos > 0) {
                    $lst_cultivos = array();
                    $reporte -> getListCultivosSuperficieProduccion($desde, $hasta, $gestion);
                    #lista de cultivos
                    $superficies['inscrita'] = array();
                    $superficies['retirada'] = array();
                    $superficies['rechazada'] = array();
                    $superficies['aprobada'] = array();
                    while ($obj = DBConnector::objeto()) {
                        if (strlen($obj->cultivo))
                            array_push($lst_cultivos, $obj -> cultivo);
                    }
                    foreach ($lst_cultivos as $key => $nombre) {
                        $reporte -> getSuperficieProduccionCultivos($nombre, $desde, $hasta);
                        #datos de un cultivo determinado
                        while ($row = DBConnector::objeto()) {
                            array_push($superficies['inscrita'], $row -> inscrita);
                            array_push($superficies['retirada'], $row -> retirada);
                            array_push($superficies['rechazada'], $row -> rechazada);
                            array_push($superficies['aprobada'], $row -> aprobada);
                        }
                    }
                    $total_apr = array_sum($superficies['aprobada']);
                    //calculo de porcentaje  de cada cultivo
                    $superficies['porcentaje'] = array();
                    if ($total_apr) {
                        foreach ($superficies['aprobada'] as $valor) {
                            $total = ($valor * 100) / $total_apr;
                            array_push($superficies['porcentaje'], $total);
                        }
                    } else {
                        foreach ($superficies['aprobada'] as $valor) {
                            array_push($superficies['porcentaje'], 0);
                        }
                    }
                    if (in_array($desde, array("01", "04", "07", "10"))) {
                        $desde = $dates -> getInicioMes($desde);
                        $hasta = $dates -> getFinMes($hasta);
                    } else {
                        $desde = $dates -> cambiar_formato_fecha($desde);
                        $hasta = $dates -> cambiar_formato_fecha($hasta);
                    }
                    require '../modelo/reporte/pdf/superficie_produccion_semilla_gestiones.class.php';
                    $json['pdf'] = $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }

                echo json_encode($json);
                break;
            case 'volumen' :
                session_start();
                //nro de sistema
                $area = strip_tags($_REQUEST['sistema']);
                //gestion a buscar
                $gestion = intval($_REQUEST['gestion']);
                //fecha inicial
                $desde = ($_REQUEST['desde']) ? $dates -> cambiar_tipo_mysql($_REQUEST['desde']) : $gestion . '-01-01';
                //fecha final
                $hasta = ($_REQUEST['hasta']) ? $dates -> cambiar_tipo_mysql($_REQUEST['hasta']) : $gestion . '-12-31';
                $filas_volumen_produccion = HojaCosecha::getTotalVolumenProduccion($desde, $hasta);
                if ($filas_volumen_produccion) {
                    #if (TRUE){
                    $lst_cultivos = array();
                    //lista de cultivos
                    $lst_variedades = array();
                    //lista de variedades
                    $lst_variedades_cultivo = array();
                    //total de variedades de un cultivo en fecha especificada
                    $lst_categorias = array('basica', 'registrada', 'certificada', 'certificada-B', 'fiscalizada');
                    $lst_volumen_produccion_categorias = array();
                    //contiene el total de de la categoria de un cultivo y variedad
                    Semilla::getListCultivosSuperficie($desde, $hasta);
                    while ($obj = DBConnector::objeto()) {
                        array_push($lst_cultivos, $obj -> cultivo);
                        //lista de cultivos
                        Semilla::getVariedadesCultivo($obj -> cultivo, $desde, $hasta);
                        array_push($lst_variedades_cultivo, DBConnector::filas());
                        //total de variedades de un cultivo en fecha especificada
                        while ($obj2 = DBConnector::objeto()) {
                            array_push($lst_variedades, $obj2 -> variedad);
                            //lista de variedades
                        }
                    }
                    foreach ($lst_cultivos as $i => $cultivo_name) {
                        $contador = $lst_variedades_cultivo[$i];
                        while ($contador <= $lst_variedades_cultivo[$i]) {
                            foreach ($lst_variedades as $j => $variedad_name) {
                                foreach ($lst_categorias as $k => $categoria_name) {
                                    $total = HojaCosecha::getTotalCategoriaVariedadCultivo($cultivo_name, $variedad_name, $categoria_name, $desde, $hasta);
                                    if ($total) {
                                        array_push($lst_volumen_produccion_categorias, $total);
                                    } else {
                                        array_push($lst_volumen_produccion_categorias, '0.00');
                                    }
                                }
                            }
                            $contador++;
                        }
                    }
                    if (in_array($desde, array("01", "04", "07", "10"))) {
                        $desde = $dates -> getInicioMes($desde);
                        $hasta = $dates -> getFinMes($hasta);
                    } else {
                        $desde = $dates -> cambiar_formato_fecha($desde);
                        $hasta = $dates -> cambiar_formato_fecha($hasta);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////
                    /////////   PDF
                    require '../modelo/reporte/pdf/volumen_produccion_cultivo_gestiones.class.php';
                    $json['pdf'] = $filename;
                    $json['msg'] = "OK";
                } else {
                    $json['msg'] = "error";
                }

                echo json_encode($json);

                break;
        }
        break;
}
if (!substr_compare($pagina, 'f', 0, 1)) {

} else {
    switch ($pagina) {

        case 'busqueda' :
            break;
        case 'resultado' :
            include 'vista/busqueda/resultadoBusqueda.html.php';
            break;
    }
}
?>