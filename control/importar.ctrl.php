<?php
// Tiempo ilimitado para el script
set_time_limit(0);
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/Reader/Excel2007.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . "/PHPExcel/IOFactory.php";
$opcion = $_REQUEST['opt'];
$pagina = $_REQUEST['pag'];
global $resultado_array;
switch ($opcion) {
    case 'new' :
        switch ($pagina) {
            /**
             * formulario de importacion de archivos excel para BD v1.0
             * */
            case 'form_import' :
                include '../vista/busqueda/importar.html.php';
                break;

            case 'datos' :
                //comprobamos que sea una petición ajax
                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                    //obtenemos el archivo a subir
                    $file = $_FILES['archivo']['name'];
                    $extension = end(explode(".", $file));
                    $solo_excel = array('xls', 'xlsx');
                    //verificar extension de archivo
                    if (in_array($extension, $solo_excel)) {
                        //comprobamos si existe un directorio para subir el archivo
                        //si no es así, lo creamos
                        if (!is_dir("../vista/temp/"))
                            mkdir("../vista/temp", 0777);

                        //comprobamos si el archivo ha subido
                        $ubicacion = "../vista/temp/" . basename($file);
                        if ($file && move_uploaded_file($_FILES['archivo']['tmp_name'], $ubicacion)) {
                            $fileCSV = Excel::convertirXLStoCSV($ubicacion);
                            $time_inicial = microtime(TRUE);
                            $estado = Excel::importarCSVToBDv2($fileCSV);
                            $time_final = microtime(TRUE);
                            $time_elapsed = $time_final-$time_inicial;
                            
                            if ($estado != 'error') {
                                echo json_encode(array("state" => "OK", "msg" => "Datos ingresados exitosamente","file" => $estado,"time"=>$time_elapsed));
                            } else {
                                echo json_encode(array("state" => "formato", "msg" => "archivo con formato invalido"));
                            }

                        } else {
                            echo json_encode(array("state" => "error", "msg" => "Error al subir archivo. El archivo esta abierto"));
                        }
                    } else {
                        echo json_encode(array("state" => "error", "msg" => "Seleccione archivo valido"));
                    }
                } else {
                    echo json_encode(array("state" => "error", "msg" => "Procesando el archivo"));
                }
                break;
        }
        break;

    case 'actualizar' :
        switch ($pagina) {
            case 'database' :
                $filename = $_GET['file'];
                #$queries = explode(';', file_get_contents("../vista/temp/".$filename.'.sql'));
                $state = TRUE;  //verifica si  hay una consulta
                /*
                foreach ($queries as $query) {
                    if ($query != '') {
                        DBConnector::ejecutar($query);                        
                    }else{
                        $state = FALSE;
                        break;
                    }
                }*/
                if ($state){
                    $json['msg'] = "OK";
                    $json['description'] = "Datos actualizados existosamente!!!";                    
                }else{
                    $json['msg'] = "error";
                    $json['description'] = "No existen datos para consulta";
                }
                echo json_encode($json);
                break;
        }
        break;
    case 'buscar' :
        switch($pagina) {
            case 'buscador' :
                session_start();
                $semillera = ($_POST['semillera'] === 'todas') ? '%' : trim($_POST['semillera']);
                $semillerista = ($_POST['productor'] === 'todos') ? '%' : trim($_POST['productor']);
                $cultivo = ($_POST['cultivo'] === 'todos') ? '%' : trim($_POST['cultivo']);
                $gestion = intval($_POST['gestion']);
                $area = $_POST['area'];
                if ($semillerista != '%') {
                    $productor = $dates -> getNombreApellido($semillerista);
                    $nombre = $productor['nombre'];
                    $apellido = $productor['apellido'];
                } else {
                    $nombre = '%';
                    $apellido = '%';
                }

                $solicitudes -> buscadorSemilleras('solicitud', $cultivo, $nombre, $apellido, $semillera, $gestion, $area);

                $ls_semilleras = array();
                $ls_productores = array();
                $ls_cultivo = array();

                if (!strcasecmp($area, 'certificacion')) {
                    include '../vista/certificacion/datos/certificacion.php';
                } else {
                    include '../vista/fiscalizacion/datos/fiscalizacion.php';
                }

                break;
            case 'todo' :
                $texto_a_buscar = $_POST['cadena_buscar'];
                $objBuscador = $buscar -> buscarCadena($texto_a_buscar);
                $buscar -> prepareSearch();
                $buscar -> prepareConsulta();

                global $semilleras;
                global $semilleristas;
                global $campanhas;
                global $cultivos;
                global $variedades;
                global $categoria_sembradas;
                global $categoria_producidas;

                $semilleras = array();
                $semilleristas = array();
                $campanhas = array();
                $cultivos = array();
                $variedades = array();
                $categoria_sembradas = array();
                $categoria_producidas = array();
                $buscar -> executeConsulta();
                while ($fila = DBConnector::resultado()) {
                    array_push($semilleras, $fila -> semillera);
                    $nombre_semillerista = utf8_encode($fila -> nombre) . ' ' . utf8_encode($fila -> apellido);
                    array_push($semilleristas, $nombre_semillerista);
                    array_push($campanhas, $fila -> campana);
                    array_push($cultivos, $fila -> cultivo);
                    array_push($variedades, $fila -> variedad);
                    array_push($categoria_sembradas, $fila -> categoria_sembrada);
                    array_push($categoria_producidas, $fila -> categoria_producir);
                }
                $resultado_array['semillera'] = $semilleras;
                $resultado_array['semillerista'] = $semilleristas;
                $resultado_array['campanha'] = $campanhas;
                $resultado_array['cultivo'] = $cultivos;
                $resultado_array['variedad'] = $variedades;
                $resultado_array['categoria_sembrada'] = $categoria_sembradas;
                $resultado_array['categoria_producir'] = $categoria_producidas;

                $filas = count($semilleras);
                if ($filas > 0) {
                    include ('../vista/busqueda/resultadoBusqueda.html.php');
                } else {
                    $json['semillera'] = '';
                    echo json_encode($resultado_array);
                }
                break;
            case 'campanhas' :
                $anho = date('y') - 1;
                $json = array();
                $buscar -> getCampanhas($anho);
                while ($fila = DBConnector::resultado()) {
                    $campania = explode('/', $fila -> campanha);
                    if (count($campania[1]) == 2) {
                        $campania += 2000;
                    }
                    array_push($json, $campania[1]);
                }
                echo json_encode($json);
                break;
            case 'gestiones' :
                $semillera = trim($_POST['semillera']);
                $campanha = trim($_POST['campanha']);
                $productor = isset($_POST['productor']) ? trim($_POST['productor']) : '';

                $counter = 0;

                if (!empty($campanha) && empty($productor)) {
                    $query = Busqueda::getGestionesAnteriores($semillera, $campanha);
                } else {
                    $temp = explode(" ", $productor);

                    if (count($temp) > 2) {
                        $nombre = $temp[0];
                        $apellido = $temp[1] . $temp[2];
                    } else {
                        $nombre = $temp[0];
                        $apellido = $temp[1];
                    }
                    $query = Busqueda::getGestionesAnteriores($semillera, $campanha, $nombre, $apellido);
                }
                DBConnector::ejecutar($query);
                if (!DBConnector::nroError()) {
                    $semilleras = array();
                    $semilleristas = array();
                    $cultivo = array();
                    $comunidad = array();
                    $variedad = array();
                    $categoria_sembrada = array();
                    $inscrita = array();
                    $nro_campo = array();
                    $aprobada = array();
                    $rechazada = array();
                    $categoria_producir = array();
                    $nro_bolsa = array();
                    $inscripcion = array();
                    $inspeccion_campo = array();
                    $analisis_laboratorio_etiqueta = array();
                    $total_costo = array();
                    $total_inscrita = array();
                    $total_aprobada = array();
                    $total_rechazada = array();
                    $nroBolsas_total = array();
                    $inscripcion = array();
                    $inspeccion_campo_total = array();
                    $analisis_laboratorio_etiqueta_total = array();
                    $total_general = array();

                    while ($fila = DBConnector::resultado()) {
                        array_push($semilleras, $fila -> semillera);
                        array_push($semilleristas, $fila -> nombre . ' ' . $fila -> apellido);
                        array_push($comunidad, $fila -> comunidad);
                        array_push($cultivo, $fila -> cultivo);
                        array_push($variedad, $fila -> variedad);
                        array_push($categoria_sembrada, $fila -> categoria_sembrada);
                        array_push($inscrita, $fila -> inscrita);
                        array_push($nro_campo, $fila -> nro_campo);
                        array_push($aprobada, $fila -> aprobada);
                        array_push($rechazada, $fila -> rechazada);
                        array_push($categoria_producir, $fila -> categoria_producir);
                        array_push($nro_bolsa, $fila -> nro_bolsa);
                        array_push($inscripcion, $fila -> inscripcion);
                        array_push($inspeccion_campo, $fila -> inspeccion_campo);
                        array_push($analisis_laboratorio_etiqueta, $fila -> analisis_laboratorio_etiqueta);
                        array_push($total_costo, $fila -> total_costo);
                        array_push($total_inscrita, $fila -> totalInscrita);
                        array_push($total_aprobada, $fila -> totalAprobada);
                        array_push($total_rechazada, $fila -> totalRechazada);
                        array_push($nroBolsas_total, $fila -> nroBolsas_total);
                        array_push($inscripcion, $fila -> inscripcion_total);
                        array_push($inspeccion_campo_total, $fila -> inspeccion_campo_total);
                        array_push($analisis_laboratorio_etiqueta_total, $fila -> analabet_total);
                        array_push($total_general, $fila -> total_general);
                    }
                    $json['semillera'] = $semilleras;
                    $json['semillerista'] = $semilleristas;
                    $json['comunidad'] = $comunidad;
                    $json['cultivo'] = $cultivo;
                    $json['variedad'] = $variedad;
                    $json['cat_sembrada'] = $categoria_sembrada;
                    $json['sup_inscrita'] = $inscrita;
                    $json['nro_campo'] = $nro_campo;
                    $json['sup_aprobada'] = $aprobada;
                    $json['sup_rechazada'] = $rechazada;
                    $json['cat_producir'] = $categoria_producir;
                    $json['nro_bolsa'] = $nro_bolsa;
                    $json['inscripcion'] = $inscripcion;
                    $json['inspeccion_campo'] = $inspeccion_campo;
                    $json['anlabet'] = $analisis_laboratorio_etiqueta;
                    $json['total_costo'] = $total_costo;
                    $json['total_inscrita'] = $total_inscrita;
                    $json['total_aprobada'] = $total_aprobada;
                    $json['total_rechazada'] = $total_rechazada;
                    $json['total_nro_bolsas'] = $nroBolsas_total;
                    $json['inscripcion'] = $inscripcion;
                    $json['total_inspeccion_campo'] = $inspeccion_campo_total;
                    $json['total_anlabet'] = $analisis_laboratorio_etiqueta_total;
                    $json['total_general'] = $total_general;
                    echo json_encode($json);
                } else {
                    echo json_encode('');
                }
                break;
        }
        break;
}
if (!substr_compare($pagina, 'f', 0, 1)) {

} else {
    switch ($pagina) {

        case 'busqueda' :
            break;
        case 'resultado' :
            include 'vista/busqueda/resultadoBusqueda.html.php';
            break;
    }
}
?>