<?php
session_start();

$pagina = $_REQUEST['pag'];

//guarda los datos del formulario solicitado
switch ($pagina) {
        //formulario de registro del administrador del sistema
    case 'nUserAdmin':
        include '../vista/login/nuevo_usuario_admin.html.php';
        break;
        //reningresar al sistema por nombre de usuario
    case 'relogin':
        $usuario->getDatosUsuarioByLogin($login);
        $obj = DBConnector::objeto();
        $_SESSION['usrID'] = $obj->id_login;
        $_SESSION['usr_nombre'] = $obj->nombre;
        $_SESSION['usr_apellido'] = $obj->apellido;
        $_SESSION['usr_login'] = $obj->login;
        $_SESSION['usr_nivel'] = $obj->nivel;
        $_SESSION['usr_area'] = $obj->area;
        $_SESSION['usr_iarea'] = $obj->id_area;
        $_SESSION['usr_edt'] = $obj->crud;

        break;
        //ingresar al sistema
    case 'ingresar':
        if (empty($_SESSION['usr_nivel'])) {
            $nick = trim(strip_tags($_POST['usuario']));
            $passwd = trim(strip_tags($_POST['clave']));
            $usuarios->getUsuarioAd($nick, $passwd);
            if (DBConnector::filas() == 1) {
                $obj = DBConnector::objeto();
                $_SESSION['usrID'] = $obj->id_login;
                $_SESSION['usr_nombre'] = $obj->nombre;
                $_SESSION['usr_apellido'] = $obj->apellido;
                $_SESSION['usr_login'] = $obj->login;
                $_SESSION['usr_nivel'] = $obj->nivel;
                $_SESSION['usr_area'] = $obj->nombre_area;
                $_SESSION['usr_iarea'] = $obj->id_area;
                $_SESSION['usr_edt'] = $obj->crud;

                $partes_ruta = pathinfo(__FILE__);
                $_SESSION['home'] = $dates->getBaseName($partes_ruta['dirname']);
                $bitacora->setIngreso($obj->login);
                echo 'OK';
            } else
                echo DBConnector::mensaje();
        }
        break;
        //salir del sistema
    case 'salir':
        if (!empty($_SESSION['usr_nivel'])) {
            $bitacora->setSalida($_SESSION['usr_login']);
            if (!DBConnector::nroError()) {
                $reportes = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW .  '/reportes/';
                if (file_exists($reportes)) {
                    #determina el size de la carpeta reportes
                    $folder_size = filesize($reportes);
                    $folder_reportes = $reportes;
                    if ($folder_size) {
                        $files = scandir($folder_reportes, 1);
                        $files_total = count($files) - 3;
                        $index = 0;
                        while ($index <= $files_total) {
                            ##eliminar arcvhivos en la carpeta reportes
                            unlink($folder_reportes . $files[$index]);
                            $index++;
                        }
                    }
                }
                else{
                    mkdir($reportes);
                }
                #determina el size de la carpeta reportes de imagenes
                $temporal = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . '/temp/';
                if (!file_exists($temporal)){
                    mkdir($temporal);
                }
                
                $image_folder_size = filesize($temporal);
                $image_folder_reportes = $temporal;
                if ($folder_size) {
                    $files = scandir($image_folder_reportes, 1);
                    $files_total = count($files) - 3;
                    $index = 0;
                    while ($index <= $files_total) {
                        ##eliminar arcvhivos en la carpeta reportes
                        unlink($image_folder_reportes . $files[$index]);
                        $index++;
                    }
                }
                session_destroy();
                echo 'OK';
            } else
                echo 'error';
        } else
            echo '403';
        break;

        //listado de todos los usuarios
    case 'listar':
        $usuarios->getUsuarios();
        include '../vista/login/lista_usuarios.html.php';
        break;
        //para mostrar u ocultar menu editar
    case 'mn_editar':
        $id = intval($_GET['id']);
        $usuarios->getDatos($id);

        if (!DBConnector::error()) {
            $obj = DBConnector::objeto();
            $json['resp'] = $obj->crud;
        } else {
            $json['resp'] = 1100;
        }

        echo json_encode($json);

        break;
    case 'perdetus':
        $id = intval($_REQUEST['id']);
        $usuarios->getDatos($id);
        $json = array();
        while ($datos = DBConnector::objeto()) {
            $json['apellido'] = $datos->apellido;
            $json['nombre'] = $datos->nombre;
            $json['crud'] = $datos->crud;
            $json['id'] = $datos->id_login;
        }

        echo json_encode($json);
        break;
    case 'allow':
        $id = intval($_REQUEST['id']);
        $usuarios->getEstado($id);

        echo json_encode(DBConnector::objeto());
        break;
    case 'habilitar':
        $id = intval($_GET['id']);
        $estado = (trim($_GET['estado']) == 'y') ? 1 : 2;

        $usuarios->setEstado($id, $estado);

        if (!DBConnector::nroError()) {
            $json['msg'] = 'OK';
        } else {
            $json['msg'] = DBConnector::mensaje();
        }

        echo json_encode($json);
        break;
    case 'permisos':
        $usuarios->getUsuarios();
        $users = array();
        $count = 1;
        while ($datos = DBConnector::objeto()) {
            $user['apellido'] = $datos->apellido;
            $user['nombre'] = $datos->nombre;
            $user['area'] = $datos->area;
            $user['estado'] = $datos->estado;
            $crud = $dates->getCRUD($datos->crud);

            $user['crear'] = $crud['c'];
            $user['ver'] = $crud['r'];
            $user['modificar'] = $crud['u'];
            $user['eliminar'] = $crud['d'];
            $user['id'] = $datos->id_login;
            $user['count'] = $count++;
            array_push($users, $user);
        }
        include '../vista/login/permisos.html.php';
        break;

        //Lista de categorias y generaciones
    case 'categoria':
        $categoria->listarCategorias();

        include '../vista/administracion/categoria.html.php';
        break;
        //actualizar datos del usuario (aprobar usuario, modificar area)
        //cargar datos
    case 'ver_datos':
        //session_start();
        //echo $_SESSION['usr_login'];
        $usuarios->getDatos($_SESSION['usr_login']);
        $usuario = DBConnector::asociativo();
        include '../vista/login/datos_usuario_reg.html.php';
        break;
        //eliminar usuario
    case 'eliminar':
        $id = intval($_REQUEST['id']);

        $usuarios->delete_user($id);
        if (!DBConnector::nroError()) {
            echo 'OK';
        } else {
            echo DBConnector::mensaje();
        }
        break;
        //formulario de datos
    case 'updUsuario':
        $id = intval($_REQUEST['id']);
        $usuarios->getDatos($id);
        include '../vista/login/editar_usuario.html.php';
        break;
        //actualizar datos de usuario
    case 'actualizar':
        $nombre = trim($_POST['nombre']);
        $apellido = trim($_POST['apellido']);
        $area = intval($_POST['iarea']);
        $usuario = trim($_POST['usuario']);
        $estado = trim($_POST['estado']);
        $id_login = trim($_POST['id']);

        if (!is_numeric($usuario)) {

            $usuarios->updateUsuario(ucfirst($nombre), ucfirst($apellido), $area, $id_login, $usuario, $estado);

            if (!DBConnector::nroError())
                echo 'OK';
            else
                echo DBConnector::mensaje();
        } else
            echo 'Nombre';
        #nombre de usuario invalido

        break;
        // actualizar permisos de edicion
        // d deshabilitar edicion 1100
        // h habilitar edicion 1110
    case 'pactualizar':
        $id = intval($_GET['pid']);
        $psistem = $_GET['psistem'];
        if ($psistem == 'h') {
            $usuarios->updatePermiso($id, 1);
            $_SESSION['usr_edt'] = 1110;
            if (!DBConnector::nroError()) {
                $json['msg']  = 'POK';
            } else {
                $json['msg'] = DBConnector::error();
            }
        } else {
            $usuarios->updatePermiso($id, 3);
            $_SESSION['usr_edt'] = 1100;
            if (!DBConnector::nroError()) {
                $json['msg']  =  'MOK';
            } else {
                $json['msg'] = DBConnector::error();
            }
        }

        echo json_encode($json);
        break;
        // formulario para crear respaldo de BD
    case 'nueva_copia':
        include '../vista/login/respaldar.php';
        break;
        //crea un respaldo de la BD del sistema
    case 'respaldar':
        if (!empty($_POST['nombreArchivo']))
            $nombre = $_POST['nombreArchivo'];
        else
            $nombre = date('dMHis');
        $fecha = date('Y-m-d');
        $hora = date('H:i:s');
        $iuser = $_POST['iuser'];

        $backup->insertCopia($nombre, $fecha, $hora, $iuser);
        $insertCopy = DBConnector::nroError();
        $error = $usuarios->respaldar_base_datos($nombre, DB_USER, DB_PASS, DB_NAME);
        if (!$error && !$insertCopy)
            echo 'OK';
        else
            echo 'error';
        break;
        // comprobar nombre de usuario
    case 'check':
        $usuario = $_GET['usuario'];
        Usuario::getNombreUsuarios($usuario);

        if (DBConnector::filas())
            $result['uso'] = 'ocupado';
        else
            $result['uso'] = 'libre';
        echo json_encode($result);
        break;
}
