<?php
# Es una petición AJAX
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $opcion = strip_tags($_REQUEST['opt']);
    $pagina = isset($_REQUEST['pag']) ? strip_tags($_REQUEST['pag']) : '-';
    switch($opcion) {
        case 'new' :
            switch($pagina) {
                case 'muestra' :
                    $nivel = intval($_GET['nivel']);
                    include '../vista/laboratorio/muestra.html.php';
                    break;
                case 'resultado' :
                    $laboratorio -> getMuestrasLaboratorioByCampo2();
                    include '../vista/laboratorio/resultado.html.php';
                    break;
                case 'campo' :
                    include '../vista/laboratorio/buscar.html.php';
                    break;
            }
            break;
        case 'guardar' :
            switch ($pagina) {
                case 'resultados' :
                    $nroAnalisis = trim($_POST['analisis']);
                    $f_recepcion = trim($_POST['f_recepcion']);
                    $f_resultado = !empty($_POST['resultado']) ? trim($_POST['resultado']) : '';
                    $pureza = !empty($_POST['pureza']) ? trim($_POST['pureza']) : 0.0;
                    $germinacion = !empty($_POST['germinacion']) ? trim($_POST['germinacion']) : 0.0;
                    $humedad = !empty($_POST['humedad']) ? trim($_POST['humedad']) : 0.0;
                    $nroLoteSemilla = trim($_POST['lote']);
                    $tipoSemilla = trim($_POST['tipo']);
                    $nroCampo = trim($_POST['campo']);
                    $nroBolsa = trim($_POST['bolsa']);
                    $kgBolsa = trim($_POST['kgbolsa']);
                    $total = !empty($_POST['total']) ? trim($_POST['total']) : 0;
                    $calibre = trim($_POST['calibre']);
                    $nroSemillaKilo = trim($_POST['semillakg']);
                    $observacion = !empty($_POST['observacion']) ? trim($_POST['observacion']) : '-';
                    $idCosecha = trim($_POST['icos']);
                    $imuestra = $_POST['muestra'];
                    #$muestra['imuestra'] = intval($_POST['imuestra']);
                    
                    $idSolicitud = Tiene::getIdSolicitud($idCosecha);
                    //cambiamos la fecha al tipo de mysql
                    $f_recepcion = $dates -> cambiar_tipo_mysql($f_recepcion);
                    $f_resultado = $dates -> cambiar_tipo_mysql($f_resultado);
                    $importada = $_POST['estado'];
                    $laboratorio -> setResultadoMuestra($f_recepcion, $nroLoteSemilla, $pureza, $germinacion, $humedad, $observacion, $tipoSemilla, $nroBolsa, $kgBolsa, $total, $calibre, $imuestra);
                    $nroresultado = DBConnector::lastInsertId();
                    #echo "==$nroresultado";
                    $iresultado = Muestra_laboratorio::getIdMuestraByNroSolicitud($nroCampo);
                    if (!DBConnector::nroError()) {
                        Solicitud::updateMuestraResultado($idSolicitud);
                        Semilla::updateLoteByNroCampo($nroCampo, $nroLoteSemilla);
                        if ($importada == 2) {
                            Resultado_certificada::setResultadoCertifica($imuestra, $nroresultado);
                        } else {
                            Resultado_importada::setResultadoImportada($imuestra, $nroresultado);
                        }
                        Laboratorio::updateEstadoAnalisis($iresultado);
                        echo 'OK';
                    } else
                        echo DBConnector::mensaje();
                    break;
                case 'muestra' :
                    //cambiamos la fecha al tipo de mysql
                    $muestraLab['fecha_recepcion'] = $dates -> cambiar_tipo_mysql(trim($_POST['recepcion']));
                    $muestraLab['cultivo'] = trim($_POST['cultivo']);
                    $muestraLab['variedad'] = trim($_POST['variedad']);
                    $campo = trim($_POST['nrocampo']);
                    $muestraLab['nrocampo'] = Semilla::getIdSemillaByNroCampo($campo);
                    $muestraLab['nro_campo'] = ($dates -> buscaBarra($campo) == 2) ? Semilla::getIdSemillaByNroCampoFiscal($campo) : Semilla::getIdSemillaByNroCampo($campo);

                    $germinacion = isset($_POST['germinacion']) ? intval($_POST['germinacion']) : 0;
                    $humedad = isset($_POST['humedad']) ? intval($_POST['humedad']) : 0;
                    $pureza = isset($_POST['pureza']) ? intval($_POST['pureza']) : 0;
                    $muestraLab['sistema'] = Sistema::getIdSistema($_POST['sistema']);
                    $muestraLab['analisis'] = Laboratorio::getIdTipoAnalisis($germinacion, $humedad, $pureza);
                    $muestraLab['origen'] = strip_tags($_POST['origen']);
                    $muestraLab['nro_analisis'] = Laboratorio::getAnalisisCount()+1;
                    $tipo_semilla = ($muestraLab['sistema'] == 'fiscalizacion') ? trim($_POST['tipo_semilla']) : '';
                    # var_dump($muestra);
                    if ($tipo_semilla == 'importada' && $muestra['sistema'] == 'fiscalizacion') {
                        $muestraLab['tipo_semilla'] = (strtolower(trim($_POST['tipoSemilla'])) == 'importada') ? 1 : 0;
                        $muestraLab['especieImp'] = trim($_POST['especieImp']);
                        $muestraLab['variedadImp'] = trim($_POST['variedadImp']);
                        $muestraLab['categoriaImp'] = trim($_POST['categoriaImp']);
                        $muestraLab['origenImp'] = trim($_POST['origenImp']);
                        $muestraLab['cantidadImp'] = trim($_POST['cantidadImp']);
                        $muestraLab['certificadoImp'] = trim($_POST['certificadoImp']);
                        $muestraLab['aduanaImp'] = trim($_POST['aduanaIngresoImp']);
                        $muestraLab['destinoImp'] = trim($_POST['destinoSemillaImp']);
                        $muestraLab['distribucionImp'] = trim($_POST['adistribucionImp']);

                        $laboratorio -> setMuestraImportada($muestraLab);
                    } else {
                        $laboratorio -> setMuestra($muestraLab);
                    }

                    $isolicitud = Solicitud::getIdSolicitudByNroSolicitud($campo);
                    #echo 'isol = '.$isolicitud;exit;
                    if (!DBConnector::nroError()) {
                        Solicitud::updateEstadoMuestra($isolicitud);
                        echo 'OK';
                    } else
                        echo DBConnector::mensaje();
                    break;
            }
            break;
        case 'eliminar' :
            break;
        case 'buscar' :
            switch ($pagina) {
                case 'calibre' :
                    //lista de los tipos de semilla disponibles segun el analisis
                    Tipo_semilla::getTipoSemillaNombre();
                    $json['id'] = array();
                    $json['tipo'] = array();

                    while ($row = DBConnector::objeto()) {
                        if ($row -> tipo != 'sin informacion') {
                            array_push($json['id'], $row -> id);
                            array_push($json['tipo'], ucfirst($row -> tipo));
                        }
                    }

                    echo json_encode($json);
                    break;
                /*
                 * 1 ya tiene resultados
                 * 2 falta ingreso de resultados
                 * 3 no existe en laboratorio
                 * */
                case 'campo' :
                    $campo = trim($_POST['nrocampo']);
                    $result = $laboratorio -> getCampo($campo);

                    switch ($result) {
                        case 1 :
                            echo 1;
                            break;
                        case 2 :
                            echo 2;
                            break;
                        default :
                            echo 3;
                            break;
                    }
                    break;

                case 'prueba' :
                    $campo = trim($_GET['nrocampo']);

                    $laboratorio -> getMuestra($campo);
                    while ($row = DBConnector::resultado()) {
                        //fecha_recepcion,nro_campo,variedad,origen,semilla,analisis
                        $json['fecha'] = $dates -> cambiar_formato_fecha($row -> fecha_recepcion);
                        $json['campo'] = $row -> nro_campo;
                        $json['variedad'] = $row -> variedad;
                        $json['origen'] = $row -> origen;
                        $json['cultivo'] = $row -> semilla;
                        $json['tipo'] = $row -> analisis;
                        $json['res'] = 'OK';
                    }
                    if (DBConnector::filas())
                        echo json_encode($json);
                    else {
                        $json['res'] = 'error';
                        echo json_encode($json);
                    }
                    break;

                //filtro de busqueda de muestras
                case 'filtro' :
                    $opc = trim($_GET['opc']);
                    switch ($opc) {
                        case 'buscar' :
                            $search = trim($_GET['search']);
                            $json['nro_campo'] = array();
                            $json['fecha'] = array();
                            $json['semillerista'] = array();
                            $json['cultivo'] = array();
                            $json['variedad'] = array();
                            $json['estado'] = array();

                            if (!empty($search)) {
                                Laboratorio::getMuestrasFilter($search);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($json['nro_campo'], $obj -> nro_solicitud);
                                    array_push($json['fecha'], $obj -> sistema);
                                    array_push($json['semillerista'], utf8_encode($obj -> semillerista));
                                    array_push($json['cultivo'], $obj -> departamento);
                                    array_push($json['variedad'], $obj -> provincia);
                                    array_push($json['municipio'], $obj -> municipio);
                                    array_push($json['estado'], utf8_encode($obj -> comunidad));
                                }
                            }
                            $json['total'] = count($json['nro_campo']);
                            break;

                        case 'resultado' :
                            break;
                    }

                    echo json_encode($json);
                    break;
                case 'ilaboratorio' :
                    //devuelve el id de la muestra de laboratorio segun el id de cosecha
                    $id = intval($_GET['icosecha']);

                    $laboratorio -> getIdLaboratorioByIdCosecha($id);
                    $json = array();
                    while ($fila = DBConnector::resultado()) {
                        $json['id'] = $fila -> id_laboratorio;
                        $json['estado'] = $fila -> estado;
                    }
                    echo json_encode($json);
                    break;
                case 'lista_campos' :
                    $sistema = strip_tags($_GET['sistema']);
                    $isistema = Sistema::getIdSistema($sistema);
                    $laboratorio -> getListCampos($isistema);
                    $campos['campos'] = array();
                    if (DBConnector::filas()) {
                        while ($obj = DBConnector::objeto()) {
                            array_push($campos['campos'], $obj -> nro_campo);
                        }
                        $campos['msg'] = 'OK';
                        $campos['total'] = count($campos['campos']);
                        echo json_encode($campos);
                    } else {
                        $campos['total'] = 0;
                        echo json_encode($campos);
                    }

                    break;
                case 'muestras' :
                    //lista todas las muestras de laboratorio
                    session_start();
                    $campo = (isset($_POST['nrocampo'])) ? intval(trim($_POST['nrocampo'])) : '';

                    $laboratorio -> getMuestrasLaboratorioByCampo2($campo);

                    if (DBConnector::filas())
                        include_once '../vista/laboratorio/muestras.html.php';
                    else {
                        echo '<a>La busqueda no produjo ningun resultado</a>';
                    }
                    break;
                //busca nro de campo para ingresar muestra
                case 'muestraCampo' :
                    $sistema = trim($_GET['sistema']);
                    $campo = is_int($_GET['campo']) ? intval(trim($_GET['campo'])) : trim($_GET['campo']);
                    if ($sistema == 'certificacion') {
                        $laboratorio -> getMuestrasLaboratorio($campo, $sistema);
                        if(DBConnector::filas()){
                            $obj = DBConnector::objeto();
                            $json['cultivo'] = $obj -> cultivo;
                            $json['variedad'] = utf8_encode($obj -> variedad);
                            $json['comunidad'] = $obj -> comunidad;
                        }
                        $laboratorio -> getCamposAcondicionamientoForMuestra($campo);
                        if(DBConnector::filas()){
                            $obj = DBConnector::objeto();
                            $json['cultivo'] = $obj -> cultivo;
                            $json['variedad'] = utf8_encode($obj -> variedad);
                            $json['comunidad'] = $obj -> comunidad;
                        }
                    } elseif ($sistema == 'laboratorio') {
                        $laboratorio -> getFullMuestrasLaboratorio($campo);
                        if(DBConnector::filas()){
                            $obj = DBConnector::objeto();
                            $json['cultivo'] = $obj -> cultivo;
                            $json['variedad'] = utf8_encode($obj -> variedad);
                            $json['comunidad'] = $obj -> comunidad;
                        }
                    }
                    
                    echo json_encode($json);
                    break;
                case 'respruebas' :
                    if (!empty($_POST['buscar'])) {
                        $campo = is_int($_POST['buscar']) ? intval($_POST['buscar']) : trim($_POST['buscar']);
                        $laboratorio -> getBuscarResultadoMuestras($campo);

                        include '../vista/laboratorio/form_resultado.html.php';
                    } else {
                        echo 'Texto invalido';
                    }
                    break;

                case 'tipo_semilla' :
                    //lista de los tipos de semilla disponibles segun el analisis
                    Tipo_semilla::getTipoSemillaNombre();
                    $json['id'] = array();
                    $json['tipo'] = array();

                    while ($row = DBConnector::objeto()) {
                        if ($row -> tipo != 'sin informacion') {
                            array_push($json['id'], $row -> id);
                            array_push($json['tipo'], ucfirst($row -> tipo));
                        }
                    }

                    echo json_encode($json);
                    break;
            }
            break;
        case 'actualizar' :
            break;
        case 'ver' :
            switch ($pagina) {
                case 'listarcampos' :
                    //lista los campos que no cuentan con una muestra de laboratorio
                    $sistema = strip_tags(trim($_GET['stm']));
                    $cosechas -> getNroCamposLab($sistema);
                    $json = array();
                    #var_dump(DBConnector::resultado());exit;
                    while ($fila = DBConnector::resultado()) {
                        if ($fila -> total) {
                            array_push($json, $fila -> nro_campo);
                        } else {
                            $json = 0;
                        }
                    }
                    echo json_encode($json);
                    break;
                case 'listapruebas' :
                    //listar todas las pruebas   revisar
                    //numero de solicitud o numero de campo
                    $nro = intval(trim($_POST['nro']));
                    $sistema = trim($_POST['stm']);
                    $laboratorio -> getListMuestras($nro, $sistema);

                    if (DBConnector::filas())
                        include_once '../vista/laboratorio/resultados_laboratorio.html.php';
                    else {
                        echo 'No hay resultado';
                    }
                    break;
                case 'pruebas' :
                    //todas la pruebas de laboratorio completas y las que faltan ingresar resultados de analisis
                    $nivel = isset($_GET['area']) ? trim($_GET['area']) : '';
                    $muestras = array();
                    $laboratorio -> getMuestras();
                    include '../vista/laboratorio/resultados.html.php';
                    break;
                case 'resultados' :
                    //$laboratorio -> getResultadoMuestras();
                    $id_muestra = array();
                    $recepcion = array();
                    $resultado = array();
                    $nro_campo = array();
                    $cultivo = array();
                    $lugar_origen = array();
                    $cantidad = array();
                    $certificado = array();
                    $lote = array();
                    $germinacion = array();
                    $pureza = array();
                    $humedad = array();
                    $observacion = array();

                    $laboratorio -> getMuestraImportada();
                    #var_dump(DBConnector::filas());
                    if (DBConnector::filas() >= 1) {
                        while ($obj = DBConnector::objeto()) {
                            array_push($id_muestra, $obj -> id_muestra);
                            array_push($recepcion, $obj -> fecha_recepcion);
                            array_push($resultado, $obj -> fecha_resultado);
                            array_push($nro_campo, $obj -> nro_campo);

                            array_push($cultivo, $obj -> cultivo);
                            array_push($lugar_origen, $obj -> lugar_origen);
                            array_push($cantidad, $obj -> cantidad);
                            array_push($certificado, $obj -> certificado);

                            array_push($lote, '-');
                            array_push($germinacion, $obj -> germinacion);
                            array_push($pureza, $obj -> pureza);
                            array_push($humedad, $obj -> humedad);
                            array_push($observacion, $obj -> observacion);
                        }
                    }
                    $laboratorio -> getMuestraCertificada();
                    #var_dump(DBConnector::filas());
                    while ($obj = DBConnector::objeto()) {
                        array_push($id_muestra, $obj -> nro_analisis);
                        array_push($recepcion, $obj -> fecha_recepcion);
                        array_push($resultado, $obj -> fecha_resultado);
                        array_push($nro_campo, $obj -> nro_campo);

                        array_push($cultivo, $obj -> cultivo);
                        array_push($lugar_origen, '-');
                        array_push($cantidad, '-');
                        array_push($certificado, '-');

                        array_push($lote, $obj -> lote);
                        array_push($germinacion, $obj -> germinacion);
                        array_push($pureza, $obj -> pureza);
                        array_push($humedad, $obj -> humedad);
                        array_push($observacion, $obj -> observacion);
                    }
                    $nro_muestras = count($id_muestra);
                    //echo $nro_muestras;
                    #var_dump($id_muestra,$recepcion,$resultado,$nro_campo,$cultivo,$lugar_origen,$cantidad,$certificado,$lote,$germinacion,$pureza,$humedad,$observacion);
                    include '../vista/laboratorio/form_resultado.html.php';
                    break;
                case 'inMuestra' :
                    $nro_analisis = Laboratorio::getAnalisisCount();
                    #echo $nro_analisis;
                    // introduccion de resultados de analisis de una muestra especifica
                    $idlab = trim($_POST['id']);
                    $nroSolicitudMuestra = trim($_POST['nro']);

                    $imuestra = Muestra_laboratorio::getIdMuestraByNroSolicitud($nroSolicitudMuestra,$idlab);
                    //id muestra
                    #echo 'imuestra = '.$imuestra;
                    $sistema = Laboratorio::getMuestraLaboratorioBySistema($idlab);
                    $isemilla = Semilla::getIdSemillaByMuestraNroCampo($idlab);
                    $isolicitud = Tiene::getIdSolicitud($isemilla);
                    $nosolicitud = $nroSolicitudMuestra;
                    $nombre = Solicitud::getNombreByIdSolcitud($isolicitud);
                    $apellido = Solicitud::getApellidoByIdSolcitud($isolicitud);
                    $cultivo = Semilla::getAllCultivo($isemilla);
                    $tsemilla = Tipo_semilla::getNombreTipoSemillaById(intval($_POST['tipo']));
                    $nroMuestra = Muestra_laboratorio::getLastIdMuestra();
                    $lote = $dates -> getNroLoteFiscal($nombre, $apellido, $cultivo, $nroMuestra, 1);
                    #echo "lote =".$lote."<br>";
                    $laboratorio -> getMuestraById2($nosolicitud);
                    #var_dump(DBConnector::filas());
                    if (DBConnector::filas()) {
                        # 1:certificacion | 2: fiscalizacion
                        if ($tsemilla == 'Importada')
                            include '../vista/laboratorio/ingresar_resultado_muestras_importada.html.php';
                        else {
                            session_start();
                            Categoria::getListPorcentajeCategorias();
                            $lst_categorias = array();
                            while ($fila = DBConnector::objeto()) {
                                array_push($lst_categorias, $fila -> nombre_categoria);
                            }
                            $porcentajes = array();
                            $lst_humedad = $lst_germinacion = $lst_pureza = array();
                            foreach ($lst_categorias as $key => $categoria) {
                                Categoria::getCategoriaPorcentaje($categoria);
                                while ($obj = DBConnector::objeto()) {
                                    array_push($lst_humedad, $obj -> humedad);
                                    array_push($lst_germinacion, $obj -> germinacion);
                                    array_push($lst_pureza, $obj -> pureza);
                                }
                            }

                            $laboratorio -> getMuestraById2($nosolicitud);

                            include '../vista/laboratorio/ingresar_resultado_muestras.html.php';
                        }
                    } else {
                        echo 'No existen muestras para ser procesadas';
                    }
                    break;
                case 'muestras' :
                    //ver resultados de analisis de una muestra
                    $idlab = intval($_POST['id']);

                    $laboratorio -> verMuestraById($idlab);
                    if (DBConnector::filas())
                        include '../vista/laboratorio/resultado_muestra.html.php';
                    else {
                        echo "Error";
                    }
                    break;
            }
            break;
    }
}
?>