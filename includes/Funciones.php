<?php
class Funciones {
    /**
     * generador de contrasenias
     */
    function generar_password($length = 10) {
        // variable que tiene los caracteres permitidos para la contrasenia
        $charset = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789';

        // inicio la variable  para despues guardar la contrasenia
        $passwd = '';

        for ($i = 0; $i < $length; $i++) {
            $rand = rand() % strlen($charset);

            //obtengo un valor de la cadena y lo agrego a mi variable
            $passwd .= substr($charset, $rand, 1);
        }
        return $passwd;
    }

    /**
     * cambia la fecha de dd-mm-aaaa al estilo de mysql aaaa-mm-dd
     */
    public static function cambiar_tipo_mysql($fecha) {
        $nueva_fecha = '';
        //inicio la variable para despues guardar la fecha tipo mySql
        $dia = substr($fecha, 0, 2);
        //obtengo el dia
        $mes = substr($fecha, 3, 2);
        //obtengo el mes
        $ano = substr($fecha, 6, 9);
        //obtengo el a�o
        //escribo la fecha segun mySql
        $nueva_fecha = $ano . "-" . $mes . "-" . $dia;

        return $nueva_fecha;
    }

    /**
     * cambia la fecha de dd-mm-aaaa al estilo de mysql aaaa-mm-dd
     * para solicitud
     */
    public static function cambiar_tipo_mysql_import($fecha) {
        return date("Y-m-d", strtotime($fecha)); 
    }
    /**
     * cambia la fecha de dd-mm-aaaa al estilo de mysql aaaa-mm-dd
     * para solicitud
     */
    public static function cambiar_tipo_mysql_importV2($fecha) {        
        return date("Y-m-d", strtotime($fecha)); 
    }

    /**
     * cambia la fecha de dd-mm-aaaa al estilo de mysql aaaa-mm-dd
     */
    public static function fecha_normal_to_mysql_for_import($fecha) {
        return date("Y-m-d", strtotime($fecha)); 
    }

    /**
     * sustituye el acento por un signo
     * @param   string   $cadena   cadena a sustituir
     * 
     * @return devuelve cadena decodificada
     * */
    public function cambiar_acentoBySigno($cadena) {
        $buscar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
    $reemplazar = array("!", "¡", "#", "?", "$", "¿", "%", "{", "&", "}", "(", ")");

        return utf8_decode(str_replace($buscar, $reemplazar, $cadena));
    }

    /**
     * sustituye el signo por un acento 
     * @param   string   $cadena   cadena a sustituir
     * 
     * @return devuelve cadena decodificada
     * */
    public function cambiar_signoByAcento($cadena) {
        $reemplazar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
        $buscar = array("!", "¡", "#", "?", "$", "¿", "%", "{", "&", "}", "(", ")");

        return utf8_decode(str_replace($buscar, $reemplazar, $cadena));
    }
    
        /**
     * sustituye el acento por un signo
     * @param   string   $cadena   cadena a sustituir
     * */
    public function cambiar_acentoBySignov2($cadena) {
        $buscar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
    $reemplazar = array("!", "¡", "#", "?", "$", "¿", "%", "{", "&", "}", "(", ")");

        return str_replace($buscar, $reemplazar, $cadena);
    }

    /**
     * sustituye el signo por un acento 
     * @param   string   $cadena   cadena a sustituir
     * */
    public function cambiar_signoByAcentov2($cadena) {
        $reemplazar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
        $buscar = array("!", "¡", "#", "?", "$", "¿", "%", "{", "&", "}", "(", ")");

        return str_replace($buscar, $reemplazar, $cadena);
    }

    /**
     * cambiar formato de fecha aaaa-mm-dd a otro dd-mm-aaaa
     */
    function cambiar_formato_fecha($fecha) {
        $temp = $fecha;
        // inicio la variable;
        $fecha = explode('-', $fecha);
        $ano = $fecha[0];
        $mes = $fecha[1];
        $dia = $fecha[2];

        if (strlen($ano) > 2)
            return $dia . "-" . $mes . "-" . $ano;
        else
            return $temp;
    }

    function obtener_anho($fecha) {
        $temp = explode('-', $fecha);

        return $temp[0];
    }

    /**
     * Determina la epoca de siembra segun el mes
     */
    function campania() {
        $verano = array(1, 2, 3, 4, 5, 6);
        $invierno = array(7, 8, 9, 10, 11, 12);
        $mes = date('n');
        foreach ($verano as $valor) {
            if ($mes == $valor)
                return 'Verano/' . date('y');
        }
        foreach ($invierno as $valor) {
            if ($mes == $valor)
                return 'Invierno/' . date('y');
        }
    }

    /**
     * cambia el signo la coma por el punto en la parte decimal
     */
    function cambiar_decimal($numero) {

        if (strstr($numero, ','))
            return preg_replace('#(\,)+#', '.', $numero);
        else
            return $numero;
    }

    /**
     * cambia punto decimal por coma  para mostrar
     * */
    function cambiar_punto_decimal($numero) {
        if (strstr($numero, '.'))
            return preg_replace('#(\.)+#', ',', $numero);
        else
            return $numero;
    }

    /**
     * buscar barra en numero de campo
     * */
    function buscaBarra($nroCampo) {

        return count(explode("/", $nroCampo));
    }

    /**
     * devolver el nro de solicitud segun /
     * */
    function devolverBarraSolicitud($noSolicitud) {
        $nro = explode("/", $noSolicitud);
        $contar = count($nro);
        #var_dump($noSolicitud);
        if ($contar == 2) {
            return $noSolicitud;
        } else {
            return $nro[0];
        }
    }

    /**
     * para fiscalizacion
     * devolver el nro de solicitud segun /
     * @version 2.0
     * */
    function devolverBarraSolicitudv2($noSolicitud) {
        $nro = explode("/", $noSolicitud);

        return $nro[0];
    }

    /**
     * introducir semparador de miles   TERMINAR
     * */
    function separador_miles($numero) {
        $posDot = strlen($num) - strpos($num, '.');
        $size = strlen($num);
        $difSize = $size - $posDot;
        $miles = substr($num, 0, $difSize);
        $decimales = substr($num, $difSize, $posDot);
        $decimales = $this -> cambiar_punto_decimal($decimales);
        $cantDigitos = strlen($miles) / 3;

        $a = $miles . $decimales;
        echo 'cad4:' . $a;
    }

    /**
     * obtener el nombre completo de la categoria
     */
    function obtener_categoria($cadena) {
        $categoria = substr($cadena, 0, 3);

        switch (strtolower($categoria)) {
            case 'reg' :
                $categoria = 'Registrada';
                break;

            case 'cer' :
                $categoria = 'Certificada';
                break;

            case 'fis' :
                $categoria = 'Fiscalizada';
                break;
        }
        return $categoria;
    }

    /**
     * Comprueba si existe o no un archivo/Directorio
     */
    function exist_dir_file($archivo) {
        return file_exists($archivo);
    }

    function noCache() {
        header("Expires: Wed, 01 Dec 2010 12:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * Busca palabras que contengan ' para q puedan ser insertados en la BD
     */
    function specialWord($cadena) {
        //$cadena = str_ireplace('\'', '\\\'', $cadena);
        if (is_array($cadena)) {
            foreach ($cadena as $key => $value) {
                $cadena[$key] = addslashes($value);
            }
        } else {
            $cadena = addslashes($cadena);
        }
        return $cadena;
    }

    /**
     * reemplazar vocales con acento y ñ Ñ por guion bajo
     * */
    public static function search_tilde($cadena) {
        $buscar = array('á', 'í', 'ú', 'ó', 'é', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú');
        $reemplazar = '_';

        if (is_array($cadena)) {
            foreach ($cadena as $key => $value) {
                $cadena[$key] = str_ireplace($buscar, $reemplazar, $cadena);
            }
        } else {
            $cadena = str_ireplace($buscar, $reemplazar, $cadena);
        }
        return $cadena;
    }
    /**
     * reemplazar vocales con acento y ñ Ñ por porcentaje
     * LLAMADO EN municipioImport
     * */
    public static function tildeByPercentage($cadena) {
        $buscar = array('á', 'í', 'ú', 'ó', 'é', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú');
        $reemplazar = '%';

        if (is_array($cadena)) {
            foreach ($cadena as $key => $value) {
                $cadena[$key] = str_ireplace($buscar, $reemplazar, $cadena);
            }
        } else {
            $cadena = str_ireplace($buscar, $reemplazar, $cadena);
        }
        return $cadena;
    }

    /**
     *
     */
    function campaniaAnterior($campania) {
        $cadena = explode('/', $campania);
        $epoca = $cadena[0];
        $anio = $cadena[1] - 1;

        $campania = implode('/', array($epoca, $anio));

        return $campania;
    }

    /**
     * function que reemplaza los acentos
     * **/
    function stVocales($string) {
        //con tilde
        $ctvocales = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú');
        //sin tilde
        $stvocales = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

        return str_replace($ctvocales, $stvocales, $string);
    }

    /**
     * buscar coma en la comunidad
     * */
    function delimiter($cadena) {
        $buscar = ',';
        if (stripos($cadena, $buscar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**elimina espacio en una lista de comunidades o localidades
     * y devuelve un array con los valores
     * */
    function addSpace($cadena) {
        $auxiliar = array();
        $temp = explode(',', $cadena);
        if (count($temp) > 1) {
            foreach ($temp as $valor) {
                array_push($auxiliar, trim($valor));
            }
        } else {
            array_push($auxiliar, $cadena);
        }
        return $auxiliar;
    }

    /**
     * fecha para importar
     * cambia del formato dd-mm-yy al formato yyyy-mm-dd
     * 10-28-09
     * */
    public static function getFechaImportar($fecha) {
        if ($fecha != '') {
            $findme = '-';
            $pos = strpos($fecha, $findme);
            if ($pos === false) {
                $temp_fecha = explode("/", $fecha);
                echo(count($temp_fecha) < 3) ? $fecha : $fecha . '=';
                if ($temp_fecha[2] >= '00' && $temp_fecha[2] <= '99') {
                    return "20" . $temp_fecha[2] . "-" . $temp_fecha[1] . "-" . $temp_fecha[0];
                } else {
                    return $temp_fecha[2] . "-" . $temp_fecha[1] . "-" . $temp_fecha[0];
                }
            } else {
                $temp_fecha = explode("-", $fecha);
                if ($temp_fecha[2] >= '00' && $temp_fecha[2] <= '99') {
                    return "20" . $temp_fecha[2] . "-" . $temp_fecha[1] . "-" . $temp_fecha[0];
                } else {
                    return $temp_fecha[2] . "-" . $temp_fecha[1] . "-" . $temp_fecha[0];
                }
            }
        } else {
            return '';
        }
    }

    /* fecha para importar
     * cambia del formato dd-mm-yy al formato yyyy-mm-dd
     * 10-28-09
     * */
    public static function getFechaImportarv2($fecha) {
        if ($fecha != '') {
            $pos = strpos($fecha, "/");
            if ($pos === TRUE)
                $fecha = str_replace("/", "-", $fecha);
            $timestamp = strtotime($fecha);
        } else {
            $timestamp = strtotime('');
        }
        return date('Y-m-d', $timestamp);
    }

    /**
     * devuelve  el inicio y final de una peticion por meses
     * */
    function getMesesTrimestres($opcion) {
        switch ($opcion) {
            case '1' :
                $mes['desde'] = '01';
                $mes['hasta'] = '03';
                break;
            case '2' :
                $mes['desde'] = '04';
                $mes['hasta'] = '06';
                break;
            case '3' :
                $mes['desde'] = '07';
                $mes['hasta'] = '09';
                break;
            case '4' :
                $mes['desde'] = '10';
                $mes['hasta'] = '12';
                break;
        }
        return $mes;
    }

    /**
     * devuelve el mes segun el numero
     * */
    function getNombreMes($imes) {
        switch($imes) {
            case '1' :
                $mes = "Enero";
                break;
            case '2' :
                $mes = "Febrero";
                break;
            case '3' :
                $mes = "Marzo";
                break;
            case '4' :
                $mes = "Abril";
                break;
            case '5' :
                $mes = "Mayo";
                break;
            case '6' :
                $mes = "Junio";
                break;
            case '7' :
                $mes = "Julio";
                break;
            case '8' :
                $mes = "Agosto";
                break;
            case '9' :
                $mes = "Septiembre";
                break;
            case '10' :
                $mes = "Octubre";
                break;
            case '11' :
                $mes = "Noviembre";
                break;
            case '12' :
                $mes = "Diciembre";
                break;
        }
    }

    /**
     * devuelve el mes inicio de mesen formamto xx-xx-xxxx
     * */
    function getInicioMes($imes) {
        $anio = date('Y');
        switch($imes) {
            case '1' :
                $mes = "01-01-" . $anio;
                break;
            case '2' :
                $mes = "01-02-" . $anio;
                break;
            case '3' :
                $mes = "01-03-" . $anio;
                break;
            case '4' :
                $mes = "01-04-" . $anio;
                break;
            case '5' :
                $mes = "01-05-" . $anio;
                break;
            case '6' :
                $mes = "01-06-" . $anio;
                break;
            case '7' :
                $mes = "01-07-" . $anio;
                break;
            case '8' :
                $mes = "01-08-" . $anio;
                break;
            case '9' :
                $mes = "01-09-" . $anio;
                break;
            case '10' :
                $mes = "01-10-" . $anio;
                break;
            case '11' :
                $mes = "01-11-" . $anio;
                break;
            case '12' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-12-" . $anio;
                break;
        }
        return $mes;
    }

    /**
     * devuelve el mes inicio de mesen formamto xx-xx-xxxx
     * */
    function getFinMes($imes) {
        $anio = date('Y');
        switch($imes) {
            case '1' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-01-" . $anio;
                break;
            case '2' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-" . date("m", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-" . $anio;
                break;
            case '3' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-03-" . $anio;
                break;
            case '4' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-04-" . $anio;
                break;
            case '5' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-05-" . $anio;
                break;
            case '6' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-06-" . $anio;
                break;
            case '7' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-07-" . $anio;
                break;
            case '8' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-08-" . $anio;
                break;
            case '9' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-09-" . $anio;
                break;
            case '10' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-10-" . $anio;
                break;
            case '11' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-11-" . $anio;
                break;
            case '12' :
                $mes = date("d", (mktime(0, 0, 0, $imes + 1, 1, $anio) - 1)) . "-12-" . $anio;
                break;
        }
        return $mes;
    }

    /**
     * incrementar el numero de solicitud
     * */
    function addNroSolicitud($solicitud) {
        $temp = explode("/", $solicitud);

        return $temp[0] + 1 . "/" . $temp[1];
    }

    /**
     * buscar palabras con acento u otros signos
     * y las reemplaza con guion bajo
     * */
    function modificar($cadena) {
        $search = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü', 'ü', 'ä', 'Ä', 'Ë', 'ë', 'ï', 'Ï', 'Ö', 'ö');
        $cadena = str_ireplace($search, '_', $cadena);
        return $cadena;
    }

    /**
     * reemplaza los acentos por la vocal sin acento
     * */
    public static function reemplazarAcento($cadena) {
        $search = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú');
        $reemplazar = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
        $cadena = str_ireplace($search, $reemplazar, $cadena);
        return $cadena;
    }

    /**
     * devuleve el nombre de la carpeta
     * */
    public function getBaseName($texto) {
        $needle = "\\";
        $haystack = $texto;

        $pos = strripos($haystack, $needle);

        if ($pos === false) {
            #echo "Sorry, we did not find ($needle) in ($haystack)";
            return 'ERROR';
        } else {
            return substr($haystack, ($pos + 1));
        }
    }

    /**permisos de usuarios
     * @param integer $numero permisos
     *
     * @return array array de permisos
     * */
    function getCRUD($numero) {
        $crud['c'] = intval($numero / 1000);
        $numero = intval($numero % 1000);
        $crud['r'] = intval($numero / 100);
        $numero = intval($numero % 100);
        $crud['u'] = intval($numero / 10);
        $crud['d'] = intval($numero % 10);

        return $crud;
    }

    /**
     * devuelve el valor de un campo
     * nro, semillera y/o semillerista
     * */
    function getDatosBusqueda($string, $key) {
        switch ($key) {
            case 'numero' :
                break;
            case 'semillera' :
                break;
            case 'semillerista' :
                $partes = explode(" ", $string);
                //console.log(partes.length);
                if (count($partes) > 4) {
                    $nombre = $partes[0] + ' ' + $partes[1];
                    $apellido = $partes[2];
                } else {
                    $nombre = $partes[0];
                    $apellido = $partes[1];
                }
                break;
        }
    }

    /**
     * obtener nombre y apellido de semillerista
     * */
    function getNombreApellido($fullname) {
        $piezas = explode(' ', $fullname);
        switch (count($piezas)) {
            case 4 :
                $productor['nombre'] = $piezas[0] . ' ' . $piezas[1];
                $productor['apellido'] = $piezas[2];
                break;
            case 3 :
                $productor['nombre'] = $piezas[0] . ' ' . $piezas[1];
                $productor['apellido'] = $piezas[2];
                break;
            case 2 :
                $productor['nombre'] = $piezas[0];
                $productor['apellido'] = $piezas[1];
                break;
            default :
                $productor['nombre'] = $fullname;
                break;
        }
        return $productor;
    }

    public static function getNombreApellidoImportar($fullname) {

        $piezas = explode(' ', $fullname);
        if (count($piezas) == 2) {
            $temporal = $piezas[0] . ' ' . $piezas[1];
            if (strtolower($piezas[0]) == "grupo") {
                $productor['nombre'] = 'Grupo';
                $productor['apellido'] = $piezas[1];
                return $productor;
            } else {
                $productor['nombre'] = $piezas[0];
                $productor['apellido'] = $piezas[1];
                return $productor;
            }
        } else {
            if (count($piezas) == 3) {
                $productor['nombre'] = $piezas[0];
                $productor['apellido'] = $piezas[1] . ' ' . $piezas[2];
                return $productor;
            } elseif (count($piezas) == 4) {
                $productor['nombre'] = $piezas[0] . ' ' . $piezas[1];
                $productor['apellido'] = $piezas[2] . ' ' . $piezas[3];
                return $productor;
            }
        }
    }

    /**
     * crea el formato correcto de una solicitud segun el numero
     * */
    function getFormatoNumeroSolicitud($number) {
        $tmp_number = intval($number);
        if (($tmp_number > 0) && ($tmp_number <= 9)) {
            $aux = '00' . $tmp_number;
        } elseif (($tmp_number >= 10) && ($tmp_number <= 99)) {
            $aux = '0' . $tmp_number;
        } else {
            $aux = $tmp_number;
        }

        return $aux;
    }

    /**
     * contar las solicitudes segun el separador
     * */
    function getNroSolicitudes($cadena) {
        $piezas = explode(",", $cadena);

        return $piezas;
    }

    /**
     * separar el numero del año de fiscalizacion
     * */
    function getNextIdSolicitud($cadena) {
        $numero = explode("/", $cadena);

        return (intval($numero[0]) + 1) . "/" . $numero[1];
    }

    /**
     * devuelve la categoria y generacion
     * @param string $categoria   nombre de la categoria y generacion
     * */
    function getCategoriaGeneracion($categoria) {

        $piezas = explode("-", $categoria);
        if ($piezas[1] === '1') {
            $generacion = 'Primera';
        } elseif ($piezas[1] === '2') {
            $generacion = 'Segunda';
        } elseif ($piezas[1] === '3') {
            $generacion = 'Tercera';
        } else {
            $generacion = 'B';
        }

        return array("categoria" => $piezas[0], "generacion" => $generacion);
    }

    /**
     * cuenta los municipios o localidades existen
     * */
    function contarMuniLocal($cadena) {
        return count($cadena);
    }

    /**
     * obtener el primer digito
     * */
    function getZeroNumber($cadena, $parcelas) {
        $pzs = explode("/", $cadena);
        $subpzs = explode("0", $pzs[0]);
        if ($subpzs[0] == 0)
            $temp = 0;
        else
            $temp = '';
        $i = 1;
        while ($i <= $parcelas) {
            $result[$i] = $temp . $subpzs[1] . "/" . $pzs[1];
            $subpzs[1]++;
            $i++;
        }
        return $result;
    }

    /**
     * nombre de la carpeta
     *
     * */
    public static function reverse_strrchr($haystack, $needle) {
        $pos = strrpos($haystack, $needle);
        if ($pos === false) {
            return $haystack;
        }
        return substr($haystack, 0, $pos + 1);
    }
    /**
     * agrega ceros a la izquierda segun el nro
     * */
    function getNroSolicitud ($nro){
        $contar = strlen($nro);
        switch ($contar) {
            case '1':
                return '00'.$nro;
                break;
            
            case '2':
                return '0'.$nro;
                break;
            default:
                return $nro;
                break;    
        }
    }

    /**
     * armado de lote segun nombre, apellido,cultivo,nro solicitud
     * para fiscalizacion
     * */
    function getNroLoteFiscal($nombre, $apellido, $cultivo, $nosolicitud, $flag = 2) {
        $pos = strpos($nombre, " (");
        $pos2 = strpos($apellido, " (");
        if ($pos)
            $nombre = trim(substr($nombre, 0, $pos));
        if ($pos2)
            $apellido = trim(substr($apellido, 0, $pos2));
        $i = 1;
        $j = 0;
        $inicial = '';
        //obtener iniciales de nombre
        $nombre = explode(' ', $nombre);
        while ($i <= count($nombre)) {
            $inicial .= substr($nombre[$j], 0, 1);
            $i++;
            $j++;
        }
        $i = 1;
        $j = 0;
        //obtener iniciales de apellido
        $apellido = explode(' ', $apellido);
        while ($i <= count($apellido)) {
            $inicial .= substr($apellido[$j], 0, 1);
            $i++;
            $j++;
        }

        $inicial = strtoupper($inicial) . '-';
        if ($flag == 2) {

            $temp_nosolicitud = explode('/', $nosolicitud);

            $inicial .= $temp_nosolicitud[0] . '-';
            //listar cultivos
            Cultivo::getInicialesCultivos();
            $lst_cultivos = array();
            $lst_iniciales = array();
            while ($row = DBConnector::objeto()) {
                array_push($lst_cultivos, $row -> cultivo);
                array_push($lst_iniciales, $row -> iniciales);
            }
            //agregar iniciales de cultivo
            foreach ($lst_cultivos as $id => $nombre) {
                if (strtolower($nombre) == strtolower($cultivo)) {
                    $inicial .= $lst_iniciales[$id] . '-' . $temp_nosolicitud[1];
                    break;
                }
            }
        } else {
            $inicial .= $nosolicitud . '-';
            //listar cultivos
            Cultivo::getInicialesCultivos();
            $lst_cultivos = array();
            $lst_iniciales = array();
            while ($row = DBConnector::objeto()) {
                array_push($lst_cultivos, $row -> cultivo);
                array_push($lst_iniciales, $row -> iniciales);
            }
            //agregar iniciales de cultivo
            foreach ($lst_cultivos as $id => $nombre) {
                if (strtolower($nombre) == strtolower($cultivo)) {
                    $inicial .= $lst_iniciales[$id] . '-' . date('y');
                    break;
                }
            }
        }
        return $inicial;
    }

}
?>