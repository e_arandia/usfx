<?php
require ('../fpdf.php');

class PDF extends FPDF {
    var $logo;
    var $font;
    var $style;
    var $size;
    var $title;
    var $align;
    var $position;
   /**
     * Page header
     * @param string $logo nombre de logo
     * @param string $font  nombre de fuente
     * @param integer $size tamanho de fuente (10|12|13|15|17)
     * @param string  $style  estilo de fuente (B|I|U)
     * @param string  $title  titulo de pagina
     * @param string  @align  alinear texto (L|C|R)
     * */
    public function __construct($logo, $font, $style, $size, $title, $align, $position) {
        $this -> logo = $logo;
        $this -> font = $font;
        $this -> style = $style;
        $this -> size = $size;
        $this -> title = $title;
        $this -> align = $align;
        $this -> position = $position;
    }

    function Header() {
        // Logo
        $this -> Image($this -> logo, 10, 6, 30);
        // Arial bold 15
        $this -> SetFont($this -> font, $this -> style, $this -> size);
        // Move to the right
        $this -> Cell(80);
        // Title
        $this -> Cell(30, 10, $this -> title, 1, 0, $this -> align);
        // Line break
        $this -> Ln(20);
    }

    /**
     * Page footer
     *
     * */
    function Footer($position, $font, $style, $size, $align) {
        // -15 => Position at 1.5 cm from bottom
        $this -> SetY($this -> position);
        // Arial italic 8
        $this -> SetFont($this -> font, $this -> style, $this -> ize);
        // Page number
        $this -> Cell(0, 10, 'Pagina ' . $this -> PageNo() . '/{nb}', 0, 0, $this -> align);
    }

}

// Instanciation of inherited class
$pdf = new PDF();
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> SetFont('Times', '', 12);
for ($i = 1; $i <= 40; $i++)
    $pdf -> Cell(0, 10, 'Printing line number ' . $i, 0, 1);
$pdf -> Output();
?>
