<?php
session_start();
$value = 'Sistema de Certificacion y Fiscalizacion de Semillas';
setcookie("SICEFI", $value, time(159874));

include 'includes/Funciones.php';
include_once 'config/Setting.php';

$cache = new Funciones();
$cache -> noCache();
if (!empty($_SESSION['usr_login'])) {
    header('Location:index.php');
}
?>
        <!--
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, charset=utf-8">
        -->
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
        <html>
        <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link href="js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/style.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/jquery-ui.css"?> rel="stylesheet">
        <style>
            body {
                font: 62.5% "Trebuchet MS", sans-serif;
                margin: 50px;
            }
            .demoHeaders {
                margin-top: 2em;
            }
            #dialog-link {
                padding: .4em 1em .4em 20px;
                text-decoration: none;
                position: relative;
            }
            #dialog-link span.ui-icon {
                margin: 0 5px 0 0;
                position: absolute;
                left: .2em;
                top: 50%;
                margin-top: -8px;
            }
            #icons {
                margin: 0;
                padding: 0;
            }
            #icons li {
                margin: 2px;
                position: relative;
                padding: 4px 0;
                cursor: pointer;
                float: left;
                list-style: none;
            }
            #icons span.ui-icon {
                float: left;
                margin: 0 4px;
            }
            .fakewindowcontain .ui-widget-overlay {
                position: absolute;
            }
            select {
                width: 200px;
            }
        </style>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery-ui.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.numeric.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.login.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.form.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.funciones.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/masFunciones.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/sha1-min.js"?>></script>
        <script type="text/javascript" src="js/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/bootstrap/js/bootstrap.min.js"></script>

        <link  type="image/png" rel="shortcut icon" href="images/favicon.ico"/>
    </head>
    <body>
        <div id="login">
            <div id="logo"></div>
            <div class="reg">
                <?php
                include 'vista/confirmacion/mensaje.html';
                ?>
                <div class="ui-widget error" style="display: none">
                    <div class="ui-state-error ui-corner-all" style="left: 25em;
                    padding: 0 0.7em;
                    position: relative;
                    width: 24em;">
                        <p class="error">

                        </p>
                    </div>
                </div>
                <form id="autForm"  class="login" action="control/index.php" method="post" autocomplete="off">
                    <div style="padding-bottom: 2%; position: absolute; margin-bottom: 0.7em; left: 0%; top: 0.1%; border-top-left-radius: 10px; border-top-right-radius: 10px; background-color: rgb(31, 62, 88); text-align: center; right: 10%; width: 100%; height:18%;padding-top: 3%;">
                        <label style="position: absolute; left: 20%;">SISTEMA DE INFORMACION </label>
                        <label style="position: absolute; top: 55%; left: 13%;">CERTIFICACION Y FISCALIZACION</label>
                    </div>
                    <hr>
                    <div style="margin-bottom: 0.7em; margin-top: 2em;">
                        <label style="margin-right:5%">Usuario</label>
                        <input type="text" style="color:#808080"  name="usuario" id="ulogin" class="input log-user" size="20" autocomplete="off"/>
                        <span id="errorU">Debe escribir un nombre de usuario</span>
                    </div>
                    <div style="margin-bottom: .7em;">
                        <label>Contrase&ntilde;a</label>
                        <input type="password" style="color:#808080" name="clave" id="upass" class="input log-user" size="20" autocomplete="off"/>
                        <span id="errorP">Debe escribir una contrase&ntilde;a</span>
                    </div>
                    <div class="submit" style="width:20%;position:absolute;left:69%;top:85%">
                        <button class="btn btn-default" id="enviar-solicitud">
                            <span class="glyphicon glyphicon-log-in"></span>
                            Ingresar
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </body>
</html>