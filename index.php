<?php
session_start();
if (empty($_SESSION['usr_login'])) {
    $_SESSION['usr_nombre'] = 'invitado';
    $_SESSION['usr_login'] = '';
    $_SESSION['usr_nivel'] = 0;
    $_SESSION['usr_area'] = 0;

    header('Location:home.php');
}
//echo $_SESSION['usr_iarea'];
include 'includes/Funciones.php';
include_once 'config/Setting.php';
session_name('INIAF');

$cache = new Funciones();

$cache -> noCache();
?>
<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href=<?php echo URL.HOME_FOLDER.CSS."/tableSorter.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href="js/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/style.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/tables.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/dialogos.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/registro.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/hint.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/icons.css"?> rel="stylesheet" type="text/css" media="screen" />
        
        <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" media="screen" />
        
        <!-- estilo para la paginacion de resultados-->
        <link href=<?php echo URL.HOME_FOLDER.CSS."/paginar.css"?> rel="stylesheet" type="text/css" media="screen" />
        <link href=<?php echo URL.HOME_FOLDER.CSS."/jquery-ui.css"?> rel="stylesheet" type="text/css" media="screen" />
        <title>Sistema de Informaci&oacute;n de Certificaci&oacute;n y Fiscalizaci&oacute;n de Semillas</title>
        
        <script src=<?php echo URL.HOME_FOLDER.JS."/jquery.js"?> type="text/javascript"></script>     
        <script type="text/javascript" src="js/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery-ui.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.numeric.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.form.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.buscar.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.funciones.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.jeditable.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/sistema.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/masFunciones.js"?>></script>        
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/scrollfix.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.uitablefilter.js"?>></script>
        <script type="text/javascript" src=<?php echo URL.HOME_FOLDER.JS."/jquery.tablesorter.js"?>></script>       

        <link href=<?php echo URL.HOME_FOLDER.IMG."/favicon.ico"?> rel="shortcut icon" type="image/png">

        <style>
            span.icon:before {
                font-family: 'Glyphicons Halflings';
                content: "\f015";
            }
        </style>
    </head>
    <body class="principal">
        <?php
        echo "<input type='hidden' name='area' class='ulogin' value='" . $_SESSION['usr_area'] . "'/>";
        ?>
        <div id="header">
            <div class="sicfis">
                <label style="position: absolute; letter-spacing: 1em; word-spacing: 0.5em; margin-left: 4%;">sistema de informacion</label>
                <label style="position: absolute; letter-spacing: 1em; margin-top: 2%; word-spacing: 0.5em; margin-left: -3.5%;">certificacion y fiscalizacion</label>
            </div>
        </div>
        <div style="float: right; position: absolute; top: 1%; right: 0.5%;">
            <img src="images/logoPagina.png" style="background-size:5px 5px">
        </div>
        <div style="font-weight: bold;position: absolute;top: 3.2em; left: 2.5em">
            <div id="inicio" class="img">
                <label class="inicio" style="position:relative; left:3.5em">Inicio</label>
            </div>
            <div id="usuarioOptions">
                <div id="usuario" class="img">
                    <label class="usuario" style="position:relative; left:1.5em">Datos de usuario</label>
                </div>
                <div id="sesion" class="img">
                    &nbsp;&nbsp;
                </div>
                <label class="img" style="left: 18.5em;position: relative;top: 2em;">Salir</label>
            </div>
            <div >
                <label style="left: 50em;position: relative;top: 0.5em;"">GESTION : <?php echo ' '.date('Y');?></label>
            </div>
            <div >
                <label style="left: 80em;position: relative;top: -1.3em;"">USUARIO : <?php echo $_SESSION['usr_login'];?></label>
            </div>
        </div>
        <!-- end saludo -->
        
        </div>
        <!-- end #header -->
        <!-- end #header-wrapper -->
        <div id="page">
        <div class="informar">
        </div>
            <?php
            include 'vista/error/aviso.php';
            ?>
            <div id="content">
                <div id="dialog-message" style="display: none">
                    <p>
                        <span id="ui-icon" class="ui-icon" style="float:left; margin:0 7px 50px 0;"></span>
                    </p>
                </div>

                <div class="post">
                    <?php
                        $navegacionNoPermitida = array('/', '.', '..', '/config', '/control', '/css', '/images', '/includes', '/js', '/libs', '/modelo', '/tmp', '/vista');
                        if (isset($_REQUEST['mdl']) && isset($_REQUEST['pag']) && !in_array($_GET['mdl'], $navegacionNoPermitida) && !in_array($_REQUEST['pag'], $navegacionNoPermitida) && !in_array($_SERVER['DOCUMENT_ROOT'], $navegacionNoPermitida)) {
                            $modelo = strip_tags($_REQUEST['mdl']);
                            if (!empty($modelo)) {
                                $ruta = 'control/' . $modelo . '.ctrl.php';
                                if (file_exists($ruta)) {
                                    include ($ruta);
                                } else {
                                    echo "<img src='images/loader_4.gif'/>";
                                }
                            } else {
                                echo '<h2>NO SE ENCUENTRA EL ARCHIVO</h2>';
                            }
                        } else {;
                            echo "<img src='images/loader_4.gif'/>";
                        }
                    ?>
                    
                        
                    
                </div>

                <div id="dialog-message2" style="display: none">
                    <p></p>
                </div>
            </div><!-- end #content -->
            <div id="sidebar">
                <ul>
                    <li>
                        <?php
                        if ($_SESSION['usr_nivel']) {
                            $nivel_user = $_SESSION['usr_nivel'];
                            switch ($nivel_user) {
                                case '1' :
                                    include 'vista/menu/certificacion.php';
                                    break;
                                case '2' :
                                    include 'vista/menu/fiscalizacion.php';
                                    break;
                                case '3' :
                                    include 'vista/menu/laboratorio.php';
                                    break;
                                case '10' :
                                    include 'vista/menu/administracion.php';
                                    break;
                            }
                        }
                        ?>
                    </li>
                </ul>
            </div>
            <!-- end #sidebar -->
            
            <div style="clear: both;">
                &nbsp;
            </div>
        </div>
        <!-- end #page -->
        <script type="text/javascript">
            $(document).ready(function() {
                $("#menuP").scrollFix();
            })
        </script>
    </body>
</html>
