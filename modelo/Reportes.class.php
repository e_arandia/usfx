<?php
//libreria pdf
class Reportes {
    var $provincia;
    //contiene el nombre de la provincia o todas
    var $municipio;
    //contiene el nombre del municipio o todos
    var $semillera;
    //contiene el nombre de la semillera o todas
    var $semillerista;
    //contiene el nombre del semillerista o todos

    public function semillerasDetalleProduccionCostos($desde, $hasta, $cultivo) {
        $query = "SELECT DISTINCT(semillera) FROM view_detalle_produccion_costos ";
        $query .= "WHERE cultivo LIKE '$cultivo%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' ";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    public function getCultivosGestionesAnteriores($gestion) {
        $query = "SELECT DISTINCT(cultivo) FROM view_detalle_produccion_costos ";
        $query .= "WHERE fecha>'" . $gestion . "-01-01' AND fecha<='" . $gestion . "-12-31' ";
        $query .= "ORDER BY cultivo";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * numero de productores de una semillera
     * */
    public static function getCantidadByCultivo($semillera, $cultivo, $desde, $hasta) {
        $query = "SELECT COUNT(*) AS cantidad FROM view_detalle_produccion_costos ";
        $query .= "WHERE  semillera LIKE '$semillera' AND cultivo LIKE '$cultivo%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' ";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * productores de una semillera y etapa en la q se encuentran
     * si semillera es 0 devuelve el total
     * @param $semillera mixed   nombre de la semillera
     * */
    public static function getProductoresBySemilleraCultivoAndRango($semillera, $cultivo, $desde, $hasta) {
        $query = "SELECT * FROM view_detalle_produccion_costos ";
        $query .= "WHERE  semillera LIKE '$semillera' AND cultivo LIKE '$cultivo%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') ";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de datos segun semillera para detalle de certificacion | fiscalizacion produccion y costos
     * */
    public function getDetalleProduccionBySemillera($semillera, $cultivo, $desde, $hasta) {
        $query = "SELECT * FROM view_detalle_produccion_costos ";
        $query .= "WHERE semillera LIKE '$semillera%' AND cultivo LIKE '$cultivo%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' ";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') ";
        }
        $query .= "ORDER BY semillerista";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    public function getProduccionCostosBySemillera($semillera) {
        $query = "SELECT * FROM view_detalle_produccion_costos WHERE semillera LIKE '$semillera'";

        DBConnector::ejecutar($query);
    }

    /**
     * reporte de superficies de produccion de semilla segun cultivo
     * @param  string $cultivo   nombre del cultivo
     *
     * devuelve la cantidad total de superficie (inscrita,retirad,rechazada,aprobada) del cultivo
     * */
    public function getSuperficieProduccionCultivos($cultivo, $desde, $hasta,$sistema=1) {

        $query = "SELECT SUM(inscrita) AS inscrita,SUM(retirada) AS retirada,SUM(rechazada) AS rechazada,SUM(aprobada) AS aprobada ";
        $query .="FROM view_reporte_superficies_produccion_semillas ";
        $query .= "WHERE cultivo LIKE '$cultivo%' AND id_sistema=$sistema ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') ";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query",DBConnector::mensaje();
        }
    }

    /**
     * reporte de superficies de produccion de semilla segun cultivo
     * @param  string $cultivo   nombre del cultivo
     *
     * devuelve la cantidad total de superficie (inscrita,retirad,rechazada,aprobada) del cultivo
     * */
    public function getSuperficieProduccionCultivosGraficos($cultivo, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista) {

        $query = "SELECT SUM(aprobada) AS aprobada FROM view_reporte_superficies_produccion_semillas ";
        $query .= "WHERE id_provincia LIKE '$provincia' AND id_municipio LIKE '$municipio' AND semillera LIKE '$semillera%' AND semillerista LIKE '$semillerista%' AND ";
        $query .= "cultivo LIKE '$cultivo%' ";
        $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    public function getTotalSuperficieProduccion($cultivo, $desde, $hasta) {
        $query = "SELECT SUM(aprobada) AS total FROM  view_reporte_superficies_produccion_semillas ";
        $query .= "WHERE cultivo = '$cultivo' AND (fecha >= '$desde' AND fecha <= '$hasta')";
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        return $obj -> total;
    }

    /**
     * 
     * */
    public static function getListCultivosSuperficieProduccion($desde, $hasta, $gestion = '',$sistema = 1) {

        $query = "SELECT DISTINCT(cultivo) FROM view_reporte_superficies_produccion_semillas WHERE cultivo NOT IN ('acondicionamiento','plantines','descanso','barbecho') ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '$gestion' AND id_sistema=$sistema";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query",DBConnector::mensaje();
        }
    }

    /**
     * contador de filas para reporte
     * @param date $desde  mes inicio
     * @param date $hasta  mes final
     * */
    public function getTotalSuperficieProduccionCultivos($desde, $hasta,$gestion,$sistema = 1) {
        $query = "SELECT COUNT(DISTINCT(cultivo)) AS total FROM view_reporte_superficies_produccion_semillas ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND gestion = '$gestion' AND id_sistema=$sistema";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta')";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query::",DBConnector::mensaje();    
        }else{
            $row = DBConnector::objeto();
            return $row -> total;    
        }        
    }

    /**
     * volumenes de produccion de un cultivo y variedad dterminada segun categoria
     * */
    public function getVolumenProduccionCultivo($cultivo, $variedad, $categoria, $desde, $hasta) {
        $query = "SELECT SUM (aprobada) AS aprobada FROM view_reporte_volumen_produccion_cultivos_categorias";
        $query .= "WHERE cultivo = '$cultivo' AND variedad = '$variedad' AND categoriaP LIKE 'categoria%' AND (mes >= '$desde' AND mes <= '$hasta')";

        DBConnector::ejecutar($query);
    }

    /**
     * metodo publico para graficar
     * @param   string   $grafico       nombre del tipo de grafico
     * @param   string   $tipo          tipo de grafico
     * @param   integer  $sistema       numero de sistema [1:certificacion - 2:fiscalizacion]
     * @param   date     $desde         fecha de inicio
     * @param   date     $hasta         fecha final
     * @param   bool     $filtro        TRUE se aplica el filtro
     * @param   mixed    $provincia     nombre de la provincia
     * @param   mixed    $municipio     nombre del municipio
     * @param   mixed    $semillera     nombre de la semillera
     * @param   mixed    $semillerista  nombre de semillerista
     * */
    public function graficar($grafico, $tipo, $sistema, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista, $filtro = FALSE) {
        if ($filtro) {
            $this -> setFiltro(TRUE, $provincia, $municipio, $semillera, $semillerista);
        } else {
            $this -> setFiltro(FALSE);
        }
        if ($grafico == 'culsuper') {
            $img = $this -> graficarSuperficieCultivo($tipo, $sistema, $desde, $hasta, $this -> provincia, $this -> municipio, $this -> semillera, $this -> semillerista);
        } else {
            $img = $this -> graficarVolumenCultivo($tipo, $sistema, $desde, $hasta);
        }

        return $img;
    }

    /**
     * graficar  superficie de cultivos
     * */
    private function graficarSuperficieCultivo($tipo, $sistema, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista) {

        //lista de cultivos que estan en el rango
        $lst_cultivos = array();
        Semilla::getListCultivosSuperficieGrafico($desde, $hasta, $provincia, $municipio, $semillera, $semillerista);
        if (DBConnector::filas() > 0) {

            while ($obj = DBConnector::objeto()) {
                array_push($lst_cultivos, $obj -> cultivo);
            }
            //array que contiene el total de supercicie aprobada
            $lst_superficie = array();
            //sumar superficie aprobada
            foreach ($lst_cultivos as $key => $nombre) {
                $total = 0;
                $this -> getSuperficieProduccionCultivosGraficos($nombre, $desde, $hasta, $provincia, $municipio, $semillera, $semillerista);
                #datos de un cultivo determinado
                if (DBConnector::filas() > 0) {
                    while ($row = DBConnector::objeto()) {
                        $total += $row -> aprobada;
                        //sumar superficie aprobada
                    }
                    //agregar al array
                    array_push($lst_superficie, $total);
                } else {
                    //agregar al array
                    array_push($lst_superficie, 0);
                }
            }
            switch ($tipo) {
                case 'lineal' :
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_line.php';
                    $formato = new Funciones();
                    $datay1 = $lst_superficie;

                    // Setup the graph
                    $graph = new Graph(470, 285);
                    #$graph -> SetScale("textlin");
                    $graph -> SetScale("textlin", 0, max($lst_superficie));
                    $theme_class = new UniversalTheme;

                    $graph -> SetTheme($theme_class);
                    $graph -> img -> SetAntiAliasing(false);
                    $desde = $formato -> cambiar_formato_fecha($desde);
                    $hasta = $formato -> cambiar_formato_fecha($hasta);
                    if ($sistema==1){
                        $graph -> title -> Set('Superficie de Semilla Certificada');
                    }else{
                        $graph -> title -> Set('Superficie de Semilla Fiscalizada');
                    }
                    $graph -> subtitle -> Set("DEL $desde AL $hasta ");
                    $graph -> SetBox(false);

                    $graph -> img -> SetAntiAliasing();

                    $graph -> yaxis -> HideZeroLabel();
                    $graph -> yaxis -> HideLine(false);
                    $graph -> yaxis -> HideTicks(false, false);

                    $graph -> xgrid -> Show();
                    $graph -> xgrid -> SetLineStyle("solid");
                    $graph -> xaxis -> SetTickLabels($lst_cultivos);
                    $graph -> xgrid -> SetColor('#E3E3E3');

                    // Create the first line
                    $p1 = new LinePlot($datay1);
                    $graph -> Add($p1);
                    $p1 -> SetColor("#6495ED");
                    #$p1 -> SetLegend('Line 1');

                    $graph -> legend -> SetFrameWeight(1);
                    $file = date('mYHis') . ".png";
                    // Display the graph
                    $graph -> Stroke($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . REPORT . '/' . $file);
                    break;
                case 'barra' :
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_bar.php';
                    $formato = new Funciones();
                    $graph = new Graph(470, 285);
                    //Define el tipo de escala que va a utilizar y el valor minimo y maximo para el eje y
                    $graph -> SetScale("textlin", 0, max($lst_superficie));

                    // Asigna el titulo de la gráfica
                    $desde = $formato -> cambiar_formato_fecha($desde);
                    $hasta = $formato -> cambiar_formato_fecha($hasta);
                    if ($sistema==1){
                        $graph -> title -> Set('Superficie de Semilla Certificada');
                    }else{
                        $graph -> title -> Set('Superficie de Semilla Fiscalizada');
                    }
                    $graph -> subtitle -> Set("DEL $desde AL $hasta ");

                    // Asigna el titulo y la alineacion para el eje x
                    $graph -> xaxis -> SetTickLabels($lst_cultivos);
                    //Asigna el titulo y la alineacion para el eje y
                    $graph -> yaxis -> SetTitle("Superficie", "middle");
                    //Define una serie, en este caso para un grafico de barras
                    $cultivos = new BarPlot($lst_superficie);
                    //Asigna la leyenda para la serie
                    $cultivos -> SetLegend("Cultivos");
                    //agrega la serie temperatura al grafico
                    $graph -> Add($cultivos);
                    //nombre de archivo
                    $file = date('mYHis') . ".png";
                    // Display the graph
                    $graph -> Stroke($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . REPORT . '/' . $file);
                    break;
                case 'circular' :
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_pie.php';
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_pie3d.php';
                    $formato = new Funciones();
                    // Some data
                    $data = $lst_superficie;

                    // Create the Pie Graph.
                    $graph = new PieGraph(470, 285);
                    $graph -> SetShadow();

                    // Set A title for the plot
                    $desde = $formato -> cambiar_formato_fecha($desde);
                    $hasta = $formato -> cambiar_formato_fecha($hasta);
                    if ($sistema==1){
                        $graph -> title -> Set('Superficie de Semilla Certificada');
                    }else{
                        $graph -> title -> Set('Superficie de Semilla Fiscalizada');
                    }
                    $graph -> subtitle -> Set("DEL $desde AL $hasta ");
                    $graph -> title -> SetFont(FF_FONT1, FS_BOLD);
                    $graph -> legend -> SetPos(0.1, 0.9, 'left', 'bottom');
                    $graph -> legend -> SetColumns(5);
                    // Create
                    $p1 = new PiePlot3D($data);
                    #$p1 -> SetLegends($lst_cultivos);
                    $p1 -> SetCenter(0.5, 0.5);
                    $p1 -> SetSize(0.45);
                    $p1 -> Explode(array(20, 0, 20));
                    $p1 -> SetLegends($lst_cultivos);
                    $p1 -> SetLabelType(PIE_VALUE_PER);

                    $labels = array();
                    foreach ($lst_cultivos as $id => $cultivo) {
                        array_push($labels, $cultivo . "\n(%.2f%%)");
                    }
                    $p1 -> SetLabels($labels);
                    $p1 -> SetLabelPos(1);
                    $p1 -> value -> Show();
                    $p1 -> value -> SetFont(FF_ARIAL, FS_NORMAL, 9);
                    $p1 -> value -> SetColor('darkgray');
                    $graph -> Add($p1);

                    //nombre de archivo
                    $file = date('mYHis') . ".png";
                    // Display the graph
                    $graph -> Stroke($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . REPORT . '/' . $file);
                    break;
            }
            return $file;
        } else {
            //imagen con mensaje de error
            $errorFile = "msgError.jpg";
            return $errorFile;
        }
    }

    /**
     * graficar volumen de produccion de cultivos
     * */
    private function graficarVolumenCultivo($tipo, $sistema, $desde, $hasta) {

    }

    /**
     * inicializar variables de filtro
     * @param   bool   $filtro        TRUE se aplica el filtro
     * @param   mixed  $provincia     nombre de la provincia
     * @param   mixed  $municipio     nombre del municipio
     * @param   mixed  $semillera     nombre de la semillera
     * @param   mixed  $semillerista  nombre de semillerista
     *
     * */
    private function setFiltro($filtro, $provincia = '', $municipio = '', $semillera = '', $semillerista = '') {
        if ($filtro) {
            $this -> provincia = $provincia;
            $this -> municipio = $municipio;
            $this -> semillera = $semillera;
            $this -> semillerista = $semillerista;
        } else {
            $this -> provincia = '%';
            $this -> municipio = '%';
            $this -> semillera = '%';
            $this -> semillerista = '%';
        }
        #echo ($provincia.'='.$municipio.'='.$semillera.'='.$semillerista);exit;
    }

}
?>