<?php
/**
 * Clase para manejar todo lo referido a la superficie
 */
class Superficie {

    /**
     * Ingresa la cantidad de superficie inscrita si no existe
     * @param   int $sInscrita      Contiene la superficie inscrita.
     * @param   int $isemilla       contiene el id de la semilla
     */
    public static function setInscrita($sInscrita, $isemilla) {
        if ($sInscrita != '') {
            $query = "SELECT id_superficie FROM superficie WHERE inscrita = $sInscrita";

            DBConnector::ejecutar($query);

            if (!DBConnector::filas()) {
                $query = "INSERT INTO superficie (inscrita,id_semilla) VALUES ($sInscrita,$isemilla )";
                DBConnector::ejecutar($query);
                if (DBConnector::nroError()) {
                    echo "ERROR al registrar la superficie :: $query";
                    exit ;
                }
                return DBConnector::lastInsertId();
            } else {
                $obj = DBConnector::objeto();
                return $obj -> id_superficie;
            }
        } else {
            $query = "SELECT id_superficie FROM superficie WHERE inscrita =  0.00";
            DBConnector::ejecutar($query);
            echo "GET SUPERFICIE INSCRITA 0.00: ";
            ///var_dump(DBConnector::objeto());
            $obj = DBConnector::objeto();
            return $obj -> id_superficie;
        }
    }

    public static function setInscritav2($idsuperficieInscrita, $id_semilla) {
        $query = "INSERT INTO superficie (inscrita,id_semilla) VALUES (";
        $query .= "$idsuperficieInscrita,$id_semilla)";

        DBConnector::ejecutar($query);
    }

    /**
     * Ingresa la cantidad de superficie inscrita si no existe
     * @param 	int	$sInscrita		Contiene la superficie inscrita.
     * @param   int $isemilla       contiene el id de la semilla
     */
    public static function setInscritaImportar($sInscrita, $isemilla) {
        //seleccionar id de superfice inscrita si ya existe
        $query = "SELECT id_inscrita FROM inscrita WHERE superficie = $sInscrita";
        DBConnector::ejecutar($query);
        if (DBConnector::filas()) {
            $obj = DBConnector::objeto();
            return $obj -> id_inscrita;
        } else {//si no existe el id de la superficie se inserta en la tabla y se devuelve el id
            $query = "INSERT INTO inscrita (superficie) VALUES ($sInscrita)";
            DBConnector::ejecutar($query);

            return DBConnector::lastInsertId();
        }
    }

    /**
     * Actualiza la superficie retirada
     * @param int  $retirada     Contiene la superficie retirada.
     * @param int  $isemilla     Contiene identificador de la semilla.
     */
    public static function setRetirada($isemilla, $retirada) {
        $query = "UPDATE superficie SET retirada = $retirada WHERE id_semilla=$isemilla";
        DBConnector::ejecutar($query);
    }

    /**
     * Actualiza la superficie rechazada
     * @param int  $rechazada    Contiene la superficie rechazada.
     * @param int  $isemilla     Contiene identificador de la semilla.
     */
    public static function setRechazada($isemilla, $rechazada) {
        $query = "UPDATE superficie SET rechazada = $rechazada WHERE id_semilla=$isemilla";
        DBConnector::ejecutar($query);
    }

    /**
     * Actualiza la superficie aprobada
     * Cambia el estado de superficie en 3
     * @param int  $inscrita     Contiene la superficie aprobada.
     * @param int  $isemilla     Contiene identificador de la semilla.
     */
    public static function setAprobada($isemilla, $aprobada) {
        $query = "UPDATE superficie SET estado=3,aprobada = $aprobada WHERE id_semilla=$isemilla";
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query::",DBConnector::mensaje();
        }
    }

    /**
     * ingreso de superficie fiscalizada
     * solo para control
     * @param integer $idSol id de solicitud
     * @param integer $idSem id de semilla
     *
     * @return void
     * */
    public static function setSupFiscal($iSem) {
        $query = "INSERT INTO superficie (id_semilla) VALUES ($iSem)";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR superficie_fiscal :: ",DBConnector::mensaje();
    }

    /**
     * modifica una superficie
     *
     * @param  $tipo   contiene a que tipo de superficie se modificara
     * @param  $tamanho contiene el nuevo tamaho
     * @param  $id     contiene el id de la solicitud
     */
    public static function updateSuperficie($tipo = '', $tamanho, $id) {
        if ($tipo != '') {
            if ($tipo == 'aprobada' && is_array($tamanho)) {
                //var_dump($tamanho);
                $query = "UPDATE superficie SET rechazada =" . $tamanho['rechazada'];
                $query .= " ,retirada =" . $tamanho['retirada'];
                $query .= " ,aprobada=" . ($tamanho['inscrita'] - ($tamanho['rechazada'] + $tamanho['retirada']));
                $query .= " WHERE id_semilla = $id";
            } else {
                $query = "UPDATE superficie SET inscrita =" . $tamanho['inscrita'];
                $query .= " ,rechazada =" . $tamanho['rechazada'];
                $query .= " ,retirada =" . $tamanho['retirada'];
                $query .= " ,aprobada=" . ($tamanho['inscrita'] - ($tamanho['rechazada'] + $tamanho['retirada']));
                $query .= " WHERE id_semilla = $id";
            }
            echo $query;
            DBConnector::ejecutar($query);
            return TRUE;
        } else
            return FALSE;
    }

    /**
     * elimina una superficie
     * @param  $id     contiene el id de la solicitud
     */
    function deleteSuperficie($id) {
        $query = "DELETE FROM superficie WHERE id_superficie=$id";

        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de superficie segun el nombre y apellido
     * de la parcela que fue introducida cuando se registro la semilla
     * @param string $nombre  nombre del semillerista
     * @param string $apellido  apellido del semillerista
     * @param integer $idproductor[opcional] id de la solicitud
     *
     * @return integer numero id de la superficie
     * */
    public function getSuperficieID($nombre, $apellido, $idproductor = '') {
        $query = "SELECT MAX(id_superficie) as id_superficie FROM view_superficie WHERE ";
        if (!empty($idproductor))
            $query .= "id_solicitud=$idproductor AND ";
        if (!empty($apellido))
            $query .= "apellido LIKE '%$apellido%' AND ";

        $query .= "nombre LIKE '%$nombre%' AND estado = 1 AND estado_solicitud = 4";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::filas();
        if (DBConnector::filas()) {
            $query = "SELECT MAX(id_superficie) as id_superficie 
			          FROM view_superficie
                      WHERE nombre LIKE '%$nombre%' 
                            AND apellido LIKE '%$apellido%' 
                            AND (estado_solicitud <=5 AND estado =1)";
            #echo $query;
            DBConnector::ejecutar($query);
            while ($aux2 = DBConnector::objeto()) {
                $temp = $aux2 -> id_superficie;
            }
            return $temp;
        } else {
            while ($aux2 = DBConnector::resultado()) {
                $temp = $aux2 -> id_superficie;
                break;
            }
            return $temp;
        }
    }

    /**
     * devuelve el id de la semilla segun la superficie
     * @param integer  $id  id de la superficie
     *
     * @return id de la semilla
     * */
    public static function getIdSemillaByIdSuperficie($id) {
        $query = "SELECT id_semilla FROM superficie WHERE id_superficie = $id";

        DBConnector::ejecutar($query);

        $respuesta = DBConnector::objeto();

        return $respuesta -> id_semilla;
    }

    /**
     * devuelve el id de la superficie
     *  @param decimal  $superficie  superficie de la parcela
     *
     * agrega la superficie si no esta registrada y devuelve el id
     * */
    public static function getIdSuperficieBySuperficie($superficie) {
        $query = "SELECT id_inscrita FROM inscrita WHERE superficie = $superficie";
        #echo $query;
        DBConnector::ejecutar($query);

        if (!DBConnector::nroError()) {
            if (DBConnector::filas()) {
                $row = DBConnector::objeto();
                return $row -> id_inscrita;
            } else {
                Inscrita::setInscrita($superficie);
                DBConnector::ejecutar($superficie);

                return DBConnector::lastInsertId();
            }
        } else {
            echo "ERROR>> superficie inscrita : $query";
        }
    }

    /**
     * devuelve el id de la superficie
     *  @param decimal  $superficie  superficie de la parcela
     *
     * agrega la superficie si no esta registrada y devuelve el id
     * */
    public static function getIdSuperficieBySuperficieImportar($superficie) {
        if (!empty($superficie)) {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = $superficie";
            #echo $query;
            DBConnector::ejecutar($query);

            if (!DBConnector::nroError()) {
                if (DBConnector::filas()) {
                    $row = DBConnector::objeto();
                    return $row -> id_inscrita;
                } else {
                    Inscrita::setInscrita($superficie);
                    return DBConnector::lastInsertId();
                }
            } else {
                echo "ERROR>> superficie inscrita : $query";
            }
        } else {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = 0.00";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();

            return $obj -> id_inscrita;

        }
    }

    /**
     * devuelve el id de la superficie
     *  @param decimal  $superficie  superficie de la parcela
     *
     * agrega la superficie si no esta registrada y devuelve el id
     * */
    public static function getIdSuperficieInscritaBySuperficieImportar($superficie) {
        if ($superficie == '') {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = 0.00";
            DBConnector::ejecutar($query);
            $row = DBConnector::objeto();
            return $row -> id_inscrita;
        } else {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = " . round($superficie, 2);

            DBConnector::ejecutar($query);

            if (DBConnector::filas()) {
                $row = DBConnector::objeto();
                return $row -> id_inscrita;
            } else {
                Inscrita::setInscrita($superficie);
                DBConnector::ejecutar($superficie);

                return DBConnector::lastInsertId();
            }
        }
    }

    /**
     * devuelve el id de la superficie segun el id de la semilla
     * @param integer  $id  id de la superficie
     *
     * @return id de la semilla
     * */
    public static function getIdSuperficieByIdSemilla($id) {       
        if (is_numeric($id)) {
            $query = "SELECT id_superficie FROM superficie WHERE id_semilla = $id";
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "".__METHOD__.":: $query :: ",DBConnector::mensaje();
            }else{
                if (DBConnector::filas()) {
                    $respuesta = DBConnector::objeto();
                    return $respuesta -> id_superficie;
                } else {
                    return 0;
                }
            }            
        } else {
            return 0;
        }
    }

    /**Devuelve el id de la inspeccion que corresponde a una superficie
     * @param integer  $id  id de la superficie
     * */
    public static function getIdInspeccion($id) {
        if ($id > 0) {
            $query = "SELECT id_inspeccion FROM superficie_inspeccion WHERE id_superficie=$id";
            DBConnector::ejecutar($query);
            #echo $query;
            $respuesta = DBConnector::objeto();
            if (DBConnector::filas()) {
                $respuesta = $respuesta -> id_inspeccion;
            } else {
                $respuesta = FALSE;
            }

            return $respuesta;
        } else {
            return 0;
        }
    }

    /**
     * verifica si la superficie es mayor a O
     * */
    public static function getLastSuperficie($objPHPExcel, $col, $row) {
        $superficie['inscrita'] = $objPHPExcel -> getCellByColumnAndRow($col, $row) -> getCalculatedValue();
        $superficie['rechazada'] = $objPHPExcel -> getCellByColumnAndRow(($col + 1), ($row - 1)) -> getCalculatedValue();
        $superficie['retirada'] = $objPHPExcel -> getCellByColumnAndRow(($col + 2), ($row - 1)) -> getCalculatedValue();
        while ($superficie['inscrita'] == '') {
            $superficie['inscrita'] = $objPHPExcel -> getCellByColumnAndRow($col, ($row - 1)) -> getCalculatedValue();
            $superficie['rechazada'] = $objPHPExcel -> getCellByColumnAndRow(($col + 1), ($row - 1)) -> getCalculatedValue();
            $superficie['retirada'] = $objPHPExcel -> getCellByColumnAndRow(($col + 2), ($row - 1)) -> getCalculatedValue();
            if ($superficie['inscrita'] >= 0.00) {
                break;
            } else {
                $superficie['rechazada'] = $superficie['retirada'] = 0;
            }
        }
        $superficie['rechazada'] = $superficie['retirada'] = 0;
        $superficie['aprobada'] = $superficie['inscrita'] - $superficie['rechazada'] - $superficie['retirada'];

        return $superficie;
    }

    /**
     * verifica si la superficie es mayor a O
     * */
    public static function getLastSuperficiev2(array $data, $col, $row, $total) {
        if ($col < $total) {
            $columna_0 = ($col < $total) ? $col : 21;
            $columna_1 = (($col + 1) < $total) ? $col + 1 : 22;
            $columna_2 = (($col + 2) < $total) ? $col + 2 : 23;
            $superficie['inscrita'] = $data[$columna_0][$row];
            $superficie['rechazada'] = $data[$columna_1][$row - 1];
            $superficie['retirada'] = $data[$columna_2][$row - 1];
            while ($superficie['inscrita'] == '') {
                $superficie['inscrita'] = $data[$columna_0][$row - 1];
                $superficie['rechazada'] = $data[$columna_1][$row - 1];
                $superficie['retirada'] = $data[$columna_2][$row - 1];
                if ($superficie['inscrita'] >= 0.00) {
                    break;
                } else {
                    $superficie['rechazada'] = $superficie['retirada'] = 0;
                    $columna_0 = ($col < $total) ? $col : 21;
                    $columna_1 = (($col + 1) < $total) ? $col + 1 : 22;
                    $columna_2 = (($col + 2) < $total) ? $col + 2 : 23;
                }
            }
            $superficie['rechazada'] = $superficie['retirada'] = 0;
            $superficie['aprobada'] = $superficie['inscrita'] - $superficie['rechazada'] - $superficie['retirada'];

            return $superficie;
        } else {
            $superficie['rechazada'] = $superficie['retirada'] = 0;
            $superficie['aprobada'] = $superficie['inscrita'] - $superficie['rechazada'] - $superficie['retirada'];
        }
    }

    /**Devuelve el id de la superficie inscrita  para importar
     * @param integer  $id  id de la superficie
     * */
    public static function getImportarInscrita($inscrita) {
        if ($inscrita == '' || $inscrita == '0.00') {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = 0.00";
            #echo $query;
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()) {
                echo "ERROR al seleccionar la superficie inscrita 0:: $query";
                exit ;
            }
            $obj = DBConnector::objeto();

            return $obj -> id_inscrita;
        } else {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = " . round($inscrita, 2);
            #echo $query;
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()) {
                echo "ERROR al seleccionar la superficie $inscrita:: $query";
                exit ;
            }
            $obj = DBConnector::objeto();

            return $obj -> id_inscrita;
        }
    }

    /**
     * superficie rechazada para importar desde excel
     * */
    public static function getImportarRechazada($rechazada) {
        if (empty($rechazada)) {
            return 0;
        } else {
            return round($rechazada, 2);
        }
    }

    /**
     * superficie retirada para importar desde excel
     * */
    public static function getImportarRetirada($retirada) {
        if (empty($retirada)) {
            return 0;
        } else {
            return round($retirada, 2);
        }
    }

    /**
     * superficie aprobada para importar desde excel
     * */
    public static function getImportarAprobada($aprobada) {
        if (empty($aprobada)) {
            return 0;
        } else {
            return round($aprobada, 2);
        }
    }

    /**
     * registro de superficie segun solicitud
     * consulta tabla superficie  con id_semilla
     * */
    public static function getRegistro($id) {

        $query = "SELECT inscrita,rechazada,retirada,aprobada FROM view_superficie_2 WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR::".__METHOD__."::$query",DBConnector::mensaje();
    }

    #REVISAR
    /**todas las superficies*/
    function getSuperficies() {
        $query = "SELECT * FROM superficie ";

        return DBConnector::ejecutar($query);

    }

    /**superficie segun el nro de campo y cultivo
     * @param integer $isolicitud        nro de solicitud
     * @param optional string $cultivo   nombre del cultivo
     * @param optional string $nroCampo  numero de campo
     * */
    public function getSuperficieCampo($isolicitud, $cultivo = '', $nroCampo = '') {
        if (empty($cultivo) && empty($nroCampo)) {
            $query = "SELECT * FROM view_superfice_campo WHERE id_solicitud = $isolicitud";
        } else {
            $query = "SELECT * FROM view_superfice_campo WHERE id_solicitud = $isolicitud AND nombre_cultivo = '$cultivo' AND nro_campo = '$nroCampo'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * filtro de busqueda para certificacion
     * */
    public static function getSuperficieCertificaFilter($search) {
        $query = "SELECT semillerista,nro_campo,inscrita,rechazada,retirada,aprobada,semillera,id_superficie,comunidad ";
        $query .= "FROM view_superficie ";
        $query .= "WHERE semillerista LIKE '$search%' OR semillera LIKE '$search%' OR  inscrita LIKE '$search%' OR rechazada LIKE '$search%' OR ";
        $query .= "retirada LIKE '$search%' OR aprobada LIKE '$search%' OR semillera LIKE '$search%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**superficies de cada solicitud*/
    function getSuperficieSolicitud($area) {
        $query = "SELECT * FROM view_superficie";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND estado_solicitud=4";
        $query .= " ORDER BY semillerista";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**superficies de cada solicitud*/
    function getSuperficieSolicitudByBlock($area, $inicio, $nroReg) {
        $query = "SELECT * FROM view_superficie";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND estado_solicitud>=4";
        $query .= " ORDER BY id_superficie DESC,semillerista LIMIT $inicio,$nroReg";
        #echo $query;
        return $query;
    }

    /**superficie aprobada de acuerdo a:
     * @param string   $cultivo    nombre del cultivo
     * @param int      $sistema    1:certificacion 2:fiscalizacion
     */
    public static function getTotalSupAprobadaByCultivo($cultivo, $sistema, $desde = '', $hasta = '', $gestion = '') {
        $query = "SELECT SUM(aprobada) AS total FROM view_superficie ";
        $query .= "WHERE cultivo LIKE '$cultivo' AND  id_sistema LIKE '$sistema%' ";
        if (!empty($desde)) {
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND estado_solicitud >= 12 ";
                $query .= "AND gestion = '$gestion' ";
            } else {
                $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND estado_solicitud >= 12 ";
            }
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (!DBConnector::nroError()){
            if (DBConnector::filas()) {
                $row = DBConnector::objeto();
                return $row -> total;
            } else {
                return 0;
            }
        }else{
            echo "ERROR [total Superficie] :: ",DBConnector::mensaje();
        }
    }

    /**
     * superficie de cultivo segun gestion
     * */
    public static function getSuperficieCultivoByGestion($cultivo, $sistema, $gestion) {
        $query = "SELECT SUM(sup.aprobada)AS total
                  FROM superficie sup INNER JOIN solicitud sol ON sup.id_solicitud = sol.id_solicitud
                                      INNER JOIN semilla sem ON  sem.id_semilla=sup.id_semilla
                  WHERE sem.cultivo = '$cultivo' AND 
                        sol.sistema LIKE '$sistema%' AND sol.fecha LIKE ('$gestion-%')";

        DBConnector::ejecutar($query);
        $res = DBConnector::resultado();

        return $res -> total;
    }

    /** actualiza el estado de una superficie
     *  @param int $id  numero de semilla
     *
     * @return void
     * */
    public static function updateEstado($id) {
        $query = "UPDATE superficie SET estado = 3 WHERE id_semilla = $id";
        //echo $id.'**'.$query;exit;
        DBConnector::ejecutar($query);

    }

    /**
     * devuelve la superficie total segun la categoria
     * */
    public static function getSuperficieByCategoria($categoria, $cultivo = '') {
        $query = "SELECT total FROM view_reporte_superficie_categoria WHERE categoria = '$categoria'";
        if ($cultivo) {
            $query .= " AND cultivo='$cultivo'";
        }
        echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();
        if ($obj -> total > 0)
            return $obj -> total;
        else {
            return '0.0';
        }
    }

    /**
     * datos de superficie segun id
     */
    public static function getSuperficieById($id) {
        $query = "SELECT * FROM view_superficie WHERE id_superficie=$id";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de superficie segun id de semilla
     * @param integer $id  numero id de la semilla
     *
     * @return Devuelve la superficie inscrita
     */
    public static function getSuperficieInscritaByIdSem($id) {
        $query = "SELECT inscrita,rechazada,retirada,aprobada FROM view_superficie WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * id superficie segun id solicitud e id semilla
     *
     * @return id_superficie
     * */
    public static function getSuperficieByIdSolSem($idSol, $idSem) {
        $query = "SELECT MAX(id_superficie) as id_superficie FROM superficie WHERE id_solicitud=$idSol AND id_semilla=$idSem";
        #echo $query;
        DBConnector::ejecutar($query);

        $row = DBConnector::resultado();

        return $row -> id_superficie;
    }

    /**
     * id de la superficie segun el id de la semilla
     *
     * **/
    public static function getSuperficieByIDNroCampo($isemilla) {
        $query = "SELECT id_superficie "; 
		$query .= "FROM superficie "; 
		$query .= "WHERE id_semilla = $isemilla";
        #echo $query;
        DBConnector::ejecutar($query);
        $temp = DBConnector::objeto();          
        return $temp -> id_superficie;
        
    }

    /**
     * id de la superficie segun el nro de campo
     * utilizado certificacion.ctrl.php
     * opcion ver->superficie_isemilla_iproductor
     * **/
    public static function getSuperficieByIdSemilla_IdSemillerista($isemilla, $isemillerista) {
        $query = "SELECT id_superficie FROM superficie WHERE id_semilla=$isemilla";
        #echo $query;
        DBConnector::ejecutar($query);

        $fila = DBConnector::objeto();

        return $fila -> id_superficie;
    }

    /**
     * Superficie aprobada
     * @param   integer   $isolicitud    id de solicitud
     * @param   integer   $isemilla      id de la semilla
     * */
    public static function getSuperficieAprobada($isolicitud, $isemilla) {
        $query = "SELECT aprobada FROM view_superficie WHERE id_solicitud = $isolicitud AND id_semilla = $isemilla";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: $query ::",DBConnector::mensaje(); 
    }

    /**
     * Superficie aprobada
     * @param   integer   $isolicitud    id de solicitud
     * @param   integer   $isemilla      id de la semilla
     * */
    public static function getSuperficieAprobadaForDetalleResumen($isemilla) {
        $query = "SELECT aprobada FROM superficie WHERE id_semilla = $isemilla";
        #echo $query;
        DBConnector::ejecutar($query);

    }

    /**supperficie total de un productor en determinado cultivo variedad y campaña*/
    public static function getSuperficieTotal($id_solicitud, $cultivo, $variedad, $campania, $sup = '') {
        if ($sup == '') {
            $query = "SELECT sum(aprobada)+ sum(retirada) AS total, COUNT(*) AS numbers
            FROM view_superficie
            WHERE cultivo LIKE '$cultivo' AND 
                  variedad = '$variedad' AND
                  campanha='$campania' AND 
                  id_solicitud = $id_solicitud";
            //    echo $query;
        } else {
            $query = "SELECT sum(rechazada) AS total, COUNT(*) AS numbers
            FROM view_superficie
            WHERE cultivo LIKE '$cultivo' AND 
                  variedad = '$variedad' AND
                  campanha='$campania' AND 
                  id_solicitud = $id_solicitud";
            //echo $query;
        }
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     */

    public function getSuperficieSQL() {
        $query = 'INSERT INTO superficie (id_superficie,inscrita,rechazada,retirada,aprobada,estado,id_semilla) VALUES';
        $this -> getSuperficies();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_superficie . "," . $row -> inscrita . ",";
                $query .= $row -> rechazada . "," . $row -> retirada . ",";
                $query .= $row -> aprobada . "," . $row -> estado . ",";
                $query .= $row -> id_semilla . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>