<?php
class Muestra_importada{
    /**
     * lista todos los tipos de semilla
     * */
     public function getMuestraImportada(){
         $query = "SELECT * FROM muestra_importada";
         
         DBConnector::ejecutar($query);
     }
     
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getMuestraImportadaSQL() {
        $query = 'INSERT INTO muestra_importada (id_importada,cultivo,especie,variedad,categoria,lugar_origen,cantidad,certificado,aduana_ingreso,destino_semilla,distribucion,analisis) VALUES ';
        $this -> getMuestraImportada();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_importada . ",";
                $query .= $row -> cultivo .",'".$row->especie."',";
                $query .= $row -> variedad . ",". $row->categoria .",'";
                $query .= $row -> lugar_origen ."','". $row->cantidad ."','";
                $query .= $row -> certificado ."','".$row->aduana_ingreso . "','";
                $query .= $row->destino_semilla ."','".$row->distribucion."',";
                $query .= $row->analisis. "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>