<?php
/**
 * @package  model
 * @author  Edwin W. Arandia Zeballos
 * @version 2
 */
class Semilla {
    /**
     * insert semilla de acondicionamiento
     * */
     public function insertAcondicionamiento(array $acondicionamiento){
         $query = "INSERT INTO semilla (nro_campo,cultivo,variedad) VALUES (";
         $query .= "'".$acondicionamiento['campo']."',".$acondicionamiento['cultivo'].",".$acondicionamiento['variedad'].")";
     }

    /**
     * Inserta una nueva semilla de una determinada solicitud
     *
     * @param   varchar         $comunidad
     * @param 	varchar			$nroCampo
     * @param 	varchar			$Campanha
     * @param 	varchar			$Cultivo
     * @param 	varchar			$Variedad
     * @param 	varchar			$catSembrada
     * @param 	varchar			$catProducir
     * @param 	mediumint 		$CantSemillaEmpleada
     * @param 	varchar			$nroLote
     * @param 	date 			$fechaSiembra
     * @param 	int 			$PlantasHas
     * @param 	varchar			$cultivoAnterior
     * @param 	decimal(4,2)  	$supParcela
     * @param 	mediumint 		$estado
     *
     * @return void
     */
    public static function insertSemilla($nroCampo, $Campanha, $Cultivo, $Variedad, $catSembrada, $catProducir, $CantSemillaEmpleada, $fechaSiembra, $PlantasHas, $cultivoAnterior, $superficie) {
        $query = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela)
                 VALUES ('$nroCampo', $Campanha, $Cultivo, $Variedad,$catSembrada, $catProducir, $CantSemillaEmpleada, '$fechaSiembra', $PlantasHas, $cultivoAnterior,$superficie)";
        #echo "insert semilla :: ".$query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * Inserta una nueva semilla de una determinada solicitud
     *
     * @param   varchar         $comunidad
     * @param   varchar         $nroCampo
     * @param   varchar         $Campanha
     * @param   varchar         $Cultivo
     * @param   varchar         $Variedad
     * @param   varchar         $catSembrada
     * @param   varchar         $catProducir
     * @param   mediumint       $CantSemillaEmpleada
     * @param   varchar         $nroLote
     * @param   date            $fechaSiembra
     * @param   int             $PlantasHas
     * @param   varchar         $cultivoAnterior
     * @param   decimal(4,2)    $supParcela
     * @param   mediumint       $estado
     *
     * @return void
     */
    public static function insertSemillaImportar($nroCampo, $Campanha, $Cultivo, $Variedad, $catSembrada, $catProducir, $CantSemillaEmpleada, $lote, $fechaSiembra, $PlantasHas, $cultivoAnterior, $superficie) {
        $query = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) ";
        $query .= "VALUES ('$nroCampo', $Campanha, $Cultivo, $Variedad,$catSembrada, $catProducir, $CantSemillaEmpleada, '" . $lote . "','$fechaSiembra', $PlantasHas, $cultivoAnterior,$superficie)";
        #echo "insert semilla :: ".$query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query, '?';
            exit ;
        }
    }

    /**
     *
     * */
    public static function insertSemillaFiscalImportar($nro_campo, $cultivo, $variedad, $categoria_producir, $lote) {
        $query = "INSERT INTO semilla (nro_campo,cultivo,variedad,categoria_producir,nro_lote) VALUES (";
        $query .= "'$nro_campo', $cultivo, $variedad,$categoria_producir, '" . $lote . "')";
        #echo "insert semilla :: ".$query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query, '?';
            exit ;
        }
    }

    /**
     * semilla fiscalizada
     * */
    public function insertSemillaFiscal($nro_campo, $Cultivo, $Variedad, $categoria, $nroLote) {

        $query = "INSERT INTO semilla (nro_campo,cultivo,variedad,categoria_producir,nro_lote) ";
        $query .= "VALUES ('$nro_campo', $Cultivo, $Variedad,$categoria,'$nroLote')";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * Actualiza los datos de una semilla  ARREGLAR
     */
    public static function actualizarSemilla($comunidad, $localidad, $nroCampo, $campanha, $cultivo, $variedad, $catSembrada, $catProducir, $cantSemillaEmpleada, $nroLote, $fechaSiembra, $plantasHas, $cultivoAnterior, $supParcela, $idSolicitud) {
        $query = "UPDATE semilla SET  comunidad = '$comunidad',
		                              localidad = '$localidad', 
		                              nro_campo =  '$nroCampo',
		                              campanha ='$campanha',
		                              cultivo ='$cultivo',
		                              variedad ='$variedad',
		                              categoria_sembrada ='$catSembrada',
		                              categoria_producir ='$catProducir',
                                      cantidad_semilla_empleada =$cantSemillaEmpleada,
                                      nro_lote ='$nroLote',
                                      fecha_siembra ='$fechaSiembra',
                                      plantas_por_hectarea = '$plantasHas', 
                                      cultivo_anterior ='$cultivoAnterior',
                                      superficie_parcela=$supParcela
                   WHERE id_solicitud=$idSolicitud";
        DBConnector::ejecutar($query);
    }

    public function actualizarSemillaFiscal() {
        $query = "";

        DBConnector::ejecutar($query);
    }
    
    /**
     * verifica los datos
     */
    public function checkCamposFiscal($cultivo, $variedad, $nro_lote) {
        $error = '';
        if ($cultivo == '') {
            $error .= 'Debe seleccionar cultivo<br>';
        }
        if ($nro_lote == '') {
            $error .= 'Lote no puede ser vacio <br>';
        }
        if ($variedad == '') {
            $error .= 'Debe seleccionar variedad';
        }
        return $error;
    }
    /**
     * verificar cultivo en fecha inicio,fecha final y gestion
     * */
     public static function checkCultivo($cultivo,$area,$desde,$hasta,$gestion){
         $query = "SELECT cultivo FROM view_semilla ";
         $query .= "WHERE estado_solicitud = 12 AND id_sistema = $area ";
         if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >='$desde' AND mes <='$hasta') AND gestion = '$gestion'";

        } else {
            $query .= "AND (fecha >='$desde' AND fecha <='$hasta') AND gestion = '$gestion'";
        }
        
        DBConnector::ejecutar($query);
        if (DBConnector::filas()){
            return TRUE;
        }else{
            return FALSE;
        }
     }
    /**
     * lista de campanhas
     * */
    public static function getListCampanhas($sistema) {
        $query = "SELECT DISTINCT(campanha) FROM view_campanhas_productor WHERE sistema LIKE '$sistema%'";

        DBConnector::ejecutar($query);
    }

    /**
     * campaña(s) de un productor
     * @param string $nombre  nombre del semillerista
     * @param string $apellido apellido del semillerista
     * @param string $sistema
     */
    public function getCampaniaProductor($nombre, $apellido, $semillera, $sistema) {
        $query = "SELECT DISTINCT(campanha) FROM view_campanhas_productor ";
        $query .= "WHERE apellido LIKE '%$apellido%' AND nombre LIKE '%$nombre%' AND semillera LIKE '$semillera%' AND sistema LIKE '$sistema%'";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     *
     * */
    public function getCampaniaForCuenta($campanha, $sistema) {
        $query = "SELECT DISTINCT(campanha) FROM view_semilla ";
        $query .= "WHERE campanha LIKE '$campanha%' AND sistema LIKE '$sistema%'";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     *
     * */
    public function getCampaniaByIdSemillaForCuenta($id) {
        $query = "SELECT campanha FROM view_semilla ";
        $query .= "WHERE id_semilla = $id";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            $obj = DBConnector::objeto();

            return $obj -> campanha;
        }
    }

    public static function getCampaniaForBusqueda($area) {
        $query = "SELECT DISTINCT(campanha) FROM view_gestiones_anteriores ";
        $query .= "WHERE id_sistema LIKE '$area%' AND gestion < " . date('Y');
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * nro de campo y cultivo de una semilla segun el id de la solicitud
     */
    public static function getCampoCultivoById($id) {
        $query = "SELECT nro_campo,cultivo FROM view_semilla WHERE id_solicitud=" . intval($id);
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * nro de campo y cultivo de una semilla segun el id de la solicitud
     */
    public static function getCampoCultivoFiscalById($id) {
        $query = "SELECT cultivo FROM view_semilla_fiscal WHERE id_solicitud=" . intval($id);
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * nro de campo y cultivo de una semilla segun el id de la solicitud
     */
    public static function getCantidadSemillaEmpleadaImportar($cantidad) {
        if ($cantidad == '') {
            return 0;
        } else {
            return $cantidad;
        }
    }

    /**devuelve la categoriaa producir, categoria sembrada  y generacion segun el nro de campo
     * @param mixed   nro_campo    numero de campo
     * */
    public static function getCategoriaGeneracionByCampo($nroCampo) {
        $query = "SELECT categoria_sembrada AS categoriaS, categoria_producir AS categoria,generacion,inicial FROM view_categoria_generacion WHERE nro_campo = '$nroCampo'";

        DBConnector::ejecutar($query);
    }

    /** devuelve el nro de categoria sembrada de una semilla
     *
     * @param integer id id de la semilla
     * */
    public static function getCategoriaSembrada($id) {
        $query = "SELECT categoria_sembrada FROM view_t_semilla WHERE id_semilla = $id";
        #echo $query; exit;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> categoria_sembrada;
    }

    public static function getCategoriaProducir($id) {
        $query = "SELECT categoria_producir FROM view_t_semilla WHERE id_semilla = $id";
        #echo $query; exit;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> categoria_producir;
    }

    /**
     * devuelve la categoria o vacio  si no existe
     * */
    public static function getCategoriaProducirImportarFiscal($categoria) {
        if (empty($categoria))
            return '';
        else {
            return $categoria;
        }
    }

    public static function getCategoriaProducirByVariedad($categoria) {
        $query = "SELECT categoria_producir FROM view_semilla WHERE categoria_producir LIKE '$id%'";
        #echo $query; exit;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> categoria_producir;
    }

    /**
     * devuelve el nro de categoria producir de una semilla
     * */
    public static function getCategoriaProducirByNroCampo($nro) {
        $query = "SELECT categoria_producir FROM view_t_semilla WHERE nro_campo='$nro'";

        DBConnector::ejecutar($query);
        $row = DBConnector::objeto();

        return $row -> categoria_producir;
    }
    
    /**
     * devuleve el cultivo, variedad y origen segun el numero de campo
     * */
     public function getDatosMuestraLaboratorioByCampo ($campo){
         $query = "SELECT cultivo,variedad,comunidad ";
         $query .= "FROM view_semilla ";
         $query .= "WHERE nro_campo='$campo' AND sistema LIKE 'certifica%'";
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__."::$query::",DBConnector::mensaje();
         
     }
    
    /**
     * devolver generacion de categoria a producir
     * */
     public static function getCategoriaSembradaByIdSemilla($id){
         $query = "SELECT categoria_sembrada FROM semilla ";
         $query .= "WHERE id_semilla=$id";
         #echo $query;
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__."::$query",DBConnector::mensaje();
         $obj = DBConnector::objeto();
         return $obj->categoria_sembrada;
     }

    /*
     * devuelve la fecha de siembra de una determinada parcela
     * */
    public static function getFechaSiembraByIdSemilla($isemilla) {
        $query = "SELECT fecha_siembra FROM view_semilla WHERE id_semilla = $isemilla";
        #echo $query;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> fecha_siembra;
    }

    /**
     * devuelve el id de la solicitud segun
     * @param string $nombre
     * @param string $apellido
     * @param string $semillera
     * */
    public function getIdSemillaByProductor_Semillera($nombre, $apellido, $semillera) {
        $query = "SELECT id_semilla FROM view_solicitudes vsol";
        $query .= "INNER JOIN solicitud_semilla ss ON ss.id_solicitud=vsol.id_solicitud ";
        $query .= "INNER JOIN view_semilla vsem ON vsem.id_semilla=";
        $quwery .= "WHERE nombre='$nombre' AND apellido='$apellido' AND semillera='$semillera'";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> id_semilla;
        else
            return '0';
    }
    /**
     * devuelve los datos de la semilla producida segun el id de semilla
     * */
     public function getIdSemillaProducidaByIdSolicitud($id){
         $query = "SELECT * ";
         $query .= "FROM view_semilla_producida ";
         $query .= "WHERE id_solicitud = $id";
         
         DBConnector::ejecutar($query);
         
         if(DBConnector::nroError()){
             echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
         }         
     }

    /**
     * devuelve el id de la semilla segun el nro de campo
     *
     * @param mixed    $nrocampo    numero de campo
     *
     * @return   devuelve el  id de la semilla correspondiente
     * */
    public static function getIdSemillaByNroCampo($nro) {
        $query = "SELECT id_semilla FROM view_semilla WHERE nro_campo = '$nro' ";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();
        if (DBConnector::filas())
            return $row -> id_semilla;
        else
            return -1;
    }

    /**
     * devuelve el id de la semilla segun el nro de campo
     *
     * @param mixed    $nrocampo    numero de campo
     *
     * @return   devuelve el  id de la semilla correspondiente
     * */
    public static function getIdSemillaByNroCampoImportar($nro) {
        $query = "SELECT MAX(id_semilla),id_semilla FROM semilla WHERE nro_campo = '$nro' ";
        #echo $query;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();
        if (DBConnector::filas())
            return $row -> id_semilla;
        else
            return -1;
    }

    /**
     * devuelve el id de la semilla segun el nro de campo
     *
     * @param mixed    $nrocampo    numero de campo
     *
     * @return   devuelve el  id de la semilla correspondiente
     * */
    public static function getIdSemillaByMuestraNroCampo($nro) {
        $query = "SELECT id_semilla FROM view_lst_semilla_full WHERE nro_campo LIKE '$nro%'";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
        $row = DBConnector::objeto();
        #echo $row->id_semilla;
        return $row -> id_semilla;
    }

    /**
     * devuelve el id de la semilla segun el nro de campo en fiscalizacion
     *
     * @param mixed    $nrocampo    numero de campo
     *
     * @return   devuelve el  id de la semilla correspondiente
     * */
    public static function getIdSemillaByNroCampoFiscal($nro) {
        $query = "SELECT id_semilla FROM view_semilla_fiscal WHERE nro_campo = '$nro'";
        #echo $query;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();
        #echo $row->id_semilla;
        return $row -> id_semilla;
    }

    /**
     * devuelve el id de la semilla segun el nro de campo en fiscalizacion
     *
     * @param mixed    $nrocampo    numero de campo
     *
     * @return   devuelve el  id de la semilla correspondiente
     * */
    public static function getIdSemillaMuestraByNroCampoFiscal($nro) {
        $query = "SELECT id_solicitud FROM view_datos_cosecha_fiscal WHERE nro_campo = '$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query::",DBConnector::mensaje();
        }else{
            $row = DBConnector::objeto();        
            return Tiene::getIdSemilla($row->id_solicitud);    
        }
    }

    /**
     * @param integer $id  id de la solicitud
     *
     * @return Devuelve el numero de campo(s) de un semillerista
     */
    public static function getNroCampoImportar($nro_campo) {
        if ($nro_campo == '') {
            return 0;
        } else {
            return $nro_campo;
        }
    }

    /**
     * @param integer $id  id de la solicitud
     *
     * @return Devuelve el numero de campo(s) de un semillerista
     */
    public function getNroCamposProductor($id, $semillera = "", $estado = "") {
        $query = "SELECT * FROM view_comunidad_nrocampo ";
        $query .= "WHERE id_solicitud = $id AND semillera LIKE '$semillera%' ";
        if (empty($estado)) {
            #$query .= "AND estado_solicitud IN (5,6,7,8)";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * numero de campo segun id de la semilla
     * */
    public static function getNroCampoByIdSemilla($id) {
        $query = "SELECT nro_campo FROM view_semilla WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #DBConnector::mensaje();
        $resultado = DBConnector::objeto();
        if (DBConnector::nroError() || !DBConnector::filas()){
            echo "ERROR ::".__METHOD__.":: $query :: ",DBConnector::mensaje();
        }else{
            return $resultado -> nro_campo;
        }
        
    }

    /**
     * nro de campo desde tabla
     * */
    public static function getNroCampoByIdSemilla2($id) {
        $query = "SELECT nro_campo FROM semilla WHERE id_semilla=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        #DBConnector::mensaje();
        $resultado = DBConnector::objeto();

        return $resultado -> nro_campo;
    }

    /**id de la semilla segun el nro de campo
     * @param string    $noCampo   numero de campo
     * @param integer   $iSemillerista  id de semillerista
     *
     * @return retorna el id_semilla
     * */
    public function getIdSemillaByCampo($noCampo, $iSemillerista = '') {
        if (empty($iSemillerista)) {
            $query = "SELECT id_semilla FROM semilla WHERE nro_campo ='$noCampo'";
            #echo $query;
            DBConnector::ejecutar($query);
            while ($resultado = DBConnector::objeto()) {
                return $resultado -> id_semilla;
            }
        } else {//cuando especifica el id del semillerista
            $query = "SELECT sem.id_semilla 
            FROM semilla sem
            INNER JOIN solicitud_semilla ssem ON ssem.id_semilla=sem.id_semilla 
            INNER JOIN solicitud sol ON sol.id_solicitud=ssem.id_solicitud 
            WHERE sol.id_solicitud = $iSemillerista";
            #echo '--'.$query;
            DBConnector::ejecutar($query);
            while ($resultado = DBConnector::objeto()) {
                return $resultado -> id_semilla;
            }
        }
    }
    
    /**
     * devuelve el id de la semilla segun cultivo,gestion y rango
     * */
     public static function getIdSemillaByCultivoGestionForXLSResumen($cultivo,$area,$desde,$hasta,$gestion){
         $query = "SELECT id_semilla FROM view_semilla ";
         $query .= "WHERE cultivo LIKE '$cultivo%' AND id_sistema = $area AND estado_solicitud=12 ";
         if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >='$desde' AND mes <='$hasta') AND gestion = '$gestion'";

        } else {
            $query .= "AND (fecha >='$desde' AND fecha <='$hasta') AND gestion = '$gestion'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (!DBConnector::nroError() && DBConnector::filas() > 0){
            $obj = DBConnector::objeto();        
            return $obj->id_semilla;
        }else{
            return 1;
        }
     }
    /**
     * devuelve el id de superficie segun el id de la semilla
     * */
    public static function getIdSuperficieByIdSemilla($id) {
        $query = "SELECT id_superficie FROM semilla WHERE id_semilla= $id";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de superficie segun el id de la semilla
     * */
    public static function getIdSuperficieByIdSemillaImportar($id) {
        $query = "SELECT superficie_parcela FROM semilla WHERE id_semilla= $id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> superficie_parcela;
    }

    /**
     * devuelve el id de la categoria sembrada segun el id de la semilla
     * */
    public static function getIdCategoriaSembradaByIdSemilla($id) {
        $query = "SELECT categoria_sembrada FROM view_semilla WHERE id_semilla=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> categoria_sembrada;
    }

    /**
     * devuelve el id de la categoria sembrada segun el id de la semilla
     * */
    public static function getIdCategoriaProducirByIdSemilla($id) {
        $query = "SELECT id_generacion FROM view_semilla WHERE id_semilla=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> id_generacion;
        else {
            return 0;
        }
    }

    /**
     * plantas por hectarea para importacion segun valor
     * */
    public static function getPlantasHectareasImportar($plantas_hectarea) {
        if (strtolower($plantas_hectarea) == "cosechado") {
            return 1;
        }
        if (strtolower($plantas_hectarea) == "en cosecha") {
            return 2;
        }
        if ($plantas_hectarea > 0) {
            return $plantas_hectarea;
        } else {
            return 3;
        }
    }

    /**
     * registro de semilla
     * @param integer $id  id de solicitud
     * */
    public static function getRegistro($id) {
        $query = "SELECT nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla,";
        $query .= "fecha_siembra,plantas_hectarea FROM view_semilla WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR::".__METHOD__."::$query",DBConnector::mensaje();
    }

    /**
     * registro de semilla
     * @param integer $id  id de solicitud
     * */
    public static function getRegistroFiscal($id) {
        $query = "SELECT cultivo,variedad,lote FROM view_semilla_fiscal WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**semillas de productores*/
    public function getSemillaByArea($area) {
        $query = "SELECT * FROM view_semilla";
        if ($area != 10)
            $query .= " WHERE sol.id_sistema='$area'";
        $query .= " ORDER BY nro_campo,campanha DESC";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * filtro de semilla segun tipo
     * */
    public static function getSemillaCertificaFilter($search) {
        $query = "SELECT nombre_apellido AS semillerista,semillera,id_solicitud,comunidad,nro_campo,campanha,cultivo,variedad,";
        $query .= "categoria_sembrada,categoria_producir,cantidad_semilla,nro_lote,fecha_siembra,plantas_hectarea,";
        $query .= "cultivo_anterior,inscrita AS parcela FROM view_semilla ";
        $query .= "WHERE (semillera LIKE '%$search%' OR nombre_apellido LIKE '%$search%' OR comunidad LIKE '$search%' OR ";
        $query .= "cultivo LIKE '$search%' OR variedad LIKE '$search%') AND gestion ='".date('Y')."' AND plantin=0 AND id_sistema=1 ";
        $query .= "ORDER BY semillera";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * filtro de semilla segun tipo
     * */
    public static function getSemillaFiscalFilter($search) {
        $query = "SELECT semillera,semillerista,comunidad,cultivo,variedad,lote FROM view_semilla_fiscal ";
        $query .= "WHERE semillerista LIKE '$search%' OR comunidad LIKE '$search%' OR variedad LIKE '$search%' OR lote LIKE '$search%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**Categoria en campo segun el productor, cultivo, y nro de campo*/
    public function getSemillaCategoriaProducir($isolicitud, $cultivo, $nroCampo) {
        $query = "SELECT * FROM view_semilla_categoria_producir ";
        $query .= "WHERE id_solicitud = $isolicitud AND cultivo = $cultivo AND nro_campo = '$nroCampo'";
        #echo '***' . $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * nro de campo y superficie inscrita segun id de productor
     * @param integer $id  id de semilla
     * @param integer $iproductor  id de solicitud
     * */
    public static function getNroCampoInscritaById($id, $iproductor) {
        $query = "SELECT * FROM view_check_superficie WHERE id_semilla=$id and id_solicitud=$iproductor";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**semillas de productores por bloque*/
    public function getSemilleraSolicitudByBlock($area, $inicio = 0, $nroReg = 15) {
        $query = "SELECT * FROM view_semilla ";
        if ($area != 10){
            $query .= " WHERE id_sistema=$area AND estado_solicitud>=1 AND gestion = '" . date('Y') . "' ";
        }
        $query .= "ORDER BY id_semilla DESC,fecha_siembra DESC, nro_campo,campanha DESC LIMIT $inicio,$nroReg";
        
        #echo $query;
        return $query;
    }

    /**
     * devuelve el cultivo segun el id de semilla
     * @param integer $id id de la semilla
     *
     * @return nombre del cultivo que pertenee al id
     * */
    public static function getCultivo($id) {
        $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        //echo DBConnector::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> cultivo;
    }

    /**
     * devuelve el cultivo segun el id de semilla en laboratorio
     * @param integer $id id de la semilla
     *
     * @return nombre del cultivo que pertenee al id
     * */
    public static function getAllCultivo($id) {
        $query = "SELECT cultivo FROM view_lst_semilla_full WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> cultivo;
    }

    /**
     * cultivo de fiscalizacion
     * */
    public static function getCultivoByIdSolicitud($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-') {
            $query = "SELECT cultivo FROM view_semilla_fiscal WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();

            return $obj -> cultivo;
        } else {
            return '-';
        }
    }
        /**
     * cultivo de certificacion
     * */
    public static function getCultivoCertificaByIdSolicitud($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-') {
            $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();

            return $obj -> cultivo;
        } else {
            return '-';
        }
    }
    /**
     * devuelve el cultivo de certificacion
     * @param integer  $id  id de solicitud
     * */
    public static function getCultivoForCertificaByIdSolicitud($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-') {
            $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();

            return $obj -> cultivo;
        } else {
            return '-';
        }
    }

    /**
     * cultivo segun id semilla
     * @param    integer    $id_semilla    id de la semilla
     * 
     * @return   nombre del cultivo
     * */
    public static function getCultivoByIdSemillaForCuenta($id_semilla) {
        $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id_semilla";
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo DBConnector::mensaje(),'=',$query;
        }else{
            $obj = DBConnector::objeto();
            return $obj -> cultivo;    
        }            
    }

    /**
     * devuelve el cultivo segun el id de solicitud
     * para certificacion
     * */
    public static function getCultivoByIdSolicitudCertifica($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-' || is_null($id)) {
            $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id_semilla";
            
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            if (strlen($obj -> cultivo)){
                return $obj -> cultivo;
            }else{
               return '-'; 
            }
        } else {
            return '-';
        }
    }
    /**
     * devuelve el cultivo segun el id de solicitud
     * para fiscalizacion
     * */
    public static function getCultivoByIdSolicitudFiscaliza($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-' || is_null($id)) {
            $query = "SELECT cultivo FROM view_semilla_fiscal WHERE id_semilla=$id_semilla";
            
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            if (strlen($obj -> cultivo)){
                return $obj -> cultivo;
            }else{
               return '-'; 
            }
        } else {
            return '-';
        }
    }    
    /**
     * devuelve el valor de plantin segun el id de solicitud
     * */
     public static function getPlantinByIdSolicitud($id){
         $id_semilla = Tiene::getIdSemilla($id);
         if (is_numeric($id)){
             $query = "SELECT plantin FROM view_semilla WHERE id_semilla = $id_semilla";
             
             DBConnector::ejecutar($query);
             if(DBConnector::nroError()){
                 echo "ERROR ::".__METHOD__.":: $query",DBConnector::mensaje();
             }else{
                 
                 $obj = DBConnector::objeto();
                 return $obj->plantin;
             }
         }
     }

    /**
     * devuelve el id de un cultivo segun el id de solicitud para CUENTA
     * @param integer  $id   numero id de semilla
     * @return nombre de cultivo
     * */
    public static function getCultivoByIdSemillaCertifica($id_semilla) {
        if ($id_semilla != '-') {
            $query = "SELECT cultivo FROM view_semilla WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()) {
                echo DBConnector::mensaje(), '=', $query;
            } else {
                $obj = DBConnector::objeto();

                return $obj -> cultivo;
            }

        } else {
            return '-';
        }
    }

    /**
     * devuelve el cultivo segun el id de semilla
     * @param integer $id nro de campo
     *
     * @return nombre del cultivo que pertenece al id
     * */
    public static function getCultivoByNroCampo($nro) {
        $query = "SELECT cultivo FROM view_semilla WHERE nro_campo='$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
        //echo DBConnector::mensaje();
        $resultado = DBConnector::objeto();

        return $resultado -> cultivo;
    }

    /**
     * lista todos los cultivos
     * */
    public static function getListCultivos() {
        $query = "SELECT DISTINCT(cultivo) FROM view_cultivos_by_id_solicitud";

        DBConnector::ejecutar($query);
    }

    /**
     * lista todos los cultivos  por rango de meses de un area
     *
     * */
    public static function getListCultivosByTime($desde, $hasta, $area, $gestion) {
        $query = "SELECT DISTINCT(cultivo) FROM view_cultivos_by_id_solicitud ";
        $query .= "WHERE area=$area AND estado >= 12 AND ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "(mes >='$desde' AND mes <='$hasta') AND gestion = '$gestion'";

        } else {
            $query .= "(fecha >='$desde' AND fecha <='$hasta') AND gestion = '$gestion'";
        }

        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR lista de cultivos :: ",DBConnector::mensaje(),'=',$query;
    }

    /**
     * lista todos los cultivos por un rango de fechas
     * */
    public static function getListCultivosByDates($desde, $hasta, $area) {
        $query = "SELECT DISTINCT(cultivo) FROM view_cultivos_by_id_solicitud WHERE (fecha >= '$desde' AND fecha <= '$hasta') AND area=$area";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos segun un rango de mes
     * @param  date   $desde    fecha inicial
     * @param  date   $hasta    fecha final
     * */
    public static function getListCultivosSuperficie($desde, $hasta, $gestion = '',$sistema=1) {

        $query = "SELECT DISTINCT(cultivo) FROM view_semilla WHERE cultivo NOT IN ('acondicionamiento','plantines','descanso','barbecho') ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '$gestion' AND sistema = $sistema";
        } else {
            $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta') AND sistema = $sistema";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * lista de cultivos segun un rango de mes
     * @param  date   $desde    fecha inicial
     * @param  date   $hasta    fecha final
     * */
    public static function getListCultivosSuperficieForVolumen($desde, $hasta, $gestion = '') {

        $query = "SELECT DISTINCT(cultivo) FROM view_reporte_volumen_produccion_cultivos_categorias ";
        $query .= "WHERE cultivo NOT IN ('acondicionamiento','plantines','descanso','barbecho') ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' ";
        } else {
            $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta') ";
        }
        $query .= "ORDER BY cultivo ASC";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos segun un rango de mes
     * */
    public static function getListCultivosSuperficieGrafico($desde, $hasta, $provincia, $municipio, $semillera, $semillerista) {

        $query = "SELECT DISTINCT(cultivo) FROM view_reporte_superficies_produccion_semillas ";
        $query .= "WHERE id_provincia LIKE '$provincia' AND id_municipio LIKE '$municipio' AND semillera LIKE '$semillera%' AND semillerista LIKE '$semillerista%' AND ";
        $query .= "cultivo NOT IN ('acondicionamiento','plantines','descanso','barbecho') ";
        $query .= "AND (fecha>='$desde' AND fecha<='$hasta')";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos segun un rango de mes
     * */
    public static function getListCultivosSuperficieRegion($desde, $hasta, $provincia, $municipio, $semillera, $semillerista) {

        $query = "SELECT DISTINCT(cultivo) FROM view_semilla WHERE cultivo NOT IN ('acondicionamiento','plantines','descanso','barbecho') ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') ";
        } else {
            $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta')";
        }
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos de un productor
     * @param string $nombre  nombre de semillerista
     * @param string $apellido apellido de semillerista
     * @param  string $campanha  Verano | Invierno
     *
     * @return ejecucion de consulta
     * */
    public function getCultivosProductor($nombre, $apellido, $campanha, $semillera = '%', $area = 'certifica') {
        $query = "SELECT DISTINCT(cultivo) FROM view_semilla ";
        $query .= "WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%' AND semillera LIKE '$semillera%' AND sistema LIKE '$area%'";
        if ($campanha != '')
            $query .= " AND campanha='$campanha'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve la superficie inscrita
     * @param integer $isemilla  id de la semilla
     * @param integer $isolicitud id de la solicitud
     * */
    public static function getSuperficieInscrita($isemilla, $isolicitud) {
        $query = "SELECT inscrita FROM view_semilla WHERE id_semilla = $isemilla AND id_solicitud = $isolicitud";
        #echo $query;
        DBConnector::ejecutar($query);
        //echo DBConnector::mensaje();
        $resultado = DBConnector::objeto();

        return $resultado -> inscrita;
    }

    public static function updateEstado($isemilla) {
        $query = "UPDATE semilla SET estado = 1 ";
        $query .= "WHERE id_semilla=$isemilla";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo "ERROR al actualizar estado de semilla :: $query";
            exit ;
        }
    }

    /**
     * actualiza el estado de semilla una vez se ingresa la superficie
     * */
    public static function updateEstadoSemilla($isemilla) {
        $query = "UPDATE semilla SET estado = estado + 1 ";
        $query .= "WHERE id_semilla=$isemilla";
        #echo ' -- '. $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualiza el estado de semilla una vez se realiza la superficie
     * */
    public static function updateEstadoSemillaImportar($isemilla) {
        $query = "UPDATE semilla SET estado = estado + 1 ";
        $query .= "WHERE id_semilla=$isemilla";
        #echo ' -- '. $query;
        DBConnector::ejecutar($query);
    }

    /**
     * Lista todos los cultivos existentes
     * @param integer $id id de la solicitud
     *
     * @return cultivo disponible
     * */
    public static function getAllCultivos($id = '') {
        if (!empty($id))
            $query = "SELECT cultivo,comunidad FROM view_cultivos_by_id_solicitud WHERE id_solicitud = $id AND estado IN (6,7) ";
        else {
            $query = "SELECT cultivo FROM view_costos_cultivo";
        }
        #echo $query;
        DBConnector::ejecutar($query);

    }

    /***
     * lista todos los cultivos de un determinado productor
     * */
    public static function getAllCultivosByProductor($id) {
        $query = "SELECT * FROM view_cultivos_by_id_solicitud WHERE id_solicitud = $id";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de semilla segun id
     */
    public static function getSemillaById($id) {
        $query = "SELECT * FROM view_semilla WHERE id_solicitud=$id ORDER BY nro_campo";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de semilla de fiscalizacion segun id
     */
    public static function getSemillaFiscalById($id) {
        $query = "SELECT * FROM view_semilla_fiscal WHERE id_solicitud=$id ORDER BY nro_campo";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualizar lote de semilla
     */
    public static function updateLoteByNroCampo($nro_campo, $nro_lote) {
        $query = "UPDATE semilla SET nro_lote='$nro_lote' WHERE nro_campo=$nro_campo";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
    }

    /**
     * agrega el id de la superficie inscrita a la tabla semilla
     * @param integer $id  id dela semilla sembrada
     * @param double  $superficie  id de la superficie de la parcela
     * */
    public static function updateSemilla($id, $superficie) {
        $query = "UPDATE semilla SET superficie_parcela = $superficie WHERE id_semilla=$id";
        #echo "actualizar superficie de parcela".$query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo "ERROR >> actualizar superficie_parcela de semilla $query";
        }
    }

    /**
     * elimina una semilla sembrada
     * */
    public function deleteSemilla($id) {

        $query = "DELETE FROM semilla WHERE id_semilla=$id";
        //echo $query; die('hasta aqui');
        DBConnector::ejecutar($query);
    }
    //selecciona todas las tuplas de la tabla
    private function getSemillas(){
        $query = "SELECT * from semilla";
        
        DBConnector::ejecutar($query);
    }
    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getSemillaSQL() {
        $query = 'INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela,estado) VALUES';
        $this -> getSemillas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "('";
                $query .= $row -> nro_campo . "','";
                $query .= $row -> campanha . "','" . $row -> cultivo . "','" . $row -> variedad . "','";
                $query .= $row -> categoria_sembrada . "','" . $row -> categoria_producir . "'," . $row -> cantidad_semilla_empleada . ",'";
                $query .= $row -> nro_lote . "','" . $row -> fecha_siembra . "'," . $row -> plantas_por_hectarea . ",";
                $query .= $row -> cultivo_anterior . "," . $row -> superficie_parcela . "," . $row -> estado . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

    /**
     * Lista las categorias de un cultivo
     * */
    public static function getListCategoriaCampo($cultivo, $sistema, $gestion, $desde = '', $hasta = '') {
        $query = "SELECT DISTINCT(categoria_producir) FROM view_semilla ";
        $query .= "WHERE cultivo LIKE '$cultivo' AND gestion = '$gestion' ";
        if ($desde) {
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND estado_solicitud >= 12 AND sistema LIKE '$sistema%' ORDER BY categoria_producir";

            } else {
                $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta') AND estado_solicitud >= 12 AND sistema LIKE '$sistema%' ORDER BY categoria_producir";
            }
        } else {
            $query .= "AND sistema LIKE '$sistema%' ORDER BY categoria_producir";
        }
#echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR lista categorias ::",DBConnector::mensaje();
    }

    /**
     * lote para importar desde excel
     *
     * @return Devuelve un array con el nombre de lote
     * */
    public static function getLoteImportar($lote) {
        if ($lote == "") {
            return array("");
        } else {
            $pos = strpos($lote, ";");
            if ($pos === FALSE) {
                $new_lote = array();
                $piezas_lote = explode("-", $lote);
                if (count($piezas_lote) > 1) {
                    $campos = explode("/", $piezas_lote[1]);
                    if (count($campos) > 1) {
                        //recorrer nros de campos
                        foreach ($campos as $nro) {
                            array_push($new_lote, implode("-", array(trim($piezas_lote[0]), trim($nro), trim($piezas_lote[2]), trim($piezas_lote[3]))));
                        }
                    } else {
                        array_push($new_lote, implode("-", array(trim($piezas_lote[0]), trim($campos[0]), trim($piezas_lote[2]), trim($piezas_lote[3]))));
                    }
                    return $new_lote;
                } else {
                    return array("");
                }
            } else {
                $temp = explode(";", $lote);
                foreach ($temp as $key => $lote_valor) {
                    $new_lote = array();
                    $piezas_lote = explode("-", $lote_valor);
                    if (count($piezas_lote) > 1) {
                        $campos = explode("/", $piezas_lote[1]);
                        if (count($campos) > 1) {
                            //recorrer nros de campos
                            foreach ($campos as $nro) {
                                array_push($new_lote, implode("-", array(trim($piezas_lote[0]), trim($nro), trim($piezas_lote[2]), trim($piezas_lote[3]))));
                            }
                        } else {
                            array_push($new_lote, implode("-", array(trim($piezas_lote[0]), trim($campos[0]), trim($piezas_lote[2]), trim($piezas_lote[3]))));
                        }
                        return $new_lote;
                    } else {
                        return array("");
                    }
                }
            }
        }
    }

    /**
     * lote para importar desde excel
     *
     * @return Devuelve un array con el nombre de lote
     * */
    public static function getLoteFiscalImportar($lote) {
        if ($lote == "") {
            return "";
        } else {
            return $lote;
        }
    }

    /**
     * Lista las categorias de un cultivo
     * */
    public function getTotalCategoriaCampo($sistema, $gestion) {
        $query = "SELECT COUNT(DISTINCT(categoria_producir)) AS total FROM view_semilla ";
        $query .= "WHERE id_sistema LIKE '$sistema%' AND gestion = '$gestion'";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();
        if (DBConnector::filas())
            return $row -> total;
        else {
            return 0;
        }
    }
    /**
     * total de numeros de campo de certificacion
     * */
     public static function getNextNroCampoPlantines(){
         $query = "SELECT count(*) AS total FROM semilla ";
         $query .= "WHERE plantin=1 AND fecha_siembra LIKE '".date('Y')."%'";
         DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
             echo "ERROR ::".__METHOD__.":: $query",DBConnector::mensaje();
         }else{
             $obj = DBConnector::objeto();
             if (!$obj->total){
                 return 100;
             }else{
                return $obj->total+1;    
             }             
         }
     }

    /**
     * ultimo numero de campo
     * */
    public function getMaxNroCampo() {
        $query = "SELECT nro_campo FROM semilla WHERE nro_campo <> '' ORDER BY nro_campo LIMIT 1";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * cuenta la cantidad de campos introducidos
     * */
    public function contarNroCampo($sistema = 'certifica') {
        $query = "SELECT nro_campo AS total FROM view_semilla ";
        $query .= "WHERE id_semilla=(";
        $query .= "SELECT MAX(id_semilla) FROM view_semilla WHERE sistema LIKE '$sistema%' ";
        $query .= "AND gestion = '" . date('Y') . "') ";
#echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR::".__METHOD__."::$query",DBConnector::mensaje();
        }
    }

    public function getVariedadByIdSemillaForCuenta($id) {
        $query = "SELECT variedad FROM view_semilla ";
        $query .= "WHERE id_semilla = $id";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            $obj = DBConnector::objeto();

            return $obj -> variedad;
        }
    }
    /**
     * nombre de variedad segun id_solicitud para certificacion
     * */
    public static function getVariedadByIdSolicitud($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-') {
            $query = "SELECT variedad FROM view_semilla WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            if ($obj -> variedad){
              return $obj -> variedad;  
            }else{
              return '-';  
            }            
        } else {
            return '-';
        }
    }
    /**
     * nombre de variedad segun id_solicitud para fiscalizacion
     * */
    public static function getVariedadByIdSolicitudFiscal($id) {
        $id_semilla = Tiene::getIdSemilla($id);
        if ($id_semilla != '-') {
            $query = "SELECT variedad FROM view_semilla_fiscal WHERE id_semilla=$id_semilla";
            #echo $query;
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            if ($obj -> variedad){
              return $obj -> variedad;  
            }else{
              return '-';  
            }            
        } else {
            return '-';
        }
    }
    /**
     * Devuelve las variedades de un cultivo
     * que se encuentran en un rango determinado
     * @param   string    $cultivo     nombre del cultivo
     * @param   date      $desde       fecha inicial
     * @param   date      $desde       fecha final
     * **/
    public static function getVariedadesCultivoByRango($cultivo, $desde, $hasta) {
        $query = "SELECT DISTINCT(variedad) FROM view_cultivo_variedad ";
        $query .= "WHERE cultivo LIKE '$cultivo%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') ";
        } else {
            $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta')";
        }
        DBConnector::ejecutar($query);
    }

    /**
     * Devuelve las variedades de un cultivo en un rango
     * el cultivo debera tener hoja de produccion para ser tomado en cuenta
     *
     * **/
    public static function getVariedadesCultivo($cultivo, $gestion = '',$desde='',$hasta='',$sistema=1) {
        $query = "SELECT DISTINCT(variedad) FROM view_semilla ";
        $query .= "WHERE cultivo LIKE '$cultivo%' AND id_sistema=$sistema";
        if ($gestion) {
            $query .= " AND gestion = '$gestion' ";
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND estado_solicitud >= 12 ";
            } else {
                $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta') AND estado_solicitud >= 12 ";
            }
        } else {
            $query .= "ORDER BY variedad";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR [variedades] :: ",DBConnector::mensaje();
    }
    /**
     * Devuelve las variedades de un cultivo en un rango
     * el cultivo debera tener hoja de produccion para ser tomado en cuenta
     *
     * **/
    public static function getVariedadesCultivoForVolumenProduccion($cultivo, $desde='',$hasta='') {
        $query = "SELECT DISTINCT(variedad) FROM view_reporte_volumen_produccion_cultivos_categorias ";
        $query .= "WHERE cultivo LIKE '$cultivo%' ";
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query .= "AND (mes >= '$desde' AND mes <= '$hasta') ";
                $query .= "AND gestion = '".date('Y')."' ";
            } else {
                $query .= "AND (fecha_solicitud>='$desde' AND fecha_solicitud<='$hasta') ";
            }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR [variedades] :: ",DBConnector::mensaje();
        else
            return DBConnector::filas();
    }
    /**semillas de productores*/
    public function getSemilleraSolicitud($area) {
        $query = "SELECT * FROM view_semilla";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area";
        $query .= " ORDER BY nro_campo,campanha DESC";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::filas();
    }

    /**semillas de productores*/
    public function getSemillaSolicitud($area) {
        $query = "SELECT * FROM view_semilla ";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND estado_solicitud=1 AND gestion = '" . date('Y') . "'";
        $query .= " ORDER BY id_semilla,nro_campo,campanha DESC";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::filas();
    }

    /**
     * semillas de productores
     * que estan en el proceso de fiscalizacion
     * solo muetra la gestion actual
     * */
    public function getSemilleraSolicitudFiscal() {
        $query = "SELECT * FROM view_semilla_fiscal ";
        $query .= "WHERE gestion = '".date('Y')."' ";
        $query .= " ORDER BY nro_campo DESC";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**semillas de productores*/
    public function getSemilleraSolicitudFiscalByBlock($inicio = 0, $nroReg = 15) {
        $query = "SELECT * FROM view_semilla_fiscal ";
        $query .= "WHERE gestion = '".date('Y')."' ";
        $query .= " ORDER BY id_semilla DESC, nro_campo DESC LIMIT $inicio,$nroReg";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            
        }
    }

    /**
     * ingresa la superficie obtenida desde excel
     * */

}
