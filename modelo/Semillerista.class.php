<?php
/**
 * clase semillerista
 */
class Semillerista {
    static $nombre;
    // nombre del semillerista
    static $apellido;
    // apellido del semillerista
    //id de la semillera
    static $id_semillera;
    /**
     * constructor de la clase
     * */
    public function __construct($nombre, $apellido, $id_semillera) {
        self::$nombre = $nombre;
        self::$apellido = $apellido;
        self::$id_semillera = $id_semillera;
    }

    /**
     * insertar nueva semillera
     *
     * @return id de semillerista
     * */
    public static function setSemillerista() {
        if (!self::checkSemillerista()) {
            $query = "INSERT INTO semillerista (nombre,apellido,id_semillera) VALUES ('" . self::$nombre . "','" . self::$apellido . "'," . self::$id_semillera . ")";
            DBConnector::ejecutar($query);
            //echo $query.'='.DBConnector::mensaje();
            return DBConnector::lastInsertId();
        } else {
            return self::getIdSemillerista();
        }
    }
    /**
     * insertar nueva semillerista y el id de la semillera a la q pertenece
     * @param    string    $nombre         nombre del semillerista
     * @param    string    $apellido       apellido del semillerista
     * @param    integer   $id_semillera   id de semillera 
     *
     * @return id de semillerista
     * */
    public static function setSemilleristaImport($nombre,$apellido,$id_semillera) {
        $query = "SELECT * FROM semillerista WHERE nombre = '$nombre' AND apellido = '$apellido'";
        DBConnector::ejecutar($query);
        
        if (!DBConnector::filas()){
            $query = "INSERT INTO semillerista (nombre,apellido,id_semillera) VALUES ('$nombre','$apellido',$id_semillera)";
            DBConnector::ejecutar($query);
            return DBConnector::lastInsertId();    
        }else{
            $row = DBConnector::objeto();
            
            return $row->id_semillerista;
        }        
    }
    /**
     * verifica si el nombre y apellido ya esta registrado
     *
     * @return bool   TRUE si esta registrado. Otherwise devuelve FALSE
     * */
    private static function checkSemillerista() {
        $query = "SELECT * FROM semillerista WHERE nombre = '" . self::$nombre . "' AND apellido = '" . self::$apellido . "'";

        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    /**
     * lista de semilleristas
     * */
    public static function getSemilleristas(){
        $query = "SELECT * FROM semillerista";
        
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de un semillerista segun el nombre y apellido
     * */
    public static function getIdSemillerista() {
        $query = "SELECT id_semillerista FROM semillerista WHERE nombre = '" . self::$nombre . "' AND apellido = '" . self::$apellido . "'";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_semillerista;
    }

    /**
     * devuelve el id de un semillerista segun el nombre completo
     * @param string $fullname  nombre completo del semillerista
     * */
    public static function getIdSemilleristaByFullName($fullname) {
        $query = "SELECT id_semillerista FROM view_nombre WHERE full_nombre LIKE '$fullname'";
echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError()){
             echo DBConnector::mensaje(),'=',$query;
         } 
        $obj = DBConnector::objeto();

        return $obj -> id_semillerista;
    }
    
    /**
     * devuelve el nombre completo de semillerista  segun el nombre
     * @param string $nombre   nombre de semillerista
     * 
     * */
     public static function getFullNameByNombre($nombre){
         $query = "SELECT full_nombre AS semillerista FROM view_nombre WHERE full_nombre LIKE '$nombre%'";
         #echo $query;
         DBConnector::ejecutar($query);
     }
     /**
     * devuelve el nombre completo de semillerista  segun el nombre
     * @param string $nombre   nombre de semillerista
     * @param char   $sistema c -> certificacion :: f -> fiscalizacion 
     * */
     public static function getFullNameByNombreAndEstado($nombre,$estado,$sistema = 'c'){
         if ($sistema == 'c')
            $query = "SELECT DISTINCT(full_nombre) AS semillerista FROM view_nombre WHERE full_nombre LIKE '$nombre%' AND estado=$estado AND id_sistema = 1";
         else
            $query = "SELECT DISTINCT(full_nombre) AS semillerista FROM view_nombre WHERE full_nombre LIKE '$nombre%' AND estado=$estado AND id_sistema = 2";             
         #echo $query;
         DBConnector::ejecutar($query);
     }
          /**
     * devuelve el nombre completo de semillerista  segun el nombre
     * @param string $nombre   nombre de semillerista
     * 
     * */
     public static function getFullNameByNombreAndEstadoCertificaSuperficie($nombre,$inicio,$fin){
         if($inicio != 8 && $fin != 11){
            $query = "SELECT DISTINCT(semillerista) AS semillerista FROM view_solicitudes_certificacion ";
            $query .= "WHERE semillerista LIKE '%$nombre%' AND (estado>=$inicio AND estado<$fin) AND gestion = '".date('Y')."'";    
         }else{//semilla producida
            $query = "SELECT DISTINCT(nombre_apellido) AS semillerista FROM view_semilla ";
            $query .= "WHERE nombre_apellido LIKE '%$nombre%' AND ((estado_solicitud = 8 AND cultivo = 'papa') OR (estado_solicitud=11 AND cultivo<>'papa')) AND gestion = '".date('Y')."'";
         }
                  
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: ",DBConnector::mensaje();
     }
    /**
     * devuelve el id de un semillerista segun el nombre y apellido
     * que son argumentos de la funcion
     * @param string $nombre nombre y apellido del semillerista
     * @return devuelve el numero id que tiene
     * */
    public static function getIdSemilleristaByNombre($nombre) {
        $query = "SELECT * FROM view_lst_semilleristas WHERE nombre_apellido LIKE ('$nombre%')";

        DBConnector::ejecutar($query);
#echo DBConnector::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> id;
    }

    /**
     * Devuelve el nombre y apellido de un semillerista segun su id
     * */
    public function getNombreById($id) {
        $query = "SELECT CONCAT(nombre,' ',apellido) AS nombre FROM semillerista WHERE id_semillerista = $id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> nombre;
    }
    /**
     * nombre de semillerista por id
     * */
     public static function getNombreSemilleristaById($id){
         $query ="SELECT nombre_apellido AS semillerista FROM view_lst_semilleristas WHERE id=$id";
         #echo $query;
         DBConnector::ejecutar($query);
         
         $obj=DBConnector::objeto();
         
         return $obj->semillerista;
     }

    /**
     * formatea los datos para su uso en un script sql
     * */
     public static function getSemilleristaSQL(){
         $query = 'INSERT INTO semillerista (id_semillerista,nombre,apellido,id_semillera) VALUES';
        self::getSemilleristas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row->id_semillerista.",'";
                $query .= $row -> nombre . "','".$row->apellido."',";
                $query .= $row -> id_semillera . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
}
?>