<?php
/**
 * @author Edwin Willy Arandia Zeballos
 */
class Inspeccion {
    /**
     * Fecha de la primera inspeccion
     * @param 	array[date]	    $etapa_vegetativa   Contiene la fecha de la inspeccion [etapa vegetativa]
     * @param   array[date]     $etapa_floracion    Contiene la fecha de la inspeccion [etapa floracion]
     * @param   array[date]     $etapa_cosecha   Contiene la fecha de la inspeccion [etapa cosecha]
     * @param 	array[int]  	$superficie		    Contiene el id de la superficie.
     *
     * @return string   OK: insertar/actualizar con exito | Error:mensaje de error al guardar
     */
    public static function setInspeccion($inspeccion) {

        $query = "INSERT INTO inspeccion (primera,observacion_1) 
          VALUES ( CAST('" . $inspeccion['etapa_vegetativa'] . "' AS DATE),'" . $inspeccion['observacion_1'] . "')";
        #var_dump($inspeccion['etapa_vegetativa']);
        #echo $query;exit ;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }
    /**
     * Fecha de la primera inspeccion
     * @param   array[date]     $etapa_vegetativa   Contiene la fecha de la inspeccion [etapa vegetativa]
     * @param   array[date]     $etapa_floracion    Contiene la fecha de la inspeccion [etapa floracion]
     * @param   array[date]     $etapa_cosecha   Contiene la fecha de la inspeccion [etapa cosecha]
     * @param   array[int]      $superficie         Contiene el id de la superficie.
     *
     * @return string   OK: insertar/actualizar con exito | Error:mensaje de error al guardar
     */
    public static function setInspeccionImportar($primera,$segunda,$tercera) {

        $query = "INSERT INTO inspeccion (primera,segunda,tercera) ";
        $query .= "VALUES ('$primera' ,'$segunda','$tercera')";
#echo $query;
        DBConnector::ejecutar($query);
        #echo "INSPECCION IMPORTAR : ",$query;
        if (DBConnector::nroError()){
            echo $query;
            exit;    
        }
    }

    /**
     * modifica una superficie
     *
     * @param  $tipo   contiene a que tipo de inspeccion se modificara
     * @param  $tamanho contiene la  nueva fecha
     * @param  $id     contiene el id de la superficie
     */
    public static function updateInspecciones($tipo = '', $inspeccion, $id) {
        if ($tipo == 'primera')
            $query = "UPDATE inspeccion SET primera = CAST('" . $inspeccion['etapa_vegetativa'] . "' AS DATE),observacion_1='" . $inspeccion['observacion_1'] . "' WHERE id_inspeccion=$id";
        if ($tipo == 'segunda')
            $query = "UPDATE inspeccion SET segunda = CAST('" . $inspeccion['etapa_floracion'] . "' AS DATE),observacion_2='" . $inspeccion['observacion_2'] . "' WHERE id_inspeccion=$id";
        if ($tipo == 'tercera')
            $query = "UPDATE inspeccion SET tercera = CAST('" . $inspeccion['etapa_precosecha'] . "' AS DATE),observacion_3='" . $inspeccion['observacion_3'] . "' WHERE id_inspeccion=$id";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualiza el estado de una inspeccion
     * 1 primera inspeccion realizada
     * 2 segunda inspeccion realizada
     * 3 tercera inspeccion realizada
     *
     * @param integer id numero id de la inspeccion
     * */
    public function updEstado($id) {
        $query = "UPDATE inspeccion SET estado = estado + 1 WHERE id_inspeccion=$id";

        DBConnector::ejecutar($query);
    }
    /**
     * actualiza el estado de una inspeccion
     * 1 primera inspeccion realizada
     * 2 segunda inspeccion realizada
     * 3 tercera inspeccion realizada
     *
     * @param integer id numero id de la inspeccion
     * */
    public static function updEstadoImportar($numero,$id) {
        $query = "UPDATE inspeccion SET estado = $numero WHERE id_inspeccion=$id";

        DBConnector::ejecutar($query);
    }
    /**
     * actualiza el estado de la inspeccion
     * 1 primera inspeccion
     * 2 segunda
     * 3 tercera
     * @param integer   $iSuperficie  id de la superficie
     * */
    public static function updInspeccion($iSuperficie) {
        $query = "UPDATE superficie SET estado=estado+1 WHERE id_superficie=$iSuperficie";

        DBConnector::ejecutar($query);
    }

    /**
     * elimina una superficie
     * @param  $id     contiene el id de la solicitud
     */
    function deleteInspeccion($id) {
        $query = "DELETE FROM inspeccion WHERE id_inspeccion=$id";
        //echo $query; die('--');
        DBConnector::ejecutar($query);
    }

    /**
     * obtener todas las inspecciones
     * */
    function getInspeccionSolicitud($area) {
        $query = "SELECT * FROM view_inspecciones_superficie ";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area ";
        $query ."ORDER BY id_inspecciones DESC";   
        DBConnector::ejecutar($query);
    }
    /**
     * obtener todas las inspecciones
     * */
    function getInspeccionPlantines($area) {
        $query = "SELECT * FROM view_inspecciones_plantines ";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area ";
        $query ."ORDER BY id_solicitud DESC";   
        DBConnector::ejecutar($query);
    }
    /**
     * obtener todas las inspecciones por bloque
     * */
    function getInspeccionSolicitudByBlock($area, $inicio, $nroReg) {
        $query = "SELECT * FROM view_inspecciones_superficie";

        if ($area != 10)
            $query .= " WHERE id_sistema=$area";
        $query .= " ORDER BY id_inspeccion DESC LIMIT $inicio,$nroReg";
        #echo $query;exit;

        return $query;
    }
    /**
     * obtener todas las inspecciones por bloque
     * */
    function getInspeccionPlantinesByBlock($area, $inicio, $nroReg) {
        $query = "SELECT * FROM view_inspecciones_plantines";

        if ($area != 10)
            $query .= " WHERE id_sistema=$area";
        $query .= " ORDER BY id_solicitud DESC LIMIT $inicio,$nroReg";
        #echo $query;exit;

        return $query;
    }
    /**
     * inspecciones realizadas
     * @param integer $id  id de superficie o id_inspeccion
     * @param string $tipo  tipo de inspeccion Default:superficie
     *
     * @return retorna el id solicitado inspeccion OR superficie
     * */
    public function getInspecciones($id, $tipo = 'superficie') {
        if ($tipo == 'superficie') {
            $query = "SELECT * FROM view_inspecciones_solicitud WHERE id_superficie=$id";
        } else {
            $query = "SELECT * FROM inspeccion i INNER JOIN superficie_inspeccion si ON si.id_inspeccion = i.id_inspeccion INNER JOIN superficie s ON s.id_superficie=si.id_superficie WHERE s.id_inspeccion=$id";
        }
       #echo $query;
        DBConnector::ejecutar($query);
        #DBConnector::mensaje();
    }
    /**
     * filtro de inspecciones
     * */
    public static function getInspeccionesFilter($search){
        $query = "SELECT semillera,semillerista,nro_campo,primera,observacion_1,segunda,observacion_2,tercera,observacion_3 FROM view_inspecciones_superficie ";
        $query .= "WHERE semillerista LIKE '$search%' OR nro_campo LIKE '$search%' OR primera LIKE '$search%' OR observacion_1 LIKE '$search%' OR ";
        $query .= "segunda LIKE '$search%' OR observacion_2 LIKE '$search%' OR tercera LIKE '$search%' OR observacion_3 LIKE '$search%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * Todas las inspecciones. Utilizada para volcar los datos a un archivo
     * */
    public static function getAllInspecciones() {
        $query = "SELECT * FROM inspeccion";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * inspecciones realizadas segun el id de la superficie
     * */
    public static function getInspeccionesById($id) {
        $query = "SELECT * FROM view_inspecciones_superficie WHERE id_superficie=$id";

        DBConnector::ejecutar($query);
    }
        /**
     * inspecciones realizadas segun el id de la superficie
     * */
    public static function getInspeccionesPlantinById($id) {
        $query = "SELECT * FROM view_inspecciones_plantines ";
        $query .="WHERE id_plantines=$id";

        DBConnector::ejecutar($query);
    } 
    /**
     * inspeccion segun el id de la superficie para edicion
     * */
    public static function getInspeccionById($id) {
        $query = "SELECT * ";
        $query .= "FROM view_inspecciones "; 
        $query .= "WHERE id_superficie=$id ";

        DBConnector::ejecutar($query);
    }

    /**primera inspeccion por el id de superficie*/
    public static function getPrimeraInspeccion($isuperficie) {
        $query = "SELECT primera FROM view_inspecciones WHERE id_superficie = $isuperficie";
        #echo $query; exit;
        DBConnector::ejecutar($query);
        $temp = DBConnector::objeto();
        $primerInspeccion = empty($temp -> primera) ? '00-00-0000' : $temp -> primera;
        return $primerInspeccion;
    }

    /**
     * id de la inspeccoin de acuerdo a la superficie
     * */
    public function getInspeccionByISuperficie($id) {
        $query = "SELECT i.id_inspeccion AS inspeccionID
                   FROM superficie s 
                   INNER JOIN realiza r ON r.id_superficie =s.id_superficie
                   INNER JOIN inspeccion i ON i.id_inspeccion = r.id_inspeccion
                   WHERE s.id_superficie=$id";

        DBConnector::ejecutar($query);
        while ($temp = DBConnector::resultado()) {
            $result = $temp -> inspeccionID;
        }

        return $result;
    }

    /**
     * devuelv el estado de una inspeccion determinada
     * @param  integer id  id de la inspeccion
     *
     * @return integer
     * */
    public static function getEstado($id) {
        $query = "SELECT estado FROM inspeccion WHERE id_inspeccion=$id";

        DBConnector::ejecutar($query);

        $resultado = DBConnector::objeto();

        return $resultado -> estado;
    }
    /**reporte de inspecciones
     * @parm integer $id id de solicitud
     * */
     public static function getRegistro($id){         
         $query = "SELECT vegetativa,observacion_1,floracion,observacion_2,precosecha,observacion_3 ";
         $query .= "FROM view_inspecciones_2 WHERE id_semilla=$id";
         #echo $query;
         DBConnector::ejecutar($query);
         #echo DBConnector::mensaje();
     }
    /***
     * todas las inspecciones realizadas
     * */
    public function getInspeccionesSQL() {
        $query = 'INSERT INTO inspeccion (id_inspeccion,primera,observacion_1,segunda,observacion_2,tercera,observacion_3,estado) VALUES';
        $this -> getAllInspecciones();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_inspeccion . ",'";
                $query .= $row -> primera . "','" . $row -> observacion_1 . "','";
                $query .= $row -> segunda . "','" . $row -> observacion_2 . "','";
                $query .= $row -> tercera . "','" . $row -> observacion_3 . "',";
                $query .= $row -> estado . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
    /**
     * devuelve la ultima fecha valida para importar
     * */
     public static function getLastInspeccionImportar($objPHPExcel,$col,$row){
         
        while (TRUE) {
            $inspeccion['primera'] = $objPHPExcel -> getCellByColumnAndRow($col, $row) -> getCalculatedValue();
            $inspeccion['segunda'] = $objPHPExcel -> getCellByColumnAndRow(($col+1), ($row - 1)) -> getCalculatedValue();
            $inspeccion['tercera'] = $objPHPExcel -> getCellByColumnAndRow(($col+2), ($row - 1)) -> getCalculatedValue();
            $categoria_sembrada = $objPHPExcel -> getCellByColumnAndRow(($col-7), $row) -> getCalculatedValue();
            if ($inspeccion['primera'] !== "") {
                break;
            }else{
                $inspeccion['primera'] = $objPHPExcel -> getCellByColumnAndRow($col, ($row - 1)) -> getCalculatedValue();
                $inspeccion['segunda'] = $objPHPExcel -> getCellByColumnAndRow(($col+1), ($row - 1)) -> getCalculatedValue();
                $inspeccion['tercera'] = $objPHPExcel -> getCellByColumnAndRow(($col+2), ($row - 1)) -> getCalculatedValue();
            }
        }
        $timestamp1 = PHPExcel_Shared_Date::ExcelToPHP($inspeccion['primera']);
        $timestamp2 = PHPExcel_Shared_Date::ExcelToPHP($inspeccion['segunda']);
        $timestamp3 = PHPExcel_Shared_Date::ExcelToPHP($inspeccion['tercera']);
        $inspeccion['primera'] = date("Y-m-d", $timestamp1);
        $inspeccion['segunda'] = date("Y-m-d", $timestamp2);
        $inspeccion['tercera'] = date("Y-m-d", $timestamp3);
        return $inspeccion;
     }

}
?>