<?php
/**
 * clase semillerista
 */
class Comunidad {
    // nombre comunidad
    static $nombre;
    // nombre municipio
    static $id_municipio;
    /**
     * constructor de la clase
     * */
    public function __construct($comunidad,$id_municipio) {
        self::$nombre = $comunidad;
        self::$id_municipio = $id_municipio;
    }

    /**
     * insertar nueva semillera
     * 
     * @return id de semillerista
     * */
    public static function setComunidad() {
        if (!self::checkComunidad()) {
            $query = "INSERT INTO comunidad (nombre_comunidad,id_municipio) VALUES ('".self::$nombre."',".self::$id_municipio.")";
            DBConnector::ejecutar($query);
            #echo $query.'='.DBConnector::mensaje();
            return DBConnector::lastInsertId();
        } else {
            return self::getIdComunidad();
        }
    }

    /**
     * verifica si el nombre y apellido ya esta registrado
     *
     * @return bool   TRUE si esta registrado. Otherwise devuelve FALSE
     * */
    private static function checkComunidad() {
        $query = "SELECT * FROM comunidad WHERE nombre_comunidad = '".self::$nombre. "' AND id_municipio = ".self::$id_municipio;

        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public static function getComunidadImportar($comunidad){
        if (!empty($comunidad)){
            return addslashes($comunidad);
        }else{
            return '';
        }
    } 
     
     /**
     * devuelve el nombre de la comunidad segun el id
     * */
     public static function getNombreComunidad($id){
         $query = "SELECT nombre_comunidad FROM comunidad WHERE id_comunidad = $id";
         
         DBConnector::ejecutar($query);
         
         $obj = DBConnector::objeto();
         
         return $obj->nombre_comunidad;
     }
    /**
     * devuelve el id de un semillerista segun el nombre y apellido
     * */
    public static function getIdComunidad() {
        $query = "SELECT id_comunidad FROM comunidad WHERE nombre = '".self::$nombre. "' AND id_municipio = ".self::$id_municipio;

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_comunidad;
    }

    /**
     * Devuelve el nombre y apellido de un semillerista segun su id
     * */
    public function getComunidadById($id) {
        $query = "SELECT nombre_comunidad AS comunidad FROM comunidad WHERE id_comunidad = $id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> comunidad;
    }
}
?>