<?php
/**
 * clase sistema
 */
class Sistema {

    /**
     * ingresa un nuveo sistema
     * @param $sistema string nombre del sistema
     *
     * @return void
     * */

    public function setSistema($sistema) {
        $query = "INSERT INTO sistema(nombre) VALUES ('$sistema')";

        DBConnector::ejecutar($query);
    }
    /**
     * ingresa un nuveo sistema
     * @param $sistema string nombre del sistema
     *
     * @return void
     * */

    public static function setSistemaImport($sistema) {
        $query = "SELECT * FROM sistema WHERE nombre_sistema LIKE '".strtolower(utf8_decode($sistema))."'";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        if(DBConnector::filas()){
            $row = DBConnector::objeto();
            return $row -> id_sistema;
        }else{
           $query = "INSERT INTO sistema(nombre_sistema) VALUES ('".strtolower(utf8_decode($sistema))."')";
           DBConnector::ejecutar($query);
           return DBConnector::lastInsertId();    
        }
    }
    /**
     * eliminar un sistema
     * @param $id integer  id del sistema
     *
     * @return void
     * */
    public function delSistema($id) {
        $query = "DELETE FROM sistema WHERE id_sistema = $id";

        DBConnector::ejecutar($query);
    }

    /**
     *
     * actualizar sistema
     * @param #id integer  id de sistema
     * @param $new_sistema nombre del nuevo sistema
     *
     *@return void
     * */
    public function updSistema($id, $new_sistema) {
        $query = "UPDATE sistema SET nombre = new_sistema WHERE id_sistema = $id";

        DBConnector::ejecutar($query);
    }

    /**
     * @param $sistema string  nombre del sistema
     *
     * @return ejecuta la consulta y devuelve el id del sistema
     * */
    public static function getIdSistema($sistema) {
        $query = "SELECT id_sistema FROM sistema WHERE nombre_sistema LIKE '$sistema%'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_sistema;
    }
    /**
     * @param $sistema string  nombre del sistema
     *
     * @return ejecuta la consulta y devuelve el id del sistema
     * */
    public static function getIdSistemaById($sistema) {
        $query = "SELECT id_sistema FROM sistema WHERE nombre_sistema LIKE '$sistema%'";
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_sistema;
    }
    /**
     * lista todos los sistemas disponibles
     * */
    public function getSistema(){
        $query = "SELECT id_sistema,nombre_sistema FROM sistema";
        
        DBConnector::ejecutar($query);
    }
    /**
     * devuelve el nombre del sistema segun el id
     * @param $id integer  numero id del sistema
     * 
     * @return nombre del sisteam
     * */
    public static function getSistemaById($id){
        $query = "SELECT nombre_sistema AS sistema FROM sistema WHERE id_sistema = $id";
      #echo $query;  
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::objeto();
        
        return $obj -> sistema;
    } 
    
    /**
     * formatea los datos para su uso en un script sql
     * */
     public function getSistemaSQL(){
         $query = 'INSERT INTO sistema (id_sistema,nombre_sistema) VALUES';
        $this -> getSistema();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row->id_sistema.",'".$row -> nombre_sistema . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
}
?>