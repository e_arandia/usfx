<?php
class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
    //imagen 1
    $img1 = URL.HOME_FOLDER.IMG.'/MDRyT.jpg'; 
    //imagen 2
    $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg'; 
    // Logo
    $this->Image($img1,10,8,33);
    $this->Image($img2,170,8,33);
    //linea de separacion de cabecera y contenido
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    //linea de firma
    $this -> line(90,220,140,220);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Salto de l�nea
    $this->Ln(20);
}

// Pie de p�gina
function Footer()
{
	// Posicion: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Numero de pagina
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creacion del objeto de la clase heredada
$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->setY(28);
$pdf->Cell(0,5,utf8_decode('Costos de Certificación'),0,1,'C');
$pdf->setY(45);
//for($i=1;$i<=40;$i++)
$pdf->setXY(155,30);
$pdf->Cell(0,15,'Fecha: ',0,1);
$pdf->setXY(10,35);
$pdf->Cell(0,15,'Responsable: ',0,1);
$pdf->Cell(0,7,'Semillera: ',0,1);
$pdf->Cell(0,7,'Semillerista: ',0,1);
$pdf->Cell(0,7,utf8_decode('Número Campo: '),0,1);
$pdf->Cell(0,7,'Cultivo: ',0,1);
$pdf->Cell(0,5,'Variedad: ',0,1);
$pdf->Cell(0,5,'Categoria Aprobada: ',0,1);
$pdf->Cell(0,5,'Superficie Aprobada: ',0,1);
$pdf->Cell(0,7,'Superficie Total: ',0,1);
$pdf->Cell(0,15,'Bolsa Etiqueta: ',0,1);
$pdf->Cell(0,5,utf8_decode('Inscripción: '),0,1);
$pdf->Cell(0,5,utf8_decode('Inspección: '),0,1);
$pdf->Cell(0,5,utf8_decode('Analisis, Laboratorio y Etiquetación: '),0,1);
$pdf->Cell(0,5,'Acondicionamiento: ',0,1);
$pdf->Cell(0,5,'Plantines: ',0,1);
$pdf->Cell(0,10,'Total: ',0,1);

$pdf->Cell(0,15,'Monto Pagado: ',0,1);
$pdf->Output();
?>
