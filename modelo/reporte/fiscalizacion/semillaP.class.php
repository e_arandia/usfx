<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 140, 140, 140);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }
    function tituloPagina (){
        $this -> SetFont('Times', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Semilla Producida', 0, 1, 'C');
    }
    function titulos() {
        $this -> SetFont('Times', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15,8, 'Semillera: ', 0, 1);
        $this -> setXY(15,51);
        $this -> Cell(15,8, 'Semillerista: ', 0, 1);
        $posX = $this->w/2;
        $this->setXY($posX,45);
        $this -> Cell(15,8, utf8_decode('Número solicitud: '), 0, 1);
        $this->setXY($posX,51);
        $this -> Cell(15,8, ('Provincia: '), 0, 1);
        $this->setXY($posX,59);
        $this -> Cell(15,8, ('Municipio: '), 0, 1);
        $this->setXY($posX,64);
        $this -> Cell(15,8, ('Comunidad: '), 0, 1);
        
        $this -> setXY(15,59);
        $this -> Cell(15,8, 'Cultivo: ', 0, 1);
        $this -> setXY(15,64);
        $this -> Cell(15,8, 'Variedad: ', 0, 1);
        
        $this -> setXY(15,75);
        $this -> Cell(15,8, 'Semilla Neta: ', 0, 1);
        $this -> setXY(15,81);
        $this -> Cell(15,8, 'Categoria Obtenida: ', 0, 1);
        $this -> setXY(15,87);
        $this -> Cell(15,8, 'Numero de Etiquetas: ', 0, 1);
        /*$this -> setXY(15,93);
        $this -> Cell(15,8, 'Rango de Etiquetas: ', 0, 1);*/
    }
    function contenido ($semillaProducida){
        $this -> SetFont('Times', '', 12);
        $this -> setXY(35,45);
        $this -> Cell(15,8, $semillaProducida['semillera'], 0, 1);
        $this -> setXY(38,51);
        $this -> Cell(15,8, $semillaProducida['semillerista'], 0, 1);
        $posX = $this->w/2;
        $this -> setXY($posX+35,45);
        $this -> Cell(15,8, $semillaProducida['numero'], 0, 1);
        $this -> setXY($posX+20,51);
        $this -> Cell(15,8, $semillaProducida['provincia'], 0, 1);
        $this -> setXY($posX+20,59);
        $this -> Cell(15,8, $semillaProducida['municipio'], 0, 1);
        $this -> setXY($posX+25,64);
        $this -> Cell(15,8, $semillaProducida['comunidad'], 0, 1);
        
        $this -> setXY(32,59);
        $this -> Cell(15,8, $semillaProducida['cultivo'], 0, 1);
        $this -> setXY(35,64);
        $this -> Cell(15,8, $semillaProducida['variedad'], 0, 1);
        
        $this -> setXY(40,75);
        $this -> Cell(15,8, $semillaProducida['semilla_neta']." (T.M.)", 0, 1);
        $this -> setXY(53,81);
        $this -> Cell(15,8, $semillaProducida['categoria_obtenida'], 0, 1);
        $this -> setXY(55,87);
        $this -> Cell(15,8, $semillaProducida['numero_etiqueta'], 0, 1);
    }

    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 8
        $this -> SetFont('Arial', 'I', 8);
        // Numero de pagina
        //$this -> Cell(0, 10, 'Page ' . $this -> PageNo() . '/{nb}', 0, 0, 'C');
        $this -> setXY(27, 135);
        $this -> Cell(0, 15, utf8_decode('Responsable Fiscalización'), 0, 1, 'C');
        $this -> setXY(27, 140);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(215, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulos();
$pdf -> contenido ($semillaProducidaPDF);
$pdf -> Output();
?>
