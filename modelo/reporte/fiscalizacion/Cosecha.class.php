<?php
class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
    //imagen 1
    $img1 = URL.HOME_FOLDER.IMG.'/MDRyT.jpg'; 
    //imagen 2
    $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg'; 
    // Logo
    $this->Image($img1,10,8,33);
    $this->Image($img2,170,8,33);
    //linea de separacion de cabecera y contenido
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    //linea de firma
    $this -> line(90,100,140,100);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Salto de l�nea
    $this->Ln(20);
}

// Pie de p�gina
function Footer()
{
	// Posicion: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Numero de pagina
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creacion del objeto de la clase heredada
$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->setY(28);
$pdf->Cell(0,5,'Superficie de la parcela',0,1,'C');
$pdf->setY(45);
//titulo
$pdf->Cell(0,7,'Semillera: ',0,1);
$pdf->Cell(0,7,'Semillerista: ',0,1);
$pdf->Cell(0,7,'Cultivo: ',0,1);
$pdf->Cell(0,15,'Planta Acondicionadora: ',0,1);
//contenido
$pdf->SetFont('Times','',12);
$pdf->setXY(30,41);
$pdf->Cell(0,15,$cosechaPDF['semillera'],0,1);
$pdf->setXY(34,48);
$pdf->Cell(0,15,$cosechaPDF['semillerista'],0,1);
$pdf->setXY(26,55);
$pdf->Cell(0,15,$cosechaPDF['cultivo'],0,1);
$pdf->setXY(56,66);
$pdf->Cell(0,15,$cosechaPDF['plantaAcondicionadora'],0,1);
//responsable
$pdf->setXY(27,95);
$pdf->Cell(0,15,utf8_decode('Responsable Fiscalización'),0,1,'C');
$pdf->setXY(27,100);
$pdf->Cell(0,15,$_SESSION['usr_nombre'].' '.$_SESSION['usr_apellido'],0,1,'C');
$pdf->Output();
?>
