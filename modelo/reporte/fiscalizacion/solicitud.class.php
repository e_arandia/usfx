<?php
class PDF extends FPDF
{
// Cabecera de pagina
function Header()
{
    //imagen 1
    $img1 = URL.HOME_FOLDER.IMG.'/MDRyT.jpg'; 
    //imagen 2
    $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg'; 
	// Logo
	$this->Image($img1,10,8,33);
    $this->Image($img2,170,8,33);
    //linea de separacion de cabecera y contenido
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    //linea de firma
    $this -> line(85,185,140,185);
	// Arial bold 15
	$this->SetFont('Arial','B',15);
	// Movernos a la derecha
	$this->Cell(80);
	// Salto de l�nea
	$this->Ln(20);
}

// Pie de pagina
function Footer()
{
	// Posicion: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Numero de pagina
	#$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creacion del objeto de la clase heredada
$pdf = new PDF('P','mm',array(220,210));
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->setY(28);
$pdf->Cell(0,5,utf8_decode('Solicitud de Inscripción de Fiscalización'),0,1,'C');
$pdf->setY(45);
//for($i=1;$i<=40;$i++)
$pdf->Cell(0,15,'Numero de solicitud: ',0,1);
$pdf->Cell(0,15,'Sistema: ',0,1);
$pdf->Cell(0,15,'Semillera: ',0,1);
$pdf->Cell(0,15,'Semillerista: ',0,1);
$pdf->Cell(0,15,'Departamento: ',0,1);
$pdf->Cell(0,15,'Provincia: ',0,1);
$pdf->Cell(0,15,'Municipio: ',0,1);
$pdf->Cell(0,15,'Comunidad: ',0,1);
$pdf->Cell(0,15,'Fecha de Solicitud: ',0,1);

$pdf->SetFont('Times','',12);
$pdf->setXY(50,45);
$pdf->Cell(0,15,$solicitudPDF['numero'],0,1);
$pdf->setXY(27,60);
$pdf->Cell(0,15,utf8_decode('Fiscalización'),0,1);
$pdf->setXY(30,75);
$pdf->Cell(0,15,$solicitudPDF['semillera'],0,1);
$pdf->setXY(35,90);
$pdf->Cell(0,15,$solicitudPDF['semillerista'],0,1);
$pdf->setXY(40,105);
$pdf->Cell(0,15,$solicitudPDF['departamento'],0,1);
$pdf->setXY(30,120);
$pdf->Cell(0,15,$solicitudPDF['provincia'],0,1);
$pdf->setXY(32,135);
$pdf->Cell(0,15,$solicitudPDF['municipio'],0,1);
$pdf->setXY(35,150);
$pdf->Cell(0,15,$solicitudPDF['comunidad'],0,1);
$pdf->setXY(45,165);
$pdf->Cell(0,15,$solicitudPDF['fecha'],0,1);
//$this -> line(50,25,203,25);
$pdf->setXY(27,180);
$pdf->Cell(0,15,utf8_decode('Responsable Fiscalización'),0,1,'C');
$pdf->setXY(27,184);
$pdf->Cell(0,15,$_SESSION['usr_nombre'].' '.$_SESSION['usr_apellido'],0,1,'C');
$pdf->Output();
?>
