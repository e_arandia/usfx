<?php
class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
    //imagen 1
    $img1 = URL.HOME_FOLDER.IMG.'/MDRyT.jpg'; 
    //imagen 2
    $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg'; 
    // Logo
    $this->Image($img1,10,8,33);
    $this->Image($img2,170,8,33);
    //linea de separacion de cabecera y contenido
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    //linea de firma
    $this -> line(90,200,140,200);
	// Arial bold 15
	$this->SetFont('Arial','B',15);
	// Movernos a la derecha
	$this->Cell(80);
	// Salto de l�nea
	$this->Ln(20);
}

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('P','mm','Letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->setY(28);
$pdf->Cell(0,5,'Datos Semilla a producir',0,1,'C');
$pdf->setY(45);
//titulos
$pdf->Cell(0,11,'Numero Campo: ',0,1);
$pdf->Cell(0,10,'Semillera: ',0,1);
$pdf->Cell(0,10,'Semillerista: ',0,1);
$pdf->Cell(0,9,'Comunidad: ',0,1);
$pdf->Cell(0,13,'Cultivo: ',0,1);
$pdf->Cell(0,9,'Variedad: ',0,1);
$pdf->Cell(0,10,'Lote: ',0,1);
$pdf->Cell(0,9,'Categoria a Producir: ',0,1);
//contenido
$pdf->SetFont('Times','',12);
$pdf->setXY(30,53);
$pdf->Cell(0,15,$semillaPDF['semillera'],0,1);
$pdf->setXY(35,63);
$pdf->Cell(0,15,$semillaPDF['semillerista'],0,1);
$pdf->setXY(35,72);
$pdf->Cell(0,15,utf8_decode($semillaPDF['comunidad']),0,1);
$pdf->setXY(42,43);
$pdf->Cell(0,15,$semillaPDF['nro_solicitud'],0,1);
$pdf->setXY(26,84);
$pdf->Cell(0,15,$semillaPDF['cultivo'],0,1);
$pdf->setXY(30,95);
$pdf->Cell(0,15,$semillaPDF['variedad'],0,1);
$pdf->setXY(22,104);
$pdf->Cell(0,15,$semillaPDF['lote'],0,1);
$pdf->setXY(51,114);
$pdf->Cell(0,15,$semillaPDF['categoria_producir'],0,1);
//$this -> line(50,25,203,25);
$pdf->setXY(27,195);
$pdf->Cell(0,15,utf8_decode('Responsable Fiscalización'),0,1,'C');
$pdf->setXY(27,200);
$pdf->Cell(0,15,$_SESSION['usr_nombre'].' '.$_SESSION['usr_apellido'],0,1,'C');
$pdf->Output();
?>
