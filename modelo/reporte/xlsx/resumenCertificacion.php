<?php
#cargar plantilla
$plantilla = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . TEMPLATE . "/resumenCertifica.xlsx";
$objReader = PHPExcel_IOFactory::createReader("Excel2007");
$objPHPExcel = $objReader -> load($plantilla);
if (in_array($desde, array("01", "04", "07", "10"))) {
    $desdeXLS = "Trimestre";
    if ($desde == "01") {
        $hastaXLS = "Enero - Febrero - Marzo";
    } elseif ($desde == "04") {
        $hastaXLS = "Abril - Mayo - Junio";
    } elseif ($desde == "07") {
        $hastaXLS = "Julio - Agosto - Septiembre";
    } else {
        $hastaXLS = "Octubre - Noviembre - Diciembre";
    }
} else {
    $desdeXLS = $dates -> cambiar_formato_fecha($desde);
    $hastaXLS = $dates -> cambiar_formato_fecha($hasta);
}
$objPHPExcel -> getActiveSheet() -> SetCellValue('J3', $desdeXLS);
$objPHPExcel -> getActiveSheet() -> SetCellValue('J4', $hastaXLS);
//columna inicial para los municipios
$columna = 3;
# D
$fila = 9;
# 9
//escribir lista de municipios
$contador = 1;
#contador de municipios por columna
foreach ($lst_municipios as $key => $municipio) {
    $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna, $fila, $municipio);
    if ($contador % 4 == 0) {
        $columna++;
        $fila = 9;
        $contador = 1;
    } else {
        $fila++;
        $contador++;
    }
}
//total de comunidades
$objPHPExcel -> getActiveSheet() -> SetCellValue('C14', $totalComunidades);
$objPHPExcel -> getActiveSheet() -> SetCellValue('D14', 'Comunidades de los diferentes municipios.');
//total de productores
$objPHPExcel -> getActiveSheet() -> SetCellValue('C16', $totalProductores);
$objPHPExcel -> getActiveSheet() -> SetCellValue('D16', 'De todas las zonas y comunidades productoras de Semilla');
//columna inicial para las semilleras
$columna = 3;
# D
$fila = 18;
# 18
//escribir lista de semilleras
$contador = 1;
#contador de semilleras por columna
foreach ($lst_semilleras as $key => $semillera) {
    $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna, $fila, $semillera);
    if ($contador % 8 == 0) {
        $columna++;
        $fila = 18;
        $contador = 1;
    } else {
        $fila++;
        $contador++;
    }
}

//columna inicial para los cultivos
$columna = 3;
# D
$fila = 27;
# 27
//columna inicial para las variedades de un cultivo
$columna_var = 3;
# D
$fila_var = 29;
# 29 valor inicial
//lista de cultivos
foreach ($lst_cultivos as $key => $cultivo) {
    $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna, $fila, $cultivo);
    $columna++;
}
$contador = 1;
foreach ($lst_variedades as $key => $variedad) {
    $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_var, $fila_var, utf8_encode($variedad));
    $fila_var++;
    while ($contador <= $lst_total_variedades[$key]) {
        $columna++;
        $fila_var = 29;
        $contador++;
    }
    $columna_var++;
}

//total de categorias de un cultivo
$total_categorias = $semilla -> getTotalCategoriaCampo($sistema, $gestion);
#$objPHPExcel -> getActiveSheet() -> setCellValue('C42', $total_categorias);
#$objPHPExcel -> getActiveSheet() -> setCellValue("C42","=CONTARA(D42:L49)");
//categorias de un cultivo
$columna_cat = 3;
#D
$fila_cat = 42;
foreach ($lst_cultivos as $key => $cultivo) {
    $semilla -> getListCategoriaCampo($cultivo, $sistema, $gestion, $desde, $hasta);
    if (DBConnector::filas()) {
        while ($row = DBConnector::objeto()) {
            $contador = 1;
            #contador de variedades por columna
            $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_cat, $fila_cat, $row -> categoria_producir);
            $fila_cat++;
        }
        $columna++;
        $columna_cat++;
        $fila_cat = 42;
    } else {
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_cat, $fila_cat, '-');
        $columna_cat++;
    }
}
//total superficie aprobada de un cultivo
$columna_sup = 3;
#D
//lista de superficie de cultivo
foreach ($lst_cultivos as $cultivo) {
    if (Semilla::checkCultivo($cultivo,$area,$desde,$hasta,$gestion) ){
        $isemilla = Semilla::getIdSemillaByCultivoGestionForXLSResumen($dates -> search_tilde($cultivo), $area, $desde, $hasta, $gestion);
        Superficie::getSuperficieAprobadaForDetalleResumen($isemilla);
        if (!empty(DBConnector::filas())){
            $obj = DBConnector::objeto();
            $total = $obj->aprobada;
            #var_dump($total);
            if ($total && !is_null($total)) {
                $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_sup, 51, $total);
            } else {
                $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_sup, 51, '0.00');
            }
        }else{
            $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_sup, 51, '0.00');
        }
        $columna_sup++;
    }
}

//total de volumen de produccion de un cultivo
$columna_vol = 3;
#D
foreach ($lst_cultivos as $cultivo) {
    HojaCosecha::getVolumenTotalByCultivo($cultivo, $area, $desde, $hasta, $gestion);
    #var_dump(DBConnector::objeto());
    $row = DBConnector::objeto();
    if ($row -> total > 0.00) {
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_vol, 53, $row -> total);
    } else {
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna_vol, 53, '0.00');
    }
    $columna_vol++;
}
/*
//informe  de solicitudes
$objPHPExcel -> getActiveSheet() -> SetCellValue('D63', $total_finalizadas);
$objPHPExcel -> getActiveSheet() -> SetCellValue('D64', $total_pendientes);
*/
//muestras de laboratorio
$objPHPExcel -> getActiveSheet() -> SetCellValue('E59', $muestras['aprobadas']);
$objPHPExcel -> getActiveSheet() -> SetCellValue('F59', $muestras['rechazadas']);
$objPHPExcel -> getActiveSheet() -> SetCellValue('G59', $muestras['total']);
$objPHPExcel -> getActiveSheet() -> SetCellValue('H59', $muestras['fiscalizadas']);

//seguridad de documento
$excel -> setDocumentSecurity($objPHPExcel, 'iniaf-CH');
$excel -> setWorkSheetSecurity($objPHPExcel, 'iniaf-CH');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$filename = 'Resumen_' . ucfirst($sistema) . 'cion-' . date('Y') . '_' . date('Hismy') . '.xlsx';
$file = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . '/vista/reportes/' . $filename;
$ubicacion_archivo = '/' . HOME_FOLDER . '/vista/reportes/' . $filename;

$objWriter -> save($file);
?>