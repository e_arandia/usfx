<?php
#cargar plantilla
$plantilla = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . TEMPLATE . "/superficieProduccion.xlsx";
$objReader = PHPExcel_IOFactory::createReader("Excel2007");
$objPHPExcel = $objReader -> load($plantilla);

#titulo de hoja
$titulo = 'Superficie_Produccion';
$objPHPExcel -> getActiveSheet() -> SetTitle($titulo);
#valores iniciales
$fila = 10;
# 9
$contador = count($lst_cultivos);
$total_ins = 0;
$total_ret = 0;
$total_rech = 0;
$total_apr = 0;
$total_ptj = 0;
for ($i = 0; $i < $contador; $i++) {
    $objPHPExcel -> getActiveSheet() -> SetCellValue('C' . $fila, $lst_cultivos[$i]);
    $objPHPExcel -> getActiveSheet() -> SetCellValue('D' . $fila, number_format($superficies['inscrita'][$i],2,'.',','));
    $total_ins += floatval($superficies['inscrita'][$i]);
    $objPHPExcel -> getActiveSheet() -> SetCellValue('E' . $fila, number_format($superficies['rechazada'][$i],2,'.',','));
    $total_rech += floatval($superficies['rechazada'][$i]);
    $objPHPExcel -> getActiveSheet() -> SetCellValue('F' . $fila, floatval($superficies['retirada'][$i]));
    $total_ret += floatval($superficies['retirada'][$i]);
    $objPHPExcel -> getActiveSheet() -> SetCellValue('G' . $fila, floatval($superficies['aprobada'][$i]));
    $total_apr += floatval($superficies['aprobada'][$i]);
    $fila++;
}
//calculo de porcentaje  de cada cultivo
$superficies['porcentaje'] = array();
if ($total_apr) {
    foreach ($superficies['aprobada'] as $valor) {
        $total = ($valor * 100) / $total_apr;
        array_push($superficies['porcentaje'], $total);
    }
} else {
    foreach ($superficies['aprobada'] as $valor) {
        array_push($superficies['porcentaje'], 0);
    }
}

//porcentaje de cada cultivo
for ($j = 0; $j < $contador; $j++) {
    $objPHPExcel -> getActiveSheet() -> SetCellValue('H' . ($j + 10), floatval($superficies['porcentaje'][$j]));
    #echo $superficies['porcentaje'][$j],'=';
}

$total_porcentaje = array_sum($superficies['porcentaje']);
$fila_total = 10 + $contador;
//rango de fecha
if (in_array($desde, array("01", "04", "07", "10"))) {
    $desdeXLS = "Trimestre :";
    if ($desde == "01") {
        $hastaXLS = "Enero - Febrero - Marzo";
    } elseif ($desde == "04") {
        $hastaXLS = "Abril - Mayo - Junio";
    } elseif ($desde == "07") {
        $hastaXLS = "Julio - Agosto - Septiembre";
    } else {
        $hastaXLS = "Octubre - Noviembre - Diciembre";
    }
} else {
    $desdeXLS = "DEL " . $dates -> cambiar_formato_fecha($desde);
    $hastaXLS = "AL " . $dates -> cambiar_formato_fecha($hasta);
    $excel -> setNormalFont($objPHPExcel, 'C5', 'C5');
}
$objPHPExcel -> getActiveSheet() -> SetCellValue('C5', $desdeXLS);
$objPHPExcel -> getActiveSheet() -> SetCellValue('F5', $hastaXLS);
//suma total de superficies
$objPHPExcel -> getActiveSheet() -> SetCellValue('C' . $fila_total, 'TOTAL');
$objPHPExcel -> getActiveSheet() -> SetCellValue('D' . $fila_total, $total_ins);
$objPHPExcel -> getActiveSheet() -> SetCellValue('E' . $fila_total, $total_rech);
$objPHPExcel -> getActiveSheet() -> SetCellValue('F' . $fila_total, $total_ret);
$objPHPExcel -> getActiveSheet() -> SetCellValue('G' . $fila_total, $total_apr);
$objPHPExcel -> getActiveSheet() -> SetCellValue('H' . $fila_total, $total_porcentaje);
//bordes de celdas con datos
$objPHPExcel -> getActiveSheet() -> getStyle('C9:H' . $fila_total) -> getBorders() -> getAllBorders() -> setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//texto en negrita
$objPHPExcel -> getActiveSheet() -> getStyle('C' . $fila_total . ':H' . $fila_total) -> getFont() -> setBold(true);

#echo $rango;
// definir origen de valores
$valores = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Superficie_Produccion!$H$10:$H' . ($fila_total - 1), NULL, 1));
// definir origen los rótulos de colunas
$cultivos = array(new PHPExcel_Chart_DataSeriesValues('String', 'Superficie_Produccion!$C$10:$C' . ($fila_total - 1), NULL, 1));

//crear nueva hoja para grafico
$objPHPExcel -> createSheet(1);
$objPHPExcel -> setActiveSheetIndex(1);

//crear grafico en torta
$series = new PHPExcel_Chart_DataSeries(PHPExcel_Chart_DataSeries::TYPE_PIECHART_3D, // plotType
NULL, // plotGrouping
range(0, count($valores) - 1), // plotOrder
$valores, // plotLabel
$cultivos, // plotCategory
$valores // plotValues
);
//valores de partes de la torta
$layout1 = new PHPExcel_Chart_Layout();
$layout1 -> setShowVal(TRUE);
// Initializing the data labels with Values

//direccion de columnas
#$series -> setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COLUMN);
//  Set the series in the plot area
$plotarea = new PHPExcel_Chart_PlotArea($layout1, array($series));
//  Leyenda de grafico
$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, FALSE);

//titulo de grafico
$title = new PHPExcel_Chart_Title('SUPERFICIE DE SEMILLA CERTIFICADA POR CULTIVO (%)');

//  Create the chart
$chart = new PHPExcel_Chart('chart1', // name
$title, // title
$legend, // legend
$plotarea, // plotArea
TRUE, // plotVisibleOnly
0, // displayBlanksAs
NULL, // xAxisLabel
NULL // yAxisLabel
);

//  Set the position where the chart should appear in the worksheet
$chart -> setTopLeftPosition('B3');
$chart -> setBottomRightPosition('L18');
//  Add the chart to the worksheet
$nueva_hoja = $objPHPExcel -> getActiveSheet();
//cambiar nombre de hoja
$nueva_hoja -> setTitle("Grafico") -> addChart($chart);

$objPHPExcel -> setActiveSheetIndex(0);
//seguridad de documento
$excel->setDocumentSecurity($objPHPExcel, 'iniaf-CH');
$excel->setWorkSheetSecurity($objPHPExcel, 'iniaf-CH');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter -> setIncludeCharts(TRUE);
$filename = 'Produccion de semillas(superficie)' . '_' . date('Hismy') . '.xlsx';
$file = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . '/vista/reportes/' . $filename;
$ubicacion_archivo = '/' . HOME_FOLDER . '/vista/reportes/' . $filename;

$objWriter -> save($file);
?>