<?php
$objPHPExcel = new PHPExcel();
$objPHPExcel -> getDefaultStyle() -> getFont() -> setName('Arial');//fuente por defecto
$objPHPExcel -> getDefaultStyle() -> getFont() -> setSize(8);//tamaño de fuente 
$excel->setImagen($objPHPExcel, 'logoCH.jpg', 'B1',200,130);
$excel->setImagen($objPHPExcel, 'descripcion.jpg', 'G1',280,130);
$excel->setPagePropierties($objPHPExcel, 'detalle');
$excel->setCabecera($objPHPExcel, 'detalle');
#total de semilleras
$total_semilleras = count($lst_semillera);
//total de semilleristas de una semillera y un cultivo en un rago de fecha
$total_semilleristas = array();
//columnas que contienen los datos
$lst_columna = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R');
$objPHPExcel -> getActiveSheet() -> getColumnDimension('C') -> setVisible(FALSE); //ocultar columa semillera
$objPHPExcel -> getActiveSheet() -> getColumnDimension('F') -> setVisible(FALSE); //ocultar columa cultivo

for ($i=0; $i <$total_semilleras ; $i++) {
    $objPHPExcel -> setActiveSheetIndex($i);//colocar activa la primera hoja 
    //seguridad de documento
    $excel->setDocumentSecurity($objPHPExcel, 'iniaf-CH');
    $excel->setWorkSheetSecurity($objPHPExcel, 'iniaf-CH');   
    $clonedSheet = clone $objPHPExcel->getActiveSheet(); //clonar hoja que contiene la plantilla      
    $nueva_hoja = $objPHPExcel -> getActiveSheet();//asignar hoja activa a variable
    //cambiar nombre de hoja
    $nueva_hoja -> setTitle(substr(str_ireplace(array("/","?",":"), "_", $lst_semillera[$i]), 0,30)  );//cambiar nombre de hoja
    $objPHPExcel->addSheet($clonedSheet); //agregar hoja clonada a libro  
    //total de semilleristas que estan en la semillera
    $reporte->getCantidadByCultivo($lst_semillera[$i],$cultivo,$desde,$hasta);
    while($obj = DBConnector::objeto()){
        array_push($total_semilleristas,$obj->cantidad);
    }  
}
$nro_hojas = $objPHPExcel->getSheetCount();// cantidad de hojas en el archivo
$objPHPExcel -> setActiveSheetIndex(($nro_hojas-1));  //colocar activa la hoja que contiene la plantilla
$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);//ocultar la hoja de la plantilla
//recorremos el array para ingresar los datos en cada hoja
$aux = 0;
if ( in_array($desde, array("01", "04", "07", "10")) ){
    $desdeXLS = "Trimestre";
    if ($desde == "01"){        
        $hastaXLS = "Enero - Febrero - Marzo";
    }elseif ($desde == "04") {
        $hastaXLS =  "Abril - Mayo - Junio";
    }elseif ($desde == "07"){
        $hastaXLS = "Julio - Agosto - Septiembre";
    }else{
        $hastaXLS = "Octubre - Noviembre - Diciembre";
    }
}else{
    $desdeXLS = $dates->cambiar_formato_fecha($desde);
    $hastaXLS = $dates->cambiar_formato_fecha($hasta);   
}
for ($i=0; $i <$total_semilleras ; $i++) {
    $objPHPExcel -> setActiveSheetIndex($i);
    $contador_semilleristas = 0;
    $nro_total_semilleristas = $total_semilleristas[$i];    
    $columna = 0; //A
    $fila = 13; //13
    $objPHPExcel -> getActiveSheet() -> mergeCells('A8:B8');    
    $objPHPExcel -> getActiveSheet() -> SetCellValue('A7', "DETALLE CERTIFICACIÓN, PRODUCCIÓN Y COSTOS");    
    $objPHPExcel -> getActiveSheet() -> SetCellValue('P7', $desdeXLS);    
    $objPHPExcel -> getActiveSheet() -> SetCellValue('P8', $hastaXLS);
    $objPHPExcel -> getActiveSheet() -> SetCellValue('A8', 'SEMILLERA :');
    $objPHPExcel -> getActiveSheet() -> SetCellValue('B9', 'CULTIVO :');
    $objPHPExcel -> getActiveSheet() -> SetCellValue('D8', $lst_semillera[$i]);
    $objPHPExcel -> getActiveSheet() -> setCellValue('D9',$lst_cultivo[0]);
    if ($desdeXLS != "Trimestre" ){
        $objPHPExcel -> getActiveSheet() -> SetCellValue('O7', 'DEL');
        $objPHPExcel -> getActiveSheet() -> SetCellValue('O8', 'AL');
    }else{
        $excel->setBoldFont($objPHPExcel, 'P7', 'P7',TRUE);
    }
    $excel->setRightText($objPHPExcel, 'A8', 'B9',TRUE); //alineacion de texto a la derecha para semillera
    $excel->setRightText($objPHPExcel, 'O7', 'O8',TRUE); //alineacion de texto a la derecha para texto si se eligio rango de fecha
    $excel->setBoldFont($objPHPExcel, 'A7', 'B9');//alineacion de texto a la derecha para cultivo
    $excel->setBoldFont($objPHPExcel, 'B8', 'B8',TRUE);  //texto en negrita para semillera  
    $excel->setBoldFont($objPHPExcel, 'M8', 'M8',TRUE); //texto en negrita para cultivo
    $excel->setBoldFont($objPHPExcel, 'O7', 'O8');
    $objPHPExcel -> getActiveSheet() -> mergeCells('P7:R7'); //combinacion de celdas para texto trimestre o fecha inicial
    $objPHPExcel -> getActiveSheet() -> mergeCells('P8:R8'); //combinacion de celdas para texto de meses o fecha final
    $total_superficie = $total_aprobada = $total_rechazada = $total_bolsas = 0;  
    $total_inscripcion = $total_campo = $total_etiqueta = $total_total = 0;
    while ($contador_semilleristas < $nro_total_semilleristas) {
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($columna, $fila, ($contador_semilleristas+1));
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+1), $fila, $lst_fecha[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+2), $fila, $lst_semillera[$i]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+3), $fila, $lst_comunidad[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+4), $fila, $lst_semillerista[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+5), $fila, $lst_cultivo[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+6), $fila, $lst_variedad[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+7), $fila, $lst_cat_sembrada[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+8), $fila, $lst_superficie[$contador_semilleristas+$aux]);
        $total_superficie += $lst_superficie[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+9), $fila, $lst_nocampo[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+10), $fila, $lst_aprobada[$contador_semilleristas+$aux]);
        $total_aprobada += $lst_aprobada[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+11), $fila, $lst_rechazada[$contador_semilleristas+$aux]);
        $total_rechazada += $lst_rechazada[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+12), $fila, $lst_cat_aprobada[$contador_semilleristas+$aux]);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+13), $fila, $lst_nobolsa[$contador_semilleristas+$aux]);
        $total_bolsas += $lst_nobolsa[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+14), $fila, $lst_inscripcion[$contador_semilleristas+$aux]);
        $total_inscripcion += $lst_inscripcion[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+15), $fila, $lst_campo[$contador_semilleristas+$aux]);
        $total_campo += $lst_campo[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+16), $fila, $lst_etiqueta[$contador_semilleristas+$aux]);
        $total_etiqueta += $lst_etiqueta[$contador_semilleristas+$aux];
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+17), $fila, $lst_total[$contador_semilleristas+$aux]);
        $total_total += $lst_total[$contador_semilleristas+$aux];
        $fila++;
        $contador_semilleristas++;
        //borde a columnas
        foreach ($lst_columna as $column) {
            $excel->setLeftBorder($objPHPExcel, 'A'.($fila-1), $column . ($fila-1),'G');
            $excel->setRightBorder($objPHPExcel, 'A'.($fila-1), $column. ($fila-1),'G');
            $excel->setBottomBorder($objPHPExcel, 'A'.($fila-1), $column . ($fila-1),'D');    
        }                
    }
        //ultima fila con los totales
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+8), $fila,$total_superficie);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+10), $fila,$total_aprobada);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+11), $fila,$total_rechazada);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+13), $fila,$total_bolsas);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+14), $fila,$total_inscripcion);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+15), $fila,$total_campo);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+16), $fila,$total_etiqueta);
        $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow(($columna+17), $fila,$total_total); 
        $objPHPExcel -> getActiveSheet() -> mergeCells('A'.$fila.':H'.$fila);
        $objPHPExcel -> getActiveSheet() -> SetCellValue('A'.$fila, 'T O T A L');
        $excel->setBoldFont($objPHPExcel, 'A'.$fila,'R'.$fila,TRUE);
        //borde a la ultima fila que contiene los totales
        foreach ($lst_columna as $column) {
            $excel->setTopBorder($objPHPExcel, 'A'.$fila, $column . $fila,'G');
            $excel->setLeftBorder($objPHPExcel, 'A'.$fila, $column . $fila,'G');
            $excel->setRightBorder($objPHPExcel, 'A'.$fila, $column . $fila,'G');
            $excel->setBottomBorder($objPHPExcel, 'A'.$fila, $column . $fila,'G');    
        }
        $excel->setPrinterArea($objPHPExcel, 'A1', 'R'.$fila);
        $excel->setCenterText($objPHPExcel, 'A13', $column . $fila,TRUE);      
    $aux += $nro_total_semilleristas;  
}

$objPHPExcel -> setActiveSheetIndex(0);//colocar primer hoja activa

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$filename = 'Detalle de produccion' . ' ' . date('Y') . '_' . date('Hismy') . '.xlsx';
$file = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . '/vista/reportes/' . $filename;
$objWriter -> save($file);

?>