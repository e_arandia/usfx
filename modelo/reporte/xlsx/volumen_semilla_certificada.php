<?php
#cargar plantilla
$plantilla = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . TEMPLATE . "/volumenProduccion.xlsx";
$objReader = PHPExcel_IOFactory::createReader("Excel2007");
$objPHPExcel = $objReader -> load($plantilla);

$vol_col_cult = 1;#B  categoria
$vol_fila_cult = 11;#10

$vol_col_sbt = 2;#C subtotal
$vol_fila_sbt = 11;#11

$vol_col_var = 2;#C variedad
$vol_fila_var = 12;# 11

$vol_col_cat = 3;#D categoria Basica
$vol_fila_cat = 12;#11
             
#titulo de hoja
$titulo = 'Volumen_Produccion';
$objPHPExcel -> getActiveSheet() -> SetTitle($titulo);
//rango de fecha o trimestre

if (in_array($desde, array("01", "04", "07", "10"))) {
    $desdeXLS = "Trimestre :";
    if ($desde == "01") {
        $hastaXLS = "Enero - Febrero - Marzo";
    } elseif ($desde == "04") {
        $hastaXLS = "Abril - Mayo - Junio";
    } elseif ($desde == "07") {
        $hastaXLS = "Julio - Agosto - Septiembre";
    } else {
        $hastaXLS = "Octubre - Noviembre - Diciembre";
    }
    $excel -> setBoldFont($objPHPExcel, 'D5', 'D5');
    $excel -> setRightText($objPHPExcel,'D5', 'D5');
} else {
    $desdeXLS = "DEL " . $dates -> cambiar_formato_fecha($desde);
    $hastaXLS = "AL " . $dates -> cambiar_formato_fecha($hasta);
    $excel -> setNormalFont($objPHPExcel, 'D5', 'D5');
}

$objPHPExcel -> getActiveSheet() -> SetCellValue('D5', $desdeXLS);
$objPHPExcel -> getActiveSheet() -> SetCellValue('F5', $hastaXLS);

$volumen['porcentaje'] = array();
foreach ($lst_cultivos as $i => $cultivo_name) {
    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow($vol_col_cult,$vol_fila_cult,$cultivo_name);//nombre de la variedad
    //alineacion de texto al centro de celdas combinadas
    $excel->setCenterText($objPHPExcel, 'B'.$vol_fila_cult,'C',FALSE);
    $total_variedades = $lst_variedades_cultivo[$i];
    
    while ($total_variedades <= $lst_variedades_cultivo[$i]) {
        foreach ($lst_variedades as $j => $variedad_name) {
            $objPHPExcel -> getActiveSheet() -> setCellValueByColumnAndRow($vol_col_sbt,$vol_fila_sbt,"SUB TOTAL");
            //alineacion de texto al centro de celdas combinadas
            $excel->setRightText($objPHPExcel, 'c'.$vol_fila_sbt,'C',FALSE);
            //texto en negrita para subtotales
            $objPHPExcel -> getActiveSheet() -> getStyle('C'.$vol_fila_sbt.':J'.$vol_fila_sbt) -> getFont() -> setBold(true);
            $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow($vol_col_var,$vol_fila_var,utf8_encode($variedad_name));//nombre de la variedad
            foreach ($lst_categorias as $k => $categoria_name) {
                $total_basica=$total_registrada=$total_certificada = 0;
                $total_certificadaB = $total_fiscalizada = 0;
                foreach ($lst_volumen_produccion_categorias as $x => $valor_categoria) {
                    //sumar subtotales
                    $contar_variedades = 1;
                    $total_volumen_produccion = (count($lst_volumen_produccion_categorias)-5);
                    if ($contar_variedades < $total_volumen_produccion){
                        $total_basica = 0;
                        $total_registrada = 0;
                        $total_certificada = 0;
                        $total_certificadaB = 0;
                        $total_fiscalizada = 0;
                    }else{
                        while($contar_variedades <= $total_volumen_produccion){
                            $total_basica += $lst_volumen_produccion_categorias[$x];
                            $total_registrada +=$lst_volumen_produccion_categorias[$x+1];
                            $total_certificada +=$lst_volumen_produccion_categorias[$x+2];
                            $total_certificadaB +=$lst_volumen_produccion_categorias[$x+3];
                            $total_fiscalizada +=$lst_volumen_produccion_categorias[$x+4];
                            
                            $contar_variedades++;
                        }
                    }
                    //total de categorias
                    $total_categorias = $total_basica+$total_registrada+$total_certificada+$total_certificadaB+$total_fiscalizada;
                    //escribir totales de cada categoria
                    $vol_col_cat_sbt = 3;
                    $vol_fila_cat_sbt = ($vol_fila_cat-1);
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow($vol_col_cat_sbt,$vol_fila_cat_sbt,$total_basica);
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow( ($vol_col_cat_sbt+1),$vol_fila_cat_sbt,$total_registrada);
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow( ($vol_col_cat_sbt+2),$vol_fila_cat_sbt,$total_certificada);
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow( ($vol_col_cat_sbt+3),$vol_fila_cat_sbt,$total_certificadaB);
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow( ($vol_col_cat_sbt+4),$vol_fila_cat_sbt,$total_fiscalizada);                    
                    /////////////////cantidad en cada categoria//////////////////////////////
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow(($vol_col_cat+0),$vol_fila_cat,$valor_categoria); 
                    /////////////////////////////TOTALES///////////////////////////////////////////////////////////
                    $objPHPExcel -> getActiveSheet() ->setCellValueByColumnAndRow( ($vol_col_cat_sbt+5),$vol_fila_cat_sbt,$total_categorias);
                    if ( ($x+1) % 5 == 0){
                        $vol_fila_cat += $lst_variedades_cultivo[$i]+1;
                        break;
                    }                           
                    $vol_col_cat++;      
                }
                $vol_col_cat = 3; //D
                $vol_fila_cat = 12; //11
                break;
            }
        }
        $total_variedades++;
    }
    $objPHPExcel -> getActiveSheet() -> mergeCells('B'.$vol_fila_cult.':B'.($vol_fila_cult+$total_variedades-1));
    $vol_fila_cult += ($total_variedades+1);
}
// combinacion de celdas para el texto total
$objPHPExcel -> getActiveSheet() -> mergeCells('B'.($vol_fila_cult-1).':C'.($vol_fila_cult-1));

//alineacion de texto a la derecha
$excel->setRightText($objPHPExcel, 'B'.($vol_fila_cult-1), '',FALSE);
$objPHPExcel -> getActiveSheet() -> setCellValue('B'.($vol_fila_cult-1),"TOTAL");
//bordes de celdas con datos
$objPHPExcel -> getActiveSheet() -> getStyle('B10:J' . ($vol_fila_cult-1)) -> getBorders() -> getAllBorders() -> setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//texto en negrita
$objPHPExcel -> getActiveSheet() -> getStyle('B'.($vol_fila_cult-1).':J' . ($vol_fila_cult-1)) -> getFont() -> setBold(true);
$excel->setPrinterArea($objPHPExcel, 'A1', 'K' . $vol_fila_cult);

//seguridad de documento
$excel->setDocumentSecurity($objPHPExcel, 'iniaf-CH');
$excel->setWorkSheetSecurity($objPHPExcel, 'iniaf-CH');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter -> setIncludeCharts(TRUE);

$filename = 'Volumen de semillas' . '_' . date('Hismy') . '.xlsx';
$file = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . '/vista/reportes/' . $filename;
$ubicacion_archivo = '/' . HOME_FOLDER . '/vista/reportes/' . $filename;

$objWriter -> save($file);
?>