<?php

class PDF  extends FPDF {
    //cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL.HOME_FOLDER.IMG.'/descripcion.jpg';
        //imagen 2
        $img2 = URL.HOME_FOLDER.IMG.'/logoCH.jpg';
        // Logo
        $this -> Image($img1, 65, 15, 85);
        $this -> Image($img2, 18, 13, 33);
        // Salto de lInea
        $this -> Ln(2);
        #nombre de usuario y fecha de impresion
        $posX_final=$this->w;
        $this -> SetFont('Arial', 'B', 6);
        $this->setXY(($posX_final-40),18);
        $this -> Cell(0, 15, utf8_decode('USUARIO : '.$_SESSION['usr_login']), 0, 1);
        $this->setXY(($posX_final-40),21);
        $this -> Cell(0, 15, utf8_decode('FECHA : '.date('d - m - Y')), 0, 1);
    }
    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        
        $posX_inicio = 15;
        $posX_final = $this->w;
        $posX_medio = ($posX_final)/2;
        $posX_cuarto1 = $posX_medio/2;
        $posX_cuarto2 = ($posX_medio+$posX_final)/2;
        $posX_octavo1 = ($posX_inicio+$posX_cuarto1)/2;
        $posX_octavo2 = ($posX_cuarto1+$posX_medio)/2;
        $posX_octavo3 = ($posX_medio+$posX_cuarto2)/2;
        $posX_octavo4 = ($posX_cuarto2+$posX_final)/2;
        
        //linea de firma administrador
        $this -> line(($posX_inicio+25), 130, ($posX_medio), 130);
        #line de firma responsable
        $this -> line(($posX_medio+40), 130, $posX_octavo4+5, 130);
        
        $this -> setXY(($posX_inicio+50), 125);
        $this -> Cell(15, 15, 'Responsable Departamental o Regional', 0, 1, 'C');
        $this -> setXY($posX_octavo3, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY($posX_octavo3, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }    

}

$pdf = new PDF('L', 'mm', array(215,150));

$pdf -> SetFont('Arial', '', 8);
$pdf -> AddPage();

//imagen que contiene el grafico
$grafico_img = URL.HOME_FOLDER.VIEW.'/reportes/'.$imagen.'.png';
$pdf -> Image($grafico_img, 50, 40, 105);
$pdf -> Output();
?>
