<?php
class PDF extends FPDF {
    // Cabecera de pagina
    function Header() {
        //imagen ministerio
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen iniaf
        $img2 = URL . HOME_FOLDER . IMG . '/iniaf2.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 230, 8, 33);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 10);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
        $this -> setXY(5, 9);
        $this -> Cell(0, 7, 'MINISTERIO DE DESARROLLO RURAL Y TIERRAS ', 0, 1, 'C');
        $this -> setXY(5, 13);
        $this -> Cell(0, 7, 'INSTITUTO NACIONAL DE INNOVACION AGROPECUARIA Y FORESTAL ', 0, 1, 'C');
        $this -> setXY(5, 17);
        $this -> Cell(0, 7, 'OFICINA DEPARTAMENTAL INIAF CHUQUISACA ', 0, 1, 'C');
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(5, 25);
        $this -> Cell(0, 5, utf8_decode('Formulario N° 1A'), 0, 1, 'C');
        $this -> SetFont('Arial', 'B', 14);
        $this -> setXY(106, 25);
        $this -> Cell(0, 15, 'ESTADO DE ACREEDORES', 0, 1);
        $this -> SetFont('Arial', '', 9);
        $this -> setXY(118, 30);
        $this -> Cell(0, 15, 'SEMILLERA ', 0, 1);
        $this -> SetFont('Arial', '', 7);
        $this -> setXY(120, 33);
        $this -> Cell(0, 15, '(Expresado en Bolivianos)', 0, 1);

        //CABECERA DE TABLA
        $this -> SetFont('Arial', 'B', 7);
        $w = array(28, 80, 23, 22, 27, 27, 27, 81);
        $this -> setXY(15, 48);
        $this -> Cell($w[0], 10, 'FECHA ', 1, 1, 'C', 0);
        $this -> setXY(43, 48);
        $this -> Cell($w[1], 10, 'CONCEPTO', 1, 1, 'C', 0);
        $this -> setXY(123, 48);
        $this -> MultiCell($w[2], 5, utf8_decode('N° DE NOTA DE SALIDA'), 1, 'C', 0);
        $this -> setXY(146, 48);
        $this -> MultiCell($w[3], 10, utf8_decode('N° DE FACTURA'), 1, 'C', 0);
        $this -> setXY(168, 54);
        $this -> Cell($w[4], 4, 'TOTAL SERVICIO', 1, 1, 'C', 0);
        $this -> setXY(195, 54);
        $this -> Cell($w[5], 4, 'PAGO PARCIAL', 1, 1, 'C', 0);
        $this -> setXY(222, 54);
        $this -> Cell($w[6], 4, 'SALDO', 1, 1, 'C', 0);
        $this -> setXY(168, 48); 
        $this -> MultiCell($w[7], 6, 'IMPORTE', 1, 'C', 0);

    }

    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', '', 8);
        // linea de firmas;
        $this -> line(45, 175, 110, 175);
        $this -> setXY(62, 177);
        $this -> Cell(32, 4, 'Responsable Departamental o Regional', 0, 1, 'C', 0);
        $this -> line(157, 175, 220, 175);
        $this -> setXY(173, 177);
        $this -> Cell(32, 4, 'Enlace administrativo o Administrador', 0, 1, 'C', 0);
    }

}

$semillera = 'PRUEBA';
// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', 'letter');
$pdf -> AliasNbPages();
$pdf -> AddPage();

$pdf -> Output();
?>
