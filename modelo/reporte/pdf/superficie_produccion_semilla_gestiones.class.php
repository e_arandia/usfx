<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_pie.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_pie3d.php';
class PDF extends FPDF {
    //cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 20, 18, 55);
        $this -> Image($img2, 290, 20, 50);
        //linea de separacion de cabecera y contenido
        #$this -> line(10,25,269,25);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 10);
        $this -> setXY(15, 46);
        $this -> Cell(360, 15, 'S U P E R F I C I E S   D E   P R O D U C C I O N   D E   S E M I L L A   S E G U N   C U L T I V O S', 0, 1, 'C');
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de lInea
        $this -> Ln(2);

        //CABECERA DE TABLA
        $w = array(35, 25, 20, 28, 23, 21);
        $this -> setXY(110, 70);
        $this -> Cell($w[0], 16, utf8_decode('CULTIVO'), 1, 1, 'C', 0);
        $this -> setXY(145, 70);
        $this -> Cell(100, 8, 'SUPERFICIE (Ha.)', 1, 1, 'C', 0);
        $this -> setXY(145, 78);
        $this -> Cell($w[1], 8, 'INSCRITA', 1, 1, 'C', 0);
        $this -> setXY(170, 78);
        $this -> Cell(25, 8, 'RECHAZADA', 1, 1, 'C', 0);
        $this -> setXY(195, 78);
        $this -> Cell(25, 8, 'RETIRADA', 1, 1, 'C', 0);
        $this -> setXY(220, 78);
        $this -> Cell(25, 8, 'APROBADA', 1, 1, 'C', 0);
        $this -> setXY(245, 70);
        $this -> Cell(30, 16, 'PORCENTAJE %', 1, 1, 'C', 0);

    }

    // Tabla coloreada
    function datosSuperficie($posX, $posY, $ancho, $alto, $desde, $hasta, $lst_cultivo, $inscrita, $rechazada, $retirada, $aprobada, $porcentaje) {
        // Colores, ancho de linea y fuente en negrita
        $this -> SetDrawColor(0, 0, 0);
        #$this->SetLineWidth(.1);
        $this -> SetFont('ARIAL', '', 9);
        $tempX = $posX;
        $tempY = $posY;
        // Cabecera
        foreach ($lst_cultivo as $id => $cultivo) {
            $this -> SetXY($posX, $posY);
            $this -> Cell(($ancho + 10), $alto, $cultivo, 1, 1, 'C', 0);
            $posX += ($ancho + 10);
            $this -> SetXY($posX, $posY);
            $this -> Cell($ancho, $alto, $inscrita[$id], 1, 1, 'C', 0);
            $posX += $ancho;
            $this -> SetXY($posX, $posY);
            $this -> Cell($ancho, $alto, $rechazada[$id], 1, 1, 'C', 0);
            $posX += $ancho;
            $this -> SetXY($posX, $posY);
            $this -> Cell($ancho, $alto, $retirada[$id], 1, 1, 'C', 0);
            $posX += $ancho;
            $this -> SetXY($posX, $posY);
            $this -> Cell($ancho, $alto, $aprobada[$id], 1, 1, 'C', 0);
            $posX += $ancho;
            $this -> SetXY($posX, $posY);
            $this -> MultiCell(($ancho + 5), $alto, number_format($porcentaje[$id], 2), 1, 'C', FALSE);
            $posX = $tempX;
            $posY += $alto;
        }
        $posX = $tempX;
        $this -> SetFont('ARIAL', 'B', 8);
        $this -> SetXY($posX, $posY);
        $this -> Cell(($ancho + 10), $alto, 'Total', 1, 1, 'C', 0);
        $posX += ($ancho + 10);
        $this -> SetXY($posX, $posY);
        $this -> Cell($ancho, $alto, array_sum($inscrita), 1, 1, 'C', 0);
        $posX += $ancho;
        $this -> SetXY($posX, $posY);
        $this -> Cell($ancho, $alto, array_sum($rechazada), 1, 1, 'C', 0);
        $posX += $ancho;
        $this -> SetXY($posX, $posY);
        $this -> Cell($ancho, $alto, array_sum($retirada), 1, 1, 'C', 0);
        $posX += $ancho;
        $this -> SetXY($posX, $posY);
        $this -> Cell($ancho, $alto, array_sum($aprobada), 1, 1, 'C', 0);
        $posX += $ancho;
        $this -> SetXY($posX, $posY);
        $this -> Cell(($ancho + 5), $alto, number_format(array_sum($porcentaje), 2), 1, 1, 'C', 0);
        
        
        $this -> grafico($lst_cultivo, $aprobada, $desde, $hasta, $tempX, ($posY + 20));
    }

    private function grafico($lst_cultivos, $aprobada, $desde, $hasta, $x, $y) {

        $formato = new Funciones();
        // Some data
        $data = $aprobada;

        // Create the Pie Graph.
        $graph = new PieGraph(750, 290);
        $graph -> SetShadow();
        // Set A title for the plot
        /*
         $desde = $formato -> getInicioMes($desde);
         $hasta = $formato -> getFinMes($hasta);
         */
         
        $graph -> title -> Set('Superficie de produccion de semillas');
        $graph -> subtitle -> Set("DEL $desde AL $hasta ");
        $graph -> title -> SetFont(FF_FONT1, FS_BOLD);
        
        $graph -> legend -> SetPos(0.5, 0.999, "center" ,"center");
        $columnas = count($lst_cultivos);
        #echo $columnas;
        $graph -> legend -> SetColumns($columnas/2);
        // Create
        $p1 = new PiePlot3D($data);
        $p1->SetCenter(0.5,0.5);
        $p1->SetSize(0.40);        
        //$p1->ExplodeSlice((count($lst_cultivos)-1));
        #$p1 -> Explode(array(55,5,25));
        $p1->ExplodeAll(); 
        $p1 -> SetLegends($lst_cultivos);
        $p1->SetLabelType(PIE_VALUE_PER);
        
$labels = array();
foreach ($lst_cultivos as $id => $cultivo) {
	array_push($labels,$cultivo."\n(%.2f%%)");
}       
  
$p1->SetLabels($labels);
 
// This method adjust the position of the labels. This is given as fractions
// of the radius of the Pie. A value < 1 will put the center of the label
// inside the Pie and a value >= 1 will pout the center of the label outside the
// Pie. By default the label is positioned at 0.5, in the middle of each slice.
$p1->SetLabelPos(0.9);        
$p1->value->Show();
$p1->value->SetFont(FF_ARIAL,FS_NORMAL,9);
$p1->value->SetColor('darkgray');
        $graph -> Add($p1);
// Add and stroke 
        //nombre de archivo
        $file = date('mYHis') . ".png";
        // Display the graph
        $graph -> Stroke($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW.REPORT . '/' . $file);

        $grafico = URL . HOME_FOLDER . VIEW.REPORT. '/' . $file;
        // Logo
        $this -> Image($grafico, $x, $y, 180);
    }
    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        
        $posX_inicio = 15;
        $posX_final = $this->w;
        $posX_medio = ($posX_final)/2;
        $posX_cuarto1 = $posX_medio/2;
        $posX_cuarto2 = ($posX_medio+$posX_final)/2;
        $posX_octavo1 = ($posX_inicio+$posX_cuarto1)/2;
        $posX_octavo2 = ($posX_cuarto1+$posX_medio)/2;
        $posX_octavo3 = ($posX_medio+$posX_cuarto2)/2;
        $posX_octavo4 = ($posX_cuarto2+$posX_final)/2;
        
        //linea de firma administrador
        $this -> line(($posX_inicio+25), 250, ($posX_octavo2-30), 250);
        #line de firma responsable
        $this -> line(($posX_octavo3+30), 250, $posX_octavo4, 250);
        
        $this -> setXY(($posX_inicio+50), 245);
        $this -> Cell(15, 15, 'Responsable Departamental o Regional', 0, 1, 'C');
        $this -> setXY($posX_octavo3, 245);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY($posX_octavo3, 248);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }
}

$pdf = new PDF('L', 'mm', array(300, 390));

#$pdf -> SetFont('Arial', 'B', 8);
$pdf -> AddPage();
$pdf -> SetFont('Arial', '', 9);
$pdf -> setXY(15, 50);
$pdf -> Cell(360, 15, "DEL $desde  AL $hasta", 0, 1, 'C');
$pdf -> datosSuperficie(110, 86, 25, 8, $desde, $hasta, $lst_cultivos, $superficies['inscrita'], $superficies['rechazada'], $superficies['retirada'], $superficies['aprobada'], $superficies['porcentaje']);
$pdf -> Output();
?>
