<?php

class PDF  extends FPDF {
    var $angle = 0;
    //cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/descripcion.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoCH.jpg';
        // Logo
        $this -> Image($img1, 60, 15, 45);
        $this -> Image($img2, 18, 13, 40);
        // Salto de lInea
        $this -> Ln(2);        
        // Colors, line width and bold font
        $this -> SetFillColor(224, 235, 255);
        $this -> SetTextColor(0);
        $this -> SetDrawColor(0, 0, 0);
        $this -> SetLineWidth(.3);
        $this -> SetFont('Arial', 'B', 10);
        $this -> setXY(15, 34);
        $this -> Cell(325, 6, utf8_decode('RESUMEN DE FISCALIZACIÓN CAMPAÑA ' . date('Y')), 1, 1, 'C', TRUE);
        //cuadro de titulo de municipio
        $this -> setXY(15, 43);
        $this -> Cell(35, 20, '', 1, 1, 'C', TRUE);
        
        $this -> setXY(75, 43);
        $this -> Cell(265, 20, '', 1, 1, 'L', FALSE);

        $this -> setXY(15, 65);
        $this -> Cell(35, 5, 'COMUNIDADES', 1, 1, 'C', TRUE);
        
        $this -> setXY(15, 71.5);
        $this -> Cell(35, 5, 'PRODUCTORES', 1, 1, 'C', TRUE);        

        $this -> setXY(15, 78);
        $this -> Cell(35, 21, '', 1, 1, 'C', TRUE);//TITULO DE SEMILLERAS
        $this -> setXY(75, 78);
        $this -> Cell(265, 21, '', 1, 1, 'L', FALSE);//LISTA DE SEMILLERAS

        $this -> setXY(15, 101);
        $this -> Cell(35, 5, 'CULTIVOS', 1, 1, 'C', TRUE);
        $x = 75;//posicion en x
        $ancho = 26.5;//ancho de celda
        $i = 0;
        while ($i < 10) {
            $this -> setXY($x, 101);
            $this -> Cell($ancho, 5, '', 1, 1, 'L', FALSE);//cuadro para nombre de cultivo
            $x += $ancho;
            $i++;
        }

        $this -> setXY(15, 108);
        $this -> Cell(35, 25, '', 1, 1, 'C', TRUE);//titulo variedades
        $x = 75;//posicion en x
        $ancho = 26.5;//ancho de celda
        $i = 0;
        while ($i < 10) {
            $this -> setXY($x, 108);
            $this -> Cell($ancho, 25, '', 1, 1, 'L', FALSE);//detalle de  variedades de cada cultivo
            $x += $ancho;
            $i++;
        }

        $this -> setXY(15, 135);
        $this -> Cell(35, 20, '', 1, 1, 'C', TRUE);//categorias
        $x = 75;//posicion en x
        $ancho = 26.5;//ancho de celda
        $i = 0;
        while ($i < 10) {
            $this -> setXY($x, 135);
            $this -> Cell($ancho, 20, '', 1, 1, 'L', FALSE);//subtotal de categorias
            $x += $ancho;
            $i++;
        }

        $this -> setXY(15, 156);
        $this -> Cell(35, 5, 'SUPERFICIE(Has)', 1, 1, 'C', TRUE);        
        $x = 75;//posicion en x
        $ancho = 26.5;//ancho de celda
        $i = 0;
        while ($i < 10) {
            $this -> setXY($x, 156);
            $this -> Cell($ancho, 5, '', 1, 1, 'L', FALSE);//subtotal de superficie de cultivo
            $x += $ancho;
            $i++;
        }
        $this -> SetFont('Arial', 'B', 9);
        $this -> setXY(15, 162.5);
        $this -> MultiCell(35, 5.5, 'SUPERFICIE PRODUCCION (T.M.)', 1, 'C', TRUE);
        $x = 75;//posicion en x
        $ancho = 26.5;//ancho de celda
        $i = 0;
        while ($i < 10) {
            $this -> setXY($x, 162.5);
            $this -> Cell($ancho, 11, '', 1, 1, 'L', FALSE);//subtotal de volumen de produccion
            $x += $ancho;
            $i++;
        }
        $this -> SetFont('Arial', 'B', 10);
        $this -> setXY(15, 175);
        $this -> Cell(325, 6, utf8_decode('L    A    B    O    R    A    T    O    R    I    O'), 1, 1, 'C', TRUE);
        $this -> setXY(15, 183);
        $this -> Cell(86, 11, utf8_decode('M U E S T R A S   D E  S E M I L L E R A S'), 1, 1, 'C', TRUE);

        $this -> setXY(101, 183);
        $this -> Cell(45, 5, utf8_decode('APROBADAS'), 1, 1, 'C', FALSE);
        

        $this -> setXY(146, 183);
        $this -> Cell(45, 5, utf8_decode('RECHAZADAS'), 1, 1, 'C', FALSE);
        

        $this -> setXY(191, 183);
        $this -> Cell(45, 5, utf8_decode('SUBTOTAL'), 1, 1, 'C', FALSE);
        

        $this -> setXY(236, 183);
        $this -> Cell(45, 5, utf8_decode('FISCALIZADA'), 1, 1, 'C', FALSE);
        

        $this -> setXY(281, 183);
        $this -> Cell(45, 5, utf8_decode('TOTAL'), 1, 1, 'C', FALSE);
        
        $this->SetFont('Arial','',10);
        $this -> setXY(75, 65);
        $this -> Cell(265, 5, 'Comunidades de los diferentes municipios', 1, 1, 'L', FALSE);
        $this -> setXY(75, 71.5);
        $this -> Cell(265, 5, 'De todas las zonas y comunidades productoras de semilla', 1, 1, 'L', FALSE);
        
    }

    private function Rotate($angle, $x = -1, $y = -1) {
        if ($x == -1)
            $x = $this -> x;
        if ($y == -1)
            $y = $this -> y;
        if ($this -> angle != 0)
            $this -> _out('Q');
        $this -> angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this -> k;
            $cy = ($this -> h - $y) * $this -> k;
            $this -> _out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    function _endpage() {
        if ($this -> angle != 0) {
            $this -> angle = 0;
            $this -> _out('Q');
        }
        parent::_endpage();
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this -> Rotate($angle, $x, $y);
        $this -> Text($x, $y, $txt);
        $this -> Rotate(0);
    }

    function setTotales($solicitudes, $finalizadas, $pendientes, $municipios, $comunidades, $productores, $semilleras, $cultivos, $total_variedades, $total_categorias,$total_superficie,$total_volumen,$muestras) {
        
        $this -> SetFont('Arial', 'B', 11);
        $this -> setXY(50, 43);
        $this -> Cell(25, 20, count($municipios), 1, 1, 'C', FALSE);//total de municipios
        $this -> setXY(50, 65);
        $this -> Cell(25, 5, $comunidades, 1, 1, 'C', FALSE);//total de comunidades
        $this -> setXY(50, 71.5);
        $this -> Cell(25, 5, $productores, 1, 1, 'C', FALSE);//total de semilleristas
        $this -> setXY(50, 78);
        $this -> Cell(25, 21, count($semilleras), 1, 1, 'C', FALSE);//total de semilleras
        $this -> setXY(50, 101);
        $this -> Cell(25, 5, count($cultivos), 1, 1, 'C', FALSE);//total cultivos
        $this -> setXY(50, 108);
        $this -> Cell(25, 25, count($total_variedades), 1, 1, 'C', FALSE);//total variedades
        $this -> setXY(50, 135);
        $this -> Cell(25, 20, count($total_categorias), 1, 1, 'C', FALSE);//total de categorias
        $this -> setXY(50, 156);
        $this -> Cell(25, 5,array_sum($total_superficie),1, 1, 'C', FALSE);//total superficie cultivada
        $this -> setXY(50, 162.5);
        $this -> Cell(25, 11, array_sum($total_volumen), 1, 1, 'C', FALSE);//total volumen de produccion
        $this -> setXY(101, 188);
        $this -> Cell(45, 6, $muestras['aprobadas'], 1, 1, 'C', FALSE);//muestras aprobadas
        $this -> setXY(146, 188);
        $this -> Cell(45, 6, $muestras['rechazadas'], 1, 1, 'C', FALSE);//rechazadas
        $this -> setXY(191, 188);
        $this -> Cell(45, 6, ($muestras['aprobadas']+$muestras['rechazadas']), 1, 1, 'C', FALSE);//subtotal muestras aprobadas y rechazadas
        $this -> setXY(236, 188);
        $this -> Cell(45, 6, $muestras['fiscalizadas'], 1, 1, 'C', FALSE);//muestras fiscalizadas
        $this -> setXY(281, 188);
        $this -> Cell(45, 6, $muestras['total'], 1, 1, 'C', FALSE);//total muestras
    }

    
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setMunicipios($municipios,$posX,$posY,$ancho,$alto) {
        $this -> SetFont('Arial', '', 11);
        $contador = 1;
        foreach ($municipios as $key => $municipio) {            
            $this->SetXY($posX, $posY);
            $this->Cell($ancho, $alto, $municipio,0,1,'C',FALSE);
            if ($contador % 4 == 0) {
                $posX += $ancho;
                $contador = 1;
                $posY -= (3*$alto); 
            } else {
                $contador++;
                $posY += $alto;
            }
        }
    }
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setSemilleras($semilleras,$posX,$posY,$ancho,$alto) {
        $contador = 1;
        $this->SetFont('Arial','',11);
        #contador de semilleras por columna
        foreach ($semilleras as $key => $semillera) {
            $this->SetXY($posX, $posY);
            $temp_semillera = explode(" ",$semillera);
            $new_semillera = $this->abreviarNombre($semillera);
            $this->Cell($ancho, $alto, $new_semillera,0,1,'C',FALSE);
            if ($contador % 6 == 0) {
                $posX += $ancho;
                $contador = 1;
                $posY -= (5*$alto); 
            } else {
                $contador++;
                $posY += $alto;
            }
        } 
    }
    private function abreviarNombre($semillera){
        $lista_semilleras = explode(" ",$semillera);
        $total = count ($lista_semilleras);
        $new_semillera='';
        switch ($total) {
            case 1:
                $new_semillera = $lista_semilleras[0];
                break;            
            case 2:
                $new_semillera = implode(" ",$lista_semilleras);    
                break;
            case 3:
                if(strtolower($lista_semilleras[0]) == 'asociacion'){
                    $new_semillera = 'ASOC. '.$lista_semilleras[1]." ".$lista_semilleras[2];
                }elseif (strtolower($lista_semilleras[0]) == 'proyecto') {
                    $new_semillera = 'PROY. '.$lista_semilleras[1]." ".$lista_semilleras[2];
                }else{
                    $new_semillera = implode(" ",$lista_semilleras);
                }    
                break;
            default :
                $new_semillera = implode(" ",$lista_semilleras);
                break;    
                
        }
        return $new_semillera;
    }
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setCultivos($cultivos,$posX,$posY,$ancho,$alto) {
        $this->SetFont('Arial','',11);
        //lista de cultivos
        foreach ($cultivos as $key => $cultivo) {
            $this->SetXY($posX, $posY);
            $this->Cell($ancho, $alto, $cultivo,0,1,'C',FALSE);
            $posX += $ancho;
        }
    }
    /**
     * agregar lista de semilleras
     * @param  array     $cultivos     contiene todos los cultivos
     * @param  integer   $cantidad     total de semilleras
     * @param  integer   $posX         coordenada posicion X
     * @param  integer   $posY         coordenada posicion Y
     * @param  integer   $ancho        ancho de celda
     * @param  integer   $alto         alto de celda 
     * */
    function setVariedades($variedades,$total,$posX,$posY,$ancho,$alto,$desde,$hasta,$gestion) {
        #svar_dump($variedades,$total);
        $posY_temp = $posY;
        //listar todas las variedades de un cultivo
        $this->SetFont('Arial','',11);
        $contador = 1;
        foreach($variedades as $key => $variedad){
            $this->SetXY($posX, $posY);
            $this->Cell($ancho, $alto, $variedad, 0, 1, 'C', FALSE);
            /*
            while($contador <= $total[$key]){
                $posY += $alto;
                $contador++;
            }
            */
            $posX += $ancho;
            $posY = $posY_temp;                                                         
        }
    }
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setCategorias($categoria_producir,$total,$posX,$posY,$ancho,$alto) {
        $posY_temp = $posY;
        //listar todas las variedades de un cultivo 
        $this->SetFont('Arial','',11);
        $contador = 1;
        foreach($categoria_producir as $key => $categoria){
            $this->SetXY($posX, $posY);
            $this->Cell($ancho, $alto, $categoria, 0, 1, 'C', FALSE);            
            while($contador <= $total[$key]){                
                $posY += $alto;
                $contador++;
            }
            $posX += $ancho;
            $posY = $posY_temp;        
        }
    }
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setSuperficies($superficies,$posX,$posY,$ancho,$alto) {
        $this->SetFont("ARIAL","",11);        
        //lista de cultivos
        foreach ($superficies as $key => $superficie) {
                $this->SetXY($posX, $posY);
                $this->Cell($ancho, $alto, $superficie,0,1,'C',FALSE);
                $posX += $ancho;
        }
    }
    /**
     * agregar lista de semilleras
     * @param  array     $lista      contiene todos los municipios
     * @param  integer   $cantidad   total de semilleras
     * @param  integer   $posX       coordenada posicion X
     * @param  integer   $posY       coordenada posicion Y
     * @param  integer   $ancho      ancho de celda
     * @param  integer   $alto       alto de celda 
     * */
    function setVolumen($lst_volumenes,$posX,$posY,$ancho,$alto,$area) {
        $this->SetFont("ARIAL","",11);    
        foreach ($lst_volumenes as $key => $volumen) {
                $this->SetXY($posX, $posY);
                $this->Cell($ancho, $alto, $volumen, 0, 1,'C',FALSE);
                $posX += $ancho;
        }
    }
    /**
     * @param  mixed    $desde    fecha inicial
     * @param  mixed    $hasta    fecha final
     * @param  object   $dates    instancia de clase funciones
     * 
     * retorna la fecha inicial y final segun el tipo de mes inicial
     * si es trimestral o un rango
     * */
    function setDetalleRango($desde,$hasta,$dates){
        if ( in_array($desde, array("01", "04", "07", "10")) ){
            $desdeXLS = $dates->getInicioMes($desde);
            $hastaXLS = $dates->getFinMes(($desde+2));
        }else{
            $desdeXLS = $dates->cambiar_formato_fecha($desde);
            $hastaXLS = $dates->cambiar_formato_fecha($hasta);
        }
        $this->setXY(300,18);
        $this -> Cell(25, 4,utf8_decode('DEL : ' . $desdeXLS), 0, 1, 'C', FALSE);
        $this-> setXY(301, 22);
        $this -> Cell(25, 4,utf8_decode('AL : ' . $hastaXLS), 0, 1, 'C', FALSE);    
    }

}

$pdf = new PDF('L', 'mm', "legal");
$pdf -> SetFont('Arial', 'B', 8);
$pdf -> AddPage();
$pdf -> setDetalleRango($desde, $hasta,$dates);
$pdf -> RotatedText(20, 60, 'M U N I C I P I O S', 30);
$pdf -> RotatedText(22, 96, 'S E M I L L E R A S', 32);
$pdf -> RotatedText(22, 126, 'V A R I E D A D E S', 30);
$pdf -> RotatedText(20, 151, 'C A T E G O R I A S', 28);
$pdf -> setMunicipios($lst_municipios, 70, 44, 33, 4.5);
$pdf -> setSemilleras($lst_semilleras, 75, 79, 33, 4);
$pdf -> setCultivos($lst_cultivos, 75, 101, 26.5, 4.5);
$pdf -> setVariedades($lst_variedades,$lst_total_variedades,75,108,26.5,5,$desde,$hasta,$gestion);
$pdf -> setCategorias($lst_categorias,$lst_total_categorias,75,137,26.5,2.2);
$pdf -> setSuperficies($lst_superficies,75,154.3,26.5,10);
$pdf -> setVolumen($lst_volumen,75,163,26.5,10,$area);
$pdf -> setTotales($total_solicitudes, $total_finalizadas, $total_pendientes, $lst_municipios, $totalComunidades, $totalProductores, $lst_semilleras, $lst_cultivos, $lst_variedades, $lst_total_categorias,$lst_superficies,$lst_volumen,$muestras);
$pdf -> Output();
?>
