<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . LIBS . '/jpgraph/jpgraph_pie.php';
class PDF extends FPDF {
    //cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 20, 18, 55);
        $this -> Image($img2, 290, 20, 50);
        //linea de separacion de cabecera y contenido
        #$this -> line(10,25,269,25);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 10);
        $this -> setXY(15, 46);
        $this -> Cell(360, 15, utf8_decode('VOLUMENES DE PRODUCCION SEGUN CULTIVOS Y CATEGORIAS (en toneladas métricas)'), 0, 1, 'C');
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de lInea
        $this -> Ln(2);

        //CABECERA DE TABLA
        $w = array(35, 25, 20, 28, 23, 21);
        $this -> setXY(50, 70);
        $this -> Cell($w[0], 16, utf8_decode('CULTIVO'), 1, 1, 'C', 0);
        $this -> setXY(85, 70);
        $this -> Cell($w[0], 16, utf8_decode('VARIEDAD'), 1, 1, 'C', 0);
        $this -> setXY(120, 70);
        $this -> Cell(150, 8, 'CATEGORIAS (T.M.)', 1, 1, 'C', 0);
        $this -> setXY(120, 78);
        $this -> Cell(30, 8, 'BASICA', 1, 1, 'C', 0);
        $this -> setXY(150, 78);
        $this -> Cell(30, 8, 'REGISTRADA', 1, 1, 'C', 0);
        $this -> setXY(180, 78);
        $this -> Cell(30, 8, 'CERTIFICADA', 1, 1, 'C', 0);
        $this -> setXY(210, 78);
        $this -> Cell(30, 8, 'CERTIFICADA B', 1, 1, 'C', 0);
        $this -> setXY(240, 78);
        $this -> Cell(30, 8, 'FISCALIZADA', 1, 1, 'C', 0);
        $this -> setXY(270, 70);
        $this -> Cell(25, 16, 'TOTAL', 1, 1, 'C', 0);
        /*
        $this -> setXY(295, 70);
        $this -> Cell(30, 16, 'PORCENTAJE %', 1, 1, 'C', 0);
        */

    }

    // Tabla coloreada
    function volumenProduccion($posX_sbtc,$posY_sbtc,$posX_sbt,$posY_sbt,$posX_cul,$posY_cul,$ancho_cul, $alto_var, $alto_cul, $ancho_var, $posX_var, $posY_var,$posX_cat,$posY_cat,$ancho_cat,$alto_cat,$lst_cultivos,$lst_variedades_cultivo,$lst_variedades,$lst_categorias,$lst_volumen_produccion_categorias) {
        $total_basica = $total_registrada = $total_certificada = 0;
        $total_certificadaB = $total_fiscalizada = 0;    
        $volumen['porcentaje'] = array();
        foreach ($lst_cultivos as $i => $cultivo_name) {
            $this->SetXY($posX_cul,$posY_cul);
            $this->Cell($ancho_cul, $alto_cul, $cultivo_name, 1, 1, 'C', 0); //nombre del cultivo
            $total_variedades = $lst_variedades_cultivo[$i];
            while ($total_variedades <= $lst_variedades_cultivo[$i]) {
                $this->SetXY($posX_sbt,$posY_sbt);
                $this->SetFont('ARIAL','B',9);
                $this->Cell($ancho_var, $alto_var, "SUB TOTAL", 1, 1, 'R', 0);
                foreach ($lst_variedades as $j => $variedad_name) {                    
                    //texto en negrita para subtotales
                    $this->SetXY($posX_var,$posY_var);
                    $this->SetFont('ARIAL','',9);
                    $this->Cell($ancho_var, $alto_var, $variedad_name,1,1,FALSE);
                    //nombre de la variedad
                    foreach ($lst_categorias as $k => $categoria_name) {
                        $subtotal_variedad = 0;
                        $this->SetFont('ARIAL','');    
                        $this->SetXY($posX_cat,($posY_cat+$alto_cat));
                        //escribir subtotal de categoria
                        $this->Cell($ancho_cat,$alto_cat, $lst_volumen_produccion_categorias[$k],1,1,'C',FALSE);
                        //sumar subtotal de variedad
                        $subtotal_variedad += $lst_volumen_produccion_categorias[$k];
                        
                        //sumar cada cantidad de volumenes totales
                        $contar_categoria = 1;                       
                        $basica = $registrada = $certificada = 0;
                        $certificadaB = $fiscalizada = 0; 
                        $total_categorias = (count($lst_volumen_produccion_categorias)-5);
                        if ($contar_categoria < $total_categorias){
                            $basica = 0;
                            $registrada = 0;
                            $certificada = 0;
                            $certificadaB = 0;
                            $fiscalizada = 0;
                        }else{
                            while ($contar_categoria <= $total_categorias){
                            $basica += $lst_volumen_produccion_categorias[( $k+5 )];
                            $registrada += $lst_volumen_produccion_categorias[( ($k+1)+5 )];
                            $certificada += $lst_volumen_produccion_categorias[( ($k+2)+5 )];
                            $certificadaB += $lst_volumen_produccion_categorias[( ($k+3)+5 )];
                            $fiscalizada +=$lst_volumen_produccion_categorias[( ($k+4)+5 )];
                            $contar_categoria++;                            
                        }
                        }
                        $total_subtotales = $basica+$registrada+$certificada+$certificadaB+$fiscalizada;
                        //escribir subtotales en la hoja
                        $this->SetFont('ARIAL','B');
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell($ancho_cat,$alto_cat, $basica,1,1,'C',FALSE);
                        $total_basica += $basica;
                        $posX_sbtc += $ancho_cat;
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell($ancho_cat,$alto_cat, $registrada,1,1,'C',FALSE);
                        $total_registrada += $registrada;
                        $posX_sbtc += $ancho_cat;
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell($ancho_cat,$alto_cat, $certificada,1,1,'C',FALSE);
                        $total_certificada += $certificada;
                        $posX_sbtc += $ancho_cat;
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell($ancho_cat,$alto_cat, $certificadaB,1,1,'C',FALSE);
                        $total_certificadaB += $certificadaB;
                        $posX_sbtc += $ancho_cat;
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell($ancho_cat,$alto_cat, $fiscalizada,1,1,'C',FALSE);
                        $posX_sbtc += $ancho_cat;
                        $this->SetXY($posX_sbtc,$posY_sbtc);
                        $this->Cell(25,$alto_cat, $total_subtotales,1,1,'C',FALSE);
                        
                        
                        $total_fiscalizada += $fiscalizada;
                        $posY_sbtc = ($alto_var*$total_variedades); 
                        $posX_sbtc -= ($posX_sbtc*4);
                        
                        
                        //cantidad en cada categoria
                        if (($k+1) % 5 == 0) {                            
                            break;
                        }
                        $posX_cat += $ancho_cat;      
                    }
                    $this->setFont('ARIAL','');
                    //escribir subtotal de variedad
                    $this->SetXY(($posX_cat+30),($posY_cat+$alto_cat));
                    $this->Cell(($ancho_cat-5),$alto_cat, $subtotal_variedad,1,1,'C',FALSE); 
                    //sumar subtotales
                    $total_subtotales = $subtotal_variedad;
                    //reiniciar valores para la proxima fila
                    $posY_cat *= $total_variedades;
                    $posX_var += $alto_var;
                    $posY_var += $alto_var;                    
                }
                $total_variedades++;
            }
            $posY_cul += (($total_variedades+1)*$alto_var); 
        }
        // combinacion de celdas para el texto total
        $this->SetFont('ARIAL','B',9);
        $this->SetXY($posX_cul, ($posY_cul-4));
        $this->Cell(($ancho_cul+$ancho_var),$alto_cul,'TOTAL',1,1,'R',FALSE);
        //escribir suma de los subtotales
        $posX_total = $posX_cul+($ancho_cul+$ancho_var);
        $posY_total = ($posY_cul-4); 
        $this->SetXY($posX_total,$posY_total);
        $this->Cell($ancho_cat,$alto_cul,$total_basica,1,1,'C',FALSE);
        $posX_total += $ancho_cat;
        $this->SetXY($posX_total,$posY_total);
        $this->Cell($ancho_cat,$alto_cul,$total_registrada,1,1,'C',FALSE);
        $posX_total += $ancho_cat;
        $this->SetXY($posX_total,$posY_total);
        $this->Cell($ancho_cat,$alto_cul,$total_certificada,1,1,'C',FALSE);
        $posX_total += $ancho_cat;
        $this->SetXY($posX_total,$posY_total);
        $this->Cell($ancho_cat,$alto_cul,$total_certificadaB,1,1,'C',FALSE);
        $posX_total += $ancho_cat;
        $this->SetXY($posX_total,$posY_total);
        $this->Cell($ancho_cat,$alto_cul,$total_fiscalizada,1,1,'C',FALSE);  
        $posX_total += $ancho_cat;
        $this->SetXY($posX_total,$posY_total);
        $this->Cell(($ancho_cat-5),$alto_cul,0,1,1,'C',FALSE); 
        
        //graficar subtotales de los cultivos
        $tempX = $posX_var;
        #$this -> grafico($lst_cultivos, 0, $tempX, ($posY_var + 20));       
    }

    private function grafico($lst_cultivos, $subtotales, $x, $y) {

        $formato = new Funciones();
        // Some data
        $data = $subtotales;

        // Create the Pie Graph.
        $graph = new PieGraph(350, 290);
        $graph -> SetShadow();

        // Set A title for the plot

        $graph -> title -> Set('Volumen de Semilla Certificada por Cultivo');
        $graph -> title -> SetFont(FF_FONT1, FS_BOLD);

        $graph -> legend -> SetPos(0.5, 0.9, 'center', 'bottom');
        $graph -> legend -> SetColumns(3);
        // Create
        $p1 = new PiePlot($data);
        $p1 -> SetLegends($lst_cultivos);
        $graph -> Add($p1);

        //nombre de archivo
        $file = date('mYHis') . ".png";
        // Display the graph

        $graph -> Stroke($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . REPORT . '/' . $file);

        $grafico = URL . HOME_FOLDER . VIEW . REPORT . '/' . $file;
        // Logo
        $this -> Image($grafico, $x, $y, 180);

    }
// Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5

        #nombre de usuario y fecha de impresion
        $posX_final=$this->w;
        $this -> SetFont('Arial', 'B',9);
        $this->setXY(($posX_final-70),18);
        $this -> Cell(0, 15, utf8_decode('USUARIO : '.$_SESSION['usr_login']), 0, 1);
        $this->setXY(($posX_final-70),22);
        $this -> Cell(0, 15, utf8_decode('FECHA : '.date('d - m - Y')), 0, 1);
        
        $posX_inicio = 15;
        $posX_final = $this->w;
        $posX_medio = ($posX_final)/2;
        $posX_cuarto1 = $posX_medio/2;
        $posX_cuarto2 = ($posX_medio+$posX_final)/2;
        $posX_octavo1 = ($posX_inicio+$posX_cuarto1)/2;
        $posX_octavo2 = ($posX_cuarto1+$posX_medio)/2;
        $posX_octavo3 = ($posX_medio+$posX_cuarto2)/2;
        $posX_octavo4 = ($posX_cuarto2+$posX_final)/2;
        $posY_inicio = 15;
        $posY_final = $this->h;
        $posY_medio = ($posY_inicio+$posY_final)/2;
        $posY_cuarto2 = ($posY_medio+$posY_final)/2;
        $posY_octavo4 = ($posY_cuarto2+$posY_final)/2;
        
        //linea de firma administrador
        $this -> line(($posX_inicio+25), $posY_octavo4, ($posX_octavo2-30), $posY_octavo4);
        #line de firma responsable
        $this -> line(($posX_octavo3+30), $posY_octavo4, $posX_octavo4, $posY_octavo4);
        $this -> SetFont('Arial', 'I', 10);
        $this -> setXY(($posX_inicio+56), $posY_octavo4-4);
        $this -> Cell(15, 15, 'Responsable Departamental o Regional', 0, 1, 'C');
        $this -> setXY($posX_octavo3, $posY_octavo4-4);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY($posX_octavo3, $posY_octavo4);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }
}

$pdf = new PDF('L', 'mm', array(300, 390));

#$pdf -> SetFont('Arial', 'B', 8);
$pdf -> AddPage();
$pdf -> SetFont('Arial', '', 9);
$pdf -> setXY(15, 50);
$pdf -> Cell(360, 15, "DEL $desde  AL $hasta", 0, 1, 'C');
$pdf -> volumenProduccion(120,86,85,86,50, 86, 35, 4, (4*2), 35, 85, 90, 120, 86, 30, 4, $lst_cultivos, $lst_variedades_cultivo, $lst_variedades, $lst_categorias, $lst_volumen_produccion_categorias);
$pdf -> Output();
?>
