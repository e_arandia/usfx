<?php

class PDF  extends FPDF {
    //cabecera de pagina
    function Header() {
        //imagen 2
        $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg';
        // Logo
        $this -> Image($img2, 10, 8, 33);
        #nombre de usuario y fecha de impresion
        $posX_final=$this->w;
        $this -> SetFont('Arial', 'B', 8);
        $this->setXY(($posX_final-70),18);
        $this -> Cell(0, 15, utf8_decode('USUARIO : '.$_SESSION['usr_login']), 0, 1);
        $this->setXY(($posX_final-70),22);
        $this -> Cell(0, 15, utf8_decode('IMPRESO : '.date('d - m - Y')), 0, 1);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 6);
        $this -> setXY(15, 18);
        $this -> Cell(0, 15, utf8_decode('Certificación y Fiscalización'), 0, 1);
        $this -> setXY(10, 25);
        $this -> Cell(0, 15, 'DETALLE CERTIFICACION, PRODUCCION Y COSTOS ' . date('Y'), 0, 1);
        
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de lInea
        $this -> Ln(2);

        //CABECERA DE TABLA
        $w = array(10, 15, 25, 28, 23, 21, 20, 20, 13, 17, 18, 25, 15, 15, 15, 16, 12, 58);
        $this -> setXY(11, 46);
        $this -> Cell($w[0], 8, utf8_decode('N°'), 1, 1, 'C', 0);
        $this -> setXY(21, 46);
        $this -> Cell($w[1], 8, 'FECHA', 1, 1, 'C', 0);
        $this -> setXY(36, 46);
        $this -> Cell($w[2], 8, 'SEMILLERA', 1, 1, 'C', 0);
        $this -> setXY(61, 46);
        $this -> Cell($w[2], 8, 'COMUNIDAD', 1, 1, 'C', 0);
        $this -> setXY(86, 46);
        $this -> MultiCell($w[3], 4, 'RESPONSABLE / SEMILLERISTA', 1, 'C', 0);
        $this -> setXY(114, 46);
        $this -> Cell($w[4], 8, 'CULTIVO', 1, 1, 'C', 0);
        $this -> setXY(137, 46);
        $this -> Cell($w[5], 8, 'VARIEDAD', 1, 1, 'C', 0);
        $this -> setXY(158, 46);
        $this -> MultiCell($w[6], 4, 'CATEGORIA SEMBRADA', 1, 'C', 0);
        $this -> setXY(178, 46);
        $this -> MultiCell($w[7], 4, 'SUPERFICIE INSCRITA Has.', 1, 'C', 0);
        $this -> setXY(198, 46);
        $this -> MultiCell($w[8], 4, utf8_decode('NRO CAMPO'), 1, 'C', 0);
        $this -> setXY(211, 46);
        $this -> MultiCell($w[9], 4, 'APROBADO Has.', 1, 'C', 0);
        $this -> setXY(228, 46);
        $this -> MultiCell($w[10], 4, 'RECHAZADO Has.', 1, 'C', 0);
        $this -> setXY(246, 46);
        $this -> MultiCell($w[11], 4, 'CATEGORIA APROBADO', 1, 'C', 0);
        $this -> setXY(271, 46);
        $this -> MultiCell($w[12], 4, utf8_decode('NÚMERO BOLSAS'), 1, 'C', 0);
        $this -> setXY(286, 46);
        $this -> MultiCell($w[13], 4, 'INSCRIP. Bs.', 1, 'C', 0);
        $this -> setXY(301, 46);
        $this -> MultiCell($w[14], 4, 'CAMPO    Bs.', 1, 'C', 0);
        $this -> setXY(316, 46);
        $this -> MultiCell($w[15], 4, 'ETIQUETAS Bs.', 1, 'C', 0);
        $this -> setXY(332, 46);
        $this -> MultiCell($w[16], 4, 'TOTAL Bs.', 1, 'C', 0);
        $this -> setXY(286, 42);
        $this -> Cell($w[17], 4, utf8_decode('COSTO POR CERTIFICACIÓN'), 1, 1, 'C', 0);
    }

    // Tabla coloreada
    function datos($cultivo,$desde,$hasta,$semillera,$fechas,$comunidades,$semilleristas,$cultivos,$variedades,$cSembradas,$superficies,$nocampos,$aprobadas,$rechazadas,$cAprobadas,$nobolsas,$inscripciones,$campos,$etiquetas,$totales) {
        $total_semilleras = 1;
        //total de semilleristas de una semillera y un cultivo en un rago de fecha
        $total_semilleristas = array();
        for ($i=0; $i < $total_semilleras ; $i++) { 
            $this -> SetFont('Arial', '', 8);
            $this -> AddPage(); 
            Reportes::getCantidadByCultivo($semillera,$cultivo,$desde,$hasta);
            while($obj = DBConnector::objeto()){
                array_push($total_semilleristas,$obj->cantidad);
            }    
        }
        $aux = 0;
        $dates = new Funciones();
        if ( in_array($desde, array("01", "04", "07", "10")) ){
            $desde = $dates->getInicioMes($desde);
            $hasta = $dates->getFinMes(($desde+2));
        }else{
            $desde = $dates->cambiar_formato_fecha($desde);
            $hasta = $dates->cambiar_formato_fecha($hasta);
        }
        for ($i=0; $i <$total_semilleras ; $i++) {
            $contador_semilleristas = 0;
            $nro_total_semilleristas = $total_semilleristas[$i];
            $this -> SetFont('Arial', 'B', 5);
            
            $this -> setXY(21, 28);                
            $this->Cell(0,15,"DESDE :   $desde  HASTA :   $hasta ",0,1,'L');
            $this -> SetFont('Arial', 'B', 8);
            $this -> setXY(10, 35);
            $this -> Cell(0, 15, 'SEMILLERA : ' . $semillera, 0, 1);
            $total_superficie = $total_aprobada = $total_rechazada = $total_bolsas = 0;  
            $total_inscripcion = $total_campo = $total_etiqueta = $total_total = 0;
            $w = array(  20, 20, 13, 17, 18, 25, 15, 15, 15, 16, 12, 58);
            $h = array(  8,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4);
            $posX = 11; 
            $posY = 54;
            
            while ($contador_semilleristas < $nro_total_semilleristas) {
                $this -> SetFont('Arial', '', 7);
                $this->SetXY($posX, $posY);
                $this->Cell(10,8,($contador_semilleristas+1),1,1,'C',FALSE);
                $posX += 10;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$fechas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 15;
                $this->SetXY($posX, $posY);
                $temp_semillera = explode("PARTICULAR - ",$semillera);
                $temp_semillera2 = explode("/",$temp_semillera[0]);
                $this->Cell(25,8,$temp_semillera2[0],1,1,'C',FALSE);
                $posX += 25;
                $this->SetXY($posX, $posY);
                $this->Cell(25,8,$comunidades[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 25;
                $this->SetXY($posX, $posY);
                $this -> MultiCell(28, 8, utf8_decode($semilleristas[$contador_semilleristas]), 1, 'C', 0);
                $posX += 28;
                $this->SetXY($posX, $posY);
                $this->Cell(23,8,utf8_decode($cultivos[$contador_semilleristas]),1,1,'C',FALSE);
                $posX += 23;
                $this->SetXY($posX, $posY);
                $this->Cell(21,8,utf8_decode($variedades[$contador_semilleristas]),1,1,'C',FALSE);                
                $posX += 21;
                $this->SetXY($posX, $posY);
                $this->Cell(20,8,$cSembradas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 20;
                $this->SetXY($posX, $posY);
                $this->Cell(20,8,number_format($superficies[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_superficie += $superficies[$contador_semilleristas];
                $posX += 20;
                $this->SetXY($posX, $posY);
                $this->Cell(13,8,$nocampos[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 13;
                $this->SetXY($posX, $posY);
                $this->Cell(17,8,number_format($aprobadas[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_aprobada += $aprobadas[$contador_semilleristas];
                $posX += 17;
                $this->SetXY($posX, $posY);
                $this->Cell(18,8,number_format($rechazadas[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_rechazada += $rechazadas[$contador_semilleristas];
                $posX += 18;
                $this->SetXY($posX, $posY);
                $this->Cell(25,8,$cAprobadas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 25;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$nobolsas[$contador_semilleristas],1,1,'C',FALSE);
                $total_bolsas += $nobolsas[$contador_semilleristas];
                $posX += 15;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$inscripciones[$contador_semilleristas],1,1,'C',FALSE);
                $total_inscripcion += $inscripciones[$contador_semilleristas];
                $posX +=15;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,number_format($campos[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_campo += $campos[$contador_semilleristas];
                $posX +=15;
                $this->SetXY($posX, $posY);
                $this->Cell(16,8,$etiquetas[$contador_semilleristas],1,1,'C',FALSE);
                $total_etiqueta += $etiquetas[$contador_semilleristas];
                $posX +=16;
                $this->SetXY($posX, $posY);
                $this->Cell(12,8,number_format($campos[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_total += $totales[$contador_semilleristas];
                
                $posX = 11;
                $posY += 8;
                $contador_semilleristas++;
            }
            
            $this -> SetFont('Arial', 'B', 7);
            $this->SetXY($posX, $posY);
            $this->Cell(167,4,'T O T A L',1,1,'C',FALSE);  
            $posX += 167;
            $this->SetXY($posX,$posY);
            $this->Cell(20,4,$total_superficie,1,1,'C',FALSE);
            $posX += 20;
            $this->SetXY($posX,$posY);
            $this->Cell(13,4,'',1,1,'C',FALSE); //total  nro de campo
            $posX += 13;
            $this->SetXY($posX,$posY);
            $this->Cell(17,4,$total_aprobada,1,1,'C',FALSE);
            $posX += 17;
            $this->SetXY($posX,$posY);
            $this->Cell(18,4,$total_rechazada,1,1,'C',FALSE);
            $posX += 18;
            $this->SetXY($posX,$posY);
            $this->Cell(25,4,'',1,1,'C',FALSE); //total  categoria aprobada
            $posX += 25;
            $this->SetXY($posX,$posY);
            $this->Cell(15,4,$total_bolsas,1,1,'C',FALSE); //total  nro bolsas
            $posX += 15;
            $this->SetXY($posX,$posY);
            $this->Cell(15,4,$total_inscripcion,1,1,'C',FALSE); //total inscripcion  
            $posX += 15;
            $this->SetXY($posX,$posY);
            $this->Cell(15,4,number_format($total_campo,2),1,1,'C',FALSE); //total campo  
            $posX += 15;
            $this->SetXY($posX,$posY);
            $this->Cell(16,4,$total_etiqueta,1,1,'C',FALSE); //total etiquetas  
            $posX += 16;
            $this->SetXY($posX,$posY);
            $this->Cell(12,4,number_format($total_total,2),1,1,'C',FALSE); //total total              
            $aux += $nro_total_semilleristas;  
            $posX = 11;
            $posY = 54;
            $ancho = $w[0];
            $alto = $h[0];
        }       
    }
// Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 8);
        
        $posX_inicio = 15;
        $posX_final = $this->w;
        $posX_medio = ($posX_final)/2;
        $posX_cuarto1 = $posX_medio/2;
        $posX_cuarto2 = ($posX_medio+$posX_final)/2;
        $posX_octavo1 = ($posX_inicio+$posX_cuarto1)/2;
        $posX_octavo2 = ($posX_cuarto1+$posX_medio)/2;
        $posX_octavo3 = ($posX_medio+$posX_cuarto2)/2;
        $posX_octavo4 = ($posX_cuarto2+$posX_final)/2;
        
        //linea de firma administrador
        $this -> line(($posX_inicio+25), 130, ($posX_octavo2-30), 130);
        #line de firma responsable
        $this -> line(($posX_octavo3+30), 130, $posX_octavo4, 130);
        
        $this -> setXY(($posX_inicio+50), 125);
        $this -> Cell(15, 15, 'Responsable Departamental o Regional', 0, 1, 'C');
        $this -> setXY($posX_octavo3, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY($posX_octavo3, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }
}

$pdf = new PDF('L', 'mm', "legal");

foreach ($lst_semillera as $key => $semillera) {
    $pdf -> datos($cultivo,$desde,$hasta,$semillera,$lst_fecha,$lst_comunidad,$lst_semillerista,$lst_cultivo,$lst_variedad,$lst_cat_sembrada,$lst_superficie,$lst_nocampo,$lst_aprobada,$lst_rechazada,$lst_cat_aprobada,$lst_nobolsa,$lst_inscripcion,$lst_campo,$lst_etiqueta,$lst_total);
}
$pdf -> Output();
?>
