<?php
class PDF extends FPDF {
    // Cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 140, 140, 140);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Times', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Datos de Muestra para laboratorio', 0, 1, 'C');
    }

    //titulos
    function titulos($posX, $posY,$ancho,$alto) {
        $anchoPagina = $this->w;
        $tempPosX = $posX;
        $tempPosY = $posY;
        $tempAncho = $ancho;
        $tempAlto = $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, utf8_decode('N° análisis: '), 0, 1);
        $this -> setXY(($anchoPagina/2), $posY);
        $this -> Cell($ancho, $alto,'Fecha recepcion: ', 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, 'Cultivo :', 0, 1);
        $posY += $alto;
        $this -> setXY( $posX, $posY);
        $this -> Cell($ancho, $alto, utf8_decode('N° campo: '), 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, 'Origen: ', 0, 1);
        $posX += $alto; 
        $this -> setXY(($posX-8), ($posY+8));
        $this -> Cell($ancho, $alto, 'Fecha resultado: ', 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-8), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Lote: ', 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-8), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Pureza: ', 0, 1);
        #$posY += $alto;
        $this -> setXY(($posX+$ancho), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Germinacion: ', 0, 1);
        #$posY += $alto;
        $this -> setXY(($posX+(2*$ancho)), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Humedad: ', 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-8), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Calibre: ', 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-8), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'kg/Bolsa: ', 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-8), ($posY+$alto));
        $this -> Cell($ancho, $alto, 'Observacion: ', 0, 1);
    }

    //contenido
    function contenido(array $resultadoPDF,$posX, $posY,$ancho,$alto) {
        $anchoPagina = $this->w;
        //contenido
        $this -> SetFont('Times', '', 12);
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['nro_analisis'], 0, 1);
        $this -> setXY(($anchoPagina/2)+$ancho, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['fecha_recepcion'], 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['cultivo'], 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['nro_campo'], 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-6), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['origen'], 0, 1);
        $posY += $alto;
        $this -> setXY(($posX+$alto), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['fecha_resultado'], 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-$alto), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['lote'], 0, 1);
        $posY += $alto;
        $this -> setXY(($posX-$alto+3), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['pureza']." %", 0, 1);
        #$posY += $alto;
        $this -> setXY(($posX+(2.2*$ancho)), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['germinacion']." %", 0, 1);
        #$posY += $alto;
        $this -> setXY(($posX+(3.9*$ancho)), $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['humedad']." %", 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['calibre'], 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['kgBolsa'], 0, 1);
        $posY += $alto;
        $this -> setXY($posX, $posY);
        $this -> Cell($ancho, $alto, $resultadoPDF['observacion'], 0, 1);
    }

    function firmaResponsable() {
        //responsable
        $this -> setXY(27, 145);
        $this -> Cell(0, 15, 'Responsable ' . $_SESSION['usr_area'], 0, 1, 'C');
        $this -> setXY(27, 150);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

    /// Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 8
        $this -> SetFont('Arial', 'I', 8);
        // Numero de pagina
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C')
        $this -> setXY(27, 135);
        $this -> Cell(0, 15, 'Responsable ' . $_SESSION['usr_area'], 0, 1, 'C');
        $this -> setXY(27, 140);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(215, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();

$pdf -> tituloPagina();
$pdf -> titulos(18, 40,60,8);
$pdf -> contenido($resultadoPDF,40, 40,33,8);


$pdf -> Output();
?>
