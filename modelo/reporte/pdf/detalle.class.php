<?php

class PDF  extends FPDF {
    //cabecera de pagina
    function Header() {
        //imagen 2
        $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg';
        // Logo
        $this -> Image($img2, 10, 8, 33);
        #nombre de usuario y fecha de impresion
        $posX_final=$this->w;
        $this -> SetFont('Arial', 'B', 8);
        $this->setXY(($posX_final-70),18);
        $this -> Cell(0, 15, utf8_decode('USUARIO : '.$_SESSION['usr_login']), 0, 1);
        $this->setXY(($posX_final-70),22);
        $this -> Cell(0, 15, utf8_decode('IMPRESO : '.date('d - m - Y')), 0, 1);
        //linea de separacion de cabecera y contenido
        #$this -> line(10,25,269,25);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 10);
        $this -> setXY(15, 18);
        $this -> Cell(0, 15, utf8_decode('Certificación y Fiscalización'), 0, 1);
        $this -> setXY(10, 25);
        $this -> Cell(0, 15, 'DETALLE CERTIFICACION, PRODUCCION Y COSTOS ' . date('Y'), 0, 1);
        
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de linea
        $this -> Ln(2);

        //CABECERA DE TABLA
        $w = array(8, 20, 25, 34, 23, 21, 20, 26, 13, 19, 22, 25, 15, 15, 15, 19, 12, 61);
        $this -> setXY(23, 56);
        $this -> Cell($w[0], 8, utf8_decode('N°'), 1, 1, 'C', 0);
        $this -> setXY(31, 56);
        $this -> Cell($w[1], 8, 'FECHA', 1, 1, 'C', 0);/*
        $this -> setXY(36, 56);
        $this -> Cell($w[2], 8, 'SEMILLERA', 1, 1, 'C', 0);*/
        $this -> setXY(51, 56);
        $this -> Cell($w[2], 8, 'COMUNIDAD', 1, 1, 'C', 0);
        $this -> setXY(76, 56);
        $this -> MultiCell($w[3], 8, 'SEMILLERISTA', 1, 'C', 0);/*
        $this -> setXY(89, 56);
        $this -> Cell($w[4], 8, 'CULTIVO', 1, 1, 'C', 0);*/
        $this -> setXY(110, 56);
        $this -> Cell($w[5], 8, 'VARIEDAD', 1, 1, 'C', 0);
        $this -> SetFont('Arial', 'B', 8);
        $this -> setXY(131, 56);
        $this -> MultiCell($w[6], 4, 'CATEGORIA SEMBRADA', 1, 'C', 0);
        $this -> setXY(151, 56);
        $this -> MultiCell($w[7], 4, 'SUPERFICIE INSCRITA Has.', 1, 'C', 0);
        $this -> setXY(177, 56);
        $this -> MultiCell($w[8], 4, utf8_decode('NRO CAMPO'), 1, 'C', 0);
        $this -> setXY(190, 56);
        $this -> MultiCell($w[9], 4, 'APROBADO Has.', 1, 'C', 0);
        $this -> setXY(209, 56);
        $this -> MultiCell($w[10], 4, 'RECHAZADO Has.', 1, 'C', 0);
        $this -> setXY(231, 56);
        $this -> MultiCell($w[11], 4, 'CATEGORIA APROBADO', 1, 'C', 0);
        $this -> setXY(256, 56);
        $this -> MultiCell($w[12], 4, utf8_decode('NÚMERO BOLSAS'), 1, 'C', 0);
        $this -> setXY(271, 56);
        $this -> MultiCell($w[13], 4, 'INSCRIP. Bs.', 1, 'C', 0);
        $this -> setXY(286, 56);
        $this -> MultiCell($w[12], 4, 'INSP. CAMPO', 1, 'C', 0);
        $this -> setXY(301, 56);
        $this -> MultiCell($w[15], 4, 'ETIQUETAS Bs.', 1, 'C', 0);
        $this -> setXY(320, 56);
        $this -> MultiCell($w[16], 4, 'TOTAL Bs.', 1, 'C', 0);
        $this -> setXY(271, 52);
        $this -> Cell($w[17], 4, utf8_decode('COSTO  POR  CERTIFICACIÓN'), 1, 1, 'C', 0);
    }

    // Tabla coloreada
    function datos($cultivo,$desde,$hasta,$semillera,$fechas,$comunidades,$semilleristas,$cultivos,$variedades,$cSembradas,$superficies,$nocampos,$aprobadas,$rechazadas,$cAprobadas,$nobolsas,$inscripciones,$campos,$etiquetas,$totales) {
        
        $total_semilleras = 1;
        //total de semilleristas de una semillera y un cultivo en un rago de fecha
        $total_semilleristas = array();
        for ($i=0; $i < $total_semilleras ; $i++) { 
            $this -> SetFont('Arial', '', 10);
            $this -> AddPage(); 
            Reportes::getCantidadByCultivo($semillera,$cultivo,$desde,$hasta);
            while($obj = DBConnector::objeto()){
                array_push($total_semilleristas,$obj->cantidad);
            }    
        }
        $aux = 0;
        $dates = new Funciones();
        if ( in_array($desde, array("01", "04", "07", "10")) ){
            $desde = $dates->getInicioMes($desde);
            $hasta = $dates->getFinMes($hasta);
        }else{
            $desde = $dates->cambiar_formato_fecha($desde);
            $hasta = $dates->cambiar_formato_fecha($hasta);
        }
        for ($i=0; $i <$total_semilleras ; $i++) {
            $contador_semilleristas = 0;
            $nro_total_semilleristas = $total_semilleristas[$i];
            $this -> SetFont('Arial', 'B', 9);
            
            $this -> setXY(21, 30);                
            $this->Cell(25,15,"DESDE :   $desde  HASTA :   $hasta ",0,1,'L');
            $this -> SetFont('Arial', 'B', 10);
            $this -> setXY(38, 37);
            $this -> Cell(50, 15, 'SEMILLERA : ' . utf8_decode($semillera), 0, 1);
            $this -> setXY(38, 41);
            $this -> Cell(50, 15, 'Cultivo : ' . utf8_decode($cultivos[0]), 0, 1);
            $total_superficie = $total_aprobada = $total_rechazada = $total_bolsas = 0;  
            $total_inscripcion = $total_campo = $total_etiqueta = $total_total = 0;
            $w = array(  20, 20, 13, 17, 18, 25, 15, 15, 15, 16, 12, 58);
            $h = array(  8,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4);
            $posX = 23; 
            $posY = 64;
            $ancho_pagina = $this->w;
            while ($contador_semilleristas < $nro_total_semilleristas) {
                $this -> SetFont('Arial', '', 10);
                $this->SetXY($posX, $posY);
                $this->Cell(8,8,($contador_semilleristas+1),1,1,'C',FALSE);
                $posX += 8;
                $this->SetXY($posX, $posY);
                $this->Cell(20,8,$fechas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 20;
                $this->SetXY($posX, $posY);
                $this->Cell(25,8,utf8_decode($comunidades[$contador_semilleristas]),1,1,'C',FALSE);
                $posX += 25;
                $this->SetXY($posX, $posY);
                $this -> MultiCell(34, 8, utf8_decode($semilleristas[$contador_semilleristas]), 1, 'C', 0);
                $posX += 34;
                $this->SetXY($posX, $posY);
                $this->Cell(21,8,utf8_decode($variedades[$contador_semilleristas]),1,1,'C',FALSE);                
                $posX += 21;
                $this->SetXY($posX, $posY);
                $this->Cell(20,8,$cSembradas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 20;
                $this->SetXY($posX, $posY);
                $this->Cell(26,8,number_format($superficies[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_superficie += $superficies[$contador_semilleristas];
                $posX += 26;
                $this->SetXY($posX, $posY);
                $this->Cell(13,8,$nocampos[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 13;
                $this->SetXY($posX, $posY);
                $this->Cell(19,8,number_format($aprobadas[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_aprobada += $aprobadas[$contador_semilleristas];
                $posX += 19;
                $this->SetXY($posX, $posY);
                $this->Cell(22,8,number_format($rechazadas[$contador_semilleristas],2),1,1,'C',FALSE);
                $total_rechazada += $rechazadas[$contador_semilleristas];
                $posX += 22;
                $this->SetXY($posX, $posY);
                $this->Cell(25,8,$cAprobadas[$contador_semilleristas],1,1,'C',FALSE);
                $posX += 25;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$nobolsas[$contador_semilleristas],1,1,'C',FALSE);
                $total_bolsas += $nobolsas[$contador_semilleristas];
                $posX += 15;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$inscripciones[$contador_semilleristas],1,1,'C',FALSE);
                $total_inscripcion += $inscripciones[$contador_semilleristas];
                $posX +=15;
                $this->SetXY($posX, $posY);
                $this->Cell(15,8,$campos[$contador_semilleristas],1,1,'C',FALSE);
                $total_campo += $campos[$contador_semilleristas];
                $posX +=15;
                $this->SetXY($posX, $posY);
                $this->Cell(19,8,$etiquetas[$contador_semilleristas],1,1,'C',FALSE);
                $total_etiqueta += $etiquetas[$contador_semilleristas];
                $posX +=19;
                $this->SetXY($posX, $posY);
                $total_fila=$inscripciones[$contador_semilleristas]+$campos[$contador_semilleristas]+$etiquetas[$contador_semilleristas];
                $this->Cell(12,8,$total_fila,1,1,'C',FALSE);
                $total_total += $total_fila;
                
                if (($contador_semilleristas+1)%16 != 0){
                    $posX = 23;
                    $posY += 8;    
                }else{
                    $this->AddPage();
                    $this -> SetFont('Arial', 'B', 8);
                    $this -> setXY(38, 37);
                    $this -> Cell(50, 15, 'SEMILLERA : ' . utf8_decode($semillera), 0, 1);
                    $this -> setXY(38, 41);
                    $this -> Cell(50, 15, 'Cultivo : ' . utf8_decode($cultivos[0]), 0, 1);
                    $posX = 23;
                    $posY = 64;                    
                }                
                $contador_semilleristas++;
            }
            
            $this -> SetFont('Arial', 'B', 10);
            $this->SetXY($posX, $posY);
            $this->Cell(128,5,'T O T A L',1,1,'C',FALSE);  
            $posX += 128;
            $this->SetXY($posX,$posY);
            $this->Cell(26,5,$total_superficie,1,1,'C',FALSE);
            $posX += 26;
            $this->SetXY($posX,$posY);
            $this->Cell(13,5,'',1,1,'C',FALSE); //total  nro de campo
            $posX += 13;
            $this->SetXY($posX,$posY);
            $this->Cell(19,5,$total_aprobada,1,1,'C',FALSE);
            $posX += 19;
            $this->SetXY($posX,$posY);
            $this->Cell(22,5,$total_rechazada,1,1,'C',FALSE);
            $posX += 22;
            $this->SetXY($posX,$posY);
            $this->Cell(25,5,'',1,1,'C',FALSE); //total  categoria aprobada
            $posX += 25;
            $this->SetXY($posX,$posY);
            $this->Cell(15,5,$total_bolsas,1,1,'C',FALSE); //total  nro bolsas
            $posX += 15;
            $this->SetXY($posX,$posY);
            $this->Cell(15,5,$total_inscripcion,1,1,'C',FALSE); //total inscripcion  
            $posX += 15;
            $this->SetXY($posX,$posY);
            
            #var_dump($total_campo);
            $this->Cell(15,5,$total_campo,1,1,'C',FALSE); //total campo  
            $posX += 15;
            $this->SetXY($posX,$posY);
            $this->Cell(19,5,($total_etiqueta),1,1,'C',FALSE); //total etiquetas  
            $posX += 19;
            $this->SetXY($posX,$posY);
            $this->Cell(12,5,($total_total),1,1,'C',FALSE); //total total              
            $aux += $nro_total_semilleristas;  
            $posX = 11;
            $posY = 54;
            $ancho = $w[0];
            $alto = $h[0];
        }       
    }
    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 8);
        
        $posX_inicio = 15;
        $posX_final = $this->w;
        $posX_medio = ($posX_final)/2;
        $posX_cuarto1 = $posX_medio/2;
        $posX_cuarto2 = ($posX_medio+$posX_final)/2;
        $posX_octavo1 = ($posX_inicio+$posX_cuarto1)/2;
        $posX_octavo2 = ($posX_cuarto1+$posX_medio)/2;
        $posX_octavo3 = ($posX_medio+$posX_cuarto2)/2;
        $posX_octavo4 = ($posX_cuarto2+$posX_final)/2;
        
        //linea de firma administrador
        $this -> line(($posX_inicio+25), 130, ($posX_octavo2-30), 130);
        #line de firma responsable
        $this -> line(($posX_octavo3+30), 130, $posX_octavo4, 130);
        
        $this -> setXY(($posX_inicio+50), 125);
        $this -> Cell(15, 15, 'Responsable Departamental o Regional', 0, 1, 'C');
        $this -> setXY($posX_octavo3, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY($posX_octavo3, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }
}

$pdf = new PDF('L', 'mm', "legal");

foreach ($lst_semillera as $key => $semillera) {
    $pdf -> datos($cultivo,$desde,$hasta,$semillera,$lst_fecha,$lst_comunidad,$lst_semillerista,$lst_cultivo,$lst_variedad,$lst_cat_sembrada,$lst_superficie,$lst_nocampo,$lst_aprobada,$lst_rechazada,$lst_cat_aprobada,$lst_nobolsa,$lst_inscripcion,$lst_campo,$lst_etiqueta,$lst_total);
}

$pdf -> Output();
?>
