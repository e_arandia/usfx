<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, utf8_decode('Inspección de campo semillero'), 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15, 7, 'Semillera : ', 0, 1);
        $this -> setXY(15,50);
        $this -> Cell(15, 7, 'Semillerista : ', 0, 1);
        $posX = (($this->w)/3);
        $this -> setXY(-$posX,45);
        $this -> Cell(15, 7, utf8_decode('Número Campo : '), 0, 1);
        
        $this -> setXY(-$posX,58);
        $this -> Cell(15, 7, 'Cultivo : ', 0, 1);
        $this -> setXY(15,58);
        $this -> Cell(15, 7, 'Comunidad : ', 0, 1);
        $this -> setXY(15,68);
        $this -> Cell(15, 7, 'Inspeccion Vegetativa : ', 0, 1);
        $this -> setXY(15,74);
        $this -> Cell(15, 7, 'Observacion : ', 0, 1);
        $this -> setXY(15,81);
        $this -> Cell(15, 7, 'Inspeccion Floracion : ', 0, 1);
        $this -> setXY(15,87);
        $this -> Cell(15, 7, 'Observacion : ', 0, 1);
        $this -> setXY(15,95);
        $this -> Cell(15, 7, 'Inspeccion Precosecha : ', 0, 1);
        $this -> setXY(15,101);
        $this -> Cell(15, 7, 'Observacion : ', 0, 1);
    }

    function contenido($inspeccionPDF) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(37, 41);
        $this -> Cell(0, 15, $inspeccionPDF['semillera'], 0, 1);
        $this -> setXY(42, 46);
        $this -> Cell(0, 15, $inspeccionPDF['semillerista'], 0, 1);
        $posX = (($this->w)/3)+107;
        $this -> setXY($posX, 41);
        $this -> Cell(0, 15, $inspeccionPDF['nro_campo'], 0, 1);
        $this -> setXY(42, 54);
        $this -> Cell(0, 15, utf8_decode($inspeccionPDF['comunidad']), 0, 1);
        $this -> setXY($posX-16, 54);
        $this -> Cell(0, 15, $inspeccionPDF['cultivo'], 0, 1);
        $this -> setXY(62, 64);
        $this -> Cell(0, 15, $inspeccionPDF['primera'], 0, 1);
        $this -> setXY(45, 70);
        $this -> Cell(0, 15, ucfirst($inspeccionPDF['observacion_1']), 0, 1);
        $this -> setXY(60, 77);
        $this -> Cell(0, 15, $inspeccionPDF['segunda'], 0, 1);
        $this -> setXY(45, 83);
        $this -> Cell(0, 15, ucfirst($inspeccionPDF['observacion_2']), 0, 1);
        $this -> setXY(65, 91);
        $this -> Cell(0, 15, $inspeccionPDF['tercera'], 0, 1);
        $this -> setXY(45, 97);
        $this -> Cell(0, 15, ucfirst($inspeccionPDF['observacion_3']), 0, 1);
    }


    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
//titulo pagina
$pdf -> tituloPagina();
$pdf -> titulo();
//contenido
$pdf -> contenido($inspeccionPDF);

$pdf -> Output();
?>
