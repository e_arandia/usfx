<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Superficie de la parcela', 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15, 7, 'Semillera: ', 0, 1);
        $this -> setXY(15,53);
        $this -> Cell(15, 7, 'Semillerista: ', 0, 1);
        $posX = ($this->w)/3;
        $this -> setXY(-$posX,48);
        $this -> Cell(15, 7, 'Numero Campo: ', 0, 1);
        $this -> setXY(15,60);
        $this -> Cell(15, 7, 'Cultivo: ', 0, 1);
        $this -> setXY(15,67);
        $this -> Cell(15, 7, 'Variedad: ', 0, 1);
        $this -> setXY(15,74);
        $this -> Cell(15, 7, 'Superficie Inscrita: ', 0, 1);
        $this -> setXY(15,82);
        $this -> Cell(15, 7, 'Superficie Rechazada: ', 0, 1);
        $this -> setXY(15,90);
        $this -> Cell(15, 7, 'Superficie Retirada: ', 0, 1);
        $this -> setXY(15,98);
        $this -> Cell(15, 7, 'Superficie Aprobada: ', 0, 1);
    }

    function contenido($superficie) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(35, 45);
        $this -> Cell(15, 7, $superficie['semillera'], 0, 1);
        $this -> setXY(40, 53);
        $this -> Cell(15, 7, $superficie['semillerista'], 0, 1);
        $posX = (($this->w)/3)-34;
        $this -> setXY(-$posX, 48);
        $this -> Cell(15, 7, $superficie['nro_campo'], 0, 1);
        $this -> setXY(31, 60);
        $this -> Cell(15, 7, $superficie['cultivo'], 0, 1);
        $this -> setXY(35, 67);
        $this -> Cell(15, 7, $superficie['variedad'], 0, 1);
        $this -> setXY(53, 74);
        $this -> Cell(15, 7, $superficie['inscrita'] . ' has.', 0, 1);
        $this -> setXY(60, 82);
        $this -> Cell(15, 7, $superficie['rechazada'] . ' has.', 0, 1);
        $this -> setXY(56, 90);
        $this -> Cell(15, 7, $superficie['retirada'] . ' has.', 0, 1);
        $this -> setXY(58, 98);
        $this -> Cell(15, 7, $superficie['aprobada'] . ' has.', 0, 1);
    }

    /// Pie de p�gina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulo();
$pdf -> contenido($superficiePDF);

$pdf -> Output();
?>
