<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(85, 183, 140, 183);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Inspeccion de campo semillero', 0, 1, 'C');
    }

    function titulos() {
        $this -> SetFont('Arial', 'B', 12);

        $this -> setY(45);
        $this -> Cell(0, 7, 'Semillera: ', 0, 1);
        $this -> Cell(0, 7, 'Semillerista: ', 0, 1);
        $this -> Cell(0, 7, 'Numero campo: ', 0, 1);
        $this -> Cell(0, 7, 'Fecha emision: ', 0, 1);
        $this -> Cell(0, 7, 'Categoria campo: ', 0, 1);
        $this -> Cell(0, 20, 'Rendimiento estimado: ', 0, 1);
        $this -> Cell(0, 15, 'Superficie Parcela: ', 0, 1);
        $this -> Cell(0, 15, 'Rendimiento Campo: ', 0, 1);
        $this -> Cell(0, 15, 'Numero Cupones: ', 0, 1);
        $this -> Cell(0, 10, 'Rango cupones: ', 0, 1);
        $this -> Cell(0, 15, 'Planta acondicionadora: ', 0, 1);
    }

    function contenido($cosecha) {
        $this -> SetFont('Times', '', 12);
        $this -> setXY(32, 41);
        $this -> Cell(0, 15, $cosecha['semillera'], 0, 1);
        $this -> setXY(36, 48);
        $this -> Cell(0, 15, $cosecha['semillerista'], 0, 1);
        $this -> setXY(44, 55);
        $this -> Cell(0, 15, $cosecha['nro_campo'], 0, 1);
        $this -> setXY(42, 62);
        $this -> Cell(0, 15, $cosecha['fechaEmision'], 0, 1);
        $this -> setXY(47, 69);
        $this -> Cell(0, 15, $cosecha['categoriaCampo'], 0, 1);
        $this -> setXY(57, 82.5);
        $this -> Cell(0, 15, $cosecha['rendimientoEstimado'] . ' TM/Ha.', 0, 1);
        $this -> setXY(49, 100);
        $this -> Cell(0, 15, $cosecha['superficieParcela'] . ' Ha.', 0, 1);
        $this -> setXY(53, 115);
        $this -> Cell(0, 15, $cosecha['rendimientoCampo'] . ' TM/Ha', 0, 1);
        $this -> setXY(47, 130);
        $this -> Cell(0, 15, $cosecha['nroCupones'], 0, 1);
        $this -> setXY(45, 143);
        $this -> Cell(0, 15, $cosecha['rango'], 0, 1);
        $this -> setXY(60, 155);
        $this -> Cell(0, 15, ucfirst($cosecha['plantaAcondicionadora']), 0, 1);
    }

    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        $this -> setXY(27, 178);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 182);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('P', 'mm', array(220, 210));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf ->titulos();
$pdf -> contenido($cosechaPDF);
$pdf -> Output();
?>