<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //descripcion  cuenta
        $img3 = URL . HOME_FOLDER . IMG . '/descripcion2.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $posX = (($this -> w)-40);
        $this -> Image($img2, $posX, 8, 33);
        $this -> Image($img3, $posX-105, 12,75,10);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, $posX+35, 25);
        $this -> line(10, 25, $posX+35, 25);
        $this -> line(10, 25, $posX+35, 25);
        //linea de firma responsable departamental
        $this -> line(20, 128, 75, 128);
        //linea de firma enlace administrativo
        $this -> line(110, 128, 165, 128);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }
    function tituloPagina ($semillerista){
        $this -> SetFont('Times', 'B', 12);
        $posX = ($this -> w)/2;
        $this -> setXY($posX,28);
        $this -> Cell(10, 5, utf8_decode('Estado del Acreedor'), 0, 1, 'C');
        $this -> SetFont('Times', '', 9);
        $this -> setXY($posX,32);
        $this -> Cell(10, 5, '(Expresado en bolivianos)', 0, 1, 'C');
        
    }
    function titulos() {
        $this -> SetFont('Times', 'B', 12);
        $posX = ($this->w)-60;
        
        $this -> setXY(15,40);
        $this -> Cell(15,8, 'Responsable: ', 0, 1);
        $this -> setXY(15,46);
        $this -> Cell(15,8, 'Semillera: ', 0, 1);
        $this -> setXY(15,52);
        $this -> Cell(15,8, 'Semillerista: ', 0, 1);
        $this -> setXY($posX,40);
        $this -> Cell(15,8, 'Fecha: ', 0, 1);      
        $this -> setXY($posX,45);
        $this -> Cell(15,8, 'Cultivo: ', 0, 1,'R');   
        $this -> setXY($posX,50);
        $this -> Cell(15,8, 'Variedad: ', 0, 1,'R');

        $this -> setXY(15,60);
        $this -> Cell(15,8, 'Total a pagar: ', 0, 1);
        $this -> SetFont('Times', '', 11);
        $this -> SetFont('Times', 'B', 10);        
        $posX = 15;
        $posY = 70;
        $ancho = ($this->w/4);
        $alto = 5;
        $this -> setXY($posX,$posY);
        $this -> Cell($ancho-20,$alto+5, 'FECHA', 1, 1,'C');
        $posX += $ancho-20;  
        $this -> setXY($posX,$posY);
        $this -> Cell($ancho+20,$alto+5, 'CONCEPTO', 1, 1,'C');
        $posX +=$ancho+20; 
        $this -> setXY($posX,$posY);
        
        $this -> Cell(($ancho+14),($alto), 'IMPORTE', 1, 1,'C');
       
        $this -> SetFont('Times', 'B', 9);        
        $posY += $alto;
        $this -> setXY($posX,$posY);
        $this -> Cell(($ancho-20),($alto), 'PAGO PARCIAL', 1, 1,'C');
        $posX += $ancho-20;
        $this -> setXY($posX,$posY);
        $this -> Cell(($ancho-20),($alto), 'SALDO', 1, 1,'C');
        /*
        $this -> setXY($posX,$posY);
        $this -> Cell((3*$ancho),($alto-5), 'TOTAL SERVICIO', 1, 1,'C');
        $this -> setXY($posX,$posY);
        $this -> Cell((3*$ancho),($alto-5), 'PAGO PARCIAL', 1, 1,'C');
        $this -> setXY($posX,$posY);
        $this -> Cell((3*$ancho),($alto-5), 'SALDO', 1, 1,'C');
*/
    }
    function contenido($cuenta){
        $this -> SetFont('Times', '', 11);
        $posX = ($this->w)-45;
        $this -> setXY($posX,40);
        $this -> Cell(15,8, $cuenta['fecha'], 0, 1);
        $this -> setXY(40,40);
        $this -> Cell(15,8, $cuenta['responsable'], 0, 1);
        $this -> setXY(35,46);
        $this -> Cell(40,8, $cuenta['semillera'], 0, 1);
        $this -> setXY(38,52);
        $this -> Cell(15,8, $cuenta['semillerista'], 0, 1);      
        $this -> setXY($posX,45);
        $this -> Cell(15,8, $cuenta['cultivo'], 0, 1);   
        $this -> setXY($posX,50);
        $this -> Cell(15,8, $cuenta['variedad'], 0, 1);
        /*
        $posX = ($this->w)/2;  //mitad de hoja     
        $this -> setXY(($posX+40),60);
        $this -> Cell(15,8, $cuenta['categoria_aprobada'], 0, 1);        
        $this -> setXY(($posX+40),67);
        $this -> Cell(15,8, $cuenta['superficie_aprobada'].' Has.', 0, 1);
        
        $this -> setXY(48,60);
        $this -> Cell(15,8, $cuenta['bolsa_etiqueta'], 0, 1);
        $this -> setXY(38,67.3);
        $this -> Cell(15,8, $cuenta['inscripcion'].' Bs.', 0, 1);
        $this -> setXY(38,74.3);
        $this -> Cell(15,8, $cuenta['inspeccion'].' Bs.', 0, 1);
        $this -> setXY(83,81.3);
        $this -> Cell(15,8, $cuenta['analisis_laboratorio'].' Bs.', 0, 1);
        */
        $this -> setXY(42,60);
        $this -> Cell(15,8, $cuenta['total'].' Bs.', 0, 1);        
    }
    function cuota($isemilla) {
        $posX = 15;
        $posY = 80;
        $alto = 34;
        $ancho = 8;
        Cuenta::getCuotasByIdSemilla($isemilla);
        $cuota_show = array();
        $contador = 1;
        while ($obj = DBConnector::objeto()){
            $this -> setXY($posX,$posY);
            $this -> MultiCell($alto,$ancho, $obj->fecha, 1, 'C');
            $this -> setXY($posX+34,$posY);
            $this -> MultiCell($alto+39.5,$ancho, $contador.utf8_decode('° Pago por ').$obj->descripcion, 1, 'C');
            $this -> setXY($posX+107.3,$posY);
            array_push($cuota_show,$obj->cuota);
            $this -> MultiCell($alto,$ancho, $obj->cuota, 1, 'C');
            $this -> setXY($posX+141,$posY);
            $saldo = number_format($obj->total - array_sum($cuota_show),2,'.',',');
            $this -> MultiCell($alto,$ancho, $saldo, 1, 'C');
            $posY += $ancho;
            $contador++;
        }
        
    }

    // Pie de pagina
    function Firmas() {
        // Arial italic 8
        $this -> SetFont('Arial', 'I', 8);
        // Numero de pagina
        //$this -> Cell(0, 10, 'Page ' . $this -> PageNo() . '/{nb}', 0, 0, 'C');
        $posX = $this->w;
        #echo $posX;
        $this -> setXY(-$posX-110, 124);
        $this -> Cell(0, 15,'Responsable Departamental o Regional', 0, 1, 'C');
 
        $this -> setXY(-$posX+75, 124);
        $this -> Cell(0, 15, 'Enlace Administrativo o Administrador', 0, 1, 'C');

    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(215, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina($cuentaPDF['semillerista']);
$pdf -> titulos();
$pdf -> contenido($cuentaPDF);
$pdf -> cuota($cuentaPDF['isemilla']);
$pdf -> Firmas();
$pdf -> Output();
?>
