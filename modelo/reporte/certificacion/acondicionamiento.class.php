<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Acondicionamento de cultivo', 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15, 4, 'Semillera : ', 0, 1);
        $this -> setXY(15,53);
        $this -> Cell(15, 4, 'Semillerista : ', 0, 1);
        $this -> setXY(15,59);
        $this -> Cell(15, 4, 'Comunidad : ', 0, 1);
        $posX = (($this->w)/3);
        $this -> setXY(-$posX,45);
        $this -> Cell(15, 4, 'Numero Campo : ', 0, 1);
        $this -> setXY(15,66);
        $this -> Cell(15, 4, 'Emision Hoja de Cosecha : ', 0, 1);
        $this -> setXY(15,73);
        $this -> Cell(15, 4, 'Categoria Campo : ', 0, 1);
        $this -> setXY(15,80);
        $this -> Cell(15, 4, 'Rendimiento Estimado(TM) : ', 0, 1);
        $posX = (($this->w)/2);
        $this -> setXY($posX,79);
        $this -> Cell(15, 4, 'Rendimento Campo(TM) : ', 0, 1);
        $this -> setXY(15,87);
        $this -> Cell(15, 4, 'Superficie (Ha.): ', 0, 1);
        $this -> setXY(15,95);
        $this -> Cell(15, 4, 'Rango : ', 0, 1);
        $this -> setXY(15,103);
        $this -> Cell(15, 4, 'Numero de Cupones : ', 0, 1);
        $this -> setXY(15,111);
        $this -> Cell(15, 4, 'Planta Acondicionadora : ', 0, 1);
        $posX = (($this->w)/2);
        $this -> setXY($posX,103);
        $this -> Cell(15, 4, 'Peso Bruto de Semilla (Kg.): ', 0, 1);
    }

    function contenido($acondiciona) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(37, 45);
        $this -> Cell(15, 4, $acondiciona['semillera'], 0, 1);
        $this -> setXY(43, 53);
        $this -> Cell(15, 4, $acondiciona['semillerista'], 0, 1);
        $this -> setXY(42, 59);
        $this -> Cell(15, 4, utf8_decode($acondiciona['comunidad']), 0, 1);
        $posX = (($this->w)/3)-35;
        $this -> setXY(-$posX, 45);
        $this -> Cell(15, 4, $acondiciona['nro_campo'], 0, 1);
        $this -> setXY(70, 66);
        $this -> Cell(15, 4, $acondiciona['emision'], 0, 1);
        $this -> setXY(53, 73);
        $this -> Cell(15, 4, $acondiciona['categoria_campo'], 0, 1);
        $this -> setXY(75, 80);
        $this -> Cell(15, 4, $acondiciona['rendimiento_estimado'], 0, 1);
        $posX = (($this->w)/2)+55;
        $this -> setXY($posX, 79);
        $this -> Cell(15, 4, $acondiciona['rendimiento_campo'], 0, 1);
        $this -> setXY(48, 87);
        $this -> Cell(15, 4, $acondiciona['superficie'], 0, 1);
        $this -> setXY(32, 95);
        $this -> Cell(15, 4, $acondiciona['rango'], 0, 1);
        $this -> setXY(61, 103);
        $this -> Cell(15, 4, $acondiciona['nocupones'], 0, 1);
        $this -> setXY(70, 111);
        $this -> Cell(15, 4, $acondiciona['planta'], 0, 1);
        $posX = (($this->w)/2)+60;
        $this -> setXY($posX, 103);
        $this -> Cell(15, 4, $acondiciona['peso_bruto'], 0, 1);
    }

    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulo();
$pdf -> contenido($acondicionaPDF);

$pdf -> Output();
?>
