<?php
class PDF extends FPDF {
    // Cabecera de pagina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, utf8_decode('Solicitud de Inscripción'), 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15, 5, 'Numero de solicitud: ', 0, 1);
        $posX = ($this->w)/3;
        $this -> setXY(-$posX,45);
        $this -> Cell(15, 5, 'Sistema: ', 0, 1);
        $this -> setXY(15,57);
        $this -> Cell(15, 5, 'Semillera: ', 0, 1);
        $this -> setXY(15,65);
        $this -> Cell(15, 5, 'Semillerista: ', 0, 1);
        $this -> setXY(15,72);
        $this -> Cell(15, 5, 'Departamento: ', 0, 1);
        $this -> setXY(15,79);
        $this -> Cell(15, 5, 'Provincia: ', 0, 1);
        $this -> setXY(15,86);
        $this -> Cell(15, 5, 'Municipio: ', 0, 1);
        $this -> setXY(15,93);
        $this -> Cell(15, 5, 'Comunidad: ', 0, 1);
        $this -> setXY(15,100);
        $this -> Cell(15, 5, 'Fecha de Solicitud: ', 0, 1);
    }

    function contenido($solicitud) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(58, 40);
        $this -> Cell(0, 15, $solicitud['numero'], 0, 1);
        $posX = (($this->w)/3)-18;
        $this -> setXY(-$posX, 40);
        $this -> Cell(0, 15, utf8_decode('Certificación'), 0, 1);
        $this -> setXY(36, 52);
        $this -> Cell(0, 15, $solicitud['semillera'], 0, 1);
        $this -> setXY(42, 60);
        $this -> Cell(0, 15, $solicitud['semillerista'], 0, 1);
        $this -> setXY(46, 67);
        $this -> Cell(0, 15, $solicitud['departamento'], 0, 1);
        $this -> setXY(36, 74);
        $this -> Cell(0, 15, utf8_decode($solicitud['provincia']), 0, 1);
        $this -> setXY(37, 81);
        $this -> Cell(0, 15, utf8_decode($solicitud['municipio']), 0, 1);
        $this -> setXY(40, 88);
        $this -> Cell(0, 15, utf8_decode($solicitud['comunidad']), 0, 1);
        $this -> setXY(54, 95);
        $this -> Cell(0, 15, $solicitud['fecha'], 0, 1);
    }

    // Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 150));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulo();
$pdf -> contenido($solicitudPDF);

$pdf -> Output();
?>
