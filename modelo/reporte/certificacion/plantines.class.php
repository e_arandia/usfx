<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, utf8_decode('Certificación de Plantines'), 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,67);
        $this -> Cell(15, 7, 'Semillera : ', 0, 1);
        $this -> setXY(15,53);
        $this -> Cell(15, 7, 'Semillerista : ', 0, 1);
        $this -> setXY(15,60);
        $this -> Cell(15, 7, 'Comunidad : ', 0, 1);
        $this -> setXY(15,45);
        $this -> Cell(15, 7, 'Fecha Solicitud : ', 0, 1);
        $this -> setXY(15,74);
        $this -> Cell(15, 7, 'Cultivo : ', 0, 1);
        $this -> setXY(15,82);
        $this -> Cell(15, 7, 'Variedad : ', 0, 1);
    }

    function contenido($plantines) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(37, 67);
        $this -> Cell(15, 7, $plantines['semillera'], 0, 1);
        $this -> setXY(42, 53);
        $this -> Cell(15, 7, $plantines['semillerista'], 0, 1);
        $this -> setXY(42, 60);
        $this -> Cell(15, 7, $plantines['comunidad'], 0, 1);
        $this -> setXY(50, 45);
        $this -> Cell(15, 7, $plantines['fecha'], 0, 1);
        $this -> setXY(33, 74);
        $this -> Cell(15, 7, $plantines['cultivo'], 0, 1);
        $this -> setXY(37, 82);
        $this -> Cell(15, 7, $plantines['variedad'], 0, 1);
    }

    /// Pie de pagina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulo();
$pdf -> contenido($plantinesPDF);

$pdf -> Output();
?>
