<?php
class PDF extends FPDF {
    // Cabecera de p�gina
    function Header() {
        //imagen 1
        $img1 = URL . HOME_FOLDER . IMG . '/MDRyT.jpg';
        //imagen 2
        $img2 = URL . HOME_FOLDER . IMG . '/logoPagina.jpg';
        // Logo
        $this -> Image($img1, 10, 8, 33);
        $this -> Image($img2, 170, 8, 33);
        //linea de separacion de cabecera y contenido
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        $this -> line(10, 25, 203, 25);
        //linea de firma
        $this -> line(90, 130, 145, 130);
        // Arial bold 15
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        // Salto de l�nea
        $this -> Ln(20);
    }

    function tituloPagina() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setY(28);
        $this -> Cell(0, 5, 'Datos Semilla a producir', 0, 1, 'C');
    }

    function titulo() {
        $this -> SetFont('Arial', 'B', 12);
        $this -> setXY(15,45);
        $this -> Cell(15, 4, 'Semillera : ', 0, 1);
        $this -> setXY(15,53);
        $this -> Cell(15, 4, 'Semillerista : ', 0, 1);
        $this -> setXY(15,59);
        $this -> Cell(15, 4, 'Comunidad : ', 0, 1);
        $posX = (($this->w)/3);
        $this -> setXY(-$posX,45);
        $this -> Cell(15, 4, 'Numero Campo : ', 0, 1);
        $this -> setXY(15,66);
        $this -> Cell(15, 4, 'Cultivo : ', 0, 1);
        $this -> setXY(15,73);
        $this -> Cell(15, 4, 'Variedad : ', 0, 1);
        $this -> setXY(15,80);
        $this -> Cell(15, 4, 'Categoria Sembrada : ', 0, 1);
        $posX = (($this->w)/2);
        $this -> setXY($posX,79);
        $this -> Cell(15, 4, 'Categoria a Producir : ', 0, 1);
        $this -> setXY(15,87);
        $this -> Cell(15, 4, 'Cantidad Semilla Empleada : ', 0, 1);
        $this -> setXY(15,95);
        $this -> Cell(15, 4, 'Fecha Siembra : ', 0, 1);
        $this -> setXY(15,103);
        $this -> Cell(15, 4, 'Plantas por Hectarea : ', 0, 1);
        $this -> setXY(15,111);
        $this -> Cell(15, 4, 'Cultivo Anterior : ', 0, 1);
    }

    function contenido($semilla) {
        $this -> SetFont('Arial', '', 12);
        $this -> setXY(37, 45);
        $this -> Cell(15, 4, $semilla['semillera'], 0, 1);
        $this -> setXY(43, 53);
        $this -> Cell(15, 4, $semilla['semillerista'], 0, 1);
        $this -> setXY(42, 59);
        $this -> Cell(15, 4, utf8_decode($semilla['comunidad']), 0, 1);
        $posX = (($this->w)/3)-35;
        $this -> setXY(-$posX, 45);
        $this -> Cell(15, 4, $semilla['nro_campo'], 0, 1);
        $this -> setXY(32, 66);
        $this -> Cell(15, 4, $semilla['cultivo'], 0, 1);
        $this -> setXY(37, 73);
        $this -> Cell(15, 4, $semilla['variedad'], 0, 1);
        $this -> setXY(60, 80);
        $this -> Cell(15, 4, $semilla['categoria_sembrada'], 0, 1);
        $posX = (($this->w)/2)+45;
        $this -> setXY($posX, 79);
        $this -> Cell(15, 4, $semilla['categoria_producir'], 0, 1);
        $this -> setXY(73, 87);
        $this -> Cell(15, 4, $semilla['cantidad_semilla_empleada'], 0, 1);
        $this -> setXY(47, 95);
        $this -> Cell(15, 4, $semilla['fecha_siembra'], 0, 1);
        $this -> setXY(61, 103);
        $this -> Cell(15, 4, $semilla['plantas_hectarea'], 0, 1);
        $this -> setXY(50, 111);
        $this -> Cell(15, 4, $semilla['cultivo_anterior'], 0, 1);
    }

    // Pie de p�gina
    function Footer() {
        // Posicion: a 1,5 cm del final
        $this -> SetY(-15);
        // Arial italic 5
        $this -> SetFont('Arial', 'I', 9);
        // Numero de pagina
        #$this->Cell(0,10,date('d-m-Y'),0,0,'R');
        $this -> setXY(27, 125);
        $this -> Cell(0, 15, utf8_decode('Responsable Certificación'), 0, 1, 'C');
        $this -> setXY(27, 128);
        $this -> Cell(0, 15, $_SESSION['usr_nombre'] . ' ' . $_SESSION['usr_apellido'], 0, 1, 'C');
    }

}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L', 'mm', array(220, 160));
$pdf -> AliasNbPages();
$pdf -> AddPage();
$pdf -> tituloPagina();
$pdf -> titulo();
$pdf -> contenido($semillaPDF);

$pdf -> Output();
?>
