<?php
class PDF extends FPDF
{
// Cabecera de pagina
function Header()
{
    //imagen 1
    $img1 = URL.HOME_FOLDER.IMG.'/MDRyT.jpg'; 
    //imagen 2
    $img2 = URL.HOME_FOLDER.IMG.'/logoPagina.jpg'; 
    // Logo
    $this->Image($img1,10,8,33);
    $this->Image($img2,170,8,33);
    //linea de separacion de cabecera y contenido
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    $this -> line(10,25,203,25);
    //linea de firma
    $this -> line(90,150,140,150);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Salto de l�nea
    $this->Ln(20);
}

/// Pie de p�gina
function Footer()
{
	// Posicion: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Numero de pagina
	//$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creacion del objeto de la clase heredada
$pdf = new PDF('L','mm',array(215,200));
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->setY(28);
$pdf->Cell(0,5,'Datos de Muestra para laboratorio',0,1,'C');
$pdf->setY(45);
//for($i=1;$i<=40;$i++)
$pdf->Cell(0,7,'Semillerista: ',0,1);
$pdf->Cell(0,7,'Numero Campo: ',0,1);
$pdf->Cell(0,15,'Fecha recepcion: ',0,1);
$pdf->Cell(0,7,'Cultivo: ',0,1);
$pdf->Cell(0,7,'Variedad: ',0,1);
//contenido
$pdf->SetFont('Times','',12);
$pdf->setXY(35,41);
$pdf->Cell(0,15,$muestraPDF['semillerista'],0,1);
$pdf->setXY(41,48);
$pdf->Cell(0,15,$muestraPDF['nro_campo'],0,1);
$pdf->setXY(42,59);
$pdf->Cell(0,15,$muestraPDF['fecha_recepcion'],0,1);
$pdf->setXY(25,70);
$pdf->Cell(0,15,$muestraPDF['cultivo'],0,1);
$pdf->setXY(30,77);
$pdf->Cell(0,15,$muestraPDF['variedad'],0,1);
//responsable
$pdf->setXY(27,145);
$pdf->Cell(0,15,'Responsable '.$_SESSION['usr_area'],0,1,'C');
$pdf->setXY(27,150);
$pdf->Cell(0,15,$_SESSION['usr_nombre'].' '.$_SESSION['usr_apellido'],0,1,'C');
$pdf->Output();
?>
