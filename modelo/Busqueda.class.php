<?php
class Busqueda {
    /**
     *cadena a buscar en la base de datos
     */
    private $cadena;

    /**
     * contiene texto que sera buscado con like
     * */
    private $texto_like = '';
    private $texto_nombre = '';
    private $texto_apellido = '';
    private $texto_semillera = '';
    private $texto_categoria = '';
    private $texto_categoria_producir = '';
    private $texto_categoria_sembrada = '';
    private $texto_campanha = '';

    /**
     * texto a buscar con match
     * */
    private $texto_match = array();
    /**
     * contiene la cadena de consulta
     * */
    private $query;
    /**
     * @param cadena_busqueda  cadena de busqueda
     * @param enlace a la base de datos
     * */
    public function buscarCadena($cadena_busqueda) {
        $search = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'Á', 'É', 'Í', 'Ó', 'Ú');
        $this -> cadena = addslashes(str_ireplace($search, '_', $cadena_busqueda));
    }
    /**
     * devuelve cantidad de semilleristas
     * */
    public function getBuscarSemilleristas($semillera,$area){
        $query = "SELECT DISTINCT(semillerista) FROM view_gestiones_anteriores ";
        $query .= "WHERE semillera LIKE '$semillera' AND id_sistema=$area AND gestion < ".date('Y');
        
        DBConnector::ejecutar($query);
    }
    public function prepareSearch() {
        $contar_palabras = explode(" ", $this -> cadena);
        $palabras = count($contar_palabras);
        echo $this->cadena;
        switch ($palabras) {
            case 1 :
                switch ($this->cadena) {
                    case 'semillera' :
                        $this -> texto_like = 'semillera';
                        break;
                    case ($this->cadena == 'semillerista' || $this->cadena == 'productor') :
                        $this -> texto_like = 'semillerista';
                        break;
                    case 'categoria' :
                        $this -> texto_like = 'categoria';
                        break;
                    case 'cultivo' :
                        $this -> texto_like = 'cultivo';
                        break;
                    case 'variedad' :
                        $this -> texto_like = 'variedad';
                        break;
                    case ($this->cadena == 'campa_a' || $this->cadena == 'gestion') :
                        $this -> texto_like = 'campanha';
                        break;
                    case ($this->cadena=='basica'||$this->cadena=='certificada'||$this->cadena=='registrada'||$this->cadena=='fiscalizada') :
                        $this -> texto_categoria = $this -> cadena;
                        break;
                    default :
                        $this -> texto_like = $this -> cadena;
                        break;
                }
                break;

            case 2 :
                if ($contar_palabras[1] != 'sembrada') {
                    $this -> texto_nombre = $contar_palabras[0];
                    $this -> texto_apellido = $contar_palabras[1];
                    $this -> texto_like = 'nombre_apellido';
                } else {
                    $this -> texto_like = 'cat_sembrada';
                }

                break;
            case ($contar_palabras[1] == 'a' && $contar_palabras[2] == 'producir') :
                $this -> texto_categoria_producir = $this -> texto_like = $contar_palabras[2];
                $this -> texto_like = 'cat_producir';
                break;
            default :
                foreach ($contar_palabras as $valor) {
                    $this -> texto_fullText[] = $valor;
                }
                break;
        }
    }

    public function prepareConsulta() {
        $this -> query = "SELECT sol.semillera,sol.nombre,sol.apellido,sem.campanha AS campana, sem.categoria_sembrada,sem.categoria_producir,sem.cultivo,sem.variedad FROM solicitud ";
        switch ($this->texto_like) {

            case ($this->texto_like== 'semillera'||$this->texto_like== 'semillerista'||$this->texto_like== 'cultivo'||$this->texto_like== 'variedad'||$this->texto_like== 'categoria'||$this->texto_like== 'campanha') :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud
				INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla ";
                break;
            case 'nombre_apellido' :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud = sol.id_solicitud
				INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla WHERE sol.nombre LIKE '%" . $this -> texto_nombre . "%' AND sol.apellido LIKE '%" . $this -> texto_apellido . "%'";
                break;
            case 'gestion' :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud				
				INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla WHERE sem.campanha LIKE '%" . $this -> texto_campanha . "%'";
                break;
            case 'cat_sembrada' :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud                
                INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla WHERE sem.categoria_sembrada LIKE '%" . $this -> texto_categoria . "%'";
                break;
            case 'cat_producir' :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud                
                INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla WHERE sem.categoria_producir LIKE '%" . $this -> texto_categoria . "%'";
                break;
            default :
                $this -> query .= "sol INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud                
                INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla
			WHERE sol.semillera LIKE '%$this->texto_like%' OR sol.nombre LIKE '%$this->texto_like%' OR sol.apellido LIKE '%$this->texto_like%' OR sem.cultivo LIKE '%$this->texto_like%' OR sem.variedad LIKE '$this->texto_like%' 
 OR sem.categoria_sembrada LIKE '%$this->texto_like%' OR sem.categoria_producir LIKE '%$this->texto_like%'";
                // OR
                break;
        }
        $this -> query .= " ORDER BY sol.apellido, sem.cultivo";
        //echo $this->query;
    }

    public function executeConsulta() {
        echo $this -> query;
         DBConnector::ejecutar($this -> query);
    }

    /*
     * obtener las campanhas realizadas
     * @param string $sistema nombre del sistema-por defecto vacio
     *
     * */
    public function getCampanhas($sistema='%') {
        Semilla::getListCampanhas($sistema);
    }

    /**
     * obtener detalle de semilleristas y sus cultivos
     * */
    public static function getGestionesAnteriores($semillera, $campanha = '', $nombre = '', $apellido = '') {
        /*
         $query = "	SELECT sol.fecha,
         sol.semillera,
         sol.comunidad,
         sol.nombre,
         sol.apellido,
         sem.cultivo,
         sem.variedad,
         sem.categoria_sembrada,
         sup.inscrita,
         sem.nro_campo,
         sup.aprobada,
         sup.rechazada,
         sem.categoria_producir,
         lab.nro_bolsa,
         cta.inscripcion,
         cta.inspeccion_campo,
         cta.analisis_laboratorio_etiqueta,
         @totalGeneral:= cta.inscripcion+cta.inspeccion_campo+cta.analisis_laboratorio_etiqueta AS total_costo,
         SUM(sup.inscrita) AS totalInscrita,
         SUM(sup.aprobada) AS totalAprobada,
         SUM(sup.rechazada) AS totalRechazada,
         SUM(lab.nro_bolsa) AS nroBolsas_total,
         SUM(cta.inscripcion) AS inscripcion_total,
         SUM(cta.inspeccion_campo) AS inspeccion_campo_total,
         SUM(cta.analisis_laboratorio_etiqueta) AS analabet_total,
         SUM(@totalGeneral) AS total_general
         FROM solicitud sol INNER JOIN semilla sem ON sem.id_solicitud=sol.id_solicitud
         INNER JOIN superficie sup ON sup.id_solicitud=sol.id_solicitud
         INNER JOIN cosecha cos ON cos.id_superficie=sup.id_superficie
         INNER JOIN laboratorio lab ON lab.id_cosecha=cos.id_cosecha
         INNER JOIN cuenta cta ON cta.id_solicitud=sol.id_solicitud
         WHERE sol.semillera='$semillera'";*/
        $cadena = $semillera;
        if (!empty($campanha)) {
            $cadena .= " *" . $campanha;
        }
        if (!empty($nombre) && !empty($apellido)) {
            $cadena .= " >$nombre >$apellido";
        }
        $query = "	SELECT sol.fecha,
			                 sol.semillera,
			                 sol.comunidad,
			                 sol.nombre,
			                 sol.apellido,
			                 sem.cultivo,
			                 sem.variedad,
			                 sem.categoria_sembrada,
			                 sup.inscrita,
			                 sem.nro_campo,
			                 sup.aprobada,
			                 sup.rechazada,
			                 sem.categoria_producir,
			                 lab.nro_bolsa,
			                 cta.inscripcion,
			                 cta.inspeccion_campo,
			                 cta.analisis_laboratorio_etiqueta,
			                 @totalGeneral:= cta.inscripcion+cta.inspeccion_campo+cta.analisis_laboratorio_etiqueta AS total_costo,
			                 SUM(sup.inscrita) AS totalInscrita,
			                 SUM(sup.aprobada) AS totalAprobada,
			                 SUM(sup.rechazada) AS totalRechazada,
			                 SUM(lab.nro_bolsa) AS nroBolsas_total,
			                 SUM(cta.inscripcion) AS inscripcion_total,
			                 SUM(cta.inspeccion_campo) AS inspeccion_campo_total,
			                 SUM(cta.analisis_laboratorio_etiqueta) AS analabet_total,
			                 SUM(@totalGeneral) AS total_general
			          FROM solicitud sol
			          INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
			          INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla
			          INNER JOIN superficie sup ON sup.id_solicitud=sol.id_solicitud
			          INNER JOIN cosecha cos ON cos.id_superficie=sup.id_superficie
			          INNER JOIN laboratorio lab ON lab.id_cosecha=cos.id_cosecha
			          INNER JOIN cuenta cta ON cta.id_solicitud=sol.id_solicitud
			          WHERE sol.semillera LIKE '%$semillera' AND sem.campanha LIKE '%$campanha'";
        return $query;
    }

    public function busquedaPersonalizadaBySemillera($semillera) {
        $query = "SELECT sol.nombre,sol.apellido,sem.cultivo,sem.variedad,sem.cat_sembrada
			          FROM solicitud sol
			          INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
			          INNER JOIN  semilla sem ON sem.semilla=t.id_semilla
			          WHERE sol.semillera='$semillera'";
        // echo $query;
        DBConnector::ejecutar($query);
    }

    public function busquedaPersonalizadaBySemillerista($semillerista) {
        $query = "SELECT *
			          FROM solicitud sol INNER JOIN  semilla sem ON sol.id_solicitud=sem.solicitud
			          WHERE sol.nombre='" . $semillerista['nombre'] . "' AND sol.apellido='" . $semillerista['apellido'] . "'";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    public function busquedaPersonalizadaByCultivo($cultivo) {
        $query = "SELECT *
			      FROM solicitud sol
			      INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
			      INNER JOIN  semilla sem ON sem.semilla=t.id_semilla
			      WHERE MATCH (sol.nombre,sol.apellido,sem.cultivo,sem.variedad,sem.cat_sembrada)
			          AGAINST ('$cultivo' IN BOOLEAN MODE)";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    public function busquedaPersonalizadaByVariedad($variedad) {
        $query = "SELECT *
			          FROM solicitud sol 
			          INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
                      INNER JOIN  semilla sem ON sem.semilla=t.id_semilla
			          WHERE MATCH (sol.nombre,sol.apellido,sem.cultivo,sem.variedad,sem.cat_sembrada)
			          AGAINST ('$variedad' IN BOOLEAN MODE)";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    public function busquedaPersonalizadaByCategoria($categoria) {
        $query = "SELECT *
			          FROM solicitud sol 
			          INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
                      INNER JOIN  semilla sem ON sem.semilla=t.id_semilla
			          WHERE MATCH (sol.nombre,sol.apellido,sem.cultivo,sem.variedad,sem.cat_sembrada)
			          AGAINST ('$categoria' IN BOOLEAN MODE)";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    public static function busquedaPersonalizadaAll($buscar) {
        $query = "SELECT *
			          FROM solicitud sol 
			          INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
                      INNER JOIN  semilla sem ON sem.semilla=t.id_semilla
			          WHERE MATCH (sol.nombre,sol.apellido,sem.cultivo,sem.variedad,sem.cat_sembrada)
			          AGAINST ('$buscar' WITH QUERY EXPANSION)";
        //echo $query;
        DBConnector::ejecutar($query);
    }
} // END
?>