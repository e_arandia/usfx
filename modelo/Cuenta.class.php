<?php
/**
 * @author Edwin Willy Arandia Zeballos
 */
class Cuenta {
    /**
     * Inserta una nueva cuenta
     * @param  varchar  semillera
     * @param  varchar  productor
     * @param  varchar	responsable
     * @param  varchar 	cultivo
     * @param  varchar 	variedad
     * @param  varchar 	catAprobada
     * @param  decimal 	supAprobada
     * @param  decimal	supTotal
     * @param  decimal	bolsaEtiqueta
     * @param  decimal	total
     * @param  decimal	inscripcion
     * @param  decimal	inspeccionCampo
     * @param  decimal	anaLabEti
     * @param  decimal	acondicionamiento
     * @param  decimal	plantines
     * @param  decimal	total
     * @param  date 	fecha
     * @param  decimal	montoPagado
     * @param  decimal	saldoTotal
     * @param  decimal	saldoCampanhaAnterior
     * @param  mediumint isolicitud
     */
    /**
     * revisa los datos en cada variable
     * */
    function checkCuenta($responsable, $semillera, $productor, $cultivo, $variedad, $catAprobada, $supAprobada, $supTotal, $bolsaEtiqueta, $sbtotal, $inscripcion, $inspeccionCampo, $anaLabEti, $acondicionamiento, $plantines, $total, $fecha, $montoPagado, $saldoTotal, $saldoAnterior) {
        $msg = '';
        if (empty($semillera)) {
            $msg .= 'Semillera<br>';
        }
        if (empty($productor)) {
            $msg .= 'Semillerista<br>';
        }
        if (empty($cultivo)) {
            $msg .= 'Cultivo<br>';
        }
        if (empty($variedad)) {
            $msg .= 'Variedad<br>';
        }
        if (empty($catAprobada)) {
            $msg .= 'Categoria Aprobada<br>';
        }
        if (empty($supAprobada)) {
            $msg .= 'Superficie Aprobada<br>';
        }
        if (empty($supTotal)) {
            $msg .= 'Superficie Total<br>';
        }
        if ($bolsaEtiqueta == '') {
            $msg .= 'Bolsa / Etiqueta<br>';
        }
        if (empty($inscripcion)) {
            $msg .= 'Inscripcion <br>';
        }
        if (empty($inspeccionCampo)) {
            $msg .= 'Inspeccion campo<br>';
        }
        if ($anaLabEti == '') {
            $msg .= 'Analisis, Laboratorio, Etiquetacion<br>';
        }
        if ($acondicionamiento == '') {
            $msg .= 'Acondicionamiento<br>';
        }
        if ($plantines == '') {
            $msg .= 'Plantines<br>';
        }
        if ($total == '') {
            $msg .= 'Total<br>';
        }
        if (empty($fecha)) {
            $msg .= 'Fecha<br>';
        }
        if (empty($montoPagado)) {
            $msg .= 'Monto Pagado<br>';
        }
        return $msg;

    }

    function setCuenta($responsable, $semillera, $productor, $campanha, $variedad, $catAprobada, $supAprobada, $supTotal, $bolsaEtiqueta, $sbtotal, $total, $fecha, $montoPagado, $saldoTotal, $saldoCampanhaAnterior, $isolicitud) {
        $query = "INSERT INTO cuenta (responsable,semillera, semillerista,campanha, id_variedad,
		                              categoria_aprobada,superficie_aprobada,superficie_total,bolsa_etiqueta,
		                              subtotal,total, fecha, monto_pagado,saldo,saldo_campanha_anterior,id_semilla)
		                     VALUES ($responsable,$semillera,$productor,$campanha,$variedad,$catAprobada,$supAprobada,$supTotal,$bolsaEtiqueta,$sbtotal,$total, '$fecha', $montoPagado,$saldoTotal,$saldoCampanhaAnterior,$isolicitud )";

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo DBConnector::mensaje(),'=',$query;
        }
    }

    /**
     * cuenta de fiscalizacion
     * */
     public function setCuentaFiscal($responsable, $isemillera, $productor, $variedad, $bolsaEtiqueta, $sbtotal, $total, $fecha, $saldoTotal, $isemilla){
         $query = "INSERT INTO cuenta (responsable,semillera, semillerista, id_variedad,bolsa_etiqueta,subtotal,total,fecha,saldo,id_semilla)
                             VALUES ($responsable,$isemillera,$productor,$variedad,$bolsaEtiqueta,$sbtotal,$total, '$fecha', $saldoTotal,$isemilla)";

        #echo $query;
        DBConnector::ejecutar($query);
     }    
     /**
      * insertar cuota
      * @param   double    $monto         monto de cuota
      * @param   date      $fecha         fecha de pago de cuota
      * @param   string    $descripcion   descripcion de pago 
      * */
     public function setCuota($monto,$fecha,$descripcion,$id_cuenta){
         $query = "INSERT INTO cuota (fecha,monto,descripcion,id_cuenta) VALUES (";
         $query .= "'$fecha',$monto,'$descripcion',$id_cuenta)";
         
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo DBConnector::mensaje().'='.$query;
     }
      
     /**
     * actualizacion de datos de una cuenta
     * */
    public static function updateCuenta($responsable, $semillera, $campanha, $productor, $cultivo, $variedad, $catAprobada, $supAprobada, $supTotal, $bolsaEtiqueta, $sbtotal, $inscripcion, $inspeccionCampo, $anaLabEti, $acondicionamiento, $plantines, $total, $fecha, $montoPagado, $saldoTotal, $saldoCampanhaAnterior, $idCuenta, $link) {
        $query = "UPDATE cuenta SET responsable='$responsable',
		                            campanha='$campanha', 
		                            semillera='$semillera',
		                            productor='$productor',
		                            cultivo='$cultivo',
		                            variedad='$variedad',
		                            categoria_aprobada='$catAprobada',
		                            superficie_aprobada=$supAprobada,
		                            superficie_total=$supTotal,
		                            bolsa_etiqueta=$bolsaEtiqueta,
		                            subtotal=$sbtotal,
		                            inscripcion=$inscripcion,
		                            inspeccion_campo=$inspeccionCampo,
		                            analisis_laboratorio_etiqueta=$anaLabEti,
	                                acondicionamiento=$acondicionamiento,
	                                plantines=$plantines,
	                                total=$total, 
	                                fecha='$fecha', 
	                                monto_pagado=$montoPagado,
	                                saldo_anterior=$saldoTotal,
	                                saldo_campanha_anterior=$saldoCampanhaAnterior
	         WHERE id_cuenta=$idCuenta";
        //echo $query; exit;
        return mysql_query($query, $link);
    }

    /**
     * elimina una cuenta
     * */
    public function deleteCuenta($id) {
        $query = "DELETE FROM cuenta WHERE id_cuenta=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * obtener la cuenta de un productor determinado solo para certificacion
     * @var idSolicitud  si es diferente de 0 selecciona toda la tabla sino solo el productor
     */
    public function getCuenta($idSolicitud = '') {
        if (!empty($idSolicitud)) {
            $query = "SELECT id_cuenta FROM cuenta WHERE id_solicitud =$idSolicitud ";
        } else {
            $query = "SELECT * FROM view_cuenta WHERE id_sistema =1 ";           
        }
        $query .= "ORDER BY id_cuenta DESC";
        DBConnector::ejecutar($query);
     #echo $query;   
        if(DBConnector::nroError()){
            echo DBConnector::mensaje(),'=',$query;
        }
    }
    
    public function getCuentaPDF($idSolicitud){
        $query = "SELECT * FROM view_cuenta WHERE id_solicitud = $idSolicitud";
        
        DBConnector::ejecutar($query);
    }
    /**
     * datos de una cuenta segun semillera
     * @param string $semillera   nombre de la semillera
     * 
     * semcamculvar
     * devuelve el semillerista|nro campo|cultivo|variedad de una semillera
     * */
    public static function getCampoCultivoVariedadCuentaBySemillera($semillera,$sistema=1){
        $query = "SELECT comunidad,nombre_apellido AS semillerista,nro_campo,cultivo,variedad,";
        $query .= "campanha,estado_solicitud,id_solicitud,id_semilla,categoria_producir ";
        $query .= "FROM view_semilla ";
        $query .= "WHERE semillera LIKE  '$semillera%' AND gestion = '".date('Y')."' ";
        #echo $query;
        if ($sistema = 1){
            $query .= "AND id_sistema = 1 ";
        }else{
            $query .= "AND id_sistema = 2 ";
        }
        $query .= "ORDER BY comunidad";
        
        DBConnector::ejecutar($query);   
        if (DBConnector::nroError())
            echo "ERROR :: $query ::",DBConnector::mensaje();     
    }
    /**
     * datos de una cuenta segun semillerista
     * */
     public static function getSemilleraCampanhaCultivoVariedadBySemillerista($semillerista){
        $query = "SELECT comunidad,nombre_apellido AS semillerista,semillera,cultivo,variedad,";
        $query .= "campanha,estado_solicitud,id_solicitud,id_semilla,categoria_producir "; 
        $query .= "FROM view_semilla ";
        $query .= "WHERE nombre_apellido LIKE  '$semillerista%' AND gestion = '".date('Y')."'";
        $query .= "ORDER BY comunidad";
        #echo $query;
        DBConnector::ejecutar($query);  
        
        if(DBConnector::nroError())
            echo ":::ERROR :: $query :: ",DBConnector::mensaje();
     }
    
    /**
     * datos de una cuenta segun semillerista
     * */
     public static function getSemilleraSemilleristaCultivoVariedadByCampanha($campanha){
        $query = "SELECT nombre_apellido AS semillerista,semillera,cultivo,variedad,estado_solicitud,id_solicitud,id_semilla FROM view_semilla ";
        $query .= "WHERE campanha LIKE  '$campanha%' AND gestion = '".date('Y')."'";
        if ($sistema = 1){
            $query .= "AND id_sistema = 1";
        }else{
            $query .= "AND id_sistema = 2";
        }
        #echo $query;
        DBConnector::ejecutar($query);  
     }  
     /**
     * datos de una cuenta segun semillerista
     * */
     public static function getSemilleraSemilleristaCampanhaVariedadByCultivo($cultivo){
        $query = "SELECT comunidad,nombre_apellido AS semillerista,semillera,cultivo,variedad,";
        $query .= "campanha,estado_solicitud,id_solicitud,id_semilla,categoria_producir "; 
        $query .= "FROM view_semilla ";
        $query .= "WHERE cultivo LIKE  '$cultivo%' ";
        if ($sistema = 1){
            $query .= "AND id_sistema = 1";
        }else{
            $query .= "AND id_sistema = 2";
        }
        #echo $query;
        DBConnector::ejecutar($query);  
        if (DBConnector::nroError()){
            echo "ERROR::".__METHOD__."::$query::",DBConnector::mensaje();
        }
     }    
     /**
     * datos de una cuenta segun semillerista
     * */
    public static function getSemilleraSemilleristaCampanhaCultivoByVariedad($variedad){
        $query = "SELECT comunidad,nombre_apellido AS semillerista,semillera,cultivo,variedad,";
        $query .= "campanha,estado_solicitud,id_solicitud,id_semilla,categoria_producir "; 
        $query .= "FROM view_semilla ";
        $query .= "WHERE variedad LIKE  '$variedad%' ";
        if ($sistema = 1){
            $query .= "AND id_sistema = 1";
        }else{
            $query .= "AND id_sistema = 2";
        }
        
        DBConnector::ejecutar($query);  
     }     
    public static function getCuota($semillera,$semillerista,$cultivo,$variedad){
        
        $query = "SELECT fecha,cuota,saldo FROM view_cuenta_semilla_cuota ";
        $query .= "WHERE id_semilla = $id";
#echo $query;        
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo __METHOD__.":: ",DBConnector::mensaje();
    }
    public static function getCuotasByIdSemilla($id){
        
        $query = "SELECT fecha,cuota,total,saldo,descripcion FROM view_cuota_cuenta ";
        $query .= "WHERE id_semilla = $id ";
        #$query .= "ORDER BY fecha DESC ";
#echo $query;        
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo __METHOD__.":: ",DBConnector::mensaje();
    }
    
    public static function getCuotaPDF($id){
        $query = "SELECT * FROM view_cuota_cuenta WHERE id_cuenta = $id";
        
        DBConnector::ejecutar($query);
    }
    
    /**
     * lista de cuentas fiscalizacion
     * @param integer  $idSolicitud    id de solicitud
     * */
     public function getCuentaFiscal($idSolicitud = ''){
         $query = "SELECT * FROM view_cuenta_fiscal";
         
         if(!empty($idSolicitud)){
             $query .= " WHERE id_Solicitud = $idSolicitud";
         }
         #echo $query;
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo 'ERROR view_cuenta_fiscal ',DBConnector::mensaje();
     }
     
     /**
     * filtro e cuenta para fiscalizacion
     * */ 
     public static function getCuentaCertificaFilter($search){
         $query = "SELECT responsable,semillera,semillerista,cultivo,variedad,";
         $query .= "categoria_aprobada FROM view_cuenta ";
         $query .= "WHERE responsable LIKE '$search%' OR semillera LIKE '$search%' OR semillerista LIKE '$search%' ";
         $query .= "OR cultivo LIKE '$search%' OR variedad LIKE '$search%' ";
         #echo $query;
         DBConnector::ejecutar($query);
     }
    /**
     * filtro e cuenta para fiscalizacion
     * */ 
     public static function getCuentaFiscalFilter($search){
         $query = "SELECT responsable,semillera,semillerista,campanha,cultivo,variedad,inscripcion,inspeccion,analisis";
         $query .= ",acondicionamiento,plantines,total,fecha,monto FROM view_cuenta_fiscal ";
         $query .= "WHERE responsable LIKE '$search%' OR semillera LIKE '$search%' OR semillerista LIKE '$search%' ";
         $query .= "OR campanha LIKE '$search%' OR cultivo LIKE '$search%' OR variedad LIKE '$search%' OR inscripcion LIKE '$search%' OR inspeccion LIKE '$search%' OR analisis LIKE '$search%' ";
         $query .= "OR acondicionamiento LIKE '$search%' OR plantines LIKE '$search%' OR total LIKE '$search%' OR fecha LIKE '$search%' OR monto LIKE '$search%'";
         
         DBConnector::ejecutar($query);
     }
    # MODIFICADO - REVISAR
    /**
     * utilizado para el respaldo de datos
     * obtener la cuenta de un productor determinado
     * @var idProductor  si es diferente de 0 selecciona toda la tabla sino solo el productor
     */
    public function getCuentas() {
        $query = "SELECT * FROM cuenta";

        return DBConnector::ejecutar($query);
    }

    /**
     * obtener el total del saldo de la campanha anterior
     * **/
    public static function getSaldoAnterior($nombre, $apellido, $semillera,$isemilla) {
        $anho_anterior = date('y') - 1;
        $anho_actual = date('y');

        $query = "SELECT SUM(cta.saldo_campanha_anterior) AS saldo_anterior 
		FROM semilla sem 
		INNER JOIN solicitud_semilla t ON t.id_semilla=sem.id_semilla
		INNER JOIN solicitud sol ON sol.id_solicitud=t.id_solicitud
		INNER JOIN semillerista srt ON srt.id_semillerista=sol.id_semillerista
		INNER JOIN semillera slr ON slr.id_semillera=sol.id_semillera
        INNER JOIN cuenta cta ON cta.id_semilla=sem.id_semilla
        WHERE (sem.campanha like '%$anho_anterior' OR sem.campanha like '%$anho_actual') AND sem.id_semilla=$isemilla ";

        #echo $query; exit;
        return DBConnector::ejecutar($query);

    }

    /**
     * total de nro de bolsas de un semillerista en una campanha anterior
     * */

    private function nroBolsasByCultivo($semillera, $nombre, $apellido, $campanha, $cultivo) {
        $query = "SELECT SUM(nro_bolsa) AS nro_bolsas
		          FROM laboratorio lab 
		          INNER JOIN cosecha cos ON cos.id_cosecha = lab.id_cosecha
		          INNER JOIN superficie sup ON sup.id_superficie=cos.id_superficie
		          INNER JOIN solicitud sol ON sol.id_solicitud=sup.id_solicitud
		          INNER JOIN tiene t ON t.id_solicitud=sol.id_solicitud
		          INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla
		          WHERE sol.nombre LIKE '$nombre' AND 
		                sol.apellido LIKE '$apellido' AND 
		                sol.semillera = '$semillera' AND
		                sem.campanha LIKE '%$campanha' AND 
		                sem.cultivo = '$cultivo'";
        DBConnector::ejecutar($query);

        while ($total_nro_bolsas = DBConnector::resultado()) {
            $temp = $total_nro_bolsas -> nro_bolsas;
        }
        return $temp;
    }

    /**
     * etapa de a la que llego en el estado de certificacion
     * */
    private function getEtapaCertificacion($semillera, $nombre, $apellido, $campanha, $cultivo) {
        $query = "SELECT sol.laboratorio
		          FROM solicitud sol 
		          INNER JOIN tiene t ON t.id_solicitud=sol.id_solicitud
		          INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla
		          INNER JOIN superficie sup ON sup.id_solicitud=sem.id_semilla
		          WHERE sem.cultivo = '$cultivo' AND
		                sem.campanha LIKE '%$campanha' AND 
		                sol.semillera = '$semillera' AND 
		                sol.nombre LIKE '$nombre' AND 
		                sol.apellido LIKE '$apellido' AND
		                (sol.laboratorio > 10 OR (sol.laboratorio = 3 
		                                         AND sup.estado = 1 
		                                         AND sup.aprobada = 0)) 
		          LIMIT 1";
        DBConnector::ejecutar($query);

        while ($etapa_certificacion = DBConnector::resultado())
           $temp = $etapa_certificacion -> laboratorio;
        return $temp;
    }

    /**
     * agregar semillera?
     * */
    public static function getCategoriaCampo($isolicitud, $cultivo = '', $campania = '', $variedad = '') {
        $query = "SELECT categoria_producir, cultivo FROM view_semilla ";
        $query .= "WHERE id_solicitud = $isolicitud AND cultivo LIKE '$cultivo%' and campanha LIKE '$campania%' AND variedad LIKE '$variedad%'";
       #echo $query,'||||';
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: $query :: ",DBConnector::mensaje();        
    }

    /**
     * agregar semillera?
     * */
    public static function getCategoriaCampoFiscal($isolicitud, $cultivo = '', $variedad = '') {
        $query = "SELECT categoria_producir, generacion, cultivo FROM view_semilla_fiscal ";
        $query .= "WHERE id_solicitud = $isolicitud AND cultivo LIKE '$cultivo%' AND variedad='$variedad'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * calculo de saldo de la campanha anterior
     * */
    public static function getCostosByCultivo($semillera, $nombre, $apellido, $campanha, $cultivo) {
        $nroBolsas_ = $this -> nroBolsasByCultivo($semillera, $nombre, $apellido, $campanha, $cultivo);
        $etapa_ = $this -> getEtapaCertificacion($semillera, $nombre, $apellido, $campanha, $cultivo);

        $haba_arveja_frejol_soya['inscripcion'] = 8;
        $haba_arveja_frejol_soya['inspeccion'] = 120;
        $haba_arveja_frejol_soya['analisis_etiquetacion'] = 4;

        $papa['inscripcion'] = 8;
        $papa['inspeccion'] = 120;
        $papa['analisis_etiquetacion'] = 2;

        $mani_trigo_cebada['inscripcion'] = 8;
        $mani_trigo_cebada['inspeccion'] = 80;
        $mani_trigo_cebada['analisis_etiquetacion'] = 3;

        $maiz['inscripcion'] = 8;
        $maiz['inspeccion'] = 160;
        $maiz['analisis_etiquetacion'] = 3.5;

        $plantines['analisis_etiquetacion'] = 0.1;

        $acondicionamiento['inscripcion'] = 120;

        $costo = 0;

        if (!strcasecmp($cultivo, 'haba') || !strcasecmp($cultivo, 'arveja') || !strcasecmp($cultivo, 'frejol') || !strcasecmp($cultivo, 'soya')) {
            if ($etapa_ == 11) {
                $costo = $haba_arveja_frejol_soya['inscripcion'] + $haba_arveja_frejol_soya['inspeccion'] + $haba_arveja_frejol_soya['analisis_etiquetacion'] * $nroBolsas_;
            } else {
                $costo = $haba_arveja_frejol_soya['inscripcion'] + $haba_arveja_frejol_soya['inspeccion'];
            }
        }
        if (!strcasecmp($cultivo, 'papa')) {
            if ($etapa_ == 11) {
                $costo = $papa['inscripcion'] + $papa['inspeccion'] + $papa['analisis_etiquetacion'] * $nroBolsas_;
            } else {
                $costo = $papa['inscripcion'] + $papa['inspeccion'];
            }
        }
        if (!strcasecmp($cultivo, 'mani') || !strcasecmp($cultivo, 'trigo') || !strcasecmp($cultivo, 'cebada')) {
            if ($etapa_ == 11) {
                $costo = $mani_trigo_cebada['inscripcion'] + $mani_trigo_cebada['inspeccion'] + $mani_trigo_cebada['analisis_etiquetacion'] * $nroBolsas_;
            } else {
                $costo = $mani_trigo_cebada['inscripcion'] + $mani_trigo_cebada['inspeccion'];
            }
        }
        if (!strcasecmp($cultivo, 'maiz')) {
            if ($etapa_ == 11) {
                $costo = $maiz['inscripcion'] + $maiz['inspeccion'] + $maiz['analisis_etiquetacion'] * $nroBolsas_;
            } else {
                $costo = $maiz['inscripcion'] + $maiz['inspeccion'];
            }
        }
        if (!strcasecmp($cultivo, 'plantines')) {
            $costo = $plantines['analisis_etiquetacion'];
        }
        if (!strcasecmp($cultivo, 'acondicionamiento')) {
            $costo = $acondicionamiento['inscripcion'];
        }

        return $costo;
    }


    /**
     * cuenta por id
     * */
    public static function getCuentaById($id) {
        $query = "SELECT * FROM cuenta WHERE id_cuenta=$id ";

        DBConnector::ejecutar($query);
    }
        /**
     * cuenta por id
     * */
    public static function getCuentaByIdSolicitud($id) {
        $query = "SELECT * FROM view_cuenta ";
        $query .= "WHERE id_solicitud=$id AND id_cuenta=(";
        $query .= "SELECT MAX(id_cuenta) FROM view_cuenta WHERE id_solicitud=$id)";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
    }
    /**
     * devuelve la suma total de las cuotas segun el id de la semilla
     * */
    public static function getSaldoByIdSemilla($id) {
        $query = "SELECT SUM(cuota) AS pagado FROM view_cuota_cuenta WHERE id_semilla=$id ";
#echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: $query :: ",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();
            return $obj->pagado;
        }
        
        
        
    }
    /**
     * verifica la exitencia de una cuenta registrada
     * */
    public static function checkCuentaByIdSemilla($id){
        $query = "SELECT COUNT(*) AS total FROM cuenta WHERE id_semilla = $id";
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: $query :: ",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();
        
        return $obj->total;
        }
        
        
    }
    /**
     * cuenta por id
     * */
    public static function getCuentaByIdPDF($id) {
        $isolicitud = Tiene::getIdSolicitud($id);
        $query = "SELECT * FROM view_cuenta WHERE id_solicitud=$isolicitud ";
#echo $query;
        DBConnector::ejecutar($query);

    }

    /**id de la superficie segun el id de la semilla y el id de solicitud*/
    public static function getIdSuperficieBySolicitudSemilla($idsolicitud, $idsemilla) {
        $query = "SELECT id_superficie FROM view_id_superficie WHERE id_solicitud=$idsolicitud AND id_semilla=$idsemilla";
        #echo $query; exit;
        DBConnector::ejecutar($query);
        $temp = DBConnector::objeto();
        #var_dump(DBConnector::objeto());
        return $temp -> id_superficie;
    }
     
    public function getIdSemillaForCuenta(){
        $query = "SELECT DISTINCT(id_semilla) AS id_semilla ";
        $query .= "FROM view_cuenta";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query::",DBConnector::mensaje();
        }
    }

    /**
     * cultivo  segun nombre y apellido
     * @param string $nombre nombre de semillerista
     * @param string $apellido apellido del semillerista
     *
     * @return cultivo
     * */
    public static function getCultivoCuenta($nombre, $apellido) {
        $query = "SELECT cultivo FROM view_cultivo_variedad WHERE sistema LIKE 'certifica%' AND nombre = '$nombre' AND apellido LIKE '%$apellido%'";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
        $row = DBConnector::objeto();

        return $row -> cultivo;
    }
    /**
     * registro de cuenta
     * */
     public static function getRegistro($id){
         
         $query = "SELECT *";
         $query .= "FROM view_cuenta WHERE id_solicitud=$id";
         
         DBConnector::ejecutar($query);
        # echo DBConnector::mensaje();
     }
     
    /***
     * formatea los datos para su uso en un script sql
     * */
    public function getCuentaSQL() {

        $this -> getCuentas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO cuenta (responsable,semillera,semillerista,campanha,id_variedad,categoria_sembrada,superficie_aprobada,superficie_total,bolsa_etiqueta,subtotal,id_costo,total,fecha,monto_pagado,saldo,saldo_campanha_anterior,estado,id_semilla) VALUES';
            while ($row = DBConnector::resultado()) {

                $query .= "(" . $row -> responsable . ",";
                $query .= $row -> semillera . "," . $row -> semillerista . ",";
                $query .= $row -> campanha . ",";
                $query .= $row -> id_variedad . "," . $row -> categoria_aprobada . ",";
                $query .= $row -> superficie_aprobada . "," . $row -> superficie_total . ",";
                $query .= $row -> bolsa_etiqueta . ",";
                $query .= $row -> subtotal . "," . $row -> id_costo . ",";
                $query .= $row -> total . "," . $row -> fecha . ",";
                $query .= $row -> monto_pagado . "," . $row -> saldo . ",";
                $query .= $row -> saldo_campanha_anterior . "," . $row -> estado ."," . $row->id_semilla ."),\r\n";

            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

} // END
?>