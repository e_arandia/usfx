<?php 
/**
 * clase sistema
 */
class Provincia {
    /**
     * 
     * ingresa una nueva provincia
     * @param $nombre string nombre de la provincia
     * 
     * @return void
     * */
     public static function setProvincia($nombre){
         $nombre_2 = Funciones::search_tilde($nombre);
         $query = "SELECT * FROM provincia";
         if (DBConnector::filas()){
             $row = DBConnector::objeto();
             
             return $row -> id_provincia;
         }else{
            $query = "INSERT INTO provincia VALUES ('".utf8_encode($nombre)."')"; 
            
            DBConnector::ejecutar($query);            
             
            return DBConnector::lastInsertId();   
         }
     }
     
     /**
      * actualiza una provincia 
      * @param $nombre string nombre actualizado de la provincia
      * 
      * @return void
      * */
      public function updProvincia0($id, $new_provincia){
          $query = "UPDATE provincia SET nombre = $new_provincia WHERE $id_provincia = $id";
          
          DBConnector::ejecutar($query);
      }
      
      /**
       * Elimina una provincia
       * @param $id integer numero id de la provincia
       * 
       * @return void
       * */
       public function delProvincia($id){
           $query = "DELETE FROM provincia WHERE id_provincia = $id";
           
           DBConnector::ejecutar($query);
       }
     
	/**
     * @param $sistema string  nombre de la provincia
     * 
     * @return ejecuta la consulta y devuelve el id del sistema
     * */
    public static function getIdProvincia ($provincia){
        $query = "SELECT id_provincia FROM provincia WHERE nombre LIKE '$provincia%'";
        
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::resultado();
        
        return $obj->id_provincia;
    }
    /**
     * devuelve el nombre de la provincai segun el id
     * */    
    public static function getNombreProvincia ($id){
        $query = "SELECT nombre_provincia FROM provincia WHERE id_provincia = $id";
        #echo $query;
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::objeto();
        
        return $obj->nombre_provincia;
    }
    /**
     * lista de provincias
     * */
     public function getProvincia(){
         $query = "SELECT * FROM provincia";
         
         DBConnector::ejecutar($query);
     }
        /**
     * formatea los datos para su uso en un script sql
     * */
     public function getProvinciaSQL(){
         $query = 'INSERT INTO provincia (id_provincia,nombre_provincia,id_departamento) VALUES';
        $this -> getProvincia();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row -> id_provincia.",'".$row->nombre_provincia."',".$row->id_departamento . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
}
 ?>