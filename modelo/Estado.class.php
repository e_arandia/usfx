<?php
/**
 *
 */
class Estado {
    var $estado;
    /**
     * constructor de la clase
     * **/
    public function __construct($estado) {
        $this -> estado = $estado;
    }

    /**
     * inserta una nueva area
     *
     *@param $nombre string   nombre del area
     * */
    public function setEstado() {
        $query = "INERT INTO estado(nombre_estado) VALUES ('$this->estado')";

        DBConnector::ejecutar($query);
    }

    /***/
    public static function delArea($id) {
        $query = "DELETE FROM estado WHERE id_estado = $id";

        DBConnector::ejecutar($query);
    }

    public function updateEstado($new_nombre, $id) {
        $query = "UPDATE estado SET nombre_estado = $nombre_area WHERE id_estado = $id";

        DBConnector::ejecutar($query);
    }

    /**
     * lista todas las areas en el sistema
     * */
    public static function getEstados() {
        $query = "SELECT * FROM estado";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getEstadoSQL() {
        $query = 'INSERT INTO estado (id_estado,nombre_estado) VALUES ';
        $this -> getEstados();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_estado . ",'" . $row -> id_nombre_estado . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>