<?php
/**
 * clase responsable
 */
class Responsable {
    /**
     * agrega un nuevo responsable
     * @param string $nombre  nombre del responsable
     * */
    public static function setReponsable($nombre) {
        #echo $nombre;
        #var_dump(self::checkResponsable($nombre));
        if (self::checkResponsable($nombre)) {
            return self::getIdResponsable($nombre);
        } else {
            $query = "INSERT INTO responsable(nombre) VALUES ('$nombre')";
#echo $query;
            DBConnector::ejecutar($query);

            return DBConnector::lastInsertId();
        }
    }

    /**
     * verifica si existe el responsable
     *
     * @retun bool TRUE: existe   otherwise FALSE
     * */
    private static function checkResponsable($nombre) {
        $query = "SELECT nombre FROM view_responsable WHERE nombre LIKE '$nombre'";

        DBConnector::ejecutar($query);

        if (DBConnector::filas())
            return TRUE;
        else {
            return FALSE;
        }
    }

    /**
     * devuelve el id de un responsable
     *
     * @param string $nombre nombre del responsable
     *
     * @return id de responsable
     * */
    public static function getIdResponsable($nombre) {
        $query = "SELECT id_responsable FROM view_responsable WHERE nombre LIKE '$nombre%'";
#echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError()){
             echo DBConnector::mensaje(),'=',$query;
         }
        $obj = DBConnector::objeto();

        return $obj -> id_responsable;
    }
    
    /**
     * devuelve el nombre de responsable segun id
     * */
     public static function getNombreResponsableById($id){
         $query = "SELECT nombre FROM responsable WHERE id_responsable=$id";
         #echo $query;
         DBConnector::ejecutar($query);
         
         $obj=DBConnector::objeto();
         
         return $obj->nombre;
     }
    
    /**devuelve una lista de responsables segun busqueda
     * @param string $q  contiene cadena de busqueda
     * */
    public static function getListResponsables($q){
        $query = "SELECT nombre FROM view_responsable WHERE nombre LIKE '$q%'";
        
        DBConnector::ejecutar($query);
        
    }
}
?>