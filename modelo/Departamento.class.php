<?php 
/**
 * clase sistema
 */
class Departamento {
    /**
     * 
     * ingresa un nuevo Departamento
     * @param $nombre string nombre del Departamento
     * 
     * @return void
     * */
     public function setDepartamento($nombre){
         $query = "INSERT INTO departamento VALUES ('$nombre')";
         
         DBConnector::ejecutar($query);
     }
     
     /**
      * actualiza un Departamento 
      * @param $nombre string nombre actualizado del Departamento
      * 
      * @return void
      * */
      public function updDepartamento($id, $new_Departamento){
          $query = "UPDATE departamento SET nombre = $new_Departamento WHERE $id_departamento = $id";
          
          DBConnector::ejecutar($query);
      }
      
      /**
       * Elimina un Departamento
       * @param $id integer numero id de Departamento
       * 
       * @return void
       * */
       public function deleteDepartamento($id){
           $query = "DELETE FROM departamento WHERE id_departamento = $id";
           
           DBConnector::ejecutar($query);
       }
     
	/**
     * @param $sistema string  nombre de la Departamento
     * 
     * @return ejecuta la consulta y devuelve el id del sistema
     * */
    public static function getIdDepartamento ($Departamento){
        $query = "SELECT id_departamento FROM departamento WHERE nombre_departamento LIKE '$Departamento%'";
        
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::objeto();
        
        return $obj->id_departamento;
    }
    /**
     * devuelve el nombre del Departamento segun el id
     * */    
    public static function getNombreDepartamento ($id){
        $query = "SELECT nombre_departamento FROM departamento WHERE id_departamento = $id";
        
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::objeto();
        
        return $obj->nombre;
    }
    /**
     * lista de Departamentos
     * */
     public function getDepartamento(){
         $query = "SELECT * FROM departamento";
         
         DBConnector::ejecutar($query);
     }
        /**
     * formatea los datos para su uso en un script sql
     * */
     public function getDepartamentoSQL(){
         $query = 'INSERT INTO departamento (id_departamento,nombre_departamento) VALUES';
        $this -> getDepartamento();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row -> id_departamento.",'".$row->nombre_departamento."'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
}
 ?>