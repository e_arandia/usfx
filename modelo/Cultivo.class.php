<?php
class Cultivo {
    /**
     * Agregar nuevo cultivo
     * */
    private function setCultivo($cultivo) {
        $query = "INSERT INTO cultivo(nombre_cultivo,iniciales) VALUES ('".ucfirst($cultivo)."','".strtoupper(substr($cultivo, 0,2))."')";
#echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query:: ",DBConnector::mensaje();
    }
    /**
     * agrega un cultivo para la opcion plantines
     * */
     public function setCultivoPlantines($cultivo){
         $query = "INSERT INTO cultivo(nombre_cultivo,plantin) VALUES ('$cultivo,1')";
         DBConnector::ejecutar($query);
         if (DBConnector::mensaje())
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje(); 
     }
    /**
     * Verifica la existencia o no de un cultivo y devuelve el id del mismo
     * Agrega el cultivo si este no existe
     * @param string  $cultivo nombre del cultivo
     * */
    public function setCheckCultivo($cultivo) {
        $this -> verificarNombreCultivo($cultivo);
        #echo DBConnector::filas();
        if (DBConnector::filas()) {
            return $this -> getIdCultivo($cultivo);
        } else {
            $this -> setCultivo($cultivo);
            return DBConnector::lastInsertId();
        }
    }

    /**
     * verifica la existencia de un cultivo
     * @param string $cultivo nombre de cultivo
     *
     * @return bool devuelve TRUE si existe caso contrario devuelve FALSE
     * */
    public function verificarNombreCultivo($cultivo) {
        $this -> getNombreCultivo($cultivo);

        if (DBConnector::filas()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * lista todos los cultivos registrados
     * */
    public function getCultivo() {
        $query = "SELECT * FROM cultivo";

        DBConnector::ejecutar($query);
    }

    /**
     * Listar todos los cultivos sin acondicionamiento y plantines
     * */
    public static function getSomeCultivos() {
        $query = "SELECT cultivo FROM view_costos_cultivo WHERE (cultivo <> 'Acondicionamiento' OR cultivo <> 'acondicionamiento') AND (cultivo <>'plantines' OR cultivo<>'Plantines')";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve la lista de cultivos y sus iniciales
     * */
    public static function getInicialesCultivos() {
        $query = "SELECT cultivo,iniciales FROM view_costos_cultivo";

        DBConnector::ejecutar($query);
    }

    /**
     * listar todos los cultivos que tienen vaviedades
     * */
    public static function getFullCultivos() {
        $query = "SELECT cultivo FROM view_costos_cultivo";

        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos
     * */
    public static function getListCultivos() {
        $query = "SELECT DISTINCT(cultivo) AS cultivo FROM view_lst_cultivos ORDER BY cultivo";

        DBConnector::ejecutar($query);
    }

    /**
     * lista de cultivos registrados
     * */
    public static function getCultivos($gestion = '') {
        $query = "SELECT DISTINCT(cultivo) FROM view_detalle_produccion_costos ";
        if ($gestion){
            $query .= "WHERE gestion = '$gestion' ORDER BY cultivo ";
        }else{
            $query .= "ORDER BY cultivo";
        }

        DBConnector::ejecutar($query);
    }

    /**
     * Verifica si existe el cultivo
     * */
    private function getNombreCultivo($cultivo) {
        $query = "SELECT nombre_cultivo FROM cultivo WHERE nombre_cultivo LIKE '$cultivo%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuleve el nombre de cultivo segun el id
     * */
    public static function getNombreCultivoById($id) {
        $query = "SELECT DISTINCT(cultivo) FROM view_cultivo_variedad WHERE id_cultivo=$id";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> cultivo;
    }

    /**
     * devuelve el id del cultivo
     * @param $cultivo string nombre del cultivo
     *
     * @return id del cultivo
     * */
    public static function getIdCultivo($cultivo) {
        $query = "SELECT id_cultivo FROM cultivo where nombre_cultivo='$cultivo'";

        DBConnector::ejecutar($query);
        #echo $query.'='.DBConnector::mensaje();
        $obj = DBConnector::objeto();
        if(DBConnector::nroError() || !DBConnector::filas()){
            echo "ERROR ::".__METHOD__."::$query",DBConnector::mensaje();
        } 
        return $obj -> id_cultivo;
    }

    /**
     * devuelve el id del cultivo para importacion
     * @param $cultivo string nombre del cultivo
     *
     * @return id del cultivo
     * */
    public static function getIdCultivoImportar($cultivo) {
        if (empty($cultivo)) {
            $query = "SELECT id_cultivo FROM cultivo WHERE iniciales = 'NN'";
            
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            
            return $obj -> id_cultivo;
        } else {
            $cultivo_2 = Funciones::search_tilde($cultivo);
            $query = "SELECT id_cultivo FROM cultivo WHERE nombre_cultivo LIKE '$cultivo_2'";
            DBConnector::ejecutar($query);
            if (DBConnector::filas()) {
                $obj = DBConnector::objeto();
                return $obj -> id_cultivo;
            } else {
                $query = "INSERT INTO cultivo (nombre_cultivo) VALUES ('" . utf8_decode($cultivo) . "')";
                DBConnector::ejecutar($query);
                return DBConnector::lastInsertId();
            }
        }
    }

    /**
     * Eliminar cultivo
     * */
    public function delCultivo($id) {
        $query = "DELETE FROM cultivo WHERE id_cultivo=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de un cultvio segun el id de costo al cual esra enlazado
     * */
    public function getIdCultivoByIdCosto($id) {
        $query = "SELECT id_cultivo FROM costo WHERE id_costo=$id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_cultivo;

    }

    /**
     * Modificar nombre de cultivo
     * */
    public function updCultivo($id, $new_cultivo) {
        $query = "UPDATE cultivo SET nombre_cultivo = $new_cultivo WHERE id_cultivo=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el cultivo segun la variedad
     * incluido descanso de cultivo
     * */
    public static function getNombreCultivoByIdVariedad($id) {
        $query = "SELECT cultivo FROM view_lst_cultivo_variedad WHERE id_cultivo=$id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> cultivo;
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getCultivosSQL() {
        $query = 'INSERT INTO cultivo (id_cultivo,nombre_cultivo) VALUES ';
        $this -> getCultivo();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_cultivo . ",'" . $row -> nombre_cultivo . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>