<?php
/**
 * Clase  para la campanhas de siembra
 */
class Campanha {
    /**
     * devuleve el id de la campanha
     * @param string $campanha  nombre de la campanha
     * */
    public static function getIdCampanha($campanha) {
        $query = "SELECT id_campanha FROM campanha WHERE nombre_campanha LIKE '$campanha'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::asociativo();

        return $obj['id_campanha'];
    }
    /**
     * devuleve el id de la campanha
     * @param string $campanha  nombre de la campanha
     * */
    public static function getIdCampanhaImportar($campanha) {
        if (empty($campanha)){
            $query = "SELECT id_campanha FROM campanha ";
            $query .= "WHERE nombre_campanha = 'desconocida'";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            
            return $obj->id_campanha;
        }else{
            $temp = explode("/",$campanha);
        $query = "SELECT id_campanha FROM campanha WHERE nombre_campanha = \"".trim($temp[0])."\"";
        DBConnector::ejecutar($query);
        #echo $query,'?';
        if (DBConnector::filas()){
            $obj = DBConnector::objeto();
            
            return $obj->id_campanha;
        }else{
            $query = "INSERT INTO campanha (nombre_campanha) VALUES (\"".trim($temp[0])."\")";
            DBConnector::ejecutar($query);
            
            return DBConnector::lastInsertId();
        }
        }
    }

    /**
     * devuelve todas las campanhas
     * */
    public function getCampanhas() {
        $query = "SELECT * FROM campanha";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getCampanhaSQL() {
        $query = 'INSERT INTO campanha (id_campanha,nombre_campanha) VALUES ';
        $this -> getCampanhas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_campanha . ",'" . $row -> nombre_campanha . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>