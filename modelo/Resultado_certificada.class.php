<?php
class Resultado_certificada{
    /**
     * lista todos los resultados de muestras
     * */
     public function getResultadoCertificada(){
         $query = "SELECT * FROM resultado_certificada";
         
         DBConnector::ejecutar($query);
     }
     
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getResultadoCertificadaSQL() {
        $query = 'INSERT INTO resultado_certificada () VALUES ';
        $this -> getResultadocertificada();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_certificada . ",". $row->muestra . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
    
    
    /**
     * ingresa id de  resultado en la tabla de relacion
     * */
     public static function setResultadoCertifica($muestra,$resultado){
         $query = "INSERT INTO resultado_certificada (id_certificada,id_muestra) VALUES ($resultado,$muestra)";
         
         DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
     }
}
?>