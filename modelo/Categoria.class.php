<?php
/**
 * Categoria
 *
 * @package
 * @author iniaf
 * @copyright Edwin W. Arandia Zeballos
 * @version 2011
 * @access public
 */
class Categoria {
    /**
     * Inserta una nueva categoria
     */
    public function setCategoria($categoria) {
        $query = "INSERT INTO categoria (nombre_categoria) VALUES ('$categoria')";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * Recupera todas las categorias de la Base de datos
     */
    public function getCategorias() {
        $query = "SELECT * FROM categoria";

        DBConnector::ejecutar($query);
    }

    /**
     * Lista todas las categorias y generaciones
     */
    public function listarCategorias() {
        $query = "SELECT * FROM view_lst_categoria_generacion";

        DBConnector::ejecutar($query);
    }

    /**
     * Lista las generaciones de  la categoria a buscar
     * @param string $categoria  nombre de la categoria
     */
    public static function listarCategoriasByCategoria($categoria='') {
        $query = "SELECT * FROM view_lst_categoria_generacion ";
        if (!empty($categoria))
            $query .= "WHERE categoria LIKE '$categoria%'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR cargando lista de categorias ',DBConnector::mensaje();
    }

    /**
     * Lista todas las categorias y generaciones
     * @param int    $id     numero de identificacion
     */
    public static function list_CategoriaWithoutCat($id) {
        $query = "SELECT categoria,generacion,id_generacion ";
        $query .= "FROM view_lst_categoria_generacion ";
        $query .= "WHERE id_generacion >= $id LIMIT 3";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * lista de todas las categorias registradas y con al menos una generacion
     * */
    public static function getFullListCategorias() {
        $query = "SELECT categoria FROM view_lst_categoria_generacion";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de la categoria
     * @param string $nombre nombre de la categoria
     * **/
    public static function getIdCategoriaByName($categoria) {
        $query = "Select id_categoria FROM view_lst_categoria_generacion 
                  WHERE categoria= '$categoria'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_categoria;
    }

    /**
     * devuelve el id de la categoria
     * @param string $nombre nombre de la categoria
     * **/
    public static function getIdCategoriaByNameAndGeneracion($categoria, $generacion) {
        $query = "Select id_categoria FROM view_lst_categoria_generacion 
                  WHERE categoria= '$categoria' AND inicial='$generacion'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_categoria;
    }

    /**
     * devuelve el id de la categoria
     * @param string $nombre nombre de la categoria
     * **/
    public static function getIdCategoriaByNameAndGeneracionImportar($categoria) {
        if (empty($categoria)){
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE 'sin%' AND generacion = 'ninguna' ";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            
            return $obj -> id_categoria;
        }
        $filtro = array("SEL","GEN","PREBAS");
        if (in_array(strtoupper($categoria), $filtro)) {
            $substr_categoria = substr($categoria, 0,(strlen($categoria)-1));
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE '".strtolower($categoria)."%'";
            #echo '$$',$query,'$$';
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
                exit;
            }else{
                $obj = DBConnector::objeto();
                return $obj -> id_categoria;
            }                        
        } else {
            $temp = explode("-", $categoria);
            if (count($temp) == 2) {
                $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%' AND inicial='" . trim($temp[1]) . "'";
            } else {
                $query = "SELECT MAX(id_categoria) AS id_categoria FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%'";
            }
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            #echo $obj -> id_categoria;
            return $obj -> id_categoria;
        }
    }
    /**
     * devuelve el id de la categoria
     * @param string $nombre nombre de la categoria
     * **/
    public static function getIdCategoriaByNameAndGeneracionHojaCosechaImportar($categoria) {
        if (empty($categoria)){
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE 'sin%' AND generacion = 'ninguna' ";
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "ERROR:: ".__METHOD__."::$query OPCION EMPTY()";
            }else{
                $obj = DBConnector::objeto();
                return $obj -> id_categoria;    
            }
            
        }
        $filtro = array("SEL","CER","BAS","REG");
        if (in_array(strtoupper($categoria), $filtro)) {
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE '$categoria%'";
            #echo $query;
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "ERROR:: ".__METHOD__."::$query IN_ARRAY()";
            }else{
                $obj = DBConnector::objeto();
                return $obj -> id_categoria;    
            }
        } else {
            $temp = explode("-", $categoria);
            if (count($temp) == 2) {
                $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%' AND inicial='" . trim($temp[1]) . "'";
            } else {
                $query = "SELECT MAX(id_categoria) AS id_categoria FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%'";
            }
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "ERROR:: ".__METHOD__."::$query OPCION NO IN ARRAY()";
            }else{
                $obj = DBConnector::objeto();
                return $obj -> id_categoria;    
            }
        }
    }
    /**
     * devuelve el porcentaje de cada categoria
     * */
     public static function getListPorcentajeCategorias($filtro=TRUE){
         $query = "SELECT nombre_categoria FROM view_lst_categoria_porcentaje ";
         if ($filtro){
             $query .= "WHERE nombre_categoria NOT IN ('Sin especificar','Seleccionada','Generica','PreBasica','Rechazada')";
         }
         
         DBConnector::ejecutar($query);
         
         
     }
     public static function getCategoriaPorcentaje($categoria){
         
         $query = "SELECT humedad,germinacion,pureza FROM view_lst_categoria_porcentaje ";
         $query .= "WHERE nombre_categoria = '".strtolower($categoria)."'";
         
         DBConnector::ejecutar($query);
     } 
    /**
     * nombre de la categoria segun id
     * */
    public static function getCategoriaById($id) {
        $query = "SELECT categoria";
        $query .= " FROM view_lst_categoria_generacion";
        $query .= " WHERE id_categoria=$id";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> categoria;
    }

    /**
     * id de la categoria segun id de la generacion
     * */
    public static function getCategoriaByIdGeneracion($id) {
        $query = "SELECT DISTINCT(id_categoria)";
        $query .= " FROM view_lst_categoria_generacion";
        $query .= " WHERE id_generacion=$id";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_categoria;
    }

    /**
     * id de la categoria segun id de la generacion
     * */
    public static function getNombreCategoriaByIdGeneracion($id) {
        $query = "SELECT categoria";
        $query .= " FROM view_lst_categoria_generacion";
        $query .= " WHERE id_generacion=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> categoria;
    }

    /**
     * selecciona la categoria si existe
     * @param string $nombre nombre de la categoria
     * */
    public function getCategoriaByName($nombre) {
        $query = "SELECT categoria";
        $query .= " FROM view_lst_categoria_generacion";
        $query .= " WHERE categoria=$nombre";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getCategoriasSQL() {
        $this -> getCategorias();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO categoria (id_categoria,categoria,pureza,germinacion,humedad) VALUES';
            while ($row = DBConnector::objeto()) {
                $query .= "(";
                $query .= $row -> id_categoria . ",'" . $row -> nombre_categoria . "',";
                $query .= $row -> pureza . "," . $row -> germinacion . "," . $row -> humedad . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>