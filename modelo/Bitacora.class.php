<?php
/**
 * @package Bitacora
 * @author Edwin W. Arandia Zeballos
 * @copyright iniaf
 * @version 0.3
 */
class Bitacora{
	/**
	 *  lista todos accesos al sistema ordenados por fecha descendiente
	 */
	public function getBitacora() {
		$query = "SELECT * FROM bitacora ORDER BY fecha_ingreso DESC";

		DBConnector::ejecutar($query);
	}
    
    /**
     * Lista los accesos al sistema en bloque
     * @param int $inicio numero de inicio 
     * @param int $nroRegistros  cantidad de registros a mostrar DEFAULT:15
     * 
     * @return $query;
     * */
     public function getBitacoraByBlock($inicio, $nroRegistros=15){
        $query = "SELECT * FROM bitacora ORDER BY fecha_ingreso DESC LIMIT $inicio,$nroRegistros";

        return $query;         
     }
    
    /**
     * actualizar contador de nuevo formulario
     * */
     public function updateAgregar($usuario){
         $query = "UPDATE bitacora SET agregar = agregar+1 WHERE usuario='".$usuario."' and sesion='Abierta'" ;
         return $query;
     }
     /**
     * actualizar nro de ediciones realizadas
     * */
     public function updateEditar($usuario){
         $query = "UPDATE bitacora SET editar = editar+1 WHERE usuario='".$usuario."' and sesion='Abierta'" ;
         return $query;
     }
     /**
     * actualizar nro de formularios borrados
     * */
     public function updateEliminar($usuario){
         $query = "UPDATE bitacora SET eliminar = eliminar+1 WHERE usuario='".$usuario."' and sesion='Abierta'" ;
         return $query;
     }

	/**
	 *  obtiene los datos de la bitacora para realizar el respaldo de la tabla
     * 30-09-2012
	 */
	public function getRespaldarBitacora() {
		$query = "SELECT * FROM bitacora";

		return $query;
	}

	/**
	 * Ingresa los datos del usuario en la bitacora
	 */
	public static function setIngreso($usuario) {
		date_default_timezone_set('America/La_Paz');
		$temp = new Browser;
		$browser = $temp -> getBrowser() . ' ' . $temp -> getVersion();
		$query = "INSERT INTO bitacora (usuario,fecha_ingreso,sesion,navegador,ip_address,id_login)
        VALUES('$usuario','" . date('Y-m-d H:i:s') . "','Abierta','$browser','".$_SERVER['REMOTE_ADDR']."')";
        //echo $query; exit;  
        
		DBConnector::ejecutar($query);
	}

	/**
	 * Guarda la fecha y hora de salida
	 */
	public static function setSalida($usuario) {
		date_default_timezone_set('America/La_Paz');
		$temp = new Browser;
		$browser = $temp -> getBrowser() . ' ' . $temp -> getVersion();
		$query = "UPDATE bitacora SET fecha_salida='" . date('Y-m-d H:i:s') . "',duracion=TIMEDIFF(fecha_salida,fecha_ingreso),sesion='Cerrada'
                   WHERE (sesion='Abierta' AND usuario='$usuario' 
                   AND ip_address='" . $_SERVER['REMOTE_ADDR'] . "'AND navegador='" . $browser . "')";
		DBConnector::ejecutar($query);
	}
    
    /**
     * formatea los datos para su uso en un script sql
     * */
     public function getBitacoraSQL(){
         $query = 'INSERT INTO bitacora (id_bitacora,ip_address,usuario,fecha_ingreso,fecha_salida,duracion,navegador,sesion,id_login) VALUES';
        $this -> getBitacora();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row->id_bitacora.",'";
                $query .= $row -> ip_address . "','";
                $query .= $row -> usuario . "','" . $row -> fecha_ingreso . "','" . $row -> fecha_salida . "','";
                $query .= $row -> duracion . "','" . $row -> navegador . "','" . $row -> sesion . "',";
                $query .= $row -> id_login . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
    
}
?>