<?php
class Muestra_laboratorio{
    
    public static function getGestionesAnteriores(){
        $query = "SELECT gestion FROM view_muestras_laboratorio ";
        $query .= "WHERE gestion < ".date('Y');
        #echo $query;
        DBConnector::ejecutar($query);
    }
    public static function getLastIdMuestra(){
        $query = "SELECT MAX(id_muestra)+1 AS id_muestra FROM muestra_laboratorio";
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();
        $muestra = $obj->id_muestra;
        if (strlen($muestra) < 10)
           $res = str_pad($muestra, 3, '0', STR_PAD_LEFT);
        if (strlen($muestra) > 10 && strlen($muestra) < 100) 
            $res = str_pad($valor, 2, '0', STR_PAD_LEFT);
        
        return $res;        
    }
    /**
     * lista todos los tipos de semilla
     * */
     public function getMuestraLaboratorio(){
         $query = "SELECT * FROM muestra_laboratorio";
         
         DBConnector::ejecutar($query);
     }
     /**
     * lista todos los tipos de semilla
      * @param  integer  $id   id de semilla
     * */
     public static function getMuestraLaboratorioByIdSemilla($id){
         $query = "SELECT * FROM muestra_laboratorio ";
         $query .= "WHERE nro_campo = $id";
         #echo $query;
         DBConnector::ejecutar($query);
         if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__."::$query::",DBConnector::mensaje();
     }
     /**
      * obtiene el id de analisis segun pureza,germinacion y humedad
      * */
      public static function getAnalisisLaboratorioImportar($pureza,$germinacion,$humedad){
          if ($germinacion > 0.0 && $pureza > 0.00 && $humedad >0.00){
              $query = "SELECT id_analisis FROM tipo_analisis ";
              $query .= "WHERE germinacion = 1 AND humedad = 1 AND pureza = 1";
          }else {// solo germinacion y pureza
              $query = "SELECT id_analisis FROM tipo_analisis ";
              $query .= "WHERE germinacion = 1 AND humedad = 0 AND pureza = 1";
          }
          #echo $query;
          DBConnector::ejecutar($query);
          $obj = DBConnector::objeto();
          
          return $obj->id_analisis;
      }
     /**
      * insert muestra de laboratorio
      * */
      public function setMuestraLaboratorio($muestra){
          $query = "INSERT INTO muestra_laboratorio (fecha_recepcion,nro_campo,origen,analisis,sistema,tipo_semilla) ";
          $query .= "VALUES ('".$muesra['recepcion']."',".$muestra['nro_campo'].",'".$muestra['origen']."',".$muestra['analisis'].",".$muestra['sistema'].",".$muestra['tipo'].")";
          
          DBConnector::ejecutar($query);
      }
      /**
      * insert muestra de laboratorio para importar
       * @param  date     $recepcion    fecha de recepcion 
       * @param  char     $nro_campo    numero de campo de muestra
       * @param  string   $origen       nombre origen de muestra
       * @param  integer  $analisis     tipo de analisis a realizar [germinacion|humedad|pureza]   
      * */
      public static function setMuestraLaboratorioImportar($nro_analisis,$recepcion,$nro_campo,$origen,$analisis,$sistema){
          $query = "INSERT INTO muestra_laboratorio (nro_analisis,fecha_recepcion,nro_campo,origen,analisis,sistema) ";
          $query .= "VALUES ($nro_analisis,'$recepcion',$nro_campo,'$origen',$analisis,$sistema)";
          DBConnector::ejecutar($query);
          if (DBConnector::nroError()){
              echo DBConnector::mensaje(),$query;
              exit;
          }
      }
      /**
      * insert muestra de laboratorio para importar
       * @param  date     $recepcion    fecha de recepcion 
       * @param  char     $nro_campo    numero de campo de muestra
       * @param  string   $origen       nombre origen de muestra
       * @param  integer  $analisis     tipo de analisis a realizar [germinacion|humedad|pureza]   
      * */
      public static function setMuestraLaboratorioFiscalImportar($nro_analisis,$recepcion,$nro_campo,$origen){
          $query = "INSERT INTO muestra_laboratorio (nro_analisis,fecha_recepcion,nro_campo,origen) ";
          $query .= "VALUES ($nro_analisis,'$recepcion',$nro_campo,'$origen')";
          DBConnector::ejecutar($query);
          if (DBConnector::nroError()){
              echo DBConnector::mensaje(),$query;
              exit;
          }
      }
    /**
     * retorna el id de la muestra segun el nro de campo
     * */
     public static function getIdMuestraByNroSolicitud($campo){
        $query = "SELECT MAX(nro_analisis) AS nro_analisis ";
        $query .= "FROM view_muestras_laboratorio ";
        $query .= "WHERE nro_campo=$campo";   
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();        
            return $obj->nro_analisis;    
        }        
     }
     /**
      * devuleve el id del analisis segun el numero de analisis
      * */
      public static function getIdMuestraByNroAnalisis($nro){
          $query = "SELECT nro_analisis ";
          $query .= "FROM view_resultado_muestra_certificada ";
          $query .= "WHERE analisis = $nro";
          
          DBConnector::ejecutar($query);
          $obj = DBConnector::objeto();
          return $obj->nro_analisis;
      }
     /**
      * total de muestras segun area
      * @param int  area   1:certificacion, 2:fiscalizacion
      * */
      public function getTotalMuestrasByArea($area){
          $query = "SELECT COUNT(*) AS total FROM muestra_laboratorio WHERE sistema = $area";
          
          DBConnector::ejecutar($query);
          
          $row = DBConnector::objeto();
          
          return $row->total;
      }
      /**
      * total de muestras segun area
      * @param int  $area   1:certificacion, 2:fiscalizacion
      * @param int  $estado   0:sin resultado, 1:aprobada, 2:rechazada,  3:fiscalizada
      * */
      public function getTotalMuestrasAprobadasByArea($area,$desde,$hasta,$estado){
          $query = "SELECT COUNT(*) AS total FROM view_muestras_laboratorio ";
          $query .= "WHERE sistema = $area AND estado=$estado AND analizado=1 ";
          if (in_array($desde,array("01","04","07","10"))){
             $query .= "AND (mes_recepcion >='$desde' AND mes_recepcion <='$hasta')";
             
         }else{
             $query .= "AND (fecha_recepcion >='$desde' AND fecha_recepcion <='$hasta')";
         }
          
          DBConnector::ejecutar($query);
          #echo DBConnector::mensaje();
          if (DBConnector::filas()){
              $row = DBConnector::objeto();
          
            return $row->total;
          }else{
              return 0;
          }          
      }
      
    
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getMuestraLaboratorioSQL() {
        $query = 'INSERT INTO muestra_laboratorio (id_muestra,fecha_recepcion,nro_campo,origen,analisis,sistema,estado) VALUES ';
        $this -> getMuestraLaboratorio();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_muestra . ",'";
                $query .= $row -> fecha_recepcion ."','".$row->nro_campo."','";
                $query .= $row -> origen . "','". $row->analisis ."',";
                $query .= $row->sistema.",".$row->estado. "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>