<?php
/**
 *
 * @author Edwin Willy Arandia Zeballos
 */
class SemillaProd {
    /**
     * ingresa una nueva semilla producida
     *
     * @param  varchar 		nroCampo
     * @param  varchar 		categoria
     * @param  varchar 		generacion
     * @param  varchar 		semillaNeta
     * @param  varchar 		categoriaObtenida
     * @param  smallint   	nroEtiqueta
     * @param  varchar 		rango
     * @param  mediumint	idCosecha
     */
    public function setSemillaProd($nroSolicitud, $nroCampo, $idCategoria, $generacion, $semillaNeta, $categoriaObtenida, $nroEtiqueta, $rango) {
        $query = "INSERT INTO semilla_producida (nro_solicitud,nro_campo,categoria_obtenida,id_generacion,semilla_neta,nro_etiqueta,rango)
		VALUES ($nroSolicitud,$nroCampo,$idCategoria,$generacion,$semillaNeta,$nroEtiqueta,'$rango')";
        #echo $query;
        DBConnector::ejecutar($query);

        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        }

    }

    public static function setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $categoria, $nro_etiqueta, $rango) {
        $categoria_valor = (empty($categoria))? 9 :$categoria;
        $query = "INSERT INTO semilla_producida (nro_solicitud,nro_campo,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES (";
        $query .= "$isolicitud,$iSemilla,$semilla_neta,$categoria_valor,$nro_etiqueta,'$rango')";

        DBConnector::ejecutar($query);
        /*
         if (empty($igeneracion)&&empty($categoria)){
         echo "igeneracion :".$igeneracion.'=',"categoria :".$categoria.'???';
         }
         */
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '?'.__METHOD__.'?', $query,'?????????';
            echo "ISOLICITUD:$isolicitud ?? ISEMILLA:$iSemilla ?? SEMILLA_NETA:$semilla_neta ?? CATEGORIA :$categoria ?? NRO-ETIQUETA:$nro_etiqueta ?? RANGO:$rango";
            exit ;
        }

    }

    public function setSemillaProdFiscal($semillaNeta, $categoriaObtenida, $nroEtiqueta, $icosecha, $ilaboratorio) {
        $query = "INSERT INTO semilla_producida (semilla_neta,categoria_obtenida,nro_etiqueta,id_cosecha,id_laboratorio) 
        VALUES ($semillaNeta,'$categoriaObtenida',$nroEtiqueta,$icosecha, $ilaboratorio)";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    public static function updateSemillaProd($nroCampo, $categoria, $generacion, $semillaNeta, $categoriaObtenida, $nroEtiqueta, $rango, $idSemilla, $link) {
        $query = "UPDATE semilla_producida SET nro_campo='$nroCampo',categoria='$categoria',
		generacion='$generacion',semilla_neta=$semillaNeta,categoria_obtenida='$categoriaObtenida',
		nro_etiqueta=$nroEtiqueta,rango='$rango',id_cosecha=$idCosecha 
		WHERE id_semillaProd=$idSemilla";

        mysql_query($query, $link);
    }

    public function deleteSemillaProd($id) {
        $query = "DELETE FROM semilla_producida WHERE id_semillaProd=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * @param   string  $categoria           nombre de categoria obtenida
     * @param   strinf  $categoria_cosecha   nombre de categoria en hoja de cosecha
     * */
    public static function getCategoriaObtenidaImportar($categoria, $categoria_cosecha) {
        if (($categoria == '') && (strtolower($categoria_cosecha) == 'rechazada' || strtolower($categoria_cosecha) == 'rechazado')) {
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria='sin especificar' AND generacion='ninguna'";
            
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()){
                echo "ERROR :: ".__METHOD__."::";
            }else{
                $obj = DBConnector::objeto();
                return $obj -> id_categoria;
            }

        } else{
            return Generacion::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($categoria);
        }
            
    }

    /**
     * @param   string  $categoria           nombre de categoria obtenida
     * @param   strinf  $categoria_cosecha   nombre de categoria en hoja de cosecha
     * */
    public static function getCategoriaObtenidaFiscalImportar($categoria) {

        if ($categoria == '' || strtolower($categoria) == 'rechazada' || strtolower($categoria) == 'rechazado') {
            $query = "SELECT id_categoria FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria='sin especificar' AND generacion='ninguna'";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            return $obj -> id_categoria;

        } else
            return Generacion::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($categoria);
    }

    /**
     * devuelve el numero de generacion segun la categoria
     * */
    public static function getGeneracionCategoriaObtenidaImportar($categoria) {
        if ($categoria != '') {
            $temp = explode("-", $categoria);
            if (count($temp) > 1) {
                $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim(strtolower($temp[0])) . "%' AND inicial LIKE '" . trim($temp[1]) . "'";
            } else {
                if (strtolower($temp[0]) != "rechazada" || strtolower($temp[0]) != "rechazado") {
                    $query = "SELECT id_generacion,MIN(inicial) FROM view_lst_categoria_generacion ";
                    $query .= "WHERE categoria LIKE '" . trim(strtolower($temp[0])) . "%' ";
                } else {
                    $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
                    $query .= "WHERE categoria LIKE 'Rechazada' AND generacion = 'rechazada'";
                }
            }
        } else {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE 'Sin Especificar' AND generacion = 'Ninguna'";
        }
        #echo $query,'???';
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_generacion;
    }

    public static function getGeneracionCategoriaObtenidaFiscalImportar($categoria) {
        if (empty($categoria)) {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria LIKE 'Sin Especificar' AND generacion = 'Ninguna'";
        } elseif ($categoria == 'rechazada' || $categoria == 'rechazado') {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria='sin especificar' AND generacion='ninguna'";
        } else {
            $temp_categoria = explode("-", $categoria);
            if (count($temp_categoria) > 1) {
                $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp_categoria[0]) . "%' AND inicial = '" . trim($temp_categoria[1]) . "'";
            } else {
                $query = "SELECT MIN(id_generacion) AS id_generacion FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp_categoria[0]) . "%'";
            }

        }
        #echo $query,'=';
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
            exit ;
        }
        $obj = DBConnector::objeto();

        return $obj -> id_generacion;
    }

    public static function getEtiquetaByVariedadProductor($cat_sembrada, $cat_obtenida, $semillera) {
        $query = "SELECT nro_etiqueta FROM view_semilla_producida ";
        $query .= "WHERE semillera='$semillera' AND categoria_sembrada=$cat_sembrada AND categoria_obtenida=$cat_obtenida";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> nro_etiqueta;
        else {
            return '0';
        }
    }
    //devuelve la cantidad de etiquetas utilizadas para el cultivo
    public static function getEtiquetaByIdSolicitud($idSolicitud) {
        $query = "SELECT nro_etiqueta FROM view_semilla_producida ";
        $query .= "WHERE id_solicitud = $idSolicitud";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> nro_etiqueta;
        else {
            return '0';
        }
    }

    /**
     * devuelve el id de la generacion segun la categoria obtenida
     * */
    public static function getGeneracionImportar($categoria) {
        $new_categoria = strtolower($categoria);
        if ($new_categoria == '') {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria = 'Sin especificar' AND generacion='ninguna'";
        } elseif ($new_categoria == 'rechazada' || $new_categoria == 'rechazado') {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE categoria = 'Rechazada' AND generacion='rechazada'";
        } else {
            $temp = explode('-', $new_categoria);
            if (count($temp) > 1) {
                $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%' AND inicial LIKE '" . trim($temp[1]) . "%'";
            } else {
                $query = "SELECT MIN(id_generacion) FROM view_lst_categoria_generacion ";
                $query .= "WHERE categoria LIKE '" . trim($temp[0]) . "%'";
            }
        }
        #echo $query.'=';exit;

        DBConnector::ejecutar($query);
        if (!DBConnector::nroError()) {
            if (DBConnector::objeto() != NULL) {
                $obj = DBConnector::objeto();
                return $obj -> id_generacion;
            } else {
                return '999';
                exit ;
            }
        } else {
            echo DBConnector::mensaje() . '=' . $query;
            exit ;
        }
    }

    /**
     * devuelve el id de la semilla producida
     * segun el id de la semilla
     * */
    public function getIdSemillaProducida($idsemilla) {
        if ($idsemilla != '-' && $idsemilla != '') {
            $query = "SELECT id_semillaProd FROM semilla_producida ";
            $query .= "WHERE nro_campo = $idsemilla";
#echo $query;
            DBConnector::ejecutar($query);
            if (DBConnector::nroError()) {
                echo DBConnector::mensaje(), '=', $query;
                return 0;
            } else {
                if (DBConnector::filas() == 0) {
                    return 0;
                } else {
                    $obj = DBConnector::objeto();
                    return $obj -> id_semillaProd;
                }
            }
        } else {
            return 0;
        }
    }

    /**
     * Devuelve el nro de etiqueta para importacion
     * */
    public static function getNroCampoImportar($nro_campo) {
        /*
         if($nro_campo== ""){
         $query = "SELECT id_semilla FROM view_lst_semilla_full ";
         $query .= "WHERE nro_campo = 0";
         }else{
         $query = "SELECT id_semilla FROM view_lst_semilla_full ";
         $query .= "WHERE nro_campo = $nro_campo";
         }

         DBConnector::ejecutar($query);
         $obj = DBConnector::objeto();

         return $obj->id_semilla;
         */
        return 'SemillaProd::getNroCampoImportar';
    }

    /**
     * Devuelve el nro de etiqueta para importacion
     * */
    public static function getNroEtiquetasImportar($nro_etiqueta) {
        if ($nro_etiqueta == "") {
            return 0;
        } else {
            return $nro_etiqueta;
        }
    }

    /**
     * nro de solicitud
     * */
    public static function getNroSolicitudImportar($nro) {
        /*
         $query = "SELECT id_solicitud FROM view_solicitudes_certificacion ";
         $query .= "WHERE nro_solicitud = '$nro'";
         #echo $query;
         DBConnector::ejecutar($query);
         $obj = DBConnector::objeto();

         return $obj -> id_solicitud;
         */
        return 'SemillaProd::getNroSolicitudImportar';
    }

    /**
     * total de etiquetas
     * */
    public function getTotalEtiquetas() {
        $query = "SELECT COUNT(nro_etiqueta) AS etiquetas FROM semilla_producida sp ";
        $query .= "INNER JOIN solicitud sol ON sol.id_solicitud = sp.nro_solicitud";

        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            $row = DBConnector::objeto();

            return $row -> etiquetas;
        } else {
            return 0;
        }

    }

    /**
     * consulta de semillas producidas por un determinado productor
     * si especifica su id, sino de todos los productores
     */
    public function getSemillaProd($productorID = '', $idParcela = '', $idCosecha = '') {
        if (!empty($productorID) && !empty($idParcela) && !empty($idCosecha))
            $query = "SELECT * FROM view_laboratorio
                WHERE cos.laboratorio=1 OR sem.cultivo='papa'";
        else
            $query = "SELECT * FROM view_semilla_producida_fiscal";
        //echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * semilla producida en certificacion de la gestion actual
     * */
    public function getSemillaProducida() {
        $query = "SELECT * FROM view_semilla_producida WHERE gestion = '" . date('Y') . "' ";
        $query .= "ORDER BY id_solicitud DESC";
        DBConnector::ejecutar($query);

        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        }
    }

    /**
     * rango de etiquetas para importar
     * */
    public static function getRangoImportar($rango) {
        if ($rango == "") {
            return 0;
        } else {
            return $rango;
        }
    }
    /**
     * dewvuleve el siguiente numero de rango
     * */
     public function getRangoCertifica(){
         $query = "SELECT MAX(rango) FROM view_semilla_producida ";
         $query .= "WHERE gestion = '".date('Y')."'";
         DBConnector::ejecutar($query);
         if(DBConnector::nroError()){
             echo "ERROR ::".__METHOD__."::$query",DBConnector::mensaje();
         }else{
             $obj = DBConnector::objeto();
             if($obj->rango){
                 return $obj->rango; 
             }else{
                 return 1000;
             }
         }
     }

    public static function getRegistro($id) {
        $query = "SELECT categoria_sembrada,generacion,semilla_neta,categoria_obtenida,generacion_obtenida,";
        $query .= "nro_etiqueta,rango FROM view_semilla_producida WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * registro de fiscalizacion
     * */
    public static function getRegistroFiscal($id) {
        $query = "SELECT cultivo,categoria_obtenida,nro_etiqueta  FROM view_semilla_producida_fiscal WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * filtro de semilla producida para certificacion
     * */
    public static function getSemillaProdCertificaFilter($search = '') {
        $query = "SELECT semillera,CONCAT(nombre,' ',apellido) AS semillerista,nro_campo,categoria_sembrada,generacion,semilla_neta,";
        $query .= "categoria_obtenida,generacion_obtenida,nro_etiqueta,rango FROM view_semilla_producida ";
        $query .= "WHERE semillera LIKE '%$search%' OR nombre LIKE '$search%' OR  apellido LIKE '$search%' OR nro_campo LIKE '$search%' OR categoria_sembrada  LIKE '$search%' OR ";
        $query .= "generacion LIKE '$search%' OR semilla_neta LIKE '$search%' OR categoria_obtenida LIKE '$search%' OR ";
        $query .= "generacion_obtenida LIKE '$search%' OR nro_etiqueta LIKE '$search%' OR rango LIKE '$search%'";

        DBConnector::ejecutar($query);
    }
    
    /**
     * filtro de semilla producida para fiscalizacion
     * */
    public static function getSemillaProdFiscalFilter($search) {
        $query = "SELECT semillera,CONCAT(nombre,' ',apellido) AS semillerista,nro_campo,semilla_neta,categoria_obtenida,nro_etiqueta FROM view_semilla_producida_fiscal ";
        $query .= "WHERE semillera LIKE '$search%' OR nombre LIKE '$search%' OR  apellido LIKE '$search%' OR nro_campo LIKE '$search%' OR semilla_neta LIKE '$search%' ";
        $query .= "OR categoria_obtenida LIKE '$search%' OR nro_etiqueta LIKE '$search%'";

        DBConnector::ejecutar($query);
    }
    public static function getSemillaProdCertificaForPDF($id,$sistema = 'certifica') {
        $query = "SELECT nro_campo,nro_etiqueta,categoria_obtenida,semilla_neta ";
        if ($sistema=='certifica'){           
           $query .= "FROM view_semilla_producida "; 
        }else{
           $query .= "FROM view_semilla_producida_fiscal ";          
        }
         $query .= "WHERE id_solicitud=$id AND gestion='".date('Y')."'";  

        DBConnector::ejecutar($query);
    }
    /**
     * Los datos de la tabla semilla_producida
     * */
    public function getSemillasProducidas() {
        $query = " SELECT * FROM semilla_producida";

        DBConnector::ejecutar($query);
    }

    /**
     * semilla neta producida para importar
     * */
    public static function getSemillaProducidaImport($semilla_neta) {
        if ($semilla_neta = "") {
            return 0.00;
        } else {
            return round($semilla_neta, 2);
        }
    }

    /**
     * Extrae los datos para ser insertados en un scrpt sql
     * @param  id_semillaProd	mediumint(8)
     * @param  nro_campo	varchar(8)
     * @param  categoria	varchar(15)
     * @param  generacion	varchar(10)
     * @param  semilla_neta	decimal(4,2)
     * @param  categoria_obtenida	varchar(15)
     * @param  nro_etiqueta	smallint(5)
     * @param  rango	varchar(15)
     * @param  id_cosecha	mediumint(8)
     * @param  id_laboratorio	mediumint(8)
     * @param  id_categoria	tinyint(4)
     */
    public function getSemillasProducidasSQL() {
        $this -> getSemillasProducidas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO semilla_producida (nro_solicitud,nro_campo,id_generacion,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES ';
            while ($row = DBConnector::objeto()) {
                $query .= "(";
                $query .= $row -> nro_solicitud . "," . $row -> nro_campo . ",";
                $query .= $row -> id_generacion . ",";
                $query .= $row -> semilla_neta . "," . $row -> categoria_obtenida . ",";
                $query .= $row -> nro_etiqueta . ",'" . $row -> rango . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

} // END
?>