<?php 
/**
 * clase municipio
 */
class Municipio{
    /**
     * ingresa un municipio de que pertenece a una provincia
     * */
    public function setMunicipio ($nombre,$id_provincia){
        $query = "INSERT INTO municipio (nombre_municipio, id_provincia) VALUES ('$nombre',$id_provincia)";
        
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR :: ".__METHOD__." :: $query :: ",DBConnector::mensaje();
    }
    
    /**
     * ingresa un municipio de que pertenece a una provincia para importacion
     * @param   string   $municipio   nombre de municipio
     * @param   string   $provincia   nombre de provincia 
     * */
    public static function setMunicipioImport ($municipio,$provincia){
        $municipio_2 = Funciones::tildeByPercentage($municipio);
        $query = "SELECT * FROM municipio WHERE nombre_municipio LIKE '%$municipio_2' LIMIT 1";
        #echo $query;
        DBConnector::ejecutar($query);
        if (!DBConnector::nroError())
            #echo $query;
        if(DBConnector::filas()){
            $row = DBConnector::objeto();            
            return $row->id_municipio;
        }else{
            $iprovincia = Provincia::setProvincia($provincia);
            $query = "INSERT INTO municipio (nombre_municipio, id_provincia) VALUES ('$municipio',$iprovincia)";
            DBConnector::ejecutar($query);

            return DBConnector::lastInsertId();
        }
    }
    /**
     * actualizar los datos de un municipio
     * */
     public function updMunicipio ($id, $new_municipio){
         $query = "UPDATE municipio SET nombre = '$new_municipio' WHERE id_municipio = $id";
         
         DBConnector::ejecutar($query);
     }
     /**
      * eliminar un municipio
      * */
      public function delMunicipio ($id){
          $query = "DELETE FROM municipio WHERE id_municipio = $id";
          
          DBConnector::ejecutar($query);          
      }
    
	/**
     * @param $sistema string  nombre del sistema
     * 
     * @return ejecuta la consulta y devuelve el id del sistema
     * */
    public static function getIdMunicipio ($municipio){
        $query = "SELECT id_municipio FROM municipio WHERE nombre LIKE '$municipio%'";
        
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::resultado();
        
        return $obj->id_municipio;
    }
    /**
     * lista de municipios
     * */
     public function getMunicipio(){
         $query = "SELECT * FROM municipio";
         
         DBConnector::ejecutar($query);
     }
     
    
    /**
     * devuelve el nombre de un municipio segun el id 
     * */
    public static function getNombreMunicipio($id){
        $query = "SELECT nombre_municipio FROM municipio WHERE id_municipio = $id";
        
        DBConnector::ejecutar($query);
        
        $obj = DBConnector::objeto();
        
        return $obj->nombre_municipio;
        
    }
    /**
     * devuelve el nombre de un muncipio segun el id 
     * */
     public function getMunicipioById($nombre){
         $query = "SELECT id_municipio FROM municipio WHERE nombre = '$nombre'";
         
         DBConnector::ejecutar($query);  
         
         $obj = DBConnector::resultado();
         
         return $obj ->id_municipio;       
     }
     
    /**
     * devuelve la provincia a la cual perteece un municipio
     * */
    public static function getProvinciaByMunicipio($id_provincia){
        $query = "SELECT pr.nombre FROM municipio mun INNER JOIN provincia pr ON pr.id_provincia=mun.id_provincia WHERE mun.id_provincia = $id_provincia";
        
        DBConnector::ejecutar($query);
        
        while ($obj = DBConnector::resultado()){
            $json['provincias'] = $obj; 
        }
        
        return $json;
    }
    
    /**
     * formatea los datos para su uso en un script sql
     * */
     public function getMunicipioSQL(){
         $query = 'INSERT INTO municipio (id_municipio,nombre_municipio,id_provincia) VALUES ';
        $this -> getMunicipio();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row -> id_municipio.",'".$row->nombre_municipio."',".$row->id_provincia . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
     }
    
}
 ?>