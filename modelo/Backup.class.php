<?php
/**
 * Encargada de todas las operaciones q se realizan en la tabla tabbackup
 *
 * @package model
 * @author Edwin W. Arandia Zeballos
 * 12-11-11 agregado variables y los metodos initBackup,selectTable,getRespaldarBaseDeDatos
 * 27-10-16 agregado cabeceras para semillera,semillerista,sistema,departamento,provincia,municipio
 * campanha,cultivo,variedad,categoria,generacion,inscrita
 */
class Backup {
    /**
     * creacion de archivo sql
     * @param string $nombre nombre de archivo
     * @param string $contenido   contenido a ser agregado al archivo
     * */
    public function setContenido($nombre,$contenido) {
        #echo $contenido;exit;
        $file = fopen($nombre, "a") or die("Problemas en la creacion del archivo");
        //vamos añadiendo el contenido
        fputs($file, $contenido);
        //cerramos el archivo
        fclose($file);
    }

    public function cabecera() {
        $titulo = utf8_decode("-- Instituto Nacional de Innovación Agropecuaria y Forestal\r\n");
        $titulo .= utf8_decode("-- Sistema de Información de ");
        $titulo .= utf8_decode("Certificación y Fiscalización de Semillas\r\n");
        $titulo .= "-- Archivo generado el " . date('d-m-Y G:i:s') . "\r\n";

        $titulo .= "\r\n";
        $titulo .= "--\r\n-- Base de datos: 'iniaf'\r\n--\r\n";

        return $titulo;
    }

    public function cabeceraUsuarios() {
        $contenido_usuario = "\r\n\n";
        $contenido_usuario .= "--\r\n-- Volcar la base de datos para la tabla 'usuarios'\r\n--\r\n";
        $contenido_usuario .= "\r\n";

        return $contenido_usuario;
    }
    public function cabeceraSemillera() {
        $contenido_semillera = "\r\n\n";
        $contenido_semillera  .= "--\r\n-- Volcar la base de datos para la tabla 'semillera'\r\n--\r\n";
        $contenido_semillera  .= "\r\n";

        return $contenido_semillera;
    }
    
    public function cabeceraSemillerista() {
        $contenido_semillerista = "\r\n\n";
        $contenido_semillerista .= "--\r\n-- Volcar la base de datos para la tabla 'semillerista'\r\n--\r\n";
        $contenido_semillerista .= "\r\n";

        return $contenido_semillerista;
    }
    
    public function cabeceraSistema() {
        $contenido_sistema = "\r\n\n";
        $contenido_sistema .= "--\r\n-- Volcar la base de datos para la tabla 'sistema'\r\n--\r\n";
        $contenido_sistema .= "\r\n";

        return $contenido_sistema;
    }
    
    public function cabeceraDepartamento() {
        $contenido_departamento = "\r\n";
        $contenido_departamento .= "--\r\n-- Volcar la base de datos para la tabla 'departamento'\r\n--\r\n";
        $contenido_departamento .= "\r\n";

        return $contenido_departamento;
    }
    
    public function cabeceraProvincia() {
        $contenido_provincia = "\r\n\n";
        $contenido_provincia .= "--\r\n-- Volcar la base de datos para la tabla 'provincia'\r\n--\r\n";
        $contenido_provincia .= "\r\n";

        return $contenido_provincia;
    }
    
    public function cabeceraMunicipio() {
        $contenido_municipio = "\r\n\n";
        $contenido_municipio .= "--\r\n-- Volcar la base de datos para la tabla 'municipio'\r\n--\r\n";
        $contenido_municipio .= "\r\n";

        return $contenido_municipio;
    }    
    
    public function cabeceraSolicitud() {
        $contenido_solicitud = "\r\n\n";
        $contenido_solicitud .= "--\r\n-- Volcar la base de datos para la tabla 'solicitud'\r\n--\r\n";
        $contenido_solicitud .= "\r\n";

        return $contenido_solicitud;
    }
    
    public function cabeceraCampanha() {
        $contenido  .= "\r\n\n";
        $contenido  .= "--\r\n-- Volcar la base de datos para la tabla 'campanha'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }
    
    public function cabeceraCultivo() {
        $contenido  .= "\r\n";
        $contenido  .= "--\r\n-- Volcar la base de datos para la tabla 'cultivo'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }
    
    public function cabeceraVariedad() {
        $contenido  .= "\r\n";
        $contenido  .= "--\r\n-- Volcar la base de datos para la tabla 'variedad'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }
    
    

    public function cabeceraInscrita() {
        $contenido  .= "\r\n";
        $contenido  .= "--\r\n-- Volcar la base de datos para la tabla 'inscrita'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }
    
    public function cabeceraSemilla() {
        $contenido_semilla = "\r\n";
        $contenido_semilla .= "--\r\n-- Volcar la base de datos para la tabla 'semilla(sembrada)'\r\n--\r\n";
        $contenido_semilla .= "\r\n";

        return $contenido_semilla;
    }

    public function cabeceraSuperficie() {
        $contenido_superficie = "\r\n";
        $contenido_superficie .= "--\r\n-- Volcar la base de datos para la tabla 'superficie'\r\n--\r\n";
        $contenido_superficie .= "\r\n";

        return $contenido_superficie;
    }

    public function cabeceraInspecciones() {
        $contenido_inspeccion = "--\r\n-- Volcar la base de datos para la tabla 'inspeccion'\r\n--\r\n";
        $contenido_inspeccion .= "\r\n";

        return $contenido_inspeccion;
    }

    public function cabeceraHojaCosecha() {
        $contenido_cosecha = "--\r\n-- Volcar la base de datos para la tabla 'hoja_cosecha'\r\n--\r\n";
        $contenido_cosecha .= "\r\n";

        return $contenido_cosecha;
    }

    public function cabeceraLaboratorio() {
        $contenido_cuenta = "\r\n";
        $contenido_cuenta .= "--\r\n-- Volcar la base de datos para la tabla 'laboratorio'\r\n--\r\n";
        $contenido_cuenta .= "\r\n";

        return $contenido_cuenta;
    }

    public function cabeceraCuenta() {
        $contenido_cuenta = "\r\n";
        $contenido_cuenta .= "--\r\n-- Volcar la base de datos para la tabla 'cuenta'\r\n--\r\n";
        $contenido_cuenta .= "\r\n";

        return $contenido_cuenta;
    }

    public function cabeceraCosto() {
        $contenido = "\r\n";
        $contenido .= "--\r\n-- Volcar la base de datos para la tabla 'costo'\r\n--\r\n";
        $contenido .= "\r\n";

        return $contenido ;
    }

    public function cabeceraFiscalizacion() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'fiscalizacion'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraCategoria() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'categoria'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraGeneracion() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'generacion'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraSemillaProducida() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'semilla_producida'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraTiene() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'tiene'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraRealiza() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'realiza'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function cabeceraCtaCsto() {
        $contenido  = "--\r\n-- Volcar la base de datos para la tabla 'cta_csto'\r\n--\r\n";
        $contenido  .= "\r\n";

        return $contenido ;
    }

    public function getRespaldarBaseDeDatos($nombre, $fecha, $hora, $id) {        
        #insertamos la informacion de la copia en una tabla
        
        $this -> insertCopia($nombre, $fecha, $hora, $id);
        /*
         header("Content-Description: File Transfer");
         header("Content-Type: application/force-download");
         //obliga la descarga
         header("Content-Disposition: attachment; filename=$nombre");
         */
        if (!DBConnector::nroError())
            return 'OK';
        else {
            return DBConnector::mensaje();
        }
    }

    /**
     * restaura un backup
     */
    function restaurarBaseDatos($dir, $user, $password, $idBDName) {
        $error = '';
        $archivo = $_SERVER['DOCUMENT_ROOT'] . '/'.BACKUP_FOLDER;
        if (!empty($dir)) {
            $sistema = "SHOW variables WHERE variable_name= 'basedir'";
            DBConnector::ejecutar($sistema);
            while ($temp = DBConnector::asociativo()) {
                $dirBase = $temp["Value"];
            }
            $primero = substr($dirBase, 0, 1);
            if ($primero == "/") {
                $dirBase = "mysql";
            } else {
                $dirBase .= "/bin/mysql";
            }
            //$restaurar = $DirBase . ' -u ' . $user . ' -p' . $password . ' ' . $bdName . ' -e "source ' . $archivo . $dir . '"';
            //echo "mysql -e \"source $archivo$dir iniaf6\"";
            $this -> getFileNameById($dir);
            while ($fila = DBConnector::objeto()) {
                $bdName = basename($fila -> archivo);
            }
            $restaurar = $dirBase . ' -u ' . $user . ' -p' . $password . ' ' . $bdName . ' -e "source ' . $archivo . $dir . '"';

            $res = system($restaurar, $error);
            #echo $restaurar;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * retorna el nombre del archivo segun su id
     *
     * @param int id del archivo
     *
     * @return nombre del archivo
     * */
    public function getFileNameById($id) {
        $query = "SELECT archivo FROM backup WHERE id_backup=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();exit;
    }

    /**
     * guarda informacion sobre la copia de seguridad
     * */
    public function insertCopia($nombre, $fecha, $hora, $id) {

        $query = "INSERT INTO backup (fecha,hora,archivo,id_login) VALUES ('$fecha','$hora','$nombre',$id)";

        DBConnector::ejecutar($query);
    }

    /**
     * elimina una copia de seguridad
     *
     * 30-09-2012
     * */
    public function deleteArchivo($archivo) {
        $query = "DELETE FROM backup WHERE id_backup = $archivo";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve todas las copias de seguridad realizadas
     * */
    public function getCopias() {
        $query = "SELECT * FROM backup ORDER BY id_backup DESC";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getBackupSQL() {
        $query = 'INSERT INTO backup(id_backup,fecha,hora,archivo,id_login) VALUES ';
        $this -> getCopias();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_backup .",'";
                $query .= $row -> fecha."','".$row->hora."','".
                $query .= $row->archivo."',".$row->id_login . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

} // END
?>