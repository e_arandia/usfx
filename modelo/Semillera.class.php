<?php
/**
 * clase semillera
 */
class Semillera {
    /**
     * @param $semillera string nombre de la semillera
     *
     * @return boolean TRUE si no existe. FALSE si se encuentra en la tabla
     * */
    private static function checkSemillera($semillera,$id) {
        $query = "SELECT * FROM view_lst_semilleras_id WHERE semillera LIKE '$semillera%' AND id_sistema=$id";
        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    /**
     * verificar la existencia de una semillera
     * @param string $semillera nombre de la semillera
     *
     * @return bool   TRUE si existe. otherwise FALSE
     * */
    public static function existeSemillera($semillera) {
        return self::checkSemillera($semillera);
    }

    /**
     * devuelve el numero de solicitud siguiente
     * para una semillera nueva
     * **/
    public static function contarSemilleras() {
        $query = "SELECT COUNT(*) + 1 AS id FROM semillera";
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        return $obj -> id;
    }

    /**
     * devuelve el id de una semillera
     * @param string $semillera nombre de la semillera
     *
     * @return devuelve el id de la semillera
     * */
    public static function getIdSemillera($semillera,$id_sistema) {

        $query = "SELECT id AS id_semillera FROM view_lst_semilleras_id ";
        $query .= "WHERE semillera LIKE '$semillera%' AND id_sistema=$id_sistema";
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError()){
             echo DBConnector::mensaje(),'=',$query;
         }
        $obj = DBConnector::objeto();
        return $obj -> id_semillera;
    }
    /**
     * devuelve el id de una semillera
     * @param string $semillera nombre de la semillera
     *
     * @return devuelve el id de la semillera
     * */
    public static function getIdSemilleraCertifica($semillera) {

        $query = "SELECT DISTINCT(id) AS id_semillera FROM view_lst_semilleras_id WHERE semillera LIKE '$semillera%'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        return $obj -> id_semillera;
    }
    /**
     * agrega una nueva semillera o devuelve el id si ya esta registrada
     * @param string $semillera nombre de la semillera
     * @param integer $nroSolicitud numero de la solicitud
     * @param integer $id_sistema id de sistema
     *
     * @return devuelve el id de la semillera segun el sistema 
     * */
    public static function setSemillera($semillera, $nroSolicitud,$id_sistema) {
        #var_dump(self::checkSemillera($semillera));
        if (!self::checkSemillera($semillera,$id_sistema)) {
            $query = "INSERT INTO semillera (nombre,nro_solicitud) VALUES ('$semillera','$nroSolicitud')";
            #echo $query;
            DBConnector::ejecutar($query);
            return DBConnector::lastInsertId();
        } else {
            return self::getIdSemillera($semillera,$id_sistema);
        }
    }
    /**
     * agrega una nueva semillera o devuelve el id si ya esta registrada
     * @param string $semillera nombre de la semillera
     * @param integer $nroSolicitud numero de la solicitud
     * @param integer $id_sistema id de sistema
     *
     * @return devuelve el id de la semillera segun el sistema 
     * */
    public static function setSemilleraImport($semillera, $nroSolicitud) {
        $semillera_2 = Funciones::search_tilde($semillera);
        $query = "SELECT * FROM semillera WHERE nombre LIKE '$semillera_2' AND nro_solicitud='$nroSolicitud'";
        
        DBConnector::ejecutar($query);
        if(DBConnector::filas()){
            $row = DBConnector::objeto();
            
            return $row -> id_semillera;
        }else{
            $query = "INSERT INTO semillera (nombre,nro_solicitud) VALUES ('".utf8_decode($semillera)."','$nroSolicitud')";            
            DBConnector::ejecutar($query);
            
            return DBConnector::lastInsertId();
        }
    }    

    /**
     * agrega una nueva solicitud de fiscalizacion
     * @param string $semillera nombre de la semillera
     * @param integer $nroSolicitud numero de la solicitud
     *
     * @return devuelve el id de la semillera
     * */
    public static function setSemilleraFiscal($semillera, $nroSolicitud) {
        $query = "INSERT INTO semillera (nombre,nro_solicitud) VALUES ('$semillera','$nroSolicitud')";
        #echo $query;
        DBConnector::ejecutar($query);
        return DBConnector::lastInsertId();
    }

    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemillera($id) {

        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id WHERE id=$id";
        //echo $query;
        DBConnector::ejecutar($query);
        #DBConnector::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> semillera;
    }
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraBy($nombre) {

        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id WHERE semillera LIKE '$nombre%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByIdSolicitud($id) {

        $query = "SELECT DISTINCT(semillera) FROM view_solicitudes ";
        $query .= "WHERE id_solicitud LIKE '$id'";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * lista de semilleras que tienen papa
     * */
    public static function getListSemilleraPapa($nombre){
        $query = "SELECT semillera FROM view_lst_semilleras_papa WHERE semillera LIKE '$nombre%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstado($nombre,$estado) {
        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id ";
        $query .= "WHERE semillera LIKE '$nombre%' AND estado=$estado";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstadov2($nombre,$estado) {
        $query = "SELECT DISTINCT(semillera) FROM view_solicitudes ";
        $query .= "WHERE semillera LIKE '$nombre%' AND estado=$estado ";
        $query .= "ORDER BY semillera";
        echo $query;exit;
        DBConnector::ejecutar($query);
    }    
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstadoCertificaSuperficie($nombre,$inicio,$fin) {
        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id ";
        $query .= "WHERE semillera LIKE '$nombre%' AND (estado>=$inicio AND estado <=$fin)";
        #echo $query;
        DBConnector::ejecutar($query);
    } 
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstadoCertificaSemillaP($nombre,$estado) {
        $query = "SELECT DISTINCT(semillera) FROM view_solicitudes_certificacion ";
        $query .= "WHERE semillera LIKE '$nombre%' AND estado = $estado";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    
    
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstadoCertificaCosecha($nombre,$inicio,$fin) {
        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id ";
        $query .= "WHERE semillera LIKE '$nombre%' AND (estado>=$inicio AND estado <=$fin)";
        #echo $query;
        DBConnector::ejecutar($query);
    }    
    /**
     * Id de la semillera
     *
     * @param $id integer numero de semillera
     *
     * @return devuelve el nombre de la semillera que tiene el id
     * */
    public static function getNombreSemilleraByNombreAndEstadoCertificaInspeccion($nombre,$inicio,$fin) {
        $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id ";
        $query .= "WHERE semillera LIKE '$nombre%' AND (estado>=$inicio AND estado <$fin)";
        #echo $query;
        DBConnector::ejecutar($query);
    }   
    /**
     * devuelve el nombre de la semillera segun el id
     * */
     public static function getNombreSemilleraById($id){
         $query = "SELECT semillera FROM view_lst_semilleras_id WHERE id=$id";
         
         DBConnector::ejecutar($query);
         if(DBConnector::nroError()){
             echo DBConnector::mensaje(),'=',$query;
         }
         
         $obj=DBConnector::objeto();
         
         return $obj->semillera;
     }

    /**
     * lista las semilleras segun la provincia y municipio seleccionado
     * @param int $provincia   id de la provincia
     * @param int $municipio   id de municipio
     * @param int $sistema     id de sistema
     * 
     * 
     * **/
    public static function getListSemilleras($provincia, $municipio,$sistema) {
        $query = "SELECT id_semillera,semillera FROM view_semillera_provincia ";
        $query .= "WHERE iprovincia = $provincia AND imunicipio = $municipio AND id_sistema=$sistema ";
        $query .= "ORDER BY semillera ASC";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }
    /**
     * lista de semilleras segun coincidencia
     * */
     public static function getListSemillerasFilter($search){
         $query = "SELECT DISTINCT(semillera) FROM view_lst_semilleras_id WHERE id_sistema=2 AND semillera LIKE '$search%'";
         #echo $query;
         DBConnector::ejecutar($query);
     }
    /**
     * Contador de semilleras
     * */
    public static function getNroSolicitud() {
        $query = "SELECT COUNT(*) AS nro_solicitud FROM semillera";

        DBConnector::ejecutar($query);
    }
    /**
     * devuelve todas las semilleras
     * */
     public function getSemilleras (){
         $query = "SELECT * FROM semillera";
         
         DBConnector::ejecutar($query);
     }
    /**
     * formatea los datos para su uso en un script sql
     * */
     public function getSemilleraSQL() {
        $query = 'INSERT INTO semillera (id_semillera,nro_solicitud,nombre) VALUES ';
        $this -> getSemilleras();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(".$row->id_semillera.",'";
                $query .= $row -> nro_solicitud . "','";
                $query .= $row -> nombre . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>