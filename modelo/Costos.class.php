<?php
class Costos {
    /**
     * inserta un nuevo cultivo y su costo respectivo por etapa
     * */
    public function insertCosto($id_cultivo, $inscripcion, $inspeccion, $analisis_etiqueta) {
        $query = "INSERT INTO costo(id_cultivo,inscripcion,inspeccion,analisis_etiqueta)
        VALUES ($id_cultivo,$inscripcion,$inspeccion,$analisis_etiqueta)";

        DBConnector::ejecutar($query);
    }

    /**
     * elimina un cultivo y su costo
     * */
    public function deleteCosto($idCosto) {
        $query = "DELETE FROM costo WHERE id_costo=$idCosto";

        DBConnector::ejecutar($query);
    }

    public function updateCosto($id_costo, $inscripcion, $inspeccion, $analisis) {
        $query = "UPDATE costo ";
        $query .= "SET inscripcion = $inscripcion, Inspeccion=$inspeccion,analisis_etiqueta=$analisis ";
        $query .= "WHERE id_costo = $id_costo";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
    }

    /**
     * lista todos los nombres de cultivos y los costos de cada uno
     * */
    public static function getNombreCostos() {
        $query = "SELECT cultivo FROM view_costos_cultivo ";
        $query .="WHERE cultivo NOT IN ('Acondicionamiento','Barbecho','Plantines') ORDER BY cultivo ASC";

        DBConnector::ejecutar($query);
    }
    public static function getNombreCostosPlantines($id) {
        $query =  "SELECT nombre_cultivo AS cultivo FROM cultivo ";
        $query .= "WHERE plantin = $id AND nombre_cultivo <> 'plantines'";
        $query .= "ORDER BY cultivo ASC";
#echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
    }

    /**
     * Lista tdos los cultivos y sus costos
     * */
    public function getCostos($id = 0) {
        if (empty($id))
            $query = "SELECT * FROM view_costos_cultivo";
        else {
            $query = "SELECT * FROM view_costos_cultivo WHERE id = $id";
        }
#echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * costos de un cultivo
     * @param string $cultivo nombre del cultivo
     * */
    public function getCostoByName($cultivo) {
        
        switch ($cultivo) {
            case 'plantines':
                $query = "SELECT * FROM view_costos_cultivo WHERE cultivo LIKE 'plantines'";
                break;            
            case 'acondicionamiento':
                $query = "SELECT * FROM view_costos_cultivo WHERE cultivo LIKE 'acondicionamiento'";
                break;
            default :
                $query = "SELECT * FROM view_costos_cultivo WHERE cultivo LIKE '$cultivo'";
                break;
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR GETCOSTOBYNAME:: $query ::";
            echo DBConnector::mensaje();
            
            }
    }

    /**
     * @param id_costo	tinyint
     * @param cultivo	varchar
     * @param inscripcion	decimal
     * @param inspeccion	decimal
     * @param analisis_etiqueta	decimal
     * */
    public function getCostosSQL() {
        $this -> getCostos();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO costo (inscripcion,inspeccion,analisis_etiqueta,id_cultivo) VALUES';
            while ($row = DBConnector::objeto()) {
                $query .= "('";
                $query .= $row -> inscripcion . "," . $row -> inspeccion . ",";
                $query .= $row -> analisis_etiqueta . ",". $row->id_cultivo ."),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }

        return $query;
    }

}
?>