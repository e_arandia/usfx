<?php
class Resultado_importada{
/**
     * lista todos los resultados de muestras
     * */
     public function getResultadoImportada(){
         $query = "SELECT * FROM resultado_certificada";
         
         DBConnector::ejecutar($query);
     }
     
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getResultadoImportadaSQL() {
        $query = 'INSERT INTO resultado_importada () VALUES ';
        $this -> getResultadoImportada();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_importada . ",". $row->muestra . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>