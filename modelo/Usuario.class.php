<?php
/**
 * clase usuario con los metodos
 * necesarios.
 * @package modelo
 * @author Edwin W. Arandia Zeballos
 */

class Usuario {
    public static function checkUsuario($login, $pass) {
        $error = '';
        if (strlen($login) < 4)
            $error .= 'usuario';
        if ($pass == sha1(' '))
            $error .= 'password';

        return $error;
    }

    public function checkPassword($password1, $password2) {
        if ($password1 != $password2 || (strlen($password1) <= 5 || strlen($password2) <= 5)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    /**
     * lista de todos los usuarios
     * */
     public function getListUsuarios(){
         $query = "SELECT * FROM usuario";
         
         DBConnector::ejecutar($query);
     }
    /**
     * lista de todos los usuarios registrados
     */
    public function getUsuarios() {
        $query = "SELECT * FROM view_list_usuarios";

        DBConnector::ejecutar($query);
    }

    /**usuarios habilitados en el sistema*/
    public function getUsuariosHabilitados() {
        $query = "SELECT * FROM view_usuarios WHERE estado = 1 ORDER BY apellido";

        DBConnector::ejecutar($query);
    }

    /**
     * Verifica si existe o no un usuario
     *
     */
    public function getUsuarioAd($nick, $pass) {
        $query = "SELECT * FROM view_usuarios WHERE login='$nick' AND contrasena='$pass' AND estado=1";
        #echo $query;
        //return $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de un usario determinado
     *
     */
    public function getDatos($usuario) {
        if (!is_int($usuario))
            $query = "SELECT * FROM usuario user INNER JOIN area ON user.id_area=area.id_area WHERE user.login LIKE '$usuario%'";
        else {
            $query = "SELECT * FROM view_usuarios WHERE id_login=$usuario";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de usuario segunn nombre de login
     * */
    public function getDatosUsuarioByLogin($login) {
        $query = "SELECT * FROM view_usuarios WHERE login='$login'";

        DBConnector::ejecutar($query);
    }

    /**
     * estado de un usuario del sistema  habilitado | deshabilitado
     * @param $id   id del usuario
     * */
    public function getEstado($id) {
        $query = "SELECT id_login AS id,nombre,apellido,estado FROM view_usuarios WHERE id_login=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * actualizar los datos del usuario desde administracion
     */
    function updateUsuario($nombre, $apellido, $area, $id, $usuario = '', $estado = '') {
        $query = "UPDATE usuario SET nombre = '$nombre',apellido ='$apellido',id_area = $area,
        estado ='$estado',login='$usuario' WHERE id_login=$id";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualizar nombre y apellido de usuario
     * */
    public function updateInfo($nombre, $apellido, $id) {
        $query = "UPDATE usuario SET nombre ='$nombre',apellido='$apellido'
        WHERE id_login=$id";
        DBConnector::ejecutar($query);
    }

    /**
     * actualiza el password
     */
    function update_password($user, $pwd) {
        $query = "UPDATE usuario SET contrasena = sha1('$pwd') 
        WHERE id_login='$user'";

        DBConnector::ejecutar($query);
    }

    /**
     * actualiza permisos para editar informacion
     * */
    public function updatePermiso($user, $crud) {
        $query = "UPDATE usuario SET crud = $crud WHERE id_login=$user";
        //echo $query; exit;
        DBConnector::ejecutar($query);
    }

    public static function updateIntento($login) {
        $query = "UPDATE usuario SET nro_intento = nro_intento+1 WHERE login='" . $login . "'";

        DBConnector::ejecutar($query);
    }

    public static function nroIntentos($login) {
        $query = "Select nro_intento FROM usuario WHERE login='" . $login . "'";

        DBConnector::ejecutar($query);
    }

    public static function maxIntentos() {
        $query = "SELECT max_intento FROM usuario WHERE login='" . $login . "'";

        DBConnector::ejecutar($query);

        return DBConnector::resultado();
    }

    public static function resetIntentos($login) {
        $query = "UPDATE nro_intento = 1 FROM usuario WHERE login ='" . $login . "'";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el login de usuarios
     * */
    public static function getNombreUsuarios($usuario) {
        $query = "SELECT login FROM usuario WHERE login='$usuario'";
        DBConnector::ejecutar($query);
    }

    /**
     * Area que pertenece un usuario
     * */
    public static function getAreaUsuario($area) {
        $query = "SELECT nivel FROM view_usuarios WHERE id_area = $area";
        DBConnector::ejecutar($query);

        $temp = DBConnector::objeto();

        return $temp -> nivel;
    }

    /**
     * Guarda los datos de un nuevo usuario en la base de datos
     */
    function setUsuario($usuario = 8, $nombre, $apellido, $login, $contrasena, $area) {
        if ($usuario != 10) {
            $query = "INSERT INTO usuario (nombre,apellido,id_area,fecha_ingreso,nivel,login,contrasena,crud) 
                                VALUES ( '$nombre', '$apellido',$area,'" . date('Y-m-d') . "',$nivel,'$login','" . sha1($contrasena) . "',0111)";
        } else {
            $query = "INSERT INTO usuario (nombre,apellido,id_area,fecha_ingreso,login,contrasena,estado,crud) 
                 VALUES ( '$nombre', '$apellido', $area,'" . date('Y-m-d') . "','$login','" . sha1($contrasena) . "',1,3)";
        }

        //echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * habilita | deshabilita usuario
     * @param integer $id  numero id de usuario
     * @param integer $estado  habilita | deshabilita usuario en el sistema
     * 
     * */
    public function setEstado($id,$estado){
        $query = "UPDATE usuario SET estado=$estado WHERE id_login=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * eliminar usuario determinado
     */
    function delete_user($idUser) {
        $query = "DELETE FROM usuario WHERE id_login = " . $idUser;
        echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query :: ",DBConnector::mensaje();
    }

    /**
     * realiza una copia de la base de datos a un archivo sql
     */
    function respaldar_base_datos($destino, $user, $password, $bdName) {
        $error = '';

        $archivo = $_SERVER['DOCUMENT_ROOT'] . '/backupSICEF/';
        $sistema = "SHOW variables WHERE variable_name= 'basedir'";
        DBConnector::ejecutar($sistema);
        while ($temp = DBConnector::resArray()) {
            $dirBase = $temp['Value'];
        }
        $primero = substr($dirBase, 0, 1);
        if ($primero == "/") {
            $dirBase = "mysqldump";
        } else {
            $dirBase .= "/bin/mysqldump";
        }
        if (!file_exists($archivo)) {
            mkdir($archivo);
        }
        $archivo .= $destino . '.sql';
        $executa = $dirBase . ' -u ' . $user . ' -p ' . $password . ' --opt --ignore-table=' . $bdName . '.tabbackup --skip-comments --compact --add-drop-table -c ' . $bdName . ' > ' . $archivo;
        echo $executa;
        exit ;
        system($executa, $error);
        if (!$error) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Extrae los datos para ser insertados en un scrpt sql
     * @param  id_login	tinyint(4)
     * @param  nombre	varchar(35)
     * @param  apellido	varchar(35)
     * @param  area	enum('certificacion','fiscalizacion','laboratorio','administracion')
     * @param  fecha_ingreso	date
     * @param  nivel	tinyint(2)
     * @param  login	varchar(15)
     * @param  contrasena	char(40)
     * @param  max_intento	tinyint(1)
     * @param  nro_intento	tinyint(1)
     * @param  estado	varchar(13)
     */
    public function getUsuariosSQL() {
        $this -> getListUsuarios();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO usuario (id_login,nombre,apellido,id_area,fecha_ingreso,login,contrasena,estado,crud) VALUES ';
            while ($row = DBConnector::objeto()) {
                $query .= " (".$row->id_login.",'";
                $query .= $row -> nombre . "','";
                $query .= $row -> apellido . "'," . $row -> id_area . ",'";
                $query .= $row -> fecha_ingreso ."','";
                $query .= $row -> login . "','" . $row -> contrasena . "',";
                $query .= $row -> estado . "," . $row->crud ."),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
} // END
?>