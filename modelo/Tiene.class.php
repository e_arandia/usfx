<?php
class Tiene {
    /**
     * relacion cuenta-semilla
     * @param integer $isemilla id de la semilla
     * @param integer $isolicitud id de la solicitud
     * */
    public static function solicitud_semilla($isolicitud, $isemilla) {
        $query = "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$isemilla)";
       # echo "tabla de relacion solicitud_semilla: ".$query; exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR >> al insertar en tabla solicitud-semilla :: $query";
            exit;
        }
    }
    /**
     * relacion cuenta-semilla
     * @param integer $isemilla id de la semilla
     * @param integer $icuenta id de la cuenta
     * */
    public static function cuenta_semilla($icuenta,$isemilla){
        $query = "INSERT INTO cuenta_semilla (id_cuenta,id_semilla) VALUES ($icuenta,$isemilla)";
        
        DBConnector::ejecutar($query);
    }
    /**
     * por defecto elimina por id de solicitud
     * @param  string   $texto  nombre de tabla
     * @param  integer  $id     numero de id
     * */
    public static function deleteTieneById($id,$texto='solicitud'){
        if($texto == 'solicitud'){
            $query = "DELETE FROM solicitud_semilla WHERE id_solicitud = $id";    
        }else{
            $query = "DELETE FROM solicitud_semilla WHERE id_semilla = $id";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }
    private function getTiene() {        
        $query = 'SELECT * FROM tiene';

        DBConnector::ejecutar($query);
    }
    
    private function getTieneCuenta() {
        $query = 'SELECT * FROM cuenta_semilla';

        DBConnector::ejecutar($query);
    }

    /**
     *
     * @param integer id id de la semilla
     *
     * @return devuelve el id de una solicitud segun el id de la semilla ubicada en la tabla solicitud_semilla
     * */
    public static function getIdSolicitud($id) {
        $query = "SELECT id_solicitud FROM solicitud_semilla WHERE id_semilla = $id";

        DBConnector::ejecutar($query);

        $result = DBConnector::objeto();

        return $result -> id_solicitud;
    }

    /**
     * Devuelve el id de una semilla
     * @param integer id_solicitud de solicitud
     *
     * */
    public static function getIdSemilla($isolicitud) {
        $query = "SELECT id_semilla ";
        $query .= "FROM solicitud_semilla WHERE id_solicitud=$isolicitud";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }else{
            
            if (DBConnector::filas()) {
                $result = DBConnector::objeto();
                return $result -> id_semilla;
            } else {
                #echo $query;
                return '-';
            }
        }        
    }
    
    public static function getIdCuentaByIdSemilla($isemilla){
        $query = "SELECT id_cuenta FROM cuenta_semilla WHERE id_semilla = $isemilla";
        
        DBConnector::ejecutar($query);
    }
    
    public static function getIdSemillaByIdCuenta($icuenta){
        $query = "SELECT id_semilla FROM cuenta_semilla WHERE id_cuenta = $icuenta";
        
        DBConnector::ejecutar($query);
    }
    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getTieneSQL() {
        $query = 'INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES';
        $this -> getTiene();
        if (DBConnector::filas() == 0) {
            $query = "";
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_solicitud . "," . $row -> id_semilla . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
    
    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getTieneCuentaSQL() {
        $query = 'INSERT INTO cuenta_semilla (id_cuenta,id_semilla) VALUES';
        $this -> getTieneCuenta();
        if (DBConnector::filas() == 0) {
            $query = "";
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_cuenta . "," . $row -> id_semilla . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>