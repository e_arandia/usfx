<?php
/**
 *
 */
class Excel {
    #nombre de archivo
    var $filename;
    #ubicacion de archivo
    var $ubicacion;
    #instancia que contiene el obj excel
    var $objPHPExcel;
    #contienen el nombre de la plantilla excel
    var $plantilla;
    /**
     * constructor de la clase
     * */
    public static function cargarPlantilla($plantilla) {
        $objReader = PHPExcel_IOFactory::createReader("Excel2007");
        self::setPlantilla($plantilla);
        $this -> objPHPExcel = $objReader -> load($this -> plantilla);
    }

    /**
     * @param string $plantilla   nombre de la pantilla del archivo excel
     * */
    private static function setPlantilla($plantilla) {
        #cargar plantilla
        $this -> plantilla = $_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . VIEW . TEMPLATE . "/" . $plantilla;
    }

    /**
     * datos de la cabecera para el archivo de resumen de certificacion y fiscalizacion
     * */
    public static function cabeceraResumen() {

    }

    public static function getUbicacion() {
        return $this -> ubicacion;
    }

    /**
     * datos de cabecera de produccion y costos
     * */
    public static function datosProduccionCostos() {

    }

    /**
     * lee solo las solicitudes de A8 - Jxx
     *
     * @param object  $objWorkSheet  instancia PHPExcel
     * @param integer $highestRow    numero maximo de filas
     * @param integer $irow          fila inicial
     * */
    public static function leerSolicitudes($objWorkSheet, $highestRow, $irow) {

        return $solicitud;
    }

    /**
     * insertar solicitudes
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarSolicitudToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();

        //leemos cuantas columnas tiene e.g. AX
        $highestCol = $objWorksheet -> getHighestColumn();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['solicitud'] = array();
            $fiscalizacion['solicitud'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {
                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "solicitud";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $nro_solicitud = $objWorksheet -> getCellByColumnAndRow(0, $row) -> getValue();
                        $isistema = Sistema::setSistemaImport($sistema);
                        $isemillera = Semillera::setSemilleraImport($objWorksheet -> getCellByColumnAndRow(7, $row) -> getValue(), $nro_solicitud, $isistema);
                        $imunicipio = Municipio::setMunicipioImport($objWorksheet -> getCellByColumnAndRow(4, $row) -> getValue(), $objWorksheet -> getCellByColumnAndRow(3, $row) -> getValue());
                        $semillerista = Funciones::getNombreApellidoImportar($objWorksheet -> getCellByColumnAndRow(8, $row) -> getValue());
                        $isemillerista = Semillerista::setSemilleristaImport(utf8_decode($semillerista['nombre']), utf8_decode($semillerista['apellido']), $isemillera);
                        $comunidad = addslashes($objWorksheet -> getCellByColumnAndRow(5, $row) -> getValue());
                        $fecha = $objWorksheet -> getCellByColumnAndRow(9, $row) -> getCalculatedValue();
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha);
                        $fecha_php = date("Y-m-d", $timestamp);
                        //armar consulta
                        $sql = "INSERT INTO solicitud (id_semillera,id_sistema,id_municipio,id_semillerista,comunidad,fecha) VALUES (";
                        $sql .= "$isemillera,$isistema,$imunicipio,$isemillerista,'$comunidad','$fecha_php');\r\n";
                        //ejecutar consulta
                        //////////////////////////////////////DBConnector::ejecutar($sql);
                        fputs($file, $sql);
                        fclose($file);
                        //agregar id de solicitud
                        array_push($certificacion['solicitud'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar semillas
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarSemillaToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['semilla'] = array();
            $fiscalizacion['semilla'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "semilla";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $nro_campo = Semilla::getNroCampoImportar($objWorksheet -> getCellByColumnAndRow(10, $row) -> getValue());
                        $campanha = Campanha::getIdCampanhaImportar($objWorksheet -> getCellByColumnAndRow(11, $row) -> getValue());
                        $cultivo = Cultivo::getIdCultivoImportar($objWorksheet -> getCellByColumnAndRow(12, $row) -> getValue());
                        $variedad = Variedad::getIdVariedadImportar($objWorksheet -> getCellByColumnAndRow(13, $row) -> getValue(), $cultivo);
                        $cat_sembrada = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(14, $row) -> getValue());
                        $cat_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(15, $row) -> getValue());
                        $cantidad_semilla_empleada = $objWorksheet -> getCellByColumnAndRow(16, $row) -> getCalculatedValue();
                        $lote = $objWorksheet -> getCellByColumnAndRow(17, $row) -> getCalculatedValue();
                        $fecha_excel_siembra = (!empty($objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue())) ? $objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue() : "01/01/1900";
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_excel_siembra);
                        $fecha_php_siembra = date("Y-m-d", $timestamp);
                        $plantas_hectarea = Semilla::getPlantasHectareasImportar($objWorksheet -> getCellByColumnAndRow(19, $row) -> getValue());
                        $cultivo_anterior = Cultivo::getIdCultivoImportar($objWorksheet -> getCellByColumnAndRow(20, $row) -> getValue());
                        $superficie_parcela = Superficie::getIdSuperficieBySuperficieImportar($objWorksheet -> getCellByColumnAndRow(21, $row) -> getCalculatedValue());
                        //armar consulta
                        $sql = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                        $sql .= "'$nro_campo',$campanha,$cultivo,$variedad,$cat_sembrada,$cat_producir,$cantidad_semilla_empleada,'$lote','$fecha_php_siembra',$plantas_hectarea,$cultivo_anterior,$superficie_parcela);\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['semilla'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar superficie
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarSuperficieToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        //hoja de solo lectura
        $objReader -> setReadDataOnly(true);
        //cargamos el archivo
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['superficie'] = array();
            $fiscalizacion['superficie'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "superficie";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $inscrita = Superficie::getImportarInscrita($objWorksheet -> getCellByColumnAndRow(21, $row) -> getCalculatedValue());
                        $rechazada = Superficie::getImportarRechazada($objWorksheet -> getCellByColumnAndRow(22, $row) -> getValue());
                        $retirada = Superficie::getImportarRetirada($objWorksheet -> getCellByColumnAndRow(23, $row) -> getValue());
                        $aprobada = Superficie::getImportarAprobada($objWorksheet -> getCellByColumnAndRow(24, $row) -> getCalculatedValue());

                        //armar consulta
                        $sql = "INSERT INTO superficie (inscrita,rechazada,retirada,aprobada,id_semilla) VALUES (";
                        $sql .= "$inscrita,$rechazada,$retirada,$aprobada);\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['superficie'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar inspeccion
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarInspeccionToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        //hoja de solo lectura
        $objReader -> setReadDataOnly(true);
        //cargamos el archivo
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['inspeccion'] = array();
            $fiscalizacion['inspeccion'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "inspeccion";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $primera_inspeccion = (!empty($objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue())) ? $objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue() : "01/01/1900";
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($primera_inspeccion);
                        $primera_php = date("Y-m-d", $timestamp);

                        $segunda_inspeccion = (!empty($objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue())) ? $objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue() : "01/01/1900";
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($segunda_inspeccion);
                        $segunda_php = date("Y-m-d", $timestamp);

                        $tercera_inspeccion = (!empty($objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue())) ? $objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue() : "01/01/1900";
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($tercera_inspeccion);
                        $tercera_php = date("Y-m-d", $timestamp);
                        //armar consulta
                        $sql = "INSERT INTO inspeccion (primera,segunda,tercera) VALUES (";
                        $sql .= "'$primera_php','$segunda_php','$tercera_php');\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['inspeccion'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar semillas
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarHojaCosechaToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['cosecha'] = array();
            $fiscalizacion['cosecha'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "hojaCosecha";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $nro_campo = $objWorksheet -> getCellByColumnAndRow(28, $row) -> getCalculatedValue();
                        $fecha_emision = empty($objWorksheet -> getCellByColumnAndRow(29, $row) -> getCalculatedValue()) ? '' : $objWorksheet -> getCellByColumnAndRow(29, $row) -> getCalculatedValue();
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_emision);
                        $fecha_emision_php = date("Y-m-d", $timestamp);

                        $categoria_campo = Categoria::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($objWorksheet -> getCellByColumnAndRow(30, $row) -> getCalculatedValue());
                        $rendimiento_estimado = HojaCosecha::getRendimientoEstimadoImportar($objWorksheet -> getCellByColumnAndRow(31, $row) -> getCalculatedValue());
                        $superficie = HojaCosecha::getSuperficieParcelaImportar($objWorksheet -> getCellByColumnAndRow(32, $row) -> getCalculatedValue());
                        $rendimiento_campo = HojaCosecha::getRendimientoCampoImportar($objWorksheet -> getCellByColumnAndRow(33, $row) -> getCalculatedValue());
                        $nro_cupones = HojaCosecha::getNroCuponesImportar($objWorksheet -> getCellByColumnAndRow(34, $row) -> getCalculatedValue());
                        $planta_acondicionadora = HojaCosecha::getPlantaAcondicionadoraImportar($objWorksheet -> getCellByColumnAndRow(35, $row) -> getCalculatedValue());
                        //falta isuperficie - alaboratorio segun cultivo
                        //armar consulta
                        $sql = "INSERT INTO cosecha (nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado,superficie_parcela,rendimiento_campo,nro_cupones,rango_cupones,planta_acondicionadora,alaboratorio,id_superficie) VALUES (";
                        $sql .= "'$nro_campo','$fecha_emision_php',$categoria_campo,$rendimiento_estimado,$superficie,$rendimiento_campo,$nro_cupones,'$planta_acondicionadora');\r\n";

                        fputs($file, $sql);
                        fclose($file);
                    }
                }
            }
            return $filename;
            //devolver array q contiene los id de las solicitudes de certificacion
        } else {
            return 'error';
        }
    }

    /**
     * insertar semillas
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarSemillaProducidaToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['semillaP'] = array();
            $fiscalizacion['semillaP'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "semilla_producida";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $igeneracion = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue());
                        $semilla_neta = SemillaProd::getSemillaProducidaImport($objWorksheet -> getCellByColumnAndRow(36, $row) -> getCalculatedValue());
                        $igeneracion = SemillaProd::getGeneracionCategoriaObtenidaImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue());
                        $categoria_obtenida = SemillaProd::getCategoriaObtenidaImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue(), $objWorksheet -> getCellByColumnAndRow(30, $row) -> getCalculatedValue());
                        $nro_etiqueta = SemillaProd::getNroEtiquetasImportar($objWorksheet -> getCellByColumnAndRow(38, $row) -> getCalculatedValue());
                        $rango = SemillaProd::getRangoImportar($objWorksheet -> getCellByColumnAndRow(39, $row) -> getCalculatedValue());
                        if (empty($semilla_neta) && empty($nro_etiqueta) && empty($rango)) {
                            $semilla_neta = $nro_etiqueta = $rango = 0;
                            ///SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $igeneracion,$categoria_obtenida, $nro_etiqueta, $rango);
                            $sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,id_generacion,semilla_neta,categoria_obtenida,nro_etiqueta,rango) semilla_producida VALUES ";
                            $sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                        } else {
                            #echo $isolicitud, '=',$iSemilla, '=',$categoria_obtenida, '=',$semilla_neta, '=',$nro_etiqueta, '=',$rango;
                            ///SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $igeneracion, $categoria_obtenida, $nro_etiqueta, $rango);
                            $sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES ";
                            $sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                        }
                        #Solicitud::updateEstadoBySemillaProd($isolicitud);
                        $query = "UPDATE solicitud SET estado = 10 WHERE id_solicitud = $isolicitud;\r\n";
                        if ($cultivo_name != 'papa')
                            #HojaCosecha::updateStado($iCosecha);
                            $sql .= "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $iCosecha AND estado = 0);\r\n";
                        #Solicitud::updateEstado($isolicitud);
                        $query = "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                        //armar consulta
                        $sql = "INSERT INTO semilla_producida (nro_solicitud,nro_campo,id_generacion,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES (";
                        $sql .= "$nro_solicitud,'$nro_campo',$igeneracion,$semilla_neta,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['semillaP'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar semillas
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarMuestraLaboratorioToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['muestra'] = array();
            $fiscalizacion['muestra'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "muestra_certificacion";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $nro_analisis = $objWorksheet -> getCellByColumnAndRow(40, $row) -> getCalculatedValue();

                        $fecha_recepcion = empty($objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue()) ? '' : $objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue();
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_recepcion);
                        $fecha_recepcion_php = date("Y-m-d", $timestamp);

                        $nro_campo = $objWorksheet -> getCellByColumnAndRow(10, $row) -> getCalculatedValue();
                        $origen = $objWorksheet -> getCellByColumnAndRow(5, $row) -> getCalculatedValue();

                        $pureza = $objWorksheet -> getCellByColumnAndRow(44, $row) -> getCalculatedValue();
                        $germinacion = $objWorksheet -> getCellByColumnAndRow(45, $row) -> getCalculatedValue();
                        $humedad = $objWorksheet -> getCellByColumnAndRow(46, $row) -> getCalculatedValue();
                        $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                        $isistema = Sistema::setSistemaImport($sistema);
                        //falta
                        //armar consulta
                        $sql = "INSERT INTO muestra_laboratorio (fecha_recepcion,nro_campo,origen,analisis,sistema) VALUES (";
                        $sql .= "'$fecha_recepcion_php',$nro_campo,'$origen',$ianalisis,$isistema);\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['muestra'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    /**
     * insertar semillas
     * @param $archivo  ubicacion del archvio  excel que se importara a la base de datos
     * */
    public static function importarResultadoLaboratorioToBd($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //si es excel 2007 cargamos su lector
        else
            $objReader = new PHPExcel_Reader_Excel5();
        //si no, cargamos el lector para archivos xls
        $objReader -> setReadDataOnly(true);
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            //empezar a leer datos
            #$solicitudes = $this->leerSolicitudes($objWorksheet, $highestRow, 8);
            $certificacion['resultado'] = array();
            $fiscalizacion['resultado'] = array();

            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {

                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $filename = "restultado_certificacion";
                        $file = fopen("../vista/temp/$filename.sql", "a") or die("Problemas en la creacion del archivo");
                        //obtener datos de la hoja excel
                        $fecha_resultado = empty($objWorksheet -> getCellByColumnAndRow(42, $row) -> getCalculatedValue()) ? '' : $objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue();
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_resultado);
                        $fecha_resultado_php = date("Y-m-d", $timestamp);

                        $lote = Resultado_muestras::getLoteImportar($objWorksheet -> getCellByColumnAndRow(43, $row) -> getCalculatedValue());
                        $pureza = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(44, $row) -> getCalculatedValue());
                        $germinacion = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(45, $row) -> getCalculatedValue());
                        $humedad = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(46, $row) -> getCalculatedValue());
                        $observacion = Resultado_muestras::getObservacionImportar(trim($objWorksheet -> getCellByColumnAndRow(49, $row) -> getCalculatedValue()));

                        //falta id_muestra
                        //armar consulta
                        $sql = "INSERT INTO resultado_muestras (fecha,lote,pureza,germinacion,humedad,observacion,id_muestra) VALUES (";
                        $sql .= "'$fecha_resultado_php','$lote',$pureza,$germinacion,$humedad,'$observacion');\r\n";

                        fputs($file, $sql);
                        fclose($file);
                        //ejecutar consulta
                        ////////////////////////////////DBConnector::ejecutar($sql);
                        //agregar id de solicitud
                        array_push($certificacion['resultado'], DBConnector::lastInsertId());
                    }
                }
            }
            return $filename;
        } else {
            return 'error';
        }
    }

    public static function convertirXLStoCSV($archivo) {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader -> load($archivo);
        // Export to CSV file.
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter -> setSheetIndex(0);
        // Select which sheet.
        $objWriter -> setDelimiter(',');
        // Define delimiter
        $filefolder = "../vista/temp/exportFile.csv";
        $objWriter -> save($filefolder);
        unlink($archivo);
        return $filefolder;
    }

    public static function leerCSVtoBD($archivo) {
        $objReader = new PHPExcel_Reader_CSV();
        $objReader -> setInputEncoding('CP1252');
        $objReader -> setDelimiter(',');
        $objReader -> setEnclosure('"');
        $objReader -> setLineEnding("\r");
        $objReader -> setSheetIndex(0);
        $objPHPExcel = $objReader -> load($archivo);

    }

    /**
     * INGRESAR DATOS DESDE ARCHIVO CSV
     * */
    public static function importarCSVToBD($archivo) {
        //leer archivo e ingresar datos a un array
        $csvFile = file($archivo);
        $data = array();
        foreach ($csvFile as $line) {
            array_push($data, str_getcsv($line, ","));
        }
        $total_filas = count($data);
        //total de filas

        $instituto_temp = explode(",", $data[0][0]);
        $direccion_temp = explode(",", $data[1][0]);
        $unidad_temp = explode(",", $data[2][0]);
        $oficina_temp = explode(",", $data[3][0]);

        $instituto = strtolower($instituto_temp[0]);
        $direccion = strtolower($direccion_temp[0]);
        $unidad = strtolower($unidad_temp[0]);
        $oficina = strtolower($oficina_temp[0]);

        if ($instituto == "instituto nacional de innovacion agropecuaria y forestal" && $direccion == "direccion nacional de semillas" && $unidad == "unidad de certificacion y control de semillas" && $oficina == "oficina departamental chuquisaca") {
            $campos = count($data[7]);

            #echo $instituto,'=',$total_filas,'=',count($campos[0]),'=',count($data[7]);exit;
            //contar campos de la primera fila
            if ($campos > 1) {
                $indice = 0;

                //contador de campos
                for ($row = 7; $row < $total_filas; $row++) {
                    $sql = '';
                    $nombre_archivo = "csvToBDv5";
                    $file = fopen("../vista/temp/" . $nombre_archivo . ".sql", "a") or die("Problemas en la creacion del archivo");

                    //nro de solicitud
                    ###$nro_solicitud = $data[$row][0];
                    $nro_solicitud = $data[$row][0];
                    $sistema = ($data[$row][1]);
                    $isistema = Sistema::setSistemaImport($sistema);
                    $provincia = $data[$row][2];
                    $municipio = $data[$row][4];
                    $imunicipio = Municipio::setMunicipioImport($municipio, $provincia);
                    $comunidad = Comunidad::getComunidadImportar($data[$row][5]);
                    $isemillera = Semillera::setSemilleraImport($data[$row][7], $nro_solicitud);
                    $semillerista = Funciones::getNombreApellidoImportar($data[$row][8]);
                    $isemillerista = Semillerista::setSemilleristaImport(utf8_decode($semillerista['nombre']), utf8_decode($semillerista['apellido']), $isemillera);
                    $fecha_solicitud = Funciones::cambiar_tipo_mysql_import($data[$row][9]);
                    #insertar solicitud
                    Solicitud::insertSolicitud($isemillera, $isistema, $isemillerista, $imunicipio, $comunidad, $fecha_solicitud);
                    $isolicitud = DBConnector::lastInsertId();

                    //////////////////////////////////////////////////////////////////////////////SEMILLA//////////////////////////
                    if (strtolower(substr($sistema, 0, 9)) == 'certifica') {

                        $nro_campo = Semilla::getNroCampoImportar($data[$row][10]);
                        $campanha = Campanha::getIdCampanhaImportar($data[$row][11]);
                        $icultivo = Cultivo::getIdCultivoImportar($data[$row][12]);
                        $ivariedad = Variedad::getIdVariedadImportar($data[$row][13], $icultivo);
                        $icat_sembrada = Categoria::getIdCategoriaByNameAndGeneracionImportar($data[$row][14]);
                        $categoria_producir = $data[$row][15];
                        $icat_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar($categoria_producir);
                        $semilla_empleada = ($data[$row][16]) ? $data[$row][16] : 0;
                        $lote = Semilla::getLoteImportar($data[$row][17]);
                        $fecha_siembra = Funciones::cambiar_tipo_mysql_import($data[$row][18]);
                        $plantas_hectarea = Semilla::getPlantasHectareasImportar($data[$row][19]);
                        $icultivo_anterior = Cultivo::getIdCultivoImportar($data[$row][20]);
                        $isuperficie_parcela = Superficie::getIdSuperficieBySuperficieImportar($data[$row][21]);

                        if (count($lote) > 1) {
                            //iterar sobre la cantidad de lotes
                            foreach ($lote as $key => $lote_valor) {
                                //id de la superficie inscrita
                                $id_inscrita = $isuperficie_parcela;
                                Semilla::insertSemillaImportar($nro_campo, $campanha, $icultivo, $ivariedad, $icat_sembrada, $icat_producir, $semilla_empleada, $lote_valor, $fecha_siembra, $plantas_hectarea, $icultivo_anterior, $id_inscrita);
                                //obtenemos el id
                                $iSemilla = DBConnector::lastInsertId();
                                //agregamos el id en la tabla semilla
                                $sql .= "UPDATE semilla SET superficie_parcela = $id_inscrita WHERE id_semilla=$iSemilla;\r\n";
                                //agregamos valores de los id en la tabla de relacion
                                $sql .= "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                                //agregamos la superficie de la parcela en la tabla superficie
                                #Superficie::setInscrita($id_inscrita, $iSemilla);
                                $sql .= "INSERT INTO superficie (inscrita,id_semilla) VALUES ($id_inscrita,$iSemilla );\r\n";
                                //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                                ////Solicitud::updateEstado($isolicitud);
                                $sql .= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;\r\n";
                                //cambia el estado de semilla a 1
                                ////Semilla::updateEstado($iSemilla);
                                $sql .= "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;\r\n";
                            }
                        } else {
                            $id_inscrita = $isuperficie_parcela;
                            Semilla::insertSemillaImportar($nro_campo, $campanha, $icultivo, $ivariedad, $icat_sembrada, $icat_producir, $semilla_empleada, '', $fecha_siembra, $plantas_hectarea, $icultivo_anterior, $id_inscrita);
                            if (DBConnector::nroError())
                                $sql .= "semilla :" . DBConnector::mensaje();
                            ////$sql = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                            ////$sql .= "'$nro_campo',$campanha,$icultivo,$ivariedad,$icat_sembrada,$icat_producir,$semilla_empleada,'','$fecha_siembra',$plantas_hectarea,$icultivo_anterior,$isuperficie_parcela);\r\n";
                            //obtenemos el id
                            $iSemilla = DBConnector::lastInsertId();
                            //agregamos el id en la tabla semilla
                            ####Semilla::updateSemilla($iSemilla, $id_inscrita);
                            $sql .= "UPDATE semilla SET superficie_parcela = $id_inscrita WHERE id_semilla=$iSemilla;\r\n";
                            #echo "updateSemilla ",DBConnector::mensaje();
                            //agregamos valores de los id en la tabla de relacion
                            ####Tiene::solicitud_semilla($isolicitud, $iSemilla);
                            $sql .= "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                            #echo "tiene ",DBConnector::mensaje();
                            //agregamos la superficie de la parcela en la tabla superficie
                            #Superficie::setInscrita($id_inscrita, $iSemilla);
                            $sql .= "INSERT INTO superficie (inscrita,id_semilla) VALUES ($id_inscrita,$iSemilla );\r\n";
                            //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                            #####Solicitud::updateEstado($isolicitud);
                            $sql .= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;\r\n";
                            //cambia el estado de semilla a 1
                            #####Semilla::updateEstado($iSemilla);
                            $sql .= "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;\r\n";
                        }
                    } else {//fiscalizacion
                        $nro_campo_temp = explode("/", $nro_solicitud);
                        $nro_campo = $nro_campo_temp[0];
                        //Cultivo
                        $icultivo = Cultivo::getIdCultivoImportar($data[$row][12]);
                        //Variedad
                        $ivariedad = Variedad::getIdVariedadImportar($data[$row][13], $icultivo);
                        $categoria_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar(Semilla::getCategoriaProducirImportarFiscal($data[$row][15]));
                        $lote = Semilla::getLoteFiscalImportar($data[$row][17]);

                        Semilla::insertSemillaFiscalImportar($nro_campo, $icultivo, $ivariedad, $categoria_producir, $lote);
                        $iSemilla = DBConnector::lastInsertId();
                        $sql .= "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                        $sql .= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;\r\n";
                        $sql .= "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;\r\n";
                    }

                     /*
                     ////////////////////////////////////////////////////INSPECCION/////////////////
                     if (strtolower(substr($sistema, 0, 9)) == 'certifica') {
                     $sql .= "INSPECCION certificacion sistema -- nro solicitud $nro_solicitud  ";
                     $primera = Funciones::cambiar_tipo_mysql_import($data[$row][25]);
                     $segunda = Funciones::cambiar_tipo_mysql_import($data[$row][26]);
                     $tercera = Funciones::cambiar_tipo_mysql_import($data[$row][27]);
                     $sql .= "$primera??$segunda??$tercera\r\n";
                     Inspeccion::setInspeccionImportar($primera, $segunda, $tercera);
                     $idInspeccion = DBConnector::lastInsertId();
                     #$sql .= "INSERT INTO inspeccion (primera,segunda,tercera) VALUES ('$primera','$segunda','$tercera');\r\n";
                     $numero_inspeccion = 0;
                     if ($primera != '' && $segunda == '') {
                     #Solicitud::updateStadoByNumber(5, $isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 5 WHERE id_solicitud = $isolicitud;\r\n";
                     $numero_inspeccion = 1;
                     #Semilla::updateEstadoSemillaImportar($iSemilla);
                     $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                     #echo "primer semilla ",DBConnector::mensaje();
                     } elseif ($segunda != '' && $tercera == '') {
                     #Solicitud::updateStadoByNumber(6, $isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 6 WHERE id_solicitud = $isolicitud;\r\n";
                     $numero_inspeccion = 2;
                     #Semilla::updateEstadoSemillaImportar($iSemilla);
                     $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                     } elseif ($tercera != '') {
                     #Solicitud::updateStadoByNumber(7, $isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 7 WHERE id_solicitud = $isolicitud;\r\n";
                     $numero_inspeccion = 3;
                     #Semilla::updateEstadoSemillaImportar($iSemilla);
                     $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                     } else
                     #Semilla::updateEstadoSemillaImportar($iSemilla);
                     $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                     //actualizar estado de solicitud
                     #Solicitud::updateEstado($isolicitud);
                     $sql .= "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                     //actualizar estado de inspeccion
                     #Inspeccion::updEstadoImportar($numero_inspeccion, $idInspeccion);
                     $sql .= "UPDATE inspeccion SET estado = $numero_inspeccion WHERE id_inspeccion=$idInspeccion;\r\n";
                     }*/
                    /*
                     //////////////////////////////////HOJA COSECHA/////////////////
                     if (strtolower(substr($sistema, 0, 9)) == 'certifica') {
                     $laboratorio = HojaCosecha::getLaboratorioImportar($data[$row][12]);
                     $nro_campo = HojaCosecha::getNroCampoImportarv2($data[$row][28]);
                     $fecha_emision_hoja = Funciones::getFechaImportarv2($data[$row][29]);
                     $cat_campo = HojaCosecha::getCategoriaCampoImportar($data[$row][30]);
                     $rendimiento_estimado = HojaCosecha::getRendimientoEstimadoImportar($data[$row][31]);
                     $superficie = HojaCosecha::getSuperficieParcelaImportar($data[$row][32]);
                     $rendimiento_campo = HojaCosecha::getRendimientoCampoImportar($data[$row][33]);
                     $nro_cupones = HojaCosecha::getNroCuponesImportar($data[$row][34]);
                     $planta_acondicionadora = HojaCosecha::getPlantaAcondicionadoraImportar($data[$row][35]);
                     $estado = 6;
                     // ??
                     HojaCosecha::setHojaCosechaImport($iSemilla, $fecha_emision_hoja, $cat_campo, $rendimiento_estimado, $superficie, $rendimiento_campo, $nro_cupones, $planta_acondicionadora, $laboratorio, $estado, $iSuperficie);
                     #$sql .= "INSERT INTO cosecha (nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado, superficie_parcela,rendimiento_campo,";
                     #$sql .= "nro_cupones,planta_acondicionadora,alaboratorio,estado,id_superficie) VALUES('";
                     #$sql .= "$nro_campo','$fecha_emision_hoja',$cat_campo,$rendimiento_estimado,$superficie, $rendimiento_campo, $nro_cupones, '$planta_acondicionadora',";
                     #$sql .= "$laboratorio,$estado,$iSuperficie);\r\n";
                     $iCosecha = DBConnector::lastInsertId();
                     $cultivo_name = $data[$row][12];
                     $estadoSolicitud = Solicitud::getEstadoSolicitud($isolicitud);
                     if ($cultivo_name != 'papa')
                     #Solicitud::updateEstado4($isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                     else {
                     if ($estadoSolicitud == 6) {
                     #Solicitud::updateEstadoCosecha($isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                     } else {
                     #Solicitud::updateEstado4($isolicitud);
                     $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                     }
                     }
                     Inspeccion::updInspeccion($iSuperficie);
                     $sql .= "UPDATE superficie SET estado=estado+1 WHERE id_superficie=$iSuperficie;\r\n";
                     }

                     //////////////////////////////////////////////SEMILLA PRODUCIDA/////////////////
                     $semilla_neta = SemillaProd::getSemillaProducidaImport($data[$row][36]);
                     $categoria_obtenida = SemillaProd::getCategoriaObtenidaImportar($data[$row][37], $data[$row][30]);
                     $igeneracion = SemillaProd::getGeneracionCategoriaObtenidaImportar($data[$row][37]);
                     $nro_etiquetas = SemillaProd::getNroEtiquetasImportar($data[$row][38]);
                     $rango = SemillaProd::getRangoImportar($data[$row][39]);

                     ###if (empty($semilla_neta) && empty($nro_etiquetas) && empty($rango)) {
                     $semilla_neta = $nro_etiquetas = $rango = 0;
                     ###SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, 1, 1, $nro_etiquetas, $rango);
                     #$sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,id_generacion,semilla_neta,categoria_obtenida,nro_etiqueta,rango) semilla_producida VALUES ";
                     #$sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                     ###} else {
                     #echo $isolicitud, '=',$iSemilla, '=',$categoria_obtenida, '=',$semilla_neta, '=',$nro_etiqueta, '=',$rango;
                     ###SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, 1, 1, $nro_etiquetas, $rango);
                     #$sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES ";
                     #$sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                     ###}

                     #Solicitud::updateEstadoBySemillaProd($isolicitud);
                     $query = "UPDATE solicitud SET estado = 10 WHERE id_solicitud = $isolicitud;\r\n";
                     if ($cultivo_name != 'papa')
                     #HojaCosecha::updateStado($iCosecha);
                     $sql .= "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $iCosecha AND estado = 0);\r\n";
                     #Solicitud::updateEstado($isolicitud);
                     $query = "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";

                     ////////////////////////////////////////ANALISIS/////////////////

                     $nro_analisis = (!empty($data[$row][40]))?Laboratorio::getNroAnalisisImportar($data[$row][40], $nro_solicitud, $sistema):$nro_campo_temp[0];
                     $fecha_recepcion = Laboratorio::getFechaImportar($data[$row][41]);
                     $fecha_resultado = Laboratorio::getFechaImportar($data[$row][42]);
                     $lote = Semilla::getLoteImportar($data[$row][43]);
                     $pureza = Resultado_muestras::getPurezaImportar($data[$row][44]);
                     $germinacion = Resultado_muestras::getGerminacionImportar($data[$row][45]);
                     $humedad = Resultado_muestras::getHumedadImportar($data[$row][46]);
                     $fecha_actualizacion = Resultado_muestras::getFechaActualizacionImportar($data[$row][47]);
                     $germinacion_act = Resultado_muestras::getActualizacionGerminacionImportar($data[$row][48]);
                     $observaciones = ($fecha_actualizacion && $germinacion_act && strtolower(substr($sistema, 0,6))=='fiscal')?Resultado_muestras::getObservacionActualizacionImportar($data[$row][49]):Resultado_muestras::getObservacionImportar($data[$row][49]);
                     if (is_array($nro_analisis)) {
                     if (count($lote) > 1) {
                     foreach ($lote as $key => $lote_valor) {
                     $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza[$key], $germinacion[$key], $humedad[$key]);
                     Muestra_laboratorio::setMuestraLaboratorioImportar($nro_analisis[$key], $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                     $imuestra = DBConnector::lastInsertId();
                     $iresultado = Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote_valor, $pureza[$key], $germinacion[$key], $humedad[$key], $observaciones, $imuestra);
                     $sql .= "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;\r\n";
                     }
                     } else {
                     $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                     Muestra_laboratorio::setMuestraLaboratorioImportar($nro_analisis[0], $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                     $imuestra = DBConnector::lastInsertId();
                     $iresultado = Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $imuestra);
                     $sql .= "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;\r\n";
                     }

                     } else {
                     $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                     Muestra_laboratorio::setMuestraLaboratorioImportar($nro_analisis, $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                     $imuestra = DBConnector::lastInsertId();
                     $iresultado = Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $imuestra);
                     $sql .= "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;\r\n";
                     }
                     */
                    fputs($file, $sql);
                    fclose($file);
                }
                return $nombre_archivo;
            } else {
                return 'error';
                //no hay datos suficientes en el archivo
            }
        }
    }

    public static function importarCSVToBDv2($archivo) {
        //leer archivo e ingresar datos a un array
        $csvFile = file($archivo);
        $data = array();
        foreach ($csvFile as $line) {
            array_push($data, str_getcsv($line, ","));
        }
        $total_filas = count($data);
        //total de filas

        $instituto_temp = explode(",", $data[0][0]);
        $direccion_temp = explode(",", $data[1][0]);
        $unidad_temp = explode(",", $data[2][0]);
        $oficina_temp = explode(",", $data[3][0]);

        $instituto = strtolower($instituto_temp[0]);
        $direccion = strtolower($direccion_temp[0]);
        $unidad = strtolower($unidad_temp[0]);
        $oficina = strtolower($oficina_temp[0]);

        if ($instituto == "instituto nacional de innovacion agropecuaria y forestal" && $direccion == "direccion nacional de semillas" && $unidad == "unidad de certificacion y control de semillas" && $oficina == "oficina departamental chuquisaca") {
            $campos = count($data[7]);

            #echo $instituto,'=',$total_filas,'=',count($campos[0]),'=',count($data[7]);exit;
            //contar campos de la primera fila
            if ($campos > 1) {
                $indice = 0;

                //contador de campos
                for ($row = 7; $row < $total_filas; $row++) {
                    $sql = '';
                    //datos
                    $nro_solicitud_data = $data[$row][0];
                    $sistema_data = ($data[$row][1]);
                    $departamento_data = $data[$row][2];
                    $provincia_data = $data[$row][3];
                    $municipio_data = $data[$row][4];
                    $comunidad_data = $data[$row][5];
                    $semillera_data = $data[$row][7];
                    $semillerista_data = $data[$row][8];
                    $fecha_solicitud_data = $data[$row][9];
                    $nro_campo_data = $data[$row][10];
                    $campanha_data = $data[$row][11];
                    $cultivo_data = $data[$row][12];
                    $variedad_data = $data[$row][13];
                    $categoria_sembrada_data = $data[$row][14];
                    $categoria_producir_data = $data[$row][15];
                    $semilla_empleada_data = $data[$row][16];
                    $lote_data = $data[$row][17];
                    $fecha_siembra_data = $data[$row][18];
                    $plantas_hectarea_data = $data[$row][19];
                    $cultivo_anterior_data = $data[$row][20];
                    $superficie_parcela_data = $data[$row][21];
                    $rechazada_data = $data[$row][22];
                    $retirada_data = $data[$row][23];
                    $aprobada_data = $data[$row][24];
                    $primera_data = $data[$row][25];
                    $segunda_data = $data[$row][26];
                    $tercera_data = $data[$row][27];
                    $fecha_emision_hoja_data = $data[$row][29];
                    $categoria_campo_data = $data[$row][30];
                    $rendimiento_estimado_data = $data[$row][31];
                    $superficie_cosecha_data = $data[$row][32];
                    $rendimiento_campo_data = $data[$row][33];
                    $nro_cupones_data = $data[$row][34];
                    $planta_acondicionadora_data = $data[$row][35];
                    $semilla_neta_data = $data[$row][36];
                    $categoria_obtenida_data = $data[$row][37];
                    $nro_etiquetas_data = $data[$row][38];
                    $rango_data = $data[$row][39];
                    $nro_analisis_data = $data[$row][40];
                    $fecha_recepcion_data = $data[$row][41];
                    $fecha_resultado_data = $data[$row][42];
                    $lote_laboratorio_data = $data[$row][43];
                    $pureza_data = $data[$row][44];
                    $germinacion_data = $data[$row][45];
                    $humedad_data = $data[$row][46];
                    $fecha_actualizacion_data = $data[$row][47];
                    $germinacion_actualizacion_data = $data[$row][48];
                    $observaciones_data = $data[$row][49];

                    if ((strtolower(substr($sistema_data, 0, 9)) == 'certifica') && !empty($data[$row+1][0])) {
                        /////////////////////////////////////////////S  O  L  I  C  I  T  U  D  ////////////////////////////////////
                        $isistema = Sistema::setSistemaImport($sistema_data);
                        $imunicipio = Municipio::setMunicipioImport($municipio_data, $provincia_data);
                        $comunidad = Comunidad::getComunidadImportar($comunidad_data);
                        $isemillera = Semillera::setSemilleraImport($semillera_data, $nro_solicitud_data);
                        $semillerista = Funciones::getNombreApellidoImportar($semillerista_data);
                        $isemillerista = Semillerista::setSemilleristaImport(utf8_decode($semillerista['nombre']), utf8_decode($semillerista['apellido']), $isemillera);
                        $fecha_solicitud = Funciones::cambiar_tipo_mysql_importV2($fecha_solicitud_data);
                        #insertar solicitud
                        Solicitud::insertSolicitud($isemillera, $isistema, $isemillerista, $imunicipio, $comunidad, $fecha_solicitud);
                        $isolicitud = DBConnector::lastInsertId();
                        /////////////////////////////////////////////S  E  M  I  L  L  A  //////////////////////////////////////////
                        $nro_campo = Semilla::getNroCampoImportar($nro_campo_data);
                        $icampanha = Campanha::getIdCampanhaImportar($campanha_data);
                        $icultivo = Cultivo::getIdCultivoImportar($cultivo_data);
                        $ivariedad = Variedad::getIdVariedadImportar($variedad_data, $icultivo);
                        $icat_sembrada = Categoria::getIdCategoriaByNameAndGeneracionImportar($categoria_sembrada_data);
                        $icat_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar($categoria_producir_data);
                        $semilla_empleada = ($semilla_empleada_data) ? $semilla_empleada_data : 0;
                        $lote = Semilla::getLoteImportar($lote_data);
                        $fecha_siembra = Funciones::cambiar_tipo_mysql_import($fecha_siembra_data);
                        $plantas_hectarea = Semilla::getPlantasHectareasImportar(str_replace(",","",$plantas_hectarea_data));
                        $icultivo_anterior = Cultivo::getIdCultivoImportar($cultivo_anterior_data);
                        $isuperficie_parcela = Superficie::getIdSuperficieBySuperficieImportar($superficie_parcela_data);

                        if (count($lote) > 1) {
                            //iterar sobre la cantidad de lotes
                            foreach ($lote as $key => $lote_valor) {
                                //id de la superficie inscrita
                                $id_inscrita = $isuperficie_parcela;
                                Semilla::insertSemillaImportar($nro_campo, $icampanha, $icultivo, $ivariedad, $icat_sembrada, $icat_producir, $semilla_empleada, $lote_valor, $fecha_siembra, $plantas_hectarea, $icultivo_anterior, $id_inscrita);
                                ////$sql .= "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                                ////$sql .= "'$nro_campo',$campanha,$icultivo,$ivariedad,$icat_sembrada,$icat_producir,$semilla_empleada,'$lote_valor','$fecha_siembra',$plantas_hectarea,$icultivo_anterior,$isuperficie_parcela);\r\n";
                                //obtenemos el id
                                $iSemilla = DBConnector::lastInsertId();
                                //agregamos el id en la tabla semilla
                                ####Semilla::updateSemilla($iSemilla, $id_inscrita);
                                $sql_cer = "UPDATE semilla SET superficie_parcela = $id_inscrita WHERE id_semilla=$iSemilla;";
                                DBConnector::ejecutar($sql_cer);
                                //agregamos valores de los id en la tabla de relacion
                                ####Tiene::solicitud_semilla($isolicitud, $iSemilla);
                                $sql_cer = "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                                DBConnector::ejecutar($sql_cer);
                                //agregamos la superficie de la parcela en la tabla superficie
                                #Superficie::setInscrita($id_inscrita, $iSemilla);
                                $sql_cer = "INSERT INTO superficie (inscrita,id_semilla) VALUES ($id_inscrita,$iSemilla );\r\n";
                                DBConnector::ejecutar($sql_cer);
                                //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                                ////Solicitud::updateEstado($isolicitud);
                                $sql_cer = "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;";
                                DBConnector::ejecutar($sql_cer);
                                //cambia el estado de semilla a 1
                                ////Semilla::updateEstado($iSemilla);
                                $sql_cer = "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;";
                                DBConnector::ejecutar($sql_cer);
                            }
                        } else {
                            $id_inscrita = $isuperficie_parcela;
                            Semilla::insertSemillaImportar($nro_campo, $icampanha, $icultivo, $ivariedad, $icat_sembrada, $icat_producir, $semilla_empleada, $lote[0], $fecha_siembra, $plantas_hectarea, $icultivo_anterior, $id_inscrita);

                            ////$sql = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                            ////$sql .= "'$nro_campo',$campanha,$icultivo,$ivariedad,$icat_sembrada,$icat_producir,$semilla_empleada,'','$fecha_siembra',$plantas_hectarea,$icultivo_anterior,$isuperficie_parcela);\r\n";
                            //obtenemos el id
                            $iSemilla = DBConnector::lastInsertId();
                            //agregamos el id en la tabla semilla
                            ####Semilla::updateSemilla($iSemilla, $id_inscrita);
                            $query_cer = "UPDATE semilla SET superficie_parcela = $id_inscrita WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($query_cer);
                            #echo "updateSemilla ",DBConnector::mensaje();
                            //agregamos valores de los id en la tabla de relacion
                            ####Tiene::solicitud_semilla($isolicitud, $iSemilla);
                            $sql_cer = "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);";
                            DBConnector::ejecutar($sql_cer);
                            $sql_cer = "INSERT INTO superficie (inscrita,id_semilla) VALUES ($id_inscrita,$iSemilla );";
                            DBConnector::ejecutar($sql_cer);
                            $sql_cer= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_cer);
                            $sql_cer = "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($sql_cer);
                        }
                        /////////////////////////////////////////////S  U  P  E  R  F  I  C  I  E  ////////////////////////////////////
                        $iInscrita = Superficie::getImportarInscrita($superficie_parcela_data);
                        $rechazada = Superficie::getImportarRechazada($rechazada_data);
                        $retirada = Superficie::getImportarRetirada($retirada_data);
                        $aprobada = Superficie::getImportarAprobada($aprobada_data);

                        #$sql .= "--- inscrita: " . $data[$row][21] . " rechazada: $rechazada -  retirada:$retirada  -  aprobada:$aprobada\r\n";
                        $iSuperficie = Superficie::setInscrita($iInscrita, $iSemilla);
                        $sql_cer = "UPDATE superficie SET retirada = $retirada WHERE id_semilla=1;";
                        DBConnector::ejecutar($sql_cer);
                        $sql_cer = "UPDATE superficie SET rechazada = $rechazada WHERE id_semilla=1;";
                        DBConnector::ejecutar($sql_cer);
                        $sql_cer = "UPDATE superficie SET aprobada = $aprobada WHERE id_semilla=1;";
                        DBConnector::ejecutar($sql_cer);
                        $sq_cer = "UPDATE solicitud SET estado = 4 WHERE id_solicitud = 1;";
                        DBConnector::ejecutar($sql_cer);
                        $sql_cer = "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=1;";
                        DBConnector::ejecutar($sql_cer);
                        if ($superficie_parcela_data == 0.00 && $aprobada == 0.00) {
                            $sql_cer = "UPDATE superficie SET estado = 0 WHERE id_semilla = 1;";
                        } else {
                            $sql_cer = "UPDATE superficie SET estado = 3 WHERE id_semilla = 1;";
                        }
                        DBConnector::ejecutar($sql_cer);
                        /////////////////////////////////////////////I  N  S  P  E  C  C  I  O  N  ////////////////////////////////////
                        $primera = Funciones::cambiar_tipo_mysql_import($primera_data);
                        $segunda = Funciones::cambiar_tipo_mysql_import($segunda_data);
                        $tercera = Funciones::cambiar_tipo_mysql_import($tercera_data);

                        Inspeccion::setInspeccionImportar($primera, $segunda, $tercera);
                        $idInspeccion = DBConnector::lastInsertId();
                        $numero_inspeccion = 0;
                        if ($primera != '' && $segunda == '') {
                            $sql_cer = "UPDATE solicitud SET estado = 5 WHERE id_solicitud = $isolicitud;\r\n";
                            DBConnector::ejecutar($sql_cer);
                            $numero_inspeccion = 1;
                            $sql_cer = "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                            DBConnector::ejecutar($sql_cer);
                            #echo "primer semilla ",DBConnector::mensaje();
                        } elseif ($segunda != '' && $tercera == '') {
                            #Solicitud::updateStadoByNumber(6, $isolicitud);
                            $sql_cer = "UPDATE solicitud SET estado = 6 WHERE id_solicitud = $isolicitud;\r\n";
                            DBConnector::ejecutar($sql_cer);
                            $numero_inspeccion = 2;
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql_cer = "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($sql_cer);
                        } elseif ($tercera != '') {
                            #Solicitud::updateStadoByNumber(7, $isolicitud);
                            $sql_cer = "UPDATE solicitud SET estado = 7 WHERE id_solicitud = $isolicitud;";
                            $numero_inspeccion = 3;
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql_cer = "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($sql_cer);
                        } else {
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql_cer = "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($sql_cer);
                        }
                        //actualizar estado de solicitud
                        #Solicitud::updateEstado($isolicitud);
                        $sql_cer= "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;";
                        DBConnector::ejecutar($sql_cer);
                        //actualizar estado de inspeccion
                        #Inspeccion::updEstadoImportar($numero_inspeccion, $idInspeccion);
                        $sql_cer = "UPDATE inspeccion SET estado = $numero_inspeccion WHERE id_inspeccion=$idInspeccion;";
                        DBConnector::ejecutar($sql_cer);
                        #relacion superficie-inspeccion
                        $sql_cer= "INSERT INTO superficie_inspeccion(id_superficie,id_inspeccion) VALUES ($iSuperficie,$idInspeccion)";
                        DBConnector::ejecutar($sql_cer);
                        ///////////////////////////////////////////H  O  J  A     C  O  S  E  C  H  A  ////////////////////////////////
                        $laboratorio = HojaCosecha::getLaboratorioImportar($cultivo_data);
                        $nro_campo = HojaCosecha::getNroCampoImportarv2($nro_campo_data);
                        $fecha_emision_hoja = Funciones::getFechaImportarv2($fecha_emision_hoja_data);
                        $categoria_campo = HojaCosecha::getCategoriaCampoImportar($categoria_campo_data);
                        $rendimiento_estimado = HojaCosecha::getRendimientoEstimadoImportar($rendimiento_estimado_data);
                        $superficie = HojaCosecha::getSuperficieParcelaImportar($superficie_cosecha_data);
                        $rendimiento_campo = HojaCosecha::getRendimientoCampoImportar($rendimiento_campo_data);
                        $nro_cupones = HojaCosecha::getNroCuponesImportar($nro_cupones_data);
                        $planta_acondicionadora = HojaCosecha::getPlantaAcondicionadoraImportar($planta_acondicionadora_data);
                        $estado = 6;
                        $iSuperficie2 = Superficie::getIdSuperficieByIdSemilla($iSemilla);

                        HojaCosecha::setHojaCosechaImport($iSemilla, $fecha_emision_hoja, $categoria_campo, $rendimiento_estimado, $superficie, $rendimiento_campo, $nro_cupones, $planta_acondicionadora, $laboratorio, $estado, $iSuperficie2);
                        $iCosecha = DBConnector::lastInsertId();
                        $cultivo_name = $data[$row][12];
                        $estadoSolicitud = Solicitud::getEstadoSolicitud($isolicitud);
                        if ($cultivo_name != 'papa') {
                            $sql_cer = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_cer);
                        } else {
                            if ($estadoSolicitud == 6) {
                                $sql_cer = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;";
                                DBConnector::ejecutar($sql_cer);
                            } else {
                                #Solicitud::updateEstado4($isolicitud);
                                $sql_cer = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;";
                                DBConnector::ejecutar($sql_cer);
                            }
                        }
                        Inspeccion::updInspeccion($iSuperficie);
                        $sql_cer = "UPDATE superficie SET estado=estado+1 WHERE id_superficie=$iSuperficie;";
                        DBConnector::ejecutar($sql_cer);
                        
                        /////////////////////////////////////////////A  N  A  L  I  S  I  S  //////////////////////////////////////////
                        $nro_analisis = (!empty($nro_analisis_data)) ? Laboratorio::getNroAnalisisImportar($nro_analisis_data, $nro_solicitud_data, $sistema_data) : 1;
                        $fecha_recepcion = Laboratorio::getFechaImportarv2($fecha_recepcion_data);
                        $lote = Semilla::getLoteImportar($lote_laboratorio_data);
                        $pureza = Resultado_muestras::getPurezaImportar($pureza_data);
                        $germinacion = Resultado_muestras::getGerminacionImportar($germinacion_data);
                        $humedad = Resultado_muestras::getHumedadImportar($humedad_data);
                        $observaciones = Resultado_muestras::getObservacionImportar($observaciones_data);
                        $fecha_resultado = Laboratorio::getFechaImportarv2($fecha_resultado_data);
                        $fecha_actualizacion = Resultado_muestras::getFechaActualizacionImportar($fecha_actualizacion_data);
                        $germinacion_actualizacion = Resultado_muestras::getActualizacionGerminacionImportar($germinacion_actualizacion_data);

                        if (is_array($nro_analisis)) {
                            if (count($lote) > 1) {
                                foreach ($lote as $key => $lote_valor) {
                                    $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza[$key], $germinacion[$key], $humedad[$key]);
                                    Muestra_laboratorio::setMuestraLaboratorioImportar($nro_analisis[$key], $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                                    $imuestra = DBConnector::lastInsertId();
                                    /////////////////////////////////////////////R  E  S  U  L  T  A  D  O  ///////////////////////////////////////

                                    if (is_array($fecha_resultado)) {// son varias fechas
                                        $nro_lotes = count($lote);
                                        $i = 0;
                                        while ($i < $nro_lotes) {
                                            Resultado_muestras::setResultadoMuestraImportar($fecha_resultado[$i], $lote[$i], $pureza[$i], $germinacion[$i], $humedad[$i], $observaciones, $imuestra);
                                            $iresultado = DBConnector::lastInsertId();
                                            if ($fecha_actualizacion != '') {
                                                Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                            }
                                            $sql_cer = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                            DBConnector::ejecutar($sql_cer);
                                            $i++;
                                        }
                                    } else {//si es solo una fecha
                                        //si hay varios lotes
                                        if (count($lote) > 1) {
                                            foreach ($lote as $key => $lote_valor) {
                                                #echo "Resultado de variable pureza : ".$lote_valor.'=';
                                                Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote_valor, $pureza[$key], $germinacion[$key], $humedad[$key], $observaciones, $imuestra);
                                                $iresultado = DBConnector::lastInsertId();
                                                if ($fecha_actualizacion != '') {
                                                    Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                                }
                                                $sql_cer = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                                DBConnector::ejecutar($sql_cer);
                                            }
                                        } else {//solo es un lote
                                            Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $imuestra);
                                            $iresultado = DBConnector::lastInsertId();
                                            if ($fecha_actualizacion != '') {
                                                Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                            }
                                            $sql_cer = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                            DBConnector::ejecutar($sql_cer);

                                        }
                                    }
                                }
                            } else {
                                $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                                $temp =is_array($nro_analisis)?$nro_analisis[0]:$nro_analisis;
                                Muestra_laboratorio::setMuestraLaboratorioImportar($temp, $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                                $imuestra = DBConnector::lastInsertId();
                                /////////////////////////////////////////////R  E  S  U  L  T  A  D  O  ///////////////////////////////////////

                                if (is_array($fecha_resultado)) {// son varias fechas
                                    $nro_lotes = count($lote);
                                    $i = 0;
                                    while ($i < $nro_lotes) {
                                        Resultado_muestras::setResultadoMuestraImportar($fecha_resultado[$i], $lote[$i], $pureza[$i], $germinacion[$i], $humedad[$key], $observaciones, $imuestra);
                                        $iresultado = DBConnector::lastInsertId();
                                        if ($fecha_actualizacion != '') {
                                            Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                        }
                                        $sql_cer = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                        DBConnector::ejecutar($sql_cer);
                                        $i++;
                                    }
                                } else {//si es solo una fecha
                                    //si hay varios lotes
                                    if (count($lote) > 1) {
                                        foreach ($lote as $key => $lote_valor) {
                                            #echo "Resultado de variable pureza : ".$lote_valor.'=';
                                            Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote_valor, $pureza[$key], $germinacion[$key], $humedad[$key], $observaciones, $imuestra);
                                            $iresultado = DBConnector::lastInsertId();
                                            if ($fecha_actualizacion != '') {
                                                Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                            }
                                            $sql_cer = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                            DBConnector::ejecutar($sql_cer);
                                        }
                                    } else {//solo es un lote
                                        Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $imuestra);
                                        $iresultado = DBConnector::lastInsertId();
                                        if ($fecha_actualizacion != '') {
                                            Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                        }
                                        $sql_2 = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                        DBConnector::ejecutar($sql_2);
                                    }
                                }
                            }
                        } else {
                            if (!empty($nro_analisis)) {
                                $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                                Muestra_laboratorio::setMuestraLaboratorioImportar($nro_analisis, $fecha_recepcion, $iSemilla, $comunidad, $ianalisis, $isistema);
                                $imuestra = DBConnector::lastInsertId();
                                /////////////////////////////////////////////R  E  S  U  L  T  A  D  O  ///////////////////////////////////////

                                if (is_array($fecha_resultado)) {// son varias fechas
                                    $nro_lotes = count($lote);
                                    $i = 0;
                                    while ($i < $nro_lotes) {
                                        Resultado_muestras::setResultadoMuestraImportar($fecha_resultado[$i], $lote[$i], $pureza[$i], $germinacion[$i], $humedad[$key], $observaciones, $imuestra);
                                        $iresultado = DBConnector::lastInsertId();
                                        if ($fecha_actualizacion != '') {
                                            Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                        }
                                        $sql_2 = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                        DBConnector::ejecutar($sql_2);
                                        $i++;
                                    }
                                } else {//si es solo una fecha
                                    //si hay varios lotes
                                    if (count($lote) > 1) {
                                        foreach ($lote as $key => $lote_valor) {
                                            #echo "Resultado de variable pureza : ".$lote_valor.'=';
                                            Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote_valor, $pureza[$key], $germinacion[$key], $humedad[$key], $observaciones, $imuestra);
                                            $iresultado = DBConnector::lastInsertId();
                                            if ($fecha_actualizacion != '') {
                                                Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                            }
                                            $sql_2 = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                            DBConnector::ejecutar($sql_2);
                                        }
                                    } else {//solo es un lote
                                        Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $imuestra);
                                        $iresultado = DBConnector::lastInsertId();
                                        if ($fecha_actualizacion != '') {
                                            Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                                        }
                                        $sql_2 = "UPDATE muestra_laboratorio SET estado=1 WHERE id_muestra = $imuestra;";
                                        DBConnector::ejecutar($sql_2);
                                    }
                                }
                            }
                        }

                        $sql_2 = "UPDATE solicitud SET estado=11 WHERE id_solicitud=$isolicitud";
                        DBConnector::ejecutar($sql_2);
                        /////////////////////////////////////////////S  E  M  I  L  L  A   P  R  O  D  U  C  I  D  A  /////////////////
                        $semilla_neta = SemillaProd::getSemillaProducidaImport($semilla_neta_data);
                        $categoria_obtenida = SemillaProd::getCategoriaObtenidaImportar($categoria_obtenida_data, $categoria_campo_data);
                        $igeneracion = SemillaProd::getGeneracionCategoriaObtenidaImportar($categoria_obtenida_data);
                        $nro_etiquetas = SemillaProd::getNroEtiquetasImportar($nro_etiquetas_data);
                        $rango = SemillaProd::getRangoImportar($rango_data);
                        if (empty($semilla_neta) && empty($nro_etiquetas) && empty($rango)) {
                            $semilla_neta = $nro_etiquetas = $rango = 0;
                            SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $categoria_obtenida, $nro_etiquetas, $rango);
                        } else {
                            SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $categoria_obtenida, $nro_etiquetas, $rango);                           
                        }

                        #Solicitud::updateEstadoBySemillaProd($isolicitud);
                        $sql_cer = "UPDATE solicitud SET estado = 10 WHERE id_solicitud = $isolicitud;";
                        DBConnector::ejecutar($sql_cer);
                        if ($cultivo_name != 'papa') {
                            #HojaCosecha::updateStado($iCosecha);
                            $sql_cer = "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $iCosecha AND estado = 0);";
                            DBConnector::ejecutar($sql_cer);
                        }
                        #Solicitud::updateEstado($isolicitud);
                        $sql_cer = "UPDATE solicitud SET estado =12 WHERE id_solicitud = $isolicitud;";
                        DBConnector::ejecutar($sql_cer);
                    } else {
                        if (strtolower(substr($sistema_data, 0, 9)) == 'fiscaliza'){
                            #######FISCALIZACION#######
                            ########################################### S  O  L  I  C  I  T  U  D  #####################################
                            $isistema = Sistema::setSistemaImport($sistema_data);
                            $imunicipio = Municipio::setMunicipioImport($municipio_data, $provincia_data);
                            $comunidad = Comunidad::getComunidadImportar($comunidad_data);
                            $isemillera = Semillera::setSemilleraImport($semillera_data, $nro_solicitud_data);
                            $semillerista = Funciones::getNombreApellidoImportar($semillerista_data);
                            $isemillerista = Semillerista::setSemilleristaImport(utf8_decode($semillerista['nombre']), utf8_decode($semillerista['apellido']), $isemillera);
                            $fecha_solicitud = Funciones::cambiar_tipo_mysql_import($fecha_solicitud_data);
                            #insertar solicitud
                            Solicitud::insertSolicitud($isemillera, $isistema, $isemillerista, $imunicipio, $comunidad, $fecha_solicitud);
                            $isolicitud = DBConnector::lastInsertId();
                            ########################################### S  E  M  I  L  L  A  ############################################
                            $nro_campo_temp = explode("/", $nro_solicitud_data);
                            $nro_campo = $nro_campo_temp[0];
                            //Cultivo
                            $icultivo = Cultivo::getIdCultivoImportar($cultivo_data);
                            //Variedad
                            $ivariedad = Variedad::getIdVariedadImportar($variedad_data, $icultivo);
                            $categoria_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar(Semilla::getCategoriaProducirImportarFiscal($categoria_producir_data));
                            $lote = Semilla::getLoteFiscalImportar($lote_data);
                            $isuperficie_parcela = Superficie::getIdSuperficieBySuperficieImportar($superficie_parcela_data);
    
                            Semilla::insertSemillaFiscalImportar($nro_campo, $icultivo, $ivariedad, $categoria_producir, $lote);
                            $iSemilla = DBConnector::lastInsertId();
                            $id_inscrita = $isuperficie_parcela;
                            
                            $sql_fis = "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);";
                            DBConnector::ejecutar($sql_fis);
                            $sql_fis = "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_fis);
                            $sql_fis = "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;";
                            DBConnector::ejecutar($sql_fis);
                            ##########################################  S  U  P  E  R  F  I  C  I  E  #################################
                            $iSuperficie = Superficie::setInscritaImportar(0, $iSemilla);
                            ##########################################  H  O  J  A    C  O  S  E  C  H  A  #################################
                            //SOLO PLANTA ACONDICIONADORA
                            #$laboratorio = HojaCosecha::getLaboratorioImportar($cultivo);
                            $estado = 6;
                            $planta_acondicionadora = HojaCosecha::getPlantaAcondicionadoraImportar($planta_acondicionadora_data);
                            
                            HojaCosecha::setHojaCosechaFiscalImport($iSemilla, $estado, $planta_acondicionadora);
                            $iCosecha = DBConnector::lastInsertId();
                            $sql_fis = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_fis);
                            
                            ##########################################  A  N  A  L  I  S  I  S  ############################################
                            $nro_analisis_temp = Laboratorio::getNroAnalisisImportar($nro_analisis_data, $nro_solicitud_data, $sistema_data);
                            $fecha_recepcion = Laboratorio::getFechaImportar($fecha_recepcion_data);
                            $nro_analisis = is_array($nro_analisis_temp)?$nro_analisis_temp[0]:$nro_analisis_temp;
                            Muestra_laboratorio::setMuestraLaboratorioFiscalImportar($nro_analisis, $fecha_recepcion, $iSemilla, $comunidad_data);
                            
                            $iMuestra = DBConnector::lastInsertId();
                            $sql_fis = "UPDATE solicitud SET estado = 9 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_fis);
                            ##########################################  R  E  S  U  L  T  A  D  O  #########################################
                            $fecha_resultado = Laboratorio::getFechaImportar($fecha_resultado_data);
                            $lote = Semilla::getLoteImportar($lote_laboratorio_data);
                            $pureza = Resultado_muestras::getPurezaImportar($pureza_data);
                            $germinacion = Resultado_muestras::getGerminacionImportar($germinacion_data);
                            $humedad = Resultado_muestras::getHumedadImportar($humedad_data);
                            $fecha_actualizacion = Resultado_muestras::getFechaActualizacionImportar($fecha_actualizacion_data);
                            $germinacion_actualizacion = Resultado_muestras::getActualizacionGerminacionImportar($germinacion_actualizacion_data);
                            $observaciones = Resultado_muestras::getObservacionImportar($observaciones_data);
    
                            Resultado_muestras::setResultadoMuestraImportar($fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $iMuestra);
                            $iresultado = DBConnector::lastInsertId();
                            if (!empty($germinacion_actualizacion))
                                #Resultado_muestras::setActualizacionResultadoMuestraImportar($fecha_resultado, $germinacion_actualizacion, $observaciones, $iMuestra);
                                Resultado_actualizacion::setResultadoActualizacion($iresultado, $fecha_actualizacion, $germinacion_actualizacion, $observaciones);
                            $sql_fis = "UPDATE solicitud SET estado = 11 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_fis);
                            #########################################  S  E  M  I  L  L  A    P  R  O  D  U  C  I  D  A  ##################
                            $semilla_neta = SemillaProd::getSemillaProducidaImport($semilla_neta_data);
                            $categoria_obtenida = SemillaProd::getCategoriaObtenidaFiscalImportar(strtolower($categoria_obtenida_data));
                            $igeneracion = SemillaProd::getGeneracionCategoriaObtenidaFiscalImportar(strtolower($categoria_obtenida_data));
                            $nro_etiquetas = SemillaProd::getNroEtiquetasImportar($nro_etiquetas_data);
                            $rango = SemillaProd::getRangoImportar($rango_data);
                            
                            if (empty($semilla_neta) && empty($nro_etiquetas) && empty($rango)) {
                                $semilla_neta = $nro_etiquetas = $rango = 0;
                                SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $categoria_obtenida, $nro_etiquetas, $rango);
                            } else {
                                SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, 1, $nro_etiquetas, $rango);
                            }
                            $sql_fis = "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $iCosecha AND estado = 0);";
                            DBConnector::ejecutar($sql_fis);
                            $sql_fis = "UPDATE solicitud SET estado = 12 WHERE id_solicitud = $isolicitud;";
                            DBConnector::ejecutar($sql_fis);
                        }
                    }
                    
                }
                return 'OK';  
            }else{
                return 'error';
            }
        }
    }

    /**
     * INGRESAR DATOS DESDE ARCHIVO EXCEL
     * */
    public static function importarExcelToBD($archivo) {
        date_default_timezone_set('America/La_Paz');
        $ext = array_pop(explode(".", $archivo));
        //obtenemos la extension del archivo
        if ($ext == "xlsx")
            //si es excel 2007 cargamos su lector
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        else
            //si no, cargamos el lector para archivos xls
            $objReader = new PHPExcel_Reader_Excel5();
        //archivo de solo lectura
        $objReader -> setReadDataOnly(true);
        //cargamos el archivo
        $objPHPExcel = $objReader -> load("$archivo");
        //indicamos que la primer hoja es la activa
        $objPHPExcel -> setActiveSheetIndex(0);
        //indicamos que empezamos con la primer hoja
        $objWorksheet = $objPHPExcel -> getActiveSheet(0);
        //Leemos cuantas filas tiene e.g. 814
        $highestRow = $objWorksheet -> getHighestRow();
        if (!$highestRow)//si tiene 0 filas
            die("El archivo de excel no contiene informacion o bien esta no es accesible");
        ///comenzamos a leer el archivo
        $instituto_name = strtolower($objWorksheet -> getCell('A1') -> getValue());
        $direccion_name = strtolower($objWorksheet -> getCell('A2') -> getValue());
        $unidad_name = strtolower($objWorksheet -> getCell('A3') -> getValue());
        $oficina_name = strtolower($objWorksheet -> getCell('A4') -> getValue());
        //comprobar si el archivo tiene los datos en las primeras 4 filas
        if ($instituto_name == "instituto nacional de innovacion agropecuaria y forestal" && $direccion_name == "direccion nacional de semillas" && $unidad_name == "unidad de certificacion y control de semillas" && $oficina_name == "oficina departamental chuquisaca") {
            $sql = '';
            //empezar a leer datos
            for ($row = 8; $row <= $highestRow; $row++) {
                if (($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue() == '') && ($objWorksheet -> getCellByColumnAndRow(1, ($row + 1)) -> getValue() == '')) {
                    break;
                } else {
                    //determinar si la fila es certificacion o fiscalizacion para agregar al array respectivo
                    $sistema = Funciones::search_tilde($objWorksheet -> getCellByColumnAndRow(1, $row) -> getValue());
                    if (($sistema == "Certificaci_n") || ($sistema == "certificaci_n")) {
                        $nombre_archivo = "excelToBDv4";
                        $file = fopen("../vista/temp/" . $nombre_archivo . ".sql", "a") or die("Problemas en la creacion del archivo");

                        //////////////////////////////////////// SOLICITUD //////////////////////////////////////////////
                        ////$sql = "SOLICITIUD \r\n";
                        $nro_solicitud = $objWorksheet -> getCellByColumnAndRow(0, $row) -> getValue();
                        $isistema = Sistema::setSistemaImport($sistema);
                        $isemillera = Semillera::setSemilleraImport($objWorksheet -> getCellByColumnAndRow(7, $row) -> getValue(), $nro_solicitud, $isistema);
                        $imunicipio = Municipio::setMunicipioImport($objWorksheet -> getCellByColumnAndRow(4, $row) -> getValue(), $objWorksheet -> getCellByColumnAndRow(3, $row) -> getValue());
                        $semillerista = Funciones::getNombreApellidoImportar($objWorksheet -> getCellByColumnAndRow(8, $row) -> getValue());
                        $isemillerista = Semillerista::setSemilleristaImport(utf8_decode($semillerista['nombre']), utf8_decode($semillerista['apellido']), $isemillera);
                        $comunidad = addslashes($objWorksheet -> getCellByColumnAndRow(5, $row) -> getValue());
                        $fecha = $objWorksheet -> getCellByColumnAndRow(9, $row) -> getCalculatedValue() + 1;
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha);
                        $fecha_php = date("Y-m-d", $timestamp);
                        if (empty($nro_solicitud) && empty($imunicipio) && empty($isemillera) && empty($isemillerista)) {
                            //si los 4 campos estan vacios saltar a siguiente fila
                            continue;
                        } else {
                            //caso contrario insertar solicitud
                            Solicitud::insertSolicitud($isemillera, $isistema, $isemillerista, $imunicipio, $comunidad, $fecha_php);
                            #$sql = "INSERT INTO solicitud (id_semillera,id_sistema,id_municipio,id_semillerista,comunidad,fecha) VALUES (";
                            #$sql .= "$isemillera,$isistema,$imunicipio,$isemillerista,'$comunidad','$fecha_php');\r\n";
                        }
                        //obtener id de solicitud
                        $isolicitud = DBConnector::lastInsertId();
                        if (DBConnector::nroError())
                            $sql .= "ERROR :" . DBConnector::mensaje();

                        //////////////////////////////////////// SEMILLA ////////////////////////////////////////////////
                        $nro_campo = Semilla::getNroCampoImportar($objWorksheet -> getCellByColumnAndRow(10, $row) -> getValue());
                        $campanha = Campanha::getIdCampanhaImportar($objWorksheet -> getCellByColumnAndRow(11, $row) -> getValue());
                        $cultivo = Cultivo::getIdCultivoImportar($objWorksheet -> getCellByColumnAndRow(12, $row) -> getValue());
                        $variedad = Variedad::getIdVariedadImportar($objWorksheet -> getCellByColumnAndRow(13, $row) -> getValue(), $cultivo);
                        $cat_sembrada = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(14, $row) -> getValue());
                        $cat_producir = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(15, $row) -> getValue());
                        $cantidad_semilla_empleada = Semilla::getCantidadSemillaEmpleadaImportar($objWorksheet -> getCellByColumnAndRow(16, $row) -> getCalculatedValue());
                        $lote = Semilla::getLoteImportar($objWorksheet -> getCellByColumnAndRow(17, $row) -> getCalculatedValue());
                        $fecha_excel_siembra = (!empty($objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue())) ? $objWorksheet -> getCellByColumnAndRow(18, $row) -> getCalculatedValue() : strtotime('1st January 2000');
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_excel_siembra);
                        $fecha_php_siembra = date("Y-m-d", $timestamp);
                        $plantas_hectarea = Semilla::getPlantasHectareasImportar($objWorksheet -> getCellByColumnAndRow(19, $row) -> getValue());
                        $cultivo_anterior = Cultivo::getIdCultivoImportar($objWorksheet -> getCellByColumnAndRow(20, $row) -> getValue());
                        $superficie_parcela = Superficie::getIdSuperficieBySuperficieImportar($objWorksheet -> getCellByColumnAndRow(21, $row) -> getCalculatedValue());
                        $iSemilla = 0;

                        if (empty($nro_campo) && empty($campanha) && empty($cultivo)) {
                            continue;
                        } else {
                            if (count($lote) > 1) {
                                //iterar sobre la cantidad de lotes
                                foreach ($lote as $key => $lote_valor) {
                                    //id de la superficie inscrita
                                    $id_inscrita = Superficie::getIdSuperficieBySuperficie($superficie_parcela);
                                    Semilla::insertSemillaImportar($nro_campo, $campanha, $cultivo, $variedad, $cat_sembrada, $cat_producir, $cantidad_semilla_empleada, $lote_valor, $fecha_php_siembra, $plantas_hectarea, $cultivo_anterior, $id_inscrita);
                                    $sql .= "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                                    $sql .= "'$nro_campo',$campanha,$cultivo,$variedad,$cat_sembrada,$cat_producir,$cantidad_semilla_empleada,'$lote_valor','$fecha_php_siembra',$plantas_hectarea,$cultivo_anterior,$superficie_parcela);\r\n";
                                    //obtenemos el id
                                    $iSemilla = DBConnector::lastInsertId();
                                    //agregamos el id en la tabla semilla
                                    #Semilla::updateSemilla($iSemilla, $id_inscrita);
                                    $sql .= "UPDATE semilla SET superficie_parcela = $id_inscrita WHERE id_semilla=$iSemilla;\r\n";
                                    //agregamos valores de los id en la tabla de relacion
                                    #Tiene::solicitud_semilla($isolicitud, $iSemilla);
                                    $sql .= "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                                    //agregamos la superficie de la parcela en la tabla superficie
                                    #Superficie::setInscrita($id_inscrita, $iSemilla);
                                    $sql .= "INSERT INTO superficie (inscrita,id_semilla) VALUES ($superficie_parcela,$iSemilla );\r\n";
                                    //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                                    #Solicitud::updateEstado($isolicitud);
                                    $sql .= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;\r\n";
                                    //cambia el estado de semilla a 1
                                    #Semilla::updateEstado($iSemilla);
                                    $sql .= "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;\r\n";
                                }
                            } else {

                                Semilla::insertSemillaImportar($nro_campo, $campanha, $cultivo, $variedad, $cat_sembrada, $cat_producir, $cantidad_semilla_empleada, $lote[0], $fecha_php_siembra, $plantas_hectarea, $cultivo_anterior, $superficie_parcela);
                                if (DBConnector::nroError())
                                    $sql .= "ERROR semilla :" . DBConnector::mensaje();
                                ///$sql = "INSERT INTO semilla (nro_campo,campanha,cultivo,variedad,categoria_sembrada,categoria_producir,cantidad_semilla_empleada,nro_lote,fecha_siembra,plantas_por_hectarea,cultivo_anterior,superficie_parcela) VALUES (";
                                ///$sql .= "'$nro_campo',$campanha,$cultivo,$variedad,$cat_sembrada,$cat_producir,$cantidad_semilla_empleada,'".$lote[0]."','$fecha_php_siembra',$plantas_hectarea,$cultivo_anterior,$superficie_parcela);\r\n";
                                //obtenemos el id
                                $iSemilla = DBConnector::lastInsertId();
                                //agregamos el id en la tabla semilla
                                Semilla::updateSemilla($iSemilla, $superficie_parcela);
                                $sql .= "UPDATE semilla SET superficie_parcela = $superficie_parcela WHERE id_semilla=$iSemilla;\r\n";
                                #echo "updateSemilla ",DBConnector::mensaje();
                                //agregamos valores de los id en la tabla de relacion
                                #Tiene::solicitud_semilla($isolicitud, $iSemilla);
                                $sql .= "INSERT INTO solicitud_semilla (id_solicitud,id_semilla) VALUES ($isolicitud,$iSemilla);\r\n";
                                #echo "tiene ",DBConnector::mensaje();
                                //agregamos la superficie de la parcela en la tabla superficie
                                #Superficie::setInscrita($id_inscrita, $iSemilla);
                                $sql .= "INSERT INTO superficie (inscrita,id_semilla) VALUES ($superficie_parcela,$iSemilla );\r\n";
                                //incrementa el valor de estado de solicitud en la tabla solicitud en 1
                                #Solicitud::updateEstado($isolicitud);
                                $sql .= "UPDATE solicitud SET estado = 1 WHERE id_solicitud = $isolicitud;\r\n";
                                ;
                                //cambia el estado de semilla a 1
                                Semilla::updateEstado($iSemilla);
                                $sql .= "UPDATE semilla SET estado = 1 WHERE id_semilla=$iSemilla;\r\n";
                            }
                            #$sql .= "datos de semilla ingresada : $nro_campo\r\n";
                        }

                        //////////////////////////////////////// SUPERFICIE /////////////////////////////////////////////
                        //obtener datos de la hoja excel
                        $super_inscrita = $objWorksheet -> getCellByColumnAndRow(21, $row) -> getCalculatedValue();
                        $idinscrita = Superficie::getImportarInscrita($objWorksheet -> getCellByColumnAndRow(21, $row) -> getCalculatedValue());
                        $rechazada = Superficie::getImportarRechazada($objWorksheet -> getCellByColumnAndRow(22, $row) -> getValue());
                        $retirada = Superficie::getImportarRetirada($objWorksheet -> getCellByColumnAndRow(23, $row) -> getValue());
                        $aprobada = Superficie::getImportarAprobada($objWorksheet -> getCellByColumnAndRow(24, $row) -> getCalculatedValue());
                        $cultivo = $objWorksheet -> getCellByColumnAndRow(12, $row) -> getCalculatedValue();

                        if (empty($inscrita)) {
                            $last_superficie = Superficie::getLastSuperficie($objWorksheet, 21, $row);
                            $idinscrita = Superficie::getImportarInscrita($last_superficie['inscrita']);
                            $rechazada = $last_superficie['rechazada'];
                            $retirada = $last_superficie['retirada'];
                            $aprobada = $last_superficie['aprobada'];
                        }

                        //ingresar datos de superficie
                        $iSuperficie = Superficie::setInscrita($idinscrita, $iSemilla);
                        #$sql .= "INSERT INTO superficie (inscrita,id_semilla) VALUES ($super_inscrita,$iSemilla );\r\n";
                        //agregar semilla retirada
                        #setRetirada($iSemilla, $retirada);
                        $sql .= "UPDATE superficie SET retirada = $retirada WHERE id_semilla=$iSemilla;\r\n";
                        #setRechazada($iSemilla, $rechazada);
                        $sql .= "UPDATE superficie SET rechazada = $rechazada WHERE id_semilla=$iSemilla;\r\n";
                        #setAprobada($iSemilla, $aprobada);
                        $sql .= "UPDATE superficie SET aprobada = $aprobada WHERE id_semilla=$iSemilla;\r\n";
                        #updateEstado2($isolicitud);
                        $sql .= "UPDATE solicitud SET estado = 4 WHERE id_solicitud = $isolicitud;\r\n";
                        #Semilla::updateEstadoSemilla($iSemilla);
                        $sql .= "UPDATE semilla SET estado = (estado + 1) WHERE id_semilla=$iSemilla;\r\n";
                        #Superficie::updateEstado($iSemilla);
                        //actualizar estado de superficie
                        if ($super_inscrita == 0.00 && $aprobada == 0.00) {
                            $sql .= "UPDATE superficie SET estado = 0 WHERE id_semilla = $iSemilla;\r\n";
                        } else {
                            $sql .= "UPDATE superficie SET estado = 3 WHERE id_semilla = $iSemilla;\r\n";
                        }

                        //////////////////////////////////////// INSPECCION /////////////////////////////////////////////
                        //obtener datos de la hoja excel
                        $sql .= "DATOS INSPECCION      \r\n";
                        $primera_inspeccion = ($objWorksheet -> getCellByColumnAndRow(25, $row) -> getCalculatedValue() > 0) ? $objWorksheet -> getCellByColumnAndRow(25, $row) -> getCalculatedValue() : -32874;
                        //01-01-1990
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($primera_inspeccion);
                        $primera_php = date("Y-m-d", $timestamp);

                        $segunda_inspeccion = ($objWorksheet -> getCellByColumnAndRow(26, $row) -> getCalculatedValue() > 0) ? $objWorksheet -> getCellByColumnAndRow(26, $row) -> getCalculatedValue() : -32874;
                        //01-01-1990
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($segunda_inspeccion);
                        $segunda_php = date("Y-m-d", $timestamp);

                        $tercera_inspeccion = ($objWorksheet -> getCellByColumnAndRow(27, $row) -> getCalculatedValue() > 0) ? $objWorksheet -> getCellByColumnAndRow(27, $row) -> getCalculatedValue() : -32874;
                        //01-01-1990
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($tercera_inspeccion);
                        $tercera_php = date("Y-m-d", $timestamp);

                        if ($primera_inspeccion < 0 && $segunda_inspeccion < 0 && $tercera_inspeccion < 0 && empty($aprobada)) {
                            $last_inspeccion = Inspeccion::getLastInspeccionImportar($objWorksheet, 25, $row);
                            $primera_php = $last_inspeccion['primera'];
                            $segunda_php = $last_inspeccion['segunda'];
                            $tercera_php = $last_inspeccion['tercera'];

                        }

                        Inspeccion::setInspeccionImportar($primera_php, $segunda_php, $tercera_php);
                        $idInspeccion = DBConnector::lastInsertId();
                        $sql .= "INSERT INTO inspeccion (primera,segunda,tercera) VALUES ('$primera_php','$segunda_php','$tercera_php');\r\n";
                        $numero_inspeccion = 0;
                        if ($primera_inspeccion > 0 && $segunda_inspeccion < 0) {
                            #Solicitud::updateStadoByNumber(5, $isolicitud);
                            $sql .= "UPDATE solicitud SET estado = 5 WHERE id_solicitud = $isolicitud;\r\n";
                            $numero_inspeccion = 1;
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                            #echo "primer semilla ",DBConnector::mensaje();
                        } elseif ($primera_inspeccion > 0 && $segunda_inspeccion > 0) {
                            #echo "1ra : $primera_inspeccion - 2da : $segunda_inspeccion - 3ra : $tercera_inspeccion\r\n";
                            $sql .= "UPDATE solicitud SET estado = 6 WHERE id_solicitud = $isolicitud;\r\n";
                            $numero_inspeccion = 2;
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                        } elseif ($segunda_inspeccion > 0 && $tercera_inspeccion > 0) {
                            #Solicitud::updateStadoByNumber(7, $isolicitud);
                            $sql .= "UPDATE solicitud SET estado = 7 WHERE id_solicitud = $isolicitud;\r\n";
                            $numero_inspeccion = 3;
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";
                        } else
                            #Semilla::updateEstadoSemillaImportar($iSemilla);
                            $sql .= "UPDATE semilla SET estado = estado + 1 WHERE id_semilla=$iSemilla;\r\n";

                        //actualizar estado de solicitud
                        #Solicitud::updateEstado($isolicitud);
                        $sql .= "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                        //actualizar estado de inspeccion
                        #Inspeccion::updEstadoImportar($numero_inspeccion, $idInspeccion);
                        $sql .= "UPDATE inspeccion SET estado = $numero_inspeccion WHERE id_inspeccion=$idInspeccion;\r\n";

                        //////////////////////////////////////// HOJA COSECHA ///////////////////////////////////////////
                        //obtener datos de la hoja excel
                        $sql .= "DATOS HOJA COSECHA\r\n";
                        $laboratorio = HojaCosecha::getLaboratorioImportar($objWorksheet -> getCellByColumnAndRow(12, $row) -> getValue());
                        $nro_campo = HojaCosecha::getNroCampoImportar($objWorksheet, 28, $row);
                        $fecha_emision = !empty($objWorksheet -> getCellByColumnAndRow(29, $row) -> getCalculatedValue()) ? $objWorksheet -> getCellByColumnAndRow(29, $row) -> getCalculatedValue() : -32874;
                        //01-01-1990
                        $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_emision);
                        $fecha_emision_php = date("Y-m-d", $timestamp);

                        $categoria_campo = HojaCosecha::getCategoriaCampoImportar($objWorksheet -> getCellByColumnAndRow(30, $row) -> getCalculatedValue());
                        $rendimiento_estimado = HojaCosecha::getRendimientoEstimadoImportar($objWorksheet -> getCellByColumnAndRow(31, $row) -> getCalculatedValue());
                        $superficie = HojaCosecha::getSuperficieParcelaImportar($objWorksheet -> getCellByColumnAndRow(32, $row) -> getCalculatedValue());
                        $rendimiento_campo = HojaCosecha::getRendimientoCampoImportar($objWorksheet -> getCellByColumnAndRow(33, $row) -> getCalculatedValue());
                        $nro_cupones = HojaCosecha::getNroCuponesImportar($objWorksheet -> getCellByColumnAndRow(34, $row) -> getCalculatedValue());
                        $planta_acondicionadora = HojaCosecha::getPlantaAcondicionadoraImportar($objWorksheet -> getCellByColumnAndRow(35, $row) -> getCalculatedValue());
                        if (empty($nro_campo) && empty($fecha_emision) && empty($categoria_campo)) {
                            $last_cosecha = HojaCosecha::getLastHojaCosecha($objPHPExcel, 28, $row);
                            $nro_campo = $last_cosecha['nrocampo'];
                            $temp_fecha_emision = explode("/", $last_cosecha['fecha_emision']);
                            $fecha_emision = implode("-", array($temp_fecha_emision[2], $temp_fecha_emision[1], $temp_fecha_emision[0]));
                            $categoria_campo = $last_cosecha['categoria_campo'];
                            $rendimiento_estimado = $last_cosecha['rendimiento_estimado'];
                            $superficie = $last_cosecha['superficie'];
                            $rendimiento_campo = $last_cosecha['rendimiento_campo'];
                            $nro_cupones = $last_cosecha['nrocupones'];
                            $planta_acondicionadora = $last_cosecha['planta_acondicionadora'];

                        }
                        $estado = 6;
                        // ??
                        HojaCosecha::setHojaCosechaImport($iSemilla, $fecha_emision_php, $categoria_campo, $rendimiento_estimado, $superficie, $rendimiento_campo, $nro_cupones, $planta_acondicionadora, $laboratorio, $estado, $iSuperficie);
                        $sql .= "INSERT INTO cosecha (nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado, superficie_parcela,rendimiento_campo,";
                        $sql .= "nro_cupones,planta_acondicionadora,alaboratorio,estado,id_superficie) VALUES('";
                        $sql .= "$nro_campo','$fecha_emision_php',$categoria_campo,$rendimiento_estimado,$superficie, $rendimiento_campo, $nro_cupones, '$planta_acondicionadora',";
                        $sql .= "$laboratorio,$estado,$iSuperficie);\r\n";
                        $iCosecha = DBConnector::lastInsertId();
                        $cultivo_name = $objWorksheet -> getCellByColumnAndRow(12, $row) -> getValue();
                        $estadoSolicitud = Solicitud::getEstadoSolicitud($isolicitud);
                        if ($cultivo_name != 'papa')
                            #Solicitud::updateEstado4($isolicitud);
                            $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                        else {
                            if ($estadoSolicitud == 6) {
                                #Solicitud::updateEstadoCosecha($isolicitud);
                                $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                            } else {
                                #Solicitud::updateEstado4($isolicitud);
                                $sql .= "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $isolicitud;\r\n";
                            }
                        }
                        Inspeccion::updInspeccion($iSuperficie);
                        $sql .= "UPDATE superficie SET estado=estado+1 WHERE id_superficie=$iSuperficie;\r\n";

                        //////////////////////////////////////// SEMILLA PRODUCIDA //////////////////////////////////////
                        //obtener datos de la hoja excel
                        $sql .= "DATOS SEMILLA PRODUCIDA \r\n";
                        $igeneracion = Categoria::getIdCategoriaByNameAndGeneracionImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue());
                        $semilla_neta = SemillaProd::getSemillaProducidaImport($objWorksheet -> getCellByColumnAndRow(36, $row) -> getCalculatedValue());
                        $igeneracion = SemillaProd::getGeneracionCategoriaObtenidaImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue());
                        $categoria_obtenida = SemillaProd::getCategoriaObtenidaImportar($objWorksheet -> getCellByColumnAndRow(37, $row) -> getCalculatedValue(), $objWorksheet -> getCellByColumnAndRow(30, $row) -> getCalculatedValue());
                        $nro_etiqueta = SemillaProd::getNroEtiquetasImportar($objWorksheet -> getCellByColumnAndRow(38, $row) -> getCalculatedValue());
                        $rango = SemillaProd::getRangoImportar($objWorksheet -> getCellByColumnAndRow(39, $row) -> getCalculatedValue());
                        if (empty($semilla_neta) && empty($nro_etiqueta) && empty($rango)) {
                            $semilla_neta = $nro_etiqueta = $rango = 0;
                            SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $igeneracion, $categoria_obtenida, $nro_etiqueta, $rango);
                            $sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,id_generacion,semilla_neta,categoria_obtenida,nro_etiqueta,rango) semilla_producida VALUES ";
                            $sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                        } else {
                            #echo $isolicitud, '=',$iSemilla, '=',$categoria_obtenida, '=',$semilla_neta, '=',$nro_etiqueta, '=',$rango;
                            SemillaProd::setSemillaProducidaImportar($isolicitud, $iSemilla, $semilla_neta, $igeneracion, $categoria_obtenida, $nro_etiqueta, $rango);
                            $sql .= "INSERT INTO semilla_producida (id_superficie,nro_campo,semilla_neta,categoria_obtenida,nro_etiqueta,rango) VALUES ";
                            $sql .= "($iSuperficie,$iSemilla,$semilla_neta,$igeneracion,$categoria_obtenida,$nro_etiqueta,'$rango');\r\n";
                        }
                        #Solicitud::updateEstadoBySemillaProd($isolicitud);
                        $query = "UPDATE solicitud SET estado = 10 WHERE id_solicitud = $isolicitud;\r\n";
                        if ($cultivo_name != 'papa')
                            #HojaCosecha::updateStado($iCosecha);
                            $sql .= "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $iCosecha AND estado = 0);\r\n";
                        #Solicitud::updateEstado($isolicitud);
                        $query = "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                        /*
                         //////////////////////////////////////// MUESTRA LABORATORIO ////////////////////////////////////
                         //obtener datos de la hoja excel
                         $nro_analisis = $objWorksheet -> getCellByColumnAndRow(40, $row) -> getCalculatedValue();
                         $fecha_recepcion = empty($objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue()) ? '' : $objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue();
                         $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_recepcion);
                         $fecha_recepcion_php = date("Y-m-d", $timestamp);
                         $lote = $objWorksheet -> getCellByColumnAndRow(43, $row) -> getCalculatedValue();
                         $nro_campo = $objWorksheet -> getCellByColumnAndRow(10, $row) -> getCalculatedValue();
                         $origen = $objWorksheet -> getCellByColumnAndRow(5, $row) -> getCalculatedValue();

                         $pureza = $objWorksheet -> getCellByColumnAndRow(44, $row) -> getCalculatedValue();
                         $germinacion = $objWorksheet -> getCellByColumnAndRow(45, $row) -> getCalculatedValue();
                         $humedad = $objWorksheet -> getCellByColumnAndRow(46, $row) -> getCalculatedValue();
                         $ianalisis = Muestra_laboratorio::getAnalisisLaboratorioImportar($pureza, $germinacion, $humedad);
                         $isistema = Sistema::setSistemaImport($sistema);
                         //determinar si son varios numeros de campos
                         $analisis_array = explode("-", $nro_analisis);
                         //dividir nros de analisis si son varios
                         $lote_array = explode("-", $lote);
                         $pureza_array = explode("/", $pureza);
                         $germinacion_array = explode("/", $germinacion);
                         $humedad_array = explode("/", $humedad);
                         if (count($analisis_array) > 1) {
                         foreach ($analisis_array as $id => $nro) {
                         $imuestra = Muestra_laboratorio::setMuestraLaboratorioImportar($fecha_recepcion_php, $nro_campo, $origen, $ianalisis, $isistema);
                         #$sql .= "INSERT INTO muestra_laboratorio (fecha_recepcion,nro_campo,origen,analisis,sistema) VALUES ('".$fecha_recepcion_php."',".$nro_campo.",'".$origen."',".$ianalisis.",".$sistema.",".$tipo.");\r\n";
                         $sql .= "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                         }
                         } else {
                         foreach ($analisis_array as $id => $nro) {
                         $imuestra = Muestra_laboratorio::setMuestraLaboratorioImportar($fecha_recepcion_php, $nro_campo, $origen, $ianalisis, $isistema);
                         #$sql .= "INSERT INTO muestra_laboratorio (fecha_recepcion,nro_campo,origen,analisis,sistema) VALUES ('".$fecha_recepcion_php."',".$nro_campo.",'".$origen."',".$ianalisis.",".$sistema.",".$tipo.");\r\n";
                         $sql .= "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $isolicitud;\r\n";
                         }
                         }
                         $imuestra = DBConnector::lastInsertId();

                         //////////////////////////////////////// RESULTADO LABORATORIO //////////////////////////////////
                         //obtener datos de la hoja excel
                         $fecha_resultado = empty($objWorksheet -> getCellByColumnAndRow(42, $row) -> getCalculatedValue()) ? '' : $objWorksheet -> getCellByColumnAndRow(41, $row) -> getCalculatedValue();
                         $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecha_resultado);
                         $fecha_resultado_php = date("Y-m-d", $timestamp);

                         $lote = Resultado_muestras::getLoteImportar($objWorksheet -> getCellByColumnAndRow(43, $row) -> getCalculatedValue());
                         $pureza = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(44, $row) -> getCalculatedValue());
                         $germinacion = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(45, $row) -> getCalculatedValue());
                         $humedad = Resultado_muestras::getPurezaImportar($objWorksheet -> getCellByColumnAndRow(46, $row) -> getCalculatedValue());
                         $observacion = Resultado_muestras::getObservacionImportar(trim($objWorksheet -> getCellByColumnAndRow(49, $row) -> getCalculatedValue()));
                         //array
                         $lote_array = explode("-", $lote);
                         $pureza_array = explode("/", $pureza);
                         $germinacion_array = explode("/", $germinacion);
                         $humedad_array = explode("/", $humedad);
                         //obtener el ultimo valor  donde esta la gestion
                         $gestion = array_pop($analisis_array);
                         //dividir el ultimo valor para obtener la gestion
                         $gestion_array = explode("/", $gestion);
                         //obtener las iniciales del cultivo
                         $cultivo = (count($lote_array[1])>=2)?$lote_array[count($lote_array[1]) - 2]:$lote_array[1];
                         //iniciales de semillerista
                         $inicial = $lote_array[0];

                         foreach ($analisis_array as $i => $valor) {
                         //crear nro de lote segun parametros
                         $piezas_lote = array($inicial, $analisis_array[$i], $cultivo, $gestion_array[1]);
                         $new_lote = implode("-", $piezas_lote);
                         Resultado_muestras::setResultadoMuestraImportar($fecha_resultado_php, $new_lote, $pureza[$i], $germinacion[$i], $humedad[$i], $observacion, $imuestra);
                         $sql .= "UPDATE muestra_laboratorio SET estado = 1 WHERE id_muestra = $imuestra;\r\n";

                         }
                         */
                        fputs($file, $sql);
                        fclose($file);
                    }
                }
            }
            return $nombre_archivo;
        } else {
            return 'error';
        }
    }

    /*
     * texto centrado
     * */
    public function setCenterText(PHPExcel $objPHPExcel, $cInicial, $cFinal, $rango = FALSE) {
        $center = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicial . ':' . $cFinal) -> applyFromArray($center);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicial) -> applyFromArray($center);
        }

    }

    /**
     * texto a la derecha
     * */
    public function setRightText(PHPExcel $objPHPExcel, $cInicial, $cFinal, $rango = FALSE) {
        $center = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicial . ':' . $cFinal) -> applyFromArray($center);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicial) -> applyFromArray($center);
        }

    }

    /**
     * contenido de celda en negrita
     * utilizada solo en detalle de certificacion
     * @param PHPExcel   $objPHPExcel   instancia de objeto excel
     * @param mixed      $cInicio       celda inicial
     * @param mixed      $cFinal        celda final
     * @param boolean    $rango         TRUE: rango de celda | FALSE:solo una celda
     * */
    public function setBoldFont(PHPExcel $objPHPExcel, $cInicio, $cFinal, $rango = TRUE) {
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getFont() -> setBold(TRUE);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . $cFinal) -> getFont() -> setBold(TRUE);
        }

    }

    /**
     * contenido de celda en texto normal
     * utilizada solo en detalle de certificacion
     * @param PHPExcel   $objPHPExcel   instancia de objeto excel
     * @param mixed      $cInicio       celda inicial
     * @param mixed      $cFinal        celda final
     * @param boolean    $rango         TRUE: rango de celda | FALSE:solo una celda
     * */
    public function setNormalFont(PHPExcel $objPHPExcel, $cInicio, $cFinal, $rango = TRUE) {
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getFont() -> setBold(FALSE);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . $cFinal) -> getFont() -> setBold(FALSE);
        }

    }

    /**
     * Formato de borde externo a todas las celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * @param    boolean     $rango       TRUE: rango de celda   FALSE:solo una celda
     * */
    public function setOutlineBorders(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D', $rango = TRUE) {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getOutline() -> setBorderStyle($ancho_borde);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . $cFinal) -> getBorders() -> getOutline() -> setBorderStyle($ancho_borde);
        }
    }

    /**
     * Formato de borde interno y externo de todas las celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * */
    public function setAllBorders(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D') {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getAllBorder() -> setBorderStyle($ancho_borde);
    }

    /**
     * Formato de borde de lado izquierdo a todas las celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * */
    public function setLeftBorder(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D', $rango = TRUE) {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        if ($rango) {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getLeft() -> setBorderStyle($ancho_borde);
        } else {
            $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . $cFinal) -> getBorders() -> getLeft() -> setBorderStyle($ancho_borde);
        }

    }

    /**
     * Formato de borde de lado derecho a un rango de celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * */
    public function setRightBorder(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D') {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getRight() -> setBorderStyle($ancho_borde);
    }

    /**
     * Formato de borde inferior a un rango de celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * */
    public function setBottomBorder(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D') {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getBottom() -> setBorderStyle($ancho_borde);
    }

    /**
     * Formato de borde superior a un rango de celdas
     * @param    PHPExcel    $phpExcel    instancia de objeto excel
     * @param    string      $cInicio     celda Inicial
     * @param    string      $cFinal      celda Final
     * @param    string      $borde       tipo de borde   D :  delgado | G : medio
     * */
    public function setTopBorder(PHPExcel $objPHPExcel, $cInicio, $cFinal, $borde = 'D') {
        if ($borde == 'D') {
            $ancho_borde = PHPExcel_Style_Border::BORDER_THIN;
        } else {
            $ancho_borde = PHPExcel_Style_Border::BORDER_MEDIUM;
        }
        $objPHPExcel -> getActiveSheet() -> getStyle($cInicio . ':' . $cFinal) -> getBorders() -> getTop() -> setBorderStyle($ancho_borde);
    }

    /**
     * establecer area de impresion
     * */
    public function setPrinterArea(PHPExcel $objPHPExcel, $cInicio, $cFinal) {
        $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setPrintArea($cInicio . ':' . $cFinal);
    }

    /**
     * agregar imagen
     * @param PHPExcel   $objPHPExcel
     * @param string     $imagen        nombre de imagen
     * @param string     $celda         ubicacion de celda
     * @param integer    $ancho         ancho de imagen
     * @param integer    $alto          alto de imagen
     * */
    public function setImagen(PHPExcel $objPHPExcel, $imagen, $celda, $ancho, $alto) {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing -> setName('Logo');
        $objDrawing -> setDescription('Logo');
        $objDrawing -> setPath($_SERVER['DOCUMENT_ROOT'] . '/' . HOME_FOLDER . IMG . '/' . $imagen);
        $objDrawing -> setCoordinates($celda);
        $objDrawing -> setWidthAndHeight($ancho, $alto);
        $objDrawing -> setWorksheet($objPHPExcel -> getActiveSheet());
    }

    /**
     * establece las propiedades del archivo
     * @param   PHPExcel  $objPHPExcel   instancia que contiene el archivo Excel
     * @param   string    $tipo          nombre del informe
     * */
    public function setPagePropierties(PHPExcel $objPHPExcel, $tipo) {
        switch ($tipo) {
            case 'detalle' :
                $objPHPExcel -> getProperties() -> setCreator('INIAF - CHUQUISACA');
                $objPHPExcel -> getProperties() -> setLastModifiedBy('INIAF - CHUQUISACA');
                $objPHPExcel -> getProperties() -> setTitle('Detalle de certificacion');
                $objPHPExcel -> getProperties() -> setSubject('Detalle de certificacion');
                $objPHPExcel -> getProperties() -> setDescription('detalle certificacion');
                //orientacion y tamaño de pagina
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER);
                //opciones de escala
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setFitToPage(1);
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setFitToWidth(1);
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setFitToHeight(1);
                //centrado horizontal y vertical de la pagina
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setHorizontalCentered(TRUE);
                $objPHPExcel -> getActiveSheet() -> getPageSetup() -> setVerticalCentered(TRUE);
                break;
        }
    }

    /**
     * document security
     * @param  PHPExcel  $objPHPExcel   instancia de hoja excel
     * @param  string    $password      password de para modificar documento
     * */
    public function setDocumentSecurity(PHPExcel $objPHPExcel, $password) {
        $objPHPExcel -> getSecurity() -> setLockWindows(TRUE);
        $objPHPExcel -> getSecurity() -> setLockStructure(TRUE);
        #$objPHPExcel -> getSecurity() -> setWorkbookPassword($password);
    }

    /**
     * worksheet security
     * @param  PHPExcel  $objPHPExcel   instancia de hoja excel
     * @param  string    $password      password de para modificar hoja
     * */
    public function setWorkSheetSecurity(PHPExcel $objPHPExcel, $password) {
        $objPHPExcel -> getActiveSheet() -> getProtection() -> setPassword($password);
        $objPHPExcel -> getActiveSheet() -> getProtection() -> setSheet(TRUE);
        $objPHPExcel -> getActiveSheet() -> getProtection() -> setSort(TRUE);
        $objPHPExcel -> getActiveSheet() -> getProtection() -> setInsertRows(TRUE);
        $objPHPExcel -> getActiveSheet() -> getProtection() -> setFormatCells(TRUE);
    }

    /**
     * establecer cabecera segun informe RESUMEN
     * */
    public function setCabecera(PHPExcel $objPHPExcel, $tipo) {
        switch ($tipo) {
            case 'detalle' :
                //BORDE PARA CABECERA
                $this -> setOutlineBorders($objPHPExcel, 'A11', 'R12', 'G', TRUE);
                $column_titulo_combinada = array('A', 'B', 'C', 'D', 'F', 'G');
                //columnas combinadas
                $fila_titulo = array('11', '12');
                foreach ($column_titulo_combinada as $columna) {
                    foreach ($fila_titulo as $fila) {
                        $this -> setCenterText($objPHPExcel, $columna . '11', 'XX', FALSE);
                        $this -> setOutlineBorders($objPHPExcel, $columna, $fila, 'G', FALSE);
                    }
                }
                $column_titulo_single = array('E', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q');
                //columnas simples
                foreach ($column_titulo_single as $columna2) {
                    foreach ($fila_titulo as $fila) {
                        $this -> setCenterText($objPHPExcel, $columna2 . '11', 'XX', FALSE);
                        $this -> setCenterText($objPHPExcel, $columna2 . '12', 'XX', FALSE);
                        $this -> setLeftBorder($objPHPExcel, $columna2, $fila, 'G', FALSE);
                    }
                }
                $this -> setOutlineBorders($objPHPExcel, 'O10', 'R10', 'G', TRUE);
                $this -> setCenterText($objPHPExcel, 'O10', 'XX', FALSE);
                //TEXTO DE TITULO EN NEGRITAS
                $objPHPExcel -> getActiveSheet() -> getStyle("A11:R12") -> getFont() -> setBold(true);
                $objPHPExcel -> getActiveSheet() -> getStyle("O10") -> getFont() -> setBold(true);
                //ANCHO Y CONTENIDO DE CELDAS DE TITULOS
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('A') -> setWidth(4);
                $objPHPExcel -> getActiveSheet() -> mergeCells('A11:A12');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('A11', 'Nro');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('B') -> setWidth(12);
                $objPHPExcel -> getActiveSheet() -> mergeCells('B11:B12');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('B11', 'FECHA');
                $objPHPExcel -> getActiveSheet() -> mergeCells('C11:C12');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('C') -> setWidth(18);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('C11', 'SEMILLERA');
                $objPHPExcel -> getActiveSheet() -> mergeCells('D11:D12');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('D') -> setWidth(14);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('D11', 'COMUNIDAD');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('E') -> setWidth(17);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('E11', 'RESPONSABLE /');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('E12', 'SEMILLERISTA');
                $objPHPExcel -> getActiveSheet() -> mergeCells('F11:F12');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('F11', 'CULTIVO');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('G') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> mergeCells('G11:G12');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('G11', 'VARIEDAD');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('H') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('H11', 'CATEGORIA');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('H12', 'SEMBRADA');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('I') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('I11', 'SUPERFICIE');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('I12', 'Has.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('J') -> setWidth(8);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('J11', 'NRO.');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('J12', 'CAMPO');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('K') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('K11', 'APROBADO');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('K12', 'Has.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('L') -> setWidth(12);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('L11', 'RECHAZADO');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('L12', 'Has.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('M') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('M11', 'CATEGORIA');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('M12', 'APROBADO');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('N') -> setWidth(8);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('N11', 'NUMERO');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('N12', 'BOLSAS');
                $objPHPExcel -> getActiveSheet() -> mergeCells('O10:R10');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('O10', 'COSTO POR CERTIFICACION');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('O') -> setWidth(12);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('O11', 'INSCRIPCION');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('O12', 'Bs.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('P') -> setWidth(8);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('P11', 'CAMPO');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('P12', 'Bs.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('Q') -> setWidth(11);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('Q11', 'ETIQUETAS');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('Q12', 'Bs.');
                $objPHPExcel -> getActiveSheet() -> getColumnDimension('R') -> setWidth(7);
                $objPHPExcel -> getActiveSheet() -> SetCellValue('R11', 'TOTAL');
                $objPHPExcel -> getActiveSheet() -> SetCellValue('R12', 'Bs.');
                $objPHPExcel -> getActiveSheet() -> getStyle('A11:R12') -> getAlignment() -> setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel -> getActiveSheet() -> getStyle('A11:R12') -> getAlignment() -> setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                break;
        }
    }

}
?>