<?php
/**
 * Clase  para la superficie inscrita
 */
class Inscrita {
    /**
     * Agrega la superficie inscrita  en la tabla
     * @param double $superficie  numero decimal de la superficie con el formato 000.00
     * */
    public static function setInscrita($superficie) {
        $query = "INSERT INTO inscrita (superficie) VALUES ($superficie)";

        DBConnector::ejecutar($query);
    }

    /**lista todas las superficies inscritas*/
    private function getInscrita() {
        $query = "SELECT * FROM inscrita";

        DBConnector::ejecutar($query);
    }
    
    

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getInscritaSQL() {
        $query = 'INSERT INTO inscrita (id_inscrita,superficie) VALUES ';
        $this -> getInscrita();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_inscrita . "," . $row -> superficie . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>