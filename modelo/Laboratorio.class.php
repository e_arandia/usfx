<?php
/**
 *
 * @author Edwin Willy Arandia Zeballos
 */
class Laboratorio {
    /**
     * undocumented function
     * @param  varchar  nroAnalisis
     * @param  date 	recepcion
     * @param  date 	resultado
     * @param  varchar 	nroCampo
     * @param  varchar 	nroLote
     * @param  varchar 	tipoSemilla
     * @param  int 		nroBolsa
     * @param  int 		kgBolsa
     * @param  int 		total
     * @param  varchar 	calibre
     * @param  int 		numeroSemillaKilo
     * @param  decimal 	humedad
     * @param  decimal 	germinacion
     * @param  decimal 	pureza
     * @param  text 	observacion
     * @param  int    	identificador de cosecha
     */
    public function setResultadoMuestra($f_recepcion, $nroLoteSemilla, $pureza, $germinacion, $humedad, $observacion, $tipoSemilla, $nroBolsa, $kgBolsa, $total, $calibre, $imuestra) {
        $query = "INSERT INTO resultado_muestras (fecha,lote,pureza,germinacion,humedad,observacion,tipoSemilla,nroBolsa,kgBolsa,total,calibre,id_muestra) ";
        $query .= "VALUES ('$f_recepcion','$nroLoteSemilla',$pureza,$germinacion,$humedad,'$observacion',$tipoSemilla,$nroBolsa,$kgBolsa,$total,'$calibre',$imuestra)";

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ",__METHOD__."=",DBConnector::mensaje(),'=',$query;
    }

    public function setResultadoMuestra_($resultado) {
        $query = "INSERT INTO resultado_muestras (fecha,lote,pureza,humedad,germinacion,observacion) ";
        $query .= "VALUES (" . $resultado[fecha] . ",'" . $resultado['lote'] . "'," . $resultado['pureza'] . "," . $resultado['germinacion'] . "," . $resultado['humedad'] . ",'" . $resultado['observacion'] . "')";

        DBConnector::ejecutar($query);
    }

    /**
     * muestra de semilla importada para fiscalizacion
     * @param array $muestra   contiene todos los datos de una muestra importada
     * */
    public function setMuestraImportada(array $muestra) {
        $query = "INSERT INTO muestra_importada (especie,variedad,categoria,lugar_origen,cantidad,certificado,aduana_ingreso,destino_semilla,distribucion,analisis) VALUES (";
        $query .= "'" . $muestra['especie'] . "','" . $muestra['variedad'] . "','" . $muestra['categoria'] . "','" . $muestra['lugar_origen'] . "','" . $muestra['cantidad'] . "','";
        $query .= $muestra['certificado'] . "','" . $muestra['aduana_ingreso'] . "','" . $muestra['destino_semilla'] . "','" . $muestra['distribucion'] . "'," . $muestra['analisis'];

        DBConnector::ejecutar($query);
    }

    /**
     * datos de muestra  para analisis
     * @param array $muestra contiene todos los datos de una muestra
     * */
    public function setMuestra(array $muestra) {
        $query = "INSERT INTO muestra_laboratorio (nro_analisis,fecha_recepcion,nro_campo,origen,sistema,analisis) VALUES ";
        $query .= "(".$muestra['nro_analisis'].",'" . $muestra['fecha_recepcion'] . "','" . $muestra['nrocampo'] . "','" . $muestra['origen'] . "'," . $muestra['sistema'] . "," . $muestra['analisis'] . ")";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: $query :: ".__METHOD__.":: ",DBConnector::mensaje();
        }
    }

    public function updateLaboratorio($nroAnalisis, $recepcion, $resultado, $nroCampo, $nroLote, $tipoSemilla, $nroBolsa, $kgBolsa, $total, $calibre, $nroSemillaKilo, $humedad, $germinacion, $pureza, $observacion, $idLaboratorio, $link) {
        $query = "UPDATE laboratorio SET nro_analisis=$nroAnalisis,fecha_recepcion=$recepcion,
		fecha_resultado=$resultado,nro_campo=$nroCampo,nro_lote=$nroLote,
	    tipo_semilla=$tipoSemilla,nro_bolsa=$nroBolsa,kg_bolsa=$kgBolsa,
	    total=$total,calibre=$calibre,nro_semilla_kilo=$nroSemillaKilo,humedad=$humedad,
	    germinacion=$germinacion,pureza=$pureza,observacion=$observacion 
	    WHERE id_laboratorio = $idLaboratorio";

        return mysql_query($query, $link);
    }

    public function deleteLaboratorio($id, $link) {
        $query = "DELETE FROM laboratorio WHERE id_laboratorio=$id";
        //echo $query; die('iii');
        return mysql_query($query, $link);
    }

    /**
     * nros de campos que pasaron a laboratorio y pasaran a form de semilla producida
     * @param integer $nro   numero de campo
     * */
    public function nroCampos($nro = '') {
        $query = "SELECT DISTINCT(nro_campo) FROM view_semilla WHERE (cultivo <> 'papa' AND estado_solicitud=8)";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * nros de campos que pasaron a laboratorio y pasaran a form de semilla producida
     * @param integer $nro   numero de campo
     * */
    public function nroCampos2($nombre, $apellido) {
        $query = "SELECT DISTINCT(nro_campo) FROM view_semilla WHERE (estado_solicitud=11 OR estado_solicitud=10) AND ";
        $query .= "nombre LIKE '$nombre%' AND apellido LIKE '$apellido%' AND gestion='".date('Y')."'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * nros de campos que tienen resultado de analisis
     * */
    public function nroCampoResultado() {

        $query = "SELECT DISTINCT(nro_campo) FROM laboratorio WHERE estado = 1";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * fecha de laboratorio para importar desde CSV
     * */
    public static function getFechaImportar($fecha) {
        if (!empty($fecha)) {
            $temp = explode("y", $fecha);
            return Funciones::getFechaImportarv2($temp[0]);
        } else {
            return '';
        }
    }
    /**
     * fecha de laboratorio para importar desde CSV en formato dd[-dd]/mm[-mm]/yy
     * */
    public static function getFechaImportarv2($fecha) {
        $temp_fecha = explode("/",$fecha);
        //buscar - en dia
        if (count($temp_fecha[0])>1){
            $year = $temp_fecha[2];
            $pos = strpos($temp_fecha[0],"-");
            if($pos){
                $temp_dia = explode("-",$temp_fecha[0]);
                $temp_mes = explode("-",$temp_fecha[1]);
                if (count($temp_dia)>1 AND count($temp_dia) == count($temp_mes)){ //si cantidad de dias y meses son iguales
                    $new_fecha = array();
                    foreach ($temp_dia as $id => $dia) {
                        array_push($new_fecha,implode("",array($year,$temp_mes[$id],$dia)));
                    }    
                }else{//verificar que si son varios dias y un solo mes
                    if(count($temp_dia)>1 AND count($temp_mes)==1){
                        foreach ($temp_dia as $id => $dia) {
                            array_push($new_fecha,implode("",array($year,$temp_mes[1],$dia)));
                        }    
                    }
                }                
            }else{ // si no encuentra - en la parte de dia
                $new_fecha = Funciones::cambiar_tipo_mysql_import($fecha);    
            }
        }else{ // si el dia es solo 1
            $new_fecha = Funciones::cambiar_tipo_mysql_import($fecha);
        }        
        return $new_fecha;
    }

    /**
     * muestras para laboratorio
     * */
    public function getFullMuestrasLaboratorio($campo) {
        $query = "SELECT * FROM view_muestras_laboratorio WHERE nro_campo='$campo'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * id de laboratorio de un nro de campo
     */
    public function getIdLaboratorio($nro_campo) {
        $query = "SELECT nro_analisis FROM view_muestras_laboratorio "; 
        $query .= "WHERE nro_campo='$nro_campo'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * id de laboratorio de un nro de campo
     */
    public function getIdLaboratorioByIdCosecha($id) {
        $query = "SELECT id_laboratorio, estado 
                  FROM laboratorio 
                  WHERE id_cosecha=$id";
        DBConnector::ejecutar($query);
    }

    /**
     * Los datos de laboratorio si nroLoteSemilla es 0
     */
    public function getLaboratorio($NroLoteSemilla = '') {
        if (empty($NroLoteSemilla))
            $query = "SELECT * FROM muestra_laboratorio";
        else
            $query = "SELECT * FROM muestra_laboratorio WHERE nro_lote ='$NroLoteSemilla'";
        DBConnector::ejecutar($query);
        
    }

    /**
     * resultado de muestras realizadas
     * */
    public function getResultadoMuestras($campo = '') {
        $query = "SELECT * FROM laboratorio WHERE estado = 1";
        if (!empty($campo)) {
            $query .= " AND nro_campo LIKE '$campo%'";
        }
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * resultado de muestras certificadas
     * */
    public function getMuestraCertificada() {
        $query = "SELECT * FROM view_resultado_muestra_certificada";

        DBConnector::ejecutar($query);
    }

    /**
     * resultado de muestras importadas
     * */
    public function getMuestraImportada() {
        $query = "SELECT * FROM view_resultado_muestra_importada";

        DBConnector::ejecutar($query);
    }

    /***
     * lista los campos disponibles para registrar la muestra de laboratorio
     * 1  certificacion
     * 2  fiscalizacion
     * 3  laboratorio
     *
     * */
    public function getListCampos($sistema,$nombre='certifica') {
        if ($sistema == 'todo') {
            $query = "SELECT DISTINCT(nro_campo) FROM view_muestras_nros_campos";
        } elseif ($sistema == 'laboratorio') {
            $query = "SELECT DISTINCT(nro_campo) FROM view_muestras_nros_campos";
        } elseif ($sistema == 2 || $sistema == 'fiscalizacion') {//fiscalizacion
            $query = "SELECT DISTINCT(nro_campo) FROM view_hoja_cosecha_fiscal WHERE gestion = '".date('Y')."'";
        } else {//certificacion
            $query = "SELECT DISTINCT(nro_campo) FROM view_muestras_certificacion WHERE sistema LIKE '$nombre%' AND cultivo <> 'papa'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR view_muestras_nros_campos ',DBConnector::mensaje();
    }
    /***
     * lista los campos disponibles para registrar la muestra de laboratorio
     * 1  certificacion
     * 2  fiscalizacion
     * 3  laboratorio
     * estado = 1  registrar muestra
     * */
    public function getListCamposCertifica() {
        $query = "SELECT DISTINCT(nro_campo) FROM view_muestras_laboratorio ";
        $query .= "WHERE sistema=1 AND gestion LIKE '".date('Y')."%' AND estado=1";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR ::'.__METHOD__."::",DBConnector::mensaje();
    }
    /***
     * lista los campos de acondicionamiento
     * 0 registrar para muestra
     * 1 resultado registrado
     */
    public function getCamposAcondicionamiento (){
        $query = "SELECT DISTINCT(nro_campo) FROM view_acondicionamiento ";
        $query .= "WHERE estado=0 ";
        $query .= "ORDER BY nro_campo DESC";
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR ::'.__METHOD__."::",DBConnector::mensaje();
        
    }
    /***
     * selecciona campo para registrar muestra de laboratorio segun el numero de campo
     */
    public function getCamposAcondicionamientoForMuestra ($nro){
        $query = "SELECT cultivo,variedad,comunidad FROM view_acondicionamiento ";
        $query .= "WHERE nro_campo=$nro ";
        $query .= "ORDER BY nro_campo ASC";
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR ::'.__METHOD__."::",DBConnector::mensaje();
        
    }
    /**
     * selecciona el cultivo segun el nro de campo o numero de solicitud
     * @param int $campo numero de solicitud o numero de campo
     * */
    public function getListMuestras($campo, $sistema) {

        $query = "SELECT * FROM view_muestras_laboratorio ";
        $query .= "WHERE (nro_solicitud LIKE '$campo%' OR nro_campo LIKE '$campo%' OR nro_analisis LIKE '$campo%') AND sistema = $sistema AND estado=0";
        #echo $query; exit ;
        DBConnector::ejecutar($query);
    }

    /**
     * selecciona el cultivo segun el nro de campo
     * */
    public function getMuestra($campo) {
        $query = "SELECT * FROM laboratorio WHERE nro_campo LIKE '$campo%'";

        DBConnector::ejecutar($query);
    }

    /**
     * obtiene la muestra de una muestra para ingresar los resultados de analisis de acuerdo al id de laboratorio
     * */
    public static function getMuestraById($id) {
        $query = "SELECT * FROM view_muestras_laboratorio WHERE nro_analisis=$id";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * obtiene la muestra de una muestra para ingresar los resultados de analisis de acuerdo al id de laboratorio
     * */
    public function getMuestraById2($id) {
        $query = "SELECT * ";
        $query .= "FROM view_muestras_laboratorio ";
        $query .= "WHERE nro_solicitud='$id' AND estado_solicitud = 9";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__."::$query:: ",DBConnector::mensaje();
        }
    }

    /**
     * obtiene la muestra de una muestra para ingresar los resultados de analisis de acuerdo al id de laboratorio
     * */
    public static function getIdMuestraByNroSolicitud($id) {
        $query = "SELECT nro_analisis FROM view_muestras_laboratorio WHERE nro_solicitud='$id'";
        echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnetor::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> nro_analisis;
    }

    /**
     * obtiene la muestra de una muestra para ingresar los resultados de analisis de acuerdo al id de laboratorio
     * */
    public function verMuestraById($id) {
        $query = "SELECT * FROM laboratorio WHERE id_laboratorio=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * cuenta el numero de analisis
     * */
    public static function getAnalisisCount() {
        $query = "SELECT COUNT(*) AS  nro FROM view_muestras_laboratorio";

        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
        $obj = DBConnector::objeto();

        return $obj -> nro;
    }

    /**
     * selecciona el cultivo segun el nro de campo y la fecha de recepcion
     * */
    public static function getMuestras($campo = '',$area='') {
        $query = "SELECT * ";
        $query .= "FROM view_muestras_laboratorio ";
        $query .= "WHERE sistema = $area AND gestion = '".date('Y')."' ";
        $query .= "AND (nro_campo  LIKE '$campo%' OR origen LIKE '$campo%' OR cultivo LIKE '$campo%' OR variedad LIKE '$campo%' OR semillerista LIKE '$campo%')";

        DBConnector::ejecutar($query);
        
        if (DBConnector::nroError())
            echo 'ERRO view_muestras_laboratorio ',DBConnector::mensaje();
    }

    /**
     * selecciona el cultivo segun el nro de campo y la fecha de recepcion
     * solo certificacion
     * */
    public static function getListMuestrasCertificacion($campo = '') {
        $query = "SELECT * FROM view_muestras_laboratorio ";
        $query .= "WHERE (nro_campo  LIKE '$campo%' OR origen LIKE '$campo%' OR cultivo LIKE '$campo%' OR variedad LIKE '$campo%' OR semillerista LIKE '$campo%') AND sistema = 1 ";
        $query .= "ORDER BY nro_analisis DESC";
        #$query .= " AND analizado = 0";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * selecciona el cultivo segun el nro de campo y la fecha de recepcion
     * solo certificacion
     * */
    public static function getListMuestrasCertificacionv2() {
        $gestion = date('Y');
        $query = "SELECT * FROM view_muestras_laboratorio ";
        $query .= "WHERE fecha_recepcion LIKE '".date('Y')."-%' AND sistema = 1 ";
        $query .= "AND analizado = 1 ";
        $query .= "ORDER BY id_muestra DESC";
        
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * selecciona el cultivo segun el nro de campo y la fecha de recepcion
     * */
    public static function getMuestrasById($id) {
        $query = "SELECT * FROM view_muestras_laboratorio WHERE id_analisis=$id";

        DBConnector::ejecutar($query);
    }

    public function getMuestrasLaboratorioByCampo($search = '') {
        $query = "SELECT * FROM view_muestras_laboratorio ";        
        $query .= "AND gestion = '".date('Y')."' LIMIT 0,15";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    public function getMuestrasLaboratorioByCampo2($search='',$inicio=0,$registros=25) {
        $query = "SELECT * FROM view_muestras_laboratorio ";
        if (empty($search)){
            $query .= "WHERE gestion = '".date('Y')."' AND analizado = 1 ";
        }else{
            $query .= "WHERE nro_campo  LIKE '$search%' OR cultivo LIKE '$search%' OR semillerista LIKE '$search%' ";
        }
        
        $query .= "ORDER BY nro_campo DESC LIMIT $inicio,$registros";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * selecciona el cultivo segun el nro de campo y la fecha de recepcion
     * */
    public function getMuestrasFilter($search) {
        $query = "SELECT * FROM view_muestras_laboratorio ";
        $query .= "WHERE nro_campo LIKE '$search%' OR fecha_recepcion LIKE '$search%' OR ";
        $query .= "semillerista LIKE '$search%' OR cultivo,variedad LIKE '$search%'";

        DBConnector::ejecutar($query);
    }

    /**
     * muestras en certificacion
     * */
    public function getMuestrasCertificacion($campo) {
        $query = "SELECT * FROM view_muestras_certificacion ";
        $query .= "WHERE estado = 8 AND sistema LIKE 'certifica%' AND nro_campo='$campo'";

        DBConnector::ejecutar($query);
    }

    /**
     * muestras para laboratorio
     * */
    public function getMuestrasLaboratorio($campo, $sistema) {
        $query = "SELECT * FROM view_muestras_certificacion ";
        $query .= "WHERE nro_campo='$campo' AND sistema='$sistema' AND fecha_recepcion LIKE '".date('Y')."-%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * realiza la busqueda de resutlado de analsis segun
     * la semilla, nro de campo, nro de analisis o nro lote
     *
     * **/
    public function getBuscarResultadoMuestras($campo) {
        $query = "SELECT * FROM view_resultado_muestra_certificada";
        # WHERE  estado=1 AND (semilla LIKE '%$campo%' OR nro_campo LIKE '%$campo%' OR nro_lote LIKE '%$campo%' OR nro_analisis LIKE '%$campo%')";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * cultivo que esta a la espera de resultado segun su numero de campo asignado
     * */
    private function getWCampo($nro) {
        $query = "SELECT * FROM laboratorio ";
        $query .= "WHERE nro_campo=$nro AND estado=1";

        DBConnector::ejecutar($query);
    }

    /**
     * numero de campo en laboratorio que ya tienen resultado
     * 1 ya tiene resultado de analisis
     * 2 esperando resultados de analisis
     * */
    public function getCampo($nro) {

        $this -> getWCampo($nro);
        if (!DBConnector::filas()) {
            $query = "SELECT * FROM laboratorio WHERE nro_campo=$nro AND estado=1";
            DBConnector::ejecutar($query);
            if (DBConnector::filas())
                return 1;
        } else {
            return 2;
        }
    }

    /**
     * resultados de analisis realizados
     */
    public static function getResultados() {
        $query = "SELECT DISTINCT(nro_campo) FROM view_resultado_muestra_certificada";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * numero de analisis para importar desde csv
     * */
    public static function getNroAnalisisImportar($nro, $nro_solicitud, $sistema) {
        if (empty($nro))
            return 0;
        else {
            if (strtolower(substr($sistema, 0, 9)) == 'fiscaliza') {
                $nro = $nro_solicitud;
            }
            $semicolon_pos = strpos(";", $nro);
            if (!$semicolon_pos) {
                $pos = strpos($nro, "-");
                if ($pos) {
                    $new_text = substr($nro, 0, strlen($nro) - 3);
                    $gestion = explode("/", $nro);
                    //obtener gestion
                    $temp = explode("-", $new_text);
                    //obtener numero de analisis
                    $new_analisis = array();
                    foreach ($temp as $key => $analisis) {
                        array_push($new_analisis, substr(str_replace(";", "",$analisis),0,3));
                    }
                    return $new_analisis;
                } else {
                    $new_analisis = explode("/", $nro);
                    return substr(str_replace(";","",$new_analisis[0]),0,3);
                }
            } else {
                $varios_nros = explode(";", $nro);
                foreach ($varios_nros as $key => $nro) {
                    $pos = strpos($nro, "-");
                    if ($pos) {
                        $new_text = substr($nro, 0, strlen($nro) - 3);
                        $gestion = explode("/", $nro);
                        //obtener gestion
                        $temp = explode("-", $new_text);
                        //obtener numero de analisis
                        $new_analisis = array();
                        foreach ($temp as $key => $analisis) {
                            array_push($new_analisis, substr(str_replace(";", "", $analisis),0,3));
                        }
                        return $new_analisis;
                    } else {
                        $new_analisis = explode("/", $nro);
                        return substr(str_replace(";","",$new_analisis[0]),0,3);
                    }
                }
            }
        }
    }

    /**
     * nro de bolsa de un determinado campo
     */
    public static function getNroBolsasSemillera($nroCampo) {
        $query = "SELECT nro_cupones AS bolsa FROM cosecha WHERE nro_campo=$nroCampo";
        //echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * nro de bolsa de un determinado campo
     */
    public static function getNroBolsasSemilleraFiscal() {
        $query = "SELECT nro_cupones AS bolsa FROM cosecha WHERE nro_campo=$nroCampo";
        //echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * numeros de campo para muestra de laboratorio en certificacion
     * */

    /**actualiza es estado de la semilla producida
     * id ID de la cosecha
     * */
    public static function updateEstado($id) {
        $query = "UPDATE muestra_laboratorio SET estado = 1 ";
        $query .= "WHERE (id_cosecha = $id AND estado = 0)";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    public static function updateEstadoAnalisis($id) {
        $query = "UPDATE muestra_laboratorio SET estado = 1 ";
        $query .= "WHERE id_muestra = $id";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    public static function getLaboratorioById($id, $link) {
        $query = "SELECT * FROM laboratorio WHERE id_laboratorio=$id";

        return mysql_query($query, $link);
    }

    /**
     * devuelve el id del sistema segun el id de la muestra
     * @param integer $id  id de la muestra
     * */
    public static function getMuestraLaboratorioBySistema($id) {
        $query = "SELECT sistema FROM view_muestras_laboratorio WHERE nro_campo='$id'";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
        $row = DBConnector::objeto();

        return $row -> sistema;
    }

    /**
     * devuelve el id del tipo de analisis a realizar
     * */
    public static function getIdTipoAnalisis($germinacion, $humedad, $pureza) {
        #echo $germinacion.'='.$humedad.'='.$pureza;
        if ($germinacion == 1 AND $humedad == 1 AND $pureza == 1) {
            $condicion = "(germinacion=$germinacion AND humedad=$humedad AND pureza=$pureza)";
        } elseif ($pureza == 1 AND $humedad == 1) {
            $condicion = "(pureza=$pureza AND humedad=$humedad AND germinacion=0)";
        } elseif ($germinacion == 1 AND $humedad == 1) {
            $condicion = "(germinacion = $germinacion AND humedad = $humedad AND pureza=0)";
        } else {
            $condicion = "(pureza=$pureza AND germinacion=$germinacion AND humedad=0)  ";
        }
        $query = "SELECT id_analisis FROM view_tipo_analisis WHERE ";

        $query .= $condicion;
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_analisis;
    }

    /**
     * Extrae los datos para ser insertados en un scrpt sql
     * @param  id_laboratorio	mediumint(9)
     * @param  nro_analisis	varchar(10)
     * @param  fecha_recepcion	date
     * @param  fecha_resultado	date
     * @param  nro_campo	varchar(8)
     * @param  nro_lote	varchar(15)
     * @param  tipo_semilla	varchar(20)
     * @param  nro_bolsa	smallint(5)
     * @param  kg_bolsa	tinyint(3)
     * @param  total	mediumint(8)
     * @param  calibre	varchar(25)
     * @param  nro_semilla_kilo	smallint(6)
     * @param  pureza	decimal(5,2)
     * @param  humedad	decimal(5,2)
     * @param  germinacion	decimal(5,2)
     * @param  observacion	text
     * @param  estado	tinyint(1)
     * @param  id_cosecha	mediumint(9)
     */
    public function getLaboratorioSQL() {
        $this -> getLaboratorio();
        if (DBConnector::filas() == 0)
            return '';
        $query = 'INSERT INTO cosecha (id_muestra,nro_analisis,fecha_recepcion,nro_campo,origen,analisis,sistema,estado,tipo_semilla) VALUES';
        while ($row = DBConnector::objeto()) {
            $query .= "(".$row->id_muestra.",'";
            $query .= $row -> nro_analisis . "','";
            $query .= $row -> fecha_recepcion . "'," . $row -> nro_campo . ",'";
            $query .= $row -> origen . "'," . $row -> analisis . ",";
            $query .= $row -> sistema . "," . $row -> estado . ",";
            $query .= $row -> tipo_semilla . "),\r\n";
        }
        $query = substr($query, 0, strlen($query) - 3);
        $query .= ";";

        return $query;
    }

}

// END
?>