<?php
/**
 * modelo para acondicionamiento
 */
class Acondicionamiento {
    /**
     * @param array  $acondicionamiento   contiene todos loa datos de acondicionamiento
     * */
    public function insertAcondicionamiento(array $acondicionamiento){
        $query = "INSERT INTO acondicionamiento (id_solicitud,nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado,";
        $query .= "superficie,rendimiento_campo,nro_cupones,rango_cupones,planta_acondicionadora,peso_bruto_semilla,cultivo,variedad) VALUES (";
        $query .= $acondicionamiento['isolicitud'].",'".$acondicionamiento['campo']."','".$acondicionamiento['fecha']."',".$acondicionamiento['categoria_en_campo'].",";
        $query .= $acondicionamiento['rendimiento_estimado'].",".$acondicionamiento['superficie'].",".$acondicionamiento['rendimiento_campo'].",";
        $query .= $acondicionamiento['cupones'].",'".$acondicionamiento['rango']."','".$acondicionamiento['planta']."',".$acondicionamiento['pesobruto'].",";
        $query .= $acondicionamiento['cultivo'].",".$acondicionamiento['variedad'].")";
        
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
    }
        /**semillas de productores por bloque*/
    public function getAcondicionamientoByBlock($area, $inicio = 0, $nroReg = 15) {
        $query = "SELECT * FROM view_acondicionamiento";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND gestion = '" . date('Y') . "'";
        $query .= " ORDER BY id_solicitud DESC LIMIT $inicio,$nroReg";
        #echo $query;
        return $query;
    }
    /**
     * devuelve el acondicionamiento segun el id de solicitud
     * */
     public function getAcondicionamientoByIdSolicitud($id){
         $query = "SELECT * FROM view_acondicionamiento ";
         $query .= "WHERE id_solicitud = $id";
         
         DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
     }
     
     /**
      * eliminar acondicionamiento segun id_acondicionamiento o id_solicitud
      * */
      public function deleteAcondicionamiento ($id,$tipo='acondicionamiento'){
          if ($acondicionamiento == 'acondicionamiento'){
              $query = "DELETE FROM acondicionamiento WHERE id_acondicionamiento = $id";
          }else{
              $query = "DELETE FROM acondicionamiento WHERE id_solicitud = $id";
          }
          DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
      }
}
?>