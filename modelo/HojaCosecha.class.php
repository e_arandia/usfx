<?php
/**
 * Realiza las operaciones necesarias en una hoja de cosecha
 * @author  Edwin Willy Arandia Zeballos
 */
class HojaCosecha {
    /**
     * Ingresa una nueva hoja de cosecha
     * @param  int       nroCampo      		 numero de campo
     * @param  date      fechaEmision    	 fecha de emision de la hoja de cosecha
     * @param  varchar   categoriaCampo  	 categoria en campo
     * @param  decimal   rendimientoEstimado   rendimiento estimado de la cosecha
     * @param  decimal   superficieParcela     tamanho de la superficie
     * @param  decimal   rendimientoCampo      rendimiento estimado del campo
     * @param  mediumint nroCupones            numero de cupones
     * @param  varchar   rangoCupones          rango de cupones
     * @param  text      plantaAcondicionadora    planta acondicionadora
     * @param  boolean   laboratorio      		 determina si pasa por laboratorio o no
     * si es 1 va a laboratorio -  si es 0 no va a laboratorio
     */
    public function insertHojaCosecha($nroCampo, $fecha, $categoriaCampo = '', $rendimientoEstimado = 1, $superficieParcela = 1, $rendimientoCampo = 1,  $plantaAcondicionadora = '', $laboratorio = 1, $estado, $idSuperficie) {

        $query = "INSERT INTO cosecha (nro_campo,fecha_emision,categoria_en_campo,";
        $query .= "rendimiento_estimado, superficie_parcela,rendimiento_campo,";
        $query .= "planta_acondicionadora,alaboratorio,estado,id_superficie) ";
        $query .= "VALUES('$nroCampo','$fecha','$categoriaCampo',$rendimientoEstimado,";
        $query .= "$superficieParcela, $rendimientoCampo, '$plantaAcondicionadora',";
        $query .= "$laboratorio,$estado,$idSuperficie)";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        }
    }

    /**
     * Ingresa una nueva hoja de cosecha
     * @param  int       nroCampo            numero de campo
     * @param  date      fechaEmision        fecha de emision de la hoja de cosecha
     * @param  varchar   categoriaCampo      categoria en campo
     * @param  decimal   rendimientoEstimado   rendimiento estimado de la cosecha
     * @param  decimal   superficieParcela     tamanho de la superficie
     * @param  decimal   rendimientoCampo      rendimiento estimado del campo
     * @param  mediumint nroCupones            numero de cupones
     * @param  text      plantaAcondicionadora    planta acondicionadora
     * @param  boolean   laboratorio             determina si pasa por laboratorio o no
     * @param  unknown   estado
     * si es 1 va a laboratorio -  si es 0 no va a laboratorio
     */
    public static function setHojaCosechaImport($nroCampo, $fecha, $categoriaCampo, $rendimientoEstimado, $superficieParcela, $rendimientoCampo, $nroCupones, $plantaAcondicionadora, $laboratorio, $estado, $idSuperficie) {

        $query = "INSERT INTO cosecha (nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado, superficie_parcela,rendimiento_campo,";
        $query .= "nro_cupones,planta_acondicionadora,alaboratorio,estado,id_superficie) VALUES(";
        $query .= "$nroCampo,'$fecha',$categoriaCampo,$rendimientoEstimado,$superficieParcela, $rendimientoCampo, $nroCupones, '$plantaAcondicionadora',";
        $query .= "$laboratorio,$estado,$idSuperficie)";

        DBConnector::ejecutar($query);
        if (empty($query))
            echo "hoja de cosecha.Consulta vacia!!!";
        if (DBConnector::nroError()) {
            echo "ERROR::".__METHOD__."::".DBConnector::mensaje(), '=', $query;
            exit ;
        }
    }

    public static function setHojaCosechaFiscalImport($nroCampo, $estado, $plantaAcondicionadora) {

        $query = "INSERT INTO cosecha (nro_campo,estado,planta_acondicionadora) VALUES (";
        $query .= "$nroCampo,$estado,'$plantaAcondicionadora')";

        DBConnector::ejecutar($query);
        if (empty($query))
            echo "hoja de cosecha.Consulta vacia!!!";
        if (DBConnector::nroError()) {
            echo "ERROR en cosecha FISCAL:: " . $query.'='.DBConnector::mensaje();
            exit ;
        }

    }

    /**
     * hoja cosecha fiscalizacion
     * */
    public function insertHojaCosechaFiscal($nro_campo, $laboratorio,$plantaAcondicionadora, $idSuperficie) {

        $query = "INSERT INTO cosecha (nro_campo,fecha_emision,alaboratorio,planta_acondicionadora,id_superficie) "; 
        if ($laboratorio){
            $query .= "VALUES($nro_campo,'".date('Y-m-d')."',1,'$plantaAcondicionadora',$idSuperficie)";
        }else{
            $query .= "VALUES($nro_campo,'".date('Y-m-d')."',0,'$plantaAcondicionadora',$idSuperficie)";
        }
        
        #echo $query;exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR insertar hoja de cosecha fiscal ::",DBConnector::mensaje();
    }

    /**
     * actualiza los datos de una hoja d cosecha segun el id
     * */
    public static function updateHojaCosecha($nroCampo, $fecha, $categoriaCampo, $rendimientoEstimado, $superficieParcela, $rendimientoCampo, $nroCupones, $rango, $plantaAcondicionadora, $laboratorio, $id) {
        $query = "UPDATE cosecha SET nroCampo=$nroCampo,fecha='$fecha',
		 categoriaCampo='$categoriaCampo',rendimientoEstimado=$rendimientoEstimado,
	     superficieParcela=$superficieParcela, rendimientoCampo=$rendimientoCampo,
	     nroCupones=$nroCupones, rango='$rango', plantaAcondicionadora='$plantaAcondicionadora'
	     WHERE id_cosecha = $id";

        DBConnector::ejecutar($query);
    }

    /**
     * determina si el cultivo va a laboratorio segun el cultivo
     * */
    public static function getLaboratorioImportar($cultivo) {
        if ($cultivo == "papa" || $cultivo == "Papa")
            return 0;
        else
            return 1;
    }

    public static function getLastHojaCosecha($objPHPExcel, $col, $row) {
        $cosecha['nrocampo'] = $objPHPExcel -> getCellByColumnAndRow($col, $row) -> getCalculatedValue();
        while ($cosecha['nrocampo'] == '') {
            if ($cosecha['nrocampo'] >= 0.00) {
                break;
            }
            $cosecha['fecha_emision'] = $objPHPExcel -> getCellByColumnAndRow(($col + 1), ($row - 1)) -> getCalculatedValue();
            $cosecha['categoria_campo'] = Categoria::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($objPHPExcel -> getCellByColumnAndRow(($col + 2), ($row - 1)) -> getCalculatedValue());
            $cosecha['rendimiento_estimado'] = $objPHPExcel -> getCellByColumnAndRow(($col + 3), ($row - 1)) -> getCalculatedValue();
            $cosecha['superficie'] = $objPHPExcel -> getCellByColumnAndRow(($col + 4), ($row - 1)) -> getCalculatedValue();
            $cosecha['rendimiento_campo'] = $objPHPExcel -> getCellByColumnAndRow(($col + 5), ($row - 1)) -> getCalculatedValue();
            $cosecha['nro_cupones'] = $objPHPExcel -> getCellByColumnAndRow(($col + 6), ($row - 1)) -> getCalculatedValue();
            $cosecha['planta_acondicionadora'] = $objPHPExcel -> getCellByColumnAndRow(($col + 7), ($row - 1)) -> getCalculatedValue();
        }
        return $cosecha;
    }

    /**
     * busqueda de datso para importar desde csv
     * */
    public static function getLastHojaCosechav2($data, $col, $row, $total) {
        $cosecha['nrocampo'] = $data[$col][$row];
        if ($col < $total) {
            while ($cosecha['nrocampo'] == '') {
                if ($cosecha['nrocampo'] >= 0.00) {
                    break;
                }
                $cosecha['fecha_emision'] = $data[$col + 1][$row - 1];
                $cosecha['categoria_campo'] = Categoria::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($data[$col + 2][$row - 1]);
                $cosecha['rendimiento_estimado'] = $data[$col + 3][$row - 1];
                $cosecha['superficie'] = $data[$col + 4][$row - 1];
                $cosecha['rendimiento_campo'] = $data[$col + 5][$row - 1];
                $cosecha['nro_cupones'] = $data[$col + 6][$row - 1];
                $cosecha['planta_acondicionadora'] = $data[$col + 7][$row - 1];
            }
        } else {
            $cosecha['fecha_emision'] = $cosecha['categoria_campo'] = '';
            $cosecha['rendimiento_estimado'] = $cosecha['superficie'] = $$cosecha['rendimiento_campo'] = $cosecha['nro_cupones'] = 0;
            $cosecha['planta_acondicionadora'] = '';
        }
        return $cosecha;
    }

    /**
     * nro de campo para hoja de cosecha con opcion de importar desde excel
     * */
    public static function getNroCampoImportar($objExcel, $col, $row) {
        $nroCampo = $objExcel -> getCellByColumnAndRow($col, $row) -> getCalculatedValue();
        if ($nroCampo == '') {
            return $objExcel -> getCellByColumnAndRow(10, $row) -> getCalculatedValue();
        } else {
            if ($nroCampo == 0)
                return 0;
            else
                return $nroCampo;
        }
    }

    /**
     * nro de campo para hoja de cosecha con opcion de importar desde CSV
     * */
    public static function getNroCampoImportarv2($nro) {
        $nroCampo = $nro;
        if ($nroCampo == '') {
            return 0;
        } else {
            if ($nroCampo == 0)
                return 0;
            else
                return $nroCampo;
        }
    }

    /**
     * numero de todos los campos que pasaran a pruebas de laboratorio
     */
    public function getNroCamposLab($sistema) {
        $query = "SELECT nro_campo FROM view_hoja_cosecha ";
        $query .= "WHERE gestion='".date('Y')."' AND sistema LIKE '$sistema%' AND cultivo <> 'papa' ";

        DBConnector::ejecutar($query);
    }

    /**
     * selecciona el cultivo de un campo
     * una vez que  obtiene su hoja de cosecha
     * */
    public function getNroCampoCosecha($nroCampo) {
        $query = "SELECT * FROM `semilla`sem INNER JOIN superficie sup ON sem.id_semilla=sup.id_semilla
INNER JOIN cosecha cos ON cos.id_superficie=sup.id_superficie where sem.nro_campo=$nroCampo";

        DBConnector::ejecutar($query);

    }

    /**
     * Elimina una hoja de cosecha segun el id
     * */
    public function deleteHojaCosecha($id) {
        $query = "DELETE FROM cosecha WHERE id_cosecha=$id";
        //echo $query; exit;

        DBConnector::ejecutar($query);
    }

    /**
     * lista todos los campos cque son cultivos de papa de un semillerista
     * @param string $nombre nombre de semillerista
     * @param string $apellido apellido de semillerista
     */
    public function cultivoPapa($nombre, $apellido) {
        $query = "SELECT DISTINCT(nro_campo) FROM view_datos_cosecha WHERE cultivo='papa'";
        $query .= " AND nombre LIKE '$nombre%' AND apellido LIKE '%$apellido%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * obtener el cultivo de acuerdo al nro de campo
     * */
    public function cultivo_Campo($nroCampo) {
        $query = "SELECT cultivo FROM view_semilla ";
        $query .= "WHERE nro_campo = '$nroCampo'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * obtiene el id de una cosecha segun el nro de campo
     * */
    public static function getFechaEmisionImportar($fecha) {
        if (empty($fecha)) {
            return '00-00-0000';
        } else {
            return $fecha;
        }
    }

    /**
     * obtiene el id de una cosecha segun el nro de campo
     * */
    public function getHojaCosechaId($campo) {
        $query = "SELECT id_cosecha 
                  FROM view_hoja_cosecha WHERE nro_campo = '$campo'";
        //	echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * obtiene el id de una cosecha segun id de semilla
     * */
    public static function getHojaCosechaByIdSemilla($id) {
        $query = "SELECT id_cosecha "; 
        $query .= "FROM view_hoja_cosecha ";
        $query .= "WHERE id_semilla = $id";
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError()){
            echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();
        
            return $obj->id_cosecha;
        }
            
        
    }

    public function hojaCosechaID2($campo) {
        $query = "SELECT MAX(id_cosecha) as id_cosecha FROM cosecha WHERE nro_campo = '$campo' AND (estado = 0 OR estado = 1 OR estado = 4)";
        DBConnector::ejecutar($query);
    }

    /**
     * Obtiene los datos de cosecha de un determinado campo
     */
    public function getHojaCosecha() {
        $query = "SELECT * FROM cosecha ";
        return DBConnector::ejecutar($query);
    }

    /**
     * filtro de joja de cosecha  para certificacion
     * */
    public static function getHojaCosechaCertificaFilter($search) {
        $query = "SELECT semillera,semillerista,cultivo,planta_acondicionadora,";
        $query .="fecha_emision,nro_campo,categoria_en_campo AS categoria_campo,rendimiento_estimado AS rendimiento,superficie_parcela,";
        $query .="rendimiento_campo,nro_cupones AS cupones ,rango_cupones AS rango ";
        $query .= "FROM view_hoja_cosecha ";
        $query .= "WHERE (semillera LIKE '$search%' OR semillerista LIKE '$search%' OR cultivo LIKE '$search%' OR planta_acondicionadora LIKE '$search%') ";
        $query .= "AND sistema LIKE 'certifica%' AND estado_solicitud>=8 AND gestion = '".date('Y')."'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR::".__METHOD__."::",DBConnector::mensaje();
        }
    }

    /**
     * filtro de joja de cosecha  para fiscalizacion
     * */
    public static function getHojaCosechaFiscalFilter($search) {
        $query = "SELECT semillera,CONCAT(nombre,' ',apellido) AS semillerista,cultivo,planta_acondicionadora FROM view_datos_cosecha_fiscal ";
        $query .= "WHERE semillera LIKE '$search%' OR nombre LIKE '$search%' OR apellido LIKE '$search%' OR Cultivo LIKE '$search%' OR planta_acondicionadora LIKE '$search%'";

        DBConnector::ejecutar($query);
    }

    /**
     * datos de hoja de cosecha para Certificacion por id de cosecha
     * */
    public static function getHojaCosechaCertificaById($id) {
        $query = "SELECT * FROM view_datos_cosecha WHERE id_cosecha=$id";

        DBConnector::ejecutar($query);
    }
    /**
     * datos de hoja de cosecha para Certificacion por id de cosecha
     * */
    public static function getHojaCosechaCertificaByIdSolicitud($id) {
        $query = "SELECT * ";
        $query .= "FROM cosecha ";
        $query .= "WHERE id_cosecha=$id";
#echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__,":: $query ::",DBConnector::mensaje();
    }
    /**
     * datos de hoja de cosecha para fiscalizacion
     * */
    public static function getHojaCosechaFiscalById($id) {
        $query = "SELECT * FROM view_datos_cosecha_fiscal WHERE id_cosecha=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * total de rendimiento de la parcela de un cultivo en un sistema
     * @param string $cultivo nombre del cultivo
     * @param string $sistema  id del sistema [certificacion|fiscalizacion]
     *
     * resumen campaña
     * */
    public static function getVolumenTotalByCultivo($cultivo, $sistema, $desde = '', $hasta = '',$gestion='') {
        $query = "SELECT SUM(rendimiento_campo) AS total FROM view_hoja_cosecha ";
        $query .= "WHERE cultivo = '$cultivo' AND id_sistema LIKE '$sistema%' ";
        if ($desde) {
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query .= "AND (mes >='$desde' AND mes <='$hasta')";
                $query .= "AND gestion = '$gestion' AND estado_solicitud>=12 ";
            } else {
                $query .= "AND (fecha >='$desde' AND fecha <='$hasta') AND estado_solicitud>=12 ";
            }
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR [volumen produccion] ::",DBConnector::mensaje();

    }

    /**
     * categoria campo para importar
     * */
    public static function getCategoriaCampoImportar($categoria) {
        $categoria_rechazada = array("RECHAZADA");
        if (in_array(strtoupper($categoria), $categoria_rechazada)) {
            $query = "SELECT id_generacion FROM view_lst_categoria_generacion ";
            $query .= "WHERE generacion = 'rechazada'";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();

            return $obj -> id_generacion;
        } else {
            return Categoria::getIdCategoriaByNameAndGeneracionHojaCosechaImportar($categoria);
        }
    }

    /**
     * cosecha de cada solicitud
     */
    function getCosechaSolicitud($area) {
        $query = "SELECT * FROM view_datos_cosecha";
        $query .= " WHERE id_sistema=$area";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * reporte de volumen de produccion
     * @param string $cultivo    nombre del cultivo
     * @param string $variedad   nombre de la variedad
     * @param string $categoria  nombre de la categoria
     * @param mixed  $desde      1 si la busqueda sera trimestral | xxxx-xx-xx  si es por fecha
     * @param mixed  $hasta      valor segun la variable desde
     * */
    public static function getTotalCategoriaVariedadCultivo($cultivo, $variedad, $categoria, $desde, $hasta,$sistema=1) {
        $query = "SELECT SUM(rendimiento_campo) AS total FROM view_reporte_volumen_produccion_cultivos_categorias ";
        $query .= "WHERE cultivo LIKE ('$cultivo%') AND variedad LIKE ('$variedad%') AND categoriaP LIKE ('$categoria%')";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' AND id_sistema=$sistema ";
            $query .= "AND ((estado = 8 AND cultivo = 'papa') OR (estado = 12 AND cultivo <> 'papa'))";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND id_sistema=$sistema ";
            $query .= "AND ((estado = 8 AND cultivo = 'papa') OR (estado = 12 AND cultivo <> 'papa'))";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        if (is_null($obj -> total)) {
            return 0;
        } else {
            return $obj -> total;
        }
    }

    /**
     * reporte de volumen de produccion de la gestion actual
     * @param mixed  $desde      1 si la busqueda sera trimestral | xxxx-xx-xx  si es por fecha
     * @param mixed  $hasta      valor segun la variable desde
     * */
    public static function getTotalVolumenProduccion($desde, $hasta,$sistema=1) {
        $query = "SELECT COUNT(*) AS total FROM view_reporte_volumen_produccion_cultivos_categorias ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND gestion = '".date('Y')."' AND id_sistema=$sistema ";
            $query .= "AND ((estado = 8 AND cultivo = 'papa') OR (estado = 12 AND cultivo <> 'papa'))";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND id_sistema=$sistema ";
            $query .= "AND ((estado = 8 AND cultivo = 'papa') OR (estado = 12 AND cultivo <> 'papa'))";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo 'ERROR ::'.__METHOD__."||$query||",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();
            if ($obj -> total) {
                return $obj -> total;
            } else {
                return 0;
            }
        }
    }

    /**
     * cosecha de cada solicitud
     */
    function getCosechaSolicitudFiscal($area) {
        $query = "SELECT * FROM view_datos_cosecha_fiscal ";
        $query .= "WHERE gestion = '".date('Y')."' "; 
        $query .= "ORDER BY id_cosecha DESC";
        #echo $query; exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR view_datos_cosecha_fiscal ',DBConnector::mensaje();
    }

    /**
     * cosecha de cada solicitud
     */
    function getCosechaSolicitudByBlock($area, $limit, $nroRegistros) {
        $query = "SELECT * FROM view_hoja_cosecha";
        if ($area != 10)
            $query .= " WHERE gestion='".date('Y')."'";
        $query .= " ORDER BY id_cosecha DESC LIMIT $limit,$nroRegistros";

        return $query;
    }

    /**
     * cosecha de cada solicitud
     */
    function getCosechaSolicitudFiscalByBlock($area, $limit, $nroRegistros) {
        $query = "SELECT * FROM view_datos_cosecha_fiscal";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND gestion = '".date('Y')."'";
        $query .= " ORDER BY id_cosecha DESC LIMIT $limit,$nroRegistros";

        return $query;
    }

    /**
     * @param integer $id  numero id de hoja de cosecha
     *
     * @return devuelve el id solicitud  y el id superficie segun el id de cosecha
     * */
    public function getSupSolicitudID($id) {
        $query = "SELECT id_solicitud,id_superficie FROM view_sup_solicitud_id WHERE id_cosecha=$id";
        #echo $query;
        DBConnector::ejecutar($query);

        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        }
    }

    /**
     * @param integer $id  numero id de hoja de cosecha
     * */
    public function getSuperficieSolicitudID($icosecha) {
        $query = "SELECT id_superficie FROM view_sup_solicitud_id WHERE id_cosecha=$icosecha";
        #echo $query;
        DBConnector::ejecutar($query);

        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            $obj = DBConnector::objeto();

            return $obj -> id_superficie;
        }
    }

    /**
     * @param id  id de la superficie
     * */
    public static function updateStado($id) {
        $query = "UPDATE cosecha SET estado = 1 WHERE (id_cosecha = $id AND estado = 0)";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * campos q pasan a laboratorio y no son papa
     * @param estado 0 no tienen resultado de laboratorio.  1 ya tienen resultado de laboratorio
     * */
    public static function analisis() {
        $query = "SELECT nro_campo FROM cosecha WHERE laboratorio = 1 AND estado = 0";

        DBConnector::ejecutar($query);
    }

    /**
     * hoja de cosecha segun el id
     * */
    public function getCosechaByID($id, $link) {
        $query = "SELECT * 
		          FROM cosecha cos 
		          INNER JOIN superficie sup ON sup.id_superficie=cos.id_superficie
		          INNER JOIN solicitud sol ON sol.id_solicitud=sup.id_solicitud
		          INNER JOIN semilla sem ON sem.id_solicitud=sol.id_solicitud
		          WHERE cos.id_cosecha=$id";

        return mysql_query($query, $link);
    }
    /**
     * devuelve el numero maximo de cupones de emitidos
     * */
     public function getCupones(){
         $query = "SELECT rango ";
         $query .= "FROM view_semilla_producida ";
         $query .= "WHERE gestion ='".date('Y')."' ";
        #echo $query; 
         DBConnector::ejecutar($query);
         
         if(DBConnector::nroError()){
             echo "ERROR :: ",__METHOD__," :: $query ::",DBConnector::mensaje();
         }else{
             $obj = DBConnector::objeto();
             if(!empty($obj->rango)){                
                return $obj->rango;    
             }else{
                 return 1000;
             }             
         }
     }
    /**
     * id de cosecha segun su id de semilla
     * @param integer $id  id de la semilla
     * */
    public function getIdCosechaByIdSem($id) {
        if ($id != '-' && $id != '') {
            $query = "SELECT id_cosecha FROM view_hoja_cosecha WHERE id_semilla=$id";
            #echo $query;
            DBConnector::ejecutar($query);
            #echo "filas : ".DBConnector::filas();
            if (DBConnector::nroError()) {
                echo DBConnector::mensaje(), '=', $query;
                return 0;
            } else {
                if (DBConnector::filas() == 0) {
                    return 0;
                } else {
                    $obj = DBConnector::objeto();
                    return $obj -> id_cosecha;
                }
            }
        } else {
            return 0;
        }
    }

    /**
     * devuelve el id de la cosecha segun el nro de campo
     * */
    public function getIdCosechaByNroCampo($nro) {
        $query = "SELECT MAX(id_cosecha)AS id_cosecha FROM view_datos_cosecha WHERE nro_campo='$nro'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> id_cosecha;
        else {
            return '-1';
        }
    }

    /**
     * devuelve el id de la cosecha segun el nro de campo
     * */
    public function getIdCosechaByNroCampo2($nro) {
        $query = "SELECT MAX(id_cosecha) AS id_cosecha FROM view_hoja_cosecha WHERE nro_campo='$nro'";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            if (DBConnector::filas())
                return $obj -> id_cosecha;
            else {
                return '-1';
            }
        }

    }

    /**
     * nro de campo segun el id de la semilla
     * @param integer $id  id de la semilla
     * */
    public function getNroCampoByIdSemilla($id) {
        $query = "SELECT nro_campo FROM view_semilla WHERE id_semilla=$id";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> nro_campo;
    }

    /**
     * registro de hoja de cosecha
     * */
    public static function getNroCuponesImportar($nro) {
        if (empty($nro)) {
            return 0.00;
        } else {
            return round($nro, 2);
        }
    }

    /**
     * registro de hoja de cosecha
     * */
    public static function getPlantaAcondicionadoraImportar($planta) {
        if (empty($planta)) {
            return '';
        } else {
            return $planta;
        }
    }

    /**
     * registro de hoja de cosecha
     * */
    public static function getRendimientoEstimadoImportar($rendimiento) {
        if ($rendimiento > 0.00) {
            return 0.00;
        } else {
            return round($rendimiento, 2);
        }
    }

    /**
     * registro de hoja de cosecha
     * */
    public static function getRendimientoCampoImportar($rendimiento) {
        if (empty($rendiento)) {
            return 0.00;
        } else {
            return round($rendimiento, 2);
        }
    }
    /**
     * retorna el rendimiento del campo segun el nro de campo 
     * */
    public function getRendimientoCampoCertificacionByNroCampo($nro) {
        $query = "SELECT rendimiento_campo ";
        $query .= "FROM view_hoja_cosecha ";
        $query .= "WHERE nro_campo = '$nro'";
        
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }else{
            $obj = DBConnector::objeto();
            
            return $obj->rendimiento_campo;
        }
        
    }
    /**
     * rendimiento de campo segun id semilla
     * */
     public static function getRendimientoCampoForDetalleResumen($isemilla){
         $query = "SELECT rendimiento_campo FROM cosecha ";
         $query .= "WHERE nro_campo = $isemilla";
         #echo $query;
         DBConnector::ejecutar($query);
         if(DBConnector::nroError()){
             echo DBConnector::mensaje(),'=',$query;
         }else{
             $obj = DBConnector::objeto();
             #echo (DBConnector::filas());
             if (DBConnector::filas()){
                 return $obj->rendimiento_campo;
             }else{
                 return '0.00';
             } 
         }
     }
    /**
     * registro de hoja de cosecha
     * */
    public static function getRegistro($id) {
        $query = "SELECT nro_campo,fecha_emision,categoria_en_campo,rendimiento_estimado,superficie_parcela,";
        $query .= "rendimiento_campo,nro_cupones,planta_acondicionadora FROM view_datos_cosecha WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }
    /**
     * registro de hoja de cosecha
     * */
    public static function getRegistro2($id) {
        $query = "SELECT fecha_emision,rendimiento_estimado,rendimiento_campo,nro_cupones,planta_acondicionadora ";
        $query .= "FROM cosecha WHERE id_cosecha=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query ::",DBConnector::mensaje();
        }
    }
    /**
     * registro de hoja de cosecha
     * */
    public static function getRegistroFiscal($id) {
        $query = "SELECT cultivo,planta_acondicionadora FROM view_datos_cosecha_fiscal WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * devuelve la semilla neta segun el nro de campo
     * */
    public static function getSemillaNetaByNroCampo($id) {
        $query = "SELECT semilla_neta FROM view_semilla_producida WHERE nro_campo='$id'";

        DBConnector::ejecutar($query);
        if (DBConnector::filas()) {
            $obj = DBConnector::objeto();

            return $obj -> semilla_neta;
        } else {
            return '0.0';
        }
    }

    /**
     * devuelve la semilla neta segun el nro de campo
     * */
    public static function getSuperficieParcelaImportar($superficie) {
        if (empty($superficie)) {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = 0.00";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            return $obj -> id_inscrita;
        } else {
            $query = "SELECT id_inscrita FROM inscrita WHERE superficie = " . round($superficie, 2);
            DBConnector::ejecutar($query);
            if (DBConnector::filas()) {
                #echo DBConnector::mensaje(),'=',$query;
                $obj = DBConnector::objeto();
                return $obj -> id_inscrita;
            } else {
                Inscrita::setInscrita($superficie);

                return DBConnector::lastInsertId();
            }
        }
    }

    /**
     * Extrae los datos para ser insertados en un scrpt sql
     * @param  id_cosecha	mediumint(9)
     * @param  nro_campo	varchar(10)
     * @param  fecha_emision	char(10)
     * @param  categoria_en_campo	varchar(20)
     * @param  rendimiento_estimado	decimal(4,2)
     * @param  superficie_parcela	decimal(4,2)
     * @param  rendimiento_campo	decimal(4,2)
     * @param  nro_cupones	mediumint(9)
     * @param  rango_cupones	varchar(15)
     * @param  planta_acondicionadora	text
     * @param  laboratorio	tinyint(1)
     * @param  estado	tinyint(1)
     * @param  id_superficie	mediumint(8)
     * @param  boolean   laboratorio  determina si pasa por laboratorio o no
     *                   si es 1 va a laboratorio -  si es 0 no va a laboratorio
     */
    public function getHojaCosechaSQL() {
        $this -> getHojaCosecha();
        if (DBConnector::filas() == 0)
            $query = '';
        else {
            $query = 'INSERT INTO cosecha (id_cosecha,nro_campo,fecha_emision,_categoria_en_campo,rendimiento_estimado,superficie_parcela,rendimiento_campo,nro_cupones,rango_cupones,planta_acondicionadora,alaboratorio,estado,id_superficie) VALUES';
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_cosecha . ",";
                $query .= $row -> nro_campo . ",'";
                $query .= $row -> fecha_emision . "'," . $row -> categoria_en_campo . ",";
                $query .= $row -> rendimiento_estimado . "," . $row -> superficie_parcela . ",";
                $query .= $row -> rendimiento_campo . "," . $row -> nro_cupones . ",'";
                $query .= $row -> rango_cupones . "','" . $row -> planta_acondicionadora . "',";
                $query .= $row -> alaboratorio . "," . $row -> estado . "," . $row -> id_superficie . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

} // END
?>