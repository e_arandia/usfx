<?php
/**
 * Generacion
 * @author iniaf
 * @copyright Edwin W. Arandia Zeballos
 * @version 2011
 * @access public
 */
class Generacion extends Categoria {

    /** colocar Nombre de una generacion*/
    public function setGeneracion($generacion) {
        $this -> generacion = $generacion;
    }

    /**
     * lista d todas las generaciones
     * */
    public function getGeneraciones() {
        $query = "SELECT * FROM generacion";

        DBConnector::ejecutar($query);
    }

    /**
     * Obtener categoria,generacionnro_campo,cultivo por id de generacion
     * @param integer $id  numero de generacion
     * */
    public function getGeneracionById($id) {
        $query = "SELECT * FROM view_lst_categoria_generacion  WHERE id_generacion=$id";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        #echo $query.'='.DBConnector::mensaje();
    }

    /** obtiene la generacion segun la categoria  */
    function getGeneracionesByIdCategoria($categoria) {
        $query = "SELECT id_generacion, generacion FROM view_lst_categoria_generacion
                  WHERE id_categoria= $categoria";
        // echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * Obtener categoria,generacionnro_campo,cultivo por id de generacion
     * @param integer $id  numero de generacion
     * */
    public function getGeneracionByCategoria($categoria) {
        $query = "SELECT * FROM view_lst_categoria_generacion WHERE categoria LIKE '$categoria%'";
        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * Obtener la generacion por el nombre de categoria
     * */
    public function getNameGeneracionByCategoria($categoria, $nombre) {
        $query = "SELECT generacion 
                  FROM view_lst_categoria_generacion WHERE id_categoria=$categoria AND generacion = '$nombre'";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * nombre de generacion segun el id de la generacion
     * */
    public function getInicialGeneracionByIdGeneracion($id) {
        $query = "SELECT inicial FROM view_lst_categoria_generacion WHERE id_generacion=$id";
#echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> inicial;
    }
    /**
     * nombre de generacion segun el id de la generacion
     * */
    public function getNombreGeneracionByIdGeneracion($id) {
        $query = "SELECT generacion FROM view_lst_categoria_generacion WHERE id_generacion=$id";
#echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> generacion;
    }
    /**
     * devuelve la inicial de la generacion 
     * */
     public static function getInicialGeneracion($id){
         $query = "SELECT inicial FROM view_inicial_generacion WHERE id_generacion=$id";
         
         DBConnector::ejecutar($query);
         
         $obj=DBConnector::objeto();
         
         return $obj->inicial;
     }
    /**devuelve el id de la generacion
     * @param string $generacion   nombre de la generacion
     * */
    public function getIdGeneracionByName($generacion) {
        $query = "SELECT id_generacion FROM view_lst_categoria_generacion  WHERE inicial ='$generacion'";

        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> id_generacion;
    }

    /** Obtenemos todas las generaciones  */
    function listarGeneraciones() {
        $query = " SELECT * FROM generacion ";

        DBConnector::ejecutar($query);
    }

    /**Actualizar generacion segun id*/

    public function actualizarGeneracion($id, $nuevoNombre) {
        $query = "UPDATE generacion SET nombre_generacion='$nuevoNombre' WHERE id_generacion=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * Eliminar generacion segun id
     * @param integer $id
     *
     * @return void
     * */
    public function deleteGeneracion($idGeneracion) {
        $query = "DELETE FROM generacion WHERE id_generacion=$idGeneracion";

        return DBConnector::ejecutar($query);
    }

    /**
     * Generaciones que pertenecen a una categoria
     * */
    public function countGeneracion($id) {
        $query = "SELECT count(generacion) AS total FROM generacion WHERE id_categoria=$id";
        //echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * Agrega una generacion a una categoria determinada
     * @param int numero de identificacion de categoria
     *
     * @return void
     * */
    public function newGeneracion($generacion, $idCategoria) {
        $query = "INSERT INTO generacion (nombre_generacion,id_categoria)
                 VALUES ('$generacion',$idCategoria.)";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     */
    public function getGeneracionesSQL() {
        $this -> getGeneraciones();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            $query = 'INSERT INTO generacion (id_generacion,nombre_generacion,id_categoria) VALUES';
            while ($row = DBConnector::objeto()) {
                $query .= "(";
                $query .= $row -> id_generacion . ",'" . $row -> nombre_generacion . "',";
                $query .= $row -> id_categoria . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>