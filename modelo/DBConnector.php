<?php
class DBConnector {
    /**
     * instancia de conexion a la base de datos*/
    protected static $conn;
    /**
     * Sentencia sql*/
    protected static $sql;
    /**
     * var
     * */
    protected static $stmt;

    /**
     * Ultimo id insertado en una consulta*/
    protected static $insertID;
    /**
     * numero de error de una consulta*/
    protected static $nro_error;
    /**
     * descripcion de error de una consulta*/
    protected static $error;
    /**
     * resultado de la consulta
     * */
    private static $temp;
    protected static $resultado;
    /**
     * Filas Afectadas (SELECT,INSERT,UPDATE,DELETE)
     * */
    protected static $row;

    protected static function conectar() {
        self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        #self::$conn = ("SET NAMES 'utf8'");
    }

    /**
     * ejecucion de una consulta
     * */
    public static function ejecutar($sql) {

        try {
            self::$sql = $sql;
            //conexion a la base de datos
            self::conectar();
            //ejecucion de la consulta
            self::$resultado = self::$conn -> query(self::$sql);
            #echo self::$sql,'=',self::mensaje();
            if (!self::$conn -> errno) {
                //si la consulta es un insert obtenemos el id
                if (substr($sql, 0, 1) == 'I')
                    self::$insertID = self::$conn -> insert_id;
                self::$row = self::$conn -> affected_rows;
                self::$error = self::$conn -> error;
                self::$nro_error = self::$conn -> errno;
            } else {
                self::$error = self::$conn -> error;
                self::$nro_error = self::$conn -> errno;
                
            }

            self::$conn -> close();
        } catch (Exception $e) {
            echo $e -> getMessage() . '=' . $this -> mensaje();
        }
    }

    /**
     * resultado de una consulta en un array asociativo
     * */
    public static function asociativo() {
        return self::$resultado -> fetch_assoc();
    }

    /**
     * resultado de una consulta como objetos
     * */
    public static function objeto() {
        return self::$resultado -> fetch_object();
    }

    /**
     * id de ultimo insert en una tabla
     * */
    public static function lastInsertId() {
        return self::$insertID;
    }

    /**
     * descripcion del error
     * */
    public static function error() {
        return self::$error;
    }

    /**
     * numero de error
     * */
    public static function nroError() {
        return self::$nro_error;
    }

    /**
     * nro filas de la consulta
     * */
    public static function filas() {
        return self::$row;
    }

    /**
     * numero de mensaje y descripcion del mismo
     * */
    public static function mensaje() {
        echo 'Error (' . self::$nro_error . ') ' . self::$error;
    }

}
?>
