<?php
class Realiza {

    public static function superficie_inspeccion($isuperficie, $inspeccion) {
        $query = "INSERT INTO superficie_inspeccion (id_superficie,id_inspeccion) VALUES ($isuperficie,$inspeccion)";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    private function getRealiza() {
        $query = "SELECT * FROM superficie_inspeccion";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de la inspeccion segun el id de la superficie
     *
     * @param integer $id id de la superficie
     *
     * @return bool  TRUE si encuentra id de la inspeccion en la tabla de relacion caso contrario FALSE
     * */
    public static function getInspeccionId($id) {
        $query = "SELECT id_inspeccion FROM superficie_inspeccion WHERE id_superficie = $id";

        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getRealizaSQL() {
        $query = 'INSERT INTO superficie_inspeccion (id_superficie,id_inspeccion) VALUES ';
        $this -> getRealiza();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_superficie . "," . $row -> id_inspeccion . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>