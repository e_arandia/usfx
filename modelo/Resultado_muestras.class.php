<?php
class Resultado_muestras {

    /**
     * resultado pureza
     * */
    public static function getPurezaImportar($pureza) {
        if ($pureza > 0 && $pureza != '') {
            $pureza = str_replace(",", ".", $pureza);
            $pureza_temp = explode("/", $pureza);
            if (count($pureza_temp)>1){
                return $pureza_temp;
            }else{
                return round($pureza, 2);
            }
        } else {
            return 0.00;
        }
    }

    /**
     * resultado germinacion
     * */
    public static function getGerminacionImportar($germinacion) {
        if ($germinacion > 0 && $germinacion != '') {
            $germinacion = str_replace(",", ".", $germinacion);
            $germinacion_temp = explode("/", $germinacion);
            if (count($germinacion_temp)>1){
                return $germinacion_temp;
            }else{
                return round($germinacion, 2);
            }
        } else {
            return 0.00;
        }
    }

    /**
     * fecha de actualizacion
     * para fiscalizacion
     * */
    public static function getFechaActualizacionImportar($fecha) {
        if ($fecha != '') {
            return Funciones::cambiar_tipo_mysql($fecha);
        } else {
            return '';
        }
    }

    /**
     * resultado germinacion
     * */
    public static function getActualizacionGerminacionImportar($germinacion) {
        if ($germinacion > 0 && $germinacion != '') {
            $germinacion = str_replace(",", ".", $germinacion);
            return round($germinacion, 2);
        } else {
            return 0.00;
        }
    }

    /**
     * resultado humedad
     * */
    public static function getHumedadImportar($humedad) {
        if ($humedad > 0 && $humedad != '') {
            $humedad = str_replace(",", ".", $humedad);
            $humedad_temp = explode("/", $humedad);
            if (count($humedad_temp)>1){
                return $humedad_temp;
            }else{
                return round($humedad, 2);
            }
        } else {
            return 0.00;
        }
    }

    public static function mostrarPorcentajeResultadoAnalisis($porcentaje) {
        if (is_array($porcentaje)) {
            $new_porcentaje = array();
            foreach ($porcentaje as $key => $value) {
                array_push($new_porcentaje, number_format(str_replace(',', '.', $value), 2, '.', ','));
            }
            return $new_porcentaje;
        } else {
            return number_format(str_replace(',', '.', $porcentaje), 2, '.', ',');
        }
    }

    /**
     * devuelve el lote de importar BD
     * */
    public static function getLoteImportar($lote) {
        if (empty($lote)) {
            return '';
        } else {
            return $lote;
        }
    }

    /**
     * devuelve las observaciones de un analisis
     * */
    public static function getObservacionActualizacionImportar($observacion) {
        if (empty($observacion)) {
            return '';
        } else {
            return substr($observacion, strlen('actualizacion') + 1);
        }
    }

    /**
     * devuelve las observaciones de un analisis
     * */
    public static function getObservacionImportar($observacion) {
        if (empty($observacion)) {
            return '';
        } else {
            return $observacion;
        }
    }

    /**
     * lista todos los resultados de muestras
     * */
    public function getResultadoMuestras() {
        $query = "SELECT * FROM resultado_muestras";

        DBConnector::ejecutar($query);
        
        if(DBConnector::nroError()){
            echo DBConnector::mensaje(),'=',$query;
        }
    }
    /**
     * lista todos los resultados de muestras
     * */
    public static function getResultadoMuestrasByIdMuestra($id) {
        $query = "SELECT * FROM resultado_muestras ";
        $query .= "WHERE id_muestra = $id";
#echo $query;
        DBConnector::ejecutar($query);
        
        if(DBConnector::nroError()){
            echo "ERROR ".__METHOD__."::",DBConnector::mensaje(),'=',$query;
        }
    }
    /**
     * muestras rechazadas
     * @param $estado int  1:aprobada 0:rechazada
     * */
    public function getMuestrasRechazadas() {
        $query = "SELECT COUNT(*) AS rechazadas FROM resultado_muestras WHERE estado = 0";

        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> rechazadas;
    }
    public static function getResultadoLaboratorio($id){
        $query = "SELECT nro_analisis, fecha_recepcion,cultivo,nro_campo,origen,fecha_resultado,lote,pureza,germinacion,humedad,observacion,kgBolsa,calibre ";
        $query .= "FROM view_resultado_muestra_certificada ";
        $query .= "WHERE nro_analisis = $id";
               
        DBConnector::ejecutar($query);
    }
    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getResultadoMuestrasSQL() {
        $query = 'INSERT INTO resultado_muestras (id_resultado,fecha,lote,pureza,germinacion,humedad,observacion) VALUES ';
        $this -> getResultadoMuestras();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_resultado . ",'";
                $query .= $row -> fecha . "','" . $row -> lote . "',";
                $query .= $row -> pureza . "," . $row -> germinacion . ",";
                $query .= $row -> humedad . ",'" . $row -> observacion . "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

    /**
     * ingresa los resultados de una muestra
     * */                                          //($nro_analisis, $fecha_resultado, $lote[0], $pureza, $germinacion, $humedad, $observaciones, $iMuestra)
    public static function setResultadoMuestraImportar($resultado, $lote, $pureza, $germinacion, $humedad, $observacion, $idmuestra) {
        
        if (is_array($pureza)){
            foreach ($pureza as $i => $pureza_valor) {
                if ($humedad[$i]){
                    $query = "INSERT INTO resultado_muestras (fecha,lote,pureza,germinacion,humedad,observacion,id_muestra) VALUES (";
                    $humedad = empty($humedad[$i])?0:$humedad[$i];
                    $germinacion = empty($germinacion[$i])?1:$germinacion[$i];
                    $pureza_valor = ($pureza_valor!='')?1:$pureza_valor;
                    $query .= "'$resultado','$lote',".$pureza_valor.",".$germinacion.",".$humedad.",'$observacion',$idmuestra)";
                }else{
                    $germinacion = empty($germinacion[$i])?0:$germinacion[$i];
                    $pureza_valor = ($pureza_valor!='')?1:$pureza_valor;
                    $query = "INSERT INTO resultado_muestras (fecha,lote,pureza,germinacion,observacion,id_muestra) VALUES (";
                    $query .= "'$resultado','$lote',".$pureza_valor.",".$germinacion.",'$observacion',$idmuestra)";
                }
                
                DBConnector::ejecutar($query);
            }
            
        }else{
            $query = "INSERT INTO resultado_muestras (fecha,lote,pureza,germinacion,humedad,observacion,id_muestra) VALUES (";
            $query .= "'$resultado','$lote',$pureza,$germinacion,$humedad,'$observacion',$idmuestra)";
            DBConnector::ejecutar($query);    
        }
        
        if (DBConnector::nroError()) {            
            echo DBConnector::mensaje(), $query;
            exit ;
        }
    }

    /**
     * ingresa la actualizacion de germinacion de una muestra
     * */
    public static function setActualizacionResultadoMuestraImportar($fecha, $germinacion, $observacion, $id_muestra) {
        $query = "INSERT INTO resultado_muestras (fecha,germinacion,observacion,id_muestra) VALUES (";
        $query .= "'$fecha',$germinacion,'$observacion',$id_muestra)";
        

        DBConnector::ejecutar($query);
    }

}
?>