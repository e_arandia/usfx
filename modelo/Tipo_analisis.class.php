<?php
class Tipo_analisis{
    /**
     * lista todos los tipos de analisis
     * */
     public function getTipoAnalisis(){
         $query = "SELECT * FROM tipo_analisis";
         
         DBConnector::ejecutar($query);
     }
     
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getTipoAnalisisSQL() {
        $query = 'INSERT INTO tipo_analisis (id_analisis,nombre_analisis) VALUES ';
        $this -> getTipoAnalisis();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_analisis . ",'" . $row -> id_nombre_analisis . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>