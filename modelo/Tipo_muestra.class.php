<?php
class Tipo_muestra{
    /**
     * lista todos los tipos de analisis
     * */
     public function getTipoMuestra(){
         $query = "SELECT * FROM tipo_muestra";
         
         DBConnector::ejecutar($query);
     }
     
     /**
     * formatea los datos para su uso en un script sql
     * */
    public function getTipoMuestraSQL() {
        $query = 'INSERT INTO tipo_muestra (id_tipo_muestra,nombre_tipo,descripcion) VALUES ';
        $this -> getTipoMuestra();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_tipo_muestra . ",'";
                $query .= $row->nombre_tipo."','".$row->descripcion. "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>