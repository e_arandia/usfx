<?php
/**
 *
 */
class CRUD {
    var $create;
    var $read;
    var $update;
    var $delete;
    /**
     * constructor de la clase
     * **/
    public function __construct($create = 1, $read = 1, $update = 1, $delete = 0) {
        $this -> create = $create;
        $this -> read = $read;
        $this -> update = $update;
        $this -> delete = $delete;
    }

    /**
     * inserta una nueva area
     *
     *@param $nombre string   nombre del area
     * */
    public function setEstado() {
        $query = "INERT INTO crud(crear,leer,actualizar,eliminar) VALUES ($this->create,$this->read,$this->update,$this->delete)";

        DBConnector::ejecutar($query);

    }

    /**
     * devuelve las operaciones posibles segun el id
     * */
    public static function getCrudById($id) {
        $query = "SELECT CONCAT(crear,leer,actualizar,eliminar) AS crud FROM crud WHERE id_crud = $id";
        DBConnector::ejecutar($query);
        $obj = DBConnector::asociativo();

        return $obj['crud'];
    }
    /**
     * devuelve el id de los permisos
     * 
     * */
     public static function getIdCrudById($crud){
         $query = "SELECT id_crud FROM view_usuarios WHERE crud = '$crud'";
         #echo $query;
         DBConnector::ejecutar($query);
         
         $obj = DBConnector::objeto();
         
         return $obj->id_crud;
     }
    /**
     * lista de todos los tipos de permiso en el sistema
     * */
    public function getCrud() {
        $query = "SELECT * FROM crud";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getCrudSQL() {
        $query = 'INSERT INTO crud (id_crud,crear,leer,actualizar,eliminar) VALUES ';
        $this -> getCrud();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_crud .",";
                $query .= $row -> crear.",".$row->leer.",".
                $query .= $row->actualizar.",".$row->eliminar . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>