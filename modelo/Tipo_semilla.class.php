<?php
class Tipo_semilla{
    /**
     * lista todos los tipos de semilla
     * */
     public static function getTipoSemilla(){
         $query = "SELECT * FROM view_tipo_semilla";
         
         DBConnector::ejecutar($query);
     }
     
     /**
      * lista de todos los tipos(nombres) de semilla disponibles
      * */
      public static function getTipoSemillaNombre(){
          $query = "SELECT id,tipo FROM view_tipo_semilla";
          #echo $query;
          DBConnector::ejecutar($query);
      }
     /**
      * devuelve el nombre del tipo de semilla segun el id
      * */
      public static function getNombreTipoSemillaById($id){
          $query = "SELECT tipo FROM view_tipo_semilla WHERE id=$id";
          #echo $query;
          DBConnector::ejecutar($query);
          
          $obj = DBConnector::objeto();
          if(DBConnector::filas())
            return $obj->tipo;
          else {
              return '-';
          }
      }
     /**
     * formatea los datos para su uso en un script sql
     * */
    public static function getTipoSemillaSQL() {
        $query = 'INSERT INTO tipo_semilla (id_tipo_semilla,nombre_tipo,descripcion) VALUES ';
        $this -> getTiposemilla();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_tipo_semilla . ",'";
                $query .= $row->nombre_tipo."','".$row->descripcion. "'),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>