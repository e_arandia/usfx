<?php
class Variedad {
    /**
     * Agregar nuevo cultivo
     * @param string $variedad nombre de la variedad
     * @param integer $id_cultivo  identificador de cultivo
     *
     * @return void
     * */
    private static function setVariedad($variedad, $id_cultivo) {
        $query = "INSERT INTO variedad (nombre_variedad,id_cultivo) VALUES ('$variedad',$id_cultivo)";

        DBConnector::ejecutar($query);
    }

    /**
     * nombre de variedad segun el id
     * */
    public static function getNombreVariedadById($id) {
        $query = "SELECT variedad FROM view_cultivo_variedad WHERE id_variedad=$id";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> variedad;

    }

    /**
     * Listar todas las variedades de un cultivo
     * @param integer $cultivo id del cutlivo
     *
     * @return devuelve todas las variedades del cultivo
     * */
    public function getVariedades($cultivo) {
        $query = "SELECT DISTINCT(nombre_variedad) ";
        $query .= "FROM variedad WHERE id_cultivo = $cultivo ORDER BY nombre_variedad ASC";
        #echo $query;
        DBConnector::ejecutar($query);
        
        if (DBConnector::nroError()){
            echo DBConnector::mensaje().'=',$query;
        }
    }

    /**
     * verifica si existe la variedad
     *
     * @return Devuelve FALSE si no existe caso contrario TRUE
     * */
    public static function checkVariedad($variedad) {
        $query = "SELECT id_variedad FROM variedad WHERE nombre_variedad = '$variedad'";
        #echo $query;
        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            return TRUE;
        } else
            return FALSE;
    }

    /**
     * verifica la existencia de una variedad perteneciente a un cultivo
     * @param string $variedad  nombre de la variedad
     * @param integer $id_cultivo   identificador del cultivo
     *
     * @return id de la variedad
     * */
    public function setCheckVariedad($variedad, $id_cultivo) {
        if ($this -> checkVariedad($variedad)) {
            return $this -> getIdVariedad($variedad);
        } else {
            $this -> setVariedad($variedad, $id_cultivo);
            return DBConnector::lastInsertId();
        }
    }

    /**
     * devuleve la lista de variedades registradas
     * */
    public function getListVariedades() {
        $query = "SELECT * FROM variedades";

        DBConnector::ejecutar($query);
    }

    /**
     * Verifica si existe el cultivo
     * */
    public function getNombreVariedad($id_variedad) {
        $query = "SELECT nombre_variedad FROM variedad WHERE id_variedad = $id_variedad";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve la generacion de un cultivo
     * @param string cultivo  nombre del cultivo
     * */
    public static function getNombreVariedadByCultivo($cultivo) {
        $query = "SELECT DISTINCT(variedad) AS variedad FROM view_cultivo_variedad WHERE cultivo LIKE '$cultivo'";
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * devuelve el id de la variedad
     * @param $variedad string nombre de la variedad
     *
     * @return id de la variedad
     * */
    public static function getIdVariedad($variedad) {
        $query = "SELECT id_variedad FROM variedad where nombre_variedad LIKE '$variedad%'";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_variedad;
    }

    /**
     * devuelve el id de la variedad para importar
     * @param $variedad string nombre de la variedad
     *
     * @return id de la variedad
     * */
    public static function getIdVariedadImportar($variedad, $icultivo) {
        if ($variedad){
            $variedad2 = Funciones::search_tilde($variedad);
            //buscar ' en variedad
            $especial = stripos($variedad, "'");
            //si ' esta en variedad
            if ($especial !== FALSE) {
                $temp = explode("'", $variedad);
                $query = "SELECT id_variedad FROM variedad WHERE nombre_variedad LIKE '" . $temp[0] . "%'";
                 
            } else {
                $query = "SELECT id_variedad FROM variedad WHERE nombre_variedad LIKE '$variedad2'";            
            }
            DBConnector::ejecutar($query);
            if (DBConnector::filas()) {
                $obj = DBConnector::objeto();
                return $obj -> id_variedad;
            } else {
                $query = "INSERT INTO variedad (nombre_variedad,id_cultivo) VALUES (\"".utf8_decode(addslashes($variedad))."\",$icultivo)";
                #echo $query.'=';
                DBConnector::ejecutar($query);
                return DBConnector::lastInsertId();
            }            
        }else{
            $query = "SELECT id_variedad FROM variedad WHERE nombre_variedad = 'desconocida'";
            DBConnector::ejecutar($query);
            $obj = DBConnector::objeto();
            return $obj->id_variedad;
        }
    }

    /**
     * Eliminar cultivo
     * */
    public function delCultivo($id) {
        $query = "DELETE FROM costos WHERE id_costos=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * Modificar cultivo
     * */
    public function updCultivo($id, $nInscrip, $nInspeccion, $nAnalisis) {
        $query = "UPDATE costos SET inscripcion=$nInscrip,
                                    inspeccion=$nInspeccion,
                                    analisis_laboratorio=$nAnalisis
                  WHERE id_costos=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * Precios segun cultivo
     * */
    public function getPrecios($id) {
        $query = "SELECT inscripcion,inspeccion,analisis_etiqueta
         FROM view_costos_cultivo
         WHERE id=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getVariedadesSQL() {
        $query = 'INSERT INTO variedad (id_variedad,nombre_variedad,id_cultivo) VALUES ';
        $this -> getCampanhas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_variedad . ",'" . $row -> nombre_variedad . "'," . $row -> id_cultivo . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }

}
?>