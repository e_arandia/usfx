<?php
/**
 *
 */
class Area {
    var $nombre;
    var $nivel;

    /**
     * Constructor de la clase con los valores iniciales
     *
     * @param string $nombre nombre de area
     * @param int    $nivel  asignacion de nivel
     *
     * */
    public function __contruct($nombre, $nivel) {
        $this -> nombre = $nombre;
        $this -> nivel = $nivel;
    }

    /**
     * inserta una nueva area
     *
     *@param $nombre string   nombre del area
     * */
    public function insertArea() {
        $query = "INERT INTO area(nombre_area,nivel) VALUES ('ucfirst($this->nombre)',$this->nivel)";

        DBConnector::ejecutar($query);
    }

    /***/
    public function delArea($id) {
        $query = "DELETE FROM area WHERE id_area = $area";

        DBConnector::ejecutar($query);
    }

    public function updArea($new_nombre, $id) {
        $query = "UPDATE area SET nombre_area = $nombre_area WHERE id_area = $id";

        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de un area segun el nombre
     * */
    public static function getIdAreaByNombre($nombre) {
        $query = "SELECT id_area FROM area WHERE nombre_area LIKE '$nombre'";
        //echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();
        
        return $obj->id_area;
    }

    /**
     * devuelve el id de un
     * */
    public static function getNombreAreaById($id) {
        $query = "SELECT nombre_area FROM area WHERE id_area = $id";

        DBConnector::ejecutar($query);
    }

    /**
     * lista todas las areas en el sistema
     * */
    public static function getAreas() {
        $query = "SELECT id_area,nombre_area,nivel FROM area";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * */
    public function getAreaSQL() {
        $query = 'INSERT INTO area (id_area,nombre_area,nivel) VALUES ';
        $this -> getAreas();
        if (DBConnector::filas() == 0) {
            $query = '';
        } else {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_area . ",'" . $row -> id_nombre_area . "'," . $row -> nivel . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        return $query;
    }
}
?>