<?php
/**
 * modelo para plantines
 */
class Plantines {
    /**
     * datos de la semilla para plantines
     * @param array  $acondicionamiento   contiene todos loa datos de acondicionamiento
     * */
    public function insertSemillaPlantines(array $plantines){
        $query = "INSERT INTO semilla (nro_campo,cultivo,variedad,plantin,fecha_siembra) VALUES (";
        $query .= $plantines['nrocampo'].",".$plantines['icultivo'].",".$plantines['ivariedad'].",1,'".date('Y-m-d')."')";
        DBConnector::ejecutar($query);
        #echo DBConnector::lastInsertId();
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
    }
    /**
     * 
     * inspecciones  de plantines
     * @param array  $acondicionamiento   contiene todos loa datos de acondicionamiento
     * */
    public function insertPlantines(array $plantines){
        $query = "INSERT INTO plantines (inspeccion,observacion,tipo,id_solicitud) VALUES (";
        $query .= "'".$plantines['inspeccion']."','".$plantines['observacion']."',";
        $query .= "'".$plantines['tipo']."',".$plantines['isolicitud'].")";
        
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
    }
    /**
     * devuelve el acondicionamiento segun el id de solicitud
     * */
     public function getPlantinesByIdSemilla($id){
         $query = "SELECT * FROM view_plantines ";
         $query .= "WHERE id_semilla = $id";
         
         DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
     }
             /**semillas de productores por bloque*/
    public function getPlantinesByBlock($area, $inicio = 0, $nroReg = 15) {
        $query = "SELECT * FROM view_plantines";
        if ($area != 10)
            $query .= " WHERE id_sistema=$area AND gestion = '" . date('Y') . "'";
        $query .= " ORDER BY id_semilla DESC LIMIT $inicio,$nroReg";
        #echo $query;
        return $query;
    }
     /**
      * eliminar acondicionamiento segun id_plantines o id_solicitud
      * */
      public function deletePlantines($id,$tipo='plantines'){
          if ($acondicionamiento == 'plantines'){
              $query = "DELETE FROM acondicionamiento WHERE id_acondicionamiento = $id";
          }else{
              $query = "DELETE FROM acondicionamiento WHERE id_solicitud = $id";
          }
          DBConnector::ejecutar($query);
         if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
      }
}
?>