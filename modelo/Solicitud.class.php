<?php
/**
 * Responsable de ejecutar los metodos necesarios de una solicitud
 * @package  model
 * @author  Edwin W. Arandia Zeballos
 *
 */
class Solicitud {

    /**
     * Inserta nuevos valores de una solicitud.
     * si la semillera no existe primero inserta esta para
     * despues obtener el id  que le fue asignado
     * @param int      $nroSolicitud      numero de la solicitud
     * @param int      $id_sistema           id del sistema
     * @param int      $id_municipio         id del municipio
     * @param string   $comunidad         nombre de la comunidad
     * @param string   $nombre_semillera  nombre de la semillera
     * @param string   $id_semillerista   nombre y apellido de semillerista
     * @param string   $fecha             fecha de solicitud
     * @param int      $nro_parcelas      numero de parcelas (por defecto es 1)
     */
    public static function insertSolicitud($id_semillera, $id_sistema, $id_semillerista, $id_municipio, $comunidad, $fecha, $nro_parcelas = 1) {
        $query = "INSERT INTO solicitud (id_semillera,id_sistema,id_semillerista, id_municipio, comunidad,fecha,nro_parcela) VALUES ";
        $query .= "($id_semillera,$id_sistema,$id_semillerista,$id_municipio,'$comunidad','$fecha',$nro_parcelas)";
        #echo $query;exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo "ERROR al registrar la solicitud :: $query :: ",DBConnector::mensaje();
        }
    }

    /**
     * actualiza una solicitud
     * */
    public static function updateSolicitud($id, $nroSolicitud, $sistema, $departamento, $provincia, $municipio, $comunidad, $semillera, $nombre, $apellido, $fecha, $nro_parcelas) {

        $query = "UPDATE solicitud SET nro_solicitud ='$nroSolicitud',id_sistema='$sistema',id_semillera=$semillera,
		fecha='$fecha',nombre='$nombre',apellido='$apellido',
		comunidad='$comunidad',id_municipio=$municipio,id_provincia=$provincia,departamento='$departamento',nro_parcelas=$nro_parcelas
		WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);

    }

    /**
     * Actualiza la comunidad y localidad de una solicitud segun su id
     * @param   string    $comunidad    nombre de la comunidad
     * @param   string    $localidad    nombre de la localidad
     * @param   integer   $id           numero de identificacion
     *
     * @return void
     * */
    public static function updSolicitudById($comunidad, $id) {
        $query = "UPDATE solicitud SET comunidad='$comunidad'
        WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * elimina una solicitud
     *
     * @param integer  $id  numero de id
     * */
    public function deleteSolicitud($id) {
        Tiene::deleteTieneById($id,'solicitud');
        $query = "DELETE FROM solicitud WHERE id_solicitud=$id";
        echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualizar valor de laboratorio
     * 0 falta prueba de laboratorio
     * 1 aprobado
     * 2 rechazado
     * */
    /* public function setCertificacion($nombre, $apellido, $laboratorio) {
     // crear funcion, proc alm o trigger q actualice la tabla solicitud
     }
     */

    /**
     * lista todas las semilleras
     * */

    /**
     * verifica si el campo apellido esta vacio
     * */
    private function getApellidoSemillerista($apellido, $tabla, $semillera = '') {
        switch ($tabla) {
            case 'semilla' :
                $query = "SELECT apellido FROM solicitud WHERE apellido LIKE '%$apellido%' AND semillera='$semillera' AND laboratorio = 0";
                break;
            default :
                $query = "SELECT apellido FROM solicitud WHERE apellido LIKE '%$apellido%' AND semillera='$semillera'";
                break;
        }

        DBConnector::ejecutar($query);
        #echo $query;
        //no tiene apellido devuelve falso sino verdad
        if (DBConnector::filas())
            return TRUE;
        else {
            return FALSE;
        }
    }

    /**
     * ('solicitud',$nombre,$apellido,$semillera,$gestion,$area);
     * @param string $etapa
     * @param string $cultivo
     * @param string $nombre
     * @param string $semillera
     * @param string $anio
     * @param string $sistema     por defecto: admin
     * */
    public function buscadorSemilleras($etapa, $cultivo, $nombre, $semillera, $anio, $sistema = 'admin') {

        switch ($sistema) {
            case 1 :
                $query = "SELECT DISTINCT(semillera),estado,id_solicitud FROM view_solicitudes ";
                $query .= "WHERE semillera LIKE '$semillera' AND id_sistema LIKE '$sistema' AND semillerista LIKE '$nombre%' AND fecha LIKE '$anio-%' ";
                $query .= "ORDER BY semillera ASC";
                break;
            case 2 :
                $query = "SELECT DISTINCT(semillera),estado,id_solicitud FROM view_solicitudes ";
                $query .= "WHERE semillera LIKE '$semillera' AND id_sistema LIKE '$sistema' AND  semillerista LIKE '$nombre%' AND fecha LIKE '$anio-%' ";
                $query .= "ORDER BY semillera ASC";
                break;
        }

        #echo $query; exit ;
        DBConnector::ejecutar($query);
    }

    /**
     * lista todas las provincias
     * */
    public function getProvincias() {
        $query = "SELECT id_provincia,nombre_provincia ";
        $query .= "FROM view_lst_provincias ORDER BY nombre_provincia";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista municipios que coinciden
     * */
    public function getMunicipios($id) {
        $query = "SELECT id_municipio,nombre_municipio ";
        $query .= "FROM view_lst_municipio_provincia ";
        $query .= "WHERE id_provincia = $id ORDER BY nombre_municipio";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * Lista todos los nombres y apellidos registrados que coinciden
     * @param string @q nombre de busqueda
     *
     * @return lista de nombres coincidentes
     * */
    public static function getNombres($q) {
        $query = "SELECT DISTINCT(nombre) FROM view_nombre WHERE nombre LIKE '$q%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el nombre segun el id de solicitud
     * @param integer $id id de la solicitud
     *
     * @return nombre de semillerista
     * */
    public static function getNombreByIdSolcitud($id) {
        $query = "SELECT nombre FROM view_solicitudes WHERE id_solicitud = $id";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            $obj = DBConnector::objeto();
            return $obj -> nombre;
        }
    }

    /**
     * lista todos los  apellidos registrados
     * @param string $q  iniciales de apellido
     * @return lista de apellidos coincidentes
     * */
    public static function getApellidos($q) {
        $query = "SELECT DISTINCT(apellido) FROM view_nombre WHERE apellido LIKE '$q%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el apellido segun el id de solicitud
     * @param integer $id id de la solicitud
     *
     * @return apellido de semillerista
     * */
    public static function getApellidoByIdSolcitud($id) {
        $query = "SELECT apellido FROM view_solicitudes WHERE id_solicitud = $id";

        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();
        if (DBConnector::filas())
            return $obj -> apellido;
        else {
            return '-';
        }
    }

    /**
     * devuelve el id de la semillera segun el nro de solicitud y el sistema
     * @param integer $nrosolicitud  nro de la solicitud
     * @param integer $id_sistema  nro de sistema
     *
     * @return id_semillera
     * */
    function getNoSolicitudBy2($nrosolicitud, $sistem) {
        $query = "SELECT MAX(id_semillera) AS id_semillera FROM view_id_semillera WHERE nro_solicitud LIKE '$nrosolicitud' AND id_sistema = $sistem";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista las solicitudes de una etapa
     * @param string $etapa nombre de la etapa
     * */
    public function getNoSolicitudes($etapa, $sistema) {
        switch ($etapa) {
            case 'semilla' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=0)";
                #echo $query;
                break;
            case 'superficie' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=1)";
                break;
            case 'inspeccion' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud>=4 AND estado_solicitud <7)";
                #echo $query;
                break;
            case 'cosecha' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND estado_solicitud IN (6,7)";
                #echo $query;
                break;
            case 'laboratorio' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=6 AND estado_cosecha=0)";
                break;
            case 'producida' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=8 OR estado_solicitud=9 OR estado_solicitud=10)";
                #echo $query;
                break;
            case 'cuenta' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "estado_solicitud=10";

                break;
        }
        $query .= " ORDER BY nro_solicitud DESC";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista las solicitudes de una etapa
     * @param string $etapa nombre de la etapa
     * */
    public function getNoSolicitudesFiscal($etapa, $sistema) {
        switch ($etapa) {
            case 'semilla' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=0)";
                #echo $query;
                break;
            case 'cosecha' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND estado_solicitud = 1";
                echo $query;
                break;
            case 'laboratorio' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=6 AND estado_cosecha=0)";
                break;
            case 'producida' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "(estado_solicitud=8 OR estado_solicitud=9)";
                #echo $query;
                break;
            case 'cuenta' :
                $query = "SELECT DISTINCT(nro_solicitud) FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND ";
                $query .= "estado_solicitud=10";

                break;
        }
        $query .= " ORDER BY nro_solicitud DESC";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de area en el sistema (certificacion,fiscalizacion u otro)
     * @param string $val nombre del sistema
     *
     * @return id_area
     * **/
    public function getIdSistema($val) {
        return Area::getIdAreaByNombre($val);
    }

    /**
     * nro de solicitud segun semillera segun id sistema
     * @param $semillera string nombre de la semillera
     *
     * @return devuelve el nro de solicitud de la semillera
     *
     * **/
    public function getNroSolicitudBySemillera($semillera, $sistema) {
        $query = "SELECT nro_solicitud FROM view_nroSolicitud_semillera WHERE semillera = $semillera AND sistema LIKE '$sistema%'";
        //echo $query;exit ;
        DBConnector::ejecutar($query);
    }

    /**
     * obtiene las semilleras por nombre
     * @param integer $id  id de solicitud
     *
     * */
    public function getSemilleraByIdSolicitudForCuenta($id) {
        $query = "SELECT semillera FROM view_solicitudes_certificacion";
        $query .= " WHERE id_solicitud = $id";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * obtiene las semilleras por nombre
     * @param string $semillera  nombre de la semillera a buscar
     * @param string $sistema    nombre del sistema
     *
     * */
    function getSemilleraByNombre($semillera, $sistema) {
        $query = "SELECT DISTINCT semillera FROM view_full_solicitudes";
        $query .= " WHERE semillera LIKE '$semillera%' AND sistema LIKE '$sistema%'";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    public static function getSemillerasByNombreApellido($nombre, $apellido) {
        $query = "SELECT semillera FROM view_solicitudes WHERE nombre LIKE '$nombre' AND apellido LIKE '$apellido' LIMIT 1";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> semillera;
    }

    /**
     * retorna el nro de solicitud de una semillera
     * @param string $semillera  nombre de la semillera
     * @param string $sistema    nombre del sistema
     *
     * @return numero de solicitud
     * */
    public function getNoSolicitudSemillera($semillera, $sistema) {
        $query = "SELECT nro_solicitud FROM view_nro_solicitud WHERE sistema LIKE '$sistema%' AND nombre LIKE '$semillera%'";

        DBConnector::ejecutar($query);

        if (DBConnector::filas()) {
            $obj = DBConnector::objeto();
            return $obj -> nro_solicitud;
        } else {
            return 1;
        }

    }

    /**
     * Obtiene todos los nombres y apellidos de los semillaristas segun su estado
     *  tabla             valor      estado
     *  solicitud          0-10      1:semilla,2-4:superficie,5-7:inspeccion,8:cosecha,9:semillaproducida,10:laboratorio
     *  semilla            0-1       0:no tiene superficie   1:tiene superficie
     *  superficie         1-3       1:inscrita,2:rechazada,3:aprobada
     *  inpeccion          1-3       1:primera,2:segunda,3:tercera
     *  cosecha            0-1       0:no es papa,1:es papa
     *  laboratorio        0-1       0:no se completaron los datos,1:datos completos
     *  semilla producida  0-2       0:valor inicial,1:es papa2:no es papa
     * */
    public function getAllProductores($semillera, $estado, $flag = 'general') {
        $estado_1 = $estado[1];
        //limite inferior
        $estado_2 = $estado[2];
        //limite superior
        if ($estado_2 == -1) {//solo productores q realizaron solicitud
            $query = "SELECT DISTINCT(nombre),apellido FROM solicitud 
		WHERE laboratorio = $estado_1 AND semillera='$semillera'
		ORDER BY apellido ASC";
        } else {
            $query = "SELECT DISTINCT(nombre),apellido FROM solicitud sol 
				INNER JOIN semilla sem ON sol.id_solicitud=sem.id_solicitud
		WHERE  (sol.laboratorio >= $estado_1 AND sol.laboratorio <= $estado_2) 
		AND sol.semillera='$semillera' 
		ORDER BY sol.apellido ASC";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * productores de una semillera segun su estado2
     * **/
    public function getAllProductores2($semillera) {

        $query = "SELECT DISTINCT(CONCAT(nombre,' ',apellido)) as nombre FROM solicitud sol INNER JOIN semilla sem ON sol.id_solicitud=sem.id_solicitud
		WHERE ( (laboratorio >= 1 AND laboratorio <= 5) AND semillera='$semillera' ) OR sem.cultivo='papa'
		ORDER BY apellido ASC";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista todos los semilleristas
     * */
    public static function getFullProductores() {
        $query = "SELECT DISTINCT(CONCAT(nombre,' ',apellido)) AS nombre FROM solicitud";
        $query .= " WHERE nombre IS NOT NULL OR apellido IS NOT NULL ORDER BY apellido ASC";
        DBConnector::ejecutar($query);
    }

    public function getLastSolicitudIn($id, $sistema) {
        $query = "SELECT nro_solicitud,semillera,nombre,apellido FROM solicitud";
        $query .= " WHERE id_solicitud=(SELECT MAX(id_solicitud) FROM solicitud WHERE sistema LIKE '$sistema%' AND laboratorio=0)";
        DBConnector::ejecutar($query);
    }

    /**
     * registro de solicitud
     * @param $id integer id de solicitud
     * */
    public static function getRegistro($id) {
        $query = "SELECT nro_solicitud,nombre_departamento,provincia,municipio,fecha,comunidad,estado,semillera,semillerista ";
        $query .= "FROM view_solicitudes_certificacion WHERE id_solicitud=$id";
        //revisar view_solicitudes_certificacion
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR::".__METHOD__."::$query",DBConnector::mensaje();
    }

    /**
     * registro de solicitud de fiscalizacion
     * */
    public static function getRegistroFiscal($id) {
        $query = "SELECT nro_solicitud,nombre_departamento,provincia,municipio,fecha,comunidad,estado,semillera,";
        $query .= "semillerista FROM view_solicitudes_fiscalizacion WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }
    /**
     * resumen de datos de fiscalizacion
     * @param string   $cadena    cadena de busqueda para resumen de cuenta fiscal
     * */
     public function getResumenCuentaFiscal ($cadena){
            $query = "SELECT id_solicitud,id_semilla,semillera,semillerista,estado_solicitud AS estado,";
            $query .= "cultivo,variedad,categoria_producir,comunidad,nro_campo,lote ";
            $query .= "FROM view_semilla_fiscal ";
            $query .= "WHERE (semillera LIKE '$cadena%' OR semillerista LIKE '$cadena%' OR ";
            $query .= "cultivo LIKE '$cadena%' OR variedad LIKE '$cadena%') AND gestion = '".date('Y')."'";
            #echo $query;
            DBConnector::ejecutar($query); 
            if (DBConnector::nroError())
                echo 'ERROR view_semilla_fiscal :: ',DBConnector::mensaje();            
     }

    /** Devuelve todas las solicitudes segun el area
     * @param integer @area numero de area a la que pertenece
     * 1: certificacion
     * 2: fiscalizacion
     * 10: administracion
     * */
    public function getSolicitudes($area = 10) {
        //echo $area.'<br>';
        if ($area == 1)
            $query = "SELECT * FROM view_solicitudes_certificacion WHERE gestion = '" . date('Y') . "'";
        elseif ($area == 2) {
            $query = "SELECT * FROM view_solicitudes_fiscalizacion WHERE gestion = '" . date('Y') . "' ORDER BY id_solicitud DESC";
        } else {
            $query = "SELECT * FROM solicitud";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve los nombres de las columnas de una tabla
     * @param $search string cadena de busqueda
     * */
    public static function getSolicitudesCertificaFilter($search) {
        $sql = "SELECT id_solicitud,nro_solicitud,sistema,semillera,semillerista,nombre_departamento AS departamento,provincia,municipio,comunidad,fecha ";
        $sql .= "FROM view_solicitudes_certificacion ";
        $sql .= "WHERE (nro_solicitud LIKE '$search%' OR semillera LIKE '$search%' OR semillerista LIKE '%$search%'";
        $sql .= "OR comunidad LIKE '$search%' OR provincia LIKE '$search%' OR municipio LIKE '$search%') AND gestion ='".date('Y')."'";
        #echo $sql;
        DBConnector::ejecutar($sql);
    }

    /**
     * devuelve los nombres de las columnas de una tabla
     * @param $table string  nombre de la tabla
     * */
    public static function getSolicitudesFiscalFilter($search) {
        $sql = "SELECT nro_solicitud,sistema,semillera,semillerista,nombre_departamento AS departamento,provincia,municipio,comunidad,fecha FROM view_solicitudes_fiscalizacion ";
        $sql .= "WHERE semillerista LIKE '$search%' OR nro_solicitud LIKE '$search%' OR nombre_departamento LIKE '$search%' ";
        $sql .= "OR comunidad LIKE '$search%' OR provincia LIKE '$search%' OR municipio LIKE '$search%' or semillera LIKE '$search%'";
        #echo $sql;
        DBConnector::ejecutar($sql);
    }

    /** Devuelve todas las solicitudes por bloque*/
    public function getSolicitudesbyBlock($area = '', $inicio = 0, $nroReg = 15) {
        if ($area == 1) {
            $query = "SELECT * FROM view_solicitudes_certificacion ";
            $query .= "WHERE gestion = '" . date('Y') . "' ";
            $query .= "ORDER BY id_solicitud DESC, nro_solicitud ASC, semillera ASC, apellido ASC LIMIT $inicio,$nroReg";
        } elseif ($area == 2) {
            $query = "SELECT * FROM view_solicitudes_fiscalizacion ";
            $query .= "WHERE gestion = '" . date('Y') . "' ";
            $query .= "ORDER BY nro_solicitud DESC, semillera ASC LIMIT $inicio,$nroReg";
        } else {
            $query = "SELECT * FROM view_full_solicitudes ORDER BY nro_solicitud ASC, sistema ASC, semillera ASC, apellido ASC LIMIT $inicio,$nroReg";
        }
        #echo $query;
        return $query;
    }

    /**
     * cuenta el total de solicitudes de certificacion realizadas
     *
     */
    public function view_nextNroSolicitudCertifica($sistema) {
        if ($sistema == 'certifica') {
            $query = "SELECT MAX(DISTINCT(nro_solicitud)) AS nro_solicitud ";
        } else {
            $query = "SELECT MAX(nro_solicitud) AS nro_solicitud ";
        }
        $query .= "FROM view_nro_solicitud ";
        $query .= "WHERE sistema LIKE '$sistema%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    // devuelve el siguiente numero de una solicitud para nueva fiscalizacion
    public function view_getNroSolicitudFiscal($estado) {
        $query = "SELECT count( * ) AS nro_solicitud FROM view_nro_solicitud WHERE sistema LIKE 'fiscal%' AND estado=$estado";
        //  DBConnector::mensaje();
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el id de la solicitud que esta en un rango
     * */
    public static function getIdSolicitudByTime($area, $desde, $hasta, $gestion) {
        $query = "SELECT id_solicitud FROM view_solicitudes ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND estado >= 10 AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND estado >= 10 AND gestion = '$gestion'";
        }
        echo $query;
        DBConnector::ejecutar($query);
    }

    /**Id de solicitud de un determinado productor
     * @param string   $nombre  nombre del semillerista
     * @param string   $apellido  apellido del semillerista
     * */
    public static function getIdSolicitudProductor($nombre, $apellido) {
        $query = "SELECT MAX(id_solicitud) AS id_solicitud FROM view_solicitudes_certificacion WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%' AND estado IN (6,7)";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**Id de solicitud de un determinado productor
     * @param string   $nombre  nombre del semillerista
     * @param string   $apellido  apellido del semillerista
     * */
    public static function getIdSolicitudProductorCuenta($nombre, $apellido) {
        $query = "SELECT MAX(id_solicitud) AS id_solicitud FROM view_solicitudes_certificacion ";
        $query .= "WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%' AND estado >=1";
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        return $obj -> id_solicitud;
    }

    /**Id de solicitud de un determinado productor
     * @param string   $nombre  nombre completo del semillerista
     * */
    public static function getIdSolicitudProductorByFullName($nombre) {
        $query = "SELECT id_solicitud FROM view_solicitudes_fiscalizacion WHERE nombre LIKE '$nombre'";
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> id_solicitud;
    }

    /**
     *
     * */
    public static function getIdSolicitudByIdSemillera($id) {
        $query = "SELECT id_solicitud FROM view_solicitudes_certificacion WHERE id_semillera=$id";
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        if (DBConnector::filas())
            return $obj -> id_solicitud;
        else {
            return '-1';
        }
    }

    /**
     * devuelve el id de una solicitud segun el nro de solicitud
     * */
    public static function getIdSolicitudByNroSolicitud($nro) {
        $query = "SELECT id_solicitud FROM view_semilla WHERE nro_campo='$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        if (DBConnector::filas())
            return $obj -> id_solicitud;
        else {
            return '-1';
        }
    }

    /**
     * devuelve el id de una solicitud segun el nro de solicitud
     * */
    public static function getIdSolicitudMuestraFiscalLaboratorioByNroSolicitud($nro) {
        $query = "SELECT id_solicitud FROM view_datos_cosecha_fiscal WHERE nro_campo='$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        if (DBConnector::filas())
            return $obj -> id_solicitud;
        else {
            return '-1';
        }
    }

    /**Id de solicitud de un determinado productor
     * @param string   $nombre  nombre del semillerista
     * @param string   $estado  estado de solicitud del semillerista
     * */
    public static function getIdSolicitudProductorFiscal($nombre, $estado) {
        $query = "SELECT id_solicitud FROM view_solicitudes_fiscalizacion WHERE nombre LIKE '$nombre%' AND estado=$estado";
        #echo $query."=";
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**Id de solicitud de un determinado productor
     * usado en opcion SUPERFICIE-SEMILLA PRODUCIDA-CUENTA
     * @param string   $nombre  nombre del semillerista
     * @param string   $apellido  apellido del semillerista
     * @param string   $estado    estado de la solicitud
     * */
    public static function getIdSolicitudProductorByEstado($nombre, $apellido, $estado = '') {
        #echo $estado.'==';
        if ($estado == 'cuenta') {
            $query = "SELECT id_solicitud FROM view_solicitudes_certificacion WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%'";
        } else {
            if ($estado == 1) {
                $query = "SELECT id_solicitud FROM view_solicitudes_certificacion WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%' AND estado = $estado";
            } else {
                $query = "SELECT id_solicitud FROM view_semilla WHERE nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%'AND (estado_solicitud>=5 AND estado<11)";
            }

        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__.":: $query :: ",DBConnector::mensaje();
        }else{
            $row = DBConnector::objeto();
            return $row -> id_solicitud;
        }        
    }

    /**
     * @return devuelve el id de semillerista segun el id de Solicitud
     * */
    public static function getIdSemilleristaByIdSolicitud($id) {
        $query = "SELECT id_semillerista FROM view_solicitudes WHERE id_solicitud = $id ";

        DBConnector::ejecutar($query);
        $obj = DBConnector::objeto();

        return $obj -> id_semillerista;

    }

    /**
     * incrementa en 1 el valor de laboratorio
     * actualizar estado de la solicitud >1 y <6
     * 1 semilla OK  -  0
     * 2     1
     * 3     2
     * 4     3
     * 5
     * 6
     * 7
     * 8
     * 9
     *
     * @return void
     * */
    public static function updateLaboratorio($id) {
        $query = "UPDATE solicitud SET estado = estado + 1 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    public static function updateMuestraResultado($id) {
        $query = "UPDATE solicitud SET estado = 11 WHERE id_solicitud = $id";

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR :: ".__METHOD__.":: $query ::",DBConnector::mensaje();
        }
    }

    /**
     * establece un valor fijo en laboratorio
     * actualizar estado de la solicitud
     * 1 semilla OK  -  0
     * 2 sup. inscrita    1
     * 3 sup. retirada    2
     * 4 sup. rechazada   3
     * 5 sup. aprobada    4
     * 6 ins. vegetativa  6
     * 7 ins. floracion   7
     * 8 ins. cosecha     8
     * 9 hoja cosecha     9
     * 10 semilla prod   10
     *
     * @param int $id numero id de solicitud
     * @param int $esado  numero de la laboratorio
     *
     * @return void
     * */
    public static function updateEstado($id) {
        $query = "UPDATE solicitud SET estado = estado+1 WHERE id_solicitud = $id";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo "ERROR al actualizar estado de solicitud :: $query";
            exit ;
        }
    }

    public static function updateEstadoSemillaproducida($id) {
        $query = "UPDATE solicitud SET estado = 12 WHERE id_solicitud = $id";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo "ERROR al actualizar estado de solicitud :: $query";
            exit ;
        }
    }

    /**
     *
     * @param int $id numero id de solicitud
     *
     * @return void
     * */
    public static function updateEstadoMuestra($id) {
        $query = "UPDATE solicitud SET estado = 9 WHERE id_solicitud = $id";

        #echo $query;
        DBConnector::ejecutar($query);
    }

    public static function updateEstadoCosecha($id) {
        $query = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para cuenta
     * @param integer $id  id de solicitud
     * @param integer $estado estado de cuenta [12: saldo cero, 11: debe]
     * */
    public static function updateEstadoCuenta($id, $estado) {
        $query = "UPDATE solicitud SET estado = $estado WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para hoja de cosecha
     * */
    public function updateEstado3($id) {
        $query = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para hoja de cosecha
     * si el cultivo no es papa salta la prueba de laboratorio
     * */
    public static function updateEstado4($id) {
        $query = "UPDATE solicitud SET estado = 8 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }
    /**
     * para inspeccion de plantines
     * */
    public static function updateEstadoPlantines($id) {
        $query = "UPDATE solicitud SET estado = 5 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para hoja de cosecha
     * */
    public function updateEstadoFiscal($id, $estado) {
        $query = "UPDATE solicitud SET estado = $estado WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para superficie
     * */
    public static function updateEstado2($id) {
        $query = "UPDATE solicitud SET estado = 4 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * para semilla producida
     * */
    public static function updateEstadoBySemillaProd($id) {
        $query = "UPDATE solicitud SET estado = 10 WHERE id_solicitud = $id";

        #echo $query;exit;
        DBConnector::ejecutar($query);
    }

    /**
     * actualiza el contador de parcelas
     *
     * @return void
     * */
    public static function updateCountParcela($id) {
        $query = "UPDATE solicitud SET count_parcelas = count_parcelas+1 
		    WHERE id_solicitud = $id";

        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * actualizar estado  segun el numero ingresado
     * @param  integer   $numero   estado de solicitud
     * @param  integer   $id       id de la solicitud
     * */
    public static function updateStadoByNumber($numero, $id) {
        $query = "UPDATE solicitud SET estado = $numero WHERE id_solicitud = $id";

        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * reinicia el contador de parcelas
     *
     * @return void
     * */
    public static function resetCountParcela($id) {
        $query = "UPDATE solicitud SET count_parcelas = 0 
		    WHERE id_solicitud = $id";

        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * estado de laboratorio para papa
     * */
    public static function estado($isemilla, $iproductor) {
        $query = "SELECT sol.estado,sem.cultivo,
        sup.estado, sol.id_solicitud AS id_solicitud,
        sup.inscrita,sup.rechazada,sup.retirada,sup.aprobada, ins.primera
		FROM solicitud sol 
		INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud 
		INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla 
		INNER JOIN superficie sup ON sup.id_semilla = sem.id_semilla
     	INNER JOIN superficie_inspeccion r ON r.id_superficie=sup.id_superficie
     	INNER JOIN inspeccion ins ON ins.id_inspeccion=r.id_inspeccion
     	INNER JOIN cultivo cul ON cul.id_cultivo = sem.cultivo
     	WHERE (sol.estado>=1 AND sol.estado<=8) AND cul.nombre_cultivo='papa'
     	AND (sup.estado>=0 AND sup.estado<=3) AND sup.id_semilla = $isemilla AND sol.id_solicitud=$iproductor LIMIT 1";
        #echo $query;exit;
        DBCOnnector::ejecutar($query);
    }

    /**
     * estado de laboratorio para otros cultivos
     * */
    public static function estado_2($isemilla, $iproductor) {
        $query = "SELECT sol.estado,cul.nombre_cultivo, sol.id_solicitud AS id_solicitud,sup.inscrita,sup.rechazada,sup.retirada,sup.aprobada, ins.primera
		FROM solicitud sol 
		INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud
		INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla 
		INNER JOIN cultivo cul ON cul.id_cultivo=sem.cultivo
		INNER JOIN superficie sup ON sup.id_semilla=sem.id_semilla
		INNER JOIN superficie_inspeccion r ON r.id_superficie=sup.id_superficie
		INNER JOIN inspeccion ins ON ins.id_inspeccion=r.id_inspeccion
     	WHERE (sol.estado>=1 OR sol.estado<=8)
     	AND (sup.estado>=0 AND sup.estado<=3) AND sup.id_semilla = $isemilla AND sol.id_solicitud=$iproductor";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * estado de laboratorio para otros cultivos sin tener primera inspeccion
     * */
    public static function estado_2a($isemilla, $iproductor) {
        $query = "SELECT sol.estado,cul.nombre_cultivo AS cultivo, sol.id_solicitud AS id_solicitud,sup.inscrita,sup.rechazada,sup.retirada,sup.aprobada
		FROM solicitud sol 
		INNER JOIN solicitud_semilla t ON t.id_solicitud=sol.id_solicitud
        INNER JOIN semilla sem ON sem.id_semilla=t.id_semilla 
        INNER JOIN cultivo cul ON cul.id_cultivo=sem.cultivo
        INNER JOIN superficie sup ON sup.id_semilla=sem.id_semilla
        INNER JOIN superficie_inspeccion r ON r.id_superficie=sup.id_superficie
        INNER JOIN inspeccion ins ON ins.id_inspeccion=r.id_inspeccion
     	WHERE (sol.estado>=1 OR sol.estado<=8)
     	AND (sup.estado>=0 AND sup.estado<=3) AND sup.id_semilla = $isemilla AND sol.id_solicitud=$iproductor LIMIT 1";
        echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * Lista todas las semilleras
     * @param integer $sistema id del sistema
     *
     * 1:certificacion
     * 2:fiscalizacion
     * 10:administracion
     * */
    public static function getSemilleras($sistema = 1, $inicio = 'A', $final = 'F', $gestion = '2000') {
        switch ($sistema) {
            case '1' :
                $query = "SELECT DISTINCT (semillera) FROM view_solicitudes_certificacion ";
                $sql = "(semillera LIKE '$inicio%' OR semillera LIKE '".chr(ord($inicio)+1)."%' OR semillera LIKE '".chr(ord($inicio)+2)."%'  OR semillera LIKE '".chr(ord($inicio)+3)."%' OR semillera LIKE '$final%')";
                $query .= "WHERE $sql AND gestion = '$gestion' ORDER BY semillera ASC";
                break;

            case '2' :
                $query = "SELECT DISTINCT (semillera) FROM view_solicitudes_fiscalizacion ";
                $sql = "(semillera LIKE '$inicio%' OR semillera LIKE '".chr(ord($inicio)+1)."%' OR semillera LIKE '".chr(ord($inicio)+2)."%'  OR semillera LIKE '".chr(ord($inicio)+3)."%' OR semillera LIKE '$final%')";
                $query .= "WHERE $sql AND gestion = '$gestion'  ORDER BY semillera ASC";
                break;
            default :
                $query = "SELECT DISTINCT semillera FROM view_full_solicitudes ";
                $sql = "(semillera LIKE '$inicio%' OR semillera LIKE '".chr(ord($inicio)+1)."%' OR semillera LIKE '".chr(ord($inicio)+2)."%'  OR semillera LIKE '".chr(ord($inicio)+3)."%' OR semillera LIKE '$final%')";
                $query .= "WHERE $sql AND gestion = '$gestion'  ORDER BY semillera ASC";
                break;
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR al seleccionar semillera ',DBConnector::mensaje();
    }
    /**
     * devuelve la semillera segun comunidad y semillerista
     * */
    public static function getSemilleraByComunidadSemillerista($comunidad,$semillerista,$desde='',$hasta='',$sistema='',$gestion=''){
        $query = "SELECT semillera FROM view_solicitudes ";
        $query .= "WHERE comunidad = '$comunidad' AND semillerista LIKE '$semillerista'";
        
        if(!empty($desde)){
            if (in_array($desde, array("01", "04", "07", "10"))) {
                $query = "SELECT semillera FROM view_solicitudes ";
                $query .= "WHERE semillerista LIKE '$semillerista'";
                $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND estado >= 10 AND id_sistema=$sistema  ";
                $query .= "AND gestion = '$gestion' ORDER BY semillera";
    
            } else {
                $query = "SELECT semillera FROM view_solicitudes ";
                $query .= "WHERE semillerista LIKE '$semillerista'";
                $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND estado >= 10 AND id_sistema=$sistema";
                $query .= "AND gestion = '$gestion' ORDER BY semillera";
            }
            DBConnector::ejecutar($query);
            if (DBConnector::filas()){
                $row = DBConnector::objeto();        
                return $row->semillera;
            }else{
                return '';
            }
            
        }
        
        DBConnector::ejecutar($query);
        $row = DBConnector::objeto();        
        return $row->semillera;
    }
    /**
     * Lista todas las semilleras segun el cultivo y la fecha
     * @param integer $sistema id del sistema
     *
     * 1:certificacion
     * 2:fiscalizacion
     * 10:administracion
     * */
    public static function getSemillerasByCultivoAndTime($desde, $hasta, $cultivo, $sistema = 1) {
        switch ($sistema) {
            case '1' :
                $query = "SELECT DISTINCT (semillera), fecha,comunidad,semillerista,cultivo,variedad,LEFT(cat_sembrada,2),nro_campo,LEFT(cat_aprobada,2)";
                $query .= "FROM view_detalle_produccion_costos WHERE cultivo='$cultivo' AND (fecha >= '$desde' AND fecha <= '$hasta') ORDER BY semillera";
                #echo $query;exit;
                break;
            case '2' :
                $query = "SELECT DISTINCT (semillera) FROM view_solicitudes_fiscalizacion fecha >= '$desde' AND fecha <= '$hasta' ORDER BY semillera";
                break;
            default :
                $query = "SELECT DISTINCT semillera FROM view_full_solicitudes ORDER BY semillera";
                break;
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * Lista todas las semilleras menos los particulares
     * @param integer $sistema id del sistema
     *
     * 1:certificacion
     * 2:fiscalizacion
     * */
    public static function getSemillerasByTime($desde, $hasta, $gestion, $sistema = 1) {
        switch ($sistema) {
            case '1' :
                $query = "SELECT DISTINCT(semillera) FROM view_solicitudes_certificacion ";
                break;

            case '2' :
                $query = "SELECT DISTINCT(semillera) FROM view_solicitudes_fiscalizacion ";
                break;
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND estado >= 12 AND id_sistema=$sistema AND ";
            $query .= "(semillera NOT LIKE '%part_%' OR semillera NOT LIKE 'particular%') AND gestion = '$gestion' ORDER BY semillera";

        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND estado >= 12 AND id_sistema=$sistema AND ";
            $query .= "(semillera NOT LIKE '%part_%' OR semillera NOT LIKE 'particular%') AND gestion = '$gestion' ORDER BY semillera";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo DBConnector::mensaje(),'=',$query;
    }

    /**
     * total de semilleras particulares
     * @param date $desde  fecha inicio
     * @param date $hasta  fecha final
     * @param int  @sistema  id del sistema
     * 1:certificacion
     * 2:fiscalizacion
     * 10:administracion
     * */
    public static function getSemillerasTotalParticularesByTime($desde, $hasta, $gestion, $sistema = 1) {
        switch ($sistema) {
            case '1' :
                # certificacion
                $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes_certificacion ";
                break;

            case '2' :
                # fiscalizacion
                $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes_fiscalizacion ";
                break;
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND estado >= 12 AND id_sistema=$sistema AND gestion='$gestion' AND ";
            $query .= "(semillera LIKE '%part_%' OR semillera LIKE 'particular%') ORDER BY semillera";

        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND estado >= 12 AND id_sistema=$sistema AND gestion='$gestion' AND ";
            $query .= "(semillera LIKE '%part_%' OR semillera LIKE 'particular%') ORDER BY semillera";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo DBConnector::mensaje(),'=',$query;
        else{
           $obj = DBConnector::objeto();
                return $obj -> total; 
        }        
    }

    /**
     * total de semilleras particulares
     * @param date $desde  fecha inicio
     * @param date $hasta  fecha final
     * @param int  @sistema  id del sistema
     * 1:certificacion
     * 2:fiscalizacion
     * 10:administracion
     * */
    public static function getSemillerasTotalByTime($desde, $hasta, $gestion, $sistema = '1') {

        if ($sistema == '1') {
            $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes_certificacion ";
        } else {
            $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes_fiscalizacion ";
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado >= 12 AND id_sistema = $sistema AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado >= 12 AND id_sistema = $sistema AND gestion = '$gestion'";
        }

#echo $query;
        DBConnector::ejecutar($query);        
        if (DBConnector::filas()){
            $obj = DBConnector::objeto();
            return $obj -> total;
        }else
            return 0;
    }

    /**
     * devuelve las semilleras en un rango determinado
     * @param  date     $desde     fecha inicial
     * @param  date     $hasta     fecha final
     * @param  date     $gestion   gestion actual
     * @param  integer  $sistema   id de sistema
     * */
    public static function getListSemillerasByTime($desde, $hasta, $gestion, $sistema = '1') {

        if ($sistema == '1') {
            $query = "SELECT DISTINCT(semillera) AS total FROM view_solicitudes_certificacion ";
        } else {
            $query = "SELECT DISTINCT(semillera) AS total FROM view_solicitudes_fiscalizacion ";
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado >= 10 AND id_sistema = $sistema AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado >= 10 AND id_sistema = $sistema AND gestion = '$gestion'";
        }

        DBConnector::ejecutar($query);
    }
    /**
     * devuele las solicitudes en un rango de fecha
     * @param     date     $desde      fecha inicial
     * @param     date     $hasta      fecha final
     * @param     integer  $sistema    nro de sistema
     * @param     year     $gestion    año de gestion
     * */ 
     public static function getListSolicitudesByFechaForDetalleResumen($desde, $hasta, $sistema,$gestion){
         if ($sistema == '1') {
            $query = "SELECT id_solicitud,provincia,municipio,comunidad,semillera,semillerista,id_solicitud FROM view_solicitudes_certificacion ";
        } else {
            $query = "SELECT id_solicitud,provincia,municipio,comunidad,semillera,semillerista,id_solicitud FROM view_solicitudes_fiscalizacion ";
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND estado >= 12 AND ";
            $query .= "id_sistema = $sistema AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND estado >= 12 AND ";
            $query .= "id_sistema = $sistema AND gestion = '$gestion'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError()){
            echo DBConnector::mensaje(),'??',$query;
        }
     }
    /**
     * total de solicitudes pendientes en un rango de meses
     * */
    public static function getTotalSolicitudesPendientes($desde, $hasta, $sistema = 1) {
        $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes ";
        $query .= "WHERE (mes <= '$desde' AND mes >= '$hasta') and estado < 10";

        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        if ($obj -> total) {
            return $obj -> total;
        } else {
            return '0';
        }
    }

    /**
     * total de solicitudes terminadas en un rango de meses
     * */
    public static function getTotalSolicitudesTerminadas($desde, $hasta, $gestion, $sistema = 1) {
        $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_solicitudes ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado = 12 AND id_sistema = $sistema AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado =12 AND id_sistema = $sistema AND gestion = '$gestion'";
        }
        DBConnector::ejecutar($query);
        #echo $query,'=',DBConnector::mensaje();
        $obj = DBConnector::objeto();

        if (!empty($obj -> total)) {
            return $obj -> total;
        } else {
            return '0';
        }
    }

    /**
     * total de solicitudes pendientes en un rango de meses
     * */
    public static function getTotalSolicitudes($desde, $hasta, $gestion, $sistema = 1) {
        $query = "SELECT COUNT(DISTINCT(semillera)) AS total FROM view_full_solicitudes ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "WHERE (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado = 1 AND id_sistema = $sistema AND gestion = '$gestion'";
        } else {
            $query .= "WHERE (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado = 1 AND id_sistema = $sistema AND gestion = '$gestion'";
        }

        DBConnector::ejecutar($query);
        #echo $query,'=',DBConnector::mensaje();
        $obj = DBConnector::objeto();

        if (!empty($obj -> total)) {
            return $obj -> total;
        } else {
            return '0';
        }
    }

    /**
     * devuelve el nro de solicitud segun el id solicitud
     * busqueda en sistema de certificacion
     * @param integer $id_solicitud id de la solicitud
     *
     * */
    public static function getNroSolicitudByIdSolicitud($id) {
        $query = "SELECT nro_solicitud FROM view_solicitudes_certificacion WHERE id_solicitud = $nro";

        DBConnector::ejecutar($query);

        $row = DBConnector::objeto();

        return $row -> nro_solicitud;
    }

    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getNroSolicitudByNroSolicitud($nro) {
        $query = "SELECT nro_solicitud FROM view_solicitudes_fiscalizacion WHERE nro_solicitud LIKE '$nro%' AND sistema LIKE 'fiscaliza%'";

        DBConnector::ejecutar($query);
    }

    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getNroSolicitudFiscalByNroSolicitudAndEstadoFiscal($buscar, $estado,$tipo) {
        if ($tipo == 'nro'){
            $query = "SELECT nro_solicitud FROM view_solicitudes_fiscalizacion ";
            $query .= "WHERE nro_solicitud LIKE '$buscar%' AND estado=$estado AND gestion = '".date('Y')."' ";
            $query .= "ORDER BY nro_solicitud ASC";
        }elseif ($tipo == 'semillera') {
            $query = "SELECT DISTINCT(semillera) FROM view_solicitudes_fiscalizacion ";
            $query .= "WHERE semillera LIKE '$buscar%' AND estado=$estado AND gestion = '".date('Y')."' ";
            $query .= "ORDER BY semillera ASC";
        }else{
            $query = "SELECT DISTINCT(semillerista) FROM view_solicitudes_fiscalizacion ";
            $query .= "WHERE semillerista LIKE '$buscar%' AND estado=$estado AND gestion = '".date('Y')."' ";
            $query .= "ORDER BY semillerista ASC";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR view_solicitudes_fiscalizacion',DBConnector::mensaje();
    }

    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getNroSolicitudByNroSolicitudAndEstadoFiscal($nro, $estado, $tipo) {
        if ($tipo == 'nro'){
            $query = "SELECT nro_campo FROM view_datos_cosecha_fiscal ";
            $query .= "WHERE nro_campo LIKE '$nro%' AND estado_solicitud=$estado AND cultivo <> 'papa' AND gestion = '".date('Y')."' ";
        }elseif ($tipo == 'semillera') {
            $query = "SELECT semillera FROM view_datos_cosecha_fiscal ";
            $query .= "WHERE semillera LIKE '$nro%' AND estado_solicitud=$estado AND cultivo <> 'papa' AND gestion = '".date('Y')."' ";
        }else{
            $query = "SELECT semillerista FROM view_datos_cosecha_fiscal ";
            $query .= "WHERE semillerista LIKE '$nro%' AND estado_solicitud=$estado AND cultivo <> 'papa' AND gestion = '".date('Y')."' ";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR view_datos_cosecha_fiscal ',DBConnector::mensaje();
    }

    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getIdCosechaByIdSolicitud($id) {
        $query = "SELECT id_cosecha FROM view_semilla_producida_fiscal_2 ";
        $query .= "WHERE id_solicitud =$id";

        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * consulta los datos pra autompletado de busqueda segun data enviado
     * */
     public function getInformacion($cadena,$estado){
         if (is_numeric($cadena)){
             #buscar nro de solicitud
         }else{
             #primero buscar semillera
             #si no encuentra coincidencia buscar semillerista
         }
     }
    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getNroSolicitudByNroSolicitudAndEstadoCertifica($nro, $estado,$tipo) {
        if ($tipo == 'nro'){
            $query = "SELECT DISTINCT(nro_solicitud) FROM view_solicitudes_certificacion ";
            $query .= "WHERE nro_solicitud LIKE '%$nro%' ";
            $query .= "AND estado=$estado AND gestion = '".date('Y')."' ORDER BY nro_solicitud ASC";
        }elseif ($tipo == 'semillera'){
            $query = "SELECT DISTINCT(semillera) FROM view_solicitudes_certificacion ";
            $query .= "WHERE semillera LIKE '$nro%' ";
            $query .= "AND estado=$estado AND gestion = '".date('Y')."' ORDER BY semillera ASC";
        }else{
            $query = "SELECT DISTINCT(semillerista) FROM view_solicitudes_certificacion ";
            $query .= "WHERE semillerista LIKE '%$nro%' ";
            $query .= "AND estado=$estado AND gestion = '".date('Y')."' ORDER BY semillerista ASC";
        }
        
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de semilleras y solicitudese segun nro de solicitud
     * @param string $nosolicitud   numero de solicitud
     *
     * */
    public static function getNroSolicitudByNroSolicitudAndEstadoCertificaSuperficie($nro, $inicio, $fin) {
        if ($fin == 5 || $fin == 7) {//si es inspeccion
            $query = "SELECT DISTINCT(nro_solicitud) AS nro_solicitud FROM view_solicitudes_certificacion ";
            $query .= "WHERE nro_solicitud LIKE '$nro%' AND (estado >= $inicio AND estado in (4,5,6)) AND gestion = '".date('Y')."' ORDER BY nro_solicitud DESC";
        } elseif ($inicio == 8 && $fin == 12) {// si es semilla producida
            $query = "SELECT DISTINCT(nro_solicitud) AS nro_solicitud FROM view_semilla ";
            $query .= "WHERE nro_solicitud LIKE '$nro%' AND ((estado_solicitud = 8 AND cultivo = 'papa') OR (estado_solicitud=11 AND cultivo <> 'papa')) AND gestion = '".date('Y')."' ORDER BY nro_solicitud DESC ";
        } elseif ($fin == 8) {//si es hoja cosecha
            $query = "SELECT DISTINCT(nro_solicitud) AS nro_solicitud FROM view_solicitudes_certificacion ";
            $query .= "WHERE nro_solicitud LIKE '$nro%' AND (estado >= $inicio AND estado <= 8) AND gestion = '".date('Y')."' ORDER BY nro_solicitud DESC ";
        } else {//si es superficie
            $query = "SELECT DISTINCT(nro_solicitud) AS nro_solicitud FROM view_solicitudes_certificacion ";
            $query .= "WHERE nro_solicitud LIKE '$nro%' AND (estado >= $inicio AND estado<5) AND gestion = '".date('Y')."' ORDER BY nro_solicitud DESC";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__.":: ",DBConnector::mensaje();
    }

    /**
     * año de gestiones
     * @param integer $sistema id del tipo de sistema [certifiacion | fiscalizacion]
     * */
    public static function getGestiones($sistema) {
        $query = "SELECT DISTINCT(gestion) FROM view_gestiones_anteriores ";
        $query .= "WHERE id_sistema=$sistema AND gestion < " . date('Y') ." AND gestion <> '0000'";

        #echo $query; //exit;
        DBConnector::ejecutar($query);
    }

    /**nombre q coinciden con la busqueda*/
    private static function getNombreProductorBySistema($nombre_apellido, $sistema) {
        $query = "SELECT DISTINCT(CONCAT(nombre,' ',apellido)) AS nombre FROM solicitud WHERE (nombre like '$nombre_apellido%')";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /***/
    public static function getSolicitudByCultivoAndSemillera($semillera) {
        $query = "SELECT * FROM view_semilla WHERE estado = 8 AND cultivo = 'papa' AND semillera LIKE '$semillera%'";

        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun id de solicitud
     * **/
    public static function getSolicitudById($id) {
        $query = "SELECT * FROM view_single_solicitud WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitud($nro) {
        $query = "SELECT id_solicitud,semillerista,semillera,comunidad FROM view_single_solicitud WHERE nro_solicitud LIKE '$nro%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstado($nro, $estado) {
        $query = "SELECT id_solicitud,semillerista,semillera,comunidad FROM view_single_solicitud ";
        $query .= "WHERE nro_solicitud LIKE '$nro%' AND estado=$estado";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstadoCertifica($nro, $estado) {
        
        $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad,fecha ";
        $query .= "FROM view_solicitudes_certificacion ";
        if ($estado <= 4) {//superficie
            $query .= "WHERE nro_solicitud LIKE '%$nro%' AND estado=$estado AND gestion = '".date('Y')."' ORDER BY id_solicitud";
        } else {
            $query .= "WHERE nro_solicitud LIKE '%$nro%' AND estado in (5,6,7) AND gestion = '".date('Y')."' ORDER BY id_solicitud";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ",DBConnector::mensaje();
    }
    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstadoCertificaV2($nro, $estado) {
        
        $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad,fecha ";
        $query .= "FROM view_solicitudes_certificacion ";
        if ($estado <= 4) {
            $query .= "WHERE nro_solicitud LIKE '%$nro%' AND estado=$estado AND gestion = '".date('Y')."' ORDER BY comunidad";
        } else {
            $query .= "WHERE nro_solicitud LIKE '%$nro%' AND estado in (5,6,7) AND gestion = '".date('Y')."' ORDER BY comunidad";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ",DBConnector::mensaje();
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstadoForInspeccion($nro) {
        $query = "SELECT id_solicitud,semillerista,semillera,comunidad FROM view_solicitudes_certificacion ";
        $query .= "WHERE nro_solicitud LIKE '$nro%' AND estado in (4,5,6) AND gestion = '".date('Y')."' ORDER BY comunidad";
        #echo $query; 
        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstadoForCosecha($nro) {
        $query = "SELECT id_solicitud,semillerista,semillera,comunidad FROM view_solicitudes_certificacion ";
        $query .= "WHERE nro_solicitud LIKE '$nro%' AND estado in (6,7,8)";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $nro  numero de solicitud de fiscalizacion
     *
     * @return devuelve semillerista y semillera
     * **/
    public static function getSolicitudByNroSolicitudAndEstadoForSemProd($nro) {
        $query = "SELECT nro_solicitud,id_solicitud,nombre_apellido AS semillerista,semillera,comunidad,cultivo,variedad ";
        $query .= "FROM view_semilla ";
        $query .= "WHERE nro_solicitud LIKE '$nro%' AND estado_solicitud IN (8,11) AND gestion = '".date('Y')."'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR :: ".__METHOD__.":: ",DBConnector::mensaje();
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $semillera  nombre de la semillera
     *
     * @return devuelve el nro de solicitud y semillerista
     * **/
    public static function getSolicitudBySemillera($semillera) {
        $query = "SELECT id_solicitud,nro_solicitud,semillerista,comunidad FROM view_single_solicitud ";
        $query .= "WHERE semillera LIKE '$semillera%' ORDER BY nro_solicitud";

        DBConnector::ejecutar($query);
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $semillera  nombre de la semillera
     * @param integer $estado estado de solicitud
     *
     * @return devuelve el nro de solicitud,semillerista y comunidad
     * **/
    public static function getSolicitudBySemilleraAndEstado($semillera, $estado = '',$sistema = 'certifica') {
        if ($sistema == 'certifica'){
            $query = "SELECT id_solicitud,nro_campo AS nro_solicitud,semillerista,comunidad,cultivo,variedad ";
            $query .= "FROM view_semilla ";
            $query .= "WHERE semillera LIKE '$semillera%' AND estado_solicitud=$estado AND sistema LIKE '$sistema%' ";
            $query .= "ORDER BY nro_solicitud";
        }else{
            //fiscalizacion            
            if ($estado > 0){
                $query = "SELECT id_solicitud,nro_campo AS nro_solicitud,semillerista,comunidad,cultivo,variedad ";
                $query .= "FROM view_semilla_fiscal ";
                $query .= "WHERE semillera LIKE '$semillera%' AND estado_solicitud=$estado ";
                $query .= "ORDER BY id_solicitud";
            }else{//si tiene solo solicitud 
                $query = "SELECT id_solicitud,nro_solicitud,semillerista,comunidad ";
                $query .= "FROM view_solicitudes_fiscalizacion ";    
                $query .= "WHERE semillera LIKE '$semillera%' AND estado=$estado ";
                $query .= "ORDER BY id_solicitud";
            }
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR :: ",DBConnector::mensaje();
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $semillera  nombre de la semillera
     * @param integer $estado estado de solicitud
     *
     * @return devuelve el nro de solicitud,semillerista y comunidad
     * **/
    public static function getSolicitudBySemilleraAndEstadoCertifica($semillera, $estado = '',$cultivo='papa') {
        $query = "SELECT id_semilla,estado_solicitud AS estado,id_solicitud,nro_solicitud,nombre_apellido AS semillerista,comunidad,cultivo,variedad,plantin ";
        $query .= "FROM view_semilla ";
        if($estado == 0){//semilla
            $query = "SELECT estado,id_solicitud,nro_solicitud,semillerista,comunidad ";
            $query .= "FROM view_solicitudes_certificacion "; 
            $query .= "WHERE semillera LIKE '$semillera%' AND estado=$estado AND sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY semillerista"; 
        }elseif ($estado >= 1 && $estado < 4) {//superficie
            $query .= "WHERE semillera LIKE '$semillera%' AND estado_solicitud=$estado AND sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY id_solicitud";
        } elseif ($estado == 6) {
            $query .= "WHERE semillera LIKE '$semillera%' AND estado_solicitud in (6,7) AND sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY comunidad";
        }elseif ($estado == 8){//hoja cosecha
            $query .= "WHERE ( (semillera LIKE '$semillera%' AND estado_solicitud IN (6,7,8)) OR (semillera LIKE '$semillera%' AND estado_solicitud = 8 AND cultivo='papa') ) ";
            $query .= "AND sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY comunidad";    
        }elseif ($estado == 11 || $estado == 8) {//semilla producida
            $query .= "WHERE ( (semillera LIKE '$semillera%' AND estado_solicitud = 11)) ";
            $query .= "AND sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY comunidad";
        } else {//inspeccion
            $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad FROM view_solicitudes_certificacion ";
            $query .= "WHERE semillera LIKE '$semillera%' AND estado in (4,5,6) AND ";
            $query .= "sistema LIKE 'certifica%' AND gestion = '".date('Y')."' ORDER BY comunidad";
        }

        #echo $query;exit;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query :: ",DBConnector::mensaje();
    }

    /**
     * datos de solicitud segun nro de solicitud
     * @param string $semillerista nombre de semillerista
     *
     * @return devuelve el numero de solicitud y semillera
     * **/
    public static function getSolicitudBySemillerista($semillerista) {
        $query = "SELECT id_solicitud,nro_solicitud,semillera,comunidad FROM view_single_solicitud WHERE semillerista LIKE '$semillerista%'";
        #echo $query;
        DBConnector::ejecutar($query);

    }

    /**
     * datos de solicitud segun nro de solicitud FISCALIZACION
     * LLAMADO PARA SEMILLA
     * @param string $semillerista nombre de semillerista
     *
     * @return devuelve el numero de solicitud y semillera
     * **/
    public static function getSolicitudBySemilleristaAndEstado($semillerista, $estado) {
        $query = "SELECT id_solicitud,nro_solicitud,semillera,comunidad,estado ";
        $query .= "FROM view_solicitudes_fiscalizacion ";
        $query .= "WHERE semillerista LIKE '$semillerista%'and estado=$estado";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR :: ',DBConnector::mensaje();
    }
    /**
     * datos de solicitud segun nro de solicitud FISCALIZACION
     * LLAMADO PARA HOJA DE COSECHA
     * @param string $semillerista nombre de semillerista
     *
     * @return devuelve el numero de solicitud y semillera
     * **/
    public static function getSolicitudBySemilleristaAndEstadoFiscal($semillerista, $estado) {
        $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad,estado_solicitud,cultivo,variedad ";
        $query .= "FROM view_semilla_fiscal ";
        $query .= "WHERE semillerista LIKE '$semillerista%'and estado_solicitud=$estado";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR :: ',DBConnector::mensaje();
    }

    /**
     * solo gestion actual
     * LLAMADO PARA SEMILLA
     * */
    public static function getSolicitudBySemilleristaAndEstadoCertifica($semillerista, $estado) {
        $query = "SELECT id_solicitud,nro_solicitud,semillera,comunidad,estado ";
        $query .= "FROM view_solicitudes_certificacion ";
        if ($estado == 0) {//semilla
            $query .= "WHERE semillerista LIKE '%$semillerista%' AND estado=$estado ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }elseif ($estado == 6) { //hoja cosecha
            $query .= "WHERE semillerista '%$semillerista%' AND estado IN (6,7) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";    
        }elseif($estado == 11){//semilla producida
            $query .= "WHERE semillerista LIKE '%$semillerista%' AND ";
            $query .= "((estado_solicitud = 11 AND cultivo <> 'papa') OR (estado = 8 AND cultivo = 'papa')) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }else {//inspeccion
            $query .= "WHERE semillerista LIKE '%$semillerista%' AND estado IN (4,5,6) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
    }
/**
     * solo gestion actual
     * LLAMADO PARA SUPERFICIE-INSPECCION
     * */                  
    public static function getSolicitudBySemilleristaAndEstadoCertificaV22($semillerista, $estado,$sistema,$cultivo='papa') {
        $query = "SELECT id_solicitud,nro_solicitud,nombre_apellido AS semillerista,semillera,comunidad,estado,cultivo,variedad,plantin ";
        $query .= "FROM view_semilla ";
        switch ($estado) {
            case '11':
                $query .= "WHERE nombre_apellido LIKE '%$semillerista%' AND ";
                $query .= "((estado_solicitud = 11 AND cultivo <> 'papa') OR (estado_solicitud = 8 AND cultivo = 'papa')) ";
                $query .= "AND gestion = '" . date('Y') . "' AND sistema LIKE '$sistema%' ORDER BY comunidad"; 
            break;
            case '6':
                $query .= "WHERE nombre_apellido LIKE '%$semillerista%' AND estado_solicitud IN (6,7) ";
                $query .= "AND gestion = '" . date('Y') . "' AND sistema LIKE '$sistema%' ORDER BY comunidad"; 
            break;
            case '4':
                $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad,estado ";
                $query .= "FROM view_solicitudes_certificacion ";
                $query .= "WHERE (semillerista LIKE '%$semillerista%' AND estado IN (4,5,6)) OR (semillerista LIKE '%$semillerista%' AND estado IN (4,5,6)  AND cultivo='papa') ";
                $query .= "AND gestion = '" . date('Y') . "' AND sistema LIKE '$sistema%' ORDER BY comunidad";  
            break;
            case '1':
                $query .= "WHERE nombre_apellido LIKE '%$semillerista%' AND estado_solicitud=$estado ";
                $query .= "AND gestion = '" . date('Y') . "' AND sistema LIKE '$sistema%' ORDER BY comunidad";
                break;
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
    }
    public static function getSolicitudBySemilleristaAndEstadoCertificaForInspeccion($semillerista, $estado,$sistema,$cultivo='papa') {
        $query = "SELECT id_solicitud,nro_solicitud,semillerista,semillera,comunidad,estado ";
                $query .= "FROM view_solicitudes_certificacion ";
                $query .= "WHERE semillerista LIKE '%$semillerista%' AND estado IN (4,5,6) ";
                $query .= "AND gestion = '" . date('Y') . "' AND sistema LIKE '$sistema%' ORDER BY comunidad";   $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: $query ::",DBConnector::mensaje();
    }
    /**
     * solo gestion actual
     * */
    public static function getSolicitudBySemilleristaAndEstadoCertificaV2($semillerista, $estado) {
        $query = "SELECT id_solicitud,nro_solicitud,semillera,comunidad,estado ";
        $query .= "FROM view_solicitudes_certificacion ";
        if ($estado < 4 || $estado == 0) {
            $query .= "WHERE semillerista LIKE '$semillerista%' AND estado=$estado ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }elseif ($estado == 6) { //hoja cosecha
            $query .= "WHERE semillerista LIKE '$semillerista%' AND estado IN (6,7) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";    
        }elseif($estado == 11){//semilla producida
            $query .= "WHERE semillerista LIKE '$semillerista%' AND ";
            $query .= "((estado = 11 AND cultivo <> 'papa') OR (estado = 8 AND cultivo = 'papa')) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }else {
            $query .= "WHERE semillerista LIKE '$semillerista%' AND estado IN (4,5,6) ";
            $query .= "AND gestion = '" . date('Y') . "' ORDER BY comunidad";
        }

        #echo $query;
        DBConnector::ejecutar($query);
        if(DBConnector::nroError())
            echo "ERROR ::".__METHOD__.":: ",DBConnector::mensaje();
    }

    /**
     * lista las semilleras de acuerdo al sistema en el que esta registrada
     * y la etapa en la cual se encuentra
     * @param string $etapa    etapa en la cual se desea buscar
     * @param string $sistema  sistema al que pertenece
     *L
     * @return void
     * */
    public function getSemillerasBySistema($etapa, $sistema = 'admin') {
        $query = "SELECT DISTINCT (semi.nombre) FROM solicitud";

        switch ($etapa) {
            case 'solicitud' :
                $query .= " sol INNER JOIN semillera semi ON semi.id_semillera=sol.id_semillera ORDER BY semi.nombre ASC";

                break;
            case 'semilla' :
                $query = "SELECT DISTINCT(semillera) FROM view_solicitudes WHERE estado=0 AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                #echo $query;
                break;
            case 'superficie' :
                $query .= " WHERE (laboratorio>=1 AND laboratorio<=4) AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                //echo $query; exit;
                break;
            case 'inspeccion' :
                $query .= " WHERE (laboratorio>=5 AND laboratorio<=8) AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                #echo $query; exit;
                break;
            case 'cosecha' :
                $query .= " WHERE laboratorio=8 AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                break;
            case 'semillaP' :
                $query .= " WHERE laboratorio=9 AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                #semilla de papa pasa directamente a esta etapa no  pasa pruebas de laboratorio
                break;

            case 'cuenta' :
                $query = "SELECT DISTINCT(semillera) AS semilleras,id_semillera FROM view_solicitudes ";
                $query .= "WHERE (estado>=1 AND estado<12) AND sistema LIKE '$sistema%' ORDER BY semillera ASC";
                #echo $query;exit;
                break;
            case 'acreedores' :
                $query = "SELECT DISTINCT(semillera) FROM view_solicitudes WHERE id_sistema = $sistema ORDER BY semillera";
                break;
            default :
                $query = "SELECT DISTINCT(semillera) FROM view_solicitudes WHERE id_sistema = $sistema";
                break;
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * busqueda de coincidencias segun opcion
     * @param string $opcion   nombre de la opcion [semillera|semillerista|campanha|cultivo|variedad|]
     * @param string $buscar   cadena de busqueda
     * @param string $sistema  nombre del sistema para buscar
     * */
    public function getSemillerasCuenta($opcion, $buscar, $sistema = 'admin') {
        switch($opcion) {
            case 'semillera' :
                $query = "SELECT DISTINCT(semillera) AS semilleras FROM view_solicitudes ";
                $query .= "WHERE (estado>=1 AND estado<13) AND sistema LIKE '$sistema%' AND semillera LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY semillera ASC";
                break;
            case 'semillerista' :
                $query = "SELECT DISTINCT(semillerista) FROM view_solicitudes ";
                $query .= "WHERE (estado>=1 AND estado<13) AND sistema LIKE '$sistema%' AND semillerista LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY semillerista ASC";
                break;
            case 'campanha' :
                $query = "SELECT DISTINCT(campanha) FROM view_semilla ";
                $query .= "WHERE (estado>=1 AND estado<13) AND sistema LIKE '$sistema%' AND campanha LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY campanha ASC";
                break;
            case 'cultivo' :
                $query = "SELECT DISTINCT(cultivo) FROM view_semilla ";
                $query .= "WHERE (estado>=1 AND estado<13) AND sistema LIKE '$sistema%' AND cultivo LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY cultivo ASC";
                break;
            case 'variedad' :
                $query = "SELECT DISTINCT(variedad) FROM view_semilla ";
                $query .= "WHERE (estado>=1 AND estado<13) AND sistema LIKE '$sistema%' AND variedad LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY variedad ASC";
                break;
        }
       #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR :: busqueda para cuenta en view_semilla | view_solicitud ',DBConnector::mensaje();
    }
    /**
     * busqueda de coincidencias segun opcion usado en fiscalizacion
     * @param string $opcion   nombre de la opcion [semillera|semillerista|cultivo|variedad|]
     * @param string $buscar   cadena de busqueda
     * @param string $sistema  nombre del sistema para buscar
     * */
    public function getSemillerasCuentaFiscal($opcion, $buscar) {
        switch($opcion) {
            case 'semillera' :
                $query = "SELECT DISTINCT(semillera) AS semilleras FROM view_semilla_fiscal ";
                $query .= "WHERE (estado_solicitud>=1 AND estado_solicitud<13) AND semillera LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY semillera ASC ";
                break;
            case 'semillerista' :
                $query = "SELECT DISTINCT(semillerista) FROM view_semilla_fiscal ";
                $query .= "WHERE (estado_solicitud>=1 AND estado_solicitud<13) AND semillerista LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY semillerista ASC ";
                break;
            case 'campanha' :
                $query = "SELECT DISTINCT(campanha) FROM view_semilla_fiscal ";
                $query .= "WHERE (estado_solicitud>=1 AND estado_solicitud<13) AND campanha LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY campanha ASC ";
                break;
            case 'cultivo' :
                $query = "SELECT DISTINCT(cultivo) FROM view_semilla_fiscal ";
                $query .= "WHERE (estado_solicitud>=1 AND estado_solicitud<13) AND cultivo LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY cultivo ASC ";
                break;
            case 'variedad' :
                $query = "SELECT DISTINCT(variedad) FROM view_semilla_fiscal ";
                $query .= "WHERE (estado_solicitud>=1 AND estado_solicitud<13) AND variedad LIKE '$buscar%' AND gestion = '".date('Y')."' ";
                $query .= "ORDER BY variedad ASC ";
                break;
        }
       #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR :: busqueda para cuenta en view_semilla_fiscal ',DBConnector::mensaje();
    }
    /**
     * lista de semilleras segun el estado y sistema
     * */
    public function getSemillerasBySistemaAndEstado($estado, $sistema) {
        $query = "SELECT semillera FROM view_solicitudes WHERE estado = $estado AND sistema LIKE '$sistema%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * Semilleristas que pertenecen a una semillea y tienen determinado cultivo y todas sus variedades
     * */
    public function getSemilleristaVariedadesBySemilleraCultivo($semillera, $cultivo) {
        $query = "SELECT * from view_detalle_produccion_costos";

        DBConnector::ejecutar($query);
    }

    /**
     * lista de semilleras para busqueda de gestiones anteriores
     * */
    public function getSemilleraForBusqueda($area) {
        $query = "SELECT DISTINCT(semillera) FROM view_gestiones_anteriores ";
        $query .= "WHERE gestion < " . date('Y') . " AND id_sistema = $area";

        DBConnector::ejecutar($query);
    }

    /**
     * productores de una semillera y etapa en la q se encuentran
     * si semillera es 0 devuelve el total
     * @param $semillera mixed   nombre de la semillera
     * */
    public static function getProductoresFiscal($semillera) {
        $query = "SELECT semillerista,estado_solicitud,id_solicitud,cultivo ";
        $query .= "FROM view_semilla_fiscal ";
        $query .= "WHERE semillera='$semillera' AND gestion = '".date('Y')."'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo 'ERROR view_semilla_fiscal:: ',DBConnector::mensaje();
    }

    /**
     * productores de una semillera y etapa en la q se encuentran
     * si semillera es 0 devuelve el total
     * @param $semillera mixed   nombre de la semillera
     * */
    public static function getProductores($semillera = '', $sistema = '') {
        if ($semillera == 'contar') {
            $query = "SELECT COUNT(DISTINCT(nombre_apellido)) AS total ";
            $query .= "FROM view_lst_semilleristas WHERE id_sistema LIKE '$sistema%' ORDER BY nombre_apellido";
        } else {
            $query = "SELECT id_solicitud AS id,semillerista,nombre,apellido,estado ";
            $query .= "FROM view_solicitudes WHERE semillera LIKE '$semillera%' AND gestion = '".date('Y')."' ORDER BY estado";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * productores de una semillera y etapa en la q se encuentran
     * si semillera es 0 devuelve el total
     * @param $semillera mixed   nombre de la semillera
     * */
    public static function getProductoresCertifica($semillera = '') {
        if ($semillera == 'contar') {
            $query = "SELECT COUNT(DISTINCT(nombre_apellido)) AS total ";
            $query .= "FROM view_lst_semilleristas WHERE id_sistema LIKE '$sistema%' ORDER BY nombre_apellido";
        } else {
            $query = "SELECT id_solicitud AS id,semillerista,nombre,apellido,estado ";
            $query .= "FROM view_solicitudes WHERE semillera LIKE '$semillera%' AND sistema='certificacion' AND gestion = '".date('Y')."' ";
            $query .= "ORDER BY apellido ASC";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * lista de semilleristas de una semillera
     * */
    public static function getDetalleProduccionBySemilleraCultivo($semillera, $cultivo) {
        $query = "SELECT * FROM view_detalle_produccion_costos WHERE semillera = '$semillera'AND cultivo='$cultivo'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * productores de una semillera y etapa en la q se encuentran
     * si semillera es 0 devuelve el total
     * @param $semillera mixed   nombre de la semillera
     * */
    public static function getProductoresByTime($desde, $hasta, $semillera = '', $sistema = '', $gestion = '',$comunidad='') {
        if ($semillera == 'detalle') {
            $query = "SELECT semillerista FROM view_solicitudes ";
            $query .= "WHERE id_sistema = $sistema AND comunidad = '$comunidad'";
        } else {
            if ($semillera == 'contar') {
                $query = "SELECT COUNT(semillerista) AS total  FROM view_solicitudes ";
                $query .= "WHERE id_sistema = $sistema ";
            } else {
                $query = "SELECT id_solicitud AS id,semillerista,nombre,apellido,estado FROM view_solicitudes ";
                $query .= "WHERE semillera='$semillera' ";
            }
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado >= 12";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado >= 12";
        }

        if ($gestion != '') {
            $query .= " AND gestion = '$gestion'";
        }

        #echo $query;
        
        
        if($semillera == 'detalle'){
            DBConnector::ejecutar($query);
            if (DBConnector::nroError())
              echo 'ERROR EN DETALLE :: ',DBConnector::mensaje(),'=',$query;
        } 
        if ($semillera == 'contar'){
            DBConnector::ejecutar($query);
            if (DBConnector::nroError())
                echo "ERROR EN CONTAR ::",DBConnector::mensaje(),'=',$query;
            $row = DBConnector::objeto();
            return $row -> total;  
        }
    }

    public static function getListProductoresBySemilleraForGraph($semillera) {
        $semillera = ($semillera == '*') ? '*' : $semillera;

        $query = "SELECT DISTINCT(semillerista) FROM view_semillera_productores ";
        $query .= "WHERE semillera LIKE '$semillera'";

        DBConnector::ejecutar($query);
    }

    /**
     * productores segun etapa
     * @param integer $semillera  id de la semillera
     * @param string  $tabla      nombre de la etapa
     * @param integer $opt        id de sistema
     *
     * @return lista de semilleristas segun etapa que se encuentran
     *
     */
    public static function getProductoresSemillera($semillera, $tabla, $opt) {
        #echo $tabla.'='.$opt;
        switch ($tabla) {
            case 'semilla' :
                $query = "SELECT DISTINCT(nombre_apellido) FROM view_lst_semilleristas ";
                $query .= "WHERE estado = 0 AND id_semillera  = $semillera and id_sistema LIKE '$opt%' ORDER BY apellido";
                #echo $query;
                break;
            case 'superficie' :
                $query = "SELECT DISTINCT (CONCAT (nombre,' ',apellido)) AS nombre, sem.id_semillerista ";
                $query .= "FROM solicitud sol INNER JOIN semillerista sem ON sem.id_semillerista=sol.id_semillerista ";
                $query .= "WHERE sol.estado = 1 AND sol.id_semillera=$semillera";
                #echo $query;exit;
                break;
            case 'inspeccion' :
                $query = "SELECT * FROM view_lst_semillerista_inspeccion
                WHERE (estado_solicitud >=4 AND estado_solicitud<=6) AND id_semillera=$semillera AND estado>=3";
                #echo $query;
                break;
            case 'cosecha' :
                $query = "SELECT * FROM view_lst_semillerista_cosecha ";
                $query .= "WHERE id_semillera = $semillera ORDER BY nombre";
                #echo $query;exit;
                break;
            case 'semillaProducida' :
                $query = "SELECT DISTINCT(nombre) FROM view_lst_semillerista_semilla_prod WHERE ((estado>7 AND estado<11) OR (estado=8 AND cultivo='Papa')) AND id_semillera = '$semillera'";
                break;
            case 'cuenta' :
                $query = "SELECT DISTINCT(nombre_apellido) AS nombre FROM view_lst_semilleristas WHERE (estado >= 1 AND estado < 12) AND id_semillera = $semillera";

                break;
            case 'semilleristas' :
                $query .= "sol INNER JOIN superficie sup ON sup.id_solicitud = sol.id_solicitud 
				WHERE ( sol.laboratorio = 11 OR (sup.estado=1 AND sup.aprobada=0)) AND sol.semillera='$semillera' ORDER BY apellido";
                break;
            case 'detalle' :
                $query = "SELECT CONCAT(nombre,' ',apellido) AS nombre FROM view_solicitudes_certificacion WHERE id_semillera = $semillera AND sistema='$opt'";
                break;
            default : {
                if (Sistema::getIdSistema($opt) == 1 || !substr_compare($opt, 'certifica', 0, 9, FALSE)) {
                    $query = "SELECT DISTINCT(CONCAT(nombre,' ',apellido)) AS nombre,id_semillerista  FROM view_solicitudes_certificacion ";
                    $query .= "WHERE sistema LIKE '" . substr($opt, 0, 3) . "%' AND id_semillera = $semillera AND estado = 0";
                } else {
                    $query = "SELECT * FROM view_solicitudes";
                }
            }
        }
        #echo $query;
        DBConnector::ejecutar($query);
        #echo DBConnector::mensaje();
    }

    /**
     * Obtiene id de solicitud de un productor segun la etapa en la que se encuentra por nombre y apellido
     * @param string $nombre nombre del solicitante
     * @param string $apellido apellido del solicitante
     * @param string $tabla(opcional)  nombre de la etapa
     * @param string @semillera (opcional) nombre de la semillera
     *
     */
    public function getNroSolicitud($nombre, $apellido, $tabla = '', $semillera = '') {
        $query = "SELECT MAX(id_solicitud) AS isolicitud FROM solicitud ";

        switch ($tabla) {
            case 'semilla' :
                $query = "SELECT MAX(id_solicitud) AS isolicitud FROM view_full_solicitudes WHERE estado=0 AND semillera='$semillera' AND nombre LIKE '%$nombre%' AND apellido LIKE '%$apellido%'";
                #echo $query;
                break;
            case 'superficie' :
                $query = "SELECT MAX(id_solicitud) AS isolicitud FROM view_solicitudes
                WHERE (estado >= 1 AND estado<=7) AND (  (nombre LIKE '$nombre%' AND apellido LIKE '$apellido%') AND semillera LIKE '%$semillera%')";
                #echo $query;
                break;

            case 'inspeccion' :
                $query = "SELECT MAX(id_solicitud) AS isolicitud FROM view_full_solicitudes WHERE (estado =4) AND ( (nombre LIKE '$nombre%' " . $add . ") AND semillera='$semillera')";
                #echo $query;
                break;
            /*
             case 'cosecha' :
             $query .= "WHERE (laboratorio >= 6 AND laboratorio <=8) AND ( (nombre LIKE '$nombre%' AND apellido LIKE '$apellido%') AND semillera='$semillera')";
             break;
             case 'semillaProducida' :
             $query .= "sol INNER JOIN superficie sup ON sup.id_solicitud=sol.id_solicitud INNER JOIN cosecha cos ON cos.id_superficie=sup.id_superficie
             WHERE (sol.laboratorio = 9 AND cos.estado=0 AND cos.laboratorio=0) OR (sol.laboratorio =10 AND cos.estado=1 AND cos.laboratorio=1) AND
             ( (sol.nombre LIKE '$nombre%' AND sol.apellido LIKE '$apellido%') OR sol.semillera='$semillera')";
             break;*/
            case 'cuenta' :
                $query = "SELECT MAX(sol.id_solicitud) AS isolicitud
             FROM solicitud sol INNER JOIN superficie sup ON sup.id_solicitud=sol.id_solicitud
             WHERE (sol.laboratorio=11 OR (sup.estado=1 AND sup.aprobada=0))";
                break;
        }
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**provincias q coinciden con la busqueda*/
    public static function getProvinciaByNombre($provincia) {
        $query = "SELECT DISTINCT provincia FROM solicitud WHERE (provincia like '$provincia%')";
        //echo $query;
        DBConnector::ejecutar($query);
    }

    /**municipios q coinciden con la busqueda*/
    public static function getMunicipioByNombre($municipio) {
        $query = "SELECT DISTINCT mun.nombre 
        FROM solicitud sol
        INNER JOIN sistema sis ON sis.id_sistema = sol.id_sistema
        INNER JOIN provincia prov ON prov.id_provincia = sol.id_provincia
        INNER JOIN municipio mun ON mun.id_municipio = sol.id_municipio
        WHERE (mun.nombre LIKE '$municipio%')";

        DBConnector::ejecutar($query);
    }

    /**municipios q coinciden con la busqueda*/
    public static function getMunicipioByIdSolicitud($id) {
        $query = "SELECT municipio FROM view_solicitudes ";
        $query .= "WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '??', $query;
        } else {
            $obj = DBConnector::objeto();
            return $obj -> municipio;
        }
    }

    /**Todos los municipios de certificacion*/
    public static function getListMunicipios($id) {
        $query = "SELECT DISTINCT(municipio) FROM view_solicitudes WHERE id_sistema = $id ORDER BY municipio";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**
     * lista de municipios de un rango y sistema
     * */
    public static function getListMunicipiosByTime($id, $desde, $hasta, $gestion,$tipo='') {
        if ($tipo != 'detalle'){
            $query = "SELECT DISTINCT(municipio) FROM view_solicitudes ";
            $query .= "WHERE id_sistema = $id ";
        }else{
            $query = "SELECT municipio FROM view_solicitudes ";
            $query .= "WHERE id_sistema = $id ";
        }
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND estado >= 12 AND gestion = '$gestion'";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND estado >= 12 AND gestion = '$gestion'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo DBConnector::mensaje(),'=',$query;
    }

    /**
     * numero de productores de una semillera
     * */
    public static function getCantidad($semillera,$sistema='certifica') {
        $query = "SELECT COUNT(*) AS cantidad "; 
        $query .= "FROM view_solicitudes ";
        $query .= "WHERE  semillera LIKE '$semillera' AND sistema LIKE '$sistema%' AND gestion = '".date('Y')."'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**comunidad q coinciden con la busqueda (nombre de comunidad)
     * @param string $comunidad nombre de la comunidad
     * */
    public static function getComunidadByNombre($comunidad) {
        $query = "SELECT DISTINCT comunidad FROM view_lst_comunidades WHERE (comunidad LIKE '$comunidad%')";

        DBConnector::ejecutar($query);
    }

    /**comunidad de un determinado semillerista de una semillera
     * @param string $comunidad nombre de la comunidad
     * */
    public static function getComunidadByNombreApellidoSemillera($nombre, $apellido, $semillera, $sistema) {
        $query = "SELECT comunidad FROM view_solicitudes ";
        $query .= "WHERE nombre LIKE '$nombre%' AND apellido LIKE '%$apellido%' AND semillera LIKE '$semillera%' AND id_sistema =$sistema";
        #echo $query;
        DBConnector::ejecutar($query);

        $obj = DBConnector::objeto();

        return $obj -> comunidad;
    }

    /**
     * comunidad segun  nro solicitud, semillera, semillerista
     * */
    public static function getComunidadByNroSemilleraSemillerista($nro, $semillera, $semillerista) {
        $query = "SELECT comunidad FROM view_solicitudes_fiscalizacion ";
        $query .= "WHERE nro_solicitud = '$nro' AND semillera LIKE '$semillera%' AND semillerista LIKE '%$semillerista%'";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * comunidad segun el id
     * @param integer  $id  id de solicitud
     * */
    public function getComunidadById($id) {
        $query = "SELECT comunidad FROM view_lst_comunidades WHERE id_solicitud=$id";
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**
     * devuelve el cultivo y la variedad segun el nro de solicitud
     * */
    public static function getCultivoVariedadByNroSolicitud($nro) {
        $query = "SELECT cultivo,variedad ";
        $query .= "FROM view_cultivo_variedad ";
        $query .= "WHERE nro_solicitud = '$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
    }
    /**
     * devuelve el cultivo y la variedad segun el nro de solicitud
     * */
    public static function getCultivoVariedadFiscal($nro) {
        $query = "SELECT cultivo,variedad ";
        $query .= "FROM view_semilla_fiscal ";
        $query .= "WHERE nro_campo = '$nro'";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__."::$query::".DBConnector::mensaje();
        }
    }

    /**comunidad q coinciden realizacon certificacion*/
    public static function getListComunidades($sistema, $area) {
        $query = "SELECT count(DISTINCT comunidad) AS comunidades FROM view_solicitudes ";
        $query .= "WHERE (sistema LIKE '$sistema%') ORDER BY comunidad";
        #echo $query; exit;
        DBConnector::ejecutar($query);
    }

    /**comunidad q coinciden realizacon certificacion*/
    public static function getListComunidadesByTime($sistema, $desde, $hasta, $gestion) {
        $query = "SELECT count(DISTINCT comunidad) AS comunidades FROM view_solicitudes ";
        $query .= "WHERE sistema LIKE '$sistema%' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado >= 12 AND gestion = '$gestion'";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado >= 12 AND gestion = '$gestion'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError())
            echo DBConnector::mensaje(),'=',$query;
        else{
            $row = DBConnector::objeto();
            return $row -> comunidades;
        }         
    }

    /**comunidad q coinciden realizacon certificacion*/
    public static function getListaDetalleComunidadesByTimeAndMunicipio($municipio, $area, $desde, $hasta, $gestion) {
        $query = "SELECT DISTINCT(comunidad) FROM view_solicitudes ";
        $query .= "WHERE id_sistema = $area AND municipio='$municipio' ";
        if (in_array($desde, array("01", "04", "07", "10"))) {
            $query .= "AND (mes >= '$desde' AND mes <= '$hasta') AND ";
            $query .= "estado > 0 AND gestion = '$gestion'";
        } else {
            $query .= "AND (fecha>='$desde' AND fecha<='$hasta') AND ";
            $query .= "estado >= 10 AND gestion = '$gestion'";
        }
        #echo $query;
        DBConnector::ejecutar($query);
    }

    /**etapa a la cual llego* un semillerista*/
    public static function getEtapaLaboratorio($nombre, $apellido) {
        $query = "SELECT sol.laboratorio 
		           FROM solicitud sol INNER JOIN superficie sup ON sup.id_solicitud=sol.id_solicitud
		                              INNER JOIN semilla sem ON sem.id_solicitud=sol.id_solicitud
		           WHERE ( (sol.laboratorio=11)
		                OR (sol.laboratorio=3 AND sup.aprobada=0 AND sup.estado=1)
		                AND  sol.nombre LIKE '%$nombre%' AND sol.apellido LIKE '%$apellido%')";
        //echo $query;
        DBConnector::ejecutar($query);

        $fila = DBConnector::resultado();

        return $fila -> laboratorio;
    }

    /**
     * devuelve el estado de una solicitud segun el id de solicitud
     * @param integer  $id  id de solicitud
     *
     * @return numero de estado
     * */
    public static function getEstado($id) {
        $query = "SELECT estado,comunidad FROM view_solicitudes WHERE id_solicitud = $id";
        #echo $query;exit;
        return DBConnector::ejecutar($query);

        #return json_encode(DBConnector::objeto());
    }

    /**
     * devuelve el estado de una solicitud segun el id de solicitud
     * @param integer  $id  id de solicitud
     *
     * @return numero de estado
     * */
    public static function getEstadoRegistro($id) {
        $query = "SELECT estado FROM view_solicitudes WHERE id_solicitud = $id";
        #echo $query;
        DBConnector::ejecutar($query);
        if (DBConnector::nroError()) {
            echo DBConnector::mensaje(), '=', $query;
        } else {
            $obj = DBConnector::objeto();
            return $obj -> estado;
        }
    }

    /**
     * devuelve el estado de la solicitud de una determinada solicitud
     * por id de solicitud
     * */
    public static function getEstadoSolicitud($id) {
        $query = "SELECT estado FROM view_solicitudes WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
        if (DBConnector::nroError()){
            echo "ERROR ::".__METHOD__.":: $query :: ",DBConnector::mensaje();
        }else{
            $resultado = DBConnector::objeto();
            return $resultado -> estado;
        }        
    }

    /**
     * devuelve el id de la solicitud
     *  @param string      $nombre      nombre de semillerista
     *  @param string      $apellido    apellido de semillerista
     *  @param string      $semillera   nombre de la semillera
     * */
    public static function getEstadoByNombreApellidoSemillera($nombre, $apellido, $semillera) {
        $query = "SELECT estado FROM view_solicitudes WHERE nombre='$nombre' AND apellido='$apellido' AND semillera='$semillera'";
        #echo $query;
        DBConnector::ejecutar($query);
        //echo DBConnector::mensaje();
        $resultado = DBConnector::objeto();

        return $resultado -> estado;
    }

    /**
     * lista todas las comunidades
     * */
    public function getNroParcelas($id) {
        $query = "SELECT comunidad,nro_parcelas,count_parcelas 
                  FROM solicitud 
                  WHERE id_solicitud=$id";

        DBConnector::ejecutar($query);
    }

    /**
     * formatea los datos para su uso en un script sql
     * id_solicitud int
     * nro_solicitud string
     * sistema string
     * departamento string
     * provincia string
     * municipio string
     * comunidad string
     * localidad string
     * nro_parcelas int
     * count_parcelas int
     * semillera string
     * nombre string
     * apellido string
     * laboratorio int
     * fecha date
     * */
    public function getSolicitudSQL() {
        $dates = new Funciones();
        $query = 'INSERT INTO solicitud (id_semillera,id_sistema,id_municipio,id_semillerista,comunidad,fecha,estado) VALUES ';
        $this -> getSolicitudes();

        if (DBConnector::filas() > 0) {
            while ($row = DBConnector::objeto()) {
                $query .= "(" . $row -> id_semillera . ",";
                $query .= $row -> id_sistema . "," . $row -> id_municipio . ",";
                $query .= $row -> id_semillerista . ",'" . $row -> comunidad . "',";
                $query .= "'" . $row -> fecha . "'," . $row -> estado . "),\r\n";
            }
            $query = substr($query, 0, strlen($query) - 3);
            $query .= ";";
        }
        #echo $query;exit;
        return $query;
    }

}

// END
?>