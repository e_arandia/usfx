<?php
/*
 * SERVER     nombre o direccion IP del servidor
 * DB_USER    nombre de usuario de la base de datos
 * DB_PASS    password de la base de datos
 * DB_NAME    nombre de la base de datos
 * SESSION    nombre de la sesion
 * IMG        nombre de la carpeta de imagenes de reportes
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'iniaf');
define('DB_PASS', 'CERFISLAB');
define('DB_NAME', 'iniaf');
define('SESSION','INIAF');
/*
 * configuracion del sistema  
 */
//
#define('URL','http://192.168.1.5/');
define('URL','http://localhost/');
define('HOME_FOLDER','usfx');
define('BACKUP_FOLDER','backupSICEF/');
define('IMG', '/images');
define('REPORT','/reportes');
define('LIBS', '/libs');
define('CSS','/css');
define('JS','/js');
define('MODEL','/modelo');
define('VIEW','/vista');
define('TEMPLATE','/templates');
?>